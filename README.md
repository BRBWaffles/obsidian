# Obsidian Vault Instructions

---

## Reproduce the Vault

#### Option 1: Clone via HTTPS:

- Clone this repo to your PC with **HTTPS**.
	- Install [**Git**](https://git-scm.com/downloads).
	- Choose a directory to want the repo to be in.
	- Open the terminal and use the command `cd <your-directory>'
	- Once in the directory, use the command `git clone https://gitlab.com/BRBWaffles/obsidian.git`

You may need to set up your **Git** user configuration in order to clone the repo. Here are the commands you need to run:

`git config --global user.name John Doe`

`git config --global user.email johndoe@example.com`

#### Option 2: Download via zip:

- Download this git repo to your PC as a **zip**.
	- Near the top of this page, click the **"Code"** button.
	- Under **"Download source code"**, select **zip**.
	- Once downloaded, unzip the **obsidian-master.zip** file wherever you want.

**Option 1** is preferable to **Option 2**, as you'll be able to `cd` to the vault **root directory** and use the `git pull` terminal command to pull directly from the repo if there are any changes. Using **Option 2** may look easier, but it also means you need to update the vault manually by redownloading the zip file whenever there are changes made to the **GitLab** repo.

---

## Initialize the Vault

- Download and install [**Obsidian**](https://obsidian.md/).
- Open **Obsidian** and select **Open folder** as vault.
- Navigate to the vault folder, select it, and click **Select Folder**.
- Once the vault has loaded into **Obsidian**, click the 🔍.

---

## Navigate the Vault

Now you're ready to navigate the vault. You can search for any nutrition-related topic you wish. For example, if you want to find information on whole grains, you can search with the hashtag **"#whole_grains"** or the keyword **"whole grains"** to find all notes related to whole grains.

To learn more about **Obsidian**, you can visit the following links:

- [**Obsidian Help Page**](https://help.obsidian.md/Obsidian/Index)
- [**Obsidian Discord Server**](https://discord.com/invite/veuWUTm)
- [**Obsidian Beginner Guide**](https://www.youtube.com/watch?v=QgbLb6QCK88)

---

## Support the Vault

The vault is a work in progress and has taken a great deal of time to build thus far. If you have found this resource useful and wish to support its development, consider pledging to the [**Nutrivore Patreon**](https://www.patreon.com/thenutrivore)!

If you wish to contribute to the vault, submit a [**Merge Request**](https://gitlab.com/BRBWaffles/obsidian/-/merge_requests). If you do make a **Merge Request**, make sure to add the **.obsidian** folder to your **.gitignore** to avoid unnecessary merge conflicts.

---

## Follow the Vault

Lastly, be sure to ⭐ the repo and click the 🔔 to be notified of any changes to the vault!