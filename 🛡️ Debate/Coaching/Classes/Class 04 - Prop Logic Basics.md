<div style="page-break-after: always;">

## Tautologies

#### 1. Law of Identity

This fundamental law of logic states that every object is identical to itself. It emphasizes the consistency and predictability of entities in logical and mathematical expressions by affirming that an object or idea must always be exactly what it is and not something else.

>**Example:** Asserting that if 'x is a cat', then 'x is a cat'; self-evident identity.

#### 2. Law of Non-Contradiction

This principle asserts that a statement and its negation cannot both be true at the same time. It is a foundational rule in classical logic, ensuring that contradictions do not exist within a logically coherent system, thereby maintaining the system’s integrity.

>**Example:** For 'x is a bird', it cannot be true and not true at the same time.

#### 3. Law of Excluded Middle

This law holds that for any proposition, either that proposition is true, or its negation is true—there is no middle ground or third option. This law underpins the binary nature of traditional logical determinations, reinforcing a clear separation between truth and falsity.

>**Example:** For 'x is alive', either 'x is alive' is true or 'x is alive' is false, no third possibility.

---

## Other Principles

#### 1. Principle of Explosion

This principle states that from a contradiction, any conclusion can be validly derived. Essentially, it suggests that once falsehood is introduced, anything can logically follow.

>**Example:** Assuming P∧¬P, derive Q: From P∧¬P, infer any Q (e.g., 'unicorns exist').

#### 2. De Morgan's Law

The negation of a conjunction is equivalent to the disjunction of the negations, and the negation of a disjunction is equivalent to the conjunction of the negations.

>**Example:** Using ¬(P∧Q)≡¬P∨¬Q, an application to natural language might be like "if it's not raining outside and if it's not cold outside, then it is neither raining nor cold outside (e.g., ¬(raining∧cold)≡¬raining∨¬cold).

---

</div>
<div style="page-break-after: always;">

## Formal Fallacies

#### 1. Affirming the Consequent

This occurs when someone incorrectly assumes the cause based on an effect that can also result from other causes.

>**Form:** If P then Q, Q is true; therefore, P must be true.

>**Example:** If it rains, the street is wet. The street is wet, so it must have rained. (The street could be wet for other reasons.)

#### 2. Denying the Antecedent

This fallacy arises when someone wrongly concludes the absence of an outcome based on the absence of one possible cause, ignoring other causes that might produce the same outcome.

>**Form:** If P then Q, P is false; therefore, Q must be false.

>**Example:** If I am in Paris, I am in France. I am not in Paris, so I am not in France. (I could be elsewhere in France.)

#### 3. Fallacy of the Undistributed Middle

This involves a mistaken inference that because two categories share a property, they are the same, overlooking their distinctions.

>**Form:** P implies Q, R implies Q; therefore, P implies R.

>**Example:** If there is a dog, then it is a mammal. If there is a cat, then it is a mammal. Therefore, if there is a dog, then there is a cat.

#### 4. Illicit Major

This error is made when the conclusion improperly generalizes about all members of a category based on shared characteristics with a broader group.

>**Form:** All X are Y, all Z are Y; therefore, all X are Z.

>**Example:** All females are humans. All males are humans. Therefore, all females are male. 

</div>
<div style="page-break-after: always;">

#### 5. Illicit Minor

This mistake happens when an assumption that two subgroups share the same properties because they belong to the same larger group is incorrectly made.

>**Form:** All X are Y, all X are Z; therefore, all Y are Z.

>**Example:** All apples are fruit. All apples are red. Therefore, all fruit are red.

#### 6. Fallacy of Exclusive Premises

This involves drawing a conclusion about two groups based on their separate exclusion from a third group, which logically does not follow.

>**Form:** No P is Q, No R is Q; therefore, No P is R.

>**Example:** No Scottish people are English people. No Polish people are English people. Therefore, no Scottish people are Polish people. 

#### 7. Fallacy of Four Terms

The fallacy of four terms is the formal fallacy that occurs when a syllogism has four (or more) terms rather than the requisite three, rendering it invalid. 

>**Form:** P implies Q, R implies S; therefore, P implies S.

>**Example:** All humans are mammals. All dogs are pets. Therefore, all humans are pets.

#### 8. Affirmative Conclusion from a Negative Premise

This fallacy occurs when an argument erroneously includes four distinct terms in a categorical syllogism, preventing a proper conclusion.

>**Form:** No P is Q. All R are P. Therefore, no R is Q.

>**Example:** No Scottish people are English. All Americans are Scottish. Therefore, no Americans are English.

</div>
<div style="page-break-after: always;">

---

# Hashtags

#debate 
#debate_coursework 
