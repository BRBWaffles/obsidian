## Logic

Logic is a broad field that encompasses many different branches and sub-fields, and studies the principles and rules of reasoning, inference, and argumentation. It is concerned with the validity and soundness of arguments, and with the relationships between statements and/or propositions.

When we think of logic as it pertains to debate, we will typically be thinking about propositional logic. But this is not the only logic that exists. Propositional logic is just one branch of logic among many, and there are many other types of logics that are studied in philosophy, mathematics, computer science, and other fields. While propositional logic deals with propositions, which are statements that can be either true or false, other types of logics deal with other kinds of objects and concepts.

For example, predicate logic extends propositional logic by introducing the concept of predicates, which are statements that express properties of objects. Modal logic deals with the concepts of possibility, necessity, and contingency. Fuzzy logic deals with concepts that have degrees of truth or falsity, rather than being strictly true or false. Deontic logic deals with the concepts of obligation, permission, and prohibition. Temporal logic deals with the concepts of time and change.

Here are some examples of different types of logics:

|      **TYPES**       |       **OF**        |      **LOGICS**      | 
|:--------------------:|:-------------------:|:--------------------:|
| Propositional logic  |   Predicate logic   |     Modal logic      |
|     Fuzzy logic      |    Deontic logic    |   Epistemic logic    |
|    Temporal logic    | Non-monotonic logic | Intuitionistic logic |
|   Relevance logic    |     Free logic      | Substructural logic |
| Paraconsistent logic | Multi-valued logic  |    Quantum logic     |
|  Situation calculus  |  Description logic  |  Dialectical logic   |

## Propositional Logic

Propositional logic has several "laws" or "rules" that are essentially tautologies, that is to say things that are always true on propositional logic:

1.  **Law of identity:** P is always equal to P.
2.  **Law of non-contradiction:** P and ¬P is never true.
3.  **Law of excluded middle:** P or ¬P is always true.

Additionally, there are "laws" or "rules" that leverage different connective operators to establish different inference structures. These are called inference rules. Here are a few basic ones:

1.  **Modus ponens:** If P implies Q, and P is true, then Q must be true.
2.  **Modus tollens:** If P implies Q, and Q is false, then P must be false.
3. **Hypothetical syllogism:** If P implies Q, and Q implies R, and P is true, then R must be true.
4. **Disjunctive syllogism:** If P is true or Q is true, and P is false, then Q is true.

## Scientific Epistemology

Theoretical virtues of science refer to the desirable qualities that scientific theories should possess to be considered good explanations of natural phenomena. Here are some of the most commonly recognized theoretical virtues of science.

1.  **Testibility:** A hypothesis is scientific only if it is testable, that is, only if it predicts something more than what is predicted by the background theory alone.
   
2.  **Fruitfulness** Other things being equal, the best hypothesis is the one that is the most fruitful, that is, makes the most successful novel predictions.
   
3.  **Scope:** Other things being equal, the best hypothesis is the one that has the greatest scope, that is, that explains and predicts the most diverse phenomena.
   
4.  **Parsimony:** Other things being equal, the best hypothesis is the simplest one, that is, the one that makes the fewest assumptions.
   
5.  **Conservatism:** Other things being equal, the best hypothesis is the one that is the most conservative, that is, the one that fits best with established beliefs.

---

# Hashtags

#debate 
#debate_coursework  