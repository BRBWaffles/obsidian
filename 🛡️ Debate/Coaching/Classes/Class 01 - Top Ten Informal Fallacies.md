<div style="page-break-after: always;">

## Informal Fallacies

Fallacies can be thought of as errors in reasoning that are so commonplace that they were eventually given their own names. All fallacies, whether formal or informal, are just different forms of non sequitur. That is to say that all fallacies merely represent different sorts of inferential errors that challenge either the validity or the soundness of an argument.

In this first class, we will cover the top informal fallacies, with formal fallacies being covered in a later class. The following list of the informal fallacies that you are most likely to encounter.

#### 1. Red Herring

A red herring is a type of rhetorical tactic, typically used to obfuscate, that involves referring to an irrelevant point. It can be thought of as a point, reference, or example, that distracts from the main point of a discussion or larger argument. 

>**Example:** "Bacon must be healthy for people because I have a 95-year old grandmother who eats bacon every day!"

#### 2. Begging the Question

Colloquially, begging the question may be understood as merely leaving certain questions unanswered after an argument is rendered. However, in philosophy, begging the question has a very different definition, and refers to a type of circular reasoning. Begging the question specifically refers to the act of presupposing the conclusion of an argument in its premises. That is to say that at least one of the premises of an argument will hinge on that argument's conclusion being true.

>**Example:** "God exists because it is stated in his own words in the Bible!"

#### 3. Strawman

A strawman fallacy is characterized by either intentionally or unintentionally misrepresenting your interlocutor's position or argument, such as to make said position or argument easier to attack. It is also the inverse of the Motte and Bailey fallacy, which is characterized by misrepresenting your own position or argument in order to make it easier to defend.

>**Example:** "People who argue for taxation are pushing communism!"

</div>
<div style="page-break-after: always;">

#### 4. Equivocation

An equivocation occurs when one uses a term with a certain meaning in one part of their argument, like the premises, but also uses the same term with a different meaning in another part of their argument, like the conclusion. This is an extremely common fallacy, and occurs often across virtually all domains of debate.

>**Example:** "The announcer said the game ended with a tie, but I didn't see any string, so the announcer must be wrong."

#### 5. Appeal to Nature

An appeal to nature is characterized by the affirmation that something is good, preferable, or desirable, merely because it is natural. This fallacy is common in the domain of human health, such as when health product advertisers claim that their product is beneficial because it either contains more natural ingredients or fewer artificial ingredients.

>**Example:** "Red meat is clearly healthy for humans if we evolved consuming it!"

#### 6. Appeal to Authority

When one appeals to authority, it simply means that one affirms that a proposition is true in virtue of it being uttered by an authority. This fallacy typically pervasive within any domain wherein there are experts who publicly profess their opinions.

>**Example:** "The carnivore diet is healthy because Paul Saladino concluded this after years of researching diet!"

#### 7. Appeal to Ignorance

An appeal to ignorance is typically defined as affirming that a proposition is true merely because it has not been shown to be false. This is common in domains of science wherein evidence for a particular research question is scant, and the gaps in knowledge can be filled with poor reasoning.

>**Example:** "It's never been shown that blueberries don't cure cancer, so we're safe in assuming that blueberries do cure cancer!"

</div>
<div style="page-break-after: always;">

#### 8. Appeal from Incredulity

The hallmark of this fallacy is assuming that a proposition is false merely because you personally do not believe, or can't imagine, that the proposition is true. This fallacy is tightly tied to the cognitive bias known as confirmation bias, which will be discussed later.

>**Example:** "That's nonsense, because I just can't believe it!"

#### 9. Muddying the Waters

Muddying the waters is less of a fallacy and more of a rhetorical device designed to obfuscate and make one's position extremely ambiguous or unclear. This is extremely prevalent in political or ethical debates, wherein it is common to vaguely gesture at your opponent with the mere appearance of disagreement rather than actually providing clear arguments.

>**Example:** "We all know those studies are bad, because you just follow the money if you want to know the truth!"

#### 10. Genetic Fallacy

The crux of the genetic fallacy is to conclude that a position is wrong merely in virtue of the one uttering the position. This type of fallacy is remarkably common, if not ubiquitous, in the political debate sphere.  

>**Example:** "I know what Joe Biden says is wrong, because Joe Biden is an idiot."

</div>

---

# Hashtags

#debate 
#debate_coursework 