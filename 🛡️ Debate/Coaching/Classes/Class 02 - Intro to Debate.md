<div style="page-break-after: always;">

## Fundamental Concepts

#### 1. Propositions

A proposition is simply a "truth-apt" statement. For a statement to be truth-apt just means that it can be either true or false. This is just to say that the statement has a truth value that can be assigned to it. Propositions are the basic components of arguments in propositional/classical logic.

>**Example:** "It is raining outside."

#### 2. Arguments

In the most basic sense, an argument is a set of premises (or even a single premise) followed by a conclusion, where the premises and conclusions are comprised of propositions. This is even true of mathematical arguments, but it's typically obscured behind shorthand and notation.

>**Example:** If it is raining outside, then the ground is wet. It's raining outside. Therefore, the ground is wet.

#### 3. Validity

Validity is a property of arguments. An argument is valid if, and only if, the conclusion logically follows from the premises. The example from the previous entry in this document (2. Arguments) is an example of a valid argument. Invalid arguments suffer from structural errors that lead to their conclusions not being deducible from their premises.

>**Valid Example:** If there are unicorns on the moon, then the world will end in 1975. There are unicorns on the moon. Therefore, the world will end in 1975.

>**Invalid Example:** If there are unicorns on the moon, then the world will end in 1975. There are unicorns on the moon. Therefore, leprechauns exist.

#### 4. Soundness

Soundness is also a property of arguments. Arguments are sound if, and only if, they are valid and their premises are all true. The above example of a valid argument is valid in its structure, but not sound. Often times an argument that is valid in structure can have untrue premises. Firstly, unicorns don't exist, and secondly, the world didn't end in 1975. So, the argument is valid, but not sound. 

>**Sound Example:** All cats are mammals. Garfield is a cat. Therefore, Garfield is a mammal.

>**Unsound Example:** All cats are mammals. Garfield is not a cat. Therefore, Garfield is not a mammal.

</div>
<div style="page-break-after: always;">

#### 5. Defeaters

Simply speaking, defeaters are types of responses that either significantly, or entirely, deflate the persuasive force of an argument and/or the truth value of a proposition. There are three types of defeaters:

>**Rebutting Defeaters:** An argument that directly negates a proposition and/or renders an argument unsound.

>**Undercutting Defeaters:** An argument that lowers the probability of a proposition being true and/or casts serious doubt on the soundness of an argument.

>**No-Reasons Defeaters:** An argument that demonstrates that there is no reason to believe that a proposition is true and/or an argument is sound.

#### 6. A Priori

A priori knowledge, or justification, is independent of experience or empirical evidence. It is knowledge that can be obtained through reason or logical analysis alone, and does not require any observation or experimentation to be justified.

>**Example:** Objects with three sides are triangles. Boat sails have three sides. Therefore, boat sails are triangles.

#### 7. A Posteriori

Much like the term a priori, a posteriori refers to knowledge or justification. However, unlike a priori, a posteriori knowledge, or justification, is based on experience or empirical evidence. It is knowledge that requires observation or experimentation to be justified, and cannot be obtained through reason or logical analysis alone.

>**Example:** Birds have feathers. Pigeons are birds. Therefore, pigeons have feathers.

#### 8. Analyticity

A proposition is analytic when is true by definition. It is a statement that can be deduced from the meanings of its terms, without any need for empirical evidence or experience to verify its truth or falsity.

>**Example:** Triangles have three sides.

</div>
<div style="page-break-after: always;">

#### 9. Syntheticity

Unlike an analytic proposition, a synthetic proposition is *not* true by definition, but is instead true because of the way the world is. It is a statement that requires empirical evidence or experience to verify its truth or falsity.

>**Example:** Boat sails are triangles.

</div>

---

# Hashtags

#debate 
#debate_coursework 