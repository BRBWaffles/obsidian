## First session - fallacies

1. Top 10 most common fallacies
	1. red herring
	2. begging the question
	3. appeal to nature
	4. appeal to authority
	5. appeal from incredulity
	6. muddying the waters
	7. poisoning the well
	8. gish galloping
	9. appeal to ignorance
	10. motte and bailey
3. Homework
	1. identify 5 examples of informal fallacies correctly

## Second session - intro to debate

1. Reviewing homework
2. Philosophy/debate terms
	1. argument
	2. proposition
	3. soundness
	5. validity
	6. defeaters
	7. a priori
	8. a posteriori
	9. empirical
	10. analytic
	11. synthetic
	12. contradiction
3. Homework
	1. identify 5 examples of terms correctly

## Third session - branches philosophy

1. Reviewing homework
2. Logic
	1. laws of thought/classical logic
		1. law of non-contradiction
		2. law of excluded middle
		3. law of identity
	2. validity
	3. soundness
	4. modality
		1. necessity
		2. contingency
3. Epistemology
	1. skepticism
	2. contextualism
	3. scientific method

## Fourth session - propositional logic

1. Reviewing homework
2. Arguments
	1. deductive
	2. inductive
	3. abductive
3. Propositional logic
	1. truth tables
	2. writing arguments
	3. formal fallacies
	4. principle of explosion
4. Homework
	1. create valid inferences
	2. do a prop logic quiz

## fifth session - debate structure

1. Dialogue flow tree
2. Homework
	1. apply the tree to a debate

## sixth session - mock debate

1. Reviewing homework
2. Mock debate

## Optional class - vegan debate

1. NTT mock dialectic

---

# Hashtags

#debate 
#debate_coursework 

