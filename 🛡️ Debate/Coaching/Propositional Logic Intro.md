## Operators & Symbols

| <font color="CC6600">**Symbol**</font> | <font color="CC6600">**Operation**</font> | <font color="CC6600">**Example**</font> | <font color="CC6600">**Description**</font>                                                              |
|:--------------------------------------:|:-----------------------------------------:|:---------------------------------------:|:-------------------------------------------------------------------------------------------------------- |
|                   →                    |                  Implies                  |                   P→Q                   | A proposition that is only false if Q is false.                                                          |
|                   ↔                    |               Biconditional               |                   P↔Q                   | A proposition that denotes logic equivalence, and is true when both predicates are either true or false. |
|                   ¬                    |                 Negation                  |                   ¬P                    | A proposition that is true if and only if P is false.                                                    | 
|                   ∧                    |                Conjunction                |                   P∧Q                   | A proposition that is true if and only if both P and Q are true.                                         |
|                   ∨                    |                Disjunction                |                   P∨Q                   | A proposition that is false if and only if both P and Q are false.                                       |
|                   ⊻                    |               Exclusive Or                |                   P⊻Q                   | A proposition that is true if and only if either only P or only Q are true.                              |
|                   ∀                    |           Universal Quantifier            |                 ∀x(Px)                  | A proposition that states that for all x, P is true.                                                     |
|                   ∃                    |          Existential Quantifier           |                 ∃x(Px)                  | A proposition that states that there is at least one X, such that P(x) is true.                          |
|                   ∴                    |                 Therefore                 |                   ∴Q                    | The conclusion of a syllogism                                                                            |
|                   :=                   |                Definition                 |            P:=grass is green            | A general symbol used for defining propositions.                                                         |
|                   ()                   |                Parentheses                |                  (P∧Q)                  | A means of grouping propositions together.                                                               |



# Logical Necessity

### Tautology

| <font color="CC6600">**P**</font> | <font color="CC6600">**True**</font> | 
| --------------------------------- | ------------------------------------ |
| T                                 | T                                    |
| F                                 | T                                    |

# Logical Impossibility

### Contradiction

| <font color="CC6600">**P**</font> | <font color="CC6600">**¬P**</font> | <font color="CC6600">**P∧¬P**</font> |
|:---------------------------------:|:----------------------------------:|:------------------------------------ |
|                 T                 |                 F                  | F                                    |
|                 F                 |                 T                  | F                                    | 

# Logical Possibility

### Implication (P→Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P→Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | T                                   |
|                 T                 |                 F                 | F                                   |
|                 F                 |                 T                 | T                                   |
|                 F                 |                 F                 | T                                   | 

### Biconditional (P↔Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P↔Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | T                                   |
|                 T                 |                 F                 | F                                   |
|                 F                 |                 T                 | F                                   | 
|                 F                 |                 F                 | T                                   |

### Conjunction (P∧Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P∧Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | T                                   |
|                 T                 |                 F                 | F                                   |
|                 F                 |                 T                 | F                                   |
|                 F                 |                 F                 | F                                   |                 F                 |                 T                 | F                                   |
 

### Disjunction (P∨Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P∨Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | T                                   |
|                 T                 |                 F                 | T                                   |
|                 F                 |                 T                 | T                                   | 
|                 F                 |                 F                 | F                                   |

### Exclusive Disjunction (P⊻Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P⊻Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | F                                   |
|                 T                 |                 F                 | T                                   |
|                 F                 |                 T                 | T                                   | 
|                 F                 |                 F                 | F                                   |

# Esoteric Logical Possibility

### Not And (P⊼Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P⊼Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | F                                   |
|                 T                 |                 F                 | T                                   |
|                 F                 |                 T                 | T                                   |
|                 F                 |                 F                 | T                                   |

### Not Or (P⊽Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P→Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | F                                   |
|                 T                 |                 F                 | F                                   |
|                 F                 |                 T                 | F                                   |
|                 F                 |                 F                 | T                                   | 

### Material Nonimplication (P↛Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P→Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | F                                   |
|                 T                 |                 F                 | T                                   |
|                 F                 |                 T                 | F                                   |
|                 F                 |                 F                 | F                                   | 

### Converse Nonimplication (P↚Q)

| <font color="CC6600">**P**</font> | <font color="CC6600">**Q**</font> | <font color="CC6600">**P→Q**</font> |
|:---------------------------------:|:---------------------------------:|:----------------------------------- |
|                 T                 |                 T                 | F                                   |
|                 T                 |                 F                 | F                                   | 
|                 F                 |                 T                 | T                                   |
|                 F                 |                 F                 | F                                   |

# Hashtags

#coursework
#logic_course 
#propositional_logic 