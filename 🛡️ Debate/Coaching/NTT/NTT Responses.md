# Humanity

### Genetics tho
>The trait that makes the difference is that humans have a cluster of genetics that distinguishes homo sapiens sapiens as a species, whereas non-human animals do not.

**Response:**
If a genetic test on your neighbour's kid returned the shocking result that they lack those genetics, would it be OK to kill him and eat him?

### Taxonomy tho
>The trait that makes the difference is that humans have the taxonomic classification of homo sapiens sapiens, whereas non-human animals do not.

**Response:**
If we restricted the criteria used to categorize homo sapiens to exclude some populations of people, would they be OK to kill and eat?

### Family Resemblance tho
>The trait that makes the difference is whether or not I can recognise an individual as human.

**Response:**
If we took your mother's consciousness and placed it inside of a cow's body, would it be OK to kill her and eat her?

# Cognition

### Intelligence tho
>The trait that makes the difference is that humans have human-level intelligence, whereas non-human animals do not.

**Response:**
If we found humans with the same level of intelligence as a non-human animal, would it be OK to kill them and eat them?

### Language tho
>The trait that makes the difference is that humans have the capacity to use and/or understand language, whereas non-human animals do not.

**Response:**
If we found humans with the same linguistic capacity as as non-human animals, would it be OK to kill them and eat them?

### Social Contracts tho
>The trait that makes the difference is that humans can enter into social contracts, whereas non-human animals cannot.

**Response:**
If we found humans with the same ability to enter into social contracts as as non-human animals, would it be OK to kill them and eat them?

### Moral Agency tho
>The trait that makes the difference is that humans have moral agency, whereas non-human animals do not

**Response:**
If we found humans with the same capacity for moral agency as non-human animals, would it be OK to kill them and eat them?

# Religious Belief

### Souls tho
>The trait that makes the difference is that humans have a soul, whereas non-human animals do not.

**Response:**
If we had a soul-scanner that could detect whether or not a being had a soul, and it was discovered that your neighbour's kid didn't have a soul, would it be OK to kill him and eat him?

### Image of God tho
>If we had an image-of-God scanner that could detect whether or not a being was made in the image of God, and it was discovered that your neighbour's kid wasn't made in the image of God, would it be OK to kill him and eat him?

**Response:**
What is untrue of a non-human animal that is true of a human, such that if it were made true of a human, the non-human animal would have a soul?

### Word of God tho
>The trait that makes the difference is that God's word, as articulated in the Holy Bible, permits us to consume non-human animals.

**Response:**
If the Holy Bible permitted cannibalism, would it be OK for humans to kill and eat each other?

# Norms

### Culture tho
>The trait that makes the difference is that our culture accepts the consumption of non-human animals, whereas our culture does not accept the consumption of humans.

**Response:**
If our culture developed a tolerance for the consumption of humans, would it be OK to farm humans, kill them, and eat them?

### Tradition tho
>The trait that makes the difference is that non-human animal consumption is traditional for humans, whereas human consumption is not.

**Response:**
If there was a population with a cultural tradition of cannibalism, would it then be OK to farm humans, kill them, and eat them?

### Legality tho
>The trait that makes the difference is that it is illegal to kill and eat humans, but it is not illegal to kill and eat animals.

**Response:**
If cannibalism was made legal, would it then be OK to farm humans, kill them, and eat them?

### Dominion tho
>The trait that makes the difference is that humans have dominion over non-human animals, whereas non-human animals do not have dominion over humans.

**Response:**
If space aliens had dominion over humans, would it be OK for the aliens to farm us, kill us, and eat us?

# Food

### Nutrition tho
>The trait that makes the difference is that non-human animals are rich in nutrients, whereas humans are not.

**Response:**
If it was discovered that human meat was equally as rich in nutrients as the meat of non-human animals, would it be OK to farm humans, kill them, and eat them?

### Agriculture tho
>The trait that makes the difference is that the meat of non-human animals is currently instrumental to sustaining human civilization, but the meat of humans is not.

**Response:**
If we discovered a civilization of humans who were sustaining themselves on human meat in the same way we sustain ourselves on non-human animal meat, is it OK, in your view, for them to farm, kill, and eat those humans?

### Tasty tho
>The trait that makes the difference is that non-human animals are tasty, whereas humans are not.

**Response:**
If it was discovered that human meat was equally as tasty as the meat of non-human animals, would it be OK to farm humans, kill them, and eat them?


# Hashtags

#debate 
#debate_coursework 
#namethetrait
#ntt 