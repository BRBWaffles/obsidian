### NTT Reductio (Non-Negated Antecedent w/ Conjunction)

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**T**</font>      | a being (x) is ethical to consume         |
|      <font color="CC6600">**P**</font>      | a being (x) has fur                       |
|      <font color="CC6600">**Q**</font>      | a being (x) has horns                     |
|      <font color="CC6600">**R**</font>      | a being (x) has hooves                    |
|      <font color="CC6600">**a**</font>      | hypothetical human                        |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all x, where x is a being, they are ethical to consume if, and only if, x has fur, x has horns, and x has hooves.
<br />
<font color="CC6600">
<b>(∀x(Tx↔(Px∧Qx∧Rx)))</b>
<br />
<b>P2)</b></font> A hypothetical human has fur, horns, and hooves.
<br />
<font color="CC6600">
<b>(Pa∧Qa∧Ra)</b>
<br />
<b>C)</b></font> Therefore, A hypothetical human is ethical to consume.
<br />
<font color="CC6600">
<b>(∴Ta)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Tx~4(Px~1Qx~1Rx))),(Pa~1Qa~1Ra)|=(Ta))

### NTT Reductio (Negated Antecedent w/ Conjunction)

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>  |
|:-------------------------------------------:|:------------------------------------------ |
|      <font color="CC6600">**T**</font>      | a being (x) is ethical to consume          |
|      <font color="CC6600">**P**</font>      | a being (x) has average human intelligence |
|      <font color="CC6600">**Q**</font>      | a being (x) has language skills            |
|      <font color="CC6600">**R**</font>      | a being (x) has human genetics             |
|      <font color="CC6600">**a**</font>      | hypothetical schmuman                      |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all x, where x is a being, they are not ethical to consume if, and only if, x has average human intelligence, x has language skills, and x has human genetics.
<br />
<font color="CC6600">
<b>(∀x(¬Tx↔(Px∧Qx∧Rx)))</b>
<br />
<b>P2)</b></font> A hypothetical schmuman does not have human genetics.
<br />
<font color="CC6600">
<b>(¬Ra)</b>
<br />
<b>C)</b></font> Therefore, a hypothetical schmuman is ethical to consume.
<br />
<font color="CC6600">
<b>(∴Ta)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(~3Tx~4(Px~1Qx~1Rx))),(~3Ra)|=(Ta))

### NTT Reductio (Non-Negated Antecedent w/ Disjunction)

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**T**</font>      | a being (x) is ethical to consume         |
|      <font color="CC6600">**P**</font>      | a being (x) has fur                       |
|      <font color="CC6600">**Q**</font>      | a being (x) has horns                     |
|      <font color="CC6600">**R**</font>      | a being (x) has hooves                    |
|      <font color="CC6600">**a**</font>      | hypothetical human                        |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all x, where x is a being, they are ethical to consume if, and only if, x has fur, or x has horns, or x has hooves.
<br />
<font color="CC6600">
<b>(∀x(Tx↔(Px∨Qx∨Rx)))</b>
<br />
<b>P2)</b></font> A hypothetical human has hooves.
<br />
<font color="CC6600">
<b>(Ra)</b>
<br />
<b>C)</b></font> Therefore, A hypothetical human is ethical to consume.
<br />
<font color="CC6600">
<b>(∴Ta)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Tx~4(Px~2Qx~2Rx))),(Ra)|=(Ta))

### NTT Reductio (Negated Antecedent w/ Disjunction)

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>  |
|:-------------------------------------------:|:------------------------------------------ |
|      <font color="CC6600">**T**</font>      | a being (x) is ethical to consume          |
|      <font color="CC6600">**P**</font>      | a being (x) has average human intelligence |
|      <font color="CC6600">**Q**</font>      | a being (x) has language skills            |
|      <font color="CC6600">**R**</font>      | a being (x) has human genetics             |
|      <font color="CC6600">**a**</font>      | hypothetical schmuman                      |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all x, where x is a being, they are not ethical to consume if, and only if, x has average human intelligence, or x has language skills, or x has human genetics.
<br />
<font color="CC6600">
<b>(∀x(¬Tx↔(Px∨Qx∨Rx)))</b>
<br />
<b>P2)</b></font> A hypothetical schmuman does not have average human intelligence, language skills, and human genetics.
<br />
<font color="CC6600">
<b>(¬Pa∧¬Qa∧¬Ra)</b>
<br />
<b>C)</b></font> Therefore, a hypothetical schmuman is ethical to consume.
<br />
<font color="CC6600">
<b>(∴Ta)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(~3Tx~4(Px~2Qx~2Rx))),(~3Pa~1~3Qa~1~3Ra)|=(Ta))

---

# Hashtags

#debate 
#debate_coursework 
#namethetrait
#ntt 