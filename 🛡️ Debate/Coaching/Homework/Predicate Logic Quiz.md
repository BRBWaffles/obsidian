### Inference 1

>**P1)** If one becomes a lean mass hyper responder, then one has unlocked the fountain of youth.
>**P2)** Dave Feldman became a lean mass hyper responder.
>**C)** Therefore, Dave Feldman has unlocked the fountain of youth.

**P1)**
**P2)**
**C)**

### Inference 2

>**P1)** If one eats red meat, then one has increased their risk of cancer.
>**P2)** Shawn Baker eats beef.
>**C)** Therefore, Shawn Baker has increased their risk of cancer.

**P1)**
**P2)**
**C)**

### Inference 3

>**P1)** If fish contain heavy metals, then eating too much fish can give you heavy metal poisoning.
>**P2)** Salmon contains mercury.
>**C)** Therefore, eating too much salmon can give you mercury poisoning.

**P1)**
**P2)**
**C)**

### Inference 4

>**P1)** Some people like to eat kangaroo balls.
>**P2)** Everyone who likes to eat kangaroo balls also likes to eat buffalo balls.
>**C)** Therefore, there are some people who like both kangaroo balls and buffalo balls. 

**P1)**
**P2)**
**C)**

### Inference 5

>**P1)** If the vegetables are food, then they do not have toxins.
>**P2)** Some vegetables have toxins.
>**C)** Therefore, some vegetables are not food.  

**P1)**
**P2)**
**C)**

### Inference 6

>**P1)** If one will live forever, then one is indeed a carnivore.
>**P2)** Micheal Greger is not a carnivore.
>**C)** Therefore, Michael Greger will not live forever. 

**P1)**
**P2)**
**C)**

### Inference 7

>**P1)** If a food is not healthy, then the data shows that the food increases the risk of disease.
>**P2)** RCTs show that seed oils do not increase the risk of disease.
>**C)** Therefore, seed oils are healthy.

**P1)**
**P2)**
**C)**

---

# Hashtags

#debate 
#debate_coursework 
#homework 