### Inference 1

>**P1)** If I eat enough fibre, then I will have good digestion.
>**P2)** I ate enough fibre.
>**C)** Therefore, I will have good digestion.

**A)** Modus Ponens ✓
**B)** Modus Tollens 
**C)** Hypothetical Syllogism
**D)** Disjunctive Syllogism

##### Structure

**P1:** P→Q
**P2:** P
**C:** Q

### Inference 2

>**P1)** If I scarf down too many ketobombs, then I will gain weight.
>**P2)** I did not gain weight.
>**C)** Therefore, I did not scarf down too many ketobombs.

**A)** Modus Ponens
**B)** Modus Tollens ✓
**C)** Hypothetical Syllogism
**D)** Disjunctive Syllogism

##### Structure

**P1:** P→Q
**P2:** ¬Q
**C:** ¬P

### Inference 3

>**P1)** If I eat a variety of fruits and vegetables, then I will get a range of nutrients.
>**P2)** If I get a range of nutrients, then I will prolong my health.
>**C)** Therefore, if I eat a variety of fruits and vegetables, I will prolong my health.

**A)** Modus Ponens
**B)** Modus Tollens
**C)** Hypothetical Syllogism ✓
**D)** Disjunctive Syllogism

##### Structure

**P1:** P→Q
**P2:** Q→H
**C:** P→H

### Inference 4

>**P1)** If I eat too much saturated fat, then I will increase my risk of heart disease.
>**P2)** I did not increase my risk of heart disease.
>**C)** Therefore, I did not eat too much saturated fat.

**A)** Modus Ponens
**B)** Modus Tollens ✓
**C)** Hypothetical Syllogism
**D)** Disjunctive Syllogism

##### Structure

**P1:** P→Q
**P2:** ¬Q
**C:** ¬P

### Inference 5

>**P1)** If I am a lean mass hyper responder, then I am immune to all disease.
>**P2)** I am not immune to all disease.
>**C)** Therefore, I am not a lean mass hyper responder.

**A)** Modus Ponens
**B)** Modus Tollens ✓
**C)** Hypothetical Syllogism
**D)** Disjunctive Syllogism

##### Structure

**P1:** P→Q
**P2:** ¬Q
**C:** ¬P

### Inference 6

>**P1)** If I stay on the Vertical Diet™, then I will get scurvy.
>**P2)** I stayed on the Vertical Diet™.
>**C)** Therefore, I will get scurvy.

**A)** Modus Ponens ✓
**B)** Modus Tollens
**C)** Hypothetical Syllogism
**D)** Disjunctive Syllogism

##### Structure

**P1:** P→Q
**P2:** P
**C:** Q

### Inference 7

>**P1)** I will either drink organic sheep milk or raw bison cum with my marinated ox testicles.
>**P2)** I did not drink raw bison cum with my marinated ox testicles.
>**C)** Therefore, I drank organic sheep milk with my marinated ox testicles.

**A)** Modus Ponens
**B)** Modus Tollens
**C)** Hypothetical Syllogism
**D)** Disjunctive Syllogism ✓

##### Structure

**P1:** M∨C
**P2:** ¬C
**C:** M

### Inference 8

>**P1)** If I eat too much processed food, then I will consume too much salt.
>**P2)** If I consume too much salt, then my heart will explode.
>**C)** Therefore, if I eat too much processed food, then my heart will explode.

**A)** Modus Ponens
**B)** Modus Tollens
**C)** Hypothetical Syllogism ✓
**D)** Disjunctive Syllogism

##### Structure

**P1:** P→S
**P2:** S→E
**C:** P→E

### Inference 9

>**P1)** I will either eat a salad or a soup for lunch.
>**P2)** I did not eat a salad for lunch.
>**C)** Therefore, I ate soup for lunch.

**A)** Modus Ponens
**B)** Modus Tollens
**C)** Hypothetical Syllogism
**D)** Disjunctive Syllogism ✓

##### Structure

**P1:** S∨X
**P2:** ¬S
**C:** X

### Inference 10

>**P1)** If I eat dingo liver, then I will get important n00trients.
>**P2)** I ate dingo liver.
>**C)** Therefore, I got important n00trients.

**A)** Modus Ponens ✓
**B)** Modus Tollens
**C)** Hypothetical Syllogism
**D)** Disjunctive Syllogism

##### Structure

**P1:** P→Q
**P2:** P
**C:** Q

---

# Hashtags

#debate 
#debate_coursework 
#homework  