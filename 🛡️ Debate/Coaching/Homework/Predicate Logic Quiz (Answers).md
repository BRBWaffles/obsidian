### Inference 1

>**P1)** If one becomes a lean mass hyper responder, then one has unlocked the fountain of youth.
>**P2)** Dave Feldman became a lean mass hyper responder.
>**C)** Therefore, Dave Feldman has unlocked the fountain of youth.

**P1)** ∀x(Lx→Fx)
**P2)** Ld
**C)** Fd

### Inference 2

>**P1)** If one (x) eats red meat (y), then one (x) has increased their risk of cancer (z).
>**P2)** Shawn Baker eats beef.
>**C)** Therefore, Shawn Baker has increased their risk of colon cancer.

**P1)** ∀x∀y∀z(Mxy→Cxz)
**P2)** Msb
**C)** Cso

### Inference 3

>**P1)** If fish (x) contain heavy metals (y), then eating too much fish (x) can give you heavy metal (y) poisoning.
>**P2)** Salmon contains mercury.
>**C)** Therefore, eating too much salmon can give you mercury poisoning.

**P1)** ∀x∀y(Cxy→Pxy)
**P2)** Csm
**C)** Psm

### Inference 4

>**P1)** Some people like to eat kangaroo balls.
>**P2)** Everyone who likes to eat kangaroo balls also likes to eat buffalo balls.
>**C)** Therefore, there are some people who like both kangaroo balls and buffalo balls. 

**P1)** ∃x(Px)
**P2)** ∀x(Px→Qx)
**C)** ∃x(Px∧Qx)

### Inference 5

>**P1)** If the vegetables are food, then the vegetables do not have toxins.
>**P2)** Broccoli has toxins.
>**C)** Therefore, broccoli is not food.  

**P1)** ∀x(Fx→¬Tx)
**P2)** ∃x(Tb)
**C)** ∃x(¬Fb)

### Inference 6

>**P1)** If one will live forever, then one is indeed a carnivore.
>**P2)** Micheal Greger is not a carnivore.
>**C)** Therefore, Michael Greger will not live forever. 

**P1)** ∀x(Fx→Cx)
**P2)** ¬Cm
**C)** ¬Fm

### Inference 7

>**P1)** If a food is not healthy, then the data shows that the food increases the risk of disease.
>**P2)** RCTs show that seed oils do not increase the risk of disease.
>**C)** Therefore, seed oils are healthy.

**P1)** ∀x∀y(¬Hx→Dxy)
**P2)** ¬Dsr
**C)** Hs

---

# Hashtags

#debate 
#debate_coursework 
#homework 