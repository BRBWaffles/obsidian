<div style="page-break-after: always;">

#### Fallacy 1

>"Until you show me a 50-year randomized controlled trial showing otherwise, I will continue to believe that steak is healthy."

**A)** red herring
**B)** appeal to ignorance
**C)** equivocation
**D)** appeal to nature
**E)** both B and D

#### Fallacy 2

>"You can tell me all you want that Liver King is on steroids, it just sounds ridiculous to me and I refuse to believe it."

**A)** equivocation
**B)** muddying the waters
**C)** genetic fallacy
**D)** appeal from incredulity
**E)** both D and C

#### Fallacy 3

>"We know that industrial frankenfoods made in factories are bad for you, because they're not foods that you grow yourself from the soil."

**A)** appeal to nature
**B)** strawman
**C)** begging the question
**D)** appeal to authority
**E)** both A and C

</div>
<div style="page-break-after: always;">

#### Fallacy 4

>"Why would you ever believe that vegan diets were unhealthy? The Academy of Nutrition and Dietetics says they're appropriate for all stages of the life cycle."

**A)** genetic fallacy
**B)** appeal to authority
**C)** muddying the waters
**D)** red herring
**E)** both B and D

#### Fallacy 5

>"When I said that the carnivore diet cures fatty liver, what I actually meant was that it can be an effective weight loss diet for some people."

**A)** strawman
**B)** begging the question
**C)** motte and bailey
**D)** equivocation
**E)** both A and B

</div>

---

# Hashtags

#debate 
#debate_coursework 