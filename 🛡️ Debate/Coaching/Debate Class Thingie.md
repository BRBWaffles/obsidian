# What is a Debate?

What is a debate? Ultimately, a debate is a type of adversarial conversation with the aim of establishing the justification, or lack thereof, for a particular doxastic state toward a proposition. The exploration of the justification for the debate proposition has at least two phases.

## Phases

### Phase 1: Clarification

During the clarification phase, interlocutors will elaborate on any language they're using that isn't understood. 

### Phase 2: Argumentation

During the argumentation phase, at least one party will deploy at least one argument for or against the proposition, and those arguments become the either the focus of attack or the catalyst of acceptance.

### Phase 3: Steamrolling

The steamrolling phase is one optional, additional phase that may be necessary to deploy during debate. If the debate has reached a point where it is obvious that one party should concede, the interlocutor with the upper hand may consistently press them for a concession until the interaction terminates. At this point, the discussion may only terminate if the evasive interlocutor either concedes or leaves in shame.


## Terminology

### Propositions

A proposition is simply a "truth-apt" statement. For a statement to be truth-apt just means that it can be either true or false. This is just to say that the statement has a truth value that can be assigned to it. Propositions are the basic components of arguments in propositional/classical logic.

>**Example:** "It is raining outside."


### Arguments

For our purposes, an argument is a set of premises (or even a single premise, like in the case of tautologies) followed by a conclusion, where the premises and conclusions are comprised of propositions.

>**Example:** If it is raining outside, then the ground is wet. It's raining outside. Therefore, the ground is wet.

### Defeaters

Simply speaking, defeaters are types of responses that either significantly, or entirely, deflate the persuasive force of an argument and/or the truth value of a proposition. There are three types of defeaters:

>**Rebutting Defeaters:** An argument that directly negates a proposition and/or renders an argument unsound.

>**Undercutting Defeaters:** An argument that lowers the probability of a proposition being true and/or casts serious doubt on the soundness of an argument.

>**No-Reasons Defeaters:** An argument that demonstrates that there is no reason to believe that a proposition is true and/or an argument is sound.


## Linearization Algorithm

Essentially, an optimized debate strategy will typically adhere to something like the following algorithm:

![[Pasted image 20241007185058.png]]