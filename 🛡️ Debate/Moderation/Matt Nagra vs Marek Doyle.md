## Proposition

> "You can't infer cause and effect from epidemiology"


## Notes

Marek's goalpost:
- sub-stratification
- departing from usual reporting systems (e.g., averages)

Marek's criteria for causal inference:
- **Consistency**: "epi on red meat and cvd is consistent above >100g/day."
- **Strength of association**: "the SoA is comparable between red meat and light smoking."
- **Specificity**: ""

---

# Hashtags

#debate 
#debate_moderation
