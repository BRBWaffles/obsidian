
[14:00](https://youtu.be/ZemkG6Vj7hc?si=IFPCyFczzwPP3cgk&t=840)

It would have been nice to highlight why causal inferences from Gundry's cited data would be dubious. He cites data is cross-sectional in natural, which lacks any evaluation of temporality. I also lacks individual-level follow-up and makes it susceptible to ecological fallacies.

[15:00](https://youtu.be/ZemkG6Vj7hc?si=0wC9799emR0MnxQt&t=900)

You seem to suggest that blue zones could give us an indication of the compatibility between different dietary patterns and longevity, but I don't even think that they tell us that much because of the cross-sectional nature and the susceptibility to bias and confounding. People could generally be eating a particular diet in the region and have markedly improved average longevity, but without individual-level measurements, it could be the case that the ones living the longest are the ones who are, on average, deviating from local dietary norms.

[15:20](https://youtu.be/ZemkG6Vj7hc?si=BKmIoCzyLgEQ08LG&t=920)

Okay, yeah, you alluded to it. 👍

[16:00](https://youtu.be/ZemkG6Vj7hc?si=hNH_RABHWBKklNDy&t=960)

Something to bring up here would be the fact that the prospective cohort studies that underpin our understanding of the relationship between smoking and poor health outcomes is routinely adjusted for both vitamin C and diet quality. So, the literature tends to produce results that are not expected on Gundry's hypothesis.

[18:30](https://youtu.be/ZemkG6Vj7hc?si=hRHT6ML5s8tB7d4j&t=1110)

Gundry literally confuses nicotine and vitamin B3, haha. Bruh.

[19:30](https://youtu.be/ZemkG6Vj7hc?si=HtBbugN4FXlB75wf&t=1170)

It would have been nice to dunk on him about not understanding the difference between nicotine and B3. Even if you had to pull out your phone and look up the molecular structures to show they're entirely different things and have no relation to one another, haha. Those dunk points would be 100% worth it. Then force a concession that nicotine and B3 are not related. That would have been beautiful.

[21:30](https://youtu.be/ZemkG6Vj7hc?si=Bs2Cfcz9CTIFaRbU&t=1290)

This was a really well articulated accounting of the literature on LPa and its relationship to outcomes. It shouldn't be taken for granted that anything that lowers LPa will be favourable as an intervention (such as liver disease).

[24:10](https://youtu.be/ZemkG6Vj7hc?si=3nIZY_p-PHpp9Fv9&t=1450)

The guidelines do not recommend to lower ApoB to lower oxPL levels, lol. You should have pressed him to substantiate this claim. OxPL and oxLDL don't have any clinical utility that I'm aware of, and the literature investigating them as independent risk factors hasn't been very fruitful for the hypothesis that they drive risk over and above ApoB. I dunno where he's getting this stuff, but he should have been pressed hard on this point.

Beyond appealing to the guidelines, it would be helpful to explain why lowering ApoB is the gold standard recommendation for primary and secondary CVD prevention. It's merely because no other markers tend to survive adjustment and/or control for ApoB when analyzed, and the ones that do already have different treatment modality (such as beta blockers for hypertension, GLP1 for obesity, etc). We don't have a good justification for for making oxPL or oxLDL a target for therapy.

[25:05](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=1505)

You provided a question-begging argument here. An argument that assumes the conclusion in its premises; investigations into oxPL won't change guidelines because management is going to be lowering ApoB. This could be avoided by merely elaborating on why oxPL is not currently a target for therapy.

[26:30](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=1590)

Mike asks a very good question, and Gundry dodges and proceeds to ramble. It would have been appropriate to cut him off and directly ask him how he's deriving generalizable conclusions based on his mechanistic reasoning. If his response is "I see patients tho", simply direct him to a picture of the evidence hierarchy and ask him why recommendations should be guided by the lowest possible rung.

[27:30](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=1650)

Gundry dodged another direct question about oxPL and heart attacks. When pressed again, he started rambling about beta blockers. It would have been good to press him for an answer. The intention of this discussion is to confront Gundry on his views and see how robust his views are to scrutiny. He should be providing answers to every question he's asked, but he doesn't. Redirecting him back onto a linear path is crucial. Sophists will try to side-step you. You have to keep them linear and force answers when necessary.

[29:35](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=1775)

Good good, you explained by ApoB trumps his pet biomarkers in terms of epistemic virtuousness. It's good that you did, but it should have been done at 24:10. 👍

[31:30](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=1890)

This motherfucker starts yacking about leaky gut. It would have been nice to just stop him and ask him for the ICD-10/11 code for leaky gut. When he can't produce one, ask him why he's trying to diagnose and treat a disease that doesn't seem to exist.

[35:50](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=2150)

Gundry criticizes your epistemology as "incomplete". You ask him "how so?". But, it would have been better to ask him what he means by "incomplete", because it'll probably cash out into something trivial that you might not even disagree with, but won't really succeed as a criticism against you. For example, if by incomplete he actually means you're not in possession of all of the facts or whatever, that's a criticism that will be true of all scientific and epistemic modalities. So who cares?

Scrutinizing your opponents semantics when they say something bizarre or vague like "incomplete" can be a great way to demonstrate to the audience that they're out to lunch and/or gibberating. Semantic analysis is super important.

[36:25](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=2185)

Again with this leaky gut shit. He needs to give the ICD-10/11 code or GTFO.

[37:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=2220)

LMAO. You both asked him for a symmetry breaker between his anecdotes and other doctors anecdotes, despite mutually incompatible recommendations and great patient results, and he basically responds with a non-answer; "I treat their failures and they treat my failures". He should have been dicked into the dirt here. He should have been pressed mercilessly for that symmetry breaker; what justifies sooner believing Gundry's anecdotes over some other quacks's anecdotes.

[37:50](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=2270)

Would have been nice to point out that the Lyon Diet Heart Study also replaced meat with bread, lol. That would have ground his gears.

It's a good study, but I'd be less confident in the results because the confidence intervals for RR were a barn door, unfortunately. The results are compatible with virtually abolishing CVD and also compatible with barely making a dent. It's really hard to draw firm conclusions from it, despite the seemingly impressive results. It think you make a good point, though. Regardless of what I just said there, the results aren't expected on Gundry's hypothesis.

[40:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=2400)

Wtf is this clown talking about? Antioxidants weren't included in the model for the final analysis in the LDHS. This mf is huffing whippits. He's just fucking hallucinating before our eyes.

![[Pasted image 20240711165008.png]]

[42:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=2520)

He's back on this leaky gut shit again. He needs to be pressed. ICD-10/11 codes, my dude.

[48:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=2880)

It's clear the discussion has gone completely off the rails because both Mike and yourself are allowing him to steer you down rabbitholes. It's probably no longer clear to the audience what the topic of discussion even is anymore. The convo desperately needs to be steered toward a path of linearity. He's made a number of wacky claims, but ultimately he's presumably there to defend his position that lectins causes leaky gut which leads to auto-immunity, which leads to all disease.

His position can be formalized as such:

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If one consumes lectins, then one is at risk of developing leaky gut.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> If one is at risk of developing leaky gut, then one is at risk of developing auto-immunity.
<br />
<font color="CC6600">
<b>(Q→R)</b>
<br />
<b>P3)</b></font> If one is at risk of developing auto-immunity, then one is at risk of developing all known diseases.
<br />
<font color="CC6600">
<b>(R→S)</b>
<br />
<b>C)</b></font> Therefore, if one consumes lectins, then one is at risk of developing all known diseases.
<br />
<font color="CC6600">
<b>(∴P→S)</b>
<br />
<br />
</font>
</div>

He needs to provide an argument for each one of these premises, as they all seem to be questionable at best and stone cold fucking insane at worst.

[50:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=3000)

You do give a really good response to his microbiome fuckery, which is good tho.

[52:30](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=3150)

At this point I wouldn't even touch the glyphosate claim. All it does is invite an opportunity for him to whisk you both down on another tangent. The trajectory of conversation should be corrected and placed back on a linear path. He needs to provide sound arguments for his position, and addressing his claims on glyphosate is already granting him too much. The response you gave to the point was good though. I just personally wouldn't have let the conversation get that far off track.

[54:30](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=3270)

"No juicers in central park zoos tho"

Is this asshat for real? Holy fuck, lmao.

[56:15](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=3375)

You curbstomped him here, haha. Glorious. At least we're sorta getting back on track and talking about dietary exposures and risk. But we need to steer back to the center of the bullseye and discuss lectins and whatnot.

[56:40](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=3400)

You may have misspoke here, but humans are taxonomically classified as apes. I think maybe what you meant was that humans aren't other apes, like gorillas, chimps, etc. 

[59:20](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=3560)

I'm pretty sure the lectin content of just run of the mill canned beans is essentially zero. Given that nobody eats raw beans, it's not even clear why we should care about Gundry's hypothesis. He can't even demonstrate that the means by which beans are ubiquitously consumed actually has a level of lectins that would give us any reason to believe his hypothesis is credible in the first place. If nobody is eating lectins from beans, how in the fuck are those lectins contributing to disease?

[1:02:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=3720)

You smacked him pretty good here on the seasonality point, haha.

[1:13:20](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=4400)

It's good that you eventually brought the conversation back to the question Gundry was asked at 37:00 that he dodged twice. I feel like this is a good step toward course-correcting the discussion.

[1:15:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=4500)

I think this discussion about hunter-gatherer populations could benefit from a discussion about antagonistic pleiotropy. It's the best way to killscreen all appeals to evolutionary history, pretty much. There's no good answer they can provide for it other than appealing from incredulity.

[1:18:10](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=4690)

Gundry is really starting to crumble here. After enough scrutiny we have finally distilled his position down to "I think fructose is a problem". It's like, okay, Gundry, we don't disagree that you believe that, and you're free to go take your pet theories and play with your prick in the corner with them. But when it comes to the human outcome data, there's nothing he's been able to cite to back up the seasonality claim, or the hybirdization claim, etc. It's all just speculation, and I think you both just eventually wore him down to the point where it was revealed that the claim was actually making was just some trivial horseshit.

[1:20:20](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=4820)

Great job immediately smacking his anecdotal bullshit down, haha.

[1:22:20](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=4940)

Holy fuck, at least we're back on a lectin-adjacent topic, I guess. But he just made a number of rapid-fire, bullshit claims one after the other, and if it wasn't as tangent, each of them would need to be scrutinized to completion. This guy is such con artist.

[1:23:10](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=4990)

This should have been the turning point of the entire debate. He admits that his entire position is an unproven theory. From here, he needs to be pressed HARD on why he thinks unproven theories should be thought to have clinical utility. He needs to be pressed HARD on why he thinks recommendations should be based on unproven theories. This would have been the best opportunity in the entire debate so far to steamroll for a concession that his position is essentially a nothing-burger.

Instead, this opportunity was overlooked and a superfluous line of questioning was resumed immediately after Gundry gave this answer. This is definitely a mistake. Gundry's toes are blistering against the hot coals at this point, and he admitted something devastating to his position. He was toast in this debate at that moment, but it was overlooked.

[1:25:10](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=5110)

Good to point out the selection bias for his claim there, haha.

[1:29:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=5340)

Gundry basically admits here that the selection bias point that you made, that he previously disagreed with, is actually true.

[1:31:40](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=5500)

He's insane.

[1:34:30](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=5670)

That coke-twinkie story was a really good way to highlight the folly of Gundry's approach.

[1:38:30](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=5910)

This mf is allergic to answering questions directly, hahaha

[1:42:00](https://youtu.be/ZemkG6Vj7hc?si=hv74MzmRybpA2C7u&t=6120)

This is more of a criticism of Mike, but ending with a sematic critique is weak. There were so many good opportunities to just body this clown, and after all that we end with a semantic critique. It's just such a shame.

---

# Hashtags

#debate 
#clowns 
#clownery 

