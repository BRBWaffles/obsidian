### Summary

> The argument is structured to demonstrate that, within the context of a flat Earth model, if the stars are perceived as rotating counter-clockwise from the center, they cannot also be perceived as rotating clockwise from any point on the Earth.

### Flat Earth Internal Critique

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                  |
|:-------------------------------------------:|:---------------------------------------------------------------------------------------------------------- |
|   <font color="CC6600">**P(x,y)**</font>    | (x) is perceived as rotating counter-clockwise from point (y)                                              |
|   <font color="CC6600">**Q(y,z)**</font>    | (y) is on the same side of the rotational plane as (z)                                                    | 
|   <font color="CC6600">**R(x,z)**</font>    | (x) will be perceived as rotating counter-clockwise from (z), as long as you are looking towards the plane |
|   <font color="CC6600">**S(x,z)**</font>    | (x) will be perceived as rotating clockwise from (z), as long as you are looking towards the plane         |
|      <font color="CC6600">**x**</font>      | an object                                                                                                  |
|      <font color="CC6600">**y**</font>      | vantage point A                                                                                            |
|      <font color="CC6600">**z**</font>      | vantage points B                                                                                           |
|      <font color="CC6600">**k**</font>      | stars in the sky                                                                                           |
|      <font color="CC6600">**t**</font>      | the center of the flat Earth                                                                               |
|      <font color="CC6600">**a**</font>      | anywhere else on the flat Earth                                                                                 |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If an object (x) is perceived as rotating counter-clockwise from vantage point A (y), then, for all vantage points B (z), if vantage point A (y) is on the same side of the rotational plane as vantage points B (z), then the object (x) will be perceived as rotating counter-clockwise from vantage points B (z), as long as you are looking towards the rotational plane.
<br />
<font color="CC6600">
<b>(∀x∀y∀z(Pxy→(Qyz→Rxz)))</b>
<br />
<b>P2)</b></font> The stars in the sky are perceived as rotating counter-clockwise from the center of the flat Earth and the center of the flat Earth is on the same side of the rotational plane as anywhere else on the flat Earth.
<br />
<font color="CC6600">
<b>(Pkt∧Qta)</b>
<br />
<b>P3)</b></font> If the stars in the sky are perceived as rotating counter-clockwise from anywhere else on the flat Earth, as long as you are looking towards the rotational plane, then the stars in the sky will not be perceived as rotating clockwise from anywhere else on the flat Earth, as long as you are looking towards the rotational plane.
<br />
<font color="CC6600">
<b>(Rka→¬Ska)</b>
<br />
<b>C)</b></font> Therefore, the stars in the sky will not be perceived as rotating clockwise from anywhere else on the flat Earth, as long as you are looking towards the rotational plane.
<br />
<font color="CC6600">
<b>(∴¬Ska)</b>
<br />
<br />
</font>
</div>

---

# Hashtags

#debate 
#arguments 
#flat_earth