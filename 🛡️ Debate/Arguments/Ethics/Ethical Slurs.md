### Argument for Using the Term Retard

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                              |
|:-------------------------------------------:|:-------------------------------------------------------------------------------------- |
|      <font color="CC6600">**C**</font>      | (x) slur's negative connotations have been neutralised                                 |
|      <font color="CC6600">**B**</font>      | (x) slur has been rendered non-bigoted via altered usage                               |
|      <font color="CC6600">**D**</font>      | oppressed people will continue to suffer from the use of (x) slur                      |
|      <font color="CC6600">**S**</font>      | it is permissible to neutralise the term retard's negative connotations                |
|      <font color="CC6600">**A**</font>      | it is generally permissible to use the term retard with an altered non-bigoted meaning | 
|      <font color="CC6600">**r**</font>      | retard                                                                                 |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all slurs, (x) slur's negative connotations have been neutralised if and only if, (x) slur has been rendered non-bigoted via altered usage.
<br />
<font color="CC6600">
<b>(∀x(Cx↔Bx))</b>
<br />
<b>P2)</b></font> For all slurs, if it is not the case that (x) slur's negative connotations have been neutralised, then oppressed people will continue to suffer from the use of (x) slur.
<br />
<font color="CC6600">
<b>(∀x(¬Cx→Dx))</b>
<br />
<b>P3)</b></font> It is not the case that the term retard's negative connotations have been neutralised.
<br />
<font color="CC6600">
<b>(¬Cr)</b>
<br />
<b>P4)</b></font> If the term retard has not been rendered non-bigoted via altered usage and oppressed people will continue to suffer from the use of the term retard, then it is permissible to neutralise the term retard's negative connotations.
<br />
<font color="CC6600">
<b>(¬Br∧Dr→Sr)</b>
<br />
<b>P5)</b></font> If it is permissible to neutralise the term retard's negative connotations, then It is generally permissible to use the term retard with an altered non-bigoted meaning.
<br />
<font color="CC6600">
<b>(Sr→Ar)</b>
<br />
<b>C)</b></font> Therefore, it is generally permissible to use the term retard with an altered non-bigoted meaning.
<br />
<font color="CC6600">
<b>(∴Ar)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Cx~4Bx)),(~6x(~3Cx~5Dx)),(~3Cr),(~3Br~1Dr~5Sr),(Sr~5Ar)|=(Ar))

---

### Argument for Using the Term Nigga

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                              |
|:-------------------------------------------:|:-------------------------------------------------------------------------------------- |
|      <font color="CC6600">**C**</font>      | (x) slur's negative connotations have been neutralised                                 |
|      <font color="CC6600">**B**</font>      | (x) slur has been rendered non-bigoted via altered usage                               |
|      <font color="CC6600">**D**</font>      | oppressed people will continue to suffer from the use of (x) slur                      |
|      <font color="CC6600">**S**</font>      | it is permissible to neutralise the term nigga's negative connotations                |
|      <font color="CC6600">**A**</font>      | it is generally permissible to use the term nigga with an altered non-bigoted meaning | 
|      <font color="CC6600">**n**</font>      | nigga                                                                                 |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all slurs, (x) slur's negative connotations have been neutralised if and only if, (x) slur has been rendered non-bigoted via altered usage.
<br />
<font color="CC6600">
<b>(∀x(Cx↔Bx))</b>
<br />
<b>P2)</b></font> For all slurs, If it is not the case that (x) slur's negative connotations have been neutralised, then oppressed people will continue to suffer from the use of (x) slur.
<br />
<font color="CC6600">
<b>(∀x(¬Cx→Dx))</b>
<br />
<b>P3)</b></font> It is not the case that the term nigga's negative connotations have been neutralised.
<br />
<font color="CC6600">
<b>(¬Cn)</b>
<br />
<b>P4)</b></font> If the term nigga has not been rendered non-bigoted via altered usage and oppressed people will continue to suffer from the use of the term nigga, then it is permissible to neutralise the term nigga's negative connotations.
<br />
<font color="CC6600">
<b>(¬Bn∧Dn→Sn)</b>
<br />
<b>P5)</b></font> If it is permissible to neutralise the term nigga's negative connotations, then It is generally permissible to use the term nigga with an altered non-bigoted meaning.
<br />
<font color="CC6600">
<b>(Sn→An)</b>
<br />
<b>C)</b></font> Therefore, it is generally permissible to use the term nigga with an altered non-bigoted meaning.
<br />
<font color="CC6600">
<b>(∴An)</b>
<br />
<br />
</font>
</div>

### Argument for Using the Term Bitch

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                       |
|:-------------------------------------------:|:------------------------------------------------------------------------------- |
|      <font color="CC6600">**C**</font>      | (x) slur's negative connotations have been neutralised                          |
|      <font color="CC6600">**B**</font>      | (x) slur has been rendered non-bigoted via altered usage                        |
|      <font color="CC6600">**D**</font>      | oppressed people will continue to suffer from the use of (x) slur               |
|      <font color="CC6600">**S**</font>      | it is permissible to neutralise (x) slur's negative connotations                |
|      <font color="CC6600">**A**</font>      | it is generally permissible to use (x) slur with an altered non-bigoted meaning |
|      <font color="CC6600">**b**</font>      | bitch                                                                           |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all slurs, (x) slur's negative connotations have been neutralised if and only if, (x) slur has been rendered non-bigoted via altered usage.
<br />
<font color="CC6600">
<b>(∀x(Cx↔Bx))</b>
<br />
<b>P2)</b></font> For all slurs, If it is not the case that (x) slur's negative connotations have been neutralised, then oppressed people will continue to suffer from the use of (x) slur.
<br />
<font color="CC6600">
<b>(∀x(¬Cx→Dx))</b>
<br />
<b>P3)</b></font> It is not the case that the term bitch's negative connotations have been neutralised.
<br />
<font color="CC6600">
<b>(¬Cb)</b>
<br />
<b>P4)</b></font> If the term bitch has not been rendered non-bigoted via altered usage and oppressed people will continue to suffer from the use of the term bitch, then it is permissible to neutralise the term bitch's negative connotations.
<br />
<font color="CC6600">
<b>(¬Bb∧Db→Sb)</b>
<br />
<b>P5)</b></font> If it is permissible to neutralise the term bitch's negative connotations, then It is generally permissible to use the term bitch with an altered non-bigoted meaning.
<br />
<font color="CC6600">
<b>(Sb→Ab)</b>
<br />
<b>C)</b></font> Therefore, it is generally permissible to use the term bitch with an altered non-bigoted meaning.
<br />
<font color="CC6600">
<b>(∴Ab)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Cx~4Bx)),(~6x(~3Cx~5Dx)),(~3Cn),(~3Bn~1Dn~5Sn),(Sn~5An)|=(An))

---

# Hashtags

#debate 
#arguments 
#slur
#retard
#linguistic_prescriptivism 