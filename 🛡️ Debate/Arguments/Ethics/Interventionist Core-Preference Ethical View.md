# Interventionist Core-Preference Ethical View

> This ethical framework posits that moral judgments arise from objectively measurable brain states shaped by an individual's core preferences. It asserts that when someone claims "X is morally wrong," they mean "X violates my core preferences, triggering specific neurobiological responses in my brain that motivate me to intervene.