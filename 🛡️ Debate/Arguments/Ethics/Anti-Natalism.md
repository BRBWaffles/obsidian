## The Eternal Intergalactic Sentience Patrol Squad

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If humans abstaining from procreation maximally reduces rights violations, then humans have sterilized all sentient life in the universe.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Humans have not sterilized all sentient life in the universe.
<br />
<font color="CC6600">
<b>(¬Q)</b>
<br />
<b>P3)</b></font> If humans abstaining from procreation does not maximally reduce rights violations and many more generations are required to sterilize all sentient life in the universe, then humans should not abstain from procreation until all sentient life in the universe is sterilized.
<br />
<font color="CC6600">
<b>(¬P∧R→¬S)</b>
<br />
<b>P4)</b></font> Many more generations are required to sterilize all sentient life in the universe.
<br />
<font color="CC6600">
<b>(R)</b>
<br />
<b>C)</b></font> Therefore, humans should not abstain from procreation until all sentient life in the universe is sterilized.
<br />
<font color="CC6600">
<b>(∴¬S)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(~3Q),(~3P~1R~5~3S),(R)|=(~3S))

---

# Hashtags

#debate 
#arguments 
#anti-natalism