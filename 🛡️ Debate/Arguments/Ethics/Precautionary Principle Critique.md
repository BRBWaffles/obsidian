The precautionary principle is a group of rules that function similarly. Any weak version just states permissibility and any strong version states requirement. But these rules can be formulated in different ways:

> 1) if I don't know the harm of action x, then I ought not to do action x.
> 2) if I don't know the consequences of action x, then I ought not to do action x.
> 3) if I have a doxastic leaning that action x has more undesirable than desirable consequences then I ought not to do action x.

But most people who use it seem to only ever cite something similar to the first example. In that case, you can just ask why they don't adhere to the opposing rule:

> if I don't know the harms of not doing action x, then I ought to do action x.

Applying this to the last example:

> if I have a doxastic leaning that not doing action x has more undesirable than desirable consequences, then I ought to do action x.

This doesn't run into the same issue: the knowledge component is turned from agnostic to affirmative, and the symmetry breaker is on the table.

Technically, it still needs the clarification that:

> if the ratio of desirable vs undesirable consequences is more favorable towards doing action x than it is disfavorable towards not doing action x, then I ought to do action x.

All this makes me think the precautionary principle is some kind vestige of unacknowledged consequentialist intuitions, by the way. Imagine setting undesirable consequences to violations of rights and desirable consequences to affirmations of rights. In the bivalve case, this would straight lead to arguing their rights, so why not do that in the first place instead of invoking precaution?

---

# Hashtags

#debate 
#arguments 
#philosophy 