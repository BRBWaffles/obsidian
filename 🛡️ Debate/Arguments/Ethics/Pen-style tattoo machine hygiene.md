I discussed the use of pen-style machines with several healthcare professionals. Their unanimous opinion was that for such a device to be used safely in a setting such as an operating theatre, one of the following four protocols must be followed:

**1)** The entire machine should be designed to be single-use disposable.

**OR**

**2)** Whenever a cartridge is removed, it should be immediately discarded. New cartridges must be inserted with extreme care to prevent contaminating the plunger bar. Failing to adhere to this protocol warrants disposal of the machine.

**OR**

**3)** The drive system, along with its housing, must be easily accessible and designed for either autoclaving or thorough sanitization using high-level disinfectants.

**OR**

**4)** The drive system should be isolated from the cartridge by a sterile barrier, which would be removed and discarded after each use.

However, it appears that most pen-style machines do not align with universal precautions and established health and safety standards. The first option is often deemed unfeasible and is pretty much never practiced. The second option also faces similar impracticality. The third option is applicable to only a limited number of machines. As for the fourth option, I am not aware of its implementation anywhere (other than maybe GGTS's Good Pen).

My review of the CDC's outline on Spaulding's classification system makes it clear that the CDC would likely concur with this assessment. Consequently, it seems that a worryingly large number of pen-style machines are unsuitable for use, unfortunately.

EDIT:

I'm extremely saddened by the post-hoc rationalizations of some of these users. When presented with a sound argument for why certain tattoo equipment is an infectious disease transmission hazard, the most common response has been "tattooing isn't sterile anyway". As if this is supposed to be convincing or profound, or put any client's mind at ease about the safety of the process.

Think about what you're saying. You're essentially saying that because tattooing "isn't sterile", tattoo artists should be free to not work aseptically if they choose. We all have a choice to not use dangerous equipment. What I'm suggesting isn't career-ending for any of us. It's just a minor inconvenience. It's extremely disheartening how many people elect to put others at risk unnecessarily because they personally don't want to be inconvenienced. It's shameful, and we have to do better.

---

# Hashtags

#debate 
#arguments 