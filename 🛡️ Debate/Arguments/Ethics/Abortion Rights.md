### Abortion Rights

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If one consents to becoming pregnant or one consensually engages in sexual activity without contraception, then one is implicitly committed to at least accepting the average risks for the average pregnancy.
<br />
<font color="CC6600">
<b>(P∨Q→R)</b>
<br />
<b>P2)</b></font> One consents to becoming pregnant.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> One consensually engages in sexual activity without contraception.
<br />
<font color="CC6600">
<b>(Q)</b>
<br />
<b>P4)</b></font> If one is implicitly committed to at least accepting the average risks for the average pregnancy and one's pregnancy persists long enough for fetal sentience to develop and one's risk profile during pregnancy is not high, then one is morally bound to carrying the pregnancy to term.
<br />
<font color="CC6600">
<b>(R∧F∧¬H→M)</b>
<br />
<b>P5)</b></font> One's pregnancy persists long enough for fetal sentience to develop.
<br />
<font color="CC6600">
<b>(F)</b>
<br />
<b>P6)</b></font> One's risk profile during pregnancy is not high.
<br />
<font color="CC6600">
<b>(¬H)</b>
<br />
<b>P7)</b></font> If one is morally bound to carrying the pregnancy to term, then one's whims are not a sufficient justification for the termination of sentient human life.
<br />
<font color="CC6600">
<b>(M→¬W)</b>
<br />
<b>C)</b></font> Therefore, one's whims are not a sufficient justification for the termination of sentient human life.
<br />
<font color="CC6600">
<b>(∴¬W)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~2Q~5R),(P),(Q),((R~1F~1~3H)~5M),(F),(~3H),(M~5~3W)|=(~3W))

![[📂 Media/Images/Pasted image 20230607150451.png]]

# Hastags

#debate 
#abortion
#philosophy 
#arguments 