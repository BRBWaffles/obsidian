| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                                                                 |
|:-------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|    <font color="CC6600">**Male**</font>     | the cluster of traits that associate with small, but not large, gametes.                                                                                                  |
|   <font color="CC6600">**Female**</font>    | the cluster of traits that associate with large, but not small, gametes.                                                                                                  |
|      <font color="CC6600">**W**</font>      | someone (x) is a woman                                                                                                                                                    |
|      <font color="CC6600">**M**</font>      | someone (x) is a man                                                                                                                                                      |
|      <font color="CC6600">**L**</font>      | someone (x) possesses a sufficiently greater sum of weighted traits that more closely associate with large gametes than weighted traits that associate with small gametes |
|      <font color="CC6600">**S**</font>      | someone (x) possesses a sufficiently greater sum of weighted traits that more closely associate with small gametes than weighted traits that associate with large gametes |
|      <font color="CC6600">**O**</font>      | someone (x) is over 18 years of age                                                                                                                                       |
|      <font color="CC6600">**a**</font>      | a trans person of the male sex                                                                                                                                      |
|      <font color="CC6600">**g**</font>      | a trans person of the female sex                                                                                                                                        |


<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Someone is a woman if, and only if, someone possesses a sufficiently greater sum of weighted traits that more closely associate with large gametes than weighted traits that associate with small gametes and someone is over 18 years of age.
<br />
<font color="CC6600">
<b>(∀x(Wx↔Lx∧Ox))</b>
<br />
<b>P2)</b></font> Someone is a man if, and only if, someone possesses a sufficiently greater sum of weighted traits that more closely associate with small gametes than weighted traits that associate with large gametes and someone is over 18 years of age.
<br />
<font color="CC6600">
<b>(∀x(Mx↔Sx∧Ox))</b>
<br />
<b>P3)</b></font> A trans person of the male sex possesses a sufficiently greater sum of weighted traits that more closely associate with large gametes than weighted traits that associate with small gametes and a trans person with small gametes is over 18 years of age.
<br />
<font color="CC6600">
<b>(La∧Oa)</b>
<br />
<b>P4)</b></font> A trans person of the female sex possesses a sufficiently greater sum of weighted traits that more closely associate with small gametes than weighted traits that associate with large gametes and a trans person with large gametes is over 18 years of age.
<br />
<font color="CC6600">
<b>(Sg∧Og)</b>
<br />
<b>C)</b></font> Therefore, a trans person of the male sex is a woman and a trans person with female sex is a man.
<br />
<font color="CC6600">
<b>(∴Wa∧Mg)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Wx~4Lx~1Ox)),(~6x(Mx~4Sx~1Ox)),(La~1Oa),(Sg~1Og)|=(Wa~1Mg))

---

### Maz' Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                                                                 |
|:-------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**W**</font>      | Someone (x) is a woman                                                                                                                                                    |
|      <font color="CC6600">**M**</font>      | Someone (x) is a man                                                                                                                                                      |
|      <font color="CC6600">**B**</font>      | Someone (x) is a trans person with female sex                                                                                                                             |
|      <font color="CC6600">**A**</font>      | Someone (x) is a trans person with male sex                                                                                                                               |
|      <font color="CC6600">**L**</font>      | Someone (x) possesses a sufficiently greater sum of weighted traits that more closely associate with large gametes than weighted traits that associate with small gametes |
|      <font color="CC6600">**S**</font>      | Someone (x) possesses a sufficiently greater sum of weighted traits that more closely associate with small gametes than weighted traits that associate with large gametes | 
|      <font color="CC6600">**O**</font>      | Someone (x) is over 18 years of age                                                                                                                                       |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Someone is a woman if, and only if, someone possesses a sufficiently greater sum of weighted traits that more closely associate with large gametes than weighted traits that associate with small gametes **and** is over 18 years of age.
<br />
<font color="CC6600">
<b>(∀x(Wx↔(Lx∧Ox)))</b>
<br />
<b>P2)</b></font> Someone is a man if, and only if, someone possesses a sufficiently greater sum of weighted traits that more closely associate with small gametes than weighted traits that associate with large gametes and is over 18 years of age.
<br />
<font color="CC6600">
<b>(∀x(Mx↔(Sx∧Ox)))</b>
<br />
<b>P3)</b></font> Someone is a trans person with male sex if, and only if, someone possesses a sufficiently greater sum of weighted traits that more closely associate with large gametes than weighted traits that associate with small gametes and is over 18 years of age.
<br />
<font color="CC6600">
<b>∀x(Ax↔(Lx∧Ox))</b>
<br />
<b>P4)</b></font> Someone is a trans person with female sex if, and only if, someone possesses a sufficiently greater sum of weighted traits that more closely associate with small gametes than weighted traits that associate with large gametes and is over 18 years of age.
<br />
<font color="CC6600">
<b>∀x(Bx↔(Sx∧Ox))</b>
<br />
<b>C)</b></font> Therefore, a trans person with small gametes implies a women and a trans person with large gametes implies a man.
<br />
<font color="CC6600">
<b>(∴∀x(Ax→Wx)∧∀x(Bx→Mx))</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Wx~4(Lx~1Ox))),(~6x(Mx~4(Sx~1Ox))),~6x(Ax~4(Lx~1Ox)),~6x(Bx~4(Sx~1Ox))|=(~6x(Ax~5Wx)~1~6x(Bx~5Mx)))

---

# References

Hermaphrodites:
https://www.fertstert.org/article/S0015-0282(08)00233-1/fulltext

Producing both small and large gametes:
https://pubmed.ncbi.nlm.nih.gov/4532534/
https://www.deepdyve.com/lp/pubmed/ovulation-in-a-cytogenetically-proved-phenotypically-male-fertile-Steztrbecy

---

# Hashtags

#debate 
#arguments 
#transgender 
