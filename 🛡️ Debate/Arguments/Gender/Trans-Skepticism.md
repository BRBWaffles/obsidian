### Trans-Skepticism Argument

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If there is not a known symmetry-breaker between identified, transitioned trans people and identified, de-transitioned trans people, then we do not have epistemic access to the means of parsing legitimate trans people from illegitimate trans people.
<br />
<font color="CC6600">
<b>(¬P→¬Q)</b>
<br />
<b>P2)</b></font> If we do not have epistemic access to the means of parsing legitimate trans people from illegitimate trans people, then there is not a clear epistemically virtuous reason to accept the legitimacy of trans identity.
<br />
<font color="CC6600">
<b>(¬Q→¬R)</b>
<br />
<b>P3)</b></font> There is not a known symmetry-breaker between identified, transitioned trans people and identified, de-transitioned trans people.
<br />
<font color="CC6600">
<b>(¬P)</b>
<br />
<b>C)</b></font> Therefore, there is not a clear epistemically virtuous reason to accept the legitimacy of trans identity.
<br />
<font color="CC6600">
<b>(∴¬R)</b>
<br />
<br />
</font>
</div>

---

# Hashtag

#debate 
#arguments 