## Strict Gametic Definition

#### Intended Usage

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**M**</font>      | something (x) counts as male              |
|      <font color="CC6600">**F**</font>      | something (x) counts as female            |
|      <font color="CC6600">**S**</font>      | something (x) is a small gamete           |
|      <font color="CC6600">**L**</font>      | something (x) is a large gamete           |
|      <font color="CC6600">**p**</font>      | sperm                                     | 
|      <font color="CC6600">**o**</font>      | ova                                       |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something counts as male if, and only if, something is a small gamete, and something counts as female if, and only if something is a large gamete.
<br />
<font color="CC6600">
<b>(∀x(Mx↔Sx)∧∀x(Fx↔Lx))</b>
<br />
<b>P2)</b></font> Sperm are small gametes, and ova are large gametes.
<br />
<font color="CC6600">
<b>(Sp∧Lo)</b>
<br />
<b>C)</b></font> Therefore, sperm count as male and ova count as female.
<br />
<font color="CC6600">
<b>(∴Mp∧Fo)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Mx~4Sx)~1~6x(Fx~4Lx)),(Sp~1Lo)|=(Mp~1Fo))

#### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**M**</font>      | something (x) counts as male              |
|      <font color="CC6600">**F**</font>      | something (x) counts as female            |
|      <font color="CC6600">**S**</font>      | something (x) is a small gamete           |
|      <font color="CC6600">**L**</font>      | something (x) is a large gamete           |
|      <font color="CC6600">**e**</font>      | people                                    |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something counts as male if, and only if, something is a small gamete, and something counts as female if, and only if something is a large gamete.
<br />
<font color="CC6600">
<b>(∀x(Mx↔Sx)∧∀x(Fx↔Lx))</b>
<br />
<b>P2)</b></font> People are neither small gametes nor large gametes.
<br />
<font color="CC6600">
<b>(¬Se∧¬Le)</b>
<br />
<b>C)</b></font> Therefore, people do not count as male or female.
<br />
<font color="CC6600">
<b>(∴¬Me∧¬Fe)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Mx~4Sx)~1~6x(Fx~4Lx)),(~3Se~1~3Le)|=(~3Me~1~3Fe))

---

## Gametic Entailed Definition

#### Intended Usage

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**M**</font>      | something (x) counts as male              |
|      <font color="CC6600">**F**</font>      | something (y) counts as female            |
|      <font color="CC6600">**S**</font>      | something (x) produces small gametes      |
|      <font color="CC6600">**L**</font>      | something (y) produces large gametes      |
|      <font color="CC6600">**t**</font>       | a person with gametogenic testes          |
|      <font color="CC6600">**o**</font>       | a person with gametogenic ovaries         |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something counts as male if, and only if, something produces small gametes, and something counts as female if, and only if something produces large gametes.
<br />
<font color="CC6600">
<b>(∀x(Mx↔Sx)∧∀x(Fx↔Lx))</b>
<br />
<b>P2)</b></font> A person with gametogenic testes produces small gametes, and a person with gametogenic ovaries produces large gametes.
<br />
<font color="CC6600">
<b>(St∧Lo)</b>
<br />
<b>C)</b></font> Therefore, A person with gametogenic testes counts as male, and a person with gametogenic ovaries counts as female.
<br />
<font color="CC6600">
<b>(∴Mt∧Fo)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Mx~4Sx)~1~6x(Fx~4Lx)),(St~1Lo)|=(Mt~1Fo))

#### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**M**</font>      | something (x) counts as male              |
|      <font color="CC6600">**F**</font>      | something (y) counts as female            |
|      <font color="CC6600">**S**</font>      | something (x) produces small gametes      |
|      <font color="CC6600">**L**</font>      | something (y) produces large gametes      |
|      <font color="CC6600">**a**</font>      | a person with azoospermia                 |
|      <font color="CC6600">**w**</font>      | a person with complete gonadal dysgenesis | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something counts as male if, and only if, something produces small gametes, and something counts as female if, and only if something produces large gametes.
<br />
<font color="CC6600">
<b>(∀x(Mx↔Sx)∧∀x(Fx↔Lx))</b>
<br />
<b>P2)</b></font> A person with azoospermia does not produce small gametes and a person with complete gonadal dysgenesis does not produce large gametes.
<br />
<font color="CC6600">
<b>(¬Sa∧¬Lw)</b>
<br />
<b>C)</b></font> Therefore, a person with azoospermia does not count as male, and a person with complete gonadal dysgenesis does not count as female.
<br />
<font color="CC6600">
<b>(∴¬Ma∧¬Fw)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Mx~4Sx)~1~6x(Fx~4Lx)),(~3Sa~1~3Lw)|=(~3Ma~1~3Fw))

---

# Hashtags

#debate 
#arguments 
#transgender 