#### Dietary Patterns

**Proposition:**

>Ancestral diets have inherent disadvantages over novel diets.

**Argument:**
**P1)** If humans have more genetic adaptations to ancestral foods than novel foods, then antagonistic pleiotropy is more of a concern for ancestral foods than novel foods.
**(P→Q)**
**P2)** If antagonistic pleiotropy is more of a concern for ancestral foods than novel foods, then ancestral diets have inherent disadvantages over novel diets.
**(Q→R)**
**P3)** Humans have more genetic adaptations to ancestral foods than novel foods
**(P)**
**C)** Therefore, ancestral diets have inherent disadvantages over novel diets.
**(∴R)**

**Proposition:**

>Ketogenic diets are likely to cost anabolic potential compared to non-ketogenic diets.

**Argument:**
**P1)** If a higher proportion of amino acids are spent on gluconeogenesis while on ketogenic diets compared to non-ketogenic diets, then a lower proportion of amino acids are available for hypertrophy on ketogenic diets compared to non-ketogenic diets.  
**(P→Q)**  
**P2)** If a lower proportion of amino acids are available for hypertrophy on ketogenic diets compared to non-ketogenic diets, then ketogenic diets are likely to cost anabolic potential compared to non-ketogenic diets.
**(Q→R)**
**P3)** A higher proportion of amino acids are spent on gluconeogenesis while on ketogenic diets compared to non-ketogenic diets.  
**(P)**    
**C)** Therefore, ketogenic diets are likely to cost anabolic potential compared to non-ketogenic diets.  
**(∴R)**

**Proposition:**

>Plant-based diets do not appear to clinically reverse atherosclerosis.

**Argument:**

**P1)** If there are established definitions for atherosclerosis reversal in the domain of cardiology and current research on plant-based diets and atherosclerosis reversal do not satisfy the definitions for atherosclerosis reversal in the domain of cardiology, then plant-based diets do not appear to clinically reverse atherosclerosis.
**(P∧¬Q→¬R)**
**P2)** There are established definitions for atherosclerosis reversal in the domain of cardiology.
**(P)**
**P3)** Current research on plant-based diets and atherosclerosis reversal do not satisfy the definitions for atherosclerosis reversal in the domain of cardiology.
**(¬Q)**
**C)** Therefore, plant-based diets do not appear to clinically reverse atherosclerosis.
**(∴¬R)**


#### Meta-Nutrition

**Proposition:**

>All foods are definitionally health-promoting.

**Argument:**

**P1)** If food is defined as material consisting essentially of protein, carbohydrate, and/or fat used in the body of an organism to sustain growth, repair, and vital processes and to furnish energy, then all foods are definitionally health-promoting.
**(P→Q)**
**P2)** food is defined as material consisting essentially of protein, carbohydrate, and/or fat used in the body of an organism to sustain growth, repair, and vital processes and to furnish energy.
**(P)**
**C)** Therefore, all foods are definitionally health-promoting.
**(∴Q)**

**Proposition:**

>The optimal human diet is likely heavily artificial.

**Argument:**

**P1)** If the health value of natural foods can be improved via artificial manipulation, then the optimal human diet is likely heavily artificial.
**(V→M)**
**P2**) The health value of natural foods can be improved via artificial manipulation.
**(V)**
**C)** Therefore, the optimal human diet is likely heavily artificial.
**(∴M)**

#### Nutrition Science

**Proposition:**

>Carbs do not uniquely cause fat accumulation or obesity.

**Argument:**

**P1)** If carbs uniquely cause fat accumulation or obesity, then, populations with high carb diets have uniquely higher rates of obesity after adjustment or control over relevant confounders and covariates.
**(P→Q)**
**P2)** Populations with high carb diets do not have uniquely higher rates of obesity after adjustment or control over relevant confounders and covariates.
**(¬Q)**
**C)** Therefore, carbs do not uniquely cause fat accumulation or obesity.
**(∴¬P)**

**Proposition:**

>Chocolate does not cause atherosclerosis.

**Argument:**

**P1)** For all things, if high saturated fat food (x) causes atherosclerosis, then populations consuming more high saturated fat food (x) have higher rates of atherosclerosis after adjustment or control over relevant confounders and covariates.
**(∀x(Px→Qx))**
**P2)** Populations consuming more chocolate do not have higher rates of atherosclerosis after adjustment or control over relevant confounders and covariates.
**(¬Qc)**
**C)** Therefore, chocolate does not cause atherosclerosis.
**(∴¬Pc)**

**Proposition:**

>Consuming high amounts of dietary cholesterol increases heart disease risk.

**Argument:**

**P1)** For all things, if consuming substance (x) increases LDL, then substance (x) increases heart disease risk.
**(∀x(Px→Qx))**
**P2)** Consuming high amounts of dietary cholesterol increases LDL.
**(Pd)**
**C)** Therefore, consuming high amounts of dietary cholesterol increases heart disease risk.
**(∴Qd)**

**Proposition:**

>Nutritional epidemiology generally provides good causal estimates.

**Argument:**

**P1)** For all things, if a research method (x) consistently identifies associations that are later confirmed by randomized controlled trials, then research method (x) generally provides good causal estimates.
**(∀x(Px→Qx))**
**P2)** Nutritional epidemiology consistently identifies associations that are later confirmed by randomized controlled trials.
**(Pe)**
**C)** Therefore, nutritional epidemiology generally provides good causal estimates.
**(∴Qe)**

**Proposition:**

>Excess sodium intake increases cardiovascular disease risk.

**Argument:**

**P1)** For all things, if substance (x) consistently raises blood pressure in controlled studies and elevated blood pressure is known to cause cardiovascular disease, then high intakes of substance (x) increases cardiovascular disease risk.
**(∀x(Px∧Q→Rx))**
**P2)** Sodium consistently raises blood pressure in controlled studies.
**(Ps)**
**P3)** Elevated blood pressure is known to cause cardiovascular disease.
**(Q)**
**C)** Therefore, high intakes of sodium increases cardiovascular disease risk.
**(∴Rs)**

**Proposition:**

>Fibre, whether whole or refined, is overwhelmingly healthy.

**Argument:**

**P1)** For all things, food (x) is overwhelmingly healthy if, and only if, food (x) consistently associates with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** Fibre, whether whole or refined, consistently associates with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(Qb)**
**C)** Therefore, fibre, whether whole or refined, is overwhelmingly healthy.
**(∴Pb)**

**Proposition:**

>Fatty fish is overwhelmingly healthy.

**Argument:**

**P1)** For all things, food (x) is overwhelmingly healthy if, and only if, food (x) consistently associates with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** Fatty fish consistently associates with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(Qf)**
**C)** Therefore, fatty fish is overwhelmingly healthy.
**(∴Pf)**

**Proposition:**

>Fructose doesn't seem to uniquely cause fatty liver disease.

**Argument:**

**P1)** If experimental investigations into fructose overfeeding consistently fail to recreate a fatty liver phenotype in humans, then fructose does not seem to uniquely cause fatty liver disease.
**(P→¬Q)**
**P2)** Experimental investigations into fructose overfeeding consistently fail to recreate a fatty liver phenotype in humans.
**(P)**
**C)** Therefore, fructose does not seem to uniquely cause fatty liver disease.
**(∴¬Q)**

**Proposition:**

>Non-churned, non-homogenized dairy is generally healthy.

**Argument:**

**P1)** For all things, if high saturated fat food (x) causes atherosclerosis, then populations consuming more high saturated fat food (x) have higher rates of atherosclerosis after adjustment or control over relevant confounders and covariates.
**(∀x(Px→Qx))**
**P2)** Populations consuming more non-churned, non-homogenized dairy do not have higher rates of atherosclerosis after adjustment or control over relevant confounders and covariates.
**(¬Qh)**
**C)** Therefore, non-churned, non-homogenized dairy do not cause atherosclerosis.
**(∴¬Ph)**

**Proposition:**

>Non-hydrogenated vegetable oils are overwhelmingly healthy.

**Argument:**

**P1)** For all things, food (x) is overwhelmingly healthy if, and only if, food (x) consistently associates with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** Non-hydrogenated vegetable oils consistently associate with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(Qv)**
**C)** Therefore, non-hydrogenated vegetable oils are overwhelmingly healthy.
**(∴Pv)**

**Proposition:**

>Red meat and processed meat are overwhelmingly unhealthy.

**Argument:**

**P1)** For all things, food (x) is overwhelmingly unhealthy if, and only if, food (x) consistently associates with an increased risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** Red meat and processed meat consistently associates with an increased risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(Qm∧Qs)**
**C)** Therefore, red meat and processed meat are overwhelmingly unhealthy.
**(∴Pm∧Ps)**

**Proposition:**

>Most sources of saturated fat are overwhelmingly unhealthy.

**Argument:**

**P1)** For all things, food (x) is overwhelmingly unhealthy if, and only if, food (x) consistently associates with an increased risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** Most sources of saturated fat consistently associates with an increased risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(Qf)**
**C)** Therefore, most sources of saturated fat are overwhelmingly unhealthy.
**(∴Pf)**

**Proposition:**

>Soy products are overwhelmingly healthy.

**Argument:**

**P1)** For all things, food (x) is overwhelmingly healthy if, and only if, food (x) consistently associates with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** Soy products consistently associate with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(Qs)**
**C)** Therefore, soy products are overwhelmingly healthy.
**(∴Ps)**

**Proposition:**

>The omega-6/omega-3 ratio is unlikely to matter for health.

**Argument:**

**P1)** If increases or decreases in disease risk associated with a high or low omega-6/omega-3 ratio are better explained by changes in omega-3 alone, then the omega-6/omega-3 ratio is not likely to matter for health.
**(P→¬Q)**
**P2)** Increases or decreases in disease risk associated with a high or low omega-6/omega-3 ratio are better explained by changes in omega-3 alone.
**(P)**
**C)** Therefore, the omega-6/omega-3 ratio is not likely to matter for health.
**(∴¬Q)**

**Proposition:**

>TMAO is not likely to be causative of heart disease.

**Argument:**

**P1)** For all things, if substance (x) is likely to be causative of heart disease if, then substance (x) consistently associates with an increased risk of heart disease after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** TMAO consistently associates with an increased risk of heart disease after adjustment or control over relevant confounders and covariates.
**(¬Qt)**
**C)** Therefore, TMAO is not likely to be causative of heart disease.
**(∴¬Pt)**

**Proposition:**

>Whole plant foods are overwhelmingly healthy.

**Argument:**

**P1)** For all things, food (x) is overwhelmingly healthy if, and only if, food (x) consistently associates with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** Whole plant foods consistently associates with a reduced risk of morbidity and mortality after adjustment or control over relevant confounders and covariates.
**(Qw)**
**C)** Therefore, whole plant foods are overwhelmingly healthy.
**(∴Pw)**

**Proposition:**

>Anti-polyphenol, ancestral diet advocates are committed to favouring GMOs

**Argument:**

**P1)** If plant defense chemicals are harmful and plant defense chemicals are contained in a food, then plant defense chemicals render the food harmful.  
**(∀x∀y(Hx∧Cxy→Vxy))**  
**P2)** Polyphenols from grass are harmful.  
**(Hp)**  
**P3)** Polyphenols from grass are contained in grass-fed beef.  
**(Cpg)**  
**P4)** If polyphenols from grass render grass-fed beef harmful, then genetic modification that removes polyphenols from grass renders grass-fed beef less harmful.  
**(Vpg→Mpg)**  
**P5)** Polyphenols from grass render grass-fed beef harmful.  
**(Vpg)**  
**P6)** If genetic modification that removes polyphenols from grass renders grass-fed beef less harmful, then health-conscious, anti-polyphenol, ancestral diet advocates are committed to favouring GMOs.  
**(Mpg→G)**  
**C)** Therefore, health-conscious, anti-polyphenol, ancestral diet advocates are committed to favouring GMOs.  
**(∴G)**

#### Ethics

**Proposition:**

>We are justified in displacing sentient wild animals into non-existence.

**Argument:**

**P1)** If the natural world contains intolerable rights violations and we do not know if nature is net positive or net negative for utility and there is not any known practical means by which to end the rights violations beyond the use of force and the natural world is not currently instrumentally vital to facilitating human flourishing, then we are justified in displacing sentient wild animals into non-existence.  
**(P∧¬Q∧¬R∧¬S→T)**  
**P2)** Nature entails intolerable rights violations.  
**(P)**  
**P3)** We do not know if nature is net positive or net negative for utility.  
**(¬Q)**  
**P4)** There is not any known practical means by which to end the rights violations beyond the use of force.  
**(¬R)**  
**P5)** The natural world is not currently instrumentally vital to facilitating human flourishing.  
**(¬S)**  
**C)** Therefore, we are justified in displacing sentient wild animals into non-existence.  
**(∴T)**

**Proposition:**

>Vandalizing zoos increases the probability of harming the animals they keep.

**Argument:**

**P1)** If zoos must spend extra money cleaning graffiti, then zoos will have less money to devote to animal care.  
**(P→Q)**  
**P2)** Zoos must spend extra money cleaning graffiti.  
**(P)**  
**P3)** If zoos will have less money to devote to animal care, then vandalizing zoos increases the probability of harming the animals they keep.  
**(Q→R)**  
**C)** Therefore, vandalizing zoos increases the probability of harming the animals they keep.  
**(∴R)**

**Proposition:**

>We should not exploit animals to any greater degree than we would tolerate for humans.

**Argument:**

**P1)** For all things, if a being has moral worth, then we should not exploit it to any greater degree than we would tolerate for humans.  
**(∀x(Wx→Nx))**  
**P2)** If animals don’t have moral worth, then there exists a trait that is absent in animals such that if it were absent in humans, humans wouldn’t have moral worth.  
**(¬Wa→∃t(Ata→(Ath→¬Wh)))**  
**P3)** There doesn’t exist a trait that is absent in animals such that if it were absent in humans, humans wouldn’t have moral worth.  
**(¬∃t(Ata→(Ath→¬Wh)))**  
**C)** Therefore, we should not exploit animals to any greater degree than we would tolerate for humans.  
**(∴Na)**

**Proposition:**

>One's whims are not a sufficient justification for the termination of sentient human life.

**Argument:**

**P1)** If one consents to becoming pregnant or one consensually engages in sexual activity without contraception, then one is implicitly committed to at least accepting the average risks for the average pregnancy.  
**(P∨Q→R)**  
**P2)** One consents to becoming pregnant.  
**(P)**  
**P3)** One consensually engages in sexual activity without contraception.  
**(Q)**  
**P4)** If one is implicitly committed to at least accepting the average risks for the average pregnancy and one's pregnancy persists long enough for fetal sentience to develop and one's risk profile during pregnancy is not high, then one is morally bound to carrying the pregnancy to term.  
**(R∧F∧¬H→M)**  
**P5)** One's pregnancy persists long enough for fetal sentience to develop.  
**(F)**  
**P6)** One's risk profile during pregnancy is not high.  
**(¬H)**  
**P7)** If one is morally bound to carrying the pregnancy to term, then one's whims are not a sufficient justification for the termination of sentient human life.  
**(M→¬W)**  
**C)** Therefore, one's whims are not a sufficient justification for the termination of sentient human life.  
**(∴¬W)**

**Proposition:**

>Efilists are committed to pragmatic natalism

**Argument:**

**P1)** If efilists abstaining from procreation maximally reduces rights violations, then efilists have sterilized all sentient life in the universe.  
**(P→Q)**  
**P2)** Efilists have not sterilized all sentient life in the universe.  
**(¬Q)**  
**P3)** If efilists abstaining from procreation does not maximally reduce rights violations and many more generations are required to sterilize all sentient life in the universe, then efilists should not abstain from procreation until all sentient life in the universe is sterilized.  
**(¬P∧R→¬S)**  
**P4)** Many more generations are required to sterilize all sentient life in the universe.  
**(R)**  
**P5)** If efilists should not abstain from procreation until all sentient life in the universe is sterilized, then efilists are committed to pragmatic natalism.  
**(¬S→T)**  
**C)** Therefore, efilists are committed to pragmatic natalism.  
**(∴T)**

**Proposition:**

>Rewilding sentient animals is immoral.

**Argument:**

**P1)** For all things, if something has negative rights, then we should not defend it from rights violations to any lesser degree than we would tolerate for humans.  
**(∀x(Wx→¬Nx))**  
**P2)** Animals have negative rights.  
**(Wa)**  
**P3)** If we should not defend animals from rights violations to any lesser degree than we would tolerate for humans, then it is not permissible to subject animals to conditions that are likely to involve predation, starvation, or death due to environmental exposure to any greater degree than we would tolerate for humans.  
**(¬Na→¬F)**  
**P4)**it is not permissible to subject animals to conditions that are likely to involve predation, starvation, or death due to environmental exposure to any greater degree than we would tolerate for humans, then rewilding sentient animals is immoral.  
**(¬F→R)**  
**C)** Therefore, rewilding sentient animals is immoral.  
**(∴R)**

**Proposition:**

>We are justified in displacing nature into non-existence.

**Argument:**

**P1)** If the natural world contains intolerable rights violations and we do not know if nature is net positive or net negative for utility and there is not any known practical means by which to end the rights violations beyond the use of force and the natural world is not currently instrumentally vital to facilitating human flourishing, then we are justified in displacing nature into non-existence.  
**(P∧¬Q∧¬R∧¬S→T)**  
**P2)** Nature entails intolerable rights violations.  
**(P)**  
**P3)** We do not know if nature is net positive or net negative for utility.  
**(¬Q)**  
**P4)** There is not any known practical means by which to end the rights violations beyond the use of force.  
**(¬R)**  
**P5)** The natural world is not currently instrumentally vital to facilitating human flourishing.  
**(¬S)**  
**C)** Therefore, we are justified in displacing nature into non-existence.  
**(∴T)**

**Proposition:**

>Bivalves are not likely to be sentient. 

**Argument:**

**P1)** Something counts as a brain if, and only if, something is a complex network of interconnected neurons, integrates neuronal pathways from multiple sensory organs, and presents with distinct functional regions or nuclei with white matter tracts that facilitate inter-regional communication.  
**(∀x(Bx↔Px∧Qx∧(Rx∨Sx)))**  
**P2)** Cerebral ganglia meet criteria one and three, but not criteria two and four.  
**(Pc∧¬Qc∧Rc∧¬Sc)**  
**P3)** If cerebral ganglia do not count as brains, then the corresponding brainless biological system into which the cerebral ganglia are integrated is not likely to be sentient.  
**(¬Bc→∀x(¬Nx))**  
**C)** Therefore, bivalves are not likely to be sentient.  
**(∴¬Nb)**

#### Medical Science

**Proposition:**

>ApoB-containing lipoproteins dose-dependently cause atherosclerosis.

**Argument:**

**P1)** For all things, substance (x) dose-dependently causes atherosclerosis if, and only if, substance (x) consistently, linearly, and proportionately associates with increased plaque volume after adjustment or control over relevant confounders and covariates.
**(∀x(Px↔Qx))**
**P2)** ApoB-containing lipoproteins consistently, linearly, and proportionately associates with increased plaque volume after adjustment or control over relevant confounders and covariates.
**(Qw)**
**C)** Therefore, ApoB-containing lipoproteins dose-dependently cause atherosclerosis.
**(∴Pw)**

#### Agriculture

**Proposition:**

>Animal agriculture counts as predation.

**Argument:**

**P1)** For all things, an action (x) counts as predation if, and only if, an action (x) involves an animal regularly capturing, killing or subduing, and consuming, to the captor's benefit, another animal.  
**(∀x∀y∀z(Px↔Qxyz∧(Rxyz∨Sxyz)∧Txyz))**  
**P2)** Animal agriculture involves humans regularly capturing livestock.  
**(Qahl)**  
**P3)** Animal agriculture involves humans regularly killing or subduing livestock.  
**(Rahl∨Sahl)**  
**P4)** Animal agriculture involves humans regularly consuming, to the captors' benefit, livestock.  
**(Tahl)**  
**C)** Therefore, animal agriculture counts as predation.  
**(∴Pa)**

**Proposition:**

>It is not known if cropland leads to more animal death than wildland.

**Argument:**

**P1)** If it is known that cropland leads to more animal death than wildland, then there is evidence that cropland leads to more animal death than wildland.  
**(P→Q)**  
**P2)** There is not evidence that cropland leads to more animal death than wildland.  
**(¬Q)**  
**C)** Therefore, it is not known that cropland leads to more animal death than wildland.  
**(∴¬P)**

#### Theology

**Proposition:**

>Fine tuning is an infinitely regressive explanation for God.

**Argument:**

**P1)** If something is finely tuned, then something has a designer.  
**(∀x(Fx→Dx))**  
**P2)** God's constitution is finely tuned.  
**(Fg)**  
**P3)** If God's constitution has a designer, then fine tuning is an infinitely regressive explanation for God.  
**(Dg→T)**  
**C)** Therefore, fine tuning is an infinitely regressive explanation for God.  
**(∴T)**