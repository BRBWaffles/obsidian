### Tattoo Pioneers were Scratchers

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                |
|:-------------------------------------------:|:-------------------------------------------------------- |
|      <font color="CC6600">**S**</font>      | one (x) is a scratcher                                   |
|      <font color="CC6600">**L**</font>      | one (x) tattoos without a license                        | 
|      <font color="CC6600">**H**</font>      | one (x) tattoos without observing basic health standards |
|      <font color="CC6600">**U**</font>      | one (x) tattoos without having undertaken apprenticeship |
|      <font color="CC6600">**t**</font>      | original trailblazers of modern tattooing                |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> One is a scratcher if, and only if, one tattoos without a license and/or one tattoos without observing basic health standards and/or one tattoos without having undertaken apprenticeship.
<br />
<font color="CC6600">
<b>(∀x(Sx↔Lx∨Hx∨Ux))</b>
<br />
<b>P2)</b></font> The original trailblazers of modern tattooing tattooed without licenses.
<br />
<font color="CC6600">
<b>(Lt)</b>
<br />
<b>P3)</b></font> The original trailblazers of modern tattooing tattooed without observing basic health standards.
<br />
<font color="CC6600">
<b>(Ht)</b>
<br />
<b>P4)</b></font> The original trailblazers of modern tattooing tattooed without having undertaken apprenticeship.
<br />
<font color="CC6600">
<b>(Ut)</b>
<br />
<b>C)</b></font> Therefore, the original trailblazers of modern tattooing were scratchers.
<br />
<font color="CC6600">
<b>(∴St)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Sx~4Lx~2Hx~2Ux)),(Lt),(Ht),(Ut)|=(St))

---

# Hashtags

#debate 
#arguments 
#tattooing 