### Ancestral Health Consistency Checker

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                              |
|:-------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**A**</font>      | F(x) would be acting against their values                                      |
|      <font color="CC6600">**C**</font>      | F(x) is in favour of consuming N(y)                                                                                    | 
|    <font color="CC6600">**F(x)**</font>     | someone who favours consuming ancestral foods to the exclusion of novel foods because they value reducing disease risk |
|    <font color="CC6600">**N(y)**</font>     | a novel food that reduces disease risk when replacing an ancestral food                                                |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If there exists someone who favours consuming ancestral foods to the exclusion of novel foods because they value reducing disease risk, and there exists a novel food that reduces disease risk when replacing an ancestral food, then if that person is not in favour of consuming that novel food, then that person would be acting against their values.
<br />
<font color="CC6600">
<b>(∃xFx∧∃yNy→∀x∀y(¬Cxy→Ax))</b>
<br />
<b>P2)</b></font> There exists someone who favours consuming ancestral foods to the exclusion of novel foods because they value reducing disease risk.
<br />
<font color="CC6600">
<b>(∃xFx)</b>
<br />
<b>P3)</b></font> There exists a novel food that reduces disease risk when replacing an ancestral food.
<br />
<font color="CC6600">
<b>(∃yNy)</b>
<br />
<b>C)</b></font> Therefore, if that person is not in favour of consuming that novel food, then that person would be acting against their values.
<br />
<font color="CC6600">
<b>(∴∀x∀y(¬Cxy→Ax))</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~7xFx~1~7yNy~5~6x~6y(~3Cxy~5Ax)),(~7xFx),(~7yNy)|=(~6x~6y(~3Cxy~5Ax)))

Essentially, if our interlocutor identifies as "**F**", then all we need to do is demonstrate to them that "**N**" exists, and we're essentially home free. If they accept that "**N**" exists and they also identify as "**F**", then they should be in favour of substituting such a novel food for such an ancestral food. If they don't then they have a contradiction.

---

#  Hashtags

#debate 
#arguments 