### Ancestral Superiority Consistency Test

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                               |
|:-------------------------------------------:|:--------------------------------------------------------------------------------------- |
|      <font color="CC6600">**E**</font>       | something (y) is more ancestral than something else (z)                                 |
|      <font color="CC6600">**N**</font>       | engaging (x) with something (y) is to be favoured over engaging with something else (z) |
|      <font color="CC6600">**b**</font>       | black people                                                                            |
|      <font color="CC6600">**w**</font>       | white people                                                                            |
|      <font color="CC6600">**s**</font>       | sex                                                                                     | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If something is more ancestral than something else, then engaging with something is to be favoured over engaging with something else.
<br />
<font color="CC6600">
<b>(∀x∀y∀z(Cyz→Nxyz))</b>
<br />
<b>P2)</b></font> Black people are more ancestral than white people.
<br />
<font color="CC6600">
<b>(Cbw)</b>
<br />
<b>C)</b></font> Therefore, sex with black people is to be favoured over sex with white people.
<br />
<font color="CC6600">
<b>(∴Nsbw)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y~6z(Cyz~5Nxyz)),(Cbw)|=(Nsbw))

---

# Hashtags

#debate 
#arguments 
#clownery 
#ancestral 