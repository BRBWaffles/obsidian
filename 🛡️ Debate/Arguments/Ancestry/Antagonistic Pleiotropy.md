### Argument Against Ancestral Diets

| <font color="CC6600">**Definiendum**</font>             | <font color="CC6600">**Definitions**</font>                                                                                                                                                                                                                              |
| ------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| <font color="CC6600">**Antagonistic pleiotropy**</font> | When one adaptation controls for more than one trait, where at least one of these traits is beneficial to the organism's fitness early on in life and at least one is detrimental to the organism's fitness later on due to a decline in the force of natural selection. |
| <font color="CC6600">**Ancestral food**</font>          | A food to which we are presumably adapted, due to having consumed it consistently throughout evolutionary history.                                                                                                                                                       |
| <font color="CC6600">**Novel food**</font>              | A food to which we are presumably not adapted, due to not having consumed it consistently throughout evolutionary history.                                                                                                                                               |
| <font color="CC6600">**Short term**</font>              | Within the reproductive window.                                                                                                                                                                                                                                          |
| <font color="CC6600">**Long term**</font>               | Beyond the reproductive window.                                                                                                                                                                                                                                          |
| <font color="CC6600">**Gene degradation**</font>        | The natural decline in gene functionality due to entropy.                                                                                                                                                                                                                |
| <font color="CC6600">**Gene repair**</font>             | The regulated repair of genes that lose functionality due to entropy.                                                                                                                                                                                                    |
| <font color="CC6600">**Adaptation**</font>              | Inherited traits that are the product of natural selection.                                                                                                                                                                                                              |
| <font color="CC6600">**Ancestral diets**</font>         | Diets comprised of ancestral foods.                                                                                                                                                                                                                                      |
| <font color="CC6600">**Unnatural diets**</font>         | Diets that contain at least one novel food.                                                                                                                                                                                                                              |
| <font color="CC6600">**A**</font>                       | a human trait (x) is antagonistically pleiotropic                                                                                                                                                                                                                        |
| <font color="CC6600">**E**</font>                       | the human trait (x) is an evolutionary adaptation                                                                                                                                                                                                                        |
| <font color="CC6600">**N**</font>                       | the human trait (x) mediates more than one downstream effect, one of which is negative in the post-reproductive window of the carrier                                                                                                                                    |
| <font color="CC6600">**D**</font>                       | the majority of human genes succumb to degradation                                                                                                                                                                                                                       |
| <font color="CC6600">**M**</font>                       | human genes must be assumed to be antagonistically pleiotropic                                                                                                                                                                                                           |
| <font color="CC6600">**R**</font>                       | humans have more genetic adaptations to ancestral foods than novel foods                                                                                                                                                                                                 |
| <font color="CC6600">**C**</font>                       | antagonistic pleiotropy applies more to ancestral foods than it does to novel foods                                                                                                                                                                                      |
| <font color="CC6600">**V**</font>                       | there is a novel food that has non-inferior short term health value compared to a given ancestral foods                                                                                                                                                                  |
| <font color="CC6600">**U**</font>                       | there is a novel food that is likely to be superior to such an ancestral food in the long term                                                                                                                                                                           |
| <font color="CC6600">**O**</font>                       | there are unnatural diets that are superior to ancestral diets                                                                                                                                                                                                           |
| <font color="CC6600">**i**</font>                       | the interaction between degradation and repair                                                                                                                                                                                                                           |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> A human trait is antagonistically pleiotropic if and only if the trait is an evolutionary adaptation and the trait mediates more than one downstream effect, one of which is negative in the post-reproductive window of the carrier.
<br />
<font color="CC6600">
<b>(∀x(Ax↔Ex∧Nx))</b>
<br />
<b>P2)</b></font> The interaction between gene degradation and gene repair is an evolutionary adaptation.
<br />
<font color="CC6600">
<b>(Ei)</b>
<br />
<b>P3)</b></font> The interaction between gene degradation and gene repair mediates more than one downstream effect, one of which is negative in the post-reproductive window of the carrier.
<br />
<font color="CC6600">
<b>(Ni)</b>
<br />
<b>P4)</b></font> If the interaction between gene degradation and gene repair is antagonistically pleiotropic and all human genes succumb to degradation, then all human genes must be assumed to be antagonistically pleiotropic.
<br />
<font color="CC6600">
<b>(Ai∧D→M)</b>
<br />
<b>P5)</b></font> The majority of human genes succumb to degradation.
<br />
<font color="CC6600">
<b>(D)</b>
<br />
<b>P6)</b></font> If the majority of human genes must be assumed to be antagonistically pleiotropic and humans have more genetic adaptations to ancestral foods than novel foods, then antagonistic pleiotropy applies more to ancestral foods than it does to novel foods.
<br />
<font color="CC6600">
<b>(M∧R→C)</b>
<br />
<b>P7)</b></font> Humans have more genetic adaptations to ancestral foods than novel foods.
<br />
<font color="CC6600">
<b>(R)</b>
<br />
<b>P8)</b></font> If antagonistic pleiotropy applies more to ancestral foods than it does to novel foods and there is a novel food that has non-inferior short term health value compared to a given ancestral food, then there is a novel food that is likely to be superior to such an ancestral food in the long term.
<br />
<font color="CC6600">
<b>(C∧V→U)</b>
<br />
<b>P9)</b></font> There is a novel food that has non-inferior short term health value compared to a given ancestral food.
<br />
<font color="CC6600">
<b>(V)</b>
<br />
<b>P10)</b></font> If there is a novel food that is likely superior to such an ancestral food in the long term, then there are some unnatural diets that are superior to some ancestral diets.
<br />
<font color="CC6600">
<b>(U→O)</b>
<br />
<b>C)</b></font> Therefore, there are some unnatural diets that are superior to some ancestral diets.
<br />
<font color="CC6600">
<b>(∴O)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Ax~4Ex~1Nx)),(Ei),(Ni),(Ai~1D~5M),(D),(M~1R~5C),(R),(C~1V~5U),(V),(U~5O)%7C=(O))

Essentially, human DNA tends to degrade over time when it doesn't rightfully need to, as evidenced by the existence of biologically immortal organisms. Human DNA repair is also regulated and gene-specific. Given these facts, DNA degradation in humans itself is likely to be adaptive. This assigns every gene in our DNA that degrades over time a single antagonistically pleiotropic trait. Since most DNA in the human genome degrades over time, we can infer that over 50% of genes are antagonistically pleiotropic.

---

### Antagonistic Pleiotropy and Ancestral Foods

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If humans have more genetic adaptations to ancestral foods than novel foods, then antagonistic pleiotropy applies more to ancestral foods than novel foods.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Humans have more genetic adaptations to ancestral foods than novel foods.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> If antagonistic pleiotropy applies more to ancestral foods than novel foods, then ancestral foods are expected to decrease fitness in the post-reproductive window compared to novel foods.
<br />
<font color="CC6600">
<b>(Q→R)</b>
<br />
<b>C)</b></font> Therefore, ancestral foods are expected to decrease fitness in the post-reproductive window compared to novel foods.
<br />
<font color="CC6600">
<b>(∴R)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P),(Q~5R)|=(R))

---

### Antagonistic Pleiotropy and Ancestral Foods

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If humans have more genetic adaptations to ancestral foods than novel foods, then antagonistic pleiotropy is more of a concern for ancestral foods than novel foods.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Humans have more genetic adaptations to ancestral foods than novel foods.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, antagonistic pleiotropy is more of a concern for ancestral foods than novel foods.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

---

### Argument for the Superiority of Certain Novel Foods

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a given novel food has non-inferior health value to a given ancestral food in the reproductive window, then such a novel food is likely to be superior to such an ancestral food in the post-reproductive window.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> A given novel food has non-inferior health value to a given ancestral food in the reproductive window.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, such a novel food is likely to be superior to such an ancestral food in the post-reproductive window.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

![[Pasted image 20220619152531.png]]

Basically, we can infer that a certain subset of novel foods, which are non-inferior to ancestral foods early in life, are likely to be superior to ancestral foods later in life. This is based on the fact that most adaptations are antagonistically pleiotropic, and novel foods do not belong to the domain of foods that can be antagonistically pleiotropic.

---

### Superior Unnatural Diet

Take the best ancestral diet and...
- Swap out saturated fat.
- Add on supplementary protein powder.
- Add in supplementary soluble fibre.
- Add in phytosterol-enriched vegetable oils.
- Add in processed cocoa like chocolate
- Replace red meat with processed soy like tofu.
- Fortify foods with B12, folic acid, calcium, and vitamin D.

---

# Hashtags

#ap_argument
#novel_food
#ancestral_food
#primitive_cultures 
#arguments 
#antagonistic_pleiotropy
#philosophy
#evolution