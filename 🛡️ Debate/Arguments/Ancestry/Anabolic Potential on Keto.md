<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a higher proportion of amino acids are spent on gluconeogenesis while on ketogenic diets compared to non-ketogenic diets, then a lower proportion of amino acids are available for hypertrophy on ketogenic diets compared to non-ketogenic diets.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> A higher proportion of amino acids are spent on gluconeogenesis while on ketogenic diets compared to non-ketogenic diets.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> If a lower proportion of amino acids are available for hypertrophy on ketogenic diets compared to non-ketogenic diets, then ketogenic diets are likely to cost anabolic potential compared to non-ketogenic diets.
<br />
<font color="CC6600">
<b>(Q→R)</b>
<br />
<b>C)</b></font> Therefore, ketogenic diets are likely to cost anabolic potential compared to non-ketogenic diets.
<br />
<font color="CC6600">
<b>(∴R)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P),(Q~5R)|=(R))

---

# Hashtags

#arguments 
#debate 
#keto 
#hypertrophy 