### Immortality Reductio

| <font color="CC6600">**Variable**</font> | <font color="CC6600">**Definition**</font> |
|:----------------------------------------:|:------------------------------------------ |
|   <font color="CC6600">**H(x)**</font>   | humans undergo (x)                         |
|  <font color="CC6600">**R(x,y)**</font>  | (x) does result in (y)                     |
|    <font color="CC6600">**x**</font>     | normal physiological process               |
|    <font color="CC6600">**y**</font>     | negative health outcome                    |
|    <font color="CC6600">**s**</font>     | senescence                                 |
|    <font color="CC6600">**d**</font>     | death                                      |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all things, if humans undergo a normal physiological process, then the normal physiological process does not result in a negative health outcome.
<br />
<font color="CC6600">
<b>(∀x∀y(Hx→¬Rxy))</b>
<br />
<b>P2)</b></font> Humans undergo senescence.
<br />
<font color="CC6600">
<b>(Hs)</b>
<br />
<b>C)</b></font> Therefore, senescence does not result in death.
<br />
<font color="CC6600">
<b>(∴¬Rsd)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Hx~5~3Rxy)),(Hs)|=(~3Rsd))

---

### Hashtags

#debate 
#arguments 
