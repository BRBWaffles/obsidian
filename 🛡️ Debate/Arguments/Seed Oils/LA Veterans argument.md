###

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If underpowered studies provide weaker causal estimates than adequately powered studies, then meta-analytic summations that include adequately powered studies to the exclusion of insufficiently powered studies will provide better causal estimates than meta-analytic summations that include both adequately powered studies and insufficiently powered studies.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Underpowered studies provide weaker causal estimates than adequately powered studies.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, meta-analytic summations that include adequately powered studies to the exclusion of insufficiently powered studies will provide better causal estimates than meta-analytic summations that include both adequately powered studies and insufficiently powered studies.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

---

# Hashtags

#arguments 
#debate 