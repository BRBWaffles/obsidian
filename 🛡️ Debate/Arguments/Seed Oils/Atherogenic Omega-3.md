### Atherogenic Omega-3 Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                             |
|:-------------------------------------------:|:--------------------------------------------------------------------- |
|      <font color="CC6600">**A**</font>      | something (x) increases the risk of atherosclerosis                   |
|      <font color="CC6600">**L**</font>      | something (x) facilitates the oxidative modification of LDL particles |
|      <font color="CC6600">**F**</font>      | all MDA-producing fatty acids (x) are atherogenic                     |
|      <font color="CC6600">**m**</font>      | MDA                                                                   |
|      <font color="CC6600">**o**</font>      | omega-3s                                                              | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something increases the risk of atherosclerosis if and only if something facilitates the oxidative modification of LDL particles.
<br />
<font color="CC6600">
<b>(∀x(Ax↔Lx))</b>
<br />
<b>P2)</b></font> MDA facilitates the oxidative modification of LDL particles.
<br />
<font color="CC6600">
<b>(Lm)</b>
<br />
<b>P3)</b></font> If MDA increases the risk of atherosclerosis, then all MDA-producing fatty acids are atherogenic.
<br />
<font color="CC6600">
<b>(Am→∀x(Fx))</b>
<br />
<b>C)</b></font> Therefore, omega-3s are atherogenic.
<br />
<font color="CC6600">
<b>(∴Fo)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Ax~4Lx)),(Lm),(Am~5~6x(Fx))|=(Fo))

---

# Hashtags

#debate 
#arguments  
#omega_3 
#omega_6 
#MDA 