
### Fine Tuning Regress V2

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                   |
|:-------------------------------------------:|:----------------------------------------------------------- |
|      <font color="CC6600">**F**</font>      | something (x) is finely tuned                               |
|      <font color="CC6600">**D**</font>      | something (x) has a designer                                |
|      <font color="CC6600">**T**</font>      | fine tuning is an infinitely regressive explanation for God |
|      <font color="CC6600">**g**</font>      | God's constitution                                          |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If something is finely tuned, then something has a designer.
<br />
<font color="CC6600">
<b>(∀x(Fx→Dx))</b>
<br />
<b>P2)</b></font> God's constitution is finely tuned.
<br />
<font color="CC6600">
<b>(Fg)</b>
<br />
<b>P3)</b></font> If God's constitution has a designer, then fine tuning is an infinitely regressive explanation for God.
<br />
<font color="CC6600">
<b>(Dg→T)</b>
<br />
<b>C)</b></font> Therefore, fine tuning is an infinitely regressive explanation for God.
<br />
<font color="CC6600">
<b>(∴T)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Fx~5Dx)),(Fg),(Dg~5T)|=(T))

---

# Hashtags

#debate 
#arguments 
