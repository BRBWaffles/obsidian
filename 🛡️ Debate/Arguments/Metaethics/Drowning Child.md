## Tit-Gazing Case Reductio

>Bob is walking home from work one day when he sees a lifelike statue of a woman with pair of shapely, double-D breasts inside a nearby park. Bob can stop to admire the breasts, and he knows that the experience would be very exciting. However, Bob is not wearing his glasses and does not have enough time to enter the park. In order to fully appreciate the breasts he will have to squint and be momentarily uncomfortable. Should Bob gaze upon the breasts as he passes by?
>
>The Tit-Gazing case is aesthetically compelling because we seem to be able to see, without argument, that we ought to stop and admire the breasts. If this is right, then the judgment that we ought to admire the breasts cannot be based on some contingent social or psychological fact about us. Rather, it seems that the claim that we ought to admire the breasts is an objective aesthetic truth that exists independently of our beliefs or desires. This is evidence in favour of aesthetic realism, the view that there are objective aesthetic facts that are true independently of us and our beliefs.

---

## Hashtags

#philosophy 
#moral_realism 
#sexuality 