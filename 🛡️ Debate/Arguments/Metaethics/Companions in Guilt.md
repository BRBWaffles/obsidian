
### Companions in Guilt

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If error theory is true then there are no categorical moral norms.
<br />
<font color="CC6600">
<b>(P→¬Q)</b>
<br />
<b>P2)</b></font> If there are no square-circle norms, then there are no categorical epistemic norms.
<br />
<font color="CC6600">
<b>(¬Q→¬R)</b>
<br />
<b>P3)</b></font> There are categorical epistemic norms.
<br />
<font color="CC6600">
<b>(R)</b>
<br />
<b>C)</b></font> Therefore, error theory is false and there are categorical moral norms.
<br />
<font color="CC6600">
<b>(∴¬P∧Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5~3Q),(~3Q~5~3R),(R)|=(~3P~1Q))

---

### Companions in Guilt Interpretation

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If error theory is true then there are no square-circle norms.
<br />
<font color="CC6600">
<b>(P→¬Q)</b>
<br />
<b>P2)</b></font> If there are no square-circle norms, then there are no circle-square norms.
<br />
<font color="CC6600">
<b>(¬Q→¬R)</b>
<br />
<b>P3)</b></font> There are circle-square norms.
<br />
<font color="CC6600">
<b>(R)</b>
<br />
<b>C)</b></font> Therefore, error theory is false and there are square-circle norms.
<br />
<font color="CC6600">
<b>(∴¬P∧Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5~3Q),(~3Q~5~3R),(R)|=(~3P~1Q))

---

| Moral Norm   | Epistemic Norm | Judgement    |
| ------------ | -------------- | ------------ |
| categorical  | categorical    | gibberish    |
| categorical  | hypothetical   | equivocation |
| hypothetical | categorical    | equivocation | 
| hypothetical | hypothetical   | agree        |

---

# Hashtags

#moral_realism 
#philosophy 