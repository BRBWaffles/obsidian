### Delicious Bivalve Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                     |
|:-------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**B**</font>      | something (x) counts as a brain                                                                                               |
|      <font color="CC6600">**P**</font>      | something (x) is a complex network of interconnected neurons                                                                  |
|      <font color="CC6600">**Q**</font>      | something (x) integrates neuronal pathways from multiple sensory organs                                                       |
|      <font color="CC6600">**R**</font>      | something (x) presents with distinct functional regions                                                                       |
|      <font color="CC6600">**S**</font>      | something (x) presents with nuclei with white matter tracts that facilitate inter-regional communication                      |
|      <font color="CC6600">**N**</font>      | the corresponding brainless biological system into which the cerebral ganglia are integrated (x) is not likely to be sentient |
|      <font color="CC6600">**c**</font>      | cerebral ganglia                                                                                                              | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something counts as a brain if, and only if, something is a complex network of interconnected neurons, integrates neuronal pathways from multiple sensory organs, and presents with distinct functional regions or nuclei with white matter tracts that facilitate inter-regional communication.
<br />
<font color="CC6600">
<b>(∀x(Bx↔Px∧Qx∧(Rx∨Sx)))</b>
<br />
<b>P2)</b></font> Cerebral ganglia meet criteria one and three, but not criteria two and four.
<br />
<font color="CC6600">
<b>(Pc∧¬Qc∧Rc∧¬Sc)</b>
<br />
<b>P3)</b></font> If cerebral ganglia do not count as brains, then the corresponding brainless biological system into which the cerebral ganglia are integrated is not likely to be sentient.
<br />
<font color="CC6600">
<b>(¬Bc→∀x(¬Nx))</b>
<br />
<b>C)</b></font> Therefore, bivalves are not likely to be sentient.
<br />
<font color="CC6600">
<b>(∴¬Nb)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Bx~4Px~1Qx~1(Rx~2Sx))),(Pc~1~3Qc~1Rc~1~3Sc),(~3Bc~5~6x(~3Nx))|=(~3Nb))

---

### Argument for Bivalve Consumption

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|        <font color="CC6600">V</font>        | it is vegan to consume (x) lifeform       |
|        <font color="CC6600">S</font>        | (x) lifeform does have sentience          |
|        <font color="CC6600">b</font>        | bivalves                                  |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If it is not vegan to consume a lifeform, then the lifeform does have sentience.
<br />
<font color="CC6600">
<b>(∀x(¬Vx→Sx)</b>
<br />
<b>P2)</b></font> Bivalves do not have sentience.
<br />
<font color="CC6600">
<b>(¬Sb)</b>
<br />
<b>C)</b></font> Therefore, it is vegan to consume bivalves.
<br />
<font color="CC6600">
<b>(∴Vb)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(~3Vx~5Sx)),(~3Sb)%7C=(Vb))

---

### Ecological Harm Tho

| <font color="CC6600">**Variable**</font> | <font color="CC6600">**Definition**</font> |
|:----------------------------------------:|:------------------------------------------ |
|      <font color="CC6600">F</font>       | the food is OK to eat                      |
|      <font color="CC6600">H</font>       | the food does cause ecological harm        |
|      <font color="CC6600">s</font>       | soy                                        | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b> <font color="DADADA">If the food is OK to eat, then the food does not cause ecological harm.</font>
<br />
<b>(∀x(Fx→¬Hx))</b>
<br />
<b>P2)</b> <font color="DADADA">Soy does cause ecological harm.</font>
<br />
<b>(Hs)</b>
<br />
<b>C)</b> <font color="DADADA">Therefore, soy is not OK to eat.</font>
<br />
<b>(∴¬Fs)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Fx~5~3Hx)),(Hs)|=(~3Fs))

---

![[📂 Media/PDFs/Pasted image 20220917174011.png]]

---

### Ganglia Tho Hypothetical

>For example, say we had a braindead body on life support, and some people (like loved ones) who would experience emotional well-being if you took them off life support. Would you say "screw their emotions, we should apply the precautionary principle to that body's cerebral ganglia". I personally wouldn't. I'd take the wishes of those people to supersede keeping the ganglia alive. Likewise, I can't think of a good reason to deny people well-being from eating oysters.

### Links
#### Biomass Distribution
https://www.pnas.org/doi/10.1073/pnas.1711842115

#### Tumour Case Report
https://pubmed.ncbi.nlm.nih.gov/28042664/

#### Mollusc Sentience
[[📂 Media/PDFs/sentience-in-cephalopod-molluscs-and-decapod-crustaceans-final-report-november-2021.pdf]]

---

# Hashtags

#vegan 
#philosophy 
#sentience 
#bivalves
