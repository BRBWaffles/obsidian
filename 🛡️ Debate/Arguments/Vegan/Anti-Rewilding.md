### Anti-Rewilding

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                                                                                               |
|:-------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**W**</font>      | something (x) has negative rights                                                                                                                                                                       |
|      <font color="CC6600">**N**</font>      | we should defend something (x) from rights violations to any lesser degree than we would tolerate for humans                                                                                            |
|      <font color="CC6600">**F**</font>      | it is permissible to subject animals to conditions that are likely to involve predation, starvation, or death due to environmental exposure to any greater degree than we would tolerate for humans |
|      <font color="CC6600">**a**</font>      | animal                                                                                                                                                                                                  |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all things, if something has negative rights, then we should not defend it from rights violations to any lesser degree than we would tolerate for humans.
<br />
<font color="CC6600">
<b>(∀x(Wx→¬Nx))</b>
<br />
<b>P2)</b></font> Animals have negative rights.
<br />
<font color="CC6600">
<b>(Wa)</b>
<br />
<b>P3)</b></font> If we should not defend animals from rights violations to any lesser degree than we would tolerate for humans, then it is not permissible to subject animals to conditions that are likely to involve predation, starvation, or death due to environmental exposure to any greater degree than we would tolerate for humans.
<br />
<font color="CC6600">
<b>(¬Na→¬F)</b>
<br />
<b>P4)</b></font>it is not permissible to subject animals to conditions that are likely to involve predation, starvation, or death due to environmental exposure to any greater degree than we would tolerate for humans, then rewilding sentient animals is immoral.
<br />
<font color="CC6600">
<b>(¬F→R)</b>
<br />
<b>C)</b></font> Therefore, rewilding sentient animals is immoral.
<br />
<font color="CC6600">
<b>(∴R)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Wx~5~3Nx)),(Wa),(~3Na~5~3F)|=(~3F))

---

# Hashtags

#debate 
#arguments 
