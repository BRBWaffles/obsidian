### Flash's NTT Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                     |
|:-------------------------------------------:|:----------------------------------------------------------------------------- |
|    <font color="CC6600">**W(x)**</font>     | (x) has moral worth                                                           |
|    <font color="CC6600">**N(x)**</font>     | we should exploit (x) to any greater degree than we would tolerate for humans |
|   <font color="CC6600">**A(y,x)**</font>    | a (y) that is absent in (x)                                                   |
|      <font color="CC6600">**x**</font>      | a being                                                                       |
|      <font color="CC6600">**y**</font>      | trait                                                                         |
|      <font color="CC6600">**a**</font>      | animal                                                                        |
|      <font color="CC6600">**h**</font>      | human                                                                         |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all things, if a being has moral worth, then we should not exploit it to any greater degree than we would tolerate for humans.
<br />
<font color="CC6600">
<b>(∀x(Wx→Nx))</b>
<br />
<b>P2)</b></font> If animals don’t have moral worth, then there exists a trait that is absent in animals such that if it were absent in humans, humans wouldn’t have moral worth.
<br />
<font color="CC6600">
<b>(¬Wa→∃y(Aya→(Ayh→¬Wh)))</b>
<br />
<b>P3)</b></font> There doesn’t exist a trait that is absent in animals such that if it were absent in humans, humans wouldn’t have moral worth.
<br />
<font color="CC6600">
<b>(¬∃y(Aya→(Ayh→¬Wh)))</b>
<br />
<b>C)</b></font> Therefore, we should not exploit animals to any greater degree than we would tolerate for humans.
<br />
<font color="CC6600">
<b>(∴Na)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Wx~5Nx)),(~3Wa~5~7t(Ata~5(Ath~5~3Wh))),(~3~7t(Ata~5(Ath~5~3Wh))),(Wa)|=(Na))

---

# Vegan Society Definition Tho

| <font color="CC6600">**Defineindum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|        <font color="CC6600">V</font>        | it is vegan to do something (x)           |
|        <font color="CC6600">E</font>        | something (x) exploits animals (y)        |
|        <font color="CC6600">A</font>        | a being (x) is an animal                  |
|        <font color="CC6600">e</font>        | eat                                       |
|        <font color="CC6600">g</font>        | Groot                                     |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all things, it is vegan to do something if and only if that thing does not exploit animals.
<br />
<font color="CC6600">
<b>(∀x(Vx↔¬Ex))</b>
</font>
<br />
<font color="CC6600">
<b>P2)</b></font> If some beings are not animals, then eating those beings does not exploit animals.
<br />
<font color="CC6600">
<b>(∀x∀y(¬Ax→¬E(e(y))))</b>
</font>
<br />
<font color="CC6600">
<b>P3)</b></font> Groot is not an animal.
<br />
<font color="CC6600">
<b>(¬Ag)</b>
</font>
<br />
<font color="CC6600">
<b>C)</b></font> Therefore, it is vegan to eat Groot.
<br />
<font color="CC6600">
<b>(∴V(e(g)))</b>
</font>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Vx~4~3Ex)),(~6x~6y(~3Ax~5~3E(e(y)))),(~3Ag)|=(V(e(g))))

---

# Nick's Definition of Veganism

>Veganism is an applied ethical position that advocates for the equal, trait-adjusted application of commonplace human rights (such as the UN's Universal Declaration of Human Rights) to non-human sentient beings.

#### Terms:

**Sentient being:** an entity for whom a subjective experience can be reasonably argued, such as with vertebrate animals.

**Right:** an entitlement to an action, that if not performed, or an inaction, that if performed, would be bad in principle (meaning independent of utility concerns). For example, if others perform an action that deprives me of X, or fail to take an action necessary for me to have X, it would be considered wrong in principle, independent of the consequences or utility of such actions or inactions.

---

# Hashtags

#arguments 
#philosophy
#propositional_logic 
#logic 
#ethics 
#animals 
#vegan 