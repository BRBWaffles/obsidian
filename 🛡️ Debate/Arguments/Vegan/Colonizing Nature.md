## Non-Violence Displacement of Nature

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the natural world contains intolerable rights violations and we do not know if nature is net positive or net negative for utility and there is not any known practical means by which to end the rights violations beyond the use of force and the natural world is not currently instrumentally vital to facilitating human flourishing, then we are justified in displacing nature into non-existence.
<br />
<font color="CC6600">
<b>(P∧¬Q∧¬R∧¬S→T)</b>
<br />
<b>P2)</b></font> Nature entails intolerable rights violations.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> We do not know if nature is net positive or net negative for utility.
<br />
<font color="CC6600">
<b>(¬Q)</b>
<br />
<b>P4)</b></font> There is not any known practical means by which to end the rights violations beyond the use of force.
<br />
<font color="CC6600">
<b>(¬R)</b>
<br />
<b>P5)</b></font> The natural world is not currently instrumentally vital to facilitating human flourishing.
<br />
<font color="CC6600">
<b>(¬S)</b>
<br />
<b>C)</b></font> Therefore, we are justified in displacing nature into non-existence.
<br />
<font color="CC6600">
<b>(∴T)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~1~3Q~1~3R~1~3S~5T),(P),(~3Q),(~3R),(~3S)|=(T))

---

# Hashtags

#debate 
#arguments 
#vegan 
