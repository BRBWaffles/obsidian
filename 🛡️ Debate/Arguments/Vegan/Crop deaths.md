
### Crop Deaths Rebuttal

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If it is known that cropland leads to more animal death than wildland, then there is evidence that cropland leads to more animal death than wildland.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> There is not evidence that cropland leads to more animal death than wildland.
<br />
<font color="CC6600">
<b>(¬Q)</b>
<br />
<b>C)</b></font> Therefore, it is not known that cropland leads to more animal death than wildland.
<br />
<font color="CC6600">
<b>(∴¬P)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(~3Q)|=(~3P))

---

### Cropland vs Pastureland
https://onlinelibrary.wiley.com/doi/epdf/10.1111/ecog.05126

### Cropdeaths Article
https://anupamkatkar.com/2015/10/08/debunking-cultivating-crops-for-vegans-kills-more-animals-than-pasture-grazing-livestock/#Footnote2

---

# Hashtags

#arguments 