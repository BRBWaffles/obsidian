## Obligatory Activism

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                 |
|:-------------------------------------------:|:--------------------------------------------------------- |
|      <font color="CC6600">**C**</font>      | one has an obligation to convince others of something (x) |
|      <font color="CC6600">**M**</font>      | something (x) is a moral obligation                       |
|      <font color="CC6600">**v**</font>      | veganism                                                  |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all things, one has an obligation to convince others of an action (x) if, and only if, an action (x) is a moral obligation.
<br />
<font color="CC6600">
<b>(∀x(Cx↔Mx))</b>
<br />
<b>P2)</b></font> Veganism is a moral obligation.
<br />
<font color="CC6600">
<b>(Mv)</b>
<br />
<b>C)</b></font> Therefore, one has an obligation to convince others of veganism.
<br />
<font color="CC6600">
<b>(∴Cv)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Cx~4Mx)),(Mv)|=(Cv))

#### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                 |
|:-------------------------------------------:|:--------------------------------------------------------- |
|      <font color="CC6600">**C**</font>      | one has an obligation to convince others of something (x) |
|      <font color="CC6600">**M**</font>      | something (x) is a moral obligation                       |
|      <font color="CC6600">**r**</font>      | not raping others                                         | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> One has an obligation to convince others of something if, and only, if that something is a moral obligation.
<br />
<font color="CC6600">
<b>(∀x(Cx↔Mx))</b>
<br />
<b>P2)</b></font> Not raping others is a moral obligation.
<br />
<font color="CC6600">
<b>(Mr)</b>
<br />
<b>C)</b></font> Therefore, one has an obligation to convince others to not rape others.
<br />
<font color="CC6600">
<b>(∴Cr)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Cx~4Mx)),(Mr)|=(Cr))

---

# Hashtags

#debate 
#arguments 
#clownery 