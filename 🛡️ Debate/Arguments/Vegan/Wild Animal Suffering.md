### A) Wild animal suffering should end.

>All else equal, is a world where sentient beings are getting torn to shreds and eaten alive or starving to death if they don't succumb to this fate better than a world where this does not happen?

### B) Wild animal procreation should end.

>All else equal, is a world where sentient beings are fucking (often without even implied consent) and impregnating each other without informed consent with respect to the risks of pregnancy, birth defects from inbreeding, or the extremely high risk that their children will be torn to shreds and eaten alive or starve to death better than a world where this does not happen?

### C) We favour less risky environments.

>All else equal, is a world where tens of thousands are killed, hundreds of thousands are injured or maimed, and up to millions are displaced or rendered homeless every year due to natural disasters alone better than a world where this does not happen?

---

# Hashtags

#debate 
#arguments 
#animals 