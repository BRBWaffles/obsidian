| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                             |
|:-------------------------------------------:|:----------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**P**</font>      | something (x) counts as predation                                                                     |
|      <font color="CC6600">**Q**</font>      | something (x) involves an animal (y) regularly capturing another animal (z)                           |
|      <font color="CC6600">**R**</font>      | something (x) involves an animal (y) regularly killing another animal (z)                             |
|      <font color="CC6600">**S**</font>      | something (x) involves an animal (y) regularly subduing another animal (z)                            | 
|      <font color="CC6600">**T**</font>      | something (x) involves an animal (y) regularly consuming, to the captor's benefit, another animal (z) |
|      <font color="CC6600">**a**</font>      | animal agriculture                                                                                    |
|      <font color="CC6600">**h**</font>      | humans                                                                                                |
|      <font color="CC6600">**l**</font>      | livestock                                                                                             |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something counts as predation if, and only if, something involves an animal regularly capturing, killing or subduing, and consuming, to the captor's benefit, another animal.
<br />
<font color="CC6600">
<b>(∀x∀y∀z(Px↔Qxyz∧(Rxyz∨Sxyz)∧Txyz))</b>
<br />
<b>P2)</b></font> Animal agriculture involves humans regularly capturing livestock.
<br />
<font color="CC6600">
<b>(Qahl)</b>
<br />
<b>P3)</b></font> Animal agriculture involves humans regularly killing or subduing livestock.
<br />
<font color="CC6600">
<b>(Rahl∨Sahl)</b>
<br />
<b>P4)</b></font> Animal agriculture involves humans regularly consuming, to the captors' benefit, livestock.
<br />
<font color="CC6600">
<b>(Tahl)</b>
<br />
<b>C)</b></font> Therefore, animal agriculture counts as predation.
<br />
<font color="CC6600">
<b>(∴Pa)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y~6z(Px~4Qxyz~1(Rxyz~2Sxyz)~1Txyz)),(Qahl),(Rahl~2Sahl),(Tahl)|=(Pa))

---

### Ethical Factory Farming Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                         |
|:-------------------------------------------:|:--------------------------------------------------------------------------------- |
|      <font color="CC6600">**D**</font>      | a predator (x) engages in predation (y)                                           |
|      <font color="CC6600">**R**</font>      | it is the case that we ought stop the predator (x) from engaging in predation (y) | 
|      <font color="CC6600">**a**</font>      | human                                                                             |
|      <font color="CC6600">**m**</font>      | factory farming pigs                                                              |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a predator engages in predation, then it is not the case that we ought stop the predator from engaging in said predation.
<br />
<font color="CC6600">
<b>(∀x∀y(Dxy→¬Rxy))</b>
<br />
<b>P2)</b></font> Humans engage in factory farming pigs.
<br />
<font color="CC6600">
<b>(Dam)</b>
<br />
<b>C)</b></font> Therefore, it is not the case that we ought stop humans from engaging in factory farming pigs.
<br />
<font color="CC6600">
<b>(∴¬Ram)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Dxy~5~3Rxy)),(Dam)|=(~3Ram))

---

# Hashtags

#debate 
#arguments  
#predators 