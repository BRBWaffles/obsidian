### Dairy Cattle Rape Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|    <font color="CC6600">**P(x)**</font>     | (x) qualifies as rape                     |
|   <font color="CC6600">**Q(x,y)**</font>    | (x)  involves sexual contact with (y)     |
|   <font color="CC6600">**R(y,x)**</font>    | (y) renders informed consent for (x)      |
|      <font color="CC6600">**x**</font>      | An action                                 |
|      <font color="CC6600">**y**</font>      | an involved party                         |
|      <font color="CC6600">**a**</font>      | bulls mating with cows                    |
|      <font color="CC6600">**e**</font>      | a cow                                     |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> An action qualifies as rape if, and only if, the action involves sexual contact with an involved party and the involved party does not render informed consent for the action.
<br />
<font color="CC6600">
<b>(∀x∀y(Px↔(Qxy∧¬Ryx)))</b>
<br />
<b>P2)</b></font> A bull mating with a cow involves sexual contact with a cow.
<br />
<font color="CC6600">
<b>(Qae)</b>
<br />
<b>P3)</b></font> A cow does not render informed consent to a bull mating with a cow.
<br />
<font color="CC6600">
<b>(¬Rea)</b>
<br />
<b>C)</b></font> Therefore, a bull mating with a cow qualifies as rape.
<br />
<font color="CC6600">
<b>(∴Pa)</b>
<br />
<br />
</font>
</div>

---

#debate 
#arguments 
#vegan 
#dairy 