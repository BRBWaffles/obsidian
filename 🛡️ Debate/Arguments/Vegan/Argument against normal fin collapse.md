### Argument for Fin Collapse Abnormality

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a killer whale with a collapsed dorsal fin is healthy, then collapsed dorsal fins are normal.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Collapsed dorsal fins are not normal.
<br />
<font color="CC6600">
<b>(¬Q)</b>
<br />
<b>C)</b></font> Therefore, a killer whale with a collapsed dorsal fin is not healthy.
<br />
<font color="CC6600">
<b>(∴¬P)</b>
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(~3Q)|=(~3P))

### Argument for P2

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If empirical data shows that most killer whales do not succumb to dorsal fin collapse, then collapsed dorsal fins are not normal.
<br />
<font color="CC6600">
<b>(¬R→¬Q)</b>
<br />
<b>P2)</b></font> Empirical data shows that most killer whales do not succumb to dorsal fin collapse.
<br />
<font color="CC6600">
<b>(¬R)</b>
<br />
<b>C)</b></font> Therefore, collapsed dorsal fins are not normal.
<br />
<font color="CC6600">
<b>(∴¬Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~3R~5~3Q),(~3R)|=(~3Q))

---

# Hashtags

#debate 
#arguments 
#vegan 
#orca 