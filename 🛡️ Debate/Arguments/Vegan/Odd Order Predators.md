### Culling Odd Order Predators

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                    |
|:-------------------------------------------:|:------------------------------------------------------------------------------------------------------------ |
|      <font color="CC6600">**W**</font>      | something (x) has negative rights                                                                            |
|      <font color="CC6600">**N**</font>      | we should defend something (x) from rights violations to any lesser degree than we would tolerate for humans | 
|      <font color="CC6600">**F**</font>      | it is permissible to prevent predation with lethal force to the same degree we would tolerate for humans     |
|      <font color="CC6600">**a**</font>      | animal                                                                                                       |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all things, if something has negative rights, then we should not defend it from rights violations to any lesser degree than we would tolerate for humans.
<br />
<font color="CC6600">
<b>(∀x(Wx→¬Nx))</b>
<br />
<b>P2)</b></font> Animals have negative rights.
<br />
<font color="CC6600">
<b>(Wa)</b>
<br />
<b>P3)</b></font> If we should not defend animals from rights violations to any lesser degree than we would tolerate for humans, then it is permissible to prevent predation with lethal force to the same degree we would tolerate for humans.
<br />
<font color="CC6600">
<b>(¬Na→F)</b>
<br />
<b>C)</b></font> Therefore, it is permissible to prevent predation with lethal force to the same degree we would tolerate for humans.
<br />
<font color="CC6600">
<b>(∴F)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Wx~5~3Nx)),(Wa),(~3Na~5F)|=(F))

---

# Hashtags

#debate 
#arguments 
#vegan 
#predators 
