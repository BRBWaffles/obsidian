### Argument Against Marineland Vandalism

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If Marineland must spend extra money cleaning graffiti, then Marineland will have less money to devote to animal care.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Marineland must spend extra money cleaning graffiti.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> If Marineland will have less money to devote to animal care, then vandalizing Marineland increases the probability of harming the animals they keep.
<br />
<font color="CC6600">
<b>(Q→R)</b>
<br />
<b>C)</b></font> Therefore, vandalizing Marineland increases the probability of harming the animals they keep.
<br />
<font color="CC6600">
<b>(∴R)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P),(Q~5R)|=(R))

---

# Hashtags

#debate 
#arguments 
#vegan 
#orca