### Argument Against Israeli Pagers as Booby-Traps

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                                                        |
|:-------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**B**</font>      | (x) is a booby-trap                                                                                                                                              |
|      <font color="CC6600">**D**</font>      | (x) is a device or material                                                                                                                                      |
|      <font color="CC6600">**K**</font>      | (x) is designed, constructed, or adapted to kill or injure.                                                                                                      |
|      <font color="CC6600">**F**</font>      | (x) functions (as a device or material that was designed, constructed, or adapted to kill or injure) when a person disturbs or approaches an apparently safe act |
|      <font color="CC6600">**p**</font>      | pagers                                                                                                                                                           |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all x, x is a booby-trap if, and only if, x is a device or material AND x is designed, constructed, or adapted to kill or injure AND x functions (as a device or material that was designed, constructed, or adapted to kill or injure) when a person disturbs or approaches an apparently safe act.
<br />
<font color="CC6600">
<b>(∀x(Bx↔(Dx∧Kx∧Fx)))</b>
<br />
<b>P2)</b></font> The pagers were devices or materials.
<br />
<font color="CC6600">
<b>(Dp)</b>
<br />
<b>P3)</b></font> The pagers were designed, constructed, or adapted to kill or injure.
<br />
<font color="CC6600">
<b>(Kp)</b>
<br />
<b>P4)</b></font> The pagers did not function (as devices or materials that were designed, constructed, or adapted to kill or injure) when a person disturbs or approaches an apparently safe act.
<br />
<font color="CC6600">
<b>(¬Fp)</b>
<br />
<b>C)</b></font> Therefore, the pagers were not booby-traps.
<br />
<font color="CC6600">
<b>(∴¬Bp)</b>
<br />
<br />
</font>
</div>

# Hashtags

#debate 
#arguments 
#israel-palestine