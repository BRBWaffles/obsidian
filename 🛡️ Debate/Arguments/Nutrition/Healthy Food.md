### Healthy Food Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                                                                           |
|:-------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**P**</font>      | A given food (x) is healthy compared to another given food (w) when in a given context (y)                                                                                          |
|      <font color="CC6600">**Q**</font>      | when within that context (y) the consumption of that given food (x) is likely to increase the lag-time to the onset of illness, disease, or infirmity (z) when replacing another given other food (w) |
|      <font color="CC6600">**p**</font>      | Pepsi                                                                                                                                                                               |
|      <font color="CC6600">**d**</font>      | trapped on a desert island                                                                                                                                                          |
|      <font color="CC6600">**s**</font>      | starvation                                                                                                                                                                          |
|      <font color="CC6600">**l**</font>      | leafy greens                                                                                                                                                                        |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> A given food is healthy compared to another given food when in a given context if, and only if, when within that context the consumption of that given food is likely to increase the lag-time to the onset of illness, disease, or infirmity when replacing a given other food.
<br />
<font color="CC6600">
<b>(∀x∀y∀z∀w(Pxyw↔Qxyzw))</b>
<br />
<b>P2)</b></font> When trapped on a desert island the consumption of Pepsi increases the lag-time to the onset of starvation when replacing leafy greens.
<br />
<font color="CC6600">
<b>(Qpdsl)</b>
<br />
<b>C)</b></font> Therefore, Pepsi is healthy compared to leafy greens when trapped on a desert island.
<br />
<font color="CC6600">
<b>(∴Ppdl)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y~6z~6w(Pxyw~4Qxyzw)),(Qpdsl)|=(Ppdl))

---

# Hashtags

#debate 
#arguments 