### Epi-Denial Consistency Checker

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If experimental evidence is required for a sound causal inference and confounding is a causal concept, then experimental evidence is required to validate potential confounders.
<br />
<font color="CC6600">
<b>(P∧Q→R)</b>
<br />
<b>P2)</b></font> Experimental evidence is required for a sound causal inference.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> Confounding is a causal concept.
<br />
<font color="CC6600">
<b>(Q)</b>
<br />
<b>C)</b></font> Therefore, experimental evidence is required to validate potential confounders.
<br />
<font color="CC6600">
<b>(∴R)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~1Q~5R),(P),(Q)|=(R))

---

# Hashtags

#debate 
#arguments 