### Title

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                                             |
|:-------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**S**</font>      | a given saturated fat source (x) increases LDL-C at a similar rate to other saturated fat sources                                                     |
|      <font color="CC6600">**L**</font>      | a given saturated fat source (x) has no other known qualities that counteract said increase in LDL-C                                                  |
|      <font color="CC6600">**R**</font>      | there is a reason to believe that such a saturated fat source (x) is any better for heart health than other saturated fat sources that increase LDL-C |
|      <font color="CC6600">**c**</font>      | coconut oil                                                                                                                                           |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a given saturated fat source increases LDL-C at a similar rate to other saturated fat sources and and a given saturated fat source has no other known qualities that counteract said increase in LDL-C, then there is not a reason to believe that such a saturated fat source is any better for heart health than other saturated fat sources that increase LDL-C.
<br />
<font color="CC6600">
<b>(∀x(Sx∧Lx→¬Rx))</b>
<br />
<b>P2)</b></font> Coconut oil increases LDL-C at a similar rate to other saturated fat sources
<br />
<font color="CC6600">
<b>(Sc)</b>
<br />
<b>P3)</b></font> Coconut oil has no other known qualities that counteract said increase in LDL-C.
<br />
<font color="CC6600">
<b>(Lc)</b>
<br />
<b>C)</b></font> Therefore, there is not a reason to believe that coconut oil is any better for heart health than other saturated fat sources that increase LDL-C.
<br />
<font color="CC6600">
<b>(∴¬Rc )</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Sx~1Lx~5~3Rx)),(Sc),(Lc)|=(~3Rc))

---

# Hashtag

#saturated_fat 
#LDL 
#coronary_heart_disease 
#arguments 