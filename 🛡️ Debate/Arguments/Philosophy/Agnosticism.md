### Agnosticism Argument

| <font color="CC6600">**Variable**</font> | <font color="CC6600">**Definition**</font>                                                                |
|:----------------------------------------:|:--------------------------------------------------------------------------------------------------------- |
|    <font color="CC6600">**U**</font>     | one (x) can unpack what evidence would lead them to change their doxastic attitude on a proposition (y) |
|    <font color="CC6600">**K**</font>     | one (x) knows why they believe that a proposition (y) is true                                           |
|    <font color="CC6600">**W**</font>     | one (x) should temporarily withhold the belief that a proposition (y) is true                           |
|    <font color="CC6600">**o**</font>     | the interlocutor                                                                                                     |
|    <font color="CC6600">**r**</font>     | the proposition at hand                                                                                           | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If one cannot unpack what evidence would lead them to change their doxastic attitude on a proposition, then one does does not know why they believe that a proposition is true.
<br />
<font color="CC6600">
<b>(∀x∀y(¬Uxy→¬Kxy))</b>
<br />
<b>P2)</b></font> If one does not know why they believe that a proposition is true, then one should temporarily withhold the belief that a proposition is true.
<br />
<font color="CC6600">
<b>(∀x∀y(¬Kxy→Wxy))</b>
<br />
<b>P3)</b></font> The interlocutor  cannot unpack what evidence would lead them to change their doxastic attitude on the proposition at hand.
<br />
<font color="CC6600">
<b>(¬Uor)</b>
<br />
<b>C)</b></font> Therefore, the interlocutor should temporarily withhold the belief that the proposition at hand is true.
<br />
<font color="CC6600">
<b>(∴Wor)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(~3Uxy~5~3Kxy)),(~6x~6y(~3Kxy~5Wxy)),(~3Uor)|=(Wor))

## Hashtags

#arguments 
#philosophy 
#debate 