## Modus Bronens

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If studies show that PUFA is preferable to SFA for heart disease prevention, then people who wish to lower their heart disease risk should generally lower their SFA intake and increase their PUFA intake.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> I don't like Nick's face.
<br />
<font color="CC6600">
<b>(¬N)</b>
<br />
<b>C)</b></font> Therefore, studies do not show that PUFA is preferable to SFA for heart disease prevention and people who wish to lower their heart disease risk should not generally lower their SFA intake and increase their PUFA intake.
<br />
<font color="CC6600">
<b>(∴¬P∧¬Q)</b>
<br />
<br />
</font>
</div>

---

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If LDL causes CVD, then those whose LDL increases in response to a carnivore diet should take steps to lower their LDL.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Nick is not the epitome of health.
<br />
<font color="CC6600">
<b>(¬N)</b>
<br />
<b>C)</b></font> Therefore, LDL does not cause CVD and those whose LDL increases in response to a carnivore diet should not take steps to lower their LDL.
<br />
<font color="CC6600">
<b>(∴¬P∧¬Q)</b>
<br />
<br />
</font>
</div>

---

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If Anna does act in bad faith, then Nick is justified in blocking Anna.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> I don't like the fact that Nick uses pragmatic markers in his discourse.
<br />
<font color="CC6600">
<b>(¬N)</b>
<br />
<b>C)</b></font> Therefore, Anna does not act in bad faith and Nick is not justified in blocking Anna.
<br />
<font color="CC6600">
<b>(∴¬P∧¬Q)</b>
<br />
<br />
</font>
</div>

---

## Hashtags

#clownery 
#philosophy 
#logic 
#propositional_logic 