# Debate 1

### Human Experiment Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                           |
|:-------------------------------------------:|:------------------------------------------------------------------- |
|      <font color="CC6600">**V**</font>      | human studies (x) are vulnerable to bias and small effect sizes         |
|      <font color="CC6600">**D**</font>      | human studies (x) can demonstrate causality or support causal inference |
|      <font color="CC6600">**G**</font>      | human studies (x) are garbage                                                         |
|      <font color="CC6600">**e**</font>      | human experiments                                                   | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If human studies are vulnerable to bias and small effect sizes, then human studies cannot demonstrate causality or support causal inference.
<br />
<font color="CC6600">
<b>(∀x(Vx→¬Dx))</b>
<br />
<b>P2)</b></font> Human experiments are vulnerable to bias and small effect sizes.
<br />
<font color="CC6600">
<b>(Ve)</b>
<br />
<b>P3)</b></font> If human experiments cannot demonstrate causality or support causal inference, then human experiments are garbage.
<br />
<font color="CC6600">
<b>(¬De→Ge)</b>
<br />
<b>C)</b></font> Therefore, human experiments are garbage.
<br />
<font color="CC6600">
<b>(∴Ge)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Vx~5~3Dx)),(Ve),(~3De~5Ge)|=(Ge))

---

# Hashtags

#clownery 
#clowns  
#epidemiology 
#debate_opponents 
#debate 