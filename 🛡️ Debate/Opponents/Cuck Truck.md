# Debate Invitations and Responses

## Ken Berry
- **Social:** [KenDBerryMD](https://twitter.com/KenDBerryMD)
- **Circumstances:** Multiple debate invitations extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1366524889067368449?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended and dodged via X. [Link](https://twitter.com/TheNutrivore/status/1539720236701589504?s=20)
  2. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1539721408372039680?s=20)

## Cliff Harvey
- **Social:** [CarbAppropriate](https://twitter.com/CarbAppropriate)
- **Circumstances:** Debate invitation accepted via email and later declined via X.
- **Evidence:** [Link](https://twitter.com/CarbAppropriate/status/1372281626206507010?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1500650543886204929?s=20)

## Diana Rodgers
- **Social:** [sustainabledish](https://twitter.com/sustainabledish)
- **Circumstances:** Debate invitation extended via Instagram and presumed declined due to being deleted.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1365857401786814465?s=20)

## Dave Feldman
- **Social:** [DaveKeto](https://twitter.com/DaveKeto)
- **Circumstances:** Debate invitation conditionally accepted via X and later declined.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1306625219440730118?s=20)

## Cate Shanahan
- **Social:** [drcateshanahan](https://twitter.com/drcateshanahan)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1266438463634632709?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1479497980570857474?s=20)
  2. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1487215337116508162?s=20)
  3. Debate invitation extended via X with no response. [Link](https://twitter.com/drcateshanahan/status/1516904406805057537?s=20)
  4. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1537544882532716544?s=20)

## Paul Mason
- **Social:** [DrPaulMason](https://twitter.com/DrPaulMason)
- **Circumstances:** Nominated for debate with no follow-up.
- **Evidence:** [Link](https://twitter.com/Tom_Babington1/status/1361644276866830337?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/IdanOnTweeter/status/1373233980594618371?s=20)
  2. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1461361726943760391?s=20)

## Ivor Cummins
- **Social:** [FatEmperor](https://twitter.com/FatEmperor)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1268600451693494273?s=20)
- **Repeat Offenses:**
  1. Debate invitation declined via X. [Link](https://twitter.com/FatEmperor/status/1636920650156724226?s=20)

## Paul Saladino
- **Social:** [paulsaladinomd](https://twitter.com/paulsaladinomd)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1275912849999695872?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/FoodFirst_Ty/status/1282847039596843009?s=20)
  2. Debate invitation extended via X with no response. [Link](https://twitter.com/FoodFirst_Ty/status/1283449495682904064?s=20)
  3. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1282851963902636032?s=20)
  4. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1469136550046814219?s=20)
  5. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1591543878699843584?s=20)
  6. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1633831762509045766?s=20)

## Kyle Mamounis
- **Social:** [Nutricrinology](https://twitter.com/Nutricrinology)
- **Circumstances:** Engaged in written debate via X but disengaged once cornered.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1389259133044477953?s=20)

## Tucker Goodrich
- **Social:** [TuckerGoodrich](https://twitter.com/TuckerGoodrich)
- **Circumstances:** Engaged in written debate via X and blocked when cornered.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1276694117494358017?s=19)
- **Repeat Offenses:**
  1. Debate invitation accepted via X and subsequently declined before blocking. [Link](https://twitter.com/TuckerGoodrich/status/1428062578668830720?s=20)
  2. Debate invitation declined via X. [Link](https://twitter.com/TuckerGoodrich/status/1469366622196359170?s=20)
  3. All further debate invitations preemptively declined via X. [Link](https://twitter.com/TuckerGoodrich/status/1470029816975872007?s=20)

## Amber O'Hearn
- **Social:** [KetoCarnivore](https://twitter.com/KetoCarnivore)
- **Circumstances:** Discussion invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/FusionProgGuy/status/1413888976281169922?s=20)

## Mike Mutzel
- **Social:** [MikeMutzel](https://twitter.com/MikeMutzel)
- **Circumstances:** Challenged an egregious assertion and was ignored.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1414457776982552576?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1584645853771616256?s=20)

## Tro Kalayjian
- **Social:** [DoctorTro](https://twitter.com/DoctorTro)
- **Circumstances:** Engaged in written debate via X and dodged repeatedly before blocking.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1261351818430255104?s=20)

## Zoe Harcombe
- **Social:** [zoeharcombe](https://twitter.com/zoeharcombe)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1418263706178310149)

## Ann Childers
- **Social:** [AnnChildersMD](https://twitter.com/AnnChildersMD)
- **Circumstances:** Engaged in written debate via X and repeatedly dodged when pressed for specifics.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1447245484356108292?s=20)

## James DiNicolantonio
- **Social:** [drjamesdinic](https://twitter.com/drjamesdinic)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1460788569388171268?s=20)

## Ralph Napolitano
- **Social:** [DrRalphNap](https://twitter.com/DrRalphNap)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1462794580848300034?s=20)

## Michael Kummer
- **Social:** [mkummer82](https://twitter.com/mkummer82)
- **Circumstances:** Debate invitation declined via X.
- **Evidence:** [Link](https://twitter.com/mkummer82/status/1465755847917715464?s=20)

## Kem Minnick
- **Social:** [kemminnick](https://twitter.com/kemminnick)
- **Circumstances:** Debate invitation accepted via X with no follow-up.
- **Evidence:** [Link](https://twitter.com/kemminnick/status/1469336100300726273?s=20)
- **Repeat Offenses:**
  1. Engaged in written debate via X and blocked when cornered. [Link](https://twitter.com/The_Nutrivore/status/1476990862793986052?s=20)

## Travis Statham
- **Social:** [Travis_Statham](https://twitter.com/Travis_Statham)
- **Circumstances:** Debate invitation extended via Reddit and ignored.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1461428023207489542?s=20)
- **Repeat Offenses:**
  1. Debate invitation declined via YouTube comments section. [Link](https://www.youtube.com/watch?v=QGNNsiINehI)
  2. Debate invitation extended via Reddit with no response. [Link](https://www.reddit.com/r/StopEatingSeedOils/comments/v457tu/how_vegetable_oils_make_us_fat_zero_acre/ic9u7wu/?utm_source=share)

## Raphael Sirtoli
- **Social:** [raphaels7](https://twitter.com/raphaels7)
- **Circumstances:** Debate invitation declined via X.
- **Evidence:** [Link](https://twitter.com/raphaels7/status/1475767357188579329?s=20)
- **Repeat Offenses:**
  1. All further debate invitations preemptively declined via X. [Link](https://x.com/raphaels7/status/1783541746015654072)

## Justin Mares
- **Social:** [jwmares](https://twitter.com/jwmares)
- **Circumstances:** Engaged in written debate via X and dodged when pressed for specifics.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1329266554089869312?s=20)

## Anthony Gustin
- **Social:** [dranthonygustin](https://twitter.com/dranthonygustin)
- **Circumstances:** Debate invitation accepted via X with no follow-up.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1482502242632552449?s=20)

## Jake Mey
- **Social:** [CakeNutrition](https://twitter.com/CakeNutrition)
- **Circumstances:** Engaged in written debate via X and dodged when cornered.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1490009495581298690?s=20)
- **Repeat Offenses:**
  1. Debate invitation declined via X. [Link](https://twitter.com/The_Nutrivore/status/1490060813138280450?s=20)
  2. Imaginary debate invite preemptively declined via X. [Link](https://twitter.com/The_Nutrivore/status/1495419256737091585?s=20)

## Max Lugavere
- **Social:** [maxlugavere](https://twitter.com/maxlugavere)
- **Circumstances:** Engaged in written debate via X and dodged when pressed for specifics.
- **Evidence:** [Link](https://twitter.com/maxlugavere/status/1517683567249149953?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1518307089742848000?s=20)
  2. Debate invitation declined via X. [Link](https://twitter.com/maxlugavere/status/1518367365683064833?s=20)

## Tristan Haggard
- **Social:** [Trxstxn4](https://twitter.com/Trxstxn4)
- **Circumstances:** Debate invitation extended via X and presumed declined due to repeated dodges.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1513596227052527621?s=20)

## Joseph Everett
- **Social:** [JEverettLearned](https://twitter.com/JEverettLearned)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1535334822825971712?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1555138038740680704?s=20)

## Brad Campbell
- **Social:** [DrBradCampbell](https://twitter.com/DrBradCampbell)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1530933999610171392?s=20)

## Kevin Stock
- **Social:** [kevinstock12](https://twitter.com/kevinstock12)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1530140722225102848?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/The_Nutrivore/status/1534927799911317509?s=20)

## Carnivore Aurelius
- **Social:** [AlpacaAurelius](https://twitter.com/AlpacaAurelius)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1538272143061815299?s=20)

## Bart Kay
- **YouTube:** [BartKayNutritionScienceWatchdog](https://www.youtube.com/c/BartKayNutritionScienceWatchdog/)
- **Circumstances:** Debate invitation declined via email.
- **Evidence:** [Link](https://www.youtube.com/watch?v=M7vTJ02xxrw)
- **Repeat Offenses:**
  1. Debate invitation accepted via email with no follow-up. [Link](https://www.youtube.com/watch?v=M7vTJ02xxrw)

## Nick Eggleton
- **Social:** [NickEggleton](https://twitter.com/NickEggleton)
- **Circumstances:** Debate invitation declined via X.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1266626885703720961?s=20)

## Marty Kendall
- **Social:** [martykendall2](https://twitter.com/martykendall2)
- **Circumstances:** Engaged in written debate via X and blocked when cornered.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1310548994804809729)

## Rob Meijer
- **Social:** [EngineerDiet](https://twitter.com/EngineerDiet)
- **Circumstances:** Engaged in written debate via X and repeatedly dodged when pressed for specifics.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1435850829051793408?s=20)

## Ashwani Garg
- **Social:** [agargmd](https://twitter.com/agargmd)
- **Circumstances:** Engaged in written debate via X and repeatedly dodged when cornered.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1443328350982967303?s=20)

## Brian Kerley
- **Social:** [SeedOilDsrspctr](https://twitter.com/SeedOilDsrspctr)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1483889771134926849?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://x.com/TheNutrivore/status/1783200352646619227)

## Adam Pollock
- **Social:** [aIIegoricaI](https://twitter.com/aIIegoricaI)
- **Circumstances:** Debate invitation declined via X.
- **Evidence:** [Link](https://twitter.com/aIIegoricaI/status/1495786414402945033?s=20)

## Kait Malthaner
- **Social:** [healthcoachkait](https://twitter.com/healthcoachkait)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1503258756687306753?s=20)

## Michael Manderville
- **Social:** [MikeManderville](https://twitter.com/MikeManderville)
- **Circumstances:** Engaged in written debate via X and dodged when pressed for specifics.
- **Evidence:** [Link](https://twitter.com/The_Nutrivore/status/1503036463306489856?s=20)

## Philip Ovadia
- **Social:** [ifixhearts](https://twitter.com/ifixhearts)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1557791520324890624?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1558282337024180224?s=20)

## Nina Teicholz
- **Social:** [bigfatsurprise](https://twitter.com/bigfatsurprise)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1556510529014882305?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1467571763865210881?s=20)
  2. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1538895943050878977?s=20)

## Norstrong Chris
- **Social:** [NNMChris](https://twitter.com/NNMChris)
- **Circumstances:** Engaged in written debate via X and repeatedly dodged when cornered.
- **Evidence:** [Link](https://twitter.com/NorstrongHealth/status/1416939281973530626?s=20)
- **Repeat Offenses:**
  1. Debate invitation declined via X. [Link](https://twitter.com/The_Nutrivore/status/1534573919562350594?s=20)
  2. Debate invitation extended via X with no response. [Link](https://twitter.com/TheNutrivore/status/1534614710196260865?s=20)

## Clemens Zsófia
- **Social:** [ClemensZsofia](https://twitter.com/ClemensZsofia)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1528377198180290561?s=20)

## Elie Jarrouge
- **Social:** [ElieJarrougeMD](https://twitter.com/ElieJarrougeMD)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1506845469980315648?s=20)

## David Gornoski
- **Social:** [DavidGornoski](https://twitter.com/DavidGornoski)
- **Circumstances:** Debate declined via X.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1525474063044550657?s=20)

## Mark Sisson
- **Social:** [Mark_Sisson](https://twitter.com/Mark_Sisson)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1488360227141419009?s=20)

## Adam Singer
- **Social:** [AdamSinger](https://twitter.com/AdamSinger)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1566491269194719232?s=20)

## Benny Malone
- **Social:** [bennymaloneUK](https://twitter.com/bennymaloneUK)
- **Circumstances:** Engaged in written debate via X and dodged when pressed for specifics.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1576601875314487296?s=20)

## David Diamond
- **Social:** [LDLSkeptic](https://twitter.com/LDLSkeptic)
- **Circumstances:** Debate invitation declined via X.
- **Evidence:** [Link](https://twitter.com/LDLSkeptic/status/1583471298306375681?s=20)
- **Repeat Offenses:**
  1. All further debate invitations preemptively declined via X. [Link](https://twitter.com/LDLSkeptic/status/1583481964840902656?s=20)

## Ben Bikman
- **Social:** [BenBikmanPhD](https://twitter.com/BenBikmanPhD)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1587547250074746880?s=19)

## Gary Taubes
- **Social:** [garytaubes](https://twitter.com/garytaubes)
- **Circumstances:** Debate invitation declined via X.
- **Evidence:** [Link](https://twitter.com/garytaubes/status/1595180467552018432?s=20)

## Robb Wolf
- **Social:** [robbwolf](https://twitter.com/robbwolf)
- **Circumstances:** Debate invitation declined via X.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1601624559647875072?s=20)

## Eric Sartori
- **Social:** [CarnAtheist](https://twitter.com/CarnAtheist)
- **Circumstances:** Debate invitation declined via X.
- **Evidence:** [Link](https://twitter.com/CarnAtheist/status/1635015631627235328?s=20)

## Tart Vader
- **Discord:** [User Profile](https://discordlookup.com/user/600111075135848449#1989)
- **Circumstances:** Debate invitation declined via Discord.
- **Evidence:** ⁠🌱┃veganism⁠

## Brian Kateman
- **Social:** [BrianKateman](https://twitter.com/BrianKateman)
- **Circumstances:** Debate invitation accepted and subsequently declined via email.
- **Evidence:** [Link](https://twitter.com/BrianKateman/status/1645897476325482497?s=20)

## Edward Goeke
- **Social:** [GoekeEddie](https://twitter.com/GoekeEddie)
- **Circumstances:** Debate invitation extended via X with no response.
- **Evidence:** [Link](https://x.com/TheNutrivore/status/1773421625150746784?s=20)
- **Repeat Offenses:**
  1. Debate invitation extended via X with no response. [Link](https://x.com/TheNutrivore/status/1774154640403423359?s=20)

## Mike Sweeney
- **Social:** [thelowcarb_rd](https://twitter.com/thelowcarb_rd)
- **Circumstances:** Debate invitation extended via X and presumed declined due to blocking.
- **Evidence:** [Link](https://twitter.com/TheNutrivore/status/1776652108646821913?t=Pf4sfAC1Z0pAcxGfQFr7oQ)

