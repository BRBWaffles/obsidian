# Debate 1

## Proposition

>"veganism, which rejects the use of animals for nourishment, is contrary to the natural order and the purpose of animals as defined by natural law"

## Analysis

The argument [wasn't even valid](https://www.youtube.com/watch?v=c6DlpjsRLI0).

## Receipts

![[Pasted image 20240711160954.png]]
![[Pasted image 20240711161029.png]]

---

# Hashtags

#debate 
#debate_opponents 
#vegan 
#clowns 
#clownery 
