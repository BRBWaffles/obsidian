# Debate 1

### Debate Proposition:

> "100% Carnivore diet is the appropriate and best health choice for all people."

1) What does "appropriate" mean? In relation to what?
	- 
2) What does "best" mean?
	- 
3) What does "health" mean? Some endpoint?
	- 
4) What constitutes a 100% carnivore diet?
	- 

### Clarified Debate Proposition:

> "Z is X and Y W choice for all people."

1) Ask if this is a scientific claim.
	- 

### Line of Questioning:

1) What's the evidence?
	- " "
2) What is the argument that is this evidence is more expected on the hypothesis than the negation of the hypothesis?
	- An argument is required, because maybe the "evidence" is less or equally expected on the proposition (which would not be evidence for the proposition)
3) If he does provide an argument, examine the premises carefully
	- If a premise is unconvincing, ask for the argument for the premise
	- Repeat if necessary or until you become convinced

### Provided Argument:
|     | Propositions |
| --- | ------------ |
| P1  |              |
| P2  |              |
| C   | Therefore,   |

### Cherry on top

Whatever he says, ask him if he would accept the same evidence for a vegan diet?

---

# Arguments

### Tangential Stipulations Tho

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>   |
|:-------------------------------------------:|:------------------------------------------- |
|      <font color="CC6600">**S**</font>      | stipulation (x) is tangential to the debate |
|      <font color="CC6600">**O**</font>      | stipulation (x) is objectionable to Bart    |
|      <font color="CC6600">**a**</font>      | Bart demanding that Avi not speak to Isaac  |
|      <font color="CC6600">**n**</font>      | Nick demanding that Bart debates Avi        |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a stipulation is tangential to the debate, then the stipulation is objectionable to Bart.
<br />
<font color="CC6600">
<b>(∀x(Sx→Ox))</b>
<br />
<b>P2)</b></font> Bart demanding that Avi not speak to Isaac is tangential to the debate.
<br />
<font color="CC6600">
<b>(Sa)</b>
<br />
<b>P3)</b></font> Nick demanding that Bart debates Avi is tangential to the debate.
<br />
<font color="CC6600">
<b>(Sn)</b>
<br />
<b>P4)</b></font> If Nick demanding that Bart debates Avi is objectionable to Bart, then Bart demanding that Avi not speak to Isaac is objectionable to Bart.
<br />
<font color="CC6600">
<b>(On→Oa)</b>
<br />
<b>C)</b></font> Therefore, Bart demanding that Avi not speak to Isaac is objectionable to Bart.
<br />
<font color="CC6600">
<b>(∴Oa)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Sx~5Ox)),(Sa),(Sn),(On~5Oa)|=(Oa))

### Dodging Bart Tho

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                               |
|:----------------------------------------:|:------------------------------------------------------------------------ |
|    <font color="CC6600">**R**</font>     | stipulated conditions (x) for a debate have no bearing on the debate (y) |
|    <font color="CC6600">**M**</font>     | stipulation-maker (z) is dodging debate (y)                              |
|    <font color="CC6600">**p**</font>     | Avi being able to publicly talk to his friends                           |
|    <font color="CC6600">**a**</font>     | Bart's debate with Avi                                                   |
|    <font color="CC6600">**b**</font>     | Bart debating Avi                                                        |
|    <font color="CC6600">**n**</font>     | Nick's debate with Bart                                                  | 
|    <font color="CC6600">**t**</font>     | Bart                                                                     |
|    <font color="CC6600">**c**</font>     | Nick                                                                     |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the stipulated conditions for a debate have no bearing on the debate, then the stipulation-maker is dodging the debate.
<br />
<font color="CC6600">
<b>(∀x∀y∀z(Rxy→Mzy))</b>
<br />
<b>P2)</b></font> Avi being able to publicly talk to his friends has no bearing on Bart's debate with Avi.
<br />
<font color="CC6600">
<b>(Rpa)</b>
<br />
<b>P3)</b></font> Bart debating Avi has no bearing on Nick's debate with Bart.
<br />
<font color="CC6600">
<b>(Rbn)</b>
<br />
<b>P4)</b></font> If Nick is dodging Nick's debate with Bart, then Bart is dodging Bart's debate with Avi.
<br />
<font color="CC6600">
<b>(Mcn→Mta)</b>
<br />
<b>C)</b></font> Therefore, Bart is dodging Bart's debate with Avi.
<br />
<font color="CC6600">
<b>(∴Mta)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y~6z(Rxy~5Mzy)),(Rpa),(Rbn),(Mcn~5Mta)|=(Mta))

[[📂 Media/PDFs/Bart Kay Debate.pdf]]

---

# Bart's Chat

![[📂 Media/Images/Pasted image 20220804191438.png]]

![[📂 Media/Images/Pasted image 20220804191533.png]]

![[📂 Media/Images/Pasted image 20220804191630.png]]

![[📂 Media/Images/Pasted image 20220804191245.png]]

![[📂 Media/Images/Pasted image 20220804191655.png]]

![[📂 Media/Images/Pasted image 20220804191701.png]]

![[📂 Media/Images/Pasted image 20220804191718.png]]

![[📂 Media/Images/Pasted image 20220804191746.png]]

![[📂 Media/Images/Pasted image 20220804190921.png]]

---

# Email Convo

### Nick
Hello, Bart. I'm willing to debate against your proposition, but it's conditional. First you must lift the restrictions that you set for Avi Bitterman regarding the LDL and CVD debate. Your conditions were unreasonable, and even included absurd stipulations like him limiting his online social life (lol) and only being allowed to reference three publications (lol). If you lift these unreasonable conditions and have an untimed, unfettered debate Avi, I will debate you. It's quite simple. I'm perfectly willing to debate under those conditions. If you refuse those conditions (which are quite reasonable), it's you impeding the debate, not me.

### Bart
You cannot do justice to debating more than about three papers during a debate. There must be time to look closely into each source you want to bring. The restrictions on Your boy Avi had nothing to do with his 'social life'. I said he and Isaac Brown must not comment publicly before or during any series of debates. It was not remotely unreasonable given their behaviour around the issue at the time. If there was ANY decent scientifically valid evidence in support of your LDL hypothesis, one paper will do it. I think a limit of 3 is perfectly generous, because I KNOW there is no such evidence extant. It takes time as I said to properly debunk the kind of nonsense you ridiculous ideologues like to cite. If you refuse this debate, it is YOU refusing it, NOT me.

### Nick
My terms were clear, and I see no argument from you to support your suggestion that a single paper should necessarily be sufficient to make a particular case for the hypothesis in question. Some cases for some hypotheses would require synthesizing data from multiple domains, across tens or even hundreds of papers. I also see no argument from you to support the suggestion that Isaac Brown's behaviour on social media would have any bearing on the debate between Avi and yourself whatsoever. If you wish to provide those arguments, I'm open to receiving them. Until then, I view my terms to be more reasonable than your own, and I'm thereby considering you as the party who is gridlocking the debate. I'm perfectly open to debating when my terms have been satisfied. Proceed however you wish.

### Bart
As expected, you are a coward. No shocks. Scientific evidence comes in the form of properly controlled, properly powered, properly randomised, properly disciplined intervention based experimental works, if you want to propose cause and effect. If any such existed, it would have been published in a single paper providing that data. You and I both know that no such work exists, ergo if you propose a causal artefact here, you lose the debate, outright, before you even begin. Also historical statements around conditions on a completely separate and unrelated proposed debate that did not occur have no bearing whatever on this proposal. Basically, you're being exactly the pathetic boy I knew you would be. If you're confident in your ability to prove me wrong, you need to take this opportunity seriously. Grow up, child. Send me your three proposed papers establishing your moot, and we're on. There is no gridlock. You are attempting to gridlock by misinformation constipation. You do not win a debate by making a larger number of references to papers that do not prove your point... you bring the 1-3 top papers that you propose do prove your point.

### Nick
> _"Scientific evidence comes in the form of properly controlled, properly powered, properly randomised, properly disciplined intervention based experimental works, if you want to propose cause and effect."_ 

This is just a straightforwardly false claim. There are forms of scientific evidence that don't meet this conjunction of characteristics. Can you try again to provide an argument to support your suggestion that a single paper should necessarily be sufficient to make a particular case for the hypothesis in question?  

> _"Also historical statements around conditions on a completely separate and unrelated proposed debate that did not occur have no bearing whatever on this proposal."_ 

Again, my conditions are clear. If you're free to stipulate conditions upon others that have no bearing on previous proposals, I see no argument for why others cannot place likewise stipulations upon you for current proposals. Could you please provide that argument?

### Bart
If you want to argue with me, your opportunity is to do so live on camera, I'm not wasting any more of my valuable time here. Bring your 1-3 best proofs, provide them up front, and lets go. Book it. No more emails. [calendly.com/bart-kay](http://calendly.com/bart-kay)

---

# Hashtags

#debate 
#clowns 
#clownery 
#carnivore 
#bart_kay
#philosophy 
#debate_opponents 
