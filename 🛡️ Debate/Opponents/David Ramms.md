# Debate 1

## Proposition
> "We ought not stop predators in nature from engaging in predation."

### Ethical Factory Farming Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                         |
|:-------------------------------------------:|:--------------------------------------------------------------------------------- |
|      <font color="CC6600">**D**</font>      | a predator (x) engages in predation (y)                                           |
|      <font color="CC6600">**R**</font>      | it is the case that we ought stop the predator (x) from engaging in predation (y) | 
|      <font color="CC6600">**a**</font>      | human                                                                             |
|      <font color="CC6600">**m**</font>      | factory farming pigs                                                              |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a predator engages in predation, then it is not the case that we ought stop the predator from engaging in said predation.
<br />
<font color="CC6600">
<b>(∀x∀y(Dxy→¬Rxy))</b>
<br />
<b>P2)</b></font> Humans engage in factory farming pigs.
<br />
<font color="CC6600">
<b>(Dam)</b>
<br />
<b>C)</b></font> Therefore, it is not the case that we ought stop humans from engaging in factory farming pigs.
<br />
<font color="CC6600">
<b>(∴¬Ram)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Dxy~5~3Rxy)),(Dam)|=(~3Ram))

---

# Hashtags

#debate 
#debate_opponents 
#vegan 