# Debate 1

## Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**F**</font>      | a food (x) as poor mineral yields         |
|      <font color="CC6600">**H**</font>      | a food (x) is healthy                     |
|      <font color="CC6600">**o**</font>      | oatmeal                                   |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a food as poor mineral yields, then the food is not healthy.
<br />
<font color="CC6600">
<b>(∀x(Fx→¬Hx))</b>
<br />
<b>P2)</b></font> Oatmeal has poor mineral yields.
<br />
<font color="CC6600">
<b>(Fo)</b>
<br />
<b>C)</b></font> Therefore, oatmeal is not healthy.
<br />
<font color="CC6600">
<b>(∴¬Ho)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Fx~5~3Hx)),(Fo)|=(~3Ho))

### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**F**</font>      | a food (x) as poor mineral yields         |
|      <font color="CC6600">**H**</font>      | a food (x) is healthy                     |
|      <font color="CC6600">**n**</font>      | honey                                   |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a food as poor mineral yields, then the food is not healthy.
<br />
<font color="CC6600">
<b>(∀x(Fx→¬Hx))</b>
<br />
<b>P2)</b></font> Oatmeal has poor mineral yields.
<br />
<font color="CC6600">
<b>(Fn)</b>
<br />
<b>C)</b></font> Therefore, oatmeal is not healthy.
<br />
<font color="CC6600">
<b>(∴¬Hn)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Fx~5~3Hx)),(Fn)|=(~3Hn))

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
#carnivore 