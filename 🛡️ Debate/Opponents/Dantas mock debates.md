# Debate 3

## Proposition

>""

## Analysis

1. 

## Semantics

| **Definiendum** | **Definiens** |
|:---------------:|:-------------:|
|                 |               |
|                 |               |
|                 |               |

## Clarified Proposition

>""

## Receipts

---

# Hashtags

#debate 


# Mock Debate 1

## Proposition

>"veganism is classist"

## Analysis

1. veganism is unhealthy
	1. vitamin a
	2. need more medical care

## Semantics

1. classist
	1. unjustifiably treating people differently

---

# Mock Debate 2

## Proposition

>"I think eating animals is fine and I don't see an issue with it"

## Analysis

1. "it is ok to impose my morals by intervening on an ax murderer"
2. "it is not ok to impose your morals by intervening on my carnism"

---

# Mock Debate 3

## Proposition

>"it is wrong to lie to your kid about santa claus"

## Analysis

1. lying is wrong
	1. deontic principle?
		1. all lying is bad
			1. response: jews under your floorboards in nazi germany. lie about them being there?
				1. no
		2. lying is not always bad, but lying about santa is not worth it
			1. response: deprives them of important childhood experiences
				1. what's the evidence for that?

## Thoughts

1. it's just my value
	1. agnostic about the empirics

# Debate 4

## Proposition

>"Trans women should not compete against biological women in sports"

## Analysis

1. Lamby should clarify why it's wrong?
	1. What are the bad outcomes?
2. 

## Reasoning

1. Unfair physical advantage
	1. more muscle
	2. bigger bones
	3. more stamina

## Semantics

1. 

## Receipts

---

# Hashtags

#debate 


---

# Hashtags

#debate 
#debate_opponents 