## Proposition

>"It is more reasonable to believe that saturated fat-induced increases in serum cholesterol and/or LDL and/or ApoB will increase cardiovascular disease risk than it is to believe the opposite.""

## Argument

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If measuring and controlling for "other factors" is required to establish a variable as an "independent causal agent", then in order to establish each "other factor" as an "independent causal agent", every other "other factor" needs to be measured and controlled for.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Measuring and controlling for "other factors" is required to establish a variable as an "independent causal agent".
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, in order to establish each "other factor" as an "independent causal agent", every other "other factor" needs to be measured and controlled for.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

## Analysis

1) Ask Miguel if he can recreate my position.
3) Ask Miguel if "other factors" are independent causal agents.
	2) If no, ask why we should care about them or ask if they are confounders.
	3) If yes, run syllogism below.

## Semantics

1. Independent causal agent
	 - when a risk factor retains its statistical association with the outcome when other factors for the outcome are included or excluded.

2. Other factors
	- Obesity, especially visceral obesity
	- Insulin resistance
	- Hypertriglyceridemia
	- Metabolic syndrome
	- Type 2 diabetes
	- HIV disease
	- Low fat intake or diets enriched with polyunsaturated fat
	- Cigarette smoking
	- Liver disease
	- Renal insufficiency
	- Drugs
		- Isotretinoin
		- Sirolimus (rapamycin)
		- Protease inhibitors
		- Androgenic steroids
		- Nonselective B-blockers
		- Probucol
		- Recombinant interleukin-2
	- Moderate to severe hypertriglyceridemia
	- Critical illness:
		- sepsis
		- burns
		- small bowel exclusion
	- Anabolic steroids
	- Acquired LCAT deficiency
		- decreased ApoA-I synthesis
		- Severe cholestasis
		- Cholestatic liver disease with liver failure
		- Alcoholic hepatitis
		- Acute viral hepatitis
		- Alcoholic cirrhosis
		- Partial hepatectomy (temporary)
	- Stress
	- Genetics
	- Liver issues
	- Physical activity
	- Alcohol consumption
	- Sugar consumption

## Receipts

![[📂 Media/Images/Pasted image 20220611214046.png]]
![[📂 Media/Images/Pasted image 20220611214100.png]]

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery  
#philosophy 
#propositional_logic 
#de_morgans_laws