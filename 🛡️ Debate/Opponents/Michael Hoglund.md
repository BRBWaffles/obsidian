# Debate 1

### Melina's Animal Advocacy Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                             |
|:-------------------------------------------:|:--------------------------------------------------------------------- |
|      <font color="CC6600">**P**</font>      | action (x) extracts pleasure from a sentient being (y) at its expense | 
|      <font color="CC6600">**M**</font>      | action (x) is morally wrong                                           |
|      <font color="CC6600">**c**</font>      | cows                                                                  |
|      <font color="CC6600">**d**</font>      | dogs                                                                  |
|      <font color="CC6600">**e**</font>      | eating cows                                                           |
|      <font color="CC6600">**f**</font>      | fucking dogs                                                          |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If an action extracts pleasure from a sentient being at its expense, then such an action is morally wrong.
<br />
<font color="CC6600">
<b>(∀x∀y(Pxy→Mx))</b>
<br />
<b>P2)</b></font> Eating cows extracts pleasure from cows at their expense.
<br />
<font color="CC6600">
<b>(Pec)</b>
<br />
<b>P3)</b></font> Fucking dogs extracts pleasure from dogs at their expense.
<br />
<font color="CC6600">
<b>(Pfd)</b>
<br />
<b>C)</b></font> Therefore, eating cows and fucking dogs is morally wrong.
<br />
<font color="CC6600">
<b>(∴Me∧Mf)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Pxy~5Mx)),(Pec),(Pfd)|=(Me~1Mf))

---

# Hashtags

#debate 
#debate_opponents 
#vegan 
#ntt 