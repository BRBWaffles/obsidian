# Debate 2

## Proposition

>"Nick believes:
> 1) "

## Analysis

### Argument Against Collective Ownership

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                               |
|:-------------------------------------------:|:----------------------------------------------------------------------- |
|  <font color="CC6600">**Ownership**</font>  | (x) has authority in disputes and has the final say in how the item is used |
|   <font color="CC6600">**∃x(Cx)**</font>    | (x) exists                                                              |
|   <font color="CC6600">**∀y(Hy)**</font>    | for all things, (y) in the collective has ownership over the item       |
|      <font color="CC6600">**x**</font>      | collective ownership                                                    |
|      <font color="CC6600">**y**</font>      | everyone                                                                |
|      <font color="CC6600">**z**</font>      | one individual                                                          |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If collective ownership exists, then, for all things, everyone in the collective has ownership over the item.
<br />
<font color="CC6600">
<b>(∃x(Cx)→∀y(Hy))</b>
<br />
<b>P2)</b></font> There exists at least one individual who does not have ownership over the item.
<br />
<font color="CC6600">
<b>(∃z(¬Hz))</b>
<br />
<b>C)</b></font> Therefore, collective ownership does not exist.
<br />
<font color="CC6600">
<b>(∴¬∃x(Cx))</b>
<br />
<br />
</font>
</div>

## Semantics

| **Definiendum** | **Definiens** |
|:---------------:|:-------------:|
|                 |               |
|                 |               |
|                 |               |

## Clarified Proposition

>""

## Receipts

![[Pasted image 20241104170500.png]]
![[Pasted image 20241104170436.png]]

---

### Permissible Dog-Stomping Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>     |
|:-------------------------------------------:|:--------------------------------------------- |
|    <font color="CC6600">**P(y)**</font>     | animal (y) is property                    |
|   <font color="CC6600">**Q(x,y)**</font>    | committing harm (x) against animal (y) is permissible |
|      <font color="CC6600">**x**</font>      | committing harm against (an animal)                              |
|      <font color="CC6600">**y**</font>      | an animal                                     |
|      <font color="CC6600">**d**</font>      | a stray dog (y)                               |
|      <font color="CC6600">**s**</font>      | Peanut the squirrel (y)                       |
|      <font color="CC6600">**m**</font>      | stomping in the head of (an animal) (x)                   |
|      <font color="CC6600">**t**</font>      | euthanizing (x)                               |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all things, committing harm (x) against animal (y) is permissible if, and only if, the animal (y) is not property.
<br />
<font color="CC6600">
<b>(∀x∀y(Qxy↔¬Py))</b>
<br />
<b>P2)</b></font> A stray dog is not property and Peanut the squirrel is property.
<br />
<font color="CC6600">
<b>(¬Pd∧Ps)</b>
<br />
<b>C)</b></font> Therefore, stomping in the head of a stray dog is permissible and euthanizing Peanut the squirrel is not permissible.
<br />
<font color="CC6600">
<b>(∴Qmd∧¬Qts)</b>
<br />
<br />
</font>
</div>

---

# Debate 1

## Proposition

>"That carnivore/hyper-carnivore is best for optimal human health and is ethically appropriate."

## Analysis
1. what does "carnivore/hyper-carnivore" mean? If this is an inclusive disjunction, whichever diet is "less" carnivorous than the other will be one being argued for.
	- ""
2. what does "ethically appropriate" mean? What sort of ethics (rights, utility, rules?), Appropriate with regards to what?
	- ""

## Clarified Proposition 1

>"Red muscle meat diet with some bone marrow and salt supplies the nutrients that humans need to develop."

## Clarified Proposition 2

>"Red muscle meat diet with some bone marrow and salt is not shame-worthy."


## Clarified Proposition 3

>"Red muscle meat diet with some bone marrow and salt lacks phytochemicals that cause damage to the human body."

## Line of Questioning:

### What's the evidence?

1. Animal nutrients not found in plants
	 - Lack of animal foods causes nutritional deficiencies
	 - Brain size decreases with agriculture **(not evidence)**
2. Phytochemicals in plants 
	 - Pesticides in plants are carcinogenic **(not evidence)**
3. Limited adaptations for eating plants
	 - Don't see carnivores benefitting from plants
4. Plants want to defend themselves with chemicals
5. Many anecdotes count in favour of the carnivore **(not evidence)**
6. Blue zones eat a lot of meat and live the longest **(not evidence)**
7. Carnivorous animals don't thrive on herbivorous diets **(not evidence)**
8. The removal of fibre could help constipation
	 - We can only break down a small amount of the fibre
9. RCTs show that animal fat is superior to plant fat
	 - MCE, SDHS, WHI **(not evidence)**
 10. Paleolithic humans had the same life span as modern humans despite not having access to modern medicine **(not evidence)**
	 - Maasi live to be over 100 **(not evidence)**
 11. Harvard carnivore study **(not evidence)**

## Cherry on top

Whatever he says, ask him if he would accept the same for X diet

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
#carnivore 
#philosophy 