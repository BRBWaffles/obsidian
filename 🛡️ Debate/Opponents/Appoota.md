# Debate 1

## Proposition
>net-caught wild caught salmon is not vegan

## Notes
1. there is cost of applying the principle of indifference
	the cost is that there are more rights violations happening
		
2. it's not clear that there is a cost of applying the precautionary principle

## Questions
1. what's the evidence that there is a cost to the PoI?
	1. killing even order predators leads to a net increase in rights violations
2. how?
	1. 60% bycatch is OOP
		1. 60/100 fish causing rights violations
	2. 40% bycatch is EOP
		1. 40/100 fish preventing rights violations
	3. killing EOP allows more OOP to live than the OOP you caught

---

# Debate 2

## Proposition
>lure caught wild salmon is not vegan

## Notes

1. we're agnostic about whether these salmon are OOP or EOP
2. at worst you're doing something bad (where you'd be morally obligated not to do it) and at best you're doing something good (where it would only be morally obligatory to do it)
3. moral duties weigh against action in this case

---

# Hashtags

#debate 
#debate_opponents 
