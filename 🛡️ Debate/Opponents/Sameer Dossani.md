# Debate 1

>"These principles disprove the idea that eating meat or saturated fat causes heart attacks." - Sameer Dossani

## Analysis

1. **"we care about heart disease, we don't care about cholesterol."**
	1. ApoB is the causal agent [(1)](https://pubmed.ncbi.nlm.nih.gov/32203549/).
	2. GWAS show ApoB is linearly associated across numerous mechanisms [(2)](https://pubmed.ncbi.nlm.nih.gov/30694319/). ![[Pasted image 20220628124254.png]]
	3. LDL-C as a proportional relationship to ASCVD risk [(3)](https://pubmed.ncbi.nlm.nih.gov/27673306/). ![[Pasted image 20220628124445.png]]
	4. LDL-C correlates with risk when other risk factors are optimized [(4)](https://pubmed.ncbi.nlm.nih.gov/29241485/). ![[Pasted image 20220628124922.png]]
	5. Discordance can explain the lack of association at lower LDL-C [(5)](https://pubmed.ncbi.nlm.nih.gov/15296705/)
2. **"cholesterol is necessary for life."**
3. **"heart disease is a modern phenomenon."**
	1. Traditional populations still get ASCVD [(6)](https://pubmed.ncbi.nlm.nih.gov/23489753/).
4. **"there are non-western, historic populations that don't suffer from heart disease."**
	1. The Maasai still get ASCVD[(7)](https://pubmed.ncbi.nlm.nih.gov/5007361/).
	2. The Inuit still get ASCVD[(8)](https://pubmed.ncbi.nlm.nih.gov/2206175/)[(9)](https://pubmed.ncbi.nlm.nih.gov/12535749/).
	3. Inuit mummies still show ASCVD [(10)](https://pubmed.ncbi.nlm.nih.gov/31880790/).

https://www.ahajournals.org/doi/full/10.1161/CIRCULATIONAHA.118.034032

![[Pasted image 20220628135128.png]]

---

# Hilarious Argument

![[📂 Media/Images/Pasted image 20230723135007.png]]

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
#LDL 
#cardiovascular_disease 