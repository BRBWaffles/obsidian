# Debate 1

## Proposition
>"vegans kill more animals than non-vegans"

## Clarified Proposition
>"do you mean more animals die as a result of vegans making consumer choices as opposed to omnivores making consumer choices?"

## Questions
1. By this, do you mean more animals die as a result of vegans making consumer choices as opposed to omnivores making consumer choices?
	1. answer: yes
2. Is this in absolute or relative terms?
	1. answer: relative
3. What's the evidence that the proposition is true?
	1. Required are empirics showing that more animals die when vegans make vegan consumer choices over omnivorous consumer choices, adjusted/truncated for biofuel and animal ag feed.
	2. Statistics also need to be truncated to account for omnivores making consumer decisions consistent with veganism (such as buying bread).

## Evidence

- crop protection and production kills lots of animals
- there's a worldwide decline in insects and birds (via pesticides and habitat change)
	- compatible with veganism causing fewer deaths in the long run
	- human activity leads to insect and bird deaths in animal ag scenarios too
		- pasture for grass-fed
		- crops for grain-fed
	- none of this is interesting because there are no comparative analyses of consumer choices and their subsequent effects on animal death
		- the only information we know from this is that crop ag kills lots of animals
		- insufficient to make the case that more animals die from vegan, rather than omnivore, choices

## Questions

- would alternatives to crop ag

## Argument

P1) If you kill off animals to extinction, then ecosystems will break down
P2) you kill off animals to extinction
C) therefore, ecosystems will break down

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 