# Lipid Triad

https://pubmed.ncbi.nlm.nih.gov/29241485/

"Many CVRF-free middle-aged individuals have atherosclerosis. LDL-C, even at levels currently considered normal, is independently associated with the presence and extent of early systemic atherosclerosis in the absence of major CVRFs. These findings support more effective LDL-C lowering for primordial prevention, even in individuals conventionally considered at optimal risk."

Lipid combos:
https://pubmed.ncbi.nlm.nih.gov/25458651/

"Aside from isolated hypertriglyceridemia, low levels of HDL-C, high levels of LDL-C, and high levels of TG in any combination were associated with increased risk of CVD."

LMHR paper:
https://pubmed.ncbi.nlm.nih.gov/35106434/

"These data suggest that, in contrast to the typical pattern of dyslipidemia, greater LDL cholesterol elevation on a CRD tends to occur in the context of otherwise low cardiometabolic risk."

---

# LDL Bounty

## Criteria

1. **HDL Cholesterol of 50 mg/dL or above (≥ 1.29 mmol/L)**
2. **Triglycerides of 100 mg/dL or below (≤ 1.13 mmol/L)**
3. **LDL Cholesterol of 130 mg/dL or above (≥ 3.36 mmol/L)**
4. **Either high Coronary Heart Disease (CHD) or high Cardiovascular Disease (CVD) (see the section below)**
5. By “**normal**” and “**non-treated**“, I mean:
    - No stratifying by specific genetics
    - No stratifying by drugs (no drug studies)
    - No stratifying by a particular illness in advance of the study. (duh!)
    - _In other words, just a generally broad group of people like [Framingham Offspring](https://www.ncbi.nlm.nih.gov/pubmed/27166203) or the [Jeppesen study](https://www.ncbi.nlm.nih.gov/pubmed/11176761)_
6. And here’s some fine print that should be obvious, but just in case…
    - The study needs to be published in a reputable journal
    - It has to be dated before this article was posted, of course
    - The study needs to have at least 400 participants that are stratified by this criteria. (The two studies above have over 500)
    - I’d prefer no unusual “modeling” or “adjustments” to alter the data too far from it’s original set. This one goes by the honor system — if you have such a study and it is clearly warranted, I can give it a pass.

"High" defined as greater than the average rates of CVD per age group in the American population.

### Criticisms

1. Vanishingly small population subset. Unreasonable to expect it to have been studied.
2. If studied in the general population, they won't be keto, so translation to keto subjects may be dubious.
3. The sheer weight of the evidence in favour of LDL's causal role in ASCVD cuts deeply against the notion that this population subset would be protected to begin with.
4. Comparing this isolated cohort to the American general population is methodology that is beyond fringe, bordering on insane.
5. Ultimately if this is taken to be an indication that LDL is fine in this particular context, it would qualify as an appeal to ignorance.

---

# Hashtags

#lipidology 
#lipid_triad
#LMHR
#LDL 
#triglycerides 
#HDL 
#clowns
#clownery
#debate 
#debate_opponents 