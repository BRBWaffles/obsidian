# Debate 1

## Proposition
>"the claim that killing a particular carnist reduces total rights violations is unfalsifiable"

---

## Semantic Analysis

### Unclear Terms
1. unfalsifiable
	1. still no clue
		1. a hypothesis is falsifiable if and only if demonstrating the negation of the hypothesis is practically achievable
		2. a hypothesis is unfalsifiable if and only if demonstrating the negation of the hypothesis is impractical
2. disprove
	1. 

---

## Clarified Proposition
>demonstrating the negation of the hypothesis that "killing a particular carnist reduces total rights violations" is impractical

## Questions
1. 

# Dom's Rebuttal

>Both "killing X reduces rights violations" (A) and its negation "killing X does not reduce rights violations" (B) are falsifiable, as long as the goalposts arent moved. For both you can devise some experiment where we observe rights violations (in a set area, for a given time, etc), kill X, observe rights violations under the same constraints again. Lets label the results "less rights violations observed" P and "equal or more rights violations observed Q. Under:
>
>A+P, A is not rejected
>A+Q, A is rejected
>B+P, B is rejected
>B+Q, B is not rejected

---

### Dom's Reductio

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If one claims that killing a carnist is going to lower total rights violations and one can claim that the proof for the claim is obtainable, then the claim that killing a carnist is going to lower total rights violations is not falsifiable.
<br />
<font color="CC6600">
<b>(P∧Q→¬R)</b>
<br />
<b>P2)</b></font> One claims that killing a carnist is going to lower total rights violations.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> One can claim that the proof for the claim is obtainable.
<br />
<font color="CC6600">
<b>(Q)</b>
<br />
<b>C)</b></font> Therefore, the claim that killing a carnist is going to lower total rights violations is not falsifiable.
<br />
<font color="CC6600">
<b>(∴¬R)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~1Q~5~3R),(P),(Q)|=(~3R))

---

# Hashtags

#debate 
#debate_opponents 