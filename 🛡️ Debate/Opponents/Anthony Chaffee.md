### Polyphenol Reductio

| <font color="CC6600">**Variable**</font> | <font color="CC6600">**Definition**</font>                     |
|:----------------------------------------:|:-------------------------------------------------------------- |
|    <font color="CC6600">**H**</font>     | plant defense chemicals (x) are harmful                            |
|    <font color="CC6600">**C**</font>     | plant defense chemicals (x) are contained in food (y)          |
|    <font color="CC6600">**V**</font>     | plant defense chemicals (x) render food (y) harmful.           |
|    <font color="CC6600">**M**</font>     | genetic modification that removes (p) renders (g) less harmful |
|    <font color="CC6600">**p**</font>     | polyphenols from grass                                         |
|    <font color="CC6600">**g**</font>     | grass-fed beef                                                 |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If plant defense chemicals are harmful and plant defense chemicals are contained in a food, then plant defense chemicals render the food harmful.
<br />
<font color="CC6600">
<b>(∀x∀y(Hx∧Cxy→Vxy))</b>
<br />
<b>P2)</b></font> Polyphenols from grass are harmful.
<br />
<font color="CC6600">
<b>(Hp)</b>
<br />
<b>P3)</b></font> Polyphenols from grass are contained in grass-fed beef.
<br />
<font color="CC6600">
<b>(Cpg)</b>
<br />
<b>P4)</b></font> If polyphenols from grass render grass-fed beef harmful, then genetic modification that removes polyphenols from grass renders grass-fed beef less harmful.
<br />
<font color="CC6600">
<b>(Vpg→Mpg)</b>
<br />
<b>P5)</b></font> Polyphenols from grass render grass-fed beef harmful.
<br />
<font color="CC6600">
<b>(Vpg)</b>
<br />
<b>C)</b></font> Therefore, genetic modification that removes polyphenols from grass renders grass-fed beef less harmful.
<br />
<font color="CC6600">
<b>(∴Mpg)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Hx~1Cxy~5Vxy)),(Hp),(Cpg),(Vpg~5Mpg),(Vpg)|=(Mpg))

---

# Hashtags

#clowns 
#clownery 
#debate 
#debate_opponents 

