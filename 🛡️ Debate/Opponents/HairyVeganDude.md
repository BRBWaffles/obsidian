# Debate 1

## Proposition
> "consuming processed oils are bad"

## Analysis
1. What does "processed" mean?
2. What does "bad" mean?

## Clarified Proposition
> "consuming X oils are Y" ???

## Line of Questioning:
1. What's the evidence?
	1. ""
2. What is the argument that is this evidence is more expected on the hypothesis than the negation of the hypothesis?
	1. An argument is required, because maybe the "evidence" is less or equally expected on the proposition (which would not be evidence for the proposition)
3. If he does provide an argument, examine the premises carefully
4. If a premise is unconvincing, ask for the argument for the premise
5. Repeat if necessary or until you become convinced

---

# Hashtags

#debate 
#vegetable_oil 
#clowns 
#clownery 
#debate_opponents 