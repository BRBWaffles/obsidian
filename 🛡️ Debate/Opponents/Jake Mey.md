# Debate 1

## Proposition
> "Vegan Fridays are harmful to children."

## Arguments

### Jake's Argument Against Vegan Fridays

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font>  If Vegan Friday meals are unhealthy for children, then it is wrong to feed a Vegan Friday meals to children once per week.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> If Vegan Friday meals are unhealthy for children.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, It is wrong to feed Vegan Friday meals to children once per week.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

## Analysis

### Jake Consistency Checker

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If children undereat plant foods more than they undereat animal foods, then Meatless Mondays are more likely result in nutritional adequacy in children compared to a policy that increases animal foods.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Children undereat plant foods more than they undereat animal foods.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, Meatless Mondays are more likely result in nutritional adequacy in children compared to an intervention that increases animal foods.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

1. To think Vegan Fridays meaningfully affect health negatively is hilarious
	1. Assuming around 38 school weeks per year and one vegan meal per week, that's just (38/(365x3))x100=3% of the diet.
	2. It's even lower when you consider that it's only a small proportion of the meal that is being replaced.
		1. Assuming it's only a quarter of the diet, it would be ((38/4)/(365x3))x100=0.86% of the total diet.

---

# Hashtags

#debate
#debate_opponents 
#clowns
#clownery
#plant_foods 
#animal_foods
#public_health 