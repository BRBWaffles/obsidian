# Debate 1

## Tweet

>The reason I’m not vegan is that my personal ethics are such that they allow me to, in good conscience, indulge my belief that animal products are healthier and more enjoyable to consume than the available alternatives, despite unfortunate tradeoffs in animal deaths and suffering

## Proposition
> "Status as an obligate carnivore absolutely forecloses that a vegan diet could be superior."

## Clarified Proposition:

>  "Status as an obligate carnivore forecloses the possibility that a diet that is totally absent any animal foods could be superior to a carnivorous diet for cats."

## New Proposition

> "Cats are adapted to thrive on a meat-based diet."
> "A diet consisting only of plant products would not provide the sufficient nutrients to maximize that cat's fitness."

---

# Hashtags

#debate 
#debate_opponents 
#debate 
#clowns 
#clownery 
#vegan 
#cats 
