# Debate 1

## Proposition
> "seed oils are poison."

## Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                                                         |
|:-------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**M**</font>      | food (x) is poison                                                                                                                                                |
|      <font color="CC6600">**P**</font>      | food (x) was created by a food industry that is largely controlled by marketing budgets of companies with no motivational avenue other than to promote addictions |
|      <font color="CC6600">**s**</font>      | seed oils                                                                                                                                                         |
|      <font color="CC6600">**w**</font>      | whole foods                                                                                                                                                       |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> The food is poison if, and only, if the food was created by a food industry that is largely controlled by marketing budgets of companies with no motivational avenue other than to promote addictions.
<br />
<font color="CC6600">
<b>(∀x(Mx↔Px))</b>
<br />
<b>P2)</b></font> Seed oils were created by a food industry that is largely controlled by marketing budgets of companies with no motivational avenue other than to promote addictions.
<br />
<font color="CC6600">
<b>(Ms)</b>
<br />
<b>P3)</b></font> Whole foods were not created by a food industry that is largely controlled by marketing budgets of companies with no motivational avenue other than to promote addictions.
<br />
<font color="CC6600">
<b>(¬Mw)</b>
<br />
<b>C)</b></font> Therefore, seed oils are poison and whole foods are not poison.
<br />
<font color="CC6600">
<b>(∴Ps∧¬Pw)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Mx~4Px)),(Ms),(~3Mw)|=(Ps~1~3Pw))

## Analysis

1. Argument for P1?

---

# Hashtags

#clowns 
#clownery 
#debate 
#debate_opponents 
