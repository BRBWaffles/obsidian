# Debate 1

## Proposition
> "bivalves are sentient because they have a brain."

## Argument

1. "The definition of brain needs to apply to all species... Not being such, it is speciesist intentionally.
2. If the definition of a brain doesn't apply to sponges, then your definition of brain is speciesist on your own lights.

### ImDemonWolf's Definition of a Brain

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>               |
|:-------------------------------------------:|:------------------------------------------------------- |
|      <font color="CC6600">**B**</font>      | something (x) is a brain                                |
|      <font color="CC6600">**M**</font>      | something (x) is a convoluted mass of nervous substance |
|      <font color="CC6600">**S**</font>      | something (x) is is encased within a skull              |
|      <font color="CC6600">**c**</font>      | cerebral ganglia inside of a bivalve                  | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something is a brain if, and only if, something is a convoluted mass of nervous substance and is encased within a skull.
<br />
<font color="CC6600">
<b>(∀x(Bx↔Mx∧Sx))</b>
<br />
<b>P2)</b></font> The cerebral ganglia inside of a bivalve is convoluted masses of nervous substance.
<br />
<font color="CC6600">
<b>(Mc)</b>
<br />
<b>P3)</b></font> The cerebral ganglia inside of a bivalve is not encased within a skull.
<br />
<font color="CC6600">
<b>(¬Sc)</b>
<br />
<b>C)</b></font> Therefore, the cerebral ganglia inside of a bivalve is not a brain.
<br />
<font color="CC6600">
<b>(∴¬Bc)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Bx~4Mx~1Sx)),(Mc),(~3Sc)|=(~3Bc))

## Analysis

1. If your definition of a brain is so broad that it captures structures like cerebral ganglia, I don't see how you're not committed to the proposition that my body contains multiple brains. Do you sign off on that?

## Receipts

![[📂 Media/Images/Pasted image 20230726122259.png]]
![[📂 Media/Images/Pasted image 20230726125553.png]]
![[📂 Media/Images/Pasted image 20230726125638.png]]
![[📂 Media/Images/Pasted image 20230726125705.png]]

---

# Hashtags

#debate 
#debate_opponents 