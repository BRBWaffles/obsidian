# Debate 2

## Evidence of Consent
[[📂 Media/PDFs/Gmail - Debate.pdf]]

## Proposition

> "the natural human diet is best for ensuring optimal health."

## Analysis

1. what is the natural human diet
2. what does health mean?
3. what does it mean for health to be ensured
4. what does optimal mean
5. what does best mean and is it different than optimal?
6. why include the word best if optimal already conveys the meaning?

## Clarified Proposition

> "All else equal, any diet consisting of less than 20% meat will not be likely to yield maximum longevity to a greater degree than a diet of greater than 80% meat."

## Line of Questioning:

1) Is this a scientific hypothesis?
	1) 
2) What's the evidence?
	- "there is a tendency for natural diets to yield longevity relative to novel diets."
	- 
3) What is the argument that is this evidence is more expected on the hypothesis than the negation of the hypothesis?
	- P1: 
	- P2: 
	- C: 

"A scientific hypothesis is an empirically testable proposition that is used to explain something, and help us understand it by systematizing and unifying our knowledge."

---

# Debunk

[Carnivore Essay](https://docs.google.com/document/d/1N9C9sqqtCG4VEx8wy-kZM2RCivihuFx8odRBn79DFng/edit)

---

## Claim #1

> "There is excellent research in support of ketosis as the natural human state that is a superior alternative to being fueled by glucose/fructose."

## Analysis
The provided literature does not provide support for the claim. Even if it is true that ketosis has benefits, that does not tell us anything about whether or not ketosis is "*the* natural human state". If it does, then why isn't fruit consumption considered to be the natural human state as well? Fruit consumption associates with enormous benefits [(1)](https://pubmed.ncbi.nlm.nih.gov/28446499/)[(2)](https://pubmed.ncbi.nlm.nih.gov/25073782/). What is the difference between the literature on ketosis that is untrue of the literature on fruit, such that the natural human state involves ketosis and the natural human state does not involve fruit consumption?

### Internal Critique

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>       |
|:-------------------------------------------:|:----------------------------------------------- |
|      <font color="CC6600">**E**</font>       | the exposure show benefits to human health      |
|      <font color="CC6600">**N**</font>       | the exposure is part of the natural human state |
|      <font color="CC6600">**f**</font>       | fruit                                           |
|      <font color="CC6600">**k**</font>       | ketosis                                         | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the exposure show benefits to human health, then the exposure is part of the natural human state.
<br />
<font color="CC6600">
<b>(∀x(Ex→Nx))</b>
<br />
<b>P2)</b></font> Fruit and ketosis show benefits to human health.
<br />
<font color="CC6600">
<b>(Ef∧Ek)</b>
<br />
<b>C)</b></font> Therefore, fruit and ketosis are part of the natural human state.
<br />
<font color="CC6600">
<b>(∴Nf∧Nk)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Ex~5Nx)),(Ef~1Ek)|=(Nf~1Nk))

## Claim #2

>"The next reason why you should adopt a ketogenic carnivore diet is that carbohydrates are a poison"

## Analysis
The provided literature does not provide support for the claim, as they either pertain to hyperglycemia and hyperglycemia-induced glycation in T2DM, and mechanistic studies. The remaining points have no citations. In reality, many carbohydrate-rich whole foods are strongly inversely associated with the risk of disease [(3)](https://pubmed.ncbi.nlm.nih.gov/27537552/)[(4)](https://pubmed.ncbi.nlm.nih.gov/28140323/).


## Claim #3

>"Carbohydrates spike blood sugar, resulting in insulin secretion."

## Analysis
This proposition is phrased as though insulin is the mediator of some negative outcome. If true, it also would apply to meat. This is because beef might actually be just as insulinogenic as the average breakfast cereal [(5)](https://pubmed.ncbi.nlm.nih.gov/9356547/). Additionally, legumes seem to decrease both fasting glucose and fasting insulin when replacing even lean red meat [(6)](https://pubmed.ncbi.nlm.nih.gov/25351652/).

## Internal Critique

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>     |
|:-------------------------------------------:|:--------------------------------------------- |
|      <font color="CC6600">**S**</font>      | the exposure (x) results in insulin secretion |
|      <font color="CC6600">**H**</font>      | the exposure (x) is healthy                   |
|      <font color="CC6600">**r**</font>      | breakfast cereals                             |
|      <font color="CC6600">**b**</font>      | beef                                          |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the exposure results in insulin secretion, then the exposure is not healthy.
<br />
<font color="CC6600">
<b>(∀x(Sx→¬Hx))</b>
<br />
<b>P2)</b></font> Breakfast cereals and beef result in insulin secretion.
<br />
<font color="CC6600">
<b>(Sr∧Sb)</b>
<br />
<b>C)</b></font> Therefore, breakfast cereals and beef are not healthy.
<br />
<font color="CC6600">
<b>(∴¬Hr∧¬Hb)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Sx~5~3Hx)),(Sr~1Sb)|=(~3Hr~1~3Hb))

## Claim #3

>"While fruits are a less harmful source of fructose than soda, due to the fiber and nutrients in fruit that slow or prevent the absorption of some fructose, fruit can still make us fat and sick. The fructose molecule in a blueberry is the same fructose in a coke. Fructose elimination is indispensable for a number of reason"

## Analysis
This is an equivocation. While free fructose could have detrimental effects on weight, fruit appears to have the opposite effect [(7)](https://pubmed.ncbi.nlm.nih.gov/31256714/)[(8)](https://pubmed.ncbi.nlm.nih.gov/31139631/). In fact, comprehensive meta-analysis consistently finds a strong inverse association between plant foods of many sorts and various diseases [(9)](https://pubmed.ncbi.nlm.nih.gov/30639206/). The provided references are about free fructose, not fruit. It is unclear what is meant by "sick" in this context, but a citation is needed for the claim.

## Claim #4

>"The plant’s techniques underpinning its desire to survive, along with the impact of these on human health, will be made strikingly clear"

## Analysis
Mechanistic speculation. The provided literature does not provide support for the claim. Superseded by better evidence, mentioned above.

## Claim #5

>"But don’t plant foods have antioxidants, which must counteract the negative effects associated with plant toxins and fiber? As the evidence points out, plant foods do not provide such a benefit."

## Analysis
What negative effects are associated with plant consumption? The provided references don't support the claim. Peluso et al. (2018) and Young et al. (2002) were only measuring intermediate markers, not hard outcomes. The reference on cancer risk is superseded by better evidence [(10)](https://pubmed.ncbi.nlm.nih.gov/28338764/).

Additionally, fibre intake is strongly inversely associated with all-cause mortality [(11)](https://pubmed.ncbi.nlm.nih.gov/25552267/)[(12)](https://pubmed.ncbi.nlm.nih.gov/25143474/). These associations survive adjustment for relevant confounders, such as smoking, BMI, alcohol consumption, and physical activity. Lastly, the inverse association between fibre consumption and cardiovascular disease is linear [(13)](https://pubmed.ncbi.nlm.nih.gov/24355537/).

## Claim #6

>"A carnivorous ketogenic diet is best for human health because ruminant meat includes every nutrient we need in perfect ratios for optimal health. Plants, on the other hand, lack sufficient quantities of nutrients that are vital to sustaining life. These are the essential nutrients that are found in necessary quantities, if not completely exclusively, just in animal protein sources"

## Analysis
Mechanistic speculation. Despite the fact that sources of animal protein containing these beneficial nutrients and compounds, animal protein is almost universally associated with poorer health outcomes when replacing plant protein [(14)](https://pubmed.ncbi.nlm.nih.gov/33411911/)[(15)](https://pubmed.ncbi.nlm.nih.gov/32699048/).

## Claim #7

>"Despite alternative recommendations, a conclusive review of the available data reveals that meat does not cause cancer, and saturated fat is not the enemy"

## Analysis
For meat, it depends on the type of meat and the type of cancer. It is generally accepted that there is a relationship specifically between red meat and cancer among certain tissue types. For example, red meat consumption is associated with a dose-dependent increase in colorectal cancer risk [(16)](https://pubmed.ncbi.nlm.nih.gov/19350627/).

It should be noted that the magnitude of effect is technically greater than that of smoking, but likely non-inferior. If it is accepted that smoking increases colorectal cancer risk, then what's the difference between the two bodies of evidence that justifies the asymmetrical beliefs?

## Claim #8

>"Saturated fat was hypothesized to be bad because it leads to increased levels of LDL cholesterol, which scientists believed caused the health problems in the Western world. The belief that LDL is inherently bad is false"

## Analysis
Which scientists believed that LDL caused "the health problems of the Western world"? The provided references are superseded by better analyses with better inclusion/exclusion criteria and altogether superior methodology [(17)](https://pubmed.ncbi.nlm.nih.gov/32428300/)[(18)](https://pubmed.ncbi.nlm.nih.gov/20351774/).

Hooper et al. (2020) shows that reducing saturated fat lowers cardiovascular disease events (analysis 1.35). This effect is strongest when polyunsaturated fats are replacing saturated fats (analysis 1.44), and the relationship is likely mediated by changes in serum cholesterol (analysis 1.51). This relationship was also revealed via meta-regression analysis (table 4, regression 4). This is the exact relationship that we'd expect to see, based on wider research. [(19)](https://pubmed.ncbi.nlm.nih.gov/27673306/).

## Claim #9

>"A team of researchers at Tel Aviv University established the evidence behind the history of human hypercarnivory in a research paper"

## Analysis
Tangential. This can be granted, as it does not provide persuasive evidence that carnivore diets are optimally healthy in the long term for humans. Additionally, there are good reasons to be suspicious of foods to which we are most strongly adapted. This can be argued a priori.

If humans were heavily adapted to meat consumption, antagonistic pleiotropy could actually explain why foods like red meat associate so consistently with an increased risk of poorer health outcomes in the epidemiology [(20)](https://pubmed.ncbi.nlm.nih.gov/34284672/)[(21)](https://pubmed.ncbi.nlm.nih.gov/29137344/)[(22)](https://pubmed.ncbi.nlm.nih.gov/32011623/). 

## Claim #10

>"Despite the difficulties, in 2021, Harvard University published an observational study of 2029 respondents who ate a carnivore diet for at least 6 months. Participants reported increased levels of wellbeing and health (Lennerz et al., 2021), but the most noteworthy results were in the complete resolution or improvement of the chronic conditions that existed prior to starting the carnivore diet"

## Analysis
This study is considerably weaker than the vast majority of prospective nutritional epidemiology. If nutritional epidemiology is to be given lower credence in favour of this study, what justifies the lower credence for nutritional epidemiology?

Additionally, if anything the biomarker results would seem to indicate that the carnivore diet could resist some of the benefits typically observed with significant weight loss. Lastly, the induction of the carnivore diet was commensurate with an increase in coronary artery calcification.

## References:

[1] Schwingshackl, Lukas, et al. ‘Food Groups and Risk of All-Cause Mortality: A Systematic Review and Meta-Analysis of Prospective Studies’. _The American Journal of Clinical Nutrition_, vol. 105, no. 6, June 2017, pp. 1462–73. _PubMed_, https://doi.org/10.3945/ajcn.117.153148.

[2] Wang, Xia, et al. ‘Fruit and Vegetable Consumption and Mortality from All Causes, Cardiovascular Disease, and Cancer: Systematic Review and Dose-Response Meta-Analysis of Prospective Cohort Studies’. _BMJ (Clinical Research Ed.)_, vol. 349, July 2014, p. g4490. _PubMed_, https://doi.org/10.1136/bmj.g4490.

[3] Li, Bailing, et al. ‘Consumption of Whole Grains in Relation to Mortality from All Causes, Cardiovascular Disease, and Diabetes: Dose-Response Meta-Analysis of Prospective Cohort Studies’. _Medicine_, vol. 95, no. 33, Aug. 2016, p. e4229. _PubMed_, https://doi.org/10.1097/MD.0000000000004229.

[4] Benisi-Kohansal, Sanaz, et al. ‘Whole-Grain Intake and Mortality from All Causes, Cardiovascular Disease, and Cancer: A Systematic Review and Dose-Response Meta-Analysis of Prospective Cohort Studies’. _Advances in Nutrition (Bethesda, Md.)_, vol. 7, no. 6, Nov. 2016, pp. 1052–65. _PubMed_, https://doi.org/10.3945/an.115.011635.

[5] Holt, S. H., et al. ‘An Insulin Index of Foods: The Insulin Demand Generated by 1000-KJ Portions of Common Foods’. _The American Journal of Clinical Nutrition_, vol. 66, no. 5, Nov. 1997, pp. 1264–76. _PubMed_, https://doi.org/10.1093/ajcn/66.5.1264.

[6] Hosseinpour-Niazi, S., et al. ‘Substitution of Red Meat with Legumes in the Therapeutic Lifestyle Change Diet Based on Dietary Advice Improves Cardiometabolic Risk Factors in Overweight Type 2 Diabetes Patients: A Cross-over Randomized Clinical Trial’. _European Journal of Clinical Nutrition_, vol. 69, no. 5, May 2015, pp. 592–97. _PubMed_, https://doi.org/10.1038/ejcn.2014.228.

[7] Arnotti, Karla, and Mandy Bamber. ‘Fruit and Vegetable Consumption in Overweight or Obese Individuals: A Meta-Analysis’. _Western Journal of Nursing Research_, vol. 42, no. 4, Apr. 2020, pp. 306–14. _PubMed_, https://doi.org/10.1177/0193945919858699.

[8] Guyenet, Stephan J. ‘Impact of Whole, Fresh Fruit Consumption on Energy Intake and Adiposity: A Systematic Review’. _Frontiers in Nutrition_, vol. 6, 2019, p. 66. _PubMed_, https://doi.org/10.3389/fnut.2019.00066.

[9] Yip, Cynthia Sau Chun, et al. ‘The Associations of Fruit and Vegetable Intakes with Burden of Diseases: A Systematic Review of Meta-Analyses’. _Journal of the Academy of Nutrition and Dietetics_, vol. 119, no. 3, Mar. 2019, pp. 464–81. _PubMed_, https://doi.org/10.1016/j.jand.2018.11.007.

[10] Aune, Dagfinn, et al. ‘Fruit and Vegetable Intake and the Risk of Cardiovascular Disease, Total Cancer and All-Cause Mortality-a Systematic Review and Dose-Response Meta-Analysis of Prospective Studies’. _International Journal of Epidemiology_, vol. 46, no. 3, June 2017, pp. 1029–56. _PubMed_, https://doi.org/10.1093/ije/dyw319.

[11] Yang, Yang, et al. ‘Association between Dietary Fiber and Lower Risk of All-Cause Mortality: A Meta-Analysis of Cohort Studies’. _American Journal of Epidemiology_, vol. 181, no. 2, Jan. 2015, pp. 83–91. _PubMed_, https://doi.org/10.1093/aje/kwu257.

[12] Kim, Youngyo, and Youjin Je. ‘Dietary Fiber Intake and Total Mortality: A Meta-Analysis of Prospective Cohort Studies’. _American Journal of Epidemiology_, vol. 180, no. 6, Sept. 2014, pp. 565–73. _PubMed_, https://doi.org/10.1093/aje/kwu174.

[13] Threapleton, Diane E., et al. ‘Dietary Fibre Intake and Risk of Cardiovascular Disease: Systematic Review and Meta-Analysis’. _BMJ (Clinical Research Ed.)_, vol. 347, Dec. 2013, p. f6879. _PubMed_, https://doi.org/10.1136/bmj.f6879.

[14] Zhong, Victor W., et al. ‘Protein Foods from Animal Sources, Incident Cardiovascular Disease and All-Cause Mortality: A Substitution Analysis’. _International Journal of Epidemiology_, vol. 50, no. 1, Mar. 2021, pp. 223–33. _PubMed_, https://doi.org/10.1093/ije/dyaa205.

[15] Naghshi, Sina, et al. ‘Dietary Intake of Total, Animal, and Plant Proteins and Risk of All Cause, Cardiovascular, and Cancer Mortality: Systematic Review and Dose-Response Meta-Analysis of Prospective Cohort Studies’. _BMJ (Clinical Research Ed.)_, vol. 370, July 2020, p. m2412. _PubMed_, https://doi.org/10.1136/bmj.m2412.

[16] Huxley, Rachel R., et al. ‘The Impact of Dietary and Lifestyle Risk Factors on Risk of Colorectal Cancer: A Quantitative Overview of the Epidemiological Evidence’. _International Journal of Cancer_, vol. 125, no. 1, July 2009, pp. 171–80. _PubMed_, https://doi.org/10.1002/ijc.24343.

[17] Hooper, Lee, et al. ‘Reduction in Saturated Fat Intake for Cardiovascular Disease’. _The Cochrane Database of Systematic Reviews_, vol. 5, May 2020, p. CD011737. _PubMed_, https://doi.org/10.1002/14651858.CD011737.pub2.

[18] Mozaffarian, Dariush, et al. ‘Effects on Coronary Heart Disease of Increasing Polyunsaturated Fat in Place of Saturated Fat: A Systematic Review and Meta-Analysis of Randomized Controlled Trials’. _PLoS Medicine_, vol. 7, no. 3, Mar. 2010, p. e1000252. _PubMed_, https://doi.org/10.1371/journal.pmed.1000252.

[19] Silverman, Michael G., et al. ‘Association Between Lowering LDL-C and Cardiovascular Risk Reduction Among Different Therapeutic Interventions: A Systematic Review and Meta-Analysis’. _JAMA_, vol. 316, no. 12, Sept. 2016, pp. 1289–97. _PubMed_, https://doi.org/10.1001/jama.2016.13985.

[20] Papier, Keren, et al. ‘Meat Consumption and Risk of Ischemic Heart Disease: A Systematic Review and Meta-Analysis’. _Critical Reviews in Food Science and Nutrition_, July 2021, pp. 1–12. _PubMed_, https://doi.org/10.1080/10408398.2021.1949575.

[21] Zhao, Zhanwei, et al. ‘Red and Processed Meat Consumption and Colorectal Cancer Risk: A Systematic Review and Meta-Analysis’. _Oncotarget_, vol. 8, no. 47, Oct. 2017, pp. 83306–14. _PubMed_, https://doi.org/10.18632/oncotarget.20667.

[22] Zhong, Victor W., et al. ‘Associations of Processed Meat, Unprocessed Red Meat, Poultry, or Fish Intake With Incident Cardiovascular Disease and All-Cause Mortality’. _JAMA Internal Medicine_, vol. 180, no. 4, Apr. 2020, pp. 503–12. _PubMed_, https://doi.org/10.1001/jamainternmed.2019.6969.

---

# Hashtags

#debate 
#debate_opponents
#clowns 
#clownery 
#carnivore 
#vegan 