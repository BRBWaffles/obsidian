# Debate 1

## Proposition
> "bivalves are sentient."

## Analysis

1. Let's say there's a brain-dead baby who was born brain-dead. The only thing you know about them is that their brain has no activity and their cerebral ganglia are still active. There is no reason to believe the brain will gain additional activity on its own or can be brought to a state of activity. Do you pull the plug to give the family well-being? Keep in mind, the same amount of well-being is obtained by someone else harvesting and eating a ganglion-adjusted volume of oysters. Also, In this hypothetical, the only thing upon which to base an inference about sentience are the conditions specified above.

---

# Hashtags

#debate 
#debate_opponents 
#vegan 
#bivalves 