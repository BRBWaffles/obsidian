# Debate 1

## Proposition
> "a good person is someone with only good desires and true beliefs."

## Semantics
1. **good desire** is a desire that sentient beings generally have a reason to praise.
2. **bad desire** is a desire that sentient beings generally have a reason to condemn.

## Analysis

what if the machine determined that they desired babies getting chopped up

---

# Hashtags

#debate 
#debate_opponents 
#moral_realism 
#moral_subjectivism 