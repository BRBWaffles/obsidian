### Argument for Eating Animals

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>        |
|:-------------------------------------------:|:------------------------------------------------ |
|      <font color="CC6600">**P**</font>      | it is immoral to kill someone (x)                |
|      <font color="CC6600">**Q**</font>      | someone (x) values their life                    |
|      <font color="CC6600">**R**</font>      | someone (x) has the capacity to value their life |
|      <font color="CC6600">**a**</font>      | a given animal                                   |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> For all things, it is immoral to kill someone if, and only if, someone values their life or someone has the capacity to value their life.
<br />
<font color="CC6600">
<b>(∀x(Px↔(Qx∨Rx)))</b>
<br />
<b>P2)</b></font> A given animal do not value their life.
<br />
<font color="CC6600">
<b>(¬Qa)</b>
<br />
<b>P3)</b></font> A given animal does not have the capacity to value their life.
<br />
<font color="CC6600">
<b>(¬Ra)</b>
<br />
<b>C)</b></font> Therefore, it is not immoral to kill a given animal.
<br />
<font color="CC6600">
<b>(∴¬Pa)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Px~4(Qx~2Rx))),(~3Qa),(~3Ra)|=(~3Pa))

## Analysis

1. On what modality is it impossible for animals to value their lives?
	- an adjective
2. What is the contradiction on that modality?
	- a proposition in conjunction with its negation
3. Can you argue to that contradiction?
	- an argument with premises and a conclusion

---

# Hashtags

#debate 
#debate_opponents 