
# Debate 1

## Proposition
> "Preferences are gibberish"

## Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>               |
|:-------------------------------------------:|:------------------------------------------------------- |
|      <font color="CC6600">**P**</font>       | one's concept (x) is private                            |
|      <font color="CC6600">**E**</font>       | others can have epistemic access to one's concept (x)   |
|      <font color="CC6600">**R**</font>       | a concept (x) can have a shared referent                |
|      <font color="CC6600">**M**</font>       | the concept (x) refers to material external to the mind |
|      <font color="CC6600">**C**</font>       | a concept (x) can be communicated                       |
|      <font color="CC6600">**p**</font>       | preference                                              |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> One's concept is private if, and only if, others cannot have epistemic access to one's concept.
<br />
<font color="CC6600">
<b>(∀x(Px↔¬Ex))</b>
<br />
<b>P2)</b></font> One's concept can have a shared referent if, and only if, one's concept refers to material external to the mind.
<br />
<font color="CC6600">
<b>(∀x(Rx↔Mx))</b>
<br />
<b>P3)</b></font> One's concept can be communicated if, and only if, one's concept is not private and one's concept can have a shared referent.
<br />
<font color="CC6600">
<b>(∀x(Cx↔¬Px∧Rx))</b>
<br />
<b>P4)</b></font> Others can not have epistemic access one's preferences.
<br />
<font color="CC6600">
<b>(¬Ep)</b>
<br />
<b>P5)</b></font> One's preferences do not refer to material external to the mind.
<br />
<font color="CC6600">
<b>(¬Mp)</b>
<br />
<b>C)</b></font> Therefore, one's preferences cannot be communicated.
<br />
<font color="CC6600">
<b>(∴¬Cp)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Px~4~3Ex)),(~6x(Rx~4Mx)),(~6x(Cx~4~3Px~1Rx)),(~3Ep),(~3Mp)|=(~3Cp))

## Analysis

1) No clear reason to accept P1, P2, or P3 until the modality for possibility/impossibility is provided.
2) No clear reason to accept P4 or P5. They're just empirical claims.

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
#philosophy 
#moral_subjectivism