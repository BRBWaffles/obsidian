# Debate 1

## Proposition
> "carbohydrates cause obesity because they cause starvation."

## Arguments

### Steel Man of Gary's Argument

| <font color="CC6600">**Variable**</font> | <font color="CC6600">**Definition**</font> |
|:----------------------------------------:|:------------------------------------------ |
|    <font color="CC6600">**S**</font>     | a macronutrient (x) causes starvation      |
|    <font color="CC6600">**N**</font>     | a macronutrient (x) increases insulin      |
|    <font color="CC6600">**f**</font>     | fat                                        |
|    <font color="CC6600">**c**</font>     | carbs                                      |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> A macronutrient causes starvation if and only if a macronutrient increases insulin.
<br />
<font color="CC6600">
<b>(∀x(Sx↔Nx))</b>
<br />
<b>P2)</b></font> Fat does not increase insulin, whereas carbs do.
<br />
<font color="CC6600">
<b>(¬Nf∧Nc)</b>
<br />
<b>C)</b></font> Therefore, fat does not cause starvation, whereas carbs do.
<br />
<font color="CC6600">
<b>(∴¬Sf∧Sc)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Sx~4Nx)),(~3Nf~1Nc)|=(~3Sf~1Sc))

### Gary's Unedited Argument

| <font color="CC6600">**Variable**</font> | <font color="CC6600">**Definition**</font> |
|:----------------------------------------:|:------------------------------------------ |
|    <font color="CC6600">**S**</font>     | a macronutrient (x) causes starvation      |
|    <font color="CC6600">**N**</font>     | a macronutrient (x) increases insulin      |
|    <font color="CC6600">**H**</font>     | a macronutrient (x)  causes hunger         | 
|    <font color="CC6600">**f**</font>     | fat                                        |
|    <font color="CC6600">**c**</font>     | carbs                                      |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> A macronutrient causes starvation if and only if a macronutrient increases insulin and causes hunger.
<br />
<font color="CC6600">
<b>(∀x(Sx↔Nx∧Hx))</b>
<br />
<b>P2)</b></font> A macronutrient causes hunger if and only if a macronutrient increases insulin.
<br />
<font color="CC6600">
<b>(∀x(Nx↔Hx))</b>
<br />
<b>P3)</b></font> Fat does not increase insulin, whereas carbs do.
<br />
<font color="CC6600">
<b>(¬Nf∧Nc)</b>
<br />
<b>P4)</b></font> Fat does not cause hunger, whereas carbs do.
<br />
<font color="CC6600">
<b>(¬Hf∧Hc)</b>
<br />
<b>C)</b></font> Therefore, fat does not cause starvation, whereas carbs do.
<br />
<font color="CC6600">
<b>(∴¬Sf∧Sc)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Sx~4Nx~1Hx)),(~6x(Nx~4Hx)),(~3Nf~1Nc),(~3Hf~1Hc)|=(~3Sf~1Sc))

## Analysis

1. What the fuck does it mean to cause starvation?

---

## Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 