# Debate 1

### Allan's Argument for Holistic Management

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                     |
|:-------------------------------------------:|:------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**M**</font>      | (x) something can be managed                                                                                  |
|      <font color="CC6600">**P**</font>      | (x) something was produced                                                                                    |
|      <font color="CC6600">**C**</font>      | (x) mismanagement of nature has led to climate change                                                         |
|      <font color="CC6600">**R**</font>      | institutional policies are the root cause of climate change                                                   |
|      <font color="CC6600">**B**</font>      | institutional policy-makers have embraced complexity                                                      |
|      <font color="CC6600">**V**</font>      | we must begin taking steps to reverse desertification                                                                               |
|      <font color="CC6600">**D**</font>      | the methods discussed in (x) Allan's TEDTalk reverse desertification                                          |
|      <font color="CC6600">**G**</font>      | we must instantiate institutional policy to encourage the use of the methods discussed in (x) Allan's TEDTalk |
|      <font color="CC6600">**n**</font>      | nature                                                                                                        |
|      <font color="CC6600">**h**</font>      | holistic management                                                                                           |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If something cannot be managed, then it was produced.
<br />
<font color="CC6600">
<b>(∀x(¬◇Mx→Px))</b>
<br />
<b>P2)</b></font> Nature was not produced.
<br />
<font color="CC6600">
<b>(¬Pn)</b>
<br />
<b>P3)</b></font> If nature can be managed and the mismanagement of nature has led to climate change, then institutional policies are the root cause of climate change.
<br />
<font color="CC6600">
<b>(◇Mn∧∀x(Cx→R))</b>
<br />
<b>P4)</b></font> Desertification has led to climate change.
<br />
<font color="CC6600">
<b>(Cd)</b>
<br />
<b>P5)</b></font> If institutional policies are the root cause of climate change, then institutional policy-makers have not embraced complexity.
<br />
<font color="CC6600">
<b>(R→¬B)</b>
<br />
<b>P6)</b></font> If institutional policy-makers have not embraced complexity, then we must begin taking steps to reverse desertification.
<br />
<font color="CC6600">
<b>(¬B→V)</b>
<br />
<b>P7)</b></font> If we must begin taking steps to reverse desertification and the methods discussed in Allan's TEDTalk reverse desertification, then we must instantiate institutional policy to encourage the use of the methods discussed in Allan's TEDTalk.
<br />
<font color="CC6600">
<b>(V∧∀x(Dx→Gx))</b>
<br />
<b>P8)</b></font> Holistic management reverses desertification.
<br />
<font color="CC6600">
<b>(Dh)</b>
<br />
<b>C)</b></font> Therefore, we must instantiate institutional policy to encourage the use of holistic management.
<br />
<font color="CC6600">
<b>(∴Gh)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(~3~9Mx~5Px)),(~3Pn),(~9Mn~1~6x(Cx~5R)),(Cd),(R~5~3B),(~3B~5V),(V~1~6x(Dx~5Gx)),(Dh)|=(Gh))

---

# Hashtags

#debate 
#debate_opponents 
#agriculture 
#climate 