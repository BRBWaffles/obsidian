# Debate 1

## Proposition:

> Nick's definition is still based on a completely flimsy and inconsistent ideology.

## Questions:

1. Do you mean there's a logical contradiction?
	- yes
1. If yes, what's the contradiction?
	- couldn't say

## Link:
https://twitter.com/TheNutrivore/status/1661926888544288771?s=20

## Receipts:
![[📂 Media/Images/Pasted image 20230526130915.png]]
![[📂 Media/Images/Pasted image 20230526130924.png]]
![[📂 Media/Images/Pasted image 20230526130937.png]]
![[📂 Media/Images/Pasted image 20230526130947.png]]
![[📂 Media/Images/Pasted image 20230526131034.png]]
![[📂 Media/Images/Pasted image 20230526131109.png]]

---

# Hashtag

#debate 
#debate_opponents 
#vegan 
#philosophy 
