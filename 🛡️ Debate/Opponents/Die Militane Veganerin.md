# Debate 1

## Proposition
>"vegans kill more animals than non-vegans"

---

## Semantic Analysis

### Unclear Terms
1. **kill**
	1. intentionally causing death?
		1. directly
			1. seems false
				1. what's the evidence?
		2. indirectly
			1. what's the evidence?
	2. total animal deaths?
		1. directly
			1. false
		2. indirectly
			1. what's the evidence?
2. **vegans kill**
	1. in virtue of what?
		1. while making consumer choices?
		2. indirect causal pathways/support that leads to killing?

---

# Debate 2

## Proposition
>"vegans cause more animal suffering than non-vegans"

---

## Semantic Analysis

### Unclear Terms
1. **suffering**
	1. intentionally causing negative emotional states?
		1. directly
			1. seems false
				1. what's the evidence?
		2. indirectly
			1. what's the evidence?
2. **vegans cause more suffering**
	1. in virtue of what?
		1. while making consumer choices?
		2. indirect causal pathways/support that leads to killing?

---

# Hashtags

#debate 
#debate_opponents 
