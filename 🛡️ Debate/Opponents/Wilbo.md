## Proposition

>"Beef enhances vitality, pep, intelligence, and drive and vitality, pep, intelligence, and drive are needed in order to invent carbon capture, so beef is essential to solving climate change"

## Argument

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Vitality, pep, intelligence, and drive are needed in order to invent carbon capture.
<br />
<font color="CC6600">
<b>(V∧P∧N∧D→C)</b>
<br />
<b>P2)</b></font> Inventing carbon capture is needed to solve climate change.
<br />
<font color="CC6600">
<b>(C→L)</b>
<br />
<b>P3)</b></font> Beef leads to enhanced vitality, pep, intelligence, and drive.
<br />
<font color="CC6600">
<b>(B→V∧P∧N∧D)</b>
<br />
<b>C)</b></font> Therefore, beef is needed to solve climate change.
<br />
<font color="CC6600">
<b>(∴B→L)</b>
<br />
<br />
</font>
</div>

## Analysis

1. What's the argument for P3?

---

# Hashtags

#debate 
#meat 
#debate_opponents 