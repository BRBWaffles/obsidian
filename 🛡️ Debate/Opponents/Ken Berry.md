### Argument for Wearing Sunglasses

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                     |
|:-------------------------------------------:|:----------------------------------------------------------------------------- |
|      <font color="CC6600">**P**</font>      | prolonged exposure to high-intensity UV radiation causes eye damage           |
|      <font color="CC6600">**Q**</font>      | wearing sunglasses that block high-intensity UV radiation prevents eye damage |
|      <font color="CC6600">**R**</font>      | one (x) has the goal of preventing eye damage                                 | 
|      <font color="CC6600">**S**</font>      | one (x) is exposed to prolonged high-intensity UV radiation                   |
|      <font color="CC6600">**W**</font>      | one (x) should wear sunglasses                                                |
|      <font color="CC6600">**b**</font>      | Ken Berry                                                                     |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If prolonged exposure to high-intensity UV radiation causes eye damage, then wearing sunglasses that block high-intensity UV radiation prevents eye damage.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> If wearing sunglasses that block high-intensity UV radiation prevents eye damage and one has the goal of preventing eye damage, then if one is exposed to prolonged high-intensity UV radiation, then one should wear sunglasses.
<br />
<font color="CC6600">
<b>(∀x(Q∧Rx→(Sx→Wx)))</b>
<br />
<b>P3)</b></font> Prolonged exposure to high-intensity UV radiation causes eye damage and Ken Berry has the goal of preventing eye damage.
<br />
<font color="CC6600">
<b>(P∧Rb)</b>
<br />
<b>C)</b></font> Therefore, if Ken Berry is exposed to prolonged high-intensity UV radiation, then Ken Berry should wear sunglasses.
<br />
<font color="CC6600">
<b>(∴Sb→Wb)</b>
<br />
<br />
</font>
</div>

# Debate 1

## Proposition
> "Animal agriculture produces no unnecessary suffering."

## Argument

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If farming cows necessitates suffering, then farming cows produces no unnecessary suffering.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Farming cows necessitates suffering.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, farming cows produces no unnecessary suffering.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

## Analysis

### Reductio

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If stabbing babies necessitates suffering, then stabbing babies produces no unnecessary suffering.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Stabbing babies necessitates suffering.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, stabbing babies produces no unnecessary suffering.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree]([Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q)))

---

# Malpractice Receipts
![[📂 Media/PDFs/ME012318.pdf]]

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 