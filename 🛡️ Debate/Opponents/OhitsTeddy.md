# Debate 1

## Proposition
>"there is a causal association between mental illness and facial tattoos"

## Analysis

1. mental illness
	1. how were the diagnoses obtained?
2. from what data are you drawing the causal inference?
	1. if it's just an anecdote, what if a tattooist with equal experience reports the opposite?

## Receipts

![[Pasted image 20240420201851.png]]
![[Pasted image 20240420202003.png]]

---

# Hashtags

#debate 
#debate_opponents 
