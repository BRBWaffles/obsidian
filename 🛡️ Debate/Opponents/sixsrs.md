# Debate 1

## Proposition

> "subjecting seed oils to extreme cooking methods is one of the major issues plaguing people's health."

## Analysis

1. "major issues"
	- what proportion of total issues?
1. "people's heath"
	- what endpoints?

### Compounds tho
1. https://onlinelibrary.wiley.com/doi/abs/10.1002/ejlt.201700376
2. https://www.fda.gov.tw/upload/133/content/2013050913435287631.pdf
3. https://www.researchgate.net/publication/280786965_Effect_of_Heating_on_the_Characteristics_and_Chemical_of_Selected_Frying_Oils_and_Fats

### Cross-sectional studies tho
1. https://www.sciencedirect.com/science/article/pii/S000291652203444X

### Rodents tho
1. https://www.sciencedirect.com/science/article/abs/pii/S1537189114000536?via%3Dihub
2. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8840387/#:~:text=Epididymal%20fat%20weight%20and%20body,robustly%20decreased%20body%20fat%20accumulation

### Inflammation tho
1. https://pubmed.ncbi.nlm.nih.gov/22162245/#:~:text=The%20postprandial%20inflammatory%20response%20after,the%20presence%20of%20phenol%20compounds

### Tsimane tho
1. https://onlinelibrary.wiley.com/doi/full/10.1002/oby.22556

## Notes

Total clownery

---

# Hashtags

#debate 
#debate_opponents 

