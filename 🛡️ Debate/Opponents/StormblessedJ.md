# Debate 1

## Proposition
>"something has a soul if it has the capacity for human-level intelligence and has a consciousness"

## Semantics
1. capacity for intelligence
	- 16B neurons

---

# Debate 2

### Pepsi Monodiet Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                             |
|:-------------------------------------------:|:----------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**S**</font>      | the consumption of (x) something has only been studied in the context of consuming (y) something else |
|      <font color="CC6600">**T**</font>      | it is known what effects (x) something has in the absence of (y) something else                       |
|      <font color="CC6600">**R**</font>      | it is rational to consume a monodiet of (x) something                                                 |
|      <font color="CC6600">**m**</font>      | meat                                                                                                  |
|      <font color="CC6600">**c**</font>      | plants                                                                                                |
|      <font color="CC6600">**p**</font>      | Pepsi                                                                                                 |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the consumption of something has only been studied in the context of consuming something else, then it is not known what effects something has in the absence of something else.
<br />
<font color="CC6600">
<b>(∀x∀y(Sxy→¬Txy))</b>
<br />
<b>P2)</b></font> It is reasonable to consume a monodiet of something if and only if it is not known what effects something has in the absence of something else.
<br />
<font color="CC6600">
<b>(∀x∀y(Rx↔¬Txy))</b>
<br />
<b>P3)</b></font> The consumption of meat has only been studied in the context of plant consumption.
<br />
<font color="CC6600">
<b>(Sma)</b>
<br />
<b>P4)</b></font> The consumption of Pepsi has only been studied in the context of meat consumption.
<br />
<font color="CC6600">
<b>(Spm)</b>
<br />
<b>P5)</b></font> If is not known what effects meat has in the absence of plants, then it is not known what effects Pepsi has in the absence of meat.
<br />
<font color="CC6600">
<b>(¬Tma→¬Tpm)</b>
<br />
<b>C)</b></font> Therefore, it is rational to consume a monodiet of Pepsi.
<br />
<font color="CC6600">
<b>(∴Rp)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Sxy~5~3Txy)),(~6x~6y(Rx~4~3Txy)),(Sma),(Spm),(~3Tma~5~3Tpm)|=(Rp))

---

# Debate 1

### Argument for High Quality Research


| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                 |
|:-------------------------------------------:|:--------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**H**</font>       | A study (x) counts as high quality                                                                        |
|      <font color="CC6600">**U**</font>       | A study can be applied to anyone universally on a population level (meaning perfect external validity)                                        |
|      <font color="CC6600">**e**</font>       | An experiment with humans in a lab under constant observation and controls for life or a direct mechanism | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> A study counts as high quality if, and only if, the study can be applied to anyone universally on a population level (meaning perfect external validity).
<br />
<font color="CC6600">
<b>(∀x(Hx↔Ux))</b>
<br />
<b>P2)</b></font> An experiment with humans in a lab under constant observation and controls for life or a direct mechanism can not be applied to anyone universally on a population level (meaning imperfect external validity).
<br />
<font color="CC6600">
<b>(¬Ue)</b>
<br />
<b>P3)</b></font> An experiment with humans in a lab under constant observation and controls for life or a direct mechanism counts as high quality.
<br />
<font color="CC6600">
<b>(He)</b>
<br />
<b>C)</b></font> Therefore, it is and it not the case that an experiment with humans in a lab under constant observation and controls for life or a direct mechanism counts as high quality.
<br />
<font color="CC6600">
<b>(∴He∧¬He)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Hx~4Ux)),(~3Ue),(He)|=(He~1~3He))

---

# Hashtags

#debate
#debate_opponents 
#clowns
#clownery
