### Noah's Position

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If it is permissible to use all of an animal's body parts, then there are no non-animal body part alternatives and immediate survival turns on using the animal's body parts.
<br />
<font color="CC6600">
<b>(P→¬Q∧R)</b>
<br />
<b>P2)</b></font> There are non-animal body part alternatives.
<br />
<font color="CC6600">
<b>(Q)</b>
<br />
<b>P3)</b></font> Immediate survival does not turn using the animal's body parts.
<br />
<font color="CC6600">
<b>(¬R)</b>
<br />
<b>C)</b></font> Therefore, it is not permissible to use all of an animal's body parts.
<br />
<font color="CC6600">
<b>(∴¬P)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5~3Q~1R),(Q),(~3R)|=(~3P))

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
#vegan 