# Debate 1

## Proposition 1
>"Optimal health from a diet perspective is providing your body with all the nutrition it needs to thrive."

## Questions
1. What does it mean to thrive?
2. How is thriving measured?
3. Is it impossible to provide the body with all of these nutrients and still consume a diet that negatively impacts health?

# Debate 1

## Proposition 1
>"Optimal health from a diet perspective is providing your body with all the nutrition it needs to thrive."

## Questions
1. What does it mean to thrive?
2. How is thriving measured?
3. Is it impossible to provide the body with all of these nutrients and still consume a diet that negatively impacts health?

---

# Debate 2

## Proposition 2
>"An optimal diet does not require ANY supplementation"

## Questions
1. Is all that is meant is supplementation is not a requirement to have an optimal diet?
2. Is the claim actually that an optimal diet will never contain any supplements?

---

# Hashtags

#debate 
#debate_opponents 
#supplements 