### Ari's Rapist Bull Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|    <font color="CC6600">**P(x)**</font>     | (x) qualifies as rape                     |
|   <font color="CC6600">**Q(x,y)**</font>    | (x)  involves sexual contact with (y)     |
|   <font color="CC6600">**R(y,x)**</font>    | (y) renders informed consent for (x)      |
|      <font color="CC6600">**x**</font>      | An action                                 |
|      <font color="CC6600">**y**</font>      | an involved party                         |
|      <font color="CC6600">**a**</font>      | Ari's bull mating with Edith              |
|      <font color="CC6600">**e**</font>      | Edith                                     |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> An action qualifies as rape if, and only if, the action involves sexual contact with an involved party and the involved party does not render informed consent for the action.
<br />
<font color="CC6600">
<b>(∀x∀y(Px↔(Qxy∧¬Ryx)))</b>
<br />
<b>P2)</b></font> Ari's bull mating with Edith involves sexual contact with Edith.
<br />
<font color="CC6600">
<b>(Qae)</b>
<br />
<b>P3)</b></font> Edith does not render informed consent for Ari's bull mating with Edith.
<br />
<font color="CC6600">
<b>(¬Rea)</b>
<br />
<b>C)</b></font> Therefore, Ari's bull mating with Edith qualifies as rape.
<br />
<font color="CC6600">
<b>(∴Pa)</b>
<br />
<br />
</font>
</div>

---

# Hashtags

#debate 
#debate_opponents 
#arguments 
#vegan 