# Debate 1

## Proposition
> "People don't want to eat Beyond Meat because their stock is decreasing."

## Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>         |
|:----------------------------------------:|:-------------------------------------------------- |
|    <font color="CC6600">**P**</font>     | a firm's (x) stock has lost value                  |
|     <font color="CC6600">*Q*</font>      | a firm (x) has the highest sales in its domain (y) |
|    <font color="CC6600">**R**</font>     | a domain (y) is expected to be losing demand       |
|    <font color="CC6600">**b**</font>     | Beyond                                             |
|    <font color="CC6600">**m**</font>     | mock meat market                                   |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a firm's stock has lost value and that firm has the highest sales in its domain, then that domain is expected to be losing demand.
<br />
<font color="CC6600">
<b>(∀x∀y(Px∧Qxy→Ry))</b>
<br />
<b>P2)</b></font> Beyond's stock has lost value.
<br />
<font color="CC6600">
<b>(Pb)</b>
<br />
<b>P3)</b></font> Beyond has the highest sales in the mock meat market.
<br />
<font color="CC6600">
<b>(Qbm)</b>
<br />
<b>C)</b></font> Therefore, the mock meat market is expected to be losing demand.
<br />
<font color="CC6600">
<b>(∴Rm)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Px~1Qxy~5Ry)),(Pb),(Qbm)|=(Rm))

---

# Hashtags

#debate_opponents 
#debate 
#economics
