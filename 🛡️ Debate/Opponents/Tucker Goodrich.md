### Primitive cultures tho

<div style="text-align: center; font-weight: bold;">
Chronic Disease Claims:
</div>

> 1) Chronic diseases are not caused by known pathogens or acute toxins, and typically are found in societies that have an advanced level of agriculture, and universally in countries that have adopted industrial methods of food production; but are absent in those that have primitive methods of agriculture, or depend on hunting and gathering. As far as I am aware, there is not a single population that does not eat seed oils that suffers from the chronic diseases.

<div style="text-align: center; font-weight: bold;">
Response:
</div>

Drawing conclusions about the health value of traditional diets (or the health value of novel foods) by reference to the apparent health status of traditional cultures is an error. There are too many epistemic barriers. The details on this matter are summarized [here](https://www.the-nutrivore.com/post/should-we-eat-like-hunter-gatherers). Plus, it's [not even true](https://discord.gg/gzknNUw5hB) that ancestral or primitive populations are free of chronic disease.

Tucker's looking at ecological associations. We can also posit many things that these populations don't have access to, that modern populations with chronic disease have access to. Causal inference from this association [is dubious](https://pubmed.ncbi.nlm.nih.gov/24567587/), because it does not explore what happens to individuals in a modern context when they are actually consuming vegetable oils.

When those higher internal validity investigations are done and meta-analytically summated, we see an inverse relationship between vegetable oil intake and many chronic diseases.

### LDL Oxidation Tho

<div style="text-align: center; font-weight: bold;">
Oxidized LDL Claims:
</div>

> 1) The European Atherosclerosis Society endorses the idea that oxidized LDL is the major initiator of atherosclerosis.

<div style="text-align: center; font-weight: bold;">
Response:
</div>

For the first point, Tucker cites Figure 3 from the European Atherosclerosis Society's [second consensus statement paper](https://academic.oup.com/eurheartj/article/41/24/2313/5735221).

![[Pasted image 20220208185617.png]]

If we turn our attention to Figure 1, the first step is actually LDL particle concentration driving intimal infiltration and retention. They are very clear in their model that proteoglycan-mediated retention of LDL precedes LDL oxidation in the pathogenesis of atherosclerosis. It's also clear that aggregated LDL contribute to atherosclerosis through a separate pathway and isn't mediated by oxidative modification of LDL.

![[Pasted image 20220208185647.png]]

Tucker also leaves out the first part of the section he chooses to quote, which states that **all** LDL particles exert atherogenicity. Oxidative susceptibility is listed as a single variable.

However, the authors of the reference for this claim have [different views on the matter](https://pubmed.ncbi.nlm.nih.gov/15166804/). As they correctly state that n-6 polyunsaturated fat intake is associated with a decreased CHD risk, despite the increase in the susceptibility of LDL particles to oxidize. Meaning that despite whatever increase in LDL oxidation that is conferred through a diet higher in n-6 polyunsaturated fat, it may not be enough to offset the benefits of the n-6 polyunsaturated fat on CHD risk in the aggregate.

We can simultaneously accept that LDL oxidation is part of the causal pathway without having to grant that vegetable oils increase the risk of CHD. Furthermore, we could even accept that vegetable oils increase LDL oxidation, but we also wouldn't have to grant that vegetable oils increase the risk of CHD on that basis either. These are all different research questions, again.

> 2) M. S. Brown & Goldstein 1990, attempted to use ApoB (LDL) to induce macrophages to become foam cells, which was thought to be the first step in atherosclerosis, and thus CHD. It failed. OxLDL, however, succeeded (Steinberg et al. 1989). Which is why their paper is titled “Beyond cholesterol. Modifications of low-density lipoprotein (LDL) that increase its atherogenicity.” LDL, ApoB—what we often call ‘cholesterol’—doesn’t without oxidation induce atherosclerosis. This is the ONLY explanation we have for this process now.

The second point is a red herring. We can fully grant that oxLDL are a part of the causal pathway toward ASCVD. However, we don't also have to grant that vegetable oils would increase ASCVD risk, despite them perhaps increasing LDL oxidation. Those are two different research questions. The data that he cites here contains no information about CHD risk. Again, it is a mechanism, from which he extrapolates to risk without corroborating data.

Also, [Borén et al. 2020](https://pubmed.ncbi.nlm.nih.gov/32052833/) states very clearly that the concentration of LDL particles drives intimal retention, and that intimal retention precedes LDL oxidation in the causal pathway. To claim that oxLDL is the "only" explanation for the process is to not understand the causal model.

### Heart Disease Tho

<div style="text-align: center; font-weight: bold;">
Atherosclerosis Claims:
</div>

> 1) The beneficial effects of the Mediterranean diet on LDL oxidation are easily explained by the MUFA/PUFA balance of the diets, because oleic acid is better at replacing linoleic acid in LDL particle membranes.

<div style="text-align: center; font-weight: bold;">
Response:
</div>

To the first point, there is a study of a [fast food Mediterranean diet](https://pubmed.ncbi.nlm.nih.gov/17887946/) wherein the MUFA:PUFA ratio remained virtually identical from the pre- to post-diet phase for group A (diabetic subjects), going from 2:1 (pre-intervention) to 2.1:1 (post-intervention). The result was one of the most significant increases in the lag time to LDL oxidation I've ever observed in the literature from diet alone. This is despite an increase in PUFA.

> 2) Vitamin E has failed to show benefit for heart disease prevention because vitamin E can cause LDL to oxidize.

For the second point, This is reconcilable with the accepted pathophysiology of atherosclerosis (the response to retention hypothesis), as detailed in the European Atherosclerosis Society's landmark papers on the subject. The causal model that has been accepted by the scientists within this domain places LDL oxidation downstream in the pathway (preceded by intimal retention). Once LDL are retained, oxidation is inevitable. This is why LDL oxidation has never really panned out as a viable way to modify ASCVD risk.

Even if it were true that vitamin E can cause LDL to oxidize it would be red herring, because it's also true that high enough doses of supplemental vitamin E either [functionally abolish the effect of linoleic acid on LDL oxidation](https://pubmed.ncbi.nlm.nih.gov/8148354/) or [significantly attenuates the susceptibility of LDL to oxidize](https://pubmed.ncbi.nlm.nih.gov/10781654/). Other antioxidants like [vitamin C](https://pubmed.ncbi.nlm.nih.gov/23279831/) and [polyphenols](https://pubmed.ncbi.nlm.nih.gov/15168036/) can also lower LDL's susceptibility of oxidation.

> 3) The Lyon Diet-Heart Study is the only test of lowering linoleic acid for CVD risk improvement, and was one of the most successful diet trials ever conducted.

For the third point, Tucker attributes the 73% reduction in myocardial infarction risk observed in the [Lyon Diet-Heart Study](https://pubmed.ncbi.nlm.nih.gov/7911176/) to a 4.5g/day difference in linoleic acid, despite the fact that the trial was a multifactorial intervention that improved diet quality in multiple ways.

We have directly tested the effect of modifying the linoleic acid content of the diet on CVD risk. The [Oslo Diet-Heart Study](https://pubmed.ncbi.nlm.nih.gov/5477261/), [Los Angeles Administration Hospital Study](https://www.ahajournals.org/doi/10.1161/01.CIR.40.1S2.II-1), or the [Medical Research Council Study](https://pubmed.ncbi.nlm.nih.gov/4175085/) are all studies that specifically substituted high linoleic acid fats for saturated fat and saw reductions in CVD risk. So, why should we grant him that such a paltry contrast in exposure would produce such a massive decrease in risk, when the rest of the literature on the subject suggests that much higher intakes actually decrease risk?

But even if we entertained Tucker's notion, we can turn to the [wider body of literature](https://pubmed.ncbi.nlm.nih.gov/20351774/) seeing decreased risk with higher intakes of LA to find a defeater. This can be syllogized like this:

### Lyon Diet Heart Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If increasing LA beyond 5% of energy increases AMI risk, then increasing LA beyond 5% of energy doesn't lower AMI risk..
<br />
<font color="CC6600">
<b>(P→¬Q)</b>
<br />
<b>P2)</b></font>  Increasing LA beyond 5% of energy lowers AMI risk.
<br />
<font color="CC6600">
<b>(Q)</b>
<br />
<b>C)</b></font> Therefore,  increasing LA beyond 5% of energy doesn't increase AMI risk.
<br />
<font color="CC6600">
<b>(∴¬P)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5~3Q),(P)|=(~3Q))

> 4) One must dig deep to cherry-pick Hooper et al 2020 to support the claim that vegetable oils decrease the risk of ASCVD. All these decades of research, and we’re left with “little or no effect”. That’s the best we can do?

For next point, Tucker is the one cherry-picking [their summary](https://pubmed.ncbi.nlm.nih.gov/32428300/) in order to make his point. The summary primarily reports on mortality data, which is less sensitive than total events (which the summary states was a significant finding).

If you were to read analyses 1.35, 1.44, and 1.51, they divulge that lowering SFA lowers total CVD events, the effect is strongest when PUFA replaces SFA, and the CVD risk reduction is a function of the magnitude in serum cholesterol reduction. It is also divulged in figure 6 that serum cholesterol functions as a significant moderator variable between SFA and CVD in their meta-regression analysis. Tucker needs to unpack how this qualifies as cherry-picking.

### Mercodia Test Tho

<div style="text-align: center; font-weight: bold;">
4E6 Antibody Assay Claims:
</div>

> 1) The Mercodia 4E6 antibody assay used to measure oxidized LDL has such poor sensitivity that it essentially just provides you with a measure of ApoB, due to it making such poor distinctions between oxidized LDL and native LDL. As evidenced by the R<sup>2</sup> for oxLDL and native LDL being >0.70.

<div style="text-align: center; font-weight: bold;">
Response:
</div>

Firstly, Tucker misunderstands how the [4E6 assay works](https://www.mercodia.com/product/oxidized-ldl-elisa/#:~:text=Mercodia%20Oxidized%20LDL%20ELISA%20is,epitope%20in%20oxidized%20ApoB-100.&text=Substituting%20aldehydes%20can%20be%20produced,the%20generation%20of%20oxidized%20LDL). It captures both minimally and maximally oxidized ApoB that have a minimum number of lysine residues modified by aldehydes. The assay uses an antibody that binds to ApoB after at least 60 lysine residues have been modified. The antibody doesn't bind unless this minimum threshold of lysine residue modification has been crossed. This is significant because that is the threshold that forecloses LDL-receptor binding and opens scavenger-receptor binding.

Merely stating that oxLDL and LDL (as measured by the 4E6 assay) are strongly correlated doesn't actually mean that the 4E6 test is invalid and making poor distinctions between oxLDL and LDL. This is a similar mistake to one [that Tsimikas made](https://www.ahajournals.org/doi/full/10.1161/01.CIR.0000164264.00913.6D?related-urls=yes&legid=circulationaha%3B111%2F18%2Fe284) while defending his preference for a different assay, the E06 antibody assay. It's not clear how Tucker or Tsimikas come to their conclusions, because all it could mean is that these potentially distinct markers tend to covary.

It is expected that oxLDL (as measured by 4E6) would correlate well with ApoB if it is also detecting trivially oxidized LDL. This shouldn't be revelatory to us, and again, a high correlation doesn't demonstrate low sensitivity for the test. Tucker needs to explain why a high correlation between variables entails low sensitivity for a test that aims to make distinctions between them. This is like saying that shoe size is a poor measurement of foot size because foot size and height are tightly correlated. Like, what? That doesn't even make any sense. 

> 2) The conclusion of (Wu et al., 2006) is correct, but the results are meaningless as to causation, as they are adjusting a measure of oxLDL by itself, effectively. Of course there is no additional correlation.

Secondly, oxLDL is not being adjusted by itself, and it is absurd to think that it is. That actually doesn't make any sense. They were testing the explanatory power of each marker in a mutually adjusted model. The results of those mutual adjustments contradict the hypothesis that the 4E6 assay makes poor distinctions between oxLDL and native LDL.

If the 4E6 assay was truly making poor distinctions between oxLDL and native LDL, the results of the LRT by [Wu et al. (2006)](https://pubmed.ncbi.nlm.nih.gov/16949489/) would suggest extreme multicollinearity as indicated [similarly (enormously) wide confidence intervals](https://statisticsbyjim.com/regression/multicollinearity-in-regression-analysis/) for both results. If oxLDL and native LDL were proxying for one another in the model, we'd expect the confidence intervals for each relative risk to be inflated and more likely non-significant because of how the standard error would be effected. Instead of seeing this, we actually see evidence of independence and precision between oxLDL (as measured by 4E6) and ApoB.

On balance of probabilities, within that dataset it is not only less likely to be the case that risk is more closely tracking oxLDL, there is also no evidence for it and evidence against it.

The entire argument can be expressed syllogistically with modus tollens:

### 4E6 Sensitivity Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If extreme multicollinearity is characterized by low precision and occurs when proxies are included in the same model and the 4E6 antibody assay poorly distinguishes between oxLDL and ApoB, then including oxLDL and ApoB in the same model produces evidence of extreme multicollinearity.
<br />
<font color="CC6600">
<b>(P∧Q→R)</b>
<br />
<b>P2)</b></font> Extreme multicollinearity is characterized by low precision and occurs when proxies are included in the same model.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> Including oxLDL and ApoB in the same model does not produce evidence of extreme multicollinearity.
<br />
<font color="CC6600">
<b>(¬R)</b>
<br />
<b>C)</b></font> Therefore, the 4E6 antibody assay does not poorly distinguish between oxLDL and ApoB.
<br />
<font color="CC6600">
<b>(∴¬Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~1Q~5R),(P),(~3R)|=(~3Q))

Lastly, If oxPL and ApoB tend to correlate, then regression analysis may not find a significant correlation between oxPL/ApoB and ApoB. This is because the ratio would remain largely constant across the spectrum of ApoB. But again, it's not as though the correlation between markers contains any information about the sensitivity of the tests used to make distinctions between those markers. This seems to be an error that Tucker consistently makes.

### Lung Cancer Tho

<div style="text-align: center; font-weight: bold;">
Lung Cancer Claims:
</div>

> 1) The IARC has a 95-page monograph on cooking oils and lung cancer.

<div style="text-align: center; font-weight: bold;">
Response:
</div>

It is important to emphasize that the [IARC monograph](https://www.ncbi.nlm.nih.gov/books/NBK385523/pdf/Bookshelf_NBK385523.pdf) only contains case-control studies. Case-control studies are retrospective in nature and cannot be used to assess temporality. This means that they are extremely ill-equipped to inform causal inference. The associations may be interesting, but they are not consistently in the direction Tucker would presuppose.

In most of the analyses, it is unknown if vegetable oils are truly the source of the "fumes" or "kitchen smoke" that they are discussing. To try to get around this, we can aggregate all of the data that was specific to cooking with vegetable oils. In an attempt to make sure that lower PUFA oils were always the comparator, we will also need to invert some of the risk ratios. Here are the results:

**Random-Effects Model**
![[Pasted image 20220208205838.png]]

**Fixed-Effects Model**
![[Pasted image 20220208205920.png]]

In the aggregate, cooking with higher PUFA oils results in a non-significant decrease in lung cancer risk. Random Effects: RR 0.93 (CI 0.68-1.27), P=0.64. Fixed Effects: RR 0.91 (CI 0.78-1.05), P=0.20.

Neither of these results should cause us to run for the hills when we see a deep fryer. Chances are good that the results of these case-control studies are tracking some other exposure. Like kang use, coal stoves, wood stoves, overconsumption, etc. Especially since two studies showed an increased risk of boiling food, with one finding being significant and the other being non-significant.

There is also a [meta-analysis of prospective cohort studies](https://pubmed.ncbi.nlm.nih.gov/24925369/) investigating the relationship between PUFA and lung cancer, which showed a linear, non-significant decrease in risk with higher intakes.

![[Pasted image 20220209180218.png]]

However, many of the included risk ratios were specific to fish. If we limit the risk ratios to just those that investigated total PUFA, we see no significant association with lung cancer risk.

![[PUFAlungcancer.png]]

### Shitty Replication Tho

<div style="text-align: center; font-weight: bold;">
Nutritional Epidemiology Claims:
</div>

> 1) Findings in nutritional epidemiology tend not to replicate, because the data is terrible quality.

<div style="text-align: center; font-weight: bold;">
Response:
</div>

To support this claim, Tucker cites a paper by [Young and Karr (2011)](https://rss.onlinelibrary.wiley.com/doi/10.1111/j.1740-9713.2011.00506.x) as evidence that nutritional epidemiology and RCTs agree 0% of the time. However, if you read it, they investigated "claims of causation", rather than comparing effect sizes and directionality head to head. This is probably the least rigorous way to investigate this.

Rather, there have been two actual systematic investigations into rates of concordance between nutritional epidemiology and randomized controlled trials. In both analyses, actual treatment effects from RCTs were compared to observed associations in nutritional epidemiology.

One compared [34 associations](https://pubmed.ncbi.nlm.nih.gov/23700648/) that had matched RCT data. The other compared [97 diet-disease outcome pairs](https://pubmed.ncbi.nlm.nih.gov/34526355/), which included over 950 trials and over 750 cohort studies for their comparison analysis. Overall their findings were that nutritional epidemiology agrees with RCT data between 65-67% of the time.

These are more robust, rigorous analyses than the one that Tucker cited, and therefore they supersede Tucker's reference. Not only that but we should have more confidence in them by virtue of the fact that their findings replicated well despite their study pools differing drastically.

Here is how this position can be defended syllogistically:

### Epi Bad Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If comparing effect sizes to effect sizes is more apples-to-apples than comparing expert opinions to effect sizes, and 950 is a larger sample size than either 12 or 52, and 34 is a larger number of exposures than 11, then the Schwingshackl paper has better methods and a larger sample size than the Young and Karr paper.
<br />
<font color="CC6600">
<b>((P∧Q∧R)→S)</b>
<br />
<b>P2)</b></font> Comparing effect sizes to effect sizes is more apples-to-apples than comparing expert opinions to effect sizes.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> 950 is a larger sample size than either 12 or 52.
<br />
<font color="CC6600">
<b>(Q)</b>
<br />
<b>P4)</b></font> 97 is a larger number of exposures than 11.
<br />
<font color="CC6600">
<b>(R)</b>
<br />
<b>C)</b></font> Therefore, the Schwingshackl paper has better methods and a larger sample size than the Young and Karr paper.
<br />
<font color="CC6600">
<b>(∴S)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#((P~1Q~1R)~5S),(P),(Q),(R)|=(S))

As a funny aside, even if he was correct it would make nutrition epidemiology extremely robust. Because a truly 0% replication rate would mean that all you would have to do is do the opposite of whatever nutritional epidemiology said in order to get the right answer.

If Tucker decides to argue against epidemiology in favour of interventional research on the basis of confounding variables in epidemiology, we can run these arguments on him to check his consistency. If he accepts the premises for both inferences, then he needs to provide interventional evidence to validate every confounder.

### Experimental Evidence Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If experimental evidence is required to demonstrate causality and confounding is a causal concept, then experimental evidence is required to validate potential confounders.
<br />
<font color="CC6600">
<b>(P∧Q→R)</b>
<br />
<b>P2)</b></font> Experimental evidence is required to demonstrate causality.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>P3)</b></font> Confounding is a causal concept.
<br />
<font color="CC6600">
<b>(Q)</b>
<br />
<b>C)</b></font> Therefore, experimental evidence is required to validate potential confounders.
<br />
<font color="CC6600">
<b>(∴R)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~1Q~5R),(P),(Q)|=(R))

Tucker has also taken the position that linoleic acid might be the only dietary component that matters for chronic disease outcomes.

> Compared to what? I don't think it matters, even for TFA. That's a huge potential side-issue, so maybe we should just leave that for another time. It's highly confounded, as most TFA came along w/ LA, so hard to discern... But I feel the data is there, so we can include.

This position can be syllogized as such:

### Confounding Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the dietary component is confounding, then the dietary component is linoleic acid.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font>  The dietary component is not linoleic acid.
<br />
<font color="CC6600">
<b>(¬Q)</b>
<br />
<b>C)</b></font> Therefore, the dietary component is not confounding.
<br />
<font color="CC6600">
<b>(∴¬P)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(~3Q)|=(~3P))

### Liver Lat Tho

<div style="text-align: center; font-weight: bold;">
Fatty Liver Claims:
</div>

> 1) Linoleic acid containing intravenous lipid emulsions cause liver failure or liver dysfunction in children on total parenteral nutrition.

<div style="text-align: center; font-weight: bold;">
Response:
</div>

For the first point, within this body of literature the primary hypothesis that has been put forth to explain the cholestasis observed with soybean oil based lipid emulsions [implicates phytosterols](https://pubmed.ncbi.nlm.nih.gov/9437703/) as the primary driver.

When you match the infusion rate of phytosterol-containing fish oil based IVLEs to that of the highest allowable levels of Intralipid, such that the stigmasterol infusion rate is matched, you see [the pathology in mice](https://pubmed.ncbi.nlm.nih.gov/24107776/). Meaning that this is likely not a linoleic acid effect, but rather a phytosterol effect. 

> 2) Linoleic acid induces non-alcoholic fatty liver disease.

As for the second point, Tucker prefers to cite two studies. In the [first study](https://pubmed.ncbi.nlm.nih.gov/32652034/) youths were placed on a diet with a "low" n-6/n-3 ratio. However, exploring their supplementary materials reveals that n-6 wasn't modified, with n-3 was increased instead.

![[Pasted image 20220209154104.png]]
![[Pasted image 20220209154326.png]]

In the [second study](https://pubmed.ncbi.nlm.nih.gov/26408952/), the intervention was uncontrolled and subjects actually lost enough weight that they went from being clinically obese to clinically overweight. The independent effect of linoleic acid is not divulged here.

In fact, we have trials directly testing the effects of altering dietary fatty acid composition on liver fat accumulation. In the context of [overfeeding](https://pubmed.ncbi.nlm.nih.gov/24550191/), linoleic acid resists liver fat accumulation. In the context of [eucaloric feeding](https://pubmed.ncbi.nlm.nih.gov/22492369/), linoleic acid reduces liver fat. 

### Endocannabinoids Tho

<div style="text-align: center; font-weight: bold;">
Obesity Claims:
</div>

> 1) Linoleic acid induces obesity through endocannabinoid activation in the brain, mediated by changes in 2-arachidonoylglycerol.

<div style="text-align: center; font-weight: bold;">
Response:
</div>

For the first point, according to the secondary endpoint analysis done by [Hooper et al. 2020](https://pubmed.ncbi.nlm.nih.gov/32428300/) with the Cochrane Collaboration, Olso Diet-Heart saw a 2.5kg reduction in body weight during their study period, whereas the Medical Research Council saw no change in body weight as well.

Additionally, the LA Veterans trial did not observe significant differences in body weight by eight years of 40g/day of linoleic acid in the experimental group. The Sydney Diet Heart Study also saw reductions in body weight on a similarly high linoleic acid diet.

The acute effects of high linoleic acid diets have [also been investigated multiple times](https://pubmed.ncbi.nlm.nih.gov/20492735/). In aggregate, there is no significant effect of varying meal fatty acid composition on postprandial satiety, appetite, or energy intake.

> 2) Rimonabant provides an open-and-shut case for the obesogenic effect of linoleic acid in humans, because it blocks the effects of 2-AG on the endocannabinoid system.

To the second point, while it is true that this drug does appear to reduce energy intake and result in weight loss that is equal to just over a [quarter-pound per week](https://pubmed.ncbi.nlm.nih.gov/17054276/), CB-antagonists such as Rimonabant are not specifically targeting LA metabolism. All this research tells us is that the endocannabinoid system is involved with the regulation of body weight in humans, but it does not tell us what independent contribution of vegetable oil.

Why doesn't Tucker also hold this belief for oleic acid increasing weight via oleamide? Oleamide is an endocannabinoid that is synthesized in the human body from oleic acid (similarly to how 2-AG is synthesized from arachidonic acid), and is the primary fatty acid found in olive oil. Rodents dosed with oleamide tend to [become lethargic](https://pubmed.ncbi.nlm.nih.gov/14615880/) and [increase their energy intake](https://pubmed.ncbi.nlm.nih.gov/15193744/). It is also true that experimental diets high in oleic acid have been shown to [induce obesity in rodents](https://pubmed.ncbi.nlm.nih.gov/9627373/). Lastly, olive oil consumption has [skyrocketed in the United States since 1983](https://www.researchgate.net/publication/26524031_Olive_and_olive_pomace_oil_packing_and_marketing). This is around the same time that the American obesity epidemic [first began](https://www.niddk.nih.gov/health-information/health-statistics/overweight-obesity). Interestingly, because oleamide is [also a cannabinoid receptor-1 agonist](https://pubmed.ncbi.nlm.nih.gov/14707029/) and inhibits the action of both oleamide and 2-AG, the implications of Rimonabant apply equally to this hypothesis as they do to the hypothesis that implicates linoleic acid in obesity.

If the same sort of mechanistic hypothesis can be formed using evidence that is largely equal in quality to the evidence that was used to build the case against dietary linoleic acid, why doesn't Tucker believe that olive oil can cause obesity?

Given his claims about the endocannabinoid system, we could get him to bite the bullet on olive oil causing obesity. Here's how that could be syllogized:

Finally, for the third point, this observation can be reconciled with the [conventional paradigm of obesity](https://academic.oup.com/ajcn/advance-article/doi/10.1093/ajcn/nqac031/6522166), which posits that both access to abundant hyperpalatable food and other environmental factors converge to activate certain key systems within the human brain that drive overconsumption. The association is also superseded by experimental evidence showing that high vegetable oil diets don't seem to cause weight gain.

### Obesogenic Olive Oil Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                |
|:-------------------------------------------:|:-------------------------------------------------------- |
|      <font color="CC6600">**C**</font>       | elevating CB1 agonists (x) promotes hyperphagia          |
|      <font color="CC6600">**V**</font>       | (y) foods that elevate CB1 agonists promotes weight gain |
|      <font color="CC6600">**o**</font>       | olive oil                                                |
|      <font color="CC6600">**m**</font>       | oleamide                                                 |
|      <font color="CC6600">**s**</font>       | seed oils                                                |
|      <font color="CC6600">**a**</font>       | 2-AG                                                     | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If elevating CB1 agonists promotes hyperphagia, then foods that elevate CB1 agonists promotes weight gain.
<br />
<font color="CC6600">
<b>(∀x∀y(Hx→Wy))</b>
<br />
<b>P2)</b></font> Elevating oleamide promotes hyperphagia.
<br />
<font color="CC6600">
<b>(Hm)</b>
<br />
<b>P3)</b></font> Elevating 2-AG promotes hyperphagia.
<br />
<font color="CC6600">
<b>(Ha)</b>
<br />
<b>P4)</b></font> If seed oils promote weight gain, then olive oil promotes weight gain.
<br />
<font color="CC6600">
<b>(Ws→Wo)</b>
<br />
<b>C)</b></font> Therefore, olive oil promotes weight gain.
<br />
<font color="CC6600">
<b>(∴Wo)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Hx~5Wy)),(Hm),(Ha),(Ws~5Wo)|=(Wo))

## Notes

1. Tucker affirms P2 on his [blog](https://yelling-stop.blogspot.com/2021/11/does-linoleic-acid-induce-obesity.html).
![[Pasted image 20220307113948.png]]
2. Tucker is also pro olive oil.
![[Pasted image 20220307115137.png]]
![[Pasted image 20220307115615.png]]

---

# Hashtags
  
#debate 
#debate_opponents 
#clowns 
#clownery
#tucker_goodrich 