# Debate 3

## Proposition

>"On Nick's definition it is and is not the case that Nick is Intentionally conspiring to cause harm."

## Notes

Conceded he was a gibberish generator
![[📂 Media/Images/Pasted image 20230822151303.png]]

# Debate 2

## Proposition

>"Veganism is attacking food security."

## Argument

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> In order to be secure in our existence, we need food.
<br />
<font color="CC6600">
<b>(A)</b>
<br />
<b>P2)</b></font> There are finite amounts of resources.
<br />
<font color="CC6600">
<b>(B)</b>
<br />
<b>P3)</b></font> Population expansion is at 67 million (with a 2% multiplier) annually.
<br />
<font color="CC6600">
<b>(C)</b>
<br />
<b>P4)</b></font> Humans have morphological characteristics that inhibit us from digesting unlockable carbs such as langan and cellulose.
<br />
<font color="CC6600">
<b>(D)</b>
<br />
<b>P5)</b></font> 70% of the global surface is water.
<br />
<font color="CC6600">
<b>(E)</b>
<br />
<b>P6)</b></font> There is no structure for monocropping on water bodies.
<br />
<font color="CC6600">
<b>(F)</b>
<br />
<b>P7)</b></font> There are waste products involved in animal agriculture.
<br />
<font color="CC6600">
<b>(G)</b>
<br />
<b>P8)</b></font> 86% of waste manufacture products are fed back to livestock.
<br />
<font color="CC6600">
<b>(H)</b>
<br />
<b>P9)</b></font> The waste products are not fit for human consumption.
<br />
<font color="CC6600">
<b>(I)</b>
<br />
<b>P10)</b></font> Pigs cecums are longer than human rectums.
<br />
<font color="CC6600">
<b>(J)</b>
<br />
<b>P11)</b></font> Poultry gizzards can absorb more mineral content than humans.
<br />
<font color="CC6600">
<b>(K)</b>
<br />
<b>P12)</b></font> The phosphorous that poultry can absorb would kill a human.
<br />
<font color="CC6600">
<b>(L)</b>
<br />
<b>P13)</b></font> Copper is deadly to sheep.
<br />
<font color="CC6600">
<b>(M)</b>
<br />
<b>C)</b></font> Therefore, veganism is attacking food security.
<br />
<font color="CC6600">
<b>(∴N)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(A),(B),(C),(D),(E),(F),(G),(H),(I),(J),(K),(L),(M)|=(N))

# Debate 2

## Proposition

>"The international case studies are conclusive in their findings that dairy maximizes growth potential"

## Analysis
1. same attitude toward the relationship between smoking and longevity?
	1. if no, why?

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
#environment 
#vegan 