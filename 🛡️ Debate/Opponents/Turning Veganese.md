# Debate 1

## Proposition

>"Killing odd-order predators is incompatible with veganism."


## Semantics
1. Veganism 
	- an ethical stance against all forms human to non-human animals oppression."
2. Opression
	- use and/or abuse and/or exploitation.

## Analysis

1. Is it oppressive to let the murderers kill and eat a woman?
	- ""
2. Is it oppressive to let the lions kill and eat a woman?
	- ""
3. Is it oppressive to let the lions kill and eat a gazelle?
	- ""

### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                             |
|:-------------------------------------------:|:--------------------------------------------------------------------- |
|      <font color="CC6600">**V**</font>      | something (x) is vegan                                                |
|      <font color="CC6600">**E**</font>      | something (x) does not involve human on non-human animal exploitation |
|      <font color="CC6600">**P**</font>      | the products (x) generated are not vegan                              |
|      <font color="CC6600">**b**</font>      | bees pollinating human crops                                          |
|      <font color="CC6600">**a**</font>      | apples                                                                | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Something is vegan if, and only if, something does not involve human on non-human animal exploitation.
<br />
<font color="CC6600">
<b>(∀x(Vx↔¬Ex))</b>
<br />
<b>P2)</b></font> Bees pollinating human crops involves human on non-human exploitation.
<br />
<font color="CC6600">
<b>(Eb)</b>
<br />
<b>P3)</b></font> If bees pollinating human crops is not vegan, then the products generated are not vegan.
<br />
<font color="CC6600">
<b>(¬Vb→∀x(¬Px))</b>
<br />
<b>C)</b></font> Therefore, apples are not vegan.
<br />
<font color="CC6600">
<b>(∴¬Pa)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Vx~4~3Ex)),(Eb),(~3Vb~5~6x(~3Px))|=(~3Pa))

---

# Hashtags

#debate 
#debate_opponents 
#vegan 
