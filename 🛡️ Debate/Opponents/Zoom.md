# Debate 1

## Proposition
>"veganism is immoral."

## Argument

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                 |
|:-------------------------------------------:|:--------------------------------------------------------- |
|      <font color="CC6600">**C**</font>      | one has an obligation to convince others of something (x) |
|      <font color="CC6600">**M**</font>      | something (x) is a moral obligation                       |
|      <font color="CC6600">**O**</font>      | it is immoral to convince others of something (x)         |
|      <font color="CC6600">**I**</font>      | something (x) is immoral                                  |
|      <font color="CC6600">**v**</font>      | veganism                                                  |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> One has an obligation to convince others of something if and only if that something is a moral obligation.
<br />
<font color="CC6600">
<b>∀x(Cx↔Mx)</b>
<br />
<b>P2)</b></font> It is immoral to convince others of something if and only if that something is not a moral obligation.
<br />
<font color="CC6600">
<b>∀x(Ox↔¬Mx</b>
<br />
<b>P3)</b></font> Veganism is not a moral obligation.
<br />
<font color="CC6600">
<b>(¬Mv)</b>
<br />
<b>P4)</b></font> If it is immoral to convince others of veganism, then veganism is immoral.
<br />
<font color="CC6600">
<b>()</b>
<br />
<b>C)</b></font> Therefore, veganism is immoral.
<br />
<font color="CC6600">
<b>(∴Iv)</b>
<br />
<br />
</font>
</div>

## Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:----------------------------------------:|:------------------------------------------ |
|       <font color="CC6600">**C**</font>       |  one has an obligation to convince others of something (x)                                          | 
|       <font color="CC6600">**M**</font>       |   something (x) is a moral obligation                                            |
|       <font color="CC6600">**b**</font>       |     abstaining from baby rape                                        |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> One has an obligation to convince others of something if and only if that something is a moral obligation..
<br />
<font color="CC6600">
<b>(∀x(Cx↔Mx))</b>
<br />
<b>P2)</b></font> Abstaining from baby rape is a moral obligation..
<br />
<font color="CC6600">
<b>(Mb)</b>
<br />
<b>C)</b></font> Therefore, one has an obligation to convince others to abstain from baby rape.
<br />
<font color="CC6600">
<b>(∴Cb)</b>
<br />
<br />
</font>
</div>

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
