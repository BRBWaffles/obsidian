# Debate 1

## Proposition
>Animal products are inherently unhealthy

## Semantic Analysis
1. "inherently"
	1. necessary
		1. modal
2. 

### Unclear Terms
1. "inherently"
2. "unhealthy"

## Clarified Proposition
>Animal foods tend to cause harm

## Questions
1. "unhealthy"
2. "detrimental"
	1. tending to cause harm

1. mechanisms tho
	1. 
---

# Debate 2

## Proposition
>Industry funding is a good reason to just dismiss studies regardless

## Semantic Analysis

### Unclear Terms
1. "good"
2. "dismiss"

## Clarified Proposition
>

## Questions
1. 

---

# Debate 3

## Proposition
>Animal products and refined food use is risky because it can encourage habituation

## Semantic Analysis

### Unclear Terms
1. "habituation"
2. "risky"

## Clarified Proposition
>

## Questions
1. 

---

# Hashtags

#debate 
#debate_opponents 
