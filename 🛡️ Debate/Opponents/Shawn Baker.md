# Debate 1

## Internal Critique of Shawn's Trait

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                         |
|:-------------------------------------------:|:--------------------------------------------------------------------------------- |
|      <font color="CC6600">**P**</font>      | beings (x) are morally permissible to kill for food the way we kill cows for food | 
|      <font color="CC6600">**H**</font>      | beings (x) are human (homo sapiens sapiens)                                   |
|      <font color="CC6600">**e**</font>      | homo erectus                                                                      |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If beings are not morally permissible to kill for food the way we kill cows for food, then the beings are human (homo sapiens sapiens).
<br />
<font color="CC6600">
<b>(∀x(¬Px→Hx))</b>
<br />
<b>P2)</b></font> Homo erectus are not human.
<br />
<font color="CC6600">
<b>(¬He)</b>
<br />
<b>C)</b></font> Therefore, homo erectus are morally permissible to kill for food the way we kill cows for food.
<br />
<font color="CC6600">
<b>(∴Pe)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(~3Px~5Hx)),(~3He)|=(Pe))

---

# Hashtags

#clowns 
#clownery 
#debate_opponents 
#debate 
#carnivore 