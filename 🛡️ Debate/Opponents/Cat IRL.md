# Debate 1

## Proposition
> "Plants don't want to be eaten because they can get scared, stressed, or horny"

## Analysis
1. plants can get scared, stressed, or horny
2. fear: tries to protect itself by making itself taste like shit
3. stress: chemical communication to other X
4. horny: has a means of reproduction
5. want: inclination or movement toward something
6. clock: has an internal clock

### Sentience Reductio

| <font color="CC6600">**Variable**</font> | <font color="CC6600">**Definition**</font>                |
|:----------------------------------------:|:--------------------------------------------------------- |
|    <font color="CC6600">**F**</font>     | something (x) try to protect itself                       |
|    <font color="CC6600">**S**</font>     | something (x) communicates chemically to other things (x) |
|    <font color="CC6600">**H**</font>     | something (x) has a means of reproduction                 |
|    <font color="CC6600">**W**</font>     | something (x) has an inclination toward something         |
|    <font color="CC6600">**E**</font>     | something (x) is sentient                                 |
|    <font color="CC6600">**C**</font>     | something (x) behaves according to an internal clock      | 
|    <font color="CC6600">**g**</font>     | euglena                                           |

<div style="text-align: center">
<font color="CC6600">
<b>P1) </b></font>If something tries to protect itself and communicates chemically to other things and has a means of reproduction and an inclination toward something and behaves according to an internal clock, then something is sentient.
<br />
<font color="CC6600">
<b>(∀x(Fx∧Sx∧Hx∧Wx∧Cx→Ex))</b>
<br />
<b>P2) </b></font>Euglena tries to protect themselves.
<br />
<font color="CC6600">
<b>(Fg)</b>
<br />
<b>P3) </b></font>Euglena communicate chemically to other euglena.
<br />
<font color="CC6600">
<b>(Sg)</b>
<br />
<b>P4) </b></font>Euglena have a means of reproduction.
<br />
<font color="CC6600">
<b>(Hg)</b>
<br />
<b>P5) </b></font>Euglena have an inclination toward something.
<br />
<font color="CC6600">
<b>(Wg)</b>
<br />
<b>P6) </b></font>Euglena behaves according to an internal clock.
<br />
<font color="CC6600">
<b>(Cg)</b>
<br />
<b>C) </b></font>Therefore, euglena are sentient.
<br />
<font color="CC6600">
<b>(∴Eg)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Fx~1Sx~1Hx~1Wx~1Cx~5Ex)),(Fg),(Sg),(Hg),(Wg),(Cg)|=(Eg))

---

# Hashtags

#clowns 
#clownery 
#debate 
#debate_opponents 
#vegan 