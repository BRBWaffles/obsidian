### Shitty Argument Against Ethical Slurs

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If a derogatory attitude is morally defective, then it is pro tanto wrong to express that attitude.
<br />
<font color="CC6600">
<b>(∀x((Dx∧Mx)→WEx))</b>
<br />
<b>P2)</b></font> Derogatory attitudes shaped by harmful ideologies are morally defective.
<br />
<font color="CC6600">
<b>∀x(Dx→Mx)</b>
<br />
<b>P3)</b></font> Slurring involves expressing a derogatory attitude in an offensive sociolinguistic register shaped by harmful ideologies, whether or not the speaker possesses the derogatory attitude in question.
<br />
<font color="CC6600">
<b>(∀x(Sx→∃y(Dy∧Ey)))</b>
<br />
<b>P4)</b></font> If an act of slurring action-expresses a derogatory attitude, and that derogatory attitude is morally defective, then the act of slurring is pro tanto wrong.
<br />
<font color="CC6600">
<b>(∀x((Sx∧∃y(Dy∧Ey∧My))→WEx))</b>
<br />
<b>P5)</b></font> All acts of slurring represent action-expressions of derogatory attitudes that are morally defective.
<br />
<font color="CC6600">
<b>(∀x(Sx→∃y(Dy∧Ey∧My)))</b>
<br />
<b>C)</b></font> Therefore, all acts of slurring are pro tanto wrong.
<br />
<font color="CC6600">
<b>(∴ (∀x(Sx→∃y(Dy∧Ey∧My))∧∀x((Dx∧Mx)→WEx)→∀x(Sx→WEx)))</b>
<br />
<br />
</font>
</div>

### Valid Reformalization of the Shitty Argument Against Ethical Slurs

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>    |
|:-------------------------------------------:|:-------------------------------------------- |
|      <font color="CC6600">**D**</font>      | an attitude is derogatory                    |
|      <font color="CC6600">**M**</font>      | an attitude is morally defective             |
|      <font color="CC6600">**W**</font>      | it is pro tanto wrong to express an attitude | 
|      <font color="CC6600">**H**</font>      | an attitude was shaped by harmful ideologies |
|      <font color="CC6600">**S**</font>      | expressing an attitude via a slur            |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font>  If an attitude is derogatory and morally defective, then it is pro tanto wrong to express that attitude.
<br />
<font color="CC6600">
<b>(D∧M→W)</b>
<br />
<b>P2)</b></font> If an attitude is derogatory and was shaped my harmful ideologies, then it is morally defective.
<br />
<font color="CC6600">
<b>(D∧H→M)</b>
<br />
<b>P3)</b></font> If an attitude is expressed via a slur, then this attitude is derogatory and was shaped by harmful ideologies.
<br />
<font color="CC6600">
<b>(S→D∧H)</b>
<br />
<b>C)</b></font> Therefore, if an attitude is expressed via a slur, then it is pro tanto wrong to express that attitude.
<br />
<font color="CC6600">
<b>(∴S→W)</b>
<br />
<br />
</font>
</div>

## Receipts

![[Pasted image 20241021152013.png]]
![[Pasted image 20241021160214.png]]
![[Pasted image 20241021160221.png]]

# Debate 1

## Proposition
> "Plants have a general tendency to perform an action in ways that is consistent with their self organization."

## Argument

### Criteria for moral value

1. Autopoiesis
	- self-organization
	- self-maintenance
	- self-regulation 
	- comprising a network of interrelated processes 
	- capable of renewing itself by regulating its composition and maintenance of form
	- creating its own parts 
	- homeostasis

## Semantics

1. Homeostasis
	- the tendency toward a relatively stable equilibrium between interdependent elements
2. Tendency
	- an propensity to a particular characteristic.

## Analysis

### Reductio

| **Criteria**            | **Atom** |
| ----------------------- | -------- |
| Tendency                | Yes      |
| Equilibrium             | Yes      |
| Interdependent elements | Yes      |

1. Even atoms would qualify as having moral value.

---

## Hashtags

#debate 
#philosophy 
#sentience 
#autopoiesis
#morality
#debate_opponents
