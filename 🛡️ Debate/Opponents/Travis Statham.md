# Debate 1

## Proposition
> "It is more reasonable to believe that seed oils are not a significant, independent concern for the development of cardiovascular disease and/or cancer and/or type 2 diabetes and/or non-alcoholic fatty liver disease and/or obesity than it is to believe the opposite."

## Notes
The subject has [declined](https://twitter.com/The_Nutrivore/status/1533109830436757510?s=20&t=bzh2Z_94W32IpNNdez6OiQ) the debate invite, stating that he does not have enough knowledge on the subject to debate it with me. So maybe someone else would enjoy stepping up to the plate instead.

## Receipts

![[📂 Media/Images/Pasted image 20220513172043.png]]
![[📂 Media/Images/Pasted image 20220513152753.png]]

---

# Hashtags
 
#debate 
#debate_opponents
#clowns 
#clownery
#travis_statham  
