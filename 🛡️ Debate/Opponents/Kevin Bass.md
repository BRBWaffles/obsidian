### Internal Critique For Kevin's Argument Against Seed Oils

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                 |
|:-------------------------------------------:|:--------------------------------------------------------- |
|      <font color="CC6600">**F**</font>      | people should go out of their way to consume the food (x) |
|      <font color="CC6600">**N**</font>      | the food (x) has been studied for all health outcomes     |
|      <font color="CC6600">**P**</font>      | the food (x) is a novel food product                      | 
|      <font color="CC6600">**s**</font>      | protein powder                                            |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If people should go out of their way to consume the food, then the food has been studied for all health outcomes and the food is not a novel food product.
<br />
<font color="CC6600">
<b>(∀x(Fx→(Nx∧¬Px)))</b>
<br />
<b>P2)</b></font> Protein powder has not been studied for all health outcomes.
<br />
<font color="CC6600">
<b>(¬Ns)</b>
<br />
<b>P3)</b></font> Protein powder is a novel food product.
<br />
<font color="CC6600">
<b>(Ps)</b>
<br />
<b>C)</b></font> Therefore, people should not go out of their way to consume protein powder..
<br />
<font color="CC6600">
<b>(∴¬Fs)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Fx~5(Nx~1~3Px))),(~3Ns),(Ps)|=(~3Fs))

---

# Taurine Fuckery
![[Pasted image 20220212202913.png]]

---

# Hashtags

#debate 
#debate_opponents
#nutrient_deficiency 
#clowns 
#clownery
#taurine
#animal_foods 
#vegan 