# Debate 1

## Proposition
>"we shouldn't be involved in influencing animals."

## ~~Definition of Veganism 1~~
>~~"veganism calls for the separation of humans and animals."~~

#### Questions
1. cool with the perpetual carnage machine so long as humans aren't involved?
	1. answer: yes

### ~~Definition of Rights 1:~~
>~~"a mutual contract between humans."~~

#### Questions
1. do schmumans have rights?
	1. answer: no
2. do retarded humans have rights?
	1. answer: no

## Definition of Veganism 2
>"veganism calls not influencing animals in negative ways."

### Definition of Negative:
>"to be negative would be rights violations."

### Definition of Rights 2:
>"a mutual contract between beings with moral agency."

### Definition of Moral Agency:
>"having awareness of who your actions impact AND retaining the capacity to choose not to do perform said action."

#### Questions
1. axe murderer scenario?
	1. answer: not immoral to withhold intervention.
2. retarded axe murderer scenario?
	1. answer: immoral NOT to intervene
3. retarded axe murderer attacking retarded person
	1. answer: immoral NOT to intervene

---

# Hashtag

#debate 
#debate_opponents 
#vegan 
