# Debate 1

## Proposition

~~>"genetically engineering humans is bad?"~~
~~>"improving humanity through genetic alteration is bad?"~~

## Analysis

1. Is this in principle? Meaning all else equal?
	1. For example, let's say that we have two possible worlds that are equal, but in one world a human is genetically altered with at least one benefit to them and/or the people around them. Which world is better?
		1. If genetic alternation world is better, you're not against it in principle.
		2. If the "wildtype" world is better, then you're just crazy.

Didn't disagree with my view.

---

# Hashtags

#debate 
#debate_opponents 
