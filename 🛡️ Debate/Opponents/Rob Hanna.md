# Debate 1

## Proposition
> "someone is a woman if they have a female reproductive system."

## Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>         |
|:-------------------------------------------:|:------------------------------------------------- |
|      <font color="CC6600">**W**</font>      | someone (x) is a woman                            |
|      <font color="CC6600">**R**</font>      | someone (x) has reproductive organs to give birth |
|      <font color="CC6600">**m**</font>      | Olivia Munn                                       |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> Someone is a woman if, and only if, someone has female reproductive organs to give birth.
<br />
<font color="CC6600">
<b>(∀x(Wx↔Rx))</b>
<br />
<b>P3)</b></font> Olivia Munn does not have female reproductive organs to give birth.
<br />
<font color="CC6600">
<b>(¬Rm)</b>
<br />
<b>C)</b></font> Therefore, Olivia Munn is not a woman.
<br />
<font color="CC6600">
<b>(∴¬Wm)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Wx~4Rx)),(~3Rm)|=(~3Wm))

---

# Hashtag

#debate 
#debate_opponents 
#transgender 