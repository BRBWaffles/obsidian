# Debate 1

## Proposition
> "There's no evidence that masking reduces the spread of COVID19"

## Arguments

### Gold Standard Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If RCTs are the FDA's gold standard for causal inference, then RCTs are required for a causal inference regarding mask effectiveness.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> RCTs are the FDA's gold standard for causal inference.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, RCTs are required for a causal inference regarding mask effectiveness.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

### New Interventions Tho

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                             |
|:-------------------------------------------:|:------------------------------------------------------------------------------------- |
|      <font color="CC6600">**N**</font>      | the medical intervention (x) is new                                                   |
|      <font color="CC6600">**K**</font>      | the level of harm produced by the medical intervention (x) is known                   |
|      <font color="CC6600">**R**</font>      | an RCT is generally required for a causal inference for that medical intervention (x) |
|      <font color="CC6600">**c**</font>      | Required masking to reduce the spread of COVID                                        |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the medical intervention is new and the level of harm produced by the medical intervention is not known, then an RCT is generally required for a causal inference for that medical intervention.
<br />
<font color="CC6600">
<b>(∀x(Nx∧¬Kx→Rx))</b>
<br />
<b>P2)</b></font> Required masking to reduce the spread of COVID is new.
<br />
<font color="CC6600">
<b>(Nc)</b>
<br />
<b>P3)</b></font> The level of harm produced by required masking to reduce the spread of COVID is not known.
<br />
<font color="CC6600">
<b>(¬Kc)</b>
<br />
<b>C)</b></font> Therefore, an RCT is generally required for causal inference for required masking to reduce the spread of COVID.
<br />
<font color="CC6600">
<b>(∴Rc)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Nx~1~3Kx~5Rx)),(Nc),(~3Kc)|=(Rc))

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 