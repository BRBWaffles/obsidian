# Receipts

**Jamey:**

> Dude, you must be severely autistic.
> 
> After looking through your list of dodgers, it seems to me that your debate invitations are based on what people have said in different contexts in the past. Correct me if I am wrong here, but I don't think you'd be willing to defend claims that you've made in the past (not listed on your page), would you? If you wouldn't, wouldn't it be fair to add yourself to that list of dodgers lol?
> 
> You only seem willing (correct me if I am wrong) to defend propositions that you have put out on your home page.
> 
> You should perhaps add a note on your page that says: ''I am willing to defend claims that I've made in the past not listed on my page as well''. 
> 
> Are you willing to defend claims that you've made in the past not listed on your page? If not, I don't see how you are not a dodger. 
> 
> Walk the walk yourself then?
> 
> Did you like my videos btw? Link: https://www.youtube.com/watch?v=gj_fqoOIJC8

**Nick:**

> You misunderstand the dodger list. It's a catalogue of past behaviours, not a call to these people to defend those particular claims. Of course if they make the same claims, I can appeal to the dodger list to dunk on them (which is always fun). But no, it's just a catalogue of shitty behaviour. Whether or not they changed their views is irrelevant to whether or not they previously dodged and displayed the shitty behaviour. I mean, think about it. If I dodge someone, and I change my view, does the proposition "Nick dodged someone" suddenly become false? No. Of course not.
> 
> Also, I dunno what the video is showing me. It's just a screencap of you scrolling with zero explanation. If you want to curate a list of things you think I got wrong, I'll happily review it. But I dunno wtf I'm supposed to gather from watching a video of you scrolling.

**Jamey:**

> OK, so it's a catalogue of past behaviours. So if you were challenged on some claim made by you in the past not on your proposition list and you weren't willing to defend that particular claim, would you then add yourself to the dodger-list?
> Also, others might not be able to spot what you're doing but to me your tactics are quite easy to spot. You seem to apply intentional bad faith interpretation when you interpret others. That is what it seems like to me. You touched on this a little bit during your conversation with Vegan Bazooka. Given what you advised him to do, you probably employ similar strategies yourself.
> For instance, here's a claim from Brian Sanders: ''The risk factor of cancer from meat is 0.18 % whereas the risk factor of cancer from smoking is 10-30 %''.
> Source: The Sapien Diet: Peak Human or Food Lies?
> Even though he phrased himself horribly here, it seems to me like he's referring to the strength of the association which is a key viewpoint to take into consideration in the process of inferring causality. The way you interpreted what he said seems very dishonest and calculated. His point seems to be that in one instance you have a strong association whereas in the other instance, you have a weak association. This parameter (the strength of the association) has the potential to affect how we view different associations in different scenarios when our goal is to infer causality with confidence.
> Also, there is nothing inherently dubious about comparing risk ratios from two different contexts. I have no clue who have taught you that.
> Your article was corrected shortly after my critique on reddit. The video illustrates the correction of that article. Anyone who reads our reddit convo + watches the video is gonna be able to connect the dots. What a coincidence that you briefly after my critique corrected the article.
> Also, the amount of bad faith interpretation you seem to apply in this article is quite hilarious: A Systematic Appraisal of Pro-meat Apologetics
> You take everything people say literally. You should steelman your opponents, not strawman them. Common sense always applies and if you refuse to apply common sense, well, then no wonder why people don't wanna entertain you.
> I relistened to your conversation with vegan bazooka. I am not sure what's up with you but just because you can repeat what Bart says, that doesn't really tell me very much. For instance, you said something along the lines of (paraphrased): ''I don't need this debate, I don't need you, so I am gonna be the one who sets the terms''.
> Then you claimed to have symmetry rofl. Perhaps from a literal viewpoint I could give you that but given the context of the situation and what went on prior to Bart said that, there is no symmetry there as far as I see it and the fact that you so arrogantly thought that you had left him in a pickle when he probably just decided to disengage because he got tired of you tells me that you're not quite connected to reality.
> Why would a person who has Bart's background have any issue providing you with a reason for why the situations are different? I mean, get real.
> You have to learn how to distinguish between someone getting fed up with you and someone not being able to do something.
> Your intonation gives you away during the conversation with the vegan bazooka too. It's very clear that you had yourself engaged in muting the mic of others. You sounded guilty as hell haha.
> There is a lot to criticize here but I have other things to do. I have no clue why you think so highly of yourself.

**Nick:**

> I'm not reading all that. Just give me a point form list of things you think I got wrong. Stop rambling and just be clear.

**Jamey:**

> Stop dodging ffs. You are now engaging in dodging yourself by avoiding my critique of your manners. It's hypocritical of you to call others dodgers while you yourself are engaging in the same behaviour. If you refuse to answer my question, then you're a dodger. In your conversation with Vegan Bazooka, did you pretty much give Bazooka advice about not applying good faith interpretation?

**Nick:**

> When it comes to normative stuff, there are only two types of critiques I care about; reductio ad absurdum or a contradiction. Either show me that my views have an entailment that I would find unpalatable, or show me my views entails a contradiction. Anything short of that is just virtue signaling to me, and I don't give a fuck about it if virtues aren't shared. If you have a critique of my "manners" that fall into either of those two categories, please, render it. If not, then I really don't give a fuck.
> 
> As for the critiques of my blog. Just lay out what you think I got wrong in a point-form list, and I'll review them. I will not, however, dig through paragraphs of rambling to extract a them. Be clear and concise if you want my engagement. Quote me, detail the error, explain how it's an error, and render the correction. Remember, you're the one emailing me and requesting my time and effort. I get to choose how I spend my time and effort. I'd be happy to engage on my terms, and all I'm asking for is a clear fucking list. Here, I'll even give you a template:
> 
> Error #1
> 
> Quote:
> Error:
> Explanation:
> Correction:
> 
> Use this template for all the things you think I got wrong. Then I'd be happy to review them. If you're not willing to deliver that, then fuck off, you're wasting my time.

**Jamey:**

> Cut the attitude. Meat-eaters could say the same thing as you though couldn't they? They could say: ''I don't give a fuck about it if virtues aren't shared'' whenever a vegan tries to hold them accountable for their behaviour. So this is all meat-eaters have to say to avoid engaging?
> I don't see how this response is any different than yours. I want you to respond to this but don't you worry, I will also choose a quote from your page.
> Here's one thing you said in the article below: ''The baseline prevalence of lung cancer is much smaller than the baseline prevalence of colorectal cancer, so comparing the two is dubious''.
> I need clarification. Is your position that comparing two different risk ratios from two different contexts is fundamentally flawed when the baseline prevalence differs?
> The Sapien Diet: Peak Human or Food Lies?
> You have nothing to complain about here. I am being super fair. Answer my questions.

**Nick:**

> I'm not reading your rambling, dude.

**Jamey:**

> Two questions (it's time to answer):
> 1) If a meat-eater said: ''I don't give a fuck about it if virtues aren't shared'' whenever a vegan tries to hold them accountable for their meat-eating behaviour. Would you consider this to be a sensible, justifiable and acceptable response from the meat-eater to let him not engage with the vegan's critique?
> 2) I need clarification. Is your position that comparing two different risk ratios from two different contexts is fundamentally flawed when the baseline prevalence differs?
> Are you gonna answer or keep dodging? Unbelievable.

**Nick:**

> Just give me the list. You said you had critiques, so render them. I don't know what this fucking rambling is all about.
> Here, we'll do it like this. Give me your list of critiques, and when we're finished addressing them, I'll answer whatever questions you want. But we're going to do this one thing at a time. First we'll deal with the critiques, then we'll deal with the questions. If your next reply is anything short of a list of critiques that conform to the formatting template I gave you, I'm blocking you and not giving any of this interaction a second thought.

**Jamey:**

> I think your demand is completely unreasonable for the following reasons:
> 1) Why should we ignore the chronological order of how things played out? I was the one who asked you certain questions first, well before you brought up your silly template. Can you give a good argument for why we should prioritize your desired order and not mine?
> 2) Asking me to fill out your template without letting me gain full clarity on your position tells me that you're scared and a dodger. Your demand prevents productive dialogue and leaves me unable to gain clarity on what your views are.
> I expect you to respond to both these points, if not, you're the one dodging and preventing a productive conversation from taking place.
> All of this is being recorded btw. You're the one losing credibility here by acting the way you do. Can you really afford to dodge?

**Nick:**

> We clarified your confusion with respect to the dodger list. The next thing was your video, which is you scrolling and highlighting things with zero explanation, and the title of the video is "The Nutrivore won't own up to his mistakes." This entire time I've been asking you to tell me what mistakes those are. I'm still trying to get clarity on wtf you were talking about in your first email. Then you started asking me a bunch of tangential questions. I am proceeding in chronological order, dipshit. lol
> 
> Also, if you have a critique, you shouldn't need to ask clarifying questions. If you need clarification, then you don't have a critique because you don't even understand the view. Fucking retard.

**Jamey:**

> Imagine telling someone that the dodger-list is a catalogue of past behaviours lol. I never had any doubts about the dodger-list reflecting things that were said in the past by people and I don't really believe that all of your extended debate invitations are not pending and that you wouldn't arrange something if people actually got back to you. Stop playing games.
> The correction is clearly visible in the video: The Nutrivore won't own up to his mistakes.
> As of the video. You changed the number 53 (risk ratio number) to a different number.
> Here's our reddit exchange too: Can anyone debunk the Nutrivore? : r/StopEatingSeedOils
> I decided to skip ahead to other topics and not focus on the video thing given your displayed insincerity in your first reply. Not taking into account that part and your first reply, the chronological order would be from my first reply. However, if you wanna go back to the video thing, we can do that.
> We had our last conversation exchange on reddit less than 30 days ago. I am quite surprised that you're struggling this bad with recalling things from the past. Are you OK? You also seem to be struggling with spotting a very visible correction in the video. Thirdly, I never said that I had a critique of the thing you said about comparing risk ratios being dubious. I asked for clarification to see if I had something to present you with. Exactly, I had no critique of that particular claim, which is why I asked for clarification. How can you not grasp this? So I guess you're the retard, correct?
> Given that you gave advice to Vegan Bazooka about employing bad faith tactics like interpreting people in a bad faith manner, I don't believe for a second that you misunderstood Brian's claim. Your bad faith behaviour is potentially supported by other things as well. Isn't it just time to admit that you're a dodging, bad faith interlocutor who has nothing going for him?
> I only see a dodging chicken who is chickening out. Now that we've established that a correction was made to the article (as shown in the video) and you made a clear mistake, we can move on. If you wanna keep denying that you corrected the article, then that is merely a reflection of the type of person you are - a dishonest person.
> All of this is being documented, you know that right? This is just gonna further reinforce people's notions about you being a bad faith clown.
> Address my 1st reply now. Go ahead.

**Nick:**

> Fuck sakes, your mother should have swallowed you.

---

#  Hashtags

#debate 
#debate_opponents 