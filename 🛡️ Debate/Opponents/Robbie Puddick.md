# Debate 1

## Proposition

>"saturated fat is not a major causal contributor to heart disease process"

## Argument

1. displacement effect (displacing heart healthy fats)
2. use butter to the degree that recipes call for it

## Analysis

1. carbs tho
	- resolved by Mensink 2016
2. low fat guidelines tho
	- irrelevant, randomized
3. data quality tho
	- LA Veterans was uber based

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery
#saturated_fat 
#LDL 
#cardiovascular_disease 