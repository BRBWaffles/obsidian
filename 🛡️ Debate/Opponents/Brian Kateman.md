## Debate 1

### Proposition:

>Veganism is impossible

1. What is meant by "veganism" here?
2. On what modality is veganism impossible? 
	1. sounds like a deontic modality
	2. If veganism is deontologically impossible, it must be in virtue of some other impossibility that constrains action.
	3. On what modality is it impossible to casually stroll without causing animal suffering?
3. Why is it assumed that veganism is a deontological thesis?
	1. If it's not assumed that veganism is a deontological thesis, then what's the argument that the action of walking is less compatible with vegan principles than not walking?

### Clarified Proposition:

>Veganism is unlikely

1. What does it mean for veganism to be unlikely?

#### Brian's Argument For Impossible Veganism

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                        |
|:-------------------------------------------:|:------------------------------------------------------------------------------------------------ |
|      <font color="CC6600">**V**</font>      | it is possible for (x) one to be vegan                                                           |
|      <font color="CC6600">**S**</font>      | it is possible for (x) one's (y) actions to cause animal suffering absence of survival necessity | 
|      <font color="CC6600">**h**</font>      | human                                                                                            |
|      <font color="CC6600">**e**</font>      | existence                                                                                        |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> It's possible for one to be vegan if and only if it's possible for one's actions to not cause animal suffering in the absence of survival necessity.
<br />
<font color="CC6600">
<b>(∀x∀y(◇Vx↔◇¬Sxy))</b>
<br />
<b>P2)</b></font> Human existence necessitates animal suffering in the absence of survival necessity.
<br />
<font color="CC6600">
<b>(▢She)</b>
<br />
<b>C)</b></font> Therefore, it is impossible for humans to be vegan.
<br />
<font color="CC6600">
<b>(∴¬◇Vh)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(~9Vx~4~9~3Sxy)),(~8She)|=(~3~9Vh))

---

## Debate 2

### Definition

>Reducetarianism is the practice of eating less meat - red meat, poultry, and seafood - as well as less dairy and fewer eggs, regardless of the degree or motivation.

### My Proposition

>Reducetarianism is either subsumed by my definition OR leads to absurdity OR leads to a contradiction.

## Receipts:

![[📂 Media/Images/Pasted image 20230411160120.png]]
![[📂 Media/Images/Pasted image 20230411160134.png]]
![[📂 Media/Images/Pasted image 20230411160144.png]]
![[📂 Media/Images/Pasted image 20230411160152.png]]

---

## Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
#vegan 
#cropdeaths 