# Debate 1

## Proposition
>It’s way more likely that seed oils are harmful to humans as opposed to the opposite.

## Analysis

1. "likely"
	1. statistical term or a priori?
		1. if statistical, what's the data?
		2. if a priori, what's the argument?
			1. they're novel foods, only consumed for the last 200 years?
			2. refining process, rather than it being a novel food
				1. novel food tho
					1. arg: adaptation tho
						1. weston a price tho
						2. heavily flaws studies tho
							1. ffqs tho
				2. ~~refining food tho~~ adding compounds that strays too far from the natural **(conceded!)**
					1. scenario: choline from eggs vs oil from soybeans in a blueberry smoothie
						1. symmetry breaker: refining process specific to seed oils
							1. needs to have seed oil refining process (all other processes presumed cool)
								1. what about the refining?
									1. neurotoxins (hexane?)
										1. fibres in nose tho
									2. 
					2. ~~what is meant by refining?~~
						1. ~~adding neurotoxins?~~
				3. monocropped agriculture tho
					1. sprayed toxic chemicals (pesticides, etc)
						1. doesn't know if there's evidence
							1. appeals to anecdotes again
								1. signed off on the shit and piss and cum stew, lol
						2. how do we know the compounds 
						3. GMO allows higher doses
					2. fewer nutrients, for "good" (means essential and non-essential) nutrients **(conceded!)**
						1. multivitamins based then?
						2. apples shit?
					3. 
				4. anecdote tho
					1. anecdote > data if n=1
						1. signed off on the shit and piss and cum stew, lol
2. "harmful"
	1. with respect to what?
		1. 

---

# Debate 2

## Proposition
>"it would be more vegan to abstain from monocropped agriculture" **CONCEDED!**

## Analysis

1. "more vegan"
	1. more compatible with the VS definition

## Questions

**Cruelty** (signed off)
1. rewilding vs kidnapping and abandoning babies/kitten?
	1. rewilding = not cruel
	2. kidnapping = cruel
		1. symmetry breaker 1: rewilding better for the environment
			1. fewer toxins
			2. produces more oxygen
				1. didn't sign off/contradiction
		2. symmetry breaker 2: rewilding returning the space to its natural state
			1. didn't sign off/contradiction

**Exploitation**
1. signed off

---

# Hashtags

#debate 


#debate 
#debate_opponents 