# Debate 5

## Proposition

>it is permissible to expose Eric's views by surreptitiously recording private conversations wherein he attempts to justify animal agriculture.

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                                                                   |
|:-------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">**H**</font>      | one (x) engages in apologia for a holocaust of the innocent (y)                                                                                             |
|      <font color="CC6600">**S**</font>      | it is permissible to expose one's (x) grotesque views by surreptitiously recording private conversations wherein they attempt to justify said holocaust (y) | 
|      <font color="CC6600">**e**</font>      | Eric                                                                                                                                                        |
|      <font color="CC6600">**a**</font>      | animal agriculture                                                                                                                                          |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If one engages in apologia for a holocaust of the innocent, then it is permissible to expose one's grotesque views by surreptitiously recording private conversations wherein they attempt to justify said holocaust.
<br />
<font color="CC6600">
<b>(∀x∀y(Hxy→Sxy))</b>
<br />
<b>P2)</b></font> Eric engages in apologia for animal agriculture.
<br />
<font color="CC6600">
<b>(Hea)</b>
<br />
<b>C)</b></font> Therefore, it is permissible to expose Eric's views by surreptitiously recording private conversations wherein he attempts to justify animal agriculture.
<br />
<font color="CC6600">
<b>(∴Sea)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Hxy~5Sxy)),(Hea)|=(Sea))

---

# Debate 4?

## Notes

Subject has cucked.
![[📂 Media/Images/Pasted image 20230312154245.png]]

---

# Debate 3

## Eric's Definitions

**Sapience:**
""

**Right:**
"an entitlement [to a member of a sapient species] that would be wrong to deny." 

**Moral obligation:**
"a responsibility to protect rights [of sapient species]."

**Consideration:**
""

**Moral Consideration:**
""

## Consent
![[📂 Media/Images/Pasted image 20221010212223.png]]

---

# Debate 2

## Eric's Prop With Common Definitions

>A diet of ruminant meat, poultry, fish, eggs, dairy, olives, avocado, lettuces, skinless and seedless cucumbers, and various squashes is higher in rank, status, or quality in an essential or natural way to a diet that focuses on plants, including vegetables, fruits, whole grains, legumes, seeds, and nuts and limits or avoids animal products.

What does it mean for one diet to simply have a higher risk, status, or quality, compared to another? With respect to what standard?

---

# Debate 1

## Eric's Justifications for Eating Meat

1) we're all OK with eating animals
2) we have dominion over animals
3) we take care of animals before slaughter
4) animals are not homo sapiens
5) animals were not born of a human
6) animals do not have human cognition
7) animals are ancestral food
8) animals are perfect food

## Eric's Revised Justifications

"Animals are OK to kill and eat because..."

1) "All things will die and become food for other living things."
	- Empirical claim. Requires proof.
	- Cremation and body donation to medical research both disprove this. 
	- Why couldn't this same argument be used to justify killing and eating humans?
2) "Every species has a specific diet."
	- Definition of "specific diet" required.
	- Empirical claim. Requires proof.
	- Humans are a part of many animals' [natural diet](https://en.wikipedia.org/wiki/Man-eater).
3) "Meat is the perfect food for humans."
	- Definition of "perfect" required.
	- Empirical claim. Requires evidence.
	- Depending on the definition provided, humans may be the perfect food for some organisms. So would it not be justified to feed humans to these organisms?
4) "I would feel privileged to know my body will become future food when I die."
	- Good for you? Hypothetically, if a race of organisms felt privileged to know their bodies would become future food when they die, would that make it OK for them to farm, kill, and eat you?
	- If you take point one to be true, why would you phrase this point hypothetically?
5) "Animals living on regenerative AG farms live a privileged life."
	- There are humans who live privileged lives. Is it OK to farm, kill, and eat humans?

---

# Arguments

### Privilege to Be Eaten Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If organism X is the perfect food for organism Y, then it would be a privilege for organism X to be eaten by organism Y.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Organism X is the perfect food for organism Y.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, it would be a privilege for organism X to be eaten by organism Y.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

#### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                |
|:-------------------------------------------:|:------------------------------------------------------------------------ |
|      <font color="CC6600">**P**</font>      | organism X (x) is the perfect food for organism Y (y)                    |
|      <font color="CC6600">**Q**</font>      | it would be a privilege for organism X (x) to be eaten by organism Y (y) |
|      <font color="CC6600">**h**</font>      | humans                                                                   |
|      <font color="CC6600">**l**</font>      | lions                                                                    |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font>  If organism X is the perfect food for organism Y, then it would be a privilege for organism X to be eaten by organism Y.
<br />
<font color="CC6600">
<b>(∀x∀y(Pxy→Qxy))</b>
<br />
<b>P2)</b></font> Humans are the perfect food for lions.
<br />
<font color="CC6600">
<b>(Phl)</b>
<br />
<b>C)</b></font> Therefore, it would be a privilege for humans to be eaten by lions..
<br />
<font color="CC6600">
<b>(∴Qhl)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Pxy~5Qxy)),(Phl)|=(Qhl))

---

### Lifestyle Modification Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the intervention is health promoting, then the intervention is a lifestyle modification intervention.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> The intervention is not a lifestyle modification intervention.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, the intervention is not health promoting.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

#### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                     |
|:-------------------------------------------:|:------------------------------------------------------------- |
|      <font color="CC6600">****</font>       | the intervention (x) is health promoting                      |
|      <font color="CC6600">****</font>       | the intervention (x) is a lifestyle modification intervention |
|      <font color="CC6600">****</font>       | TPN                                                           |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the intervention is health promoting, then the intervention is a lifestyle modification intervention.
<br />
<font color="CC6600">
<b>(∀x(Px→Qx))</b>
<br />
<b>P2)</b></font> TPN is not a lifestyle modification intervention.
<br />
<font color="CC6600">
<b>(¬Qt)</b>
<br />
<b>C)</b></font> Therefore, TPN is not health promoting.
<br />
<font color="CC6600">
<b>(∴¬Pt)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Px~5Qx)),(~3Qt)|=(~3Pt))

---

### Expensive Tissue Hypothesis Tho

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                             |
|:-------------------------------------------:|:--------------------------------------------------------------------- |
|      <font color="CC6600">**E**</font>      | the Expensive Tissue Hypothesis is true                               |
|      <font color="CC6600">**H**</font>      | food (x) provides more bioavailable energy than food (y)              |
|      <font color="CC6600">**M**</font>      | then food (x) is preferable to food (y) with respect to mental health |
|      <font color="CC6600">**m**</font>      | meat                                                                  |
|      <font color="CC6600">**g**</font>      | grains                                                                |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the Expensive Tissue Hypothesis is true and X food provides more bioavailable energy than Y food, then X food is preferable to Y food with respect to mental health.
<br />
<font color="CC6600">
<b>(∀x∀y(E∧Hxy→Mxy))</b>
<br />
<b>P2)</b></font> The Expensive Tissue Hypothesis is true.
<br />
<font color="CC6600">
<b>(E)</b>
<br />
<b>P3)</b></font> Meat provides more bioavailable energy than grains.
<br />
<font color="CC6600">
<b>(Hmg)</b>
<br />
<b>C)</b></font> Therefore, meat is preferable to grains with respect to mental health.
<br />
<font color="CC6600">
<b>(∴Mmg)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(E~1Hxy~5Mxy)),(E),(Hmg)|=(Mmg))

#### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                             |
|:-------------------------------------------:|:--------------------------------------------------------------------- |
|      <font color="CC6600">**E**</font>      | the Expensive Tissue Hypothesis is true                               |
|      <font color="CC6600">**H**</font>      | food (x) provides more bioavailable energy than food (y)              |
|      <font color="CC6600">**M**</font>      | then food (x) is preferable to food (y) with respect to mental health |
|      <font color="CC6600">**s**</font>      | seed oils                                                             |
|      <font color="CC6600">**m**</font>      | meat                                                                  |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If the Expensive Tissue Hypothesis is true and X food provides more bioavailable energy than Y food, then X food is preferable to Y food with respect to mental health.
<br />
<font color="CC6600">
<b>(∀x∀y(E∧Hxy→Mxy))</b>
<br />
<b>P2)</b></font> The Expensive Tissue Hypothesis is true.
<br />
<font color="CC6600">
<b>(E)</b>
<br />
<b>P3)</b></font> Seed oils provides more bioavailable energy than meat.
<br />
<font color="CC6600">
<b>(Hsm)</b>
<br />
<b>C)</b></font> Therefore, seed oils are preferable to meat with respect to mental health.
<br />
<font color="CC6600">
<b>(∴Msm)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(E~1Hxy~5Mxy)),(E),(Hsm)|=(Msm))

---

### Fostered Evolution Tho

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If dietary pattern (x) fostered the evolution of homo sapiens, then dietary pattern (x) is intrinsically superior to dietary pattern (y) that did not foster the evolution of homo sapiens.
<br />
<font color="CC6600">
<b>(∀x∀y(Dx→Sxy))</b>
<br />
<br />
</font>
</div>

Couldn't finish the syllogism because it was question begging. Eric clarified that "fostered the evolution of homo sapiens" and " intrinsically superior" were the same thing.

---

### Sapience Tho

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**R**</font>      | A species has rights                      |
|      <font color="CC6600">**S**</font>      | a species is sapient                      |
|      <font color="CC6600">**h**</font>      | homo sapiens                              |
|      <font color="CC6600">**b**</font>      | bos taurus                                |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> A species has rights if, and only if, a species is sapient.
<br />
<font color="CC6600">
<b>(∀x(Rx↔Sx))</b>
<br />
<b>P2)</b></font> Homo sapiens sapiens are sapient.
<br />
<font color="CC6600">
<b>(Sh)</b>
<br />
<b>P3)</b></font> Bos taurus are not sapient.
<br />
<font color="CC6600">
<b>(¬Sb)</b>
<br />
<b>C)</b></font> Therefore, Homo sapiens sapiens have rights and Bos taurus do not have rights.
<br />
<font color="CC6600">
<b>(∴Rh∧¬Rb)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Rx~4Sx)),(Sh),(~3Sb)|=(Rh~1~3Rb))

#### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:-------------------------------------------:|:----------------------------------------- |
|      <font color="CC6600">**R**</font>      | A species has rights                      |
|      <font color="CC6600">**S**</font>      | a species is sapient                      |
|      <font color="CC6600">**m**</font>      | severely mentally handicapped people               | 

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> A species has rights if, and only if, a species is sapient.
<br />
<font color="CC6600">
<b>(∀x(Rx↔Sx))</b>
<br />
<b>P2)</b></font> Severely mentally handicapped people are sapient.
<br />
<font color="CC6600">
<b>(¬Sm)</b>
<br />
<b>C)</b></font> Therefore, Severely mentally handicapped people do not have rights.
<br />
<font color="CC6600">
<b>(∴¬Rm)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(Rx~4Sx)),(~3Sm)|=(~3Rm))

**Reference:**
![[📂 Media/Images/Pasted image 20221005212839.jpg]]

---

### Vegans Are Sophists Tho

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                     |
|:-------------------------------------------:|:------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">****</font>       | one (x) debates a moral topic (y)                                                                             |
|      <font color="CC6600">****</font>       | one (x) does not engage in realizing their position on the moral topic (y) to the greatest degree practicable |
|      <font color="CC6600">****</font>       | one (x) is committed to sophistry                                                                             |
|      <font color="CC6600">****</font>       | vegans                                                                                                        |
|      <font color="CC6600">****</font>       | animal agriculture                                                                                            |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If one debates a moral topic and one does not engage in realizing their position on the moral topic to the greatest degree practicable, then one is committed to sophistry.
<br />
<font color="CC6600">
<b>(∀x∀y(Dxy∧¬Rxy→Sx))</b>
<br />
<b>P2)</b></font> Vegans debate the ethics of animal agriculture.
<br />
<font color="CC6600">
<b>(Dva)</b>
<br />
<b>P3)</b></font> Vegans does not engage in realizing their position on the ethics of animal agriculture to the greatest degree practicable.
<br />
<font color="CC6600">
<b>(¬Rva)</b>
<br />
<b>C)</b></font> Therefore, Vegans are committed to sophistry.
<br />
<font color="CC6600">
<b>(∴Sv)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Dxy~1~3Rxy~5Sx)),(Dva),(~3Rva)|=(Sv))

#### Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>                                                                     |
|:-------------------------------------------:|:------------------------------------------------------------------------------------------------------------- |
|      <font color="CC6600">****</font>       | one (x) debates a moral topic (y)                                                                             |
|      <font color="CC6600">****</font>       | one (x) does not engage in realizing their position on the moral topic (y) to the greatest degree practicable |
|      <font color="CC6600">****</font>       | one (x) is committed to sophistry                                                                             |
|      <font color="CC6600">****</font>       | Eric                                                                                                        |
|      <font color="CC6600">****</font>       | regenerative agriculture                                                                                            |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If one debates a moral topic and one does not engage in realizing their position on the moral topic to the greatest degree practicable, then one is committed to sophistry.
<br />
<font color="CC6600">
<b>(∀x∀y(Dxy∧¬Rxy→Sx))</b>
<br />
<b>P2)</b></font> Eric debates the ethics of regenerative agriculture .
<br />
<font color="CC6600">
<b>(Der)</b>
<br />
<b>P3)</b></font> Eric does not engage in realizing his position on the ethics of regenerative agriculture to the greatest degree practicable.
<br />
<font color="CC6600">
<b>(¬Rer)</b>
<br />
<b>C)</b></font> Therefore, Eric is committed to sophistry.
<br />
<font color="CC6600">
<b>(∴Se)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x~6y(Dxy~1~3Rxy~5Sx)),(Der),(~3Rer)|=(Se))

---

### Dog Molestation Reductio

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font>              |
|:-------------------------------------------:|:------------------------------------------------------ |
|      <font color="CC6600">**M**</font>      | the animal (x) can understand freedom, life, and death |
|      <font color="CC6600">**F**</font>      | it is OK to sexually molest animal (x)                 |
|      <font color="CC6600">**g**</font>      | dogs                                                   |
|      <font color="CC6600">**o**</font>      | cows                                                   |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> It is not OK to sexually molest an animal if and only if the animal understands freedom, life, and death.
<br />
<font color="CC6600">
<b>(∀x(¬Mx↔Fx))</b>
<br />
<b>P2)</b></font> Dogs do not understand freedom, life, and death.
<br />
<font color="CC6600">
<b>(¬Fg)</b>
<br />
<b>P3)</b></font> Cows do not understand freedom, life, and death.
<br />
<font color="CC6600">
<b>(¬Fo)</b>
<br />
<b>P4)</b></font> If it is OK to sexually molest cows, then it is OK to sexually molest dogs.
<br />
<font color="CC6600">
<b>(Mo→Mg)</b>
<br />
<b>C)</b></font> Therefore, it is OK to sexually molest dogs.
<br />
<font color="CC6600">
<b>(∴Mg)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(~6x(~3Mx~4Fx)),(~3Fg),(~3Fo),(Mo~5Mg)|=(Mg))

---

## Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery 
#vegan 
#agriculture 
#animal_agriculture 