# Debate 1

## Debate Proposition:

> "Vegans wish to abolish and criminalize animal agriculture, however, the crop agriculture relied upon by vegans is more unethical than the animal agriculture relied upon by non-vegans."

>"There is dissonance between vegan values and the knowledge vegans possess?

1. Is the claim that vegans are hypocrites?
- Refuses to answer the question.
2. What is meant by unethical?
- 
3. What is the argument that crop ag is less ethical than non-crop ag?
- 

## Reference:
https://onlinelibrary.wiley.com/doi/full/10.1111/ecog.05126

---

# Debate 2

## Proposition
>Nick is in favour of intentionally disrupting ecosystems.

## Analysis
1. **Intentionally disrupt** just means I'm in favour of non-violent displacement of wild animals.
	1. Don't disagree with the proposition.

---

# Hashtags

#debate 
#debate_opponents 
#vegan 
#cropdeaths
#low_carb_talking_points 
#clowns 
#clownery