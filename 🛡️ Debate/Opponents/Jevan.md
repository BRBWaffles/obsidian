# Debate 2

## Proposition
> "cows can't live if humans don't eat them."

## Analysis
1. on what modality?
	1. what's the contradiction?
	2. can you infer to that contradiction?
2. if not a modal claim, what is even being said?

---

# Debate 1

## Proposition
> "animal agriculture is morally superior because future cows want to live."

## Argument

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If future cows will want to live, then future cows will be bred.
<br />
<font color="CC6600">
<b>(P→Q)</b>
<br />
<b>P2)</b></font> Future cows will want to live.
<br />
<font color="CC6600">
<b>(P)</b>
<br />
<b>C)</b></font> Therefore, future cows will be bred.
<br />
<font color="CC6600">
<b>(∴Q)</b>
<br />
<br />
</font>
</div>

[Proof Tree](https://www.umsu.de/trees/#(P~5Q),(P)|=(Q))

## Analysis

1. What the fuck does it mean for a non-existent being to want something?

---

# Hashtags

#debate 
#debate_opponents 
#clowns 
#clownery
#vegan 
#agriculture 
#animal_agriculture 