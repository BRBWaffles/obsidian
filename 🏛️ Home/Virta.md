# Points:

- All points must be taken in the context that the sample population was self-selected and paying.
- They counted improvements sustained by metformin toward their reversal statistic of 32.5%.
- Complete remission without drugs was only 6.7% among completers, and 4.7% ITT (Table 1).
- 45% of the participants started GLP1 agonists and another 10% increased their dosage (Fig. 3A).
- Hba1c essentially returned to baseline despite subjects maintaining 50% of weight loss.
	- Likely due to not reinstating medications.
	- Difficult to make inferences about insulin sensitivity either way.
- LDL was changed by 4.6 mg/dL, which is about half as much as is expected from going from the lowest to highest cholesterol intake.
	- Not sure why we care about HDL, TG, or ApoA-1.