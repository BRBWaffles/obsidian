**Interventionalist model:** A causes B if, and only if, manipulating A changes B.
**Counterfactual model:** A causes B if, and only if, had A not occurred, B would not have occurred.
**Probabilistic model:** A causes B if, and only if, A makes B more likely to obtain.

---

# Hashtags

#causal_inference 
#philosophy 