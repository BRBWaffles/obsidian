
1. **Meal Scheduling**
	- There are deeply embedded cultural norms about when people should eat.
	- It may be considered bizarre when people deviate from the traditional 3-meal structure.
	- These norms may also be reinforced institutionally by work, school, etc.

Pubs to check:
https://pmc.ncbi.nlm.nih.gov/articles/PMC11430606/
https://academic.oup.com/nutritionreviews/article-abstract/81/1/75/6623541
https://academic.oup.com/nutritionreviews/article-abstract/82/10/1309/7379733
https://www.tandfonline.com/doi/full/10.1080/07420528.2023.2180385

1. **Meal Completion**
	- There may be generational trauma related to food scarcity that still persists despite food abundance.
	- Norms that made sense in the context of food scarcity could have a detrimental impact in the context of food abundance.
	- There may be a general cultural ethos that discourages food waste, and encourages consumption beyond one's needs in order to avoid it.
	- The phrase "finish your plate" is still taught to children.
2. **Ceremonial Eating**
	- Celebratory eating is central to nearly every holiday or holiday season.
	- People may feel out of place for not partaking.
	- Refusing traditional foods may be viewed as an affront to cultural identity, and could lead to various sorts of cultural friction.
	- Even non-traditional holidays often incorporate mandatory food elements.
3. **Social Pressure**
	- Even outside the context of celebrations or ceremony, food is frequently central to many mundane social events (such as dates or outings).
	- Refusing to partake can result in awkwardness that many may feel more comfortable avoiding, resulting in consumption beyond one's needs.
	- People may also feel pressured to match other people's eating pace and quantity, and may feel out of place if they "eat like a bird".
4. **Portion Size**
	- Meals may seem less worthwhile if they do not have a high volume and/or calorie to price and/or effort ratio.
	- Paying the same amount of money for a smaller meal may seem less valuable or economically desirable, and people may have a desire to "get their money's worth".
	- People might feel more compelled to eat more than they otherwise would when participating in more open-ended, buffet-style dining, in order to justify the cost.
5. **Hedonic Eating**
	- Food may be viewed as a form of entertainment or reward, with "treating" oneself with "guilty pleasures" often forming the justification for indulging.
	- Exciting the palate may be considered one of the primary reasons for eating.
	- Nearly all food advertising emphasizes the pleasure of eating certain foods, which may be an indication of consumer motivations when buying these foods.
	- Food can be highly rewarding, and some proportion of over-consumption could be the results of self-medication of mental illness, such as depression and/or anxiety.