# Appeal to Authority

When one appeals to authority, it simply means that one affirms that a proposition is true in virtue of it being uttered by an authority. This fallacy typically pervasive within any domain wherein there are experts who publicly profess their opinions.

>**Example:** "The carnivore diet is healthy because Paul Saladino concluded this after years of researching diet!"

---

# Hashtags

#fallacies 
#appeal_to_authority
#hyperblog