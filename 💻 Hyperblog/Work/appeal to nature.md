# Appeal to Nature

An appeal to nature is characterized by the affirmation that something is good, preferable, or desirable, merely because it is natural. This fallacy is common in the domain of human health, such as when health product advertisers claim that their product is beneficial because it either contains more natural ingredients or fewer artificial ingredients.

>**Example:** "Red meat is clearly healthy for humans if we evolved consuming it!"

---

# Hashtags

#fallacies 
#appeal_to_nature
#hyperblog