Lipoproteins are complexes of lipids and proteins that transport fats through the bloodstream. They are crucial for the absorption, transport, and metabolism of lipids such as [[cholesterol]] and triglycerides.

Lipoproteins are classified based on their density: [[high-density lipoproteins]] (HDL) are known, somewhat imprecisely, as "good cholesterol" because they transport cholesterol away from the arteries to the liver for excretion, while [[low-density lipoproteins]] (LDL) are known as, also imprecisely, "bad cholesterol" because they can deposit cholesterol in artery walls, leading to atherosclerosis.

---

# Hashtags

#lipidology
#lipoproteins 
#low-density_lipoproteins 
#very_low-density_lipoproteins 
#high-density_lipoproteins
#cholesterol 
#apob 
#apoa
#dietary_fat 
#hyperblog