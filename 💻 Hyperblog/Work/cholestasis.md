# Cholestasis

Cholestasis is a pathological condition characterized by the impairment or cessation of bile flow. In this disorder, bile, which is normally produced by liver cells and transported through bile ducts to the gallbladder and small intestine, becomes blocked or slowed.

**Key features of cholestasis:**

1. **Accumulation of bile components:** Substances normally excreted in bile (such as bilirubin, bile acids, and cholesterol) build up in the liver and bloodstream.
2. **Jaundice:** The hallmark sign, resulting from elevated bilirubin levels in the blood, causing yellowing of the skin and eyes.
3. **Pruritus:** Severe itching, often one of the most distressing symptoms, caused by the accumulation of bile acids in the skin.
4. **Impaired fat absorption:** Due to reduced bile flow into the intestine, leading to malabsorption of fat-soluble vitamins (A, D, E, K).
5. **Steatorrhea:** Fatty stools due to impaired fat digestion and absorption.
6. **Potential liver damage:** Prolonged cholestasis can lead to inflammation, fibrosis, and eventually cirrhosis of the liver.

Cholestasis can occur within the liver (intrahepatic) or in the bile ducts outside the liver (extrahepatic). It can be acute or chronic, and its severity can range from mild to life-threatening, depending on the underlying cause and duration.

---

# Hashtags

#liver_function
#cholestasis
#gallstones
#gallbladder
#hyperblog