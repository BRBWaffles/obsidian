# Claim

>Seed oils are unhealthy because linoleic acid causes degeneration of the retinal macula.

## Rebuttal

This claim is merely [[mechanistic speculation]]. The notion that seed oils increase the risk of [[age-related macular degeneration]] (AMD) is essentially a [[red herring]], because it has very little empirical support, with much of the human outcome data showing incredibly heterogeneous results. Across prospective cohort studies, [[linoleic acid]] (LA) associates with a non-significant decrease in the risk of AMD [(1)](https://pubmed.ncbi.nlm.nih.gov/21402976/)[(2)](https://pubmed.ncbi.nlm.nih.gov/7786215/)[(3)](https://pubmed.ncbi.nlm.nih.gov/32181798/)[(4)](https://pubmed.ncbi.nlm.nih.gov/19433719/)[(5)](https://pubmed.ncbi.nlm.nih.gov/19433717/)[(6)](https://pubmed.ncbi.nlm.nih.gov/16832021/)[(7)](https://pubmed.ncbi.nlm.nih.gov/11157315/).

![[Pasted image 20240814192514.png]]

Among the studies with the longest follow-up time, largest cohort size, best adjustment models, and the widest exposure contrasts, the results tended to be null. For example, Chong et al. (2009) adjusted for [[ lutein]], [[zeaxanthin]], and sources of [[omega-3]], which are inversely associated with AMD. Their results were non-significant for every exposure.

The strongest study of all was Christen et al. (2011). Their analysis included three different adjustment models that help us better ascertain the relationship between AMD and seed oils. For example, their analysis showed that LA was associated with AMD only before adjustment for AMD risk factors, and that the association was likely a function of insufficient omega-3.

>_“Women in the highest tertile of LA intake, relative to the lowest, had an age- and treatment-adjusted RR of 1.41 (95% CI, 1.03-1.94; P for trend=.03). However, the RR was attenuated and no longer significant after additional adjustment for AMD risk factors and other fats. The ratio of -6 to -3 fatty acids was directly associated with the risk of AMD, and the association was strengthened when the denominator term for-3 fatty acids included only DHA and EPA (Table 2).”_

Essentially, seed oils may sometimes be acting as a correlate for low quality dietary patterns, such as the [[standard american diet]], but the seed oils themselves do not see, to independently increase the risk of AMD. Seed oils do tend to associate with dietary patterns that lack characteristics that tend to associate with a decreased risk. For example, diets that are high in seed oils tend to be low in [[carotenoids]] and omega-3, for example. After adjusting for those [[confounders]], the association vanishes.

There is also [[mendelian randomization]] (MR) data from Wang et al. (2021) investigating the relationship between genetically elevated LA biomarkers and AMD [(8)](https://pubmed.ncbi.nlm.nih.gov/33982092/). In this multinational MR study of 8631 participants they applied three different statistical tests across two subgroups investigating different combinations of gene variants. In the aggregate, genetically increased plasma LA was consistently associated with a reduced risk of AMD, regardless of the test or gene variant subgrouping. Interestingly enough, tissue AA was actually positively associated with AMD, like we saw earlier with the previously mentioned MR research.

---

## References 

1. https://pubmed.ncbi.nlm.nih.gov/21402976/
2. https://pubmed.ncbi.nlm.nih.gov/7786215/
3. https://pubmed.ncbi.nlm.nih.gov/32181798/
4. https://pubmed.ncbi.nlm.nih.gov/19433719/
5. https://pubmed.ncbi.nlm.nih.gov/19433717/
6. https://pubmed.ncbi.nlm.nih.gov/16832021/
7. https://pubmed.ncbi.nlm.nih.gov/11157315/
8. https://pubmed.ncbi.nlm.nih.gov/33982092/

---

# Hashtags

#ophthalmology
#seed_oils  
#macular_degeneration 
#mechanistic_speculation 
#fallacies  
#hyperblog