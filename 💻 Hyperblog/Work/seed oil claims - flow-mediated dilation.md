# Claim

>Seed oils are unhealthy because they decrease flow-mediated dilation.

## Rebuttal

This claim is merely [[mechanistic speculation]]. Even if this is true, studies show that these are postprandial changes in [[flow-mediated dilation]] (FMD), and there are no studies indicating that postprandial changes in FMD actually increase the risk of any particular disease or condition. Additionally, despite postprandial decreases in FMD, the opposite appears to hold true long term [(1)](https://pubmed.ncbi.nlm.nih.gov/26016869/). It appears as though [[linoleic acid]] consumption associates with favourable increases in FMD overall.

---

## References 

1. https://pubmed.ncbi.nlm.nih.gov/26016869/

---

# Hashtags

#cardiology 
#claims 
#endothelial_function
#flow-mediated_dilation
#mechanistic_speculation 
#seed_oils  
#hyperblog