# Low-Density Lipoproteins

Low-density lipoprotein (LDL) are types of [[lipoproteins]] that plays a crucial role in cholesterol transport within the human body. Often, and somewhat imprecisely, referred to as "bad cholesterol", LDL is a complex of lipids and proteins that carries [[cholesterol]] from the liver to various tissues throughout the body.

Structurally, LDL particles consist of:

- A core of cholesterol esters and triglycerides.
- An outer layer of phospholipids and unesterified cholesterol.
- A single apolipoprotein B-100 molecule.

LDL particles are formed in the bloodstream as very-low-density lipoproteins (VLDL) lose triglycerides through the action of lipoprotein lipase. As this process continues, VLDL particles become increasingly dense, eventually forming LDL.

However, elevated levels of LDL in the bloodstream are associated with an increased risk of cardiovascular disease, particularly [[atherosclerosis]]. This is because excess LDL can penetrate the arterial wall and become [[oxidized low-density lipoproteins]], triggering [[inflammation]] that leads to plaque formation.

---

# Hashtags

#lipids 
#cholesterol 
#low-density_lipoproteins 
#oxidized_low-density_lipoproteins 
#dietary_fat 
#apob 
#cardiology 
#heart_disease 
#heart_attack 
#hyperblog