# Phytosterols

Phytosterols are plant-derived compounds structurally similar to [[cholesterol]]. They are found in fruits, vegetables, nuts, seeds, and plant oils. Phytosterols help lower blood levels of cholesterol contained in [[lipoproteins]] by competing with cholesterol for absorption in the digestive system, thus reducing overall blood cholesterol levels. Regular intake of phytosterols is associated with a decreased risk of [[cardiovascular disease]] but perhaps an increased risk of [[cholestasis]].

---

# Hashtags

#cholestasis 
#gallbladder 
#gallstones 
#lipidology 
#lipids 
#liver_function 
#hyperblog
