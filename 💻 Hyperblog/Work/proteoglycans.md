
# Proteoglycans

Proteoglycans are complex macromolecules that play crucial roles in various tissues, including the subendothelial space of arteries.

General structure of proteoglycans:

1. Core protein.
2. One or more glycosaminoglycan (GAG) chains covalently attached.
3. GAGs are long, unbranched polysaccharides with repeating disaccharide units.

Proteoglycans in the subendothelial space of arteries:

1. Main types:
    - Versican (large aggregating proteoglycan).
    - Biglycan and decorin (small leucine-rich proteoglycans).
    - Perlecan (basement membrane proteoglycan).
2. Functions:
    - Structural support and tissue organization.
    - Regulation of water content and tissue hydration.
    - Interaction with lipoproteins, particularly [[low-density lipoproteins]].
    - Modulation of cell adhesion and migration.
    - Regulation of growth factor activity.
3. Role in atherosclerosis:
    - Retention of [[apolipoprotein B-containing lipoproteins]].
    - Contribution to the formation of [[atherosclerotic plaques]].
    - Modulation of inflammatory responses.
4. Interactions:
    - Bind to other extracellular matrix components (e.g., collagen, elastin).
    - Interact with cell surface receptors.
    - Sequester growth factors and cytokines.
5. Regulation:
    - Synthesis and degradation influenced by mechanical forces, growth factors, and inflammatory mediators.

Understanding the role of proteoglycans in the [[subendothelial space]] is crucial for comprehending arterial physiology and pathophysiology, particularly in the context of atherosclerosis development.

---

# Hashtags

#proteoglycans 
#subendothelial_space
#low-density_lipoproteins 
#heart_disease 
#hyperblog