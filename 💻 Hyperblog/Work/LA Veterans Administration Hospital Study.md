# LA Veterans Trial

The LA Veterans Administration Hospital Study (LAVAT) was an eight-year double-blind RCT, first reported on by Dayton et al. (1969), and aimed to investigate the effects of substituting seed oils for animal fat on the risk of [[cardiovascular disease]] [(1)](https://www.ahajournals.org/doi/10.1161/01.CIR.40.1S2.II-1)[(2)](https://pubmed.ncbi.nlm.nih.gov/4189785/). The researchers actually took enormous care to ensure that the substitution of seed oils for animal fat was the only substitution the subjects were making. Even going so far as providing ice cream made out of seed oils rather than dairy fat [(3)](https://pubmed.ncbi.nlm.nih.gov/13907771/).

It's important to acknowledge that the trial observed statistically significant increases in dietary [[linoleic acid]] (LA) and LA tissue representation in the seed oil group. LA was higher across all measured tissue compartments when comparing subjects with an adherence of at least 88% to control.

## Outcomes

#### Cardiovascular

With respect to the primary outcomes, ischemic heart disease (IHD) and acute myocardial infarction, there was a statistically non-significant decrease in events in the seed oil group. Despite being non-significant, the results are not expected on the hypothesis that seed oils increase the risk of [[cardiovascular disease]] (CVD).

- **IHD and AMI:**
	- RR: 0.74 [0.51, 1.06]

When acute cerebral infarction (ACI) is added to the composite endpoint, there is a statistically significant decrease in risk in the seed oil group. Results that are truly unexpected on the the hypothesis that seed oils increase the risk of CVD.

- **IHD, AMI, and ACI:**
	- RR: 0.64 [0.46, 0.88]

When the results are scoped to only include fatal atherosclerotic CVD events, the results are also statistically significantly in favour of the seed oil group.

- **Fatal ASCVD:**
	- RR: 0.67 [0.46, 0.96]

#### Cancer

Among the secondary endpoints was total carcinoma, which showed a borderline statistically significant increase in carcinomas in the seed oil group [(4)](https://pubmed.ncbi.nlm.nih.gov/4100347/).

- **Carcinoma:**
	- RR: 1.80 [0.96, 3.35]

This may look worrying at first, but a good explanation was provided by the authors themselves:

>"Many of the cancer deaths in the experimental group were among those who did not adhere closely to the diet. This reduces the possibility that the feeding of polyunsaturated oils was responsible for the excess carcinoma mortality observed in the experimental group. However, there were significantly more low adherers in the entire experimental group than in the controls (table VI). In both groups, the numbers of cancer deaths among the various adherence strata are compatible with random distribution (table V). A high incidence among high adherers would be expected if some constituent of the experimental diet were contributing to cancer fatality."

Additionally, there was an included table that stratified cigarette consumption subgroup by carcinoma. There was a disproportionately higher number of carcinomas within the 1/2-1 pack/day group:

| Cigarette smoking | Control    |                    |                    | Experimental |                    |                    |
| ----------------- | ---------- | ------------------ | ------------------ | ------------ | ------------------ | ------------------ |
|                   | No. of men | Carcinoma observed | Carcinoma adjusted | No. of men   | Carcinoma observed | Carcinoma adjusted |
| Unknown           | 57         | 2                  | 1.74               | 42           | 1                  | 1.15               |
| > 2 packs/day     | 13         | 0                  | 0                  | 7            | 0                  | 0                  |
| 1-2 packs/day     | 57         | 2                  | 1.66               | 38           | 4                  | 5.00               |
| 1/2-1 pack/day    | 129        | 6                  | 7.02               | 173          | 19                 | 16.53              |
| < 1/2 pack/day    | 62         | 3                  | 2.61               | 46           | 4                  | 1.64               |
| Occasional        | 18         | 1                  | 1.03               | 19           | 0                  | 0                  |
| None              | 86         | 3                  | 3.24               | 99           | 3                  | 2.79               |
| **Total**         | **422**    | **17**             | **17.30**          | **424**      | **31**             | **30.15**          |

If we turn our attention to the table that stratifies carcinomas by adherence, we can see that the majority of the excess carcinomas occurred only in the lowest adherence strata:

| Adherence (%) | Control group | Experimental group |
|---------------|---------------|--------------------|
| 0-10          | 2             | 10                 |
| 10-20         | 1             | 2                  |
| 20-30         | 1             | 3                  |
| 30-40         | 0             | 0                  |
| 40-50         | 3             | 3                  |
| 50-60         | 3             | 3                  |
| 60-70         | 0             | 4                  |
| 70-80         | 2             | 2                  |
| 80-90         | 4             | 1                  |
| 90-100        | 1             | 3                  |
| **Total**     | **17**        | **31**             |

Given the numbers in the first table, one of the only reasonable explanations is that the excess carcinomas were occurring in non-adherent smokers in the intervention group. This certainly is not expected on the hypothesis that seed oils increase the risk of [[cancer]].

Furthermore, when you account for this by removing the moderate smokers, there was a statistically non-significant decrease in carcinoma risk in the seed oil group:

- **Carcinoma Adjusted:**
	- RR: 0.65 [0.14, 2.98]

##### Skin Cancer

Lastly, there was a post-hoc analysis of skin cancer performed by Pearce et al. (1971), wherein we see that in the seed oil group, there were ten cases of skin cancer, whereas in the animal fat group there were 21:

| Site                             | Diet phase |              | Post-diet phase |              |
| -------------------------------- | ---------- | ------------ | --------------- | ------------ |
|                                  | Control    | Experimental | Control         | Experimental |
| Buccal and pharynx               | 6          | 10           | 1               | 0            |
| Digestive and peritoneum         |            |              |                 |              |
| Stomach                          | 6          | 12           | 6               | 3            |
| Other                            | 1          | 6            | 2               | 0            |
| Respiratory                      |            |              |                 |              |
| Lung and bronchus                | 13         | 12           | 4               | 3            |
| Other                            | 0          | 1            | 1               | 1            |
| Genitourinary                    | 10         | 16           | 2               | 2            |
| Prostate                         | 8          | 2            | 3               | 3            |
| Other                            | 2          | 5            | 0               | 2            |
| Total carcinomas, excluding skin | 35         | 57           | 12              | 7            |
| ==**Skin carcinomas**==          | ==**21**== | ==**13**==   | ==**4**==       | ==**2**==    |
| Other malignancies*              | 3          | 0            | 1               | 2            |
| Fatal benign tumor               | 0          | 0            | 0               | 0            |
| **Total**                        | **59**     | **70**       | **17**          | **11**       |

#### Body Weight

Some argue that seed oils increase the risk of obesity, however in this eight-year RCT, body weight fluctuated for both groups, and stayed roughly within 1% of baseline for both groups. Essentially subjects ended up essentially where they started in terms of body weight on average.

![[Pasted image 20240714002415.png]]

# References

1. https://www.ahajournals.org/doi/10.1161/01.CIR.40.1S2.II-1
2. https://pubmed.ncbi.nlm.nih.gov/4189785/
3. https://pubmed.ncbi.nlm.nih.gov/13907771/
4. https://pubmed.ncbi.nlm.nih.gov/4100347/

---

# Hashtags

#body_weight 
#cancer
#cardiology 
#cardiovascular_disease
#heart_attack 
#heart_disease 
#la_veterans 
#linoleic_acid 
#lipidology 
#randomized_controlled_trial
#stroke 
#seed_oils  
#hyperblog