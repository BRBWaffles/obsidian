# Claim

>The 4E6 antibody assay makes poor distinctions between native and oxidized LDL.

## Rebuttal

Some criticize this study for the use of the [[4e6 antibody assay]], arguing that this assay is invalid due to supposedly making poor distinctions between native [[low-density lipoproteins]] (LDL) and [[oxidized low-density lipoproteins]] (oxLDL) [(1)](https://pubmed.ncbi.nlm.nih.gov/15883220/). However, this is a [[red herring]], because if the 4E6 assay was truly making poor distinctions between oxLDL and native LDL, the two biomarkers would essentially be proxying for one another to the point of being either interchangeable or even being the same thing. In this scenario, the results of the model would suggest extreme [[multicollinearity]] as indicated by similarly (extremely) wide [[confidence intervals]] for both results.

Wu et al. (2006) used the 4E6 assay to measure oxLDL and discovered that the association between oxLDL and heart disease does not survive adjustment for traditional risk factors like [[apolipoprotein B-containing lipoproteins]] or total cholesterol/[[high density lipoprotein cholesterol]] [(2)](https://pubmed.ncbi.nlm.nih.gov/16949489/). Essentially this means that after accounting for ApoB or TC/HDL, risk is more closely tracking ApoB or TC/HDL-C, and is not particularly likely to be tracking oxLDL at all. 

If oxLDL and native LDL were truly proxying for one another in the model in this fashion, we'd expect the confidence intervals for each relative risk to be inflated and more likely non-significant. But, there is no evidence of extreme multicollinearity in the results. Therefore, it is unlikely that the 4E6 antibody assay is actually making poor distinctions between oxLDL and native LDL. This is important to consider, because the argument for extreme multicollinearity is the primary criticism used against the 4E6 antibody assay's usefulness. But the argument doesn't actually pan out.

---

## References 

1. https://pubmed.ncbi.nlm.nih.gov/15883220/
2. https://pubmed.ncbi.nlm.nih.gov/16949489/

---

# Hashtags

#4e6_antibody_assay
#cardiology 
#claims
#lipidology 
#low-density_lipoproteins 
#oxidized_low-density_lipoproteins 
#red_herring 
#hyperblog