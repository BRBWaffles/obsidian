# Claim

>Seed oils are unhealthy because they cause cholestasis.

## Rebuttal

While partially true, this claim counts as a [[red herring]], because it's not actually seed oils simpliciter that causes [[cholestasis]]. In the [[LA Veterans Administration Hospital Study]], the seed oil group had a dose-dependent increase in gallstones [(1)](https://pubmed.ncbi.nlm.nih.gov/4681896/). However, the most parsimonious way of explaining the effect is through the elimination of hepatic [[phytosterols]], leading to gallstones, not [[linoleic acid]] [(2)](https://pubmed.ncbi.nlm.nih.gov/9437703/)[(3)](https://pubmed.ncbi.nlm.nih.gov/27812789/).

Some seed oils, such as corn oil (the primary seed oil investigated in the LA Veterans trial), have extremely high concentrations of phytosterols [(4)](https://pubmed.ncbi.nlm.nih.gov/31404986/). At the doses investigated, this may be expected to cause issues, but these are not doses that people typically consume, even on the [[standard american diet]].

---

## References 

1. https://pubmed.ncbi.nlm.nih.gov/4681896/
2. https://pubmed.ncbi.nlm.nih.gov/9437703/
3. https://pubmed.ncbi.nlm.nih.gov/27812789/
4. https://pubmed.ncbi.nlm.nih.gov/31404986/

---

# Hashtags

#cholestasis
#claims
#gallstones 
#la_veterans
#liver_function 
#phytosterols
#red_herring 
#seed_oils  
#hyperblog