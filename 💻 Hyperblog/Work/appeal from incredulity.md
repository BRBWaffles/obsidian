# Appeal from Incredulity

The hallmark of this fallacy is assuming that a proposition is false merely because you personally do not believe, or can't imagine, that the proposition is true. This fallacy is tightly tied to the cognitive bias known as confirmation bias, which will be discussed later.

>**Example:** "That's nonsense, because I just can't believe it!"

---

# Hashtags

#fallacies 
#appeal_from_incredulity
#hyperblog