The crux of the genetic fallacy is to conclude that a position is wrong merely in virtue of the one uttering the position. This type of fallacy is remarkably common, if not ubiquitous, in the political debate sphere.  

>**Example:** "I know what Joe Biden says is wrong, because Joe Biden is an idiot."

---

# Hashtags

#fallacies 
#genetic_fallacy
#hyperblog