# Muddying the Waters

Muddying the waters is less of a fallacy and more of a rhetorical device designed to obfuscate and make one's position extremely ambiguous or unclear. This is extremely prevalent in political or ethical debates, wherein it is common to vaguely gesture at your opponent with the mere appearance of disagreement rather than actually providing clear arguments.

>**Example:** "We all know those studies are bad, because you just follow the money if you want to know the truth!"

---

# Hashtags

#fallacies 
#muddying_the_waters
#hyperblog