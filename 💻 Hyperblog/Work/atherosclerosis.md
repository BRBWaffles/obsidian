# Atherosclerotic Cardiovascular Disease

Atherosclerosis is a progressive, inflammatory disease of the arteries that develops over many years. This can be due to various factors including genetic predisposition, poor [[lifestyle]] habits, such as following a [[standard american diet]], and underlying medical conditions. It typically begins with endothelial dysfunction (though not always), allowing [[lipoproteins]], particularly [[low-density lipoproteins]], to accumulate in the arterial wall [(1)](https://pubmed.ncbi.nlm.nih.gov/34773457/)[(2)](). This triggers an inflammatory response, leading to a cascade of events:

- Recruitment of immune cells, especially monocytes, which differentiate into macrophages.
- Formation of foam cells as macrophages engulf [[oxidized low-density lipoproteins]].
- Migration and proliferation of smooth muscle cells from the media to the [[subendothelial space]].
- Production of extracellular matrix, forming a fibrous cap over the lipid-rich core.

**Key features of atherosclerosis:**

- Narrowing of the artery lumen, potentially restricting blood flow.
- Rupture and subsequent thrombosis, leading to a [[heart attack]].
- Chronic inflammatory process.
- Affects various arterial beds (coronary, cerebral, peripheral).
- Major cause of cardiovascular diseases.
- Influenced by multiple risk factors (e.g., dyslipidemia, hypertension, smoking, diabetes).

Management focuses on risk factor modification through lifestyle changes and pharmacological interventions. The goal is to slow progression, stabilize plaques, and prevent complications like myocardial infarction and stroke.

# References

1. https://pubmed.ncbi.nlm.nih.gov/34773457/
2. https://pubmed.ncbi.nlm.nih.gov/32052833/

---

# Hashtags

#cardiovascular_disease 
#atherosclerosis
#heart_disease 
#low-density_lipoproteins 
#inflammation
#subendothelial_space 
#heart_attack
#oxidized_low-density_lipoproteins
#cholesterol 
#hyperblog