# 4E6 Antibody Assay for oxLDL

The Mercodia 4E6 antibody assay is often used to measure [[oxidized low-density lipoproteins]] [(1)](https://www.mercodia.com/products/oxidized-ldl-elisa/). It is directed against a conformational epitope in the ApoB-100 moiety of [[low-density lipoproteins]] that is generated as a consequence of substitution of at least 60 lysine residues of ApoB-100 with aldehydes [(2)](https://pubmed.ncbi.nlm.nih.gov/16762956/). Some have claimed that the 4E6 assay is a [[seed oil claims - 4e6 antibody assay]], but their arguments are not particularly strong.

# References

1. https://www.mercodia.com/products/oxidized-ldl-elisa/
2. https://pubmed.ncbi.nlm.nih.gov/16762956/

---

# Hashtags

#lipidology
#oxidized_low-density_lipoproteins 
#low-density_lipoproteins 
#4e6_antibody_assay 
#malondialdehyde
#hyperblog