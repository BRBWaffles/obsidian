# Strawman

A strawman fallacy is characterized by either intentionally or unintentionally misrepresenting your interlocutor's position or argument, such as to make said position or argument easier to attack. It is also the inverse of the Motte and Bailey fallacy, which is characterized by misrepresenting your own position or argument in order to make it easier to defend.

>**Example:** "People who argue for taxation are pushing communism!"

---

# Hashtags

#fallacies 
#strawman
#hyperblog