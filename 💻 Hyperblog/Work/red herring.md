# Red Herring

A red herring is a type of rhetorical tactic, typically used to obfuscate, that involves referring to an irrelevant point. It can be thought of as a point, reference, or example, that distracts from the main point of a discussion or larger argument. 

>**Example:** "Bacon must be healthy for people because I have a 95-year old grandmother who eats bacon every day!"

---

# Hashtags

#fallacies 
#red_herring
#hyperblog
