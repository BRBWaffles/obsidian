# Claim

>Seed oils are unhealthy because most lipids found in atheromas are oxidized.

## Rebuttal

This claim is merely [[mechanistic speculation]], and is an example of the [[ecological fallacy]]. Essentially, it is being posited that merely because atherosclerotic plaques contain high proportions of oxidized [[linoleic acid]] (LA), that said LA must have caused the atherosclerotic plaque to begin with. It is true that plaques contain very high proportions of oxidized LA [(1)](https://pubmed.ncbi.nlm.nih.gov/10397689/)[(2)](https://pubmed.ncbi.nlm.nih.gov/8632720/). However, we must remember the pathophysiology of [[atherosclerosis]], which involves native [[low-density lipoproteins]] particles being irreversibly bound to [[proteoglycans]] in the [[subendothelial space]] [(3)](https://pubmed.ncbi.nlm.nih.gov/9637699/)[(4)](https://pubmed.ncbi.nlm.nih.gov/27472409/). Once bound, oxidation is inevitable. It's unclear that restricting seed oils will actually lead to reduced oxidized lipids in the plaque.

---

## References 

1. https://pubmed.ncbi.nlm.nih.gov/10397689/
2. https://pubmed.ncbi.nlm.nih.gov/8632720/
3. https://pubmed.ncbi.nlm.nih.gov/9637699/
4. https://pubmed.ncbi.nlm.nih.gov/27472409/

---

# Hashtags

#claims
#ecological_fallacy
#heart_disease
#cardiology
#low-density_lipoproteins
#mechanistic_speculation
#proteoglycans
#seed_oils  
#hyperblog