# Begging the Question

Colloquially, begging the question may be understood as merely leaving certain questions unanswered after an argument is rendered. However, in philosophy, begging the question has a very different definition, and refers to a type of circular reasoning. Begging the question specifically refers to the act of presupposing the conclusion of an argument in its premises. That is to say that at least one of the premises of an argument will hinge on that argument's conclusion being true.

>**Example:** "God exists because it is stated in his own words in the Bible!"

---

# Hashtags

#fallacies 
#begging_the_question
#hyperblog