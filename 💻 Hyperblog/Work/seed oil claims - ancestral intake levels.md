# Claim

>Seed oils are unhealthy because we never consumed this much linoleic acid in our history.

## Rebuttal

Essentially, this claim is an [[appeal to nature]]. If one actually investigates adipose tissue samples, or acceptable proxies like breast milk fatty acids [(1)](https://pubmed.ncbi.nlm.nih.gov/8237871/)[(2)](https://pubmed.ncbi.nlm.nih.gov/16829413/), from [[traditional populations]] eating traditional diets, the [[linoleic acid]] (LA) percentage is non-inferior to that of levels in the [[LA Veterans Administration Hospital Study]] subject population at baseline. Which means the outcomes aren't explained by having too much LA in the diet at baseline (not that such an explanation would make any sense to begin with).

![[Pasted image 20240714004122.png]]

Overall, the results show that the LA intakes of traditional cultures was well in line with average intakes well into the 1970s, as the differences are not [[statistically significant]] different from the LA Veterans study population at baseline [(3)](https://pubmed.ncbi.nlm.nih.gov/22624983/)[(4)](https://pubmed.ncbi.nlm.nih.gov/16039290/)[(5)](https://onlinelibrary.wiley.com/doi/10.1002/cphy.cp050117). Benefits to [[cardiovascular disease]] outcomes can be observed in LA Veterans.

## References 

1. https://pubmed.ncbi.nlm.nih.gov/8237871/
2. https://pubmed.ncbi.nlm.nih.gov/16829413/
3. https://pubmed.ncbi.nlm.nih.gov/22624983/
4. https://pubmed.ncbi.nlm.nih.gov/16039290/
5. https://onlinelibrary.wiley.com/doi/10.1002/cphy.cp050117

---

# Hashtags

#adipose_tissue
#ancestral_diets
#appeal_to_nature 
#breast_milk
#claims
#diets 
#linoleic_acid 
#seed_oils  
#hyperblog