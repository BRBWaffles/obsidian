# NOVA Classification System

The NOVA classification system is a framework for categorizing foods based on their degree of processing [(1)](https://pubmed.ncbi.nlm.nih.gov/19366466/). Developed by researchers at the University of São Paulo, Brazil, it aims to provide a better understanding of the impact of food processing on health.

The system divides foods into four groups:

1. **Unprocessed or minimally processed foods:** Natural foods altered slightly through processes like drying, boiling, or freezing.
2. **Processed culinary ingredients:** Substances extracted from natural foods, such as oils, butter, sugar, and salt.
3. **Processed foods:** Foods altered by adding ingredients like salt, sugar, or oil, such as canned vegetables, cheeses, and freshly made bread.
4. **Ultra-processed foods:** Industrial formulations with little to no whole foods, often containing additives for flavour, texture, or shelf life, such as snacks, soft drinks, and ready-to-eat meals.

This system helps understand the impact of food processing on health and guides healthier dietary choices.

# References

1. https://pubmed.ncbi.nlm.nih.gov/19366466/

---

# Hashtags

#diets 
#processed_food 
#hyperblog