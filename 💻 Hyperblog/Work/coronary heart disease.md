# Coronary Heart Disease

Coronary heart disease (CHD) is a condition where the coronary arteries, which supply oxygen-rich blood to the heart muscle, become narrowed or blocked due to the buildup of plaque, known as [[atherosclerosis]]. This reduction in blood flow can cause chest pain (angina), shortness of breath, and other symptoms. This can be caused by various factors including genetic predisposition, poor [[lifestyle]] habits, such as following a [[standard american diet]], and underlying medical conditions.

**Key features of coronary heart disease:**

- Narrowing or blockage of the coronary arteries due to atherosclerosis.
- Reduced blood flow to the myocardium (heart muscle), potentially leading to ischemia and infarction.
- May present acutely as [[myocardial infarction]] or chronically as stable angina.

Severe blockages can lead to heart attacks, where part of the heart muscle is damaged or dies. CHD is a major cause of death and disability globally, often preventable through [[lifestyle]] changes and medical treatment.

---

# Hashtags

#cardiovascular_disease 
#heart_disease 
#atherosclerosis 
#cardiology 
#lipidology 
#low-density_lipoproteins 
#hyperblog