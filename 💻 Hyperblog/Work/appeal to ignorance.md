# Appeal to Ignorance

An appeal to ignorance is typically defined as affirming that a proposition is true merely because it has not been shown to be false. This is common in domains of science wherein evidence for a particular research question is scant, and the gaps in knowledge can be filled with poor reasoning.

>**Example:** "It's never been shown that blueberries don't cure cancer, so we're safe in assuming that blueberries do cure cancer!"

---

# Hashtags

#fallacies 
#appeal_to_ignorance
#hyperblog