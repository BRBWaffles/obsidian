# Equivocation

An equivocation occurs when one uses a term with a certain meaning in one part of their argument, like the premises, but also uses the same term with a different meaning in another part of their argument, like the conclusion. This is an extremely common fallacy, and occurs often across virtually all domains of debate.

>**Example:** "The announcer said the game ended with a tie, but I didn't see any string, so the announcer must be wrong."

---

# Hashtags

#fallacies 
#equivocation
#hyperblog