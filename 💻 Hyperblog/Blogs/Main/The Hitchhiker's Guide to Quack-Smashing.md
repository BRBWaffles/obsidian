By no stretch of the imagination is there any shortage of quackery on social media. Chances are excellent that if you’ve spent any appreciable amount of time on social media, you have encountered some number of quacks and some sort of quackery that they espouse. You might not even be aware that you’re looking at quackery while you’re being exposed to it. So, the aim of this article will be to attempt to simplify the processes of both identifying quackery and dealing with quackery.

Fundamentally, the issue is not merely that quacks are just interpreting studies incorrectly or affirming crazy beliefs. Those are just symptoms of the problem, rather than being the root cause of the problem. Typically, the problem is occurring within their epistemic framework, and is the very thing that is actually leading them to form their crazy beliefs to begin with. As such, a universally efficacious way to get under a quack’s position and expose them as a raving lunatic is to press them on their epistemic standards in one way or another. That is to say, subject their belief-formation process to a thorough stirring, so that their insanity is allowed to bubble to the surface for all to see.

This means dragging them down to a level that is inferentially prior to their current understanding, and away from the level of research and studies. Just like the scientific method is inferentially prior to studies or study design, scientific epistemology is inferentially prior to the scientific method. If your starting point is on the level of studies and study design, following the inferential lineage backward will eventually bring you to a level where the premises will all be principles, virtues, or axioms. This is the level we need to bring the quack to in order to address the fundamental flaws in their reasoning.

The ultimate aim of this process is to expose errors in the quack’s reasoning, not to necessarily convince them of anything. Of course, the hope is that they correct the complications in their belief-formation process, such that they cease to be a quack by the end of the discussion. However, this isn’t a very likely outcome, but this is nonetheless the outcome that you should be aiming for in order to have the best good faith conversation possible. To explore why this outcome isn’t particularly likely, we’ll have to explore what a quack is, and why they’re typically so immovable. So, let’s get into some definitions.

## Quack

> “A quack is a type of scientific delinquent— one who brandishes the trappings of science, yet whose scientific standards are obstinately in violation of those considered most virtuous within the contemporary philosophy of science.”

It was no easy task to get to the bottom of what is so objectionable about the quack’s behaviour. It took interviewing multiple domain experts and trashing dozens of revisions, but I think I finally have something that is workable and minimally assailable for the time being. Essentially, the trait that quantifies over all quacks is a fundamental lack of respect for the rules of scientific inquiry.

If we were to think of science as a country with laws, a quack would be a contemptuous sort of outlaw within that country. But, quacks are not only rule-breakers by nature, they also actively turn their noses up at the rules— almost relishing in their own intransigence. Despite the commonalities, quacks can usually be categorized in at least one of three distinct ways: **deranged**, **dense**, or **dishonest**.

## Deranged

1. **The Zealot**: one who has assimilated quackery into their identity, and will be emotionally damaged if the quackery is challenged.  
    
2. **The Contrarian**: one who espouses quackery due to strong anti-establishment and/or conspiratorial and/or paranoid tendencies.  
    
3. **The Narcissist**: one who is simply so self-assured of their own infallibility that they cannot fathom that their beliefs could be quackery.  
    
4. **The Aggrieved**: one who feels wronged by some conventional scientific paradigm and is seeking vindication or revenge through quackery.  
    
5. **The JAQ-off**: one who slyly hat-tips to quackery, often in an ostentatious or cheeky manner, but claims to be just asking questions when challenged.  
    
6. **The Circlejerker**: one who is deeply impressionable and merely forms the beliefs of whatever community will accept them.
    

## Dense

1. **The Zombie**: one who has made little to no effort to form their own beliefs, but rather just recapitulates the beliefs of influential people.  
    
2. **The Imbecile**: one who lacks the requisite intelligence and/or cognitive faculties to form rational beliefs of their own accord.
    

## Dishonest

1. T**he Grifter**: one who espouses quackery, not necessarily because they believe it to be true, but rather for some ulterior motive such as money or clout.  
    
2. **The Yo-Yo**: one who spouses both quackery and non-quackery, often to the point of self-contradiction, depending on the context.
    

This probably isn’t an exhaustive list of quack species and subspecies, and will likely be updated in the future. For now, these are the most common types of quacks you’re likely to encounter online or on social media. Most quacks you will encounter will be of the deranged variety, and of them, the majority will be either contrarian or narcissistic. The narcissistic quacks are hardly worth engaging with, unless it is for the benefit of an audience. But other than that, you’ll never convince them of anything because they’re cocksure of their own perfection. Contrarians may be swayed by reason, but it is not particularly likely.

Much like the narcissistic quack, the dishonest quacks are also not likely to be worth engaging with, except for the benefit of onlookers to whom you seek to reveal the quack’s dishonesty. Dishonest quacks, especially grifters, will tend to sway their affirmations in lockstep with the trends of the time. For example, many grifters who were pushing low carbohydrate diets back in 2017 are now pushing raw, grassfed carnivore diets in 2022. It just depends on what’s trending at the moment. It’s a game to them. Engaging is typically pointless and you’re justified in disengaging, in my opinion.

Altogether, you have the best chance of convincing dense quacks, because they’re less likely to be as intransigent as other varieties. These types of quacks don’t typically believe quackery due to some emotional commitment or ulterior motives. Usually they’re just either uneducated or dumb. However, if they’re too dumb to understand the difference between good evidence and bad evidence, convincing them may be ultimately beyond your reach. But it is nonetheless worthwhile to attempt reasoning with them. The best discussions will likely be had with the zombie variety of quack, as they’re usually the closest to just being truly naive, and they’re not necessarily dumb.

Now that we have a bit of a handle on what constitutes a quack, let’s move on to the next definition we need to cover:

## Quackery

> “Any ostensible hypothesis that either fails to satisfy any critical theoretical virtue of a scientific hypothesis (i.e., testability, fruitfulness, scope, parsimony, conservativism) or satisfies fewer theoretical virtues compared to the prevailing scientific hypotheses against which it is intended to compete.”

Given that a proposition is a statement that can be either true or false, a scientific hypothesis can be thought of as basically being an empirically testable proposition. For example, say that we wanted to develop a theory to explain the rising of the sun each morning. We could generate a few different hypotheses. Hypothesis A might suppose that the sun rises because God is pulling it across the sky. Hypothesis B might suppose that the sun rises because it is revolving around the Earth. Hypothesis C might suppose that the sun rises because the Earth is spinning.

Let’s linger on hypothesis A for a moment to discuss some of its issues. Firstly, it’s completely unclear how hypothesis A could be tested, so whether or not it genuinely qualifies as a hypothesis is questionable. Those with a background in science have likely been exposed to the principle that unfalsifiable hypotheses are to be avoided, and hypothesis A is an example of that. Testability is arguably the most important aspect of a scientific hypothesis. Without testability, there is no empirical investigation. Without an empirical investigation, there is no science. The hypothesis ends up being completely ad hoc (which we will discuss later).

On the other hand, hypothesis B certainly has the capacity to make predictions. One could create a model wherein the sun travels around the Earth, and observations can certainly be made that are consistent with that model. Hypothesis C could be tested similarly. One could construct a model wherein the Earth is spinning and the sun is fixed in place, and observations can be made to see if that model pans out.

So far, hypothesis B and hypothesis C don’t seem to underdetermine phenomena differently, and the observations seem equally expected on both hypotheses. However, what if we also made the observation that there are other planets out there in space, and we also observe that those planets seem to move in relationship to the sun in a way that suggests that the sun is a fixed object. This observation is more expected on hypothesis C than hypothesis B. The observed phenomena are underdetermined on hypothesis B, and thus hypothesis C would come out on top until there is a better hypothesis to supplant it.

Great! We have established what it takes for a proposition to be a scientific hypothesis, as well as what it takes for one scientific hypothesis to prevail over another. Now let’s move on to discuss the different criteria that such hypotheses need to satisfy in order for them to be competitive within the domain of science. These are the epistemic virtues we touched on earlier— the standards by which the viability of a tentatively competing scientific hypothesis will be measured. [¹](https://www.oxfordbibliographies.com/display/document/obo-9780195396577/obo-9780195396577-0409.xml) [²](https://books.google.ca/books/about/How_to_Think_About_Weird_Things_Critical.html?id=YR4iAAAAQBAJ&redir_esc=y)

## Criteria of Adequacy

1. **Testability**: A hypothesis is scientific only if it is testable, that is, only if it predicts something more than what is predicted by the background theory alone.  
    
2. **Fruitfulness**: Other things being equal, the best hypothesis is the one that is the most fruitful, that is, makes the most successful novel predictions.  
    
3. **Scope**: Other things being equal, the best hypothesis is the one that has the greatest scope, that is, that explains and predicts the most diverse phenomena.  
    
4. **Parsimony**: Other things being equal, the best hypothesis is the simplest one, that is, the one that makes the fewest assumptions.  
    
5. **Conservatism**: Other things being equal, the best hypothesis is the one that is the most conservative, that is, the one that fits best with established beliefs.
    

All else equal, if an alternative hypothesis fails to outcompete a prevailing hypothesis on any of these measures, why form the belief that the alternative hypothesis is more likely to be true compared to the prevailing hypothesis? Beyond the testability of a hypothesis, the extent to which a hypothesis fails to satisfy any of these theoretical virtues will determine the “ad-hocness” of that hypothesis. An ad hoc hypothesis is also known as a “just-so story”, which is a phrase you may have heard before.

While it goes without saying that ad-hocness is undesirable in science, it is important to emphasize that this is the crux of the quack’s rhetoric. Quackery is like an artichoke of idiocy, and just-so story-telling is at the heart of it. You have to peel it slowly to expose the fuckery. This sort of delinquent fabulism takes many forms, and it’s not always obvious when a hypothesis is ad-hoc and failing to satisfy one or more theoretical virtues compared to another hypothesis. But, hopefully your intuitions can be adequately primed with a few examples. Let’s examine this scientific hypothesis:

> “Vegetable oils are the cause of heart disease.”

At first glance, this hypothesis appears to be extremely attractive in its parsimony. It appears to be making very few assumptions, as it is reducing the cause of heart disease down to a single variable. However, this is an illusion. This hypothesis must actually bootstrap an enormous number of assumptions in order to compensate for its lack of scope and fruitfulness. Not only does the totality of the empirical evidence weigh heavily against this hypothesis, it’s not clear what novel predictions the hypothesis has generated, if any at all.

The primary issue is that the hypothesis is lacking in scope because it does not account for the majority of our observations regarding vegetable oils and heart disease. Typically, the quack will attempt to compensate for this shortcoming by casting doubt on existing research— “those findings are wrong because epidemiology is pseudoscience”. What the quack doesn’t realize is that this actually decreases the parsimony of their hypothesis. This leaves them with an ad-hoc story that is not particularly unifying, fruitful, or parsimonious. Let’s examine another hypothesis:

> “Ancestral foods protect against all illnesses.”

On the face of it, this hypothesis might appear similar to the first in that it lacks scope and fruitfulness, and in turn compromises its own parsimony. However, it may be even worse than that. If ancestral foods are referring to foods that humans consumed during some prehistoric time period, then the hypothesis is actually untestable. Humans no longer have access to those foods, and as such the hypothesis is almost entirely ad hoc, and the theoretical virtues that it upholds are few to none.

The more sensitive you become to when a hypothesis is failing in its virtuousness, the more straightforward it will be to identify and dispatch quackery. For instance, consider what has been described above in previous examples, and examine these other quack hypotheses carefully:

> “Red meat protects against mental illness.”  
> "Sunlight protects against skin cancer.”  
> “Blueberries cure Alzheimer’s disease.”  
> “Refined carbohydrates cause obesity.”  
> “Milk products impair bone health.”  
> “Soy products feminize men.”  
> “Vegan diets cure cancer.”

Remember that quacks fundamentally don’t care about scientific rigour, and virtually all quackery will follow a similar structure at its core— an utter lack of respect for the rules we just discussed. Thus, there is a virtually universally efficacious way of uprooting quackery. Simply interrogate the quack about how their hypothesis better conforms to the rules compared to other hypotheses, and watch them crumble.

Once you start scrutinizing quackery like this on this basis, you will quickly realize that quacks are just master fabulists— iron chefs of word salads. Mind you, quacks will never admit to this. Even when their bullshit has been revealed to them point blank, the exact lack of rigour that got them to be in their current state will end up keeping them smiling through their humiliation.

At this point it would be worthwhile to discuss the **Quack’s Trichotomy**. This concept has been borrowed from Lance Bush’s anti-realist metaethical thesis. While it was originally a way of categorizing different types of moral realism, it would appear to be highly applicable to categorizing different types of quackery as well.

Basically, quackery will ultimately reduce down into one of three categories: **false**, **trivial**, or **unintelligible**. As discussed above, most quackery will end up being false (or at least more likely to be false than true). But, there are some other common cases to explore. Identifying just what kind of utterances the quack is making will be helpful in figuring out whether or not it’s even worth entertaining their madness. Consider the following three interpretations of this hypothesis:

> “Red meat is healthy.”

## False Interpretation

Perhaps the tentative quack cashes out the term “healthy” into some sort of claim about red meat not increasing the risk of heart disease. This can be tested, and it turns out that when the best available data is aggregated together, across multiple domains, red meat consumption reliably associates with an increased risk of heart disease. This means that this hypothesis would just be false, or more likely false than true.

## Trivial Interpretation

Let’s just say that all the tentative quack means by “healthy” is that they themselves just feel really good on a diet that is high in red meat. In this case, the hypothesis is actually going to be “I feel really good on a diet that is high in red meat.” This is not anything that anyone would contest, as it’s clearly true. However, it’s trivially true, and a pointless proposition to utter. We need not debate this. This claim may not necessarily make them a quack, but we should probably just leave this idiot to their fuzzy feelings and move on.

## Unintelligible Interpretation

In this case, we’ll imagine that the tentative quack doesn’t unpack the utterance any further, and just continues to insist that red meat is “healthy”, over and over. If no further clarification is offered, this interpretation of the hypothesis is just gibberish. What is healthy supposed to mean? To me, “healthy” is a relational concept that joins many relata— X is healthy for Y compared to W relative to standard Z (where healthy is defined as an exposure that increases the lag-time to the onset of illness compared to a different exposure). Unless their meaning is unpacked in an intelligible way, their utterance doesn’t even rise to the level of being a proposition. They’re literally just gibberating, and we need not debate their ramblings. Leave them to their delusions.

In an exceedingly small minority of cases that fall outside of the scope of the **Quack’s Trichotomy**, the tentative quack’s proposition will be true and convincing, rather than being false, trivial, or unintelligible. In this case, the proposition likely isn’t quackery and you likely just mistook the proposition as quackery due to miscommunication. If this happens, you should just accept the proposition and feel good that you learned a new fact today. Enjoy it when it happens. But again, this almost never happens.

There is one final trope we need to explore, though. Every so often, you will encounter someone who is committed to extraordinarily high standards of evidence. For example, quacks in the nutrition blogosphere will sometimes claim the following:

> “Evidence cannot be obtained in nutrition science unless you have studied the effects of a food in a multi-generational, quadruple-blind randomized controlled metabolic ward crossover trial in human clones.”

This is not really a hypothesis. It’s just a claim that directly pertains to scientific epistemology itself, which means we must deal with this in a unique way. In this case, we should simply ask them what theoretical virtue would be either violated or unable to be satisfied unless we had such evidence for our hypothesis? As such, it would be worthwhile to get into a few definitions regarding what constitutes evidence.

## Evidence

> “That which is more expected on a given hypothesis compared to the negation of that hypothesis.”

On this definitions, falling short of achieving a trial design like the one described above does not bar us from making discoveries that are more or less expected on different hypotheses. Thus, failing to achieve such a trial design is not a barrier to discovering any sort of evidence. If a short trial with a limited number of people finds an effect of some intervention, that effect is still going to be expected on a hypothesis that predicts it. So, it’s still evidence.

Now we’re ready to start tackling quackery ourselves. Next up, we’ll go through a universally applicable procedures for smashing quackery. The beauty is that the procedures leave the burden of proof squarely on the quack, and pressure test their epistemic standards. This means that you probably don’t even need to show up with studies of your own if you don’t want to. The ball will mostly be in their court, and the burden of proof will be squarely theirs. By the end they’ll find themselves looking up from within their own grave and they will have nobody but themselves to blame for them being there.

Before we move on to the quack-smashing debate procedure, it's recommended that you download the included [dialogue tree](https://drive.google.com/file/d/1QQaN6HRwzp3kY2DAcnHVBxeX6jBhrvkw/view?usp=share_link) and refer to it while reading.

###### PHASE ONE

###### CLARIFICATION

## Step 1: Ask for a proposition

Essentially, a proposition is what is known as a truth-apt statement. Meaning that it is a statement that can be either true or false. You can think of a proposition like a claim. A common tactic among quacks is to dance around some implied commitment without ever actually explicitly making any claims.

Quacks are notoriously unclear when they communicate. Sometimes quacks will simply gesture in your direction with a series of vague utterances that merely have the appearance of communicating disagreement. For example, a quack might say to you:

> “Read the studies you idiot!”  
> “Follow the money to know the truth!”  
> “We’ve been lied to for over fifty years!”

Notice how vague these statements are. Quackery thrives on vagueness. You must straightforwardly ask the quack for their position— “I’m sorry, I just want to understand what you mean. What do you think is true that people who believe this study think is false?” Be firm, and stand your ground. Press them until they give you an utterance that at least has the appearance of something propositional.

## Step 2: Evaluate their ostensible proposition

Once you have a proposition, we begin the clarification phase. During this step you are aiming to demystify anything that you find unclear. If there are terms over which there may be disagreement, such as relational terms with missing relata, you must ask for clarification.

If clarification is required, proceed to **step 2.1**. If clarification is not required, proceed to **step 2.2**.

## Step 2.1: Ask for clarification

Essentially what we need from the quack is a clear, contestable proposition. Scrutinize every word in their proposition if necessary. This is not to bog down the debate, but to make sure that there is as close to a complete, unambiguous shared understanding of the terms being used by the quack as possible. In the vast majority of cases, asking the quack simple clarifying questions about their proposition will make it fall apart on the spot.

If the tentative quack’s explanation devolves into bullshit like rambling or gibberish, just walk away from the idiot. If it appears intelligible and it’s just some trivial utterance that isn’t worth discussing, we can start assessing its truth value.

###### PHASE TWO

###### ARGUMENTATION

## Step 2.2: Check for modal claims

If we manage to obtain an intelligible proposition from the quack, we can begin checking for modal language. Modal language typically refers to concepts like impossibility, necessity, possibility, or contingency. Be sensitive to when your interlocutor is using words that function as synonyms for any of these terms. For example, words like “can”, “able”, “may”, “could”, or “capable” should all be taken as synonyms for the modal term “possible”, whereas words like “unable”, “cannot”, “couldn’t”, “unable”, “incapable”, should all be taken as synonyms for the modal term “impossible”.

Additionally, if your interlocutor suggests that something is necessarily the case, that should be taken as just another way of saying that the contrary is impossible. As such, impossibility and necessity are modal terms that can essentially be captured by the same modal operator. Similarly, if your interlocutor suggests that something is possible, all they’re saying is that it's necessarily not impossible, which is to say that it's contingent upon something else. So, much like impossibility and necessity, possibility and contingency can also be interpreted interchangeably, under the same modal operator. At the end of the day the terms can ultimately be cashed out into what is impossible or possible.

To be clear, these terms are just operators, and don’t mean much unless they’re in reference to a given modality. Modalities are just means by which these modal operators can be understood. There are two primary modalities that are commonly discussed in the philosophy literature— physical and logical. For example, something is logically impossible if it entails a contradiction, and something is physically impossible if it violates a law of physics.

Of course there are other modalities. In fact, there are probably infinite numbers of modalities on which an operator like impossible can be understood. For instance, while it’s certainly logically and physically possible to murder someone in any country, it is legally impossible to murder someone in most countries. Though we don’t really use modal language in this way.

Overall, impossibility can be interpreted as something that is incompatible with a given rule/standard or set of rules/standards, and possibility can just be interpreted as something that is compatible with a given rule/standard or rule/standard set.

In most cases, if the modal operator they’re invoking relates to possibility, then it’s probably not even worth continuing the discussion. In the vast majority of cases it’s just trivial to claim something is possible. If the proposition contains a modal operator related to impossibility, proceed to **step 2.3**. If their proposition does not contain a modal operator, proceed to **step 2.4**.

## Step 2.3: Ask which law is broken on their stated modality

This step is pretty straightforward. If your interlocutor is claiming something is impossible, ask them on what modality is it impossible. If necessary, explain to them what modalities and modal operators are, just so they’re both clear on what you’re asking of them and what kind of claim they’ve actually made.

If they cannot unpack the modality and the law that’s violated on that modality first give them the opportunity to amend their claim. If they refuse to amend the claim or are unable to unpack the claim, then proceed to **step 6**. If they actually manage to unpack the modality and the law that’s violated on that modality, and it’s convincing to you, you should probably just concede.

## Step 2.4: Ask for a goalpost

Assuming there were no modal claims in our tentative quack’s proposition, we can proceed to the first step in the actual debate— requesting a goalpost. Basically, you will merely ask them what evidence would be required for them to affirm that their proposition is false. This is different than asking them what evidence it would take for them to reject their proposition. We want to know what it would take for them to affirm their proposition’s negation.

This step isn’t vital, so it’s not a huge concern if our tentative quack doesn’t render a goalpost to you. However, keep in mind that if they can’t tell you their goalpost for affirming their proposition’s negation, they are essentially telling you that they don’t even know why they believe that their proposition is true. So, it’s up to you if you want to proceed further. If you want to stop here, just shame them for being a sophist and disengage.

If you decide to continue, proceed to **step 3**.

## Step 3: Ask for an inference for their proposition

This step is pretty straightforward. You’re just straight up asking for the argument for the proposition. After you’ve obtained a clear, contestable proposition from the quack, you must then ask them what the evidence is for their proposition. Asking the quack for evidence is perfectly fair. Don’t let them try to convince you otherwise. They bear the burden of proof, and the onus is solely on them to demonstrate the merits of their proposition.

If they actually manage to render an argument, you can proceed to **step 3.1**. If they refuse to render an argument, proceed to **step 6**.

## Step 3.1: Identify the type of inference

Basically there are two broad categories of inference someone can make. Either their inference is going to be a posteriori or it is going to be a priori. Both a posteriori and a priori inferences are forms of deductive arguments. However, in contrast to an a priori inference, an a posteriori inference requires synthesizing prior experience of the world. The truth or falsity of an a posteriori inference will rest on empirical evidence.

## A Posteriori

> **P1)** An unmarried person is more likely to be depressed.  
> **P2)** Jim is unmarried.  
> **C)** Therefore, Jim is more likely to be depressed.

An a priori inference doesn’t require prior experience of the world, and the truth of falsity of the inference will hinge on the definitions or meaning of the terms used.

## A Priori

> **P1)** An unmarried man is a bachelor.  
> **P2)** Jim is an unmarried man.  
> **C)** Therefore, Jim is a bachelor.

For the purposes of this article, we’ll just be focusing on a posteriori inferences. If you are interested in how to address a priori inference, please refer to the included dialogue tree at the top of this section. Otherwise, proceed to **step 3.2**.

## Step 3.2: Check for modal claims

Generally speaking, if their inference is a posteriori and it contains a modal operator, they’re likely to be referring to a physical modality (but they could be referring to another modality).

If they are invoking an operator that is related to impossibility, proceed to **step 3.3**. Once again, if the invoke a modal operator related to possibility, then it’s probably worthwhile to just agree and disengage, as it’s typically just trivial to claim that things are possible. If there are no modal operators in their claim, proceed to **step 3.4**.

## Step 3.3: Ask which law is broken on their stated modality

The overall procedure is identical to **step 2.3**. If the quack can’t unpack the claim or they refuse to amend the claim, then proceed to **step 6**. If they actually manage to unpack their the modality and the law that’s violated on that modality, and it’s convincing to you, you should probably just concede.

## Step 3.4: Evaluate their empirical evidence

During this step, it pays to have some critical appraisal skills to fall back on, but it’s not necessary. The most important thing to remember is that you have to be honest about what you find convincing and you have to be honest enough to amend your views according to new evidence.

Keep the definitions of evidence that we discussed earlier in mind going forward. After the quack has rendered their evidence, examine it carefully. In the vast majority of cases it will be unclear how this evidence should be more expected on their hypothesis as opposed to some other hypothesis, even another hypothesis that you literally dream up on the spot as a counter explanation (which we will be covered in later steps). Consider the following proposition:

> “Carnivorous diets made humans more intelligent.”

A bold proposition like this certainly requires substantial evidence, which is perfectly reasonable to request of the quack. Let’s say you ask for the evidence, and the quack renders something along the lines of this in response:

> “Because plant agriculture decreased human brain size by 11%.”

Bearing all of the above in mind, critically evaluate their evidence and think hard about the assumptions it makes. If you don’t find anything objectionable, it is perfectly OK to admit that sufficient evidence for the proposition has been rendered, and that you concede the proposition on that basis.

If the quack managed to render a goalpost to you in **step 2.4**, then proceed to **step 3.5**. If they did not render a goalpost to you, and their evidence is not convincing to you, then proceed to **step 5**.

## Step 3.5: Compare their evidence to their goalpost

Check to see if their empirical evidence is actually consistent with the goalpost they described. If their empirical evidence is either consistent or stronger than the sort of evidence described in their goalpost, then simply proceed to **step 5**. However, if their empirical evidence is _weaker_ than the sort of evidence they described in their goalpost, then the quack has just straightforwardly contradicted themselves and you can proceed straight to **step 6**.

## Step 5: Propose an alternative hypothesis

As with **step 3.4**, it pays to have some critical appraisal skills, but it is again not actually necessary. The only thing that you really require to proceed with this step is the creativity to dream up alternative hypotheses. But not just any alternative hypothesis. Your alternative hypothesis has to make the same prediction as their hypothesis, but also be mutually incompatible with their hypothesis.

In reference to the evidence presented in **step 3.4**, while this evidence may sound hilarious to us, it may actually be convincing to some people, so contesting it is probably still a good idea. Don’t dismiss it outright. At this point, it may be true that the quack has already put their foot in their mouth. But continue to be a fair interlocutor, and give the quack an opportunity to either swallow it or pull it out. Hit them back with an alternative hypothesis that makes the same prediction:

> “Perhaps agriculture actually increased the efficiency of the human brain, and there was actually no change in intelligence.”

Remember that it’s not at all clear why plant agriculture decreasing human brain size by 11% would necessarily, or even probably, be evidence for carnivorous diets making humans more intelligent. Perhaps plant agriculture increased the efficiency of our brains, such that our brains could achieve the same level of intelligence using less tissue. So, why would plant agriculture decreasing human brain size by 11% be more expected on the quack’s hypothesis as opposed to your alternative hypothesis?

Now the quack is in a tough position, because he has to actually explain why the evidence in question is more expected on their hypothesis than the one you just cooked up on the spot. In the vast majority of cases, the quack will not be able to produce any compelling reason for why one hypothesis has more explanatory power over the other.

Notice as well that we’re not asking them to substantiate their evidence, and we’re not scrambling to find studies of our own to refute their evidence. We’re just asking the quack why their hypothesis is more virtuous than ours. This is not a strategy that virtually any quacks are going to be prepared to deal with (because if they were, they probably wouldn’t propose stuff as stupid as they typically do).

In the vast majority of cases, merely asking the quack why the evidence in question is more expected on their hypothesis, rather than your own hypothesis, will lead them to fold like a chair, right on the spot.

If your interlocutor actually can demonstrate that the evidence they brought to the table is indeed more expected on their hypothesis than some alternative hypothesis, you have a few options. You retry **step 5**, and attempt to create another alternative hypothesis that will challenge the strength of their evidence even harder. Alternatively, you can concede the proposition for the time being and come back to it later, even if you don’t find it convincing. Just be honest. Conceding the debate doesn’t mean that your interlocutor was correct.

Remember, you can always be agnostic (even about your own alternative hypotheses), and try to remember that you don’t need to affirm the negation of their proposition. The onus is not on you to prove anything in this exchange. Not a goddamn thing. Remember that.

Assuming they’re unable to answer your question, it must be reiterated that it is very important to never allow the quack to flip the burden of proof on you. It’s their proposition, and the burden of proof is theirs and only theirs. A tip for identifying when the quack is trying to flip the burden of proof is when they attempt to ask questions that are not clarifying questions. Any question other than a clarifying question is always a dodge when you hold the line of questioning, and your line of questioning is only over when you’ve obtained all of the answers you require.

If the quack rejects your alternative hypothesis, proceed to **step 5.1**. If the quack accepts your alternative hypothesis and admits that he has no justification for why their evidence is more expected on their hypothesis than your hypothesis, then proceed to **step 6**.

## Step 5.1: Ask what’s wrong with your alternative hypothesis

Rejecting your alternative hypothesis could mean a couple different things. Either they’re calling the alternative hypothesis itself into question, or they are going to explain why their evidence is more expected on their hypothesis than your hypothesis. If they’re rejecting your alternative hypothesis, just ask them for the explanation. If their explanation is convincing to you, there are a couple of different options. You can either concede or return to **step 5** and press them with an even stronger hypothesis. It just depends on how comfortable you are giving it another shot.

Alternatively (and altogether more likely), if they can’t give you a clear explanation as to why their evidence is more expected on your hypothesis, then proceed to **step 6**.

###### PHASE THREE

###### STEAMROLLING

## Step 6: Demand that they concede their proposition

Once you find yourself in a position where you have not conceded the proposition and the quack is unable to render an argument in favour of the proposition, you’ve obtained all you need from the quack to begin steamrolling. Just refuse to proceed until you obtain either an argument for the proposition or a concession on the proposition itself. Anything other than these outcomes is not satisfactory. Don’t accept any “agree to disagree” cop-outs. Hold their feet to the fire and press the issue.

The quack may try to spiral down on some tangent that isn’t germane to any of the previous questions you have asked them. In this event, redirect them immediately after their ramble. If their ramble is excessively long, feel free to cut them off. Tangents waste time, and it’s inappropriate to let tangents go to completion. Most quacks are bad faith actors, so concessions are too optically intolerable for them and rendering an argument for their claim is usually beyond their capabilities. Most will simply just disengage once you’ve gotten this far.

Lastly, we have to talk about an exceptionally rare outcome you will almost never encounter on your quack smashing journey— the quack concedes the proposition. Congratulate and praise the quack for their honesty. In fact, they may not even be a quack anymore. You may have rescued them from utter insanity, and that’s certainly worth a pat on the back for both of you.

Thanks for reading! If you enjoy my writing and want more content like this, consider pledging to either my [Patreon](https://www.patreon.com/thenutrivore) or my [LiberaPay](https://liberapay.com/TheNutrivore/)! Alternatively, I accept crypto as either ETH, MATIC, or BTC at 0xdfcb7e3D00EdC138300E9Ad122dB0ebE85426a65!

## References:

[1] ‘Theoretical Virtues in Science’. Obo, Accessed 24 Dec. 2022. [https://www.oxfordbibliographies.com/display/document/obo-9780195396577/obo-9780195396577-0409.xml](https://www.oxfordbibliographies.com/display/document/obo-9780195396577/obo-9780195396577-0409.xml).

[2] Schick, Theodore, and Lewis Vaughn. How to Think About Weird Things: Critical Thinking for a New Age: Seventh Edition. McGraw-Hill Higher Education, 2013. [https://books.google.ca/books/about/How_to_Think_About_Weird_Things_Critical.html?id=YR4iAAAAQBAJ&redir_esc=y](https://books.google.ca/books/about/How_to_Think_About_Weird_Things_Critical.html?id=YR4iAAAAQBAJ&redir_esc=y)