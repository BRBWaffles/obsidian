Many Paleo diet advocates claim that hunter-gatherer diets optimally promote the long-term health of human beings. There are typically two primary justifications for this claim— firstly, the fact that hunter-gatherer populations typically appear to be robustly healthy, and secondly, the fact that humans evolved eating these types of diets. While these are technically statements of fact, I find myself forced to take a page out of the Paleo-dieter's playbook, as I remind them that correlation doesn't equal causation. So, let's dig into why their reasoning is flawed.

Firstly, let me hit you with a thought experiment for a moment. Do you think that the elderly people within a given population would appear either more healthy or less healthy if that population _never_ had access to modern medical technology? Think about this for a moment. You may understandably intuit that their health would probably be worse, correct? But that probably isn't what would happen. They would probably appear healthier, and I'll explain why.

If a population never had access to modern medical technology, one of the things you couldn't reliably do is save sick or injured children. No matter what culture you observe, a sizable proportion of children will develop infectious illnesses. These illnesses, like bacterial or viral infections, are generally mundane by modern standards. However, without modern medicine many children who developed these types of illnesses would likely not survive them.

Those who are more prone to illness likely do not have the same chances of survival as those who are less prone to illness. As such, why wouldn't the elderly in this hypothetical population present with more robust health as a result? Without medical intervention, the environment is essentially selecting for the fittest possible individuals in our hypothetical population. This would likely generate the appearance of more robust health in the elderly. Consider this carefully for a moment. If you weed out all of the weaker people, of course the remaining population is going to appear stronger.

This presents a significant problem for Paleo diet advocates who choose to cite the robust health of hunter-gatherer populations as a justification for the Paleo diet, or as a justification for recommending the diet to others. Most Paleo-dieters do seem to be aware of the environmental adversity faced by hunter-gatherer populations. But they often don't seem to appreciate how this also produces significant challenges for their narrative.

For example, if you remind a Paleo-dieter that hunter-gatherers typically had an average life expectancy of around 30 years, they will often immediately retort by stating that those estimates are confounded by infant and child mortality. But therein lies the problem— they can't have it both ways. A Paleo diet advocate cannot use that argument without tacitly admitting that infant and child mortality is typically enormously high among hunter-gatherer populations. Which it most certainly is [[1]](https://www.sciencedirect.com/science/article/abs/pii/S1090513812001237).

Approximately 26.8% of infants and 48.8% of prepubescent children die in hunter-gatherer populations. With such a high proportion of children dying, how could it be the case that the apparent health of elderly hunter-gatherers is **not** coloured by this? Remember, hunter-gatherer populations likely appear healthier because the least resilient members of those populations are already dead. Paleo diet advocates cannot admit that hunter-gatherer populations had high rates of infant and child mortality without also admitting that the resulting population is a heavily biased sample.

This type of confounding is known as survivorship bias. Basically, survivorship bias is a type of selection bias that can occur when those who did not survive a selection event or process are overlooked in favour of those who did survive. For example, let's say we were trying to construct better body armour for soldiers to wear in combat. Perhaps we might conduct a study of the soldiers who returned from battle. We could collect bullet wound distribution pattern data to help ascertain where soldiers were most likely to get shot.

However, as we see in the graphic above, this sort of analysis would exclude all those who were shot and did **not** survive. It would also overlook the sorts of bullet wound distribution patterns that tended to lead to death, which would actually have given us a significantly clearer picture of how we might construct better body armour.

Likewise, when Paleo diet advocates claim that we should eat like hunter-gatherers because hunter-gatherers are robustly healthy, they're not appreciating how survivorship bias is confounding their appraisal of hunter-gatherer health. They are essentially overlooking the other fifty percent of the hunter-gatherer population that didn't survive.

For this reason, it is dubious to use the apparent good health of hunter-gatherers as the basis for the assumption that their diets are appropriate for modern humans. Until Paleo diet advocates can figure out a way to explain why survivorship bias would not be confounding in an evaluation of hunter-gatherer health, they cannot rely on the apparent good health of hunter-gatherer populations to determine the applicability of hunter-gatherer diets to modern humans. In reality, the degree to which hunter-gatherer diets are appropriate for modern humans remains unclear.

But this isn't the only erroneous argument that Paleo-dieters will use to justify their claims. Often times Paleo diet advocates will also suggest that since we evolved eating certain foods, it is absurd to believe that any of the foods that we evolved eating could pose a long-term health risk to the average person. This reasoning has been used to dismiss robust and well-studied diet-disease relationships, such as saturated fat and cardiovascular disease, red meat and cancer, or even sodium and hypertension [[2]](https://pubmed.ncbi.nlm.nih.gov/32428300/)[[3]](https://pubmed.ncbi.nlm.nih.gov/28487287/)[[4]](https://pubmed.ncbi.nlm.nih.gov/27216139/)[[5]](https://pubmed.ncbi.nlm.nih.gov/28655835/).

Right off the bat it is quite easy to identify that this is a blatant appeal to nature fallacy, and can be outright dismissed on that basis alone. However, it is important to describe precisely why this line of reasoning fails. What we really want to know is whether or not foods are necessarily beneficial (or neutral) for long-term health merely because we evolved eating them.

To explore this question, let's first briefly consider how Darwinian natural selection works. Essentially, it is the process by which random gene mutations are selected for by different environmental challenges. Some mutations are better at dealing with certain environmental challenges than other mutations. As a result, these more adaptive mutations increase an organism's chances of producing offspring. These organisms subsequently pass on these adaptive mutations to their offspring as well.

In this image we see that black mice are less likely to get eaten by the bird than tan mice. For this reason, the random mutations that produces black mice rather than tan mice ends up being naturally selected for by the environment. However, environmental challenges like getting eaten aren't quite the same as environmental challenges that affect the long-term health of an organism.

Getting eaten when you're young is an acute event. Developing a life-threatening chronic disease in old-age, as a result of a life-long environmental exposure (like a food or nutrient), is a long, protracted event. It is unlikely that selection pressure applies to these two events symmetrically, because adaptations occur as a function of successful reproduction. As a result, the probability that _deleterious_ traits will be successfully selected against likely diminishes with age.

So, the question ends up being: how long after reproductive age does natural selection still robustly apply to human beings? Surely selection pressure doesn't end at reproductive age, because human children need human adults to raise them and care for them. But does selective pressure exist to a meaningful degree for those in the age ranges that typically associate with life-threatening chronic disease? Some scholars have attempted to estimate the force of natural selection as a function of age [[6]](https://pubmed.ncbi.nlm.nih.gov/30124168/).

Applying the above graph to human beings, our best estimations suggest that the forces of natural selection rapidly wane down to nil shortly after sexual maturity, which would be approximately 16 to 17 years of age. Around the ages of 30 to 40 is when humans likely enter the "selection shadow", which is the zone wherein natural selection no longer robustly applies.

It seems highly unlikely that the fate of a population could ever hinge on the fitness of middle-aged people who are past their prime. For fun, let's estimate the age range wherein peak human performance is likely to occur. Perhaps we could look at the average age range of Olympic athletes [[7]](https://venngage.com/blog/olympics/).  

The average age range of Olympic athletes is between ~22.5 and ~25.5, which is well before the age range seen within the selection shadow. Which honestly makes sense if you think about it. As you age it becomes less likely that natural selection will select against deleterious traits, like losing athletic performance. Once inside the selection shadow, there is likely insufficient selective pressure to extend peak physical performance into higher and higher age ranges.

The selection shadow may actually be observable in some existing traditional populations as well, such as the Tsimané people of Bolivia. While they are technically hunter/forager-horticulturalists and not strictly hunter-gatherers, they are one of the only traditional populations for which we have decent data regarding the progression of chronic disease.

Using a measurement of atherosclerotic cardiovascular disease (ASCVD) progression known as coronary artery calcification (CAC) scoring, researchers were able to quantify the prevalence of ASCVD by age group among the Tsimané [[8]](https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(17)30752-3/fulltext).

It should also be noted that CAC scores are indicative of advanced ASCVD [[9]](https://pubmed.ncbi.nlm.nih.gov/24530667/). The exclusive use of CAC scoring in this study of the Tsimané leaves us with many interpretive challenges. For example, if CAC scores are representative of advanced ASCVD (so called "hard plaques"), what proportion of less advanced ASCVD (so-called "soft plaque") might have been overlooked? It is difficult to say for sure. But the bottom line is that the Tsimané experience increases in chronic disease at approximately the same time as modern populations— well inside the selection shadow.

Granted, the prevalence of chronic disease in the Tsimané is overall lower than that of Western cultures. But, this could be expected given the fact that their diets and lifestyles are likely preferable to that of Western cultures as well. Not to mention the possible confounding due to survivorship bias, to which they are also not immune [[10]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3502712/). The Tsimané have infant mortality rates that are many fold higher than in Western cultures. This means that the Tsimané are vulnerable to the same type of confounding via survivorship bias that we discussed earlier, thus adding more layers of interpretive challenges.

The methods used to determine the age-ranges of the Tsimané were also questionable, and relied mostly on anecdote and personal judgement rather than objective measurements.

_“Birth years were assigned based on a combination of methods including using known ages from written records, relative age lists, dated events, photo comparisons of people with known ages, and cross-validation of information from independent interviews of kin.”_

When compared to more objective measures of age, the methods described above would appear to overestimate ages as the age of the subjects increases [[11]](https://pubmed.ncbi.nlm.nih.gov/27511193/). Below the age of 40, estimates appear to be rather accurate. However, the age estimations used by Kaplan et al. (2017) would seem to overestimate ages by approximately 15 years above the age of 60. After adjusting for this, it is unlikely that CAC prevalence among the Tsimané differs significantly from Western populations. In fact, it might actually be higher.

It is also imperative to mention that foods to which we are adapted might actually be more likely to be harmful for us as we age. This is because of a concept in evolutionary biology called antagonistic pleiotropy, which is the most widely accepted explanation for the evolutionary origin of aging [[12]](https://pubmed.ncbi.nlm.nih.gov/31870250/)[[13]](https://pubmed.ncbi.nlm.nih.gov/30524730/). The theory of antagonistic pleiotropy essentially posits some genetic adaptations can trade long-term health for short-term reproductive success. However, it can be inferred that most genetic adaptations are antagonistically pleiotropic.

Essentially, human DNA tends to degrade over time when it doesn't rightfully need to, as evidenced by the existence of biologically immortal organisms. Human DNA repair is also regulated and gene-specific. Given these facts, the interaction between gene degradation and gene repair is likely to be adaptive. This assigns every gene in our DNA that tends to degrade more with time a single antagonistically pleiotropic trait. Since most DNA in the human genome degrades over time, we can infer that over 50% of genes are antagonistically pleiotropic.

Since adaptations to foods are no more or less genetic than any other adaptations, we can infer that most adaptations to food are also likely to be antagonistic pleiotropic as well. From here we just infer that antagonistic pleiotropy applies more to ancestral foods than it does to novel foods. Which would suggest that the foods to which we are most strongly adapted would tend to be most antagonistically pleiotropic. This increases the likelihood that ancestral foods trade long-term fitness for short-term reproductive success.

For example, perhaps adaptations to sodium and saturated fat consumption, like regulated changes in blood pressure or lipoprotein secretion from the liver, may help to carry people to reproductive age without incident. However, those adaptations might actually increase the risk of poorer health later in life if those dietary exposures persist too long. This is why we cannot assume that natural diets are actually appropriate for maintaining the long-term health of modern humans. These adaptations to diet are likely to be antagonistic pleiotropic.

Without scientific evidence to help inform our attitudes toward the relative health value of novel foods, the health value of those foods remains is a black box. Due to the principle of indifference, we have no particular reason to suspect that truly novel foods will be either beneficial or detrimental. However, if it can be demonstrated that both an ancestral food and a novel food have the same thriving potential during the reproductive years, we can actually infer that the novel food is to be favoured.

Novel foods do not belong to the domain of foods that have the potential to be antagonistic pleiotropic, since we did not evolve consuming them. Conversely, the ancestral food in our scenario is likely to be antagonistically pleiotropic. Thus, the novel food is to be favoured over the ancestral food in this case. This concept can be illustrated clearly with a couple of simple tables.

To summarize, we can infer that ancestral foods are likely to have both positive short-term health value, as well as negative long-term health value. However, in the absence of empirical data, our doxastic attitude toward the health value of novel foods should be one of agnosticism. When the thriving potential of novel foods and ancestral foods show non-inferiority, we infer that the novel foods is less likely to be detrimental for long-term health. Altogether, the argument can be summarized like this:

**Definitions:**

**A** := a human trait (x) is antagonistically pleiotropic

**E** := the human trait (x) is an evolutionary adaptation

**N** := the human trait (x) mediates more than one downstream effect, one of which is negative in the post-reproductive window of the carrier

**D** := human genes succumb to degradation

**M** := human genes must be assumed to be antagonistically pleiotropic

**R** := humans have more genetic adaptations to ancestral foods than novel foods

**C** := antagonistic pleiotropy applies more to ancestral foods than it does to novel foods

**V** := there is a novel food that has non-inferior short-term health value compared to a given ancestral foods

**U** := there is a novel food that is likely to be superior to such an ancestral food in the long-term

**O** := there are unnatural diets that are preferable to ancestral diets.

**x** := human genes

**y** := trait of a human gene

**i** := the interaction between degradation and repair

**P1)** A human trait is antagonistically pleiotropic if and only if the human trait is an evolutionary adaptation and the human trait mediates more than one downstream effect, one of which is negative in the post-reproductive window of the carrier.

**(∀x(Ax↔(Ex∧Nx)))**

**P2)** The interaction between gene degradation and gene repair is an evolutionary adaptation.

**(Ei)**

**P3)** The interaction between gene degradation and gene repair mediates more than one downstream effect, one of which is negative in the post-reproductive window of the carrier.

**(Ni)**

**P4)** If the interaction between gene degradation and gene repair is antagonistically pleiotropic and all human genes succumb to degradation, then all human genes must be assumed to be antagonistically pleiotropic.

**(Ai∧D→M)**

**P5)** All human genes succumb to degradation.

**(D)**

**P6)** If all human genes must be assumed to be antagonistically pleiotropic and humans have more genetic adaptations to ancestral foods than novel foods, then antagonistic pleiotropy applies more to ancestral foods than it does to novel foods.

**(M∧R→C)**

**P7)** Humans have more genetic adaptations to ancestral foods than novel foods.

**(R)**

**P8)** If antagonistic pleiotropy applies more to ancestral foods than it does to novel foods and there is a novel food that has non-inferior short-term health value compared to a given ancestral food, then there is a novel food that is likely to be superior to such an ancestral food in the long-term.

**(C∧V→U)**

**P9)** There is a novel food that has non-inferior short-term health value compared to a given ancestral food.

**(V)**

**P10)** If there is a novel food that is likely superior to such an ancestral food in the long-term, then there are some unnatural diets that are superior to some ancestral diets.

**(U→O)**

**C)** Therefore, there are some unnatural diets that are superior to some ancestral diets.

**(∴O)**

Essentially, once you equalize advantages across a given novel food and a given ancestral food, the inherent disadvantages of antagonistic pleiotropy would leave us favouring the novel food over the ancestral food for long-term health. From here we can infer a priori that a diet that maximizes benefits and minimizes risks for the most amount of people is likely to be a diet that is on some level unnatural or non-ancestral. It is also important to discuss what this position is **not** arguing. It is **not** being argued that every novel food is going to be superior to every ancestral food, and it is **not** being argued that all ancestral foods are bad.

As an aside, one might point out that while ancestral foods like meat seem to increase the risk of many diseases, while other ancestral foods like fruit seem to decrease the risk of many diseases. So what gives? My arguments apply equally to fruit, and adaptations of fruit are also likely to be antagonistically pleiotropic. However, it is likely the case that the antagonistically pleiotropic pathways that are influenced by foods like fruit are less impactful than those influenced by foods like meat.

Diet is about substitutions. Replacing meat with fruit lowers risk, but that doesn't mean that fruit is without long-term harms or risks as well. Bearing this in mind, I posit that if we truly want to maximize the thriving potential of food, we must engineer our own food. Assuming that no diet of natural foods will actually lower risk to zero, if we wanted to improve the health value of food even more, how would we accomplish this without artificially manipulating those foods? In fact, we have evidence that the health value of natural foods can be improved, with examples like Golden rice.

**P1)** If the health value of natural foods can be improved via artificial manipulation, then there are unnatural foods that are preferable to some natural foods.

**(V→M)**

**P2)** The health value of natural foods can be improved via artificial manipulation.

**(V)**

**C)** Therefore, there are some unnatural foods that are preferable to some natural foods.

**(∴M)**

Lastly, it has also been suggested by some Paleo diet advocates that certain hunter-gatherer migrant studies provide us with a justification for why hunter-gatherer diets are appropriate for modern humans. This is due to the fact that these studies demonstrate that when certain hunter-gatherer populations transition from their traditional diets to more Westernized diets, they experience increases in chronic disease risk [[14]](https://pubmed.ncbi.nlm.nih.gov/6937778/)[[15]](https://pubmed.ncbi.nlm.nih.gov/7462380/). However, this is ultimately irrelevant to my point.

The fact that Western diets increase disease risk relative to certain hunter-gatherer diets does not actually lend any credibility to the notion that modern humans can achieve robust health that is equal to that of hunter-gatherers merely by emulating their diets. Perhaps the Western diet is so bad that it would negatively impact the health of any human population who ate it over the long-term. But, it could also be the case that hunter-gatherer diets are still inappropriate for modern humans in many ways— ways that may not be obvious for the reasons I've discussed throughout this article. These are not incompatible concepts.

The most I would have to grant is that a hunter-gatherer diet is likely an improvement over a Western diet. But that is a far cry from ascertaining that a hunter-gatherer diet is optimal for the long-term health of modern humans. This is just a gross overextrapolation from altogether irrelevant data.

The last inference is the icing on the cake, and a bit more convoluted, but it is necessary to argue it to an ancestral diet advocate. This is next argument cuts to the core of their epistemology regarding ancestral diets and health. All we need to do is prime the ancestral diet advocate for the inference by asking them if they identify as "**F**" (as defined below). If they do, then we proceed. If they don't, then their motivations for advocating for ancestral diets diets isn't clear at all.

**Definitions:**

**A** **:**= F(x) would be acting against their values to not be in favour of consuming N(y).

**F(x)** := someone who favours consuming ancestral foods to the exclusion of novel foods because they value reducing disease risk.

**N(y)** := a novel food that reduces disease risk when replacing an ancestral food.

**P1)** If there exists someone who favours consuming ancestral foods to the exclusion of novel foods because they value reducing disease risk, and there exists a novel food that reduces disease risk when replacing an ancestral food, then that person would be acting against their values to not be in favour of consuming that novel food.

**∃xF(x)∧∃yN(y)→∀x∀y(Axy)**

**P2)** There exists someone who favours consuming ancestral foods to the exclusion of novel foods because they value reducing disease risk.

**(∃xF(x))**

**P3)** There exists a novel food that reduces disease risk when replacing an ancestral food.

**(∃yN(y))**

**C)** Therefore, that person would be acting against their values to not be in favour of consuming that novel food.

**(∴∀x∀y(Axy))**

Essentially, if our interlocutor identifies as "**F**", then all we need to do is demonstrate to them that "**N**" exists, and we're essentially home free. If they accept that "**N**" exists and they also identify as "**F**", then they should be in favour of substituting such a novel food for such an ancestral food. If they don't, then they have a contradiction. This is where the ancestral diet advocate could face quite a dilemma.

However, there is a way around this for them, but it's absurd. They can simply reject the evidence for "**N**" existing. Which would be a hilarious move for them to make, but they can make it if they want. However, implicit in this move is the rejection of all evidence that supports "**N**" existing, regardless of the quality.

For example, if they maintain that animal fat consumption is more supportive of health than vegetable fat consumption, they'd need to reject the multiple meta-analyses and meta-regression analyses of randomized controlled trials on the subject, as well as the consistency of effect in seen in high internal validity epidemiology. The implications of taking such a position are hilarious, because they could very easily have to also reject many other diet/lifestyle-disease relationships that they likely take for granted on much weaker evidence. Such as exercise and alcohol consumption affecting cardiovascular disease risk.

It is likely that the vaunted "optimal human diet", which is to say a diet that maximizes long-term health for the greatest number of people, has actually yet to be discovered. Ultimately, to answer the question of what foods are healthy, we need science. We need robust outcome data on modern human beings, not speculation and appeal to nature fallacies. We need this science to teach us how to eat.

**UPDATE:**

Chris Masterjohn has apparently written a [lengthy response](https://chrismasterjohnphd.substack.com/p/ancestral-health-vs-antagonistic) to my position on antagonistic pleiotropy and how it relates to the long-term health value of ancestral foods. Many have asked me to rebut the article, but it's not clear to me why I should. He's not interacting with the argument at all. Let me give you an example to help illustrate my feelings in this matter. If I give someone an argument, and their response is to turn around and scream at a wall, should I feel any sort of drive to "rebut" the screaming? I don't think so.

Constructing a lengthy reply to Masterjohn's article would only serve to give readers the impression that he actually said anything of substance against my position, which he hasn't. When someone tells me that they disagree with an argument that I have presented, I take this to mean one of two things. Either they have an argument of their own that forms a conclusion that is the negation of at least one of my premises, or they give at least one of my premises such low credence that they just deny that it's true. Masterjohn didn't do either in his article.

Masterjohn just spends the article giving low credence to concepts that aren't even entailed from the argument itself, so why should I care? It's doubly hilarious to expect me to rebut his article when he actually signed off on the entailments of my position at least twice in our [debate](https://chrismasterjohnphd.substack.com/p/the-ancestral-health-debate) when he was actually interacting with the argument itself and not going off on tangents. Once at [58:26](https://youtu.be/n1I5xgvERbo?t=3506) and again at [1:39:16](https://youtu.be/n1I5xgvERbo?t=5956). Not sure what more there is to comment on here.

Thank you for reading! If you like what you've read and want help support my content, consider pledging to my [Patreon](https://www.patreon.com/thenutrivore). Every little bit helps! I hope you found the content interesting!

**References:**

[1] Anthony A. Volka and Jeremy A. Atkinson. Infant and child death in the human environment of evolutionary adaptation. Evolution and Human Behavior. Volume 34, Issue 3, May 2013, Pages 182-192. [https://www.sciencedirect.com/science/article/abs/pii/S1090513812001237](https://www.sciencedirect.com/science/article/abs/pii/S1090513812001237)

[2] Lee Hooper, et al. Reduction in saturated fat intake for cardiovascular disease. Cochrane Database Syst Rev. 2020 May. [https://pubmed.ncbi.nlm.nih.gov/32428300/](https://pubmed.ncbi.nlm.nih.gov/32428300/)

[3] Arash Etemadi, et al. Mortality from different causes associated with meat, heme iron, nitrates, and nitrites in the NIH-AARP Diet and Health Study: population based cohort study. BMJ. 2017 May 9. [https://pubmed.ncbi.nlm.nih.gov/28487287/](https://pubmed.ncbi.nlm.nih.gov/28487287/)

[4] Andrew Mente, et al. Associations of urinary sodium excretion with cardiovascular events in individuals with and without hypertension: a pooled analysis of data from four studies. Lancet. 2016 Jul 30. [https://pubmed.ncbi.nlm.nih.gov/27216139/](https://pubmed.ncbi.nlm.nih.gov/27216139/)

[5] Rik H G Olde Engberink, et al. Use of a Single Baseline Versus Multiyear 24-Hour Urine Collection for Estimation of Long-Term Sodium Intake and Associated Cardiovascular and Renal Risk. Circulation. 2017 Sep 5. [https://pubmed.ncbi.nlm.nih.gov/28655835/](https://pubmed.ncbi.nlm.nih.gov/28655835/)

[6] Thomas Flatt and Linda Partridge. Horizons in the evolution of aging. BMC Biol. 2018 Aug 20.

[https://pubmed.ncbi.nlm.nih.gov/30124168/](https://pubmed.ncbi.nlm.nih.gov/30124168/)

[7] Ryan McCready. For Olympic Athletes, Is 30 the New 20? July 2016. [https://venngage.com/blog/olympics/](https://venngage.com/blog/olympics/)

[8] Hillard Kaplan, et al. Coronary atherosclerosis in indigenous South American Tsimane: a cross-sectional cohort study. Lancet. 2017 Apr 29. [https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(17)30752-3/fulltext](https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(17)30752-3/fulltext)

[9] Mahesh V Madhavan, et al. Coronary artery calcification: pathogenesis and prognostic implications. J Am Coll Cardiol. 2014 May. [https://pubmed.ncbi.nlm.nih.gov/24530667/](https://pubmed.ncbi.nlm.nih.gov/24530667/)

[10] Michael Gurven. Infant and fetal mortality among a high fertility and mortality population in the Bolivian Amazon. Soc Sci Med. 2012 Dec. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3502712/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3502712/)

[11] Horvath et al. An epigenetic clock analysis of race/ethnicity, sex, and coronary heart disease. Genome Biol. 2016 Aug. [https://pubmed.ncbi.nlm.nih.gov/27511193/](https://pubmed.ncbi.nlm.nih.gov/27511193/)

[12] J Mitteldorf. What Is Antagonistic Pleiotropy? Biochemistry (Mosc). 2019 Dec. [https://pubmed.ncbi.nlm.nih.gov/31870250/](https://pubmed.ncbi.nlm.nih.gov/31870250/)

[13] He and Zhang. Toward a molecular understanding of pleiotropy. Genetics. 2006 Aug. [https://pubmed.ncbi.nlm.nih.gov/16702416/](https://pubmed.ncbi.nlm.nih.gov/16702416/)

[14] J M Stanhope and I A Prior. The Tokelau island migrant study: prevalence and incidence of diabetes mellitus. N Z Med J. 1980 Dec. [https://pubmed.ncbi.nlm.nih.gov/6937778/](https://pubmed.ncbi.nlm.nih.gov/6937778/)

[15] J M Stanhope and I A Prior. The Tokelau Island Migrant Study: serum lipid concentration in two environments. J Chronic Dis. 1981. [https://pubmed.ncbi.nlm.nih.gov/7462380/](https://pubmed.ncbi.nlm.nih.gov/7462380/)