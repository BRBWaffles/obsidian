Recently, a controversial paper reviewing both the health and environmental benefits of animal foods was published to the journal, Animal [[1](https://pubmed.ncbi.nlm.nih.gov/35158307/)]. This was brought to my attention by one of my patrons, who specifically requested that I respond to the first section of the article, titled "why the nutritional case against animal source foods may be overstated". My response was originally supposed to be a reaction video for my patrons. But, the more of the paper that I read, the more I realized that this would require a much more systematic appraisal.

Although I have only reviewed one section of the paper, I am told that the rest of the paper is also filled with questionable claims, falsehoods, half-truths, and dubious reasoning. As such, I will leave it to people with more domain knowledge to comment on the latter sections of the paper. As for the section that I was tasked with critiquing, I'm completely comfortable with systematically evaluating the claims contained within it. So, let's get into it.

> "Even though advocacy for moderate to heavy restriction [of animal foods] is echoed by various public heath institutions worldwide, suggesting apparent consensus, the scientific debate is not settled as the evidence has been challenged by various scientists, both for red meat (Truswell, 2009; Hite et al., 20100; Alexander et al., 2015; Klurfeld, 2015; Kruger & Zhou, 2018; Händel et al., 2020; Hill et al., 2020; Johnston et al., 2019; Leroy and Cofnas, 2020; Sholl et al., 2021)"

**Weasel words:**

Citing a couple of authors to represent the "public heath institutions" that advocate for animal food restriction, whilst also citing ten authors who challenge this notion, is a painfully misleading move. The evidence for reducing most animal foods is actually extensive, with only a small handful of exceptions. Not only that, but it's also questionable whether or not any public health institutions actually advocate for the moderate or heavy restriction of these exceptions. It's unclear, because the terms "moderate" and "heavy" are not clearly defined.

**Motte and Bailey:**

The paragraph began by referencing the presumed attitudes of "public health institutions" toward animal foods more broadly. A truly extraordinary claim that is not supposed by any of the their reference material. They then follow it up by citing authors who have pushed back specifically on these institutions' attitudes toward red meat and saturated fat. Most public health institutions that are concerned with nutrition do indeed advocate for the restriction of these foods.

> "...and saturated fat, which is not exclusive to animal source foods (Astrup et al., 2020; Krauss & Kris Etherton, 2020)."

**Red herring:**

While there are some plant foods that are high in saturated fat, we need not consume them. Those sources of saturated fat can be avoided on an animal food restricted diet. This is less true of the animal foods that these authors are specifically attempting to defend. Not only that, but no public health institutions are advocating for the consumption of plant foods like coconut or palm oil. So, I'm not even sure what this point is meant to address.

> "Among other concerns, one of the objections is that pleas for restriction are based on conflicting findings and observational relationships that are not necessarily causal, suffering from confounding and bias (Grosso et al., 2017; Händel et al., 2020; Hill et al., 2020; Leroy & Barnard, 2020; Nordhagen et al., 2020)."

**Potential contradiction:**

Confounding is a causal concept. All posited confounders need to be validated as genuine confounders using evidence that meets these authors' bar for causal inference. Otherwise we don't have a reason to consider them confounders at all. The truth is that virtually all accepted confounders in this domain are validated via epidemiological data itself. I can formalize the argument like this:

**P1)** If evidence that is stronger than the best animal food epidemiology is required to demonstrate causality and confounding is a causal concept, then evidence that is stronger than the best animal food epidemiology is required to validate potential confounders.

**(E∧C→P)**

**P2)** Evidence that is stronger than the best animal food epidemiology is required to demonstrate causality.

**(E)**

**P3)** Confounding is a causal concept.

**(C)**

**C)** Therefore, evidence that is stronger than the best animal food epidemiology is required to validate potential confounders.

**(∴P)**

In the above syllogism, the authors could very easily be affirming E and ¬E. If the authors are rejecting epidemiological evidence while simultaneously positing confounders that will be validated with epidemiological evidence, their position would entail a contradiction.

> "Unwarranted use of causal language is nonetheless widespread in the interpretation of nutritional epidemiological data, thereby posing a systemic problem and undermining the field’s credibility (Cofield et al., 2010; Ioannidis, 2018)."

**Red herring:**

Firstly, there is no reference for the claim that causal language is widespread in the interpretation of nutritional epidemiological data. Secondly, even if it were true, nutritional epidemiology has excellent validation for both its results as well as the underpinning methodology, considering its limitations and shortcomings.

If the goalpost for causal inference is human experiments, then the use of causal language is likely to be warranted more often than it is unwarranted. This is due to the high degree of concordance between the results of nutritional epidemiology and randomized controlled trials [[2](https://pubmed.ncbi.nlm.nih.gov/34526355/)][[3](https://pubmed.ncbi.nlm.nih.gov/23700648/)][[4](https://pubmed.ncbi.nlm.nih.gov/34308960/)]. Regardless, it's not clear how the widespread use of causal language poses either a systemic problem or undermines the field's credibility. Again, it just isn't clear what the point of these claims is supposed to be.

> "Moreover, the associations between red meat and metabolic disease have not only been evaluated as weak..."

**Red herring:**

It is not true that these associations are weak. When meta-analytically summated the associations are often close to linear. Not only for total mortality but also for many major chronic diseases [[5](https://pubmed.ncbi.nlm.nih.gov/28446499/)][[6](https://pubmed.ncbi.nlm.nih.gov/29039970/)][[7](https://pubmed.ncbi.nlm.nih.gov/28397016/)]. The associations generally only appear weak when there are certain sources of bias present, such as inadequate exposure contrasts, follow-up times, participant numbers, or even overadjustment for mediators.

> "...translating into small absolute risks based on low to very low certainty evidence (Johnston et al., 2019)..."

**Red herring:**

Absolute risk and certainty are concepts that don't strongly interact. You can have low absolute risks and a high degree of certainty, such as with successful human trials with event-based stopping conditions. You can also have high absolute risks and low certainty, such as with underpowered human studies with high event rates.

It's also not clear why the authors would choose to favour absolute risk over relative risk, considering that the maximum possible absolute risk differences are going to be dictated by event rates in the comparator population. This concept can be easily illustrated referring to populations that have longer follow-up times, such as with the literature on LDL and cardiovascular disease risk [[8](https://pubmed.ncbi.nlm.nih.gov/30571575/)].

The distance between the two green lines represents the absolute risk difference between low and high LDL over 10 years. Whereas the distance between the two red lines represents the absolute risk difference between low and high LDL over 30 years. Here we can see that if insufficient follow-up is observed, absolute risk differences will inevitably be smaller. This is because event rates naturally increase with time.

The meta-analysis by Vernooij et al. (2019) that the authors cited to support the claim of "small" absolute risk differences with red meat had a median follow-up time of 10.5 years for cardiovascular disease [[9](https://pubmed.ncbi.nlm.nih.gov/31569217/)]. Despite this limitation, Vernooij et al. did not appear to have made any substantive attempt to explore the heterogeneity between their included studies with subgroup analyses or meta-regression analyses. Had they done so, they could have subgrouped by follow-up time and inevitably found that the absolute risks were higher in cohorts with longer follow-ups.

But we can take it a step further. We can reveal the inconsistency in their reasoning by formalizing their argument against nutritional epidemiology, and showing that their criticisms apply to, say, human experiments as well.

**Definitions:**

V := human studies are vulnerable to bias and small effect sizes

D := human studies can demonstrate causality or support causal inference

G := are garbage

e := human experiments

**P1)** If human studies are vulnerable to bias and small effect sizes, then human studies cannot demonstrate causality or support causal inference.

**(∀x(Vx→¬Dx))**

**P2)** Human studies are vulnerable to bias and small effect sizes.

**(∀x(Vx))**

**P3)** If human experiments are human studies that cannot demonstrate causality or support causal inference, then human experiments are human studies that are garbage.

**(¬De→Ge)**

**C)** Therefore, human experiments are human studies that are garbage.

**(∴Ge)**

Using the authors' own logic, we can show that they would have to dismiss human experimental evidence on the same basis. This is because human experimental evidence is vulnerable to the same types of limitations that the authors are positing as presumably invalidating for nutritional epidemiology.

> "Associations are particularly noticeable in North America, where meat is often consumed through a fast-food window and where high-meat consumers tend to also eat less healthy diets and follow less healthy lifestyles in general. In a Canadian study, eating more meat was only associated with more all-cause cancer incidence for the subpopulation eating the lowest amounts of fruits and vegetables (Maximova et al., 2020)."

**Potential contradiction:**

Again, the authors appear to be dismissing nutritional epidemiological evidence on the basis of confounding, without justifying their asymmetrical attitudes toward the supporting evidence for the confounders they're positing. What is true of the evidence between fruits and vegetables and cancer and the evidence between meat and cancer, such that we can infer causality for one and not the other?

**Equivocation:**

This is the second time the authors have shifted the goalpost regarding what types of animals foods that are in question. First they were discussing animal foods simpliciter, only to shift the goalpost to red meat. Now they were talking about red meat, and have shifted goalposts to meat as a broad category. Whether this is intentional or just the result of sloppy writing, it is not a good look for the authors.

> "Several large-scale population-based studies, performed in individuals with ‘healthy lifestyles’, such as the Oxford-EPIC Study (Key et al., 2003) and the 45-and-Up Study (Mihrshahi et al., 2017), also find that the negative effects of red meat consumption on all-cause mortality become benign."

**Red Herring:**

From the wider literature, the typical threshold for harm with meat is at approximately 100g/day on average [[5](https://pubmed.ncbi.nlm.nih.gov/28446499/)].

The Oxford-EPIC cohort lacks power in those ranges [[10](https://pubmed.ncbi.nlm.nih.gov/23497300/)]. Data on the exposure contrasts in the 45-and-Up Study are even more unpersuasive [[11](https://pubmed.ncbi.nlm.nih.gov/28040519/)]. We have far more robust data than this, with better internal validity, follow-up times, and measurements.

In this Japanese cohort with a follow-up of 14 years, diet and lifestyle covariates were largely balanced across the quantiles of red meat intake [[12](https://pubmed.ncbi.nlm.nih.gov/33320898/)]. In fact, many covariates we'd suspect to be detrimental actually favoured meat consumption. Despite this, total meat was still associated with a statistically significant 21% increase in all-cause mortality among men between the ages of 65 and 79 years old, and a borderline-significant 41% increase in all-cause mortality risk among women between the ages of 45 and 54 years old.

These results could indicate that meat consumption is more likely to lead to premature death due to an unmeasured cause earlier in life than with men. Perhaps the seemingly premature increase in mortality could plausibly be attributed to an unmeasured female-specific endpoint, such as breast cancer [[13](https://pubmed.ncbi.nlm.nih.gov/31389007/)].

Again, just to hammer the point home, we can actually defeat the authors' position with a simple modus tollens.

**P1)** If meat consumption is likely to decrease mortality risk, then those consuming the most meat do not die more than those consuming the least meat.

**(P→¬L)**

**P2)** Those consuming the most meat do die more than those consuming the least meat.

**(L)**

**C)** Therefore, meat consumption is not likely to decrease mortality risk.

**(∴¬P)**

Given the weight and strength of the evidence in favour of meat restriction for longevity, it would be quite hilarious to see the authors attempt to reject P2. The evidence they referenced from the Oxford-EPIC cohrot and the 45-and-Up Study could be used in an attempt to reject P2. However, that evidence is very easily superseded by higher internal validity evidence with greater power, not to mention in populations that don't suffer from the same supposed confounding.

> "If red meat were indeed causally driving the associations, one would anticipate finding stronger effects in systematic reviews looking specifically at red meat intake (able to evaluate a large intake gradient) compared to dietary pattern studies (smaller intake gradient) (Johnston et al., 2018)."

**Potential contradiction:**

The association between all-cause mortality and red meat consumption is stronger than the inverse association between all-cause mortality and fruit and vegetable consumption in the most well-done systematic reviews [[5](https://pubmed.ncbi.nlm.nih.gov/28446499/)]. Again, what is true of the association between fruits and vegetables and all-cause mortality and red meat and all-cause mortality, such that we can infer causality for one and not the other?

> "On the contrary, the absolute risk reductions from both reviews specific to intake versus dietary pattern (Johnston et al., 2019) were very similar in their magnitude of effect, indicating the possibility that, even after adjustment, a multitude of other diet or lifestyle components may be confounding the associations irrespective of whether they are negative or positive (Zeraatkar & Johnston, 2019)."

**Red herring:**

This literally just doesn't make any sense. Similar effect sizes are not indicators of multicollinearity or interaction between exposures. I have no idea how the authors come to this conclusion. If I punch people in the face on Mondays and kick people in the balls on Wednesdays, the risk of injury is equal between both Mondays and Wednesdays, but Mondays and Wednesdays aren't the same thing. Just as dietary patterns and individual foods aren't the same thing.

Just because the contribution of meat and diet/lifestyle factors have similar magnitudes of effect doesn't mean a mutual adjustment would do anything to either effect. Both exposures could be interacting with the outcome without interacting with each other. This is easily one of the most bizarre claims in the entire paper. They're also comparing effect sizes between analyses investigating different populations. It's just unfounded speculation.

> "While such troubling incongruity can be partially ascribed to differences in methodological set-up between studies, it has been hypothesised that the associations found in the West could at least partially be seen as cultural constructs generated by responses to norms of eating right (Hite, 2018)."

**Red herring:**

Again, these associations are seen in populations that are not consuming Westernized diets [[12](https://pubmed.ncbi.nlm.nih.gov/33320898/)]. I'm not entirely sure why the authors seem to believe that these associations are limited to Western populations eating Western diets with Western attitudes toward health.

> "An important question to consider, therefore, is "whether intake of animal and plant proteins is a marker of overall dietary patterns or of social class" (Naghshi et al., 2020). Upper-middle classes, who are particularly sensitive to the ideologies of eating virtuous, tend to eat less red meat and saturated fat because of what they symbolise, and because of what they are being told by authorities and moralising societal discourse (Leroy & Hite, 2020). However, those same people are also more educated, wealthier, and healthier in general (Leroy & Cofnas, 2020)."

**Equivocation:**

Yet again, the authors have shifted their goalpost. They went from animal foods to red meat, from red meat to meat, and now from meat to animal protein. Again, it's unclear if this is intentional or just really atrocious writing on the part of the authors. But, I will attempt to keep my rebuttals relevant to the authors' most recently stated goalpost.

**Bullshit:**

These associations are seen even when socioeconomic status are largely balanced across the quantiles of animal protein intake [[14](https://pubmed.ncbi.nlm.nih.gov/33624505/)]. In fact, in this analysis of the Women's Health Initiative Observational Study by Sun et al. (2021), those who consumed the most plant protein were typically in the lower socioeconomic strata. This is in direct opposition to their speculation about socioeconomic status confounding. Additionally, replacing animal protein with plant protein associates with a reduced risk of all-cause mortality even in populations that are situated in a higher socioeconomic stratum [[15](https://pubmed.ncbi.nlm.nih.gov/27479196/)].

> "Even if multivariable models are used to account for such confounding effects as smoking, alcohol consumption, or obesity, it may not be possible to disentangle the effects of all dietary and lifestyle factors involved, especially given the low certainty of evidence."

**Potential contradiction:**

Yet again, we find ourselves needing to ask the authors what is true of the evidence between smoking/alcohol/obesity and health outcomes and animal foods/red meat/meat/animal protein and health outcomes such that we can infer causality for one and not the other? Thus far, the authors have not divulged any clear answers to this question in their paper.

> "Therefore, WHO (2015) mentions that eating unprocessed red meat "has not yet been established as a cause of cancer” (emphasis added)..."

**Appeal to authority:**

Causal inference is an epistemic question, informed and largely adjudicated by statistics. It's rather interesting that the authors tend to offer next to no critical appraisal of methodology or interpretation when the results concord with their (obvious) biases. So far any evidence against animal food consumption as been scrutinized extensively, albeit fallaciously, but the same attempt at rigour is not extended to the counterevidence.

> "...while IARC (2015) stated that "chance, bias, and confounding could not be ruled out” with respect to the association between red meat intake and colorectal cancer. According to some (e.g., Hite, 2018), nutritional epidemiology of chronic disease is thus at risk of capturing cultural artefacts and health beliefs within observational relationships, rather than reliably quantifying actual health effects. Such observations are then used to reinforce dietary advice, potentially creating a positive feedback loop (Leroy & Hite, 2020)."

**Red herring:**

This is true of any association, as per the Duhem-Quine thesis [[16](https://en.wikipedia.org/wiki/Duhem%E2%80%93Quine_thesis)]. Causal inference is a separate consideration, and the fact that auxiliary hypotheses can be proposed is tangential. The authors imply that the ability to appeal to these auxiliary hypotheses presents a barrier to reliably quantifying actual health effects. What type of evidence do they propose needs to be used, then? Because no scientific evidence is free from this limitation.

> "This problem is further underlined by the lack of support from intervention trials (O’Connor et al., 2017; Turner & Lloyd, 2017; Leroy & Cofnas, 2020), which are designed to account for known and unknown confounders, and the fact that the mechanistic rationale for red meats remains speculative at best (Delgado et al., 2020; Leroy & Barnard, 2020)."

**Equivocation:**

The authors' references don't support the claim. Until this point they were discussing the impact of meat products on disease outcomes, not disease risk markers or biochemical mechanisms. However, one of the only studies that did attempt to replace animal foods in the diet also showed one of the largest effect sizes in reducing the risk of acute myocardial infarction [[17](https://pubmed.ncbi.nlm.nih.gov/7911176/)]. On top of that, the most well-controlled human mechanistic studies also support the inference that meat increases CVD risk factors [[18](https://pubmed.ncbi.nlm.nih.gov/31161217/)].

> "Taken together, various public health organisations make a case for the reduction of animal source foods based on their interpretation of the prevailing scientific evidence. Others, however, argue that conclusive proof for (some of) these recommendations is missing, particularly given the contribution of animal source foods to closing essential micronutrient gaps (Leroy & Barnard, 2020)."

**Potential contradiction:**

The authors need to define "conclusive proof", and demonstrate how it has been shown for all variables that they are positing as confounding. However, it's beginning to sound as though they're getting close to planting their goalpost at human experimental evidence. However, this would be a mistake, as they've already posited a number of confounders for which we have no human experimental evidence for causal interaction with the outcomes that have been discussed.

> "Arguing for strong reductions contradicts common-sense approaches, especially from an anthropological perspective (Gupta, 2016; Leroy et al., 2020a). Meat, marrow, and seafood are evolutionary components of the human diet, even if they may have displayed some nutritional and biochemical differences compared to what is produced today in intensified operations, e.g., with respect to fat composition (Kuipers et al., 2010; Manzano-Baena & Salguero-Herrera 2018) and the presence of phytochemicals (van Vliet et al., 2021a, and 2021b). The health impact of these differences may be significant but remains difficult to quantify, though polyunsaturated fatty acids/saturated fatty acids and omega 3/6 ratios of wild ruminants living in current times are similar to pasture-raised (grass-fed) beef, but dissimilar to grain-fed beef (Cordain et al., 2002b). Be that as it may, the abundant consumption of animal source foods over 2.5 million years has resulted in an adapted human anatomy, metabolism, and cognitive capacity that is divergent from other apes (Milton, 2003; Mann, 2018). Also, many hunter-gatherer populations consume far larger amounts of meat and other animal source foods (sometimes > 300 kg/p/y), than what is now consumed in the West (around 100 kg/p/y). This is likely still much below what was once valid for early humans preying on megafauna (Ben-Dor & Barkai, 2020). On a caloric basis, the animal:plant ratio of Western diets (about 1:2 in the US; Rehkamp, 2016) is the inverse of most pre-agricultural diets (mean of 2:1; Cordain et al., 2000). Such high amounts of animal source foods are not necessarily indicative of a health advantage, but it can be assumed that animal source foods are at least compatible with good health."

**Equivocation:**

Apparently we've gone from talking about animal foods to talking about red meat, from talking about red meat to talking about meat, from talking about meat to talking about animal protein, and now from talking about animal protein to to talking about meat, marrow, and seafood. This is truly astonishing. Especially considering that now they're including seafood, which no major public health institution recommends that we eschew.

**Appeal to nature:**

Just because meat is an integral part of our evolutionary history does not actually mean that it is necessarily beneficial for the long-term health of modern humans. In fact, there are valid reasons to suspect that foods to which we are most strongly adapted may actually be more detrimental for long-term health, via antagonistic pleiotropy. I discuss this in a previous blog article [[19](https://www.the-nutrivore.com/post/should-we-eat-like-hunter-gatherers)].

Appeal to nature fallacies basically affirm that because something is natural (or in this case, "evolutionary"), it then follows that it is good. However, taking this position leads to hilarious consequences. Let me demonstrate by formalizing the authors' position once more.

**P1)** If the behaviour is evolutionary, then the behaviour is good.

**(∀x(Ex→Ox))**

**P2)** Meat consumption is a behaviour that is evolutionary.

**(Ea)**

**C)** Therefore, meat consumption is a behaviour that is good.

**(∴Oa)**

It should be obvious straight away why this is problematic. There are plenty of things that are natural or "evolutionary" that we also consider to be undesirable, and we can illustrate that with a reductio ad absurdum.

**P1)** If the behaviour is evolutionary, then the behaviour is good.

**(∀x(Ex→Ox))**

**P2)** Murder is a behaviour that is evolutionary.

**(Em)**

**C)** Therefore, murder is a behaviour that is good.

**(∴Om)**

If animal foods are good by virtue of them being natural or "evolutionary", the authors will have to explain to me why something like murder is not good. As they share the same property of being evolutionary.

> "So-called "diseases of modernity" were rare in ancestral communities, in contrast to what is now seen in regions where Western diets rich in energy-dense foods and (sedentary) lifestyles prevail. In the US, 71% of packaged foods are ultraprocessed (Baldridge et al., 2019)..."

**Red herring:**

There are a number of epistemic barriers that challenge inferences about the long-term health value of more primitive living conditions for modern humans, such as survivorship bias [[20](https://pubmed.ncbi.nlm.nih.gov/25489027/)]. Primitive cultures tend to have very high rates of infant and child mortality, which modern medicine can rescue. When those children are _not_ saved, the population will appear more robust by weeding out less resilient people. When those children _are_ saved, you increase the number of less resilient people within the population.

> "Even if this has been described as a "paradox” (Cordain et al., 2002a), it mainly indicates that today’s assumptions about healthy diets, as being de facto low in red meat and saturated fat, are flawed and represent a romanticised Western viewpoint."

**Strawman:**

No public health institutions are suggesting that healthy diets are _defined_ by the absence of red meat and saturated fat. Rather, diets lower in red meat and saturated fat tend to be healthier than diets that are higher in red meat and saturated fat. But, this doesn't mean that other factors don't also matter. These dietary patterns have many other characteristics that contribute to healthfulness that have nothing to do with red meat or saturated fat.

> "To sum up, although animal source foods are primary components of the Western diet, they are also evolutionary foods to which the human body is anatomically and metabolically adapted, up to the level of the microbiome (Sholl et al., 2021), and has always obtained key nutrients from."

**Appeal to nature:**

The status of red meat as an evolutionary food is tangential to the question of whether or not red meat increases long-term disease risk in modern populations. Investigations into the health status of primitive cultures is insufficient to inform this question.

> "Although further research may be needed, their role in chronic diseases could as well be a mere artefact based on association with the actual damage from other dietary and lifestyle factors. It is uncertain yet possible that high intake of red meat could become problematic in a contemporary Western context."

**Red herring:**

Again, this is true of any association. Causal inference is a separate consideration, and the fact that auxiliary hypotheses can be proposed is, again, tangential.

**Potential contradiction:**

For the last time, posited confounders require validation that meets the authors' bar for causal inference. Thus far, no such bar has been provided and no validation was offered for any of the confounders that were posited. That which is stated without evidence can be dismissed without evidence. Anyone can baselessly speculate.

> "Moreover, contemporary cultures that have maintained traditional diets and lifestyles typically have low burdens of chronic disease (e.g., Kaplan et al., 2017)."

**Red herring:**

The authors reference a cross-sectional analysis of the Tsimane population conducted by Kaplan et al. (2017) [[21](https://pubmed.ncbi.nlm.nih.gov/28320601/)]. It's questionable whether or not their results qualify as low burdens of chronic disease for that population in the first place.

This is made even more questionable after accounting for ~15-year age overestimations that were likely to have biased their results [[22](https://immunityageing.biomedcentral.com/articles/10.1186/s12979-019-0165-8)][[23](https://pubmed.ncbi.nlm.nih.gov/27511193/)][[24](https://pubmed.ncbi.nlm.nih.gov/34038540/)]. After this adjustment, the cardiovascular disease burden within the Tsimane is likely largely comparable with the results of the MESA cohort.

Here on the chart above we see Tsimane age estimates using DNA methylation on the Y axis, against Tsimane age estimates using the methods of Kaplan et al. on the X axis. Kaplan et al. (2017) estimated the ages of the Tsimane participants using written records, relative age lists, dated events, photo comparisons of people with known ages, and cross-validation of information from independent interviews of kin.

Apparently such methodology would appear to introduce a fair amount of bias, as the more objective measures of age tend not to agree with them. Furthermore, all of these more robust measures of age seem to point to overestimations on the party of Kaplan et al. that are all roughly in the same ballpark of 10-20 years.

**Equivocation:**

The category of "chronic disease" is a superset, including many individual diseases. The only disease endpoint investigated in the authors' reference was cardiovascular disease progression (measured by coronary artery calcification). So, I'm not sure why they feel justified in referring to chronic disease as a broad category with this single reference.

To wrap this up, I'd just like to say that I've never before seen a peer-reviewed publication that was so densely packed with logical fallacies and inconsistencies. Mind you this is only the first section, related to disease risk. I was only responsible for appraising this section, but from what I've been told about the remainder of the paper it could potentially be even more absurd. Which is scary to me.

Altogether the authors were guilty of eleven red herrings, six potential contradictions, five equivocations, and eight other assorted fallacies. From what I've read, no truly persuasive arguments were offered in favour of their view, and their attempts to criticize the prevailing paradigm were uniformly hollow and superficial.

Ultimately, the authors actually describe the absurdity of their approach better than I could in the introduction of their paper. Truly astonishing.

> _"Due to constraints in format, we restrict ourselves to generating a perspective that favours concepts over details and methodological data."_

Thanks for reading! If you enjoy my writing and want more content like this, consider pledging to my [Patreon](https://www.patreon.com/thenutrivore)!

**References:**

[1] Leroy, Frédéric, et al. ‘Animal Board Invited Review: Animal Source Foods in Healthy, Sustainable, and Ethical Diets - An Argument against Drastic Limitation of Livestock in the Food System’. _Animal: An International Journal of Animal Bioscience_, vol. 16, no. 3, Mar. 2022, p. 100457. _PubMed_, [https://doi.org/10.1016/j.animal.2022.100457](https://doi.org/10.1016/j.animal.2022.100457.)

[2] Schwingshackl, Lukas, et al. ‘Evaluating Agreement between Bodies of Evidence from Randomised Controlled Trials and Cohort Studies in Nutrition Research: Meta-Epidemiological Study’. _BMJ (Clinical Research Ed.)_, vol. 374, Sept. 2021, p. n1864. _PubMed_, [https://doi.org/10.1136/bmj.n1864](https://doi.org/10.1136/bmj.n1864)

[3] Moorthy, Denish, et al. _Concordance Between the Findings of Epidemiological Studies and Randomized Trials in Nutrition: An Empirical Evaluation and Citation Analysis: Nutritional Research Series, Vol. 6_. Agency for Healthcare Research and Quality (US), 2013. _PubMed_, [http://www.ncbi.nlm.nih.gov/books/NBK138246/](http://www.ncbi.nlm.nih.gov/books/NBK138246/)

[4] Beyerbach, Jessica, et al. ‘Evaluating Concordance of Bodies of Evidence from Randomized Controlled Trials, Dietary Intake, and Biomarkers of Intake in Cohort Studies: A Meta-Epidemiological Study’. _Advances in Nutrition (Bethesda, Md.)_, vol. 13, no. 1, Feb. 2022, pp. 48–65. _PubMed_, [https://doi.org/10.1093/advances/nmab095](https://doi.org/10.1093/advances/nmab095)

[5] Schwingshackl, Lukas, et al. ‘Food Groups and Risk of All-Cause Mortality: A Systematic Review and Meta-Analysis of Prospective Studies’. _The American Journal of Clinical Nutrition_, vol. 105, no. 6, June 2017, pp. 1462–73. _PubMed_, [https://doi.org/10.3945/ajcn.117.153148](https://doi.org/10.3945/ajcn.117.153148)

[6] Bechthold, Angela, et al. ‘Food Groups and Risk of Coronary Heart Disease, Stroke and Heart Failure: A Systematic Review and Dose-Response Meta-Analysis of Prospective Studies’. _Critical Reviews in Food Science and Nutrition_, vol. 59, no. 7, 2019, pp. 1071–90. _PubMed_, [https://doi.org/10.1080/10408398.2017.1392288](https://doi.org/10.1080/10408398.2017.1392288.)

[7] Schwingshackl, Lukas, et al. ‘Food Groups and Risk of Type 2 Diabetes Mellitus: A Systematic Review and Meta-Analysis of Prospective Studies’. _European Journal of Epidemiology_, vol. 32, no. 5, May 2017, pp. 363–75. _PubMed_, [https://doi.org/10.1007/s10654-017-0246-y](https://doi.org/10.1007/s10654-017-0246-y)

[8] Abdullah, Shuaib M., et al. ‘Long-Term Association of Low-Density Lipoprotein Cholesterol With Cardiovascular Mortality in Individuals at Low 10-Year Risk of Atherosclerotic Cardiovascular Disease’. _Circulation_, vol. 138, no. 21, Nov. 2018, pp. 2315–25. _PubMed_, [https://doi.org/10.1161/CIRCULATIONAHA.118.034273](https://doi.org/10.1161/CIRCULATIONAHA.118.034273)

[9] Vernooij, Robin W. M., et al. ‘Patterns of Red and Processed Meat Consumption and Risk for Cardiometabolic and Cancer Outcomes: A Systematic Review and Meta-Analysis of Cohort Studies’. _Annals of Internal Medicine_, vol. 171, no. 10, Nov. 2019, pp. 732–41. _PubMed_, [https://doi.org/10.7326/M19-1583](https://doi.org/10.7326/M19-1583)

[10] Rohrmann, Sabine, et al. ‘Meat Consumption and Mortality--Results from the European Prospective Investigation into Cancer and Nutrition’. _BMC Medicine_, vol. 11, Mar. 2013, p. 63. _PubMed_, [https://doi.org/10.1186/1741-7015-11-63](https://doi.org/10.1186/1741-7015-11-63)

[11] Mihrshahi, Seema, et al. ‘Vegetarian Diet and All-Cause Mortality: Evidence from a Large Population-Based Australian Cohort - the 45 and Up Study’. _Preventive Medicine_, vol. 97, Apr. 2017, pp. 1–7. _PubMed_, [https://doi.org/10.1016/j.ypmed.2016.12.044](https://doi.org/10.1016/j.ypmed.2016.12.044)

[12] Saito, Eiko, et al. ‘Association between Meat Intake and Mortality Due to All-Cause and Major Causes of Death in a Japanese Population’. _PloS One_, vol. 15, no. 12, 2020, p. e0244007. _PubMed_, [https://doi.org/10.1371/journal.pone.0244007](https://doi.org/10.1371/journal.pone.0244007)

[13] Lo, Jamie J., et al. ‘Association between Meat Consumption and Risk of Breast Cancer: Findings from the Sister Study’. _International Journal of Cancer_, vol. 146, no. 8, Apr. 2020, pp. 2156–65. _PubMed_, [https://doi.org/10.1002/ijc.32547](https://doi.org/10.1002/ijc.32547)

[14] Sun, Yangbo, et al. ‘Association of Major Dietary Protein Sources With All-Cause and Cause-Specific Mortality: Prospective Cohort Study’. _Journal of the American Heart Association_, vol. 10, no. 5, Feb. 2021, p. e015553. _PubMed_, [https://doi.org/10.1161/JAHA.119.015553](https://doi.org/10.1161/JAHA.119.015553)

[15] Song, Mingyang, et al. ‘Association of Animal and Plant Protein Intake With All-Cause and Cause-Specific Mortality’. _JAMA Internal Medicine_, vol. 176, no. 10, Oct. 2016, pp. 1453–63. _PubMed_, [https://doi.org/10.1001/jamainternmed.2016.4182](https://doi.org/10.1001/jamainternmed.2016.4182)

[16] ‘Duhem–Quine Thesis’. _Wikipedia_, 13 Jan. 2022. _Wikipedia_, [https://en.wikipedia.org/w/index.php?title=Duhem%E2%80%93Quine_thesis&oldid=1065410241](https://en.wikipedia.org/w/index.php?title=Duhem%E2%80%93Quine_thesis&oldid=1065410241)

[17] de Lorgeril, M., et al. ‘Mediterranean Alpha-Linolenic Acid-Rich Diet in Secondary Prevention of Coronary Heart Disease’. _Lancet (London, England)_, vol. 343, no. 8911, June 1994, pp. 1454–59. _PubMed_, [https://doi.org/10.1016/s0140-6736(94)92580-1](https://doi.org/10.1016/s0140-6736(94)92580-1)

[18] Bergeron, Nathalie, et al. ‘Effects of Red Meat, White Meat, and Nonmeat Protein Sources on Atherogenic Lipoprotein Measures in the Context of Low Compared with High Saturated Fat Intake: A Randomized Controlled Trial’. _The American Journal of Clinical Nutrition_, vol. 110, no. 1, July 2019, pp. 24–33. _PubMed_, [https://doi.org/10.1093/ajcn/nqz035](https://doi.org/10.1093/ajcn/nqz035)

[19] Hiebert, Nick. ‘Should Modern Humans Eat Like Hunter-Gatherers?’ _The Nutrivore_, 14 May 2021, [https://www.the-nutrivore.com/post/should-we-eat-like-hunter-gatherers.](https://www.the-nutrivore.com/post/should-we-eat-like-hunter-gatherers.)

[20] Perrin, James M., et al. ‘The Rise in Chronic Conditions among Infants, Children, and Youth Can Be Met with Continued Health System Innovations’. _Health Affairs (Project Hope)_, vol. 33, no. 12, Dec. 2014, pp. 2099–105. _PubMed_, [https://doi.org/10.1377/hlthaff.2014.0832](https://doi.org/10.1377/hlthaff.2014.0832)

[21] Kaplan, Hillard, et al. ‘Coronary Atherosclerosis in Indigenous South American Tsimane: A Cross-Sectional Cohort Study’. _Lancet (London, England)_, vol. 389, no. 10080, Apr. 2017, pp. 1730–39. _PubMed_, [https://doi.org/10.1016/S0140-6736(17)30752-3](https://doi.org/10.1016/S0140-6736(17)30752-3)

[22] Li, Mingde, et al. ‘Age Related Human T Cell Subset Evolution and Senescence’. _Immunity & Ageing_, vol. 16, no. 1, Sept. 2019, p. 24. _BioMed Central_, [https://doi.org/10.1186/s12979-019-0165-8](https://doi.org/10.1186/s12979-019-0165-8)

[23] Horvath, Steve, et al. ‘An Epigenetic Clock Analysis of Race/Ethnicity, Sex, and Coronary Heart Disease’. _Genome Biology_, vol. 17, no. 1, Aug. 2016, p. 171. _PubMed_, [https://doi.org/10.1186/s13059-016-1030-0](https://doi.org/10.1186/s13059-016-1030-0)

[24] Irimia, Andrei, et al. ‘The Indigenous South American Tsimane Exhibit Relatively Modest Decrease in Brain Volume With Age Despite High Systemic Inflammation’. _The Journals of Gerontology. Series A, Biological Sciences and Medical Sciences_, vol. 76, no. 12, Nov. 2021, pp. 2147–55. _PubMed_, [https://doi.org/10.1093/gerona/glab138](https://doi.org/10.1093/gerona/glab138)