[Brian Sanders](https://www.sapien.org/brian) is an entrepreneur and filmmaker who advocates for an "ancestral" diet he has coined the Sapien Diet™. This diet is characterized by high intakes of animal products and considerably strict abstinence from processed food consumption. His diet is supposedly based on the teaching of [Weston Price](https://en.wikipedia.org/wiki/Weston_A._Price), a dentist and nutritional anthropologist of the early 1900s who also advocated for the consumption of animal products. Right off the bat, we can tell that Brian's diet is marinated in quackery, as Price's methods and inferences were [questionable at best](https://quackwatch.org/related/holisticdent/).

Whatever the case, Brian publicly advocated for his diet back in 2020 during a talk at [Low Carb Down Under](https://lowcarbdownunder.com.au) entitled ["Despite what you've been told COWS CAN SAVE THE WORLD"](https://www.youtube.com/watch?v=VYTjwPcNEcw). In this talk, he describes the benefits and virtues of animal food production and consumption, and also touches on the supposed pitfalls and fallacies of veganism and plant-based diets. He structures his talk into three sections, which serve as direct rebuttals to three propositions that he characterizes as mainstream:

- Red meat is harmful to human health.
    
- Cows are harmful to the environment.
    
- It's unethical to kill animals for food.
    

Brian's talk is riddled with half-truths, misunderstandings, bald-faced lies, and other idiocy. To go through everything individually would take far too long, so in this article I will only be addressing the primary points that Brian makes throughout his presentation. Some of the quotes that I will be referencing will be paraphrased to account for Brian's unclear manner of speaking, but I nonetheless believe that I have represented his beliefs fairly. So, let's dive in!

###### Heath Claims

**Claim #1 (**[**4:52**](https://youtu.be/VYTjwPcNEcw?t=292)**):**

> "Hong Kong eats the most amount of meat and animal foods in the world and has the longest life expectancy."

**Post-hoc Fallacy**

This is just a post-hoc fallacy and a misunderstanding of the evidence being presented. Firstly, Brian's argument doesn't even get off the ground unless there is an assessment of what is causing the differences in longevity, and how the mortality stats are calculated. In this case, Brian is suggesting temporal connections that can't be verified to exist based on the data provided. More to the point, differential ecological associations between populations can't really tell us anything about differences in outcomes between individuals consuming the least meat versus individuals consuming the most meat.

For example, there is a positive ecological association between smoking and life expectancy [[1]](http://www.thefunctionalart.com/2018/07/visualizing-amalgamation-paradoxes-and.html). But, we wouldn't infer from this association that cigarette consumption increases longevity. Those sorts of inferences are what prospective cohort studies and randomized controlled trials are for, as they are actually equipped to assess individual-level exposure and outcomes.

**Base Rate Fallacy**  
According to Brian's source, Hong Kong's meat intake has been steadily increasing and didn't reach UK-levels of intake in 1972 and US-levels of intake in 1967 [[2]](https://www.nationalgeographic.com/what-the-world-eats/). If the hypothesis is that meat causes higher mortality via chronic disease, then people who started eating that much meat during those time periods would barely even be old enough to contribute substantially higher mortality statistics at the time of this report anyway. So, while it's true that Hong Kong now eats a lot of meat and enjoys longer lifespans, Brian is not appreciating the base rate of historical meat consumption for this population.

As an aside, it is also interesting to note that those in Hong Kong who followed a non-ancestral "Portfolio diet" (which is characterized by low saturated fats, sodium, and dietary cholesterol, as well as higher plant proteins, vegetable oils, high fruits, vegetables, and whole grains) were at a lower risk of dying from all causes, as well as dying of CVD or cancer [[3]](https://pubmed.ncbi.nlm.nih.gov/34959911/). In fact, multiple sensitivity analyses were done to remove the possibility for reverse causality, and the reductions in risk are still apparent.

**Claim #2 (**[**5:13**](https://youtu.be/VYTjwPcNEcw?t=313)**):**

> "There is an inverse association between red meat and total mortality in Asian populations."

**Red Herring**

The aggregated difference in the lowest to highest red meat intake in this pooled analysis was ~60g/day, which is approximately two bites [[4]](https://pubmed.ncbi.nlm.nih.gov/23902788/). It's unclear how this is capable of informing our judgements about the health value of red meat in the context of something like a Sapien Diet™. Not only that, but the contrast is occurring primarily at lower levels of intake.

Generally speaking, contrasts in red meat intake exceeding 80-100g/day are typically required on a per-cohort basis to reveal the increased risk of cardiovascular disease mortality or even all-cause mortality. Contrasts that fall below this threshold often do not provide for the statistical power that is necessary to obtain a statistically significant estimate.

Brian's own reference also confirms that meat intake in Asian populations is generally quite a bit lower than the United States.

> “Per capita beef consumption has decreased to some degree in the past decade in the United States but still remains substantially higher than that in Asian countries. Beef consumption increased in China, Japan, and Korea from 1970 to 2007.”

In actuality, when we select Asian populations with the widest contrasts in red meat intake, with sound multivariable adjustment models, appropriate population ages, and adequate follow-up time, we see the increase in total mortality with red meat very clearly [[5]](https://pubmed.ncbi.nlm.nih.gov/33320898/). Even when diet and lifestyle covariates are balanced between ranges of intake. These results are also consistent with results we see in American cohorts that also balance diet and lifestyle covariates reasonably well, such as the Nurse's Health Study [[6]](https://pubmed.ncbi.nlm.nih.gov/22412075/).

**Potential Contradiction**

Additionally, Brian has [stated publicly](https://twitter.com/FoodLiesOrg/status/1419347985935257601?s=20&t=_2uz8-vTlkgxnFPP4DCqtg) that steak is a good source of iron and can protect against iron deficiency. This claim is in accord with the available evidence on red meat consumption and iron deficiency anemia, with unprocessed red meat associating with a 20% decreased risk of anemia in the UK Biobank cohort [[7]](https://pubmed.ncbi.nlm.nih.gov/33648505/). Interestingly, unprocessed red meat was also associated with a statistically significant increase in the risk of many other diseases as well.

If the methods were sensitive enough to detect the inverse association with iron deficiency anemia, why not also conclude that the methods were also sensitive enough to detect the effect on heart disease as well? Why form differential beliefs about the causal nature of these associations?

Furthermore, the association between red meat intake and heart disease is observable when meta-analyzed as well [[8]](https://pubmed.ncbi.nlm.nih.gov/34284672/). For every 50g/day increase in red meat intake, there was a statistically significant dose-dependent relationship between red meat intake and heart disease.

Almost all of the most highly powered studies found positive associations between heart disease risk and red meat. Altogether, those studies also had the highest contrast in red meat, with Asian countries having the lowest contrast, as mentioned earlier. The majority of the included studies included adjustments for diet quality, used validated food frequency questionnaires, and had adequate follow-up time. The greatest increase in risk was found among studies with follow-up times exceeding 10 years. Removing all of the more poorly powered cohorts leaves us with a remarkably consistent relationship overall.

This one change results in a 38.3% attenuation in the I², going from 41.3% to 13%. Which suggests that of the variance that was attributable to heterogeneity, poor statistical power could explain about 68% of it.

Lastly, when cohort studies from around the world are meta-analyzed, we see the same thing for all cause mortality [[9]](https://pubmed.ncbi.nlm.nih.gov/24148709/). Overall, there is a non-significant increase in all-cause mortality risk when comparing the lowest red meat intakes to the highest red meat intakes. Whiteman, et al. (1999) was the only study that found a statistically significant decrease in risk, and is the sole reason for the non-significant finding.

However, Whiteman, et al. also had one of the shortest follow-up times, one of the youngest populations (and thus one of the lowest death rates), and used a very poor adjustment model (only adjusting for three confounders). If a leave-one-out analysis is performed that excludes Whiteman, et al., a different picture is painted.

Anybody who wishes to complain about this reanalysis is free to try to explain to me why increasing the potential for bias with poor multivariable adjustment is a desirable thing. But, it probably doesn't matter anyway. This meta-analysis was published in 2014, and more cohort studies have been published since then. If we add those results to the original forest plot conducted by Larsson et al., we would still get significant results.

**Claim #3 (**[**5:22**](https://youtu.be/VYTjwPcNEcw?t=322)**):**

> "We cannot use correlational studies to try to say that meat is bad."

**Category Error**

It's unclear what this means. As per the Duhem-Quine thesis, all scientific findings are correlational in one way or another [[10]](https://en.wikipedia.org/wiki/Duhem%E2%80%93Quine_thesis). As such, the state of being "correlational" is not a differential property between any two modes of scientific investigation. For example, intervention studies are a form of observational study, because you're observing what happens when you intervene. So this objection just seems like confusion, because if there ever was a study that showed meat to be "bad", it would be straightforwardly correlational in nature. Assuming that what Brian means by "correlational studies" is actually just nutritional epidemiology, even that would still be wrong.

In 2021, Shwingshakl et al. published an enormous meta-analysis that compared the results of 950 nutritional randomized controlled trials to results from 750 prospective cohort studies [[11]](https://pubmed.ncbi.nlm.nih.gov/34526355/). In the aggregate, results from nutritional epidemiology are consistent with results from nutritional randomized controlled trials approximately 92% of the time when comparing intakes to intakes (omitting supplements).

Across 23 comparisons of meta-analytically summated findings from both bodies of evidence, only 2 comparisons showed discordance. This means that nutritional epidemiology tends to replicate in randomized controlled trials in the supermajority of cases.

If not randomized controlled trials, I have no idea where Brian plants his goalpost for high evidential quality. Nevertheless, current evidence suggests that nutritional epidemiology is highly validated methodology if randomized controlled trials are used as the standard. If one places high credence in randomized controlled trials, it's unclear why they wouldn't also place high credence in nutritional epidemiology.

**Claim #4 (**[**5:36**](https://youtu.be/VYTjwPcNEcw?t=336)**):**

> "73% of hunter gatherers get over 50% of their diet from animal foods."

**Non Sequitur**

I'm not sure why we should care. If this isn't cashing out into some sort of health claim, then it seems very much like a non sequitur. Assuming this is implying something about the health value of hunter-gatherer diets, it is also misleading. I've discussed [here](https://www.the-nutrivore.com/post/should-we-eat-like-hunter-gatherers) why positions like this ultimately fail.

**Claim #5 (**[**5:51**](https://youtu.be/VYTjwPcNEcw?t=351)**):**

> _"We have beef as this highly bioavailable, easily digestible protein, and beans is one example of something that is touted as a pretty high protein food. But this is not the case."_

**False Claim**

This is just whacky, and it seems to be based on data from a 1950s textbook to which I cannot get access. However, we don't really need to in order to address this claim. Let's take a look at this table that Brian has generated.

First of all, if these two foods are weight-standardized, then the protein content of navy beans only makes sense if the navy beans were considered raw. Navy beans can't even be consumed raw because they're hard as fucking rocks. So, immediately this table is potentially misleading, having possibly presented an unrealistic comparison between these two foods. But, that's not the most egregious part of Brian's table. It's actually completely unnecessary to consider digestibility and biological value separately as Brian has [[12]](https://pubmed.ncbi.nlm.nih.gov/26369006/).

> _"The PDCAAS value should predict the overall efficiency of protein utilization based on its two components, digestibility and biological value (BV; nitrogen retained divided by digestible nitrogen). The principle behind this approach is that the utilization of any protein will be first limited by digestibility, which determines the overall amount of dietary amino acid nitrogen absorbed, and BV describes the ability of the absorbed amino acids to meet the metabolic demand."_

Biological value is inherently captured by both of the standard protein quality scores, the protein digestibility-corrected amino acid score (PDCAAS) and the digestible indispensable amino acid score (DIAAS). This means that if you want to represent all the things that matter for a given protein in isolation (such as digestibility, biological value, and limiting amino acids), all you need is either a PDCAAS or DIAAS value for the proteins in question. But, the DIAAS is probably better.

Aggregating DIAAS data across multiple protein foods paints a completely different picture than the one that Brian cobbled together [[13]](https://pubmed.ncbi.nlm.nih.gov/28748078/)[[14]](https://pubmed.ncbi.nlm.nih.gov/33333894/)[[15]](https://pubmed.ncbi.nlm.nih.gov/34476569/)[[16]](https://onlinelibrary.wiley.com/doi/full/10.1002/fsn3.1809). Some plant proteins actually do quite well. But what are these numbers really representing? Ultimately the scores are going to be truncated by limiting amino acids more than any other parameter, and pairing complementary proteins will increase the DIAAS value [[17]](https://pubmed.ncbi.nlm.nih.gov/34685808/). In fact, this is also true of the PDCAAS, as combining different lower-scoring plant proteins will often result in perfect scores [[18]](https://www.2000kcal.cz/lang/en/static/protein_quality_and_combining_pdcaas.php).

If beef is awesome in virtue of it getting a perfect score for protein digestibility, biological value, and limiting amino acids, then navy beans and wild rice must also be awesome too. If not, then I don't know what the hell Brian is talking about, or why he even brings the point up. It's also worth pointing out that certain animal foods, like collagen, actually score a zero on the PDCAAS as well.

As an aside, even if plant protein was generally inferior to animal protein by some evaluative standard (such as the PDCAAS or DIAAS), it would not necessarily mean that it would be more desirable to consume animal protein over plant protein. That would depend on one's goals. In fact, animal protein is associated with a number of chronic diseases in a dose-dependent manner, whereas plant protein is inversely associated, also in a dose-dependent manner [[19]](https://pubmed.ncbi.nlm.nih.gov/32699048/). This also holds true for Japanese populations, by the way [[20]](https://pubmed.ncbi.nlm.nih.gov/31682257/).

**Claim #6 (**[**6:35**](https://youtu.be/VYTjwPcNEcw?t=395)**):**

> "744 studies were excluded from consideration in the WHO's evaluation of meat as a carcinogen."

**Red Herring**

Here, Brian is referring to an analysis on red meat and colorectal cancer risk that was conducted by the International Agency for Research on Cancer (IARC) [[21]](https://pubmed.ncbi.nlm.nih.gov/26514947/). If you dig into the IARC's methods, you can see that they had very specific, sound inclusion-exclusion criteria, which involved selecting cohort studies with the widest contrasts in red meat intake, clear definitions, sufficient event rates and participant numbers, and adequate adjustment models.

> "A meta-analysis including data from 10 cohort studies reported a statistically significant dose-response association between consumption of red meat and/or processed meat and cancer of the colorectum. The relative risks of cancer of the colorectum were 1.17 (95% CI, 1.05-1.31) for an increase in consumption of red meat of 100 g/day and 1.18 (95% CI, 1.10-1.28) for an increase in consumption of processed meat of 50 g/day. Based on the balance of evidence, and taking into account study design, size, quality, control of potential confounding, exposure assessment, and magnitude of risk, an increased risk of cancer of the colorectum was seen in relation to consumption of red meat and of processed meat."

This is very sensible methodology for anyone familiar with epidemiology. Additionally, this is not the only reason Brian's claim is misleading, because there are not 744 cohort studies or RCTs combined on this question. Full stop. I can only imagine that he is referring to mechanistic studies or other weaker forms of evidence. He's never fully unpacked this claim, to my knowledge.

**Claim #6 (**[**7:05**](https://youtu.be/VYTjwPcNEcw?t=425)**):**

> _"There were 15 studies showing that red meat was good and 14 studies showing that red meat was bad. I mean, it's basically a toss-up."_

**Red Herring**

Again, the IARC had very strict inclusion-exclusion criteria, and of the studies that met those criteria, the majority of them found statistically significant associations between red meat and colorectal cancer. This is after multivariable adjustment for known confounders and covariates, in populations that we'd expect to have an increased risk. It's straight up expected that not all of the available studies on a given research question will be included in a meta-analysis.

**Red Herring**

Brian then goes on to claim that the results are a toss-up simply because there were 15 studies ostensibly showing that red meat was "good" and 14 studies ostensibly showing that red meat was "bad". To imply that this is necessarily a "toss up" is just pure confusion. Even if you have double the studies showing that red meat is "good", that doesn't necessarily mean there is a lower probability that red meat is "bad". It depends on the strength of the studies included. Consider this forest plot.

Here we see that despite the fact that there is a 2:1 ratio of studies that show a decreased risk with red meat to studies that show an increased risk with red meat, the summary effect measure still points toward a statistically significant increase in risk. This is because not every study has equal power or precision. Some findings are just less certain than others.

**Claim #7 (**[**7:22**](https://youtu.be/VYTjwPcNEcw?t=442)**):**

> "The risk factor of cancer from meat is 0.18%, whereas the risk factor of cancer from smoking is 10-30%."

**Unintelligible**

This is truly bizarre. It's incredibly unclear what Brian is trying to say here, and he was unable to unpack it to me in [our verbal debate](https://www.youtube.com/watch?v=S8p39Gwct1Y), so I'm not even sure he knowns what the fuck he means. His use of the term "risk factor" here makes the utterance appear like a category error. However, if I really stretch my imagination, I may be able to cobble together an interpretation that isn't gibberish.

**Equivocation**

Firstly, this appears to be just a straight up equivocation of cancer types. Colorectal cancer and lung cancer are two different diseases, and the prevalence of these diseases are different in the general population. If Brian's criticism is that the relative risk of lung cancer from smoking is higher than the relative risk of colorectal cancer from red meat, then he's just confused. Massive differences in the magnitude of those effect estimates are expected, as the prevalence of a given disease will determine the maximum possible relative risk [[22]](https://pubmed.ncbi.nlm.nih.gov/21402371/).

Let's take a look at the prevalence of these diseases in Canada [[23]](https://cancer.ca/en/cancer-information/cancer-types.). The prevalence of lung cancer among Canadian non-smokers is 1 in 84 (1.19% prevalence). Prevalence of colorectal cancer, assuming red meat has nothing to do with colorectal cancer, is 1 in 16 (6.25% prevalence). The baseline prevalence of lung cancer is much smaller than the baseline prevalence of colorectal cancer, so comparing the two is dubious.

In the case of lung cancer and colorectal cancer, the maximum possible relative risks would be ~53 and ~16, respectively. So it's not even mathematically possible for the relative risk of colorectal cancer from red meat to even approach the upper bounds for the relative risk of lung cancer from smoking that Brian submitted (assuming he meant 30x and not 30%).

For this reason, it's best that we do an apples to apples comparison. In a massive 2009 analysis by Huxley, et al., which helped inform the IARC's analysis on meat and cancer, 26 cohort studies were included in their meta-analytic summation [[24]](https://pubmed.ncbi.nlm.nih.gov/19350627/). Overall they showed a statistically significant 21% increase in risk of colorectal cancer with unprocessed red meat.

However, they also included an analysis on smoking, which found a statistically significant 16% increase in the risk of colorectal cancer with smoking as well. Yes, that is right— there was a slightly stronger association between red meat and colorectal cancer than there was between smoking and colorectal cancer. But the two were likely non-inferior. Huxley et al. also found around the same magnitude of effect for many other exposures.

What's the symmetry breaker? Why form a causal belief with regards to smoking or physical inactivity or obesity and not red meat? If Brian argues that he doesn't infer causality for any of the exposures with non-inferior effect sizes to red meat, then the appropriate thing to do is honestly just to laugh at his absurdity and move on.

If Brian argues that red meat has never been studied in the context of a junk-free diet, then we could just argue it in the opposite direction. For example, the smoking literature is also notoriously unadjusted for dietary covariates (which is something not many people appreciate about that body of evidence). As such, smoking arguably has never been studied in the context of a meat-free diet either, so perhaps the data on smoking is simply biased by red meat. Again, we need symmetry breakers.

**Claim #8 (**[**7:37**](https://youtu.be/VYTjwPcNEcw?t=457)**):**

> _"Ancestral foods are better than processed foods."_

**Appeal to Nature**

Why should we be using anthropological data about ancestral diets to inform best practice in terms of modern diets for modern humans? And why is the property of being ancestral a reasonable sufficiency criteria for a food to be "better" than processed foods? This seems like a non sequitur.

Favouring whole foods is heuristic, and not a rule. There are plenty of examples of processed foods being superior to whole foods, even foods that we could identify as ancestral. In fact, there are been specific analyses investigating the differential contributions of animal-based foods (red meat, poultry, fish, dairy, and eggs) and ultra-processed foods to disease risk within the context of a health-conscious population [[25]](https://pubmed.ncbi.nlm.nih.gov/35199827/).

Overall, ultra-processed foods and animal foods are non-inferior to one another for CVD mortality and cancer mortality risk. Animal based foods also seem to associate with the risk of endocrine disorders like T2DM, whereas ultra-processed foods did not. Once again, we require symmetry-breakers. Why form the belief that ultra-processed foods increase the risk of these diseases and not animal foods?

###### Environmental Claims

**Claim #9 (**[**10:13**](https://youtu.be/VYTjwPcNEcw?t=613)**):**

> "Grazing agricultural systems are better for the environment and sequester more carbon."

**False Claim**

This claim is as hilarious as it is vague. Firstly, better compared to what? According to Brian's reference (which was an analyses of the association between White Oak Pastures' regenerative grazing methodology and carbon balance) it was assumed that carbon sequestration estimates were exclusively from beef, yet poultry accounted for almost half of the carcass weight (46.5%) of the entire system [[26]](https://www.frontiersin.org/articles/10.3389/fsufs.2020.544984/full). They also can’t even attribute the sequestration to cows because the study was cross-sectional in design.

This study isn't actually investigating temporal changes in soil carbon at all. To make matters worse, the author's darling figure, −4.4kg CO₂-e kg carcass weight−1 per year, was actually just produced from thin air, and the cross-sectional association between years of grazing and soil carbon stocks between pasturelands was just assumed to be reflecting grazing-mediated soil carbon sequestration. Utterly misleading sophistry.

> _"Importantly, if we were to attribute the soil C sequestration across the chronosequence to only cattle, MSPR beef produced in this system would be a net sink of −4.4 kg CO2-e kg CW−1 annually."_

Even just ignoring the fact that this methodology creates mathematically atrocious pigs and chickens in terms of carbon balance in their model, the data provided suggests that 20 years worth of carbon sequestration are roughly equal to three years of plant composting. In second figure of the publication, they show a cross-sectional analysis of seven different degraded lands (previously used for crops) that are in the process of being restored over a varied number of years.

> _"In years 1–3, these fields are minimally grazed and receive 1 cm of compost ha−1 yr−1. After year 3, exogenous inputs (hay and compost) were ceased, and the regeneration strategy shifted toward an animal-only approach, whereby animals were the primary mechanism of improving the land."_

The third dot is roughly equal to that of the seventh dot on the chart, which represents three years of plant composting and 20 years of grazing, respectively. If we're assuming a causal relationship between grazing and soil carbon stock, why not also assume a causal relationship between composting and soil carbon stock?

If composting can increase soil carbon sequestration that much, why are they even trying to argue for grazing agriculture as a solution? It would appear that plant agriculture waste could be a viable solution for restoring these degraded lands as well. This is also just granting that these associations are reflective of soil carbon sequestration over time at all, which they may not be. Again, this analysis is cross-sectional.

It's convenient that the time scale of the investigation by Rowntree, et al. caps out at 20 years, because current literature suggests that there is a soil carbon saturation point at around 20 years, after which pasture grazing systems will yield diminishing returns.

Here we see three different scenarios for soil carbon sequestration rates from grazing agriculture. Even under the most generous estimates (the larger black hashed line), soil carbon sequestration plateaus at around 20 years.

However, current estimates suggest that if we switch to more plant-predominant diets by the year 2050, we could reforest a large proportion of current pastureland, which acts as a substantial carbon sink (~547GtCO2) [[27]](https://www.nature.com/articles/s41893-020-00603-4). The effect of that over 30 years is to neutralize about 15 years of fossil fuel emissions and 12 years of total world GHG emissions.

If we switch to plant-predominant diets by 2050 we could sequester -14.7 GtCO2 per year by reforesting pasture land, compared to other agricultural methods that make use of grazing land for pasture. Merely the introduction of pasture grazing increases land use by almost double compared to a vegan agricultural system [[28]](https://experts.syr.edu/en/publications/carrying-capacity-of-us-agricultural-land-ten-diet-scenarios).

In fact, there are stepwise decreases in the per-person carrying capacity of different agricultural scenarios as more pastureland is included in the model. However, vegan agricultural scenarios were largely comparable to both dairy-inclusive and egg-inclusive vegetarian models.

As mentioned earlier in this article, there are plant agriculture methods that are also touted as "regenerative", that also may have greater soil carbon sequestration potential per hectare than current "regenerative" grazing livestock methods [[29]](https://www.nature.com/articles/ncomms6012). It would be interesting to see a head-to-head comparison of wheat versus meat, using "regenerative" methodology, on soil carbon sequestration overall.

**Claim #10 (**[**13:15**](https://youtu.be/VYTjwPcNEcw?t=795)**):**

> "We have enough land for grazing agricultural systems."

**False Claim**

It has been calculated that even if all grasslands were repurposed for grazing, this could provide everyone on earth with 7-18g/day of animal protein per person on Earth [[30]](https://www.oxfordmartin.ox.ac.uk/publications/grazed-and-confused/). The authors also provided a hyper-idealized, candy-land scenario they also calculated that 80g/day of animal protein per person on Earth. However, this would require all pasturable land on Earth being used. As an aside, the authors also calculated an additional scenario that included waste from plant agriculture, but it probably won't be very relevant to Brian's idealized world, because plant agriculture would be extremely minimal on the Sapien Diet™.

The remaining two scenarios encounter some issues when we think of what would be required to sustain people on meat-heavy diets, because 80g of protein from the fattiest beef still would not provide enough calories per person. We would appear to need multiple planets. But we should use a grass-fed example to do the calculations, so I've chosen White Oak Pastures' ground beef as the example meat. We'll also be multiplying the results by 2.5 to account for the extra land used by rotational grazing and/or holistic management [[26]](https://www.frontiersin.org/articles/10.3389/fsufs.2020.544984/full).

Under no plausible scenario (calculable from the 'Grazed and Confused?' report) would either continuous grazing or rotational grazing be tenable on a global scale. Even if the calorie allotment for animal foods in the EAT Lancet diet was occupied only by grass-fed beef, we'd still be exceeding the carrying capacity of the Earth by 70% for continuous grazing and 325% for rotational grazing. As for Brian's pet diet, the Sapien Diet™, we'd need over 10 Earths in the optimistic plausible scenario.

Essentially, we would need to figure out a way to extend the pasturable land beyond the available land on Earth. Perhaps we could do this by terraforming Mars or ascending to a type 2 civilization on the Kardashev scale by building a megastructure in space, such as a Dyson sphere or an O'Neill cylinder. But, those options don't sound very ancestral at all.

We're not even scratching the surface, though. The authors of 'Grazed and Confused?' likely did not consider the suitability of each grassland in their calculation, because current suitability thresholds are set for crop production, rather than livestock. The issue is that grass itself could be considered a crop, so it's unclear why suitability considerations that have been established for crop production wouldn't also apply to pasture-raised animal production.

The IIASA/FAO define the suitability of a given grassland as a threshold of a 25% ratio of actual yield per acre and potential yield per acre [[31]](https://pure.iiasa.ac.at/id/eprint/13290/). Had these suitability criteria been considered by the authors of 'Grazed and Confused?', their models likely would have produced much smaller estimates. This is because much of the available grassland is either unsuitable or poorly suitable to begin with.

These suitability criteria have been used by livestock agriculture advocates to argue against the scalability of crop agriculture and for the scalability of grazing-based livestock agriculture [[32]](https://www.sciencedirect.com/science/article/abs/pii/S2211912416300013). However, [Avi Bitterman](https://twitter.com/AviBittMD) demonstrated on his [Discord server](https://discord.gg/YtfQNPnk) that these individuals are not symmetrically applying this standard and it would actually turn out that current "regenerative" grazing systems wouldn't be likely to even meet the suitability standards themselves.

According to figures produced by White Oak Pastures, their "regenerative" grazing system is far less efficient than conventional feedlot approaches [[33]](https://blog.whiteoakpastures.com/hubfs/WOP-LCA-Quantis-2019.pdf). Overall, White Oak Pastures uses 150% more land than conventional approaches to yield only about 20% of what a conventional farm can produce, and only 90% of the average slaughter weight.

This would give us a "suitability" estimate of around 7%, which would likely drastically reduce the amount of grassland that would be considered suitable for "regenerative" grazing agriculture as well. It would be doubly important to adhere to this standard when critiquing "regenerative" plant agricultural methods, in order to ensure an apples-to-apples comparison [[29]](https://www.nature.com/articles/ncomms6012).

**Claim #11 (**[**13:54**](https://youtu.be/VYTjwPcNEcw?t=834)**):**

> _"If we don't use all this cropland for corn, wheat, and soy...we can use some of this land for cows."_

**False Claim**

Here, Brian presents us a with figure from the USDA showing the available cropland in the United States as of 2007, and suggests that we simply have enough land for "regenerative" grazing. No analysis or even conceptual model of how this could be done was actually provided. He just expects us to take it for granted that the claim is true. But is it actually true?

As with the issues for this narrative that were entailed from the land requirement estimates detailed in the 'Grazed and Confused?' report, this narrative again encounters similar issues here. Firstly, a complete grazing agriculture scenario has been modeled, and the results suggest that the United States wouldn't get anywhere close to plausibly being able to meet their current demand for beef with grazing agriculture [[34]](https://iopscience.iop.org/article/10.1088/1748-9326/aad401). We'd simply need more land. About 30% more land.

> _"Increases in cattle population, placements, and slaughter rates are demonstrated in figure 2. The increased slaughtering and placement numbers would also require a 24% increase in the size of the national beef cow-calf herd, proportional to the increased annual grass-finishing placement rate, in order to provide additional cattle to stock the grass-finishing stage. Increases in both the cow-calf herd and the grass-finishing population together would result in a total increase to the US cattle population of an additional 23 million cattle, or 30% more than the current US beef cattle population as a whole"_

If Brian wants to criticize vegans for indulging idealized pie-in-the-sky fantasies, what in the sweet holy blue fuck is this shit? The United States can't maintain their current demands for beef with the system that Brian is proposing. This is doubly problematic if you consider that the White Oak Pastures model for which Brian advocates probably requires even more land than the conventional grazing methods included in the above model. This means that 30% could very well be an underestimate.

###### Ethical Claims

**Claim #12 (**[**19:11**](https://youtu.be/VYTjwPcNEcw?t=1151)**):**

> _"Vegans kill animals too. It's death on a plate, there's just no blood."_

**Appeal to Hypocrisy**

It's not clear what point this is trying to make. It seems like a failed appeal to hypocrisy to me. But, let's try to tackle the proposition in the most charitable way possible. Let's assume that Brian means to say that the Sapien Diet™ leads to fewer animal deaths than vegan diets that rely on plant agriculture (which is a claim that he has made before). In this case, this is just an empirical claim and needs to be supported by some sort of evidence.

In Brian's presentation, he supports this claim with a study of wood mouse predation after harvest, which showed that up to 80% of mice were preyed upon by predators upon the harvesting of cereal crops [[35]](https://www.sciencedirect.com/science/article/abs/pii/000632079390060E?via%3Dihub). What Brian isn't taking into account is that this can actually be used to are for less mouse predation on cropland as opposed to pastureland. Let me explain.

If you cut down your crop, you will expose the mice to predation. This is true. However, this also applies to pastureland. On pastureland, there is no substantial amount of tall forage that mice can use for shelter. The mice are exposed all year round. Which actually allows for the possibility that cropland could temporarily shelter mice from predators in a way that pastureland can't.

Furthermore, during [my debate](https://www.youtube.com/watch?v=S8p39Gwct1Y) with Brian, his cited evidence was the single cow that was killed when he paid a visit to his friend's cattle farm. Needless to say, this is not very good evidence, and this is not the evidence we will be using to steelman Brian's position. Instead, let's actually look at literature that compares the wildlife carrying capacity of plant verses grazing agricultural scenarios.

In 2020, Tucker et al. published a comprehensive comparative analysis of mammalian populations across a number of human-modified areas, such as cropland and pastureland [[36]](https://onlinelibrary.wiley.com/doi/full/10.1111/ecog.05126). Overall, their findings suggest that there is higher taxonomic diversity with increasing pastureland as opposed to increasing cropland.

In the absence of countervailing data, this actually counts against the hypothesis that pastureland entails less animal death than cropland. This is because increasing taxonomic diversity implies a higher number of trophic strata. A higher number of trophic strata implies higher levels of predation. Higher levels of predation thus imply higher levels of animal death. The land with the lesser carrying capacity should probably be assumed to entail less death.

**Red Herring**

Even if we granted Brian that pastureland entailed fewer animal deaths than cropland, it's not clear why vegans should necessarily care. If veganism is understood to be a social and politic movement that aims to extend human rights to animals when appropriate, it's not clear how the deaths entailed from cropland would be incompatible to those goals. In fact, we'd likely tolerate the killing of people who threatened our food supply in comparable ways as well.

For example, if our food security was threatened by endless armies of humans with the intelligence of fieldmice or insects and we had no practical means of separating them from our food without killing them, I don't think we'd consider killing them to be a rights violation. In fact, we'd likely assume a similar defensive posture and similarly tolerate the loss of human life if a foreign country was also threatening to destroy our food. I doubt we'd even consider the enemy deaths entailed from such a defensive posture to constitute a rights violation. We have the right to defend our property against assailants, and I doubt Brian would even deny this himself.

**Claim #13 (**[**19:19**](https://youtu.be/VYTjwPcNEcw?t=1159)**):**

> _"There is no life without death. This is how nature works...animals in nature either starve to death or are eaten alive...the animals are going to die either way, and it's not a good death...billions of people rely on animals for their livelihood."_

**Potential Contradiction**

This proposition is simple to address. If Brian believes that we are ethically justified in breeding sentient animals into existence for the explicit purpose of slaughtering them for food, but we would not be justified in condemning humans to likewise treatment, he must explain why. This same question can be asked of him regardless of the justification he uses for animal agriculture.

During my debate with Brian, I submitted to him an argument for the non-exploitation of animals, which is essentially just a rephrasing of [Isaac Brown](https://twitter.com/askyourself92)'s [Name the Trait](https://drive.google.com/drive/folders/1tAjU2Bv1tsGbNLA2TfJesgbIh8JKh9zc) argument.

**Definitions:**

**W** := Moral worth

**N** := Exploit it unnecessarily any more than we would for humans

**A** := Absent

**a** := animal

**h** := human

**t** := property

**P1)** For all things, if something has moral worth we should not exploit it unnecessarily any more than we would for humans.

**(∀x(Wx→¬Nx))**

**P2)** If animals don’t have moral worth, then there exists a property that is absent in animals such that if it were absent in humans, humans wouldn’t have moral worth.

**(¬Wa→∃t(Ata→(Ath→¬Wh)))**

**P3)** There doesn’t exist a property that is absent in animals such that if it were absent in humans, humans wouldn’t have moral worth.

**(¬∃t(Ata→(Ath→¬Wh)))**

**C)** Therefore, we should not exploit animals unnecessarily any more than we would for humans.

**(∴¬Na)**

This argument for non-exploitation simply requires one to identify the property that is true of humans that also is untrue of animals, that if true of humans, would cause humans to lose sufficient moral value, such that we’d be justified in slaughtering them for food as well. Brian rejected P3, stating that "consciousness" was the property true of humans, but untrue of animals, such that animals were ethical to exploit for food but humans were not. This fails and entails a contradiction on Brian's part, because consciousness is not a differential property between humans and the animals he advocates farming for food.

Thanks for reading! If you enjoy my writing and want more content like this, consider pledging to my [Patreon](https://www.patreon.com/thenutrivore)!

**References**

[1] Cairo, Alberto. Visualizing Amalgamation Paradoxes and Ecological Fallacies. [http://www.thefunctionalart.com/2018/07/visualizing-amalgamation-paradoxes-and.html](http://www.thefunctionalart.com/2018/07/visualizing-amalgamation-paradoxes-and.html.). Accessed 21 Aug. 2022.

[2] ‘What the World Eats’. _National Geographic_, [http://www.nationalgeographic.com/what-the-world-eats/](http://www.nationalgeographic.com/what-the-world-eats/.). Accessed 21 Aug. 2022.

[3] Lo, Kenneth, et al. ‘Prospective Association of the Portfolio Diet with All-Cause and Cause-Specific Mortality Risk in the Mr. OS and Ms. OS Study’. _Nutrients_, vol. 13, no. 12, Dec. 2021, p. 4360. _PubMed_, [https://doi.org/10.3390/nu13124360](https://doi.org/10.3390/nu13124360.).

[4] Lee, Jung Eun, et al. ‘Meat Intake and Cause-Specific Mortality: A Pooled Analysis of Asian Prospective Cohort Studies’. _The American Journal of Clinical Nutrition_, vol. 98, no. 4, Oct. 2013, pp. 1032–41. _PubMed_, [https://doi.org/10.3945/ajcn.113.062638](https://doi.org/10.3945/ajcn.113.062638.).

[5] Saito, Eiko, et al. ‘Association between Meat Intake and Mortality Due to All-Cause and Major Causes of Death in a Japanese Population’. _PloS One_, vol. 15, no. 12, 2020, p. e0244007. _PubMed_, [https://doi.org/10.1371/journal.pone.0244007](https://doi.org/10.1371/journal.pone.0244007.).

[6] Pan, An, et al. ‘Red Meat Consumption and Mortality: Results from 2 Prospective Cohort Studies’. _Archives of Internal Medicine_, vol. 172, no. 7, Apr. 2012, pp. 555–63. _PubMed_, [https://doi.org/10.1001/archinternmed.2011.2287](https://doi.org/10.1001/archinternmed.2011.2287.).

[7] Papier, Keren, et al. ‘Meat Consumption and Risk of 25 Common Conditions: Outcome-Wide Analyses in 475,000 Men and Women in the UK Biobank Study’. _BMC Medicine_, vol. 19, no. 1, Mar. 2021, p. 53. _PubMed_, [https://doi.org/10.1186/s12916-021-01922-9](https://doi.org/10.1186/s12916-021-01922-9.).

[8] Papier, Keren, et al. ‘Meat Consumption and Risk of Ischemic Heart Disease: A Systematic Review and Meta-Analysis’. _Critical Reviews in Food Science and Nutrition_, July 2021, pp. 1–12. _PubMed_, [https://doi.org/10.1080/10408398.2021.1949575](https://doi.org/10.1080/10408398.2021.1949575.).

[9] Larsson, Susanna C., and Nicola Orsini. ‘Red Meat and Processed Meat Consumption and All-Cause Mortality: A Meta-Analysis’. _American Journal of Epidemiology_, vol. 179, no. 3, Feb. 2014, pp. 282–89. _PubMed_, [https://doi.org/10.1093/aje/kwt261](https://doi.org/10.1093/aje/kwt261.).

[10] ‘Duhem–Quine Thesis’. _Wikipedia_, 19 Aug. 2022. _Wikipedia_, [https://en.wikipedia.org/w/index.php?title=Duhem%E2%80%93Quine_thesis&oldid=1105377595](https://en.wikipedia.org/w/index.php?title=Duhem%E2%80%93Quine_thesis&oldid=1105377595.).

[11] Schwingshackl, Lukas, et al. ‘Evaluating Agreement between Bodies of Evidence from Randomised Controlled Trials and Cohort Studies in Nutrition Research: Meta-Epidemiological Study’. _BMJ (Clinical Research Ed.)_, vol. 374, Sept. 2021, p. n1864. _PubMed_, [https://doi.org/10.1136/bmj.n1864](https://doi.org/10.1136/bmj.n1864.).

[12] ‘Dietary Protein Quality Evaluation in Human Nutrition. Report of an FAQ Expert Consultation’. _FAO Food and Nutrition Paper_, vol. 92, 2013, pp. 1–66. [https://www.fao.org/ag/humannutrition/35978-02317b979a686a57aa4593304ffc17f06.pdf](https://www.fao.org/ag/humannutrition/35978-02317b979a686a57aa4593304ffc17f06.pdf).

[13] Nosworthy, Matthew G., et al. ‘Determination of the Protein Quality of Cooked Canadian Pulses’. _Food Science & Nutrition_, vol. 5, no. 4, July 2017, pp. 896–903. _PubMed_, [https://doi.org/10.1002/fsn3.473](https://doi.org/10.1002/fsn3.473.).

[14] Han, Fei, et al. ‘Digestible Indispensable Amino Acid Scores (DIAAS) of Six Cooked Chinese Pulses’. _Nutrients_, vol. 12, no. 12, Dec. 2020, p. E3831. _PubMed_, [https://doi.org/10.3390/nu12123831](https://doi.org/10.3390/nu12123831).

[15] Fanelli, Natalia S., et al. ‘Digestible Indispensable Amino Acid Score (DIAAS) Is Greater in Animal-Based Burgers than in Plant-Based Burgers If Determined in Pigs’. _European Journal of Nutrition_, vol. 61, no. 1, Feb. 2022, pp. 461–75. _PubMed_, [https://doi.org/10.1007/s00394-021-02658-1](https://doi.org/10.1007/s00394-021-02658-1).

[16] Herreman, Laure, et al. ‘Comprehensive Overview of the Quality of Plant‐ And Animal‐sourced Proteins Based on the Digestible Indispensable Amino Acid Score’. _Food Science & Nutrition_, vol. 8, no. 10, Oct. 2020, pp. 5379–91. _DOI.org (Crossref)_, [https://doi.org/10.1002/fsn3.1809](https://doi.org/10.1002/fsn3.1809).

[17] Han, Fei, et al. ‘The Complementarity of Amino Acids in Cooked Pulse/Cereal Blends and Effects on DIAAS’. _Plants (Basel, Switzerland)_, vol. 10, no. 10, Sept. 2021, p. 1999. _PubMed_, [https://doi.org/10.3390/plants10101999](https://doi.org/10.3390/plants10101999).

[18] _PDCAAS Calculator - 2000KCAL_. [https://www.2000kcal.cz/lang/en/static/protein_quality_and_combining_pdcaas.php](https://www.2000kcal.cz/lang/en/static/protein_quality_and_combining_pdcaas.php). Accessed 24 Aug. 2022.

[19] Naghshi, Sina, et al. ‘Dietary Intake of Total, Animal, and Plant Proteins and Risk of All Cause, Cardiovascular, and Cancer Mortality: Systematic Review and Dose-Response Meta-Analysis of Prospective Cohort Studies’. _BMJ (Clinical Research Ed.)_, vol. 370, July 2020, p. m2412. _PubMed_, [https://doi.org/10.1136/bmj.m2412](https://doi.org/10.1136/bmj.m2412).

[20] Budhathoki, Sanjeev, et al. ‘Association of Animal and Plant Protein Intake With All-Cause and Cause-Specific Mortality in a Japanese Cohort’. _JAMA Internal Medicine_, vol. 179, no. 11, Nov. 2019, pp. 1509–18. _PubMed_, [https://doi.org/10.1001/jamainternmed.2019.2806](https://doi.org/10.1001/jamainternmed.2019.2806).

[21] Bouvard, Véronique, et al. ‘Carcinogenicity of Consumption of Red and Processed Meat’. _The Lancet. Oncology_, vol. 16, no. 16, Dec. 2015, pp. 1599–600. _PubMed_, [https://doi.org/10.1016/S1470-2045(15)00444-1](https://doi.org/10.1016/S1470-2045(15)00444-1).

[22] Sainani, Kristin L. ‘Understanding Odds Ratios’. _PM & R: The Journal of Injury, Function, and Rehabilitation_, vol. 3, no. 3, Mar. 2011, pp. 263–67. _PubMed_, [https://doi.org/10.1016/j.pmrj.2011.01.009](https://doi.org/10.1016/j.pmrj.2011.01.009).

[23] _'Cancer Types'_. Canadian Cancer Society, [https://cancer.ca/en/cancer-information/cancer-types](https://cancer.ca/en/cancer-information/cancer-types.). Accessed 24 Aug. 2022.

[24] Huxley, Rachel R., et al. ‘The Impact of Dietary and Lifestyle Risk Factors on Risk of Colorectal Cancer: A Quantitative Overview of the Epidemiological Evidence’. _International Journal of Cancer_, vol. 125, no. 1, July 2009, pp. 171–80. _PubMed_, [https://doi.org/10.1002/ijc.24343](https://doi.org/10.1002/ijc.24343).

[25] Orlich, Michael J., et al. ‘Ultra-Processed Food Intake and Animal-Based Food Intake and Mortality in the Adventist Health Study-2’. _The American Journal of Clinical Nutrition_, vol. 115, no. 6, June 2022, pp. 1589–601. _PubMed_, [https://doi.org/10.1093/ajcn/nqac043](https://doi.org/10.1093/ajcn/nqac043.).

[26] Rowntree, Jason E., et al. ‘Ecosystem Impacts and Productive Capacity of a Multi-Species Pastured Livestock System’. _Frontiers in Sustainable Food Systems_, vol. 4, 2020. _Frontiers_, [https://www.frontiersin.org/articles/10.3389/fsufs.2020.544984](https://www.frontiersin.org/articles/10.3389/fsufs.2020.544984).

[27] Hayek, Matthew N., et al. ‘The Carbon Opportunity Cost of Animal-Sourced Food Production on Land’. _Nature Sustainability_, vol. 4, no. 1, Jan. 2021, pp. 21–24. _www.nature.com_, [https://doi.org/10.1038/s41893-020-00603-4](https://doi.org/10.1038/s41893-020-00603-4).

[28] Peters, Christian J., et al. ‘Carrying Capacity of U.S. Agricultural Land: Ten Diet Scenarios’. _Elementa_, vol. 2016, no. e000116, 2016. _Experts@Syracuse_, [https://doi.org/10.12952/journal.elementa.000116](https://doi.org/10.12952/journal.elementa.000116).

[29] Gan, Yantai, et al. ‘Improving Farming Practices Reduces the Carbon Footprint of Spring Wheat Production’. _Nature Communications_, vol. 5, no. 1, Nov. 2014, p. 5012. _www.nature.com_, https://doi.org/10.1038/ncomms6012.

[30] ‘Grazed and Confused?’ _Oxford Martin School_, [https://www.oxfordmartin.ox.ac.uk/publications/grazed-and-confused/](https://www.oxfordmartin.ox.ac.uk/publications/grazed-and-confused/). Accessed 24 Aug. 2022.

[31] Fischer, G., et al. _Global Agro-Ecological Zones (GAEZ v3.0)- Model Documentation_. 2012, [http://www.fao.org/soils-portal/soil-survey/soil-maps-and-databases/harmonized-world-soil-database-v12/en/](http://www.fao.org/soils-portal/soil-survey/soil-maps-and-databases/harmonized-world-soil-database-v12/en/).

[32] Mottet, Anne, et al. ‘Livestock: On Our Plates or Eating at Our Table? A New Analysis of the Feed/Food Debate’. _Global Food Security_, vol. 14, Sept. 2017, pp. 1–8. _ScienceDirect_, [https://doi.org/10.1016/j.gfs.2017.01.001](https://doi.org/10.1016/j.gfs.2017.01.001).

[33] ‘Carbon Footprint Evaluation of Regenerative Grazing At White Oak Pastures: Results Presentation’. _Quantis - Environmental Sustainability Consultancy_, [https://blog.whiteoakpastures.com/hubfs/WOP-LCA-Quantis-2019.pdf](https://blog.whiteoakpastures.com/hubfs/WOP-LCA-Quantis-2019.pdf). Accessed 24 Aug. 2022.

[34] Hayek, Matthew N., and Rachael D. Garrett. ‘Nationwide Shift to Grass-Fed Beef Requires Larger Cattle Population’. _Environmental Research Letters_, vol. 13, no. 8, July 2018, p. 084005. _DOI.org (Crossref)_, [https://doi.org/10.1088/1748-9326/aad401](https://doi.org/10.1088/1748-9326/aad401).

[35] Tew, T. E., and D. W. Macdonald. ‘The Effects of Harvest on Arable Wood Mice Apodemus Sylvaticus’. _Biological Conservation_, vol. 65, no. 3, Jan. 1993, pp. 279–83. _ScienceDirect_, [https://doi.org/10.1016/0006-3207(93)90060-E](https://doi.org/10.1016/0006-3207(93)90060-E).

[36] Tucker, Marlee A., et al. ‘Mammal Population Densities at a Global Scale Are Higher in Human‐modified Areas’. _Ecography_, vol. 44, no. 1, Jan. 2021, pp. 1–13. _DOI.org (Crossref)_, [https://doi.org/10.1111/ecog.05126](https://doi.org/10.1111/ecog.05126).