Nina Teicholz’s _The Big Fat Surprise_ (BFS) is a book that claims to reveal “the unthinkable: that everything we thought we knew about dietary fats is wrong.” This is a trope that is often exploited to sell diet/nutrition books, and it works surprisingly well.

What makes this particular book interesting is not so much that it is bad (which it is) or that it is extravagantly biased (which it also is). No, what really fascinates me about this book is that the author excessively and shamelessly lifts other people’s material. Most notably Teicholz lifts from another popular low-carb book called _Good Calories, Bad Calories_ (GCBC) by Gary Taubes.

If I had written a book and I had “borrowed” other people’s work, here’s what I would do: I would cross my fingers and pray that no one ever notices. I would never bring it up, and diffuse it as quickly as I could if someone else brought it up. Not Teicholz. She gets in there and picks fights, accusing others of plagiarizing _her work_ if they write a piece that is also critical of low-fat diets.

> .[@newscientist](https://twitter.com/newscientist) hey, how can you list all arguments from The Big Fat Surprise without mentioning the book itself in yr article? Not ethical.. — Nina Teicholz (@bigfatsurprise) [August 2, 2014](https://twitter.com/bigfatsurprise/statuses/495419761019609088)

> [@Dick_Muller](https://twitter.com/Dick_Muller) That is so true. First [@TIMEHealth](https://twitter.com/TIMEHealth) now [@newscientist](https://twitter.com/newscientist) taking my work without attribution… — Nina Teicholz (@bigfatsurprise) [August 2, 2014](https://twitter.com/bigfatsurprise/statuses/495428132099616770)

Despite all this finger-wagging Teicholz does try to bring something new to the table. She makes an effort to speak to some people involved that Taubes did not. Although given the excessive misrepresentation of not only her work but those of others I am deeply skeptical that Teicholz fairly represented her conversations with some of these individuals.

Teicholz also attempts to appeal to the soccer mom demographic by writing a chapter about how women and children are not adequately studied when it comes to low-fat diets. However, any study she might cite in favor of her low-carb narrative (Shai, for example) has similar male:female ratios, and most don’t include children. There are good reasons for this, of course, but I won’t discuss them here.

At any rate, to wrap up this introduction, the results of fact-checking the first five chapters of BFS are below. I posses all of the texts that are discussed. I will be happy to provide them if you like. Let me know.

## Chapter 1: The Fat Paradox: Good Health on a High-Fat Diet

On page 11-12 Teicholz discusses the Masai tribe of Africa and how they consume quite a bit of milk daily yet have very low cholesterol (much like Taubes does in ch. 2 of GCBC). She also mentions that they are not fat and they don’t have high blood pressure. I don’t know why she throws the blood pressure and leanness in there since no one claims that milk causes high blood pressure, nor that these African tribes that walk about 30 miles per day and burn 300-500 kcals/hour would be fat because they drink milk. The real crime here is one of omission.

In support of her argument that diets heavy in saturated fat won’t lead to high cholesterol because the Masai do it, she cites an article published in the _NEJM_ titled “Some Unique Biologic Characteristics of the Masai of East Africa” [[1]](https://doi.org/10.1056/NEJM197104012841304). The _entire point_ of that article was to claim that the reason that the Masai have such low cholesterol levels despite a diet heavy in saturated fats was because they have a unique feedback mechanism that suppresses endogenous cholesterol synthesis that most of us don’t have. Yet there of course is no mention of this in the text (or GCBC) because to suggest that their low cholesterol was due to genetics would hurt her meat-is-good-for-you narrative.

Continuing with the Masai on page 12, Teicholz discusses George Mann and his findings:

> If our current belief about animal fat is correct, then all the meat and dairy these tribesmen were eating would have caused an epidemic of heart disease in Kenya. However, Mann found exactly the opposite—he could identify almost no heart disease at all.

As evidence for this she cites a paper titled “Atherosclerosis in the Masai” that does indicate very little evidence of infarctions, but does state the following:

> We find the Masai vessels do show extensive atherosclerosis; they show coronary intimal thickening which is equal to that seen in elderly Americans.

Mann goes on to say that the reason why there are so few occlusions despite the extensive atherosclerosis (ASCVD) is that the Masai’s blood vessels enlarge as they age [[2]](https://doi.org/10.1093/oxfordjournals.aje.a121365).

Now that we have uncovered some very important points that were concealed by Teicholz, we are still confronted with an odd reality. The Masai consume a ton of milk and likely a fair amount of meat and yet they do not have elevated cholesterol levels due to a unique biological mechanism. Despite the low cholesterol they still get ASCVD. Enough that men in their prime have the blood vessels of elderly Americans. Yet despite even this they manage to escape heart attacks because their vessels are larger than average. Wow. I don’t know what to make of the Masai, except that they are indeed a unique people. In this case I think we can treat the Masai as outliers and not assume that we can live like they do and remain free of heart disease.

On page 14 Teicholz discusses a text by Hrdlicka [[3]](http://repository.si.edu/xmlui/handle/10088/15501) published near the turn of the (last) century and states:

> The Native Americans he visited were eating a diet of predominantly meat, mainly from buffalo, yet, as Hrdlicka observed, they seemed to be spectacularly healthy and lived to a ripe old age.

However, if you go look at the text you will find that the diet of Native Americans is based around, y’know, the most abundant crops in the Americas: corn and wheat. There are several pages devoted to describing the diet, so I don’t want to quote all of it, but perhaps this will give you an idea of what Hrdlicka really found. Page 19:

> The principal article of diet among the Indians throughout the Southwest and Mexico is maize, which is eaten in the form of bread of various kinds, or as mush, or boiled entire. It is also parched on charcoal and eaten thus, or is ground into a fine meal, which, sweetened, constitutes the nourishing pinole of some of the tribes. Wheat is used in similar ways but less extensively. Next in importance to corn and wheat in the Indian diet are meat and fat and beans. Meat is scarce.

For a more nuanced view of the issue see [this post](http://carbsanity.blogspot.com/2014/05/no-big-surprise.html) and scroll down to “Hrdlička and the diet of Southwestern Native Americas.”

Page 15, Teicholz attempts to make the case that Africans living in British colonies nearly 100 years ago ate a ton of meat and had basically no cancer. As evidence for both of these claims she cites what amounts as a Letter to the Editor in the _BMJ_ by George Prentice [[4]](https://doi.org/10.1136/bmj.2.3285.1181-a).

> The _British Medical Journal_ routinely carried reports from colonial physicians who, though experienced in diagnosing cancer at home, could find very little of it in the African colonies overseas. So few cases could be identified that “some seem to assume that it does not exist,” wrote George Prentice, a physician who worked in Southern Central Africa, in 1923.

If you bother to look at the publication by Prentice you will notice that right after he says that some seem to assume that cancer does not exist, he immediately states why this is both a false and dangerous belief that has led to a patient of his dying of cancer because he himself believed that Africans did not get cancer when he was a younger doctor. He didn’t remove a breast tumor when he could have and should have and that his patient died because of this. Prentice also says in addition to breast cancer he sees other cancers all the time:

> I have also seen epithelioma of the face. In this case the eyelids and the whole of one eye were completely destroyed, and the bone of the eye socket was attacked; the case was inoperable. I have seen a tumour, fungating and evidently malignant, that had practically split the bones of the face, causing the eyes to bulge laterally and giving a strange chameleon look to the patient. It was inoperable. I have seen cancer of the left ovary that proved fatal. I believe I have seen cases of malignant disease of the liver, but as there was no autopsy the diagnosis was not confirmed. I have removed many large tumours of the testicle which, if not cancerous, are of a nature unknown to me. Keloids and fatty tumours are very common.

In case you could not tell, Teicholz takes Prentice’s words _completely out of context_ to make it appear he was communicating the opposite of what he was actually communicating.

I’m sure she’s also hoping that the reader won’t realize that – true or not – all she has been discussing so far in the book are very tenuous and unscientific correlations, and that by the time the readers get to the part where she bashes these types of associations that they won’t realize she’s being wildly inconsistent and even hypocritical in how she deals with studies and observations.

Page 16:

> It is true that American beef from a cow raised on grain does have a different fatty-acid profile from an ox hunted in the wild. In 1968, the English biochemist Michael Crawford was the first to look at this question in detail. […] His paper seemed to confirm that modern-day people should not consider their domesticated meat to be anywhere near as healthy as hunted meat from the wild. And for the past forty-five years, Crawford’s paper has been widely cited, forming the general view of the subject. What Crawford buries in his data, however, is that the _saturated_ fat content of the wild and domesticated animal meats hardly differed at all.

Yep. Crawford “buries” it by making it Figure 1 in his paper [[5]](https://doi.org/10.1016/s0140-6736(68)92034-5). I know when I want to bury data in a paper I visualize it and put it at the head of the results.

By the way, this “widely cited” paper has been cited only 183 times since it’s publication in 1968. Truly a landmark paper, this one.

## Chapter 2: Why We Think Saturated Fat Is Unhealthy

### Cribbing Taubes Alert

In GCBC on page 14 Taubes discusses a century-old document that was published by a German journal that is both very difficult to find and written in German.

> The evidence initially cited in support of the hypothesis came almost exclusively from animal research-particularly in rabbits. In 1913, the Russian pathologist Nikolaj Anitschkow reported that he could induce atherosclerotic-type lesions in rabbits by feeding them olive oil and cholesterol.

Do you think Taubes both A) possesses that obscure text AND B) is fluent in German? It’s possible, although my money would be on Taubes simply reading what others had written about the study and simply paraphrasing.

BFS page 22:

> Early evidence suggestively linking cholesterol to heart disease also came from animals. In 1913, the Russian pathologist Nikolaj Anitschkow reported that he could induce atherosclerotic-type lesions in rabbits by feeding them huge amounts of cholesterol.

What are the odds that Teicholz also speaks German and has the German text that was published over 100 years ago? Again, possible, but given the similarity of how she and Taubes discuss the paper my guess is that she simply paraphrased Taubes without attribution.

Here’s something interesting… On page 16 of GCBC Taubes says the following:

> In 1937, two Columbia University biochemists, David Rittenberg and Rudolph Schoenheimer, demonstrated that the cholesterol we eat has very little effect on the amount of cholesterol in our blood.

As evidence he cites Rittenberg and Schoenheimer’s 1937 paper titled “Deuterium as an indicator in the study of intermediary metabolism XI. Further studies on the biological uptake of deuterium into organic substances, with special reference to fat and cholesterol formation” [[6]](https://doi.org/10.1126/science.82.2120.156).

On page 23 of BFS Teicholz writes the following:

> The notion that cholesterol in the diet would translate directly into higher cholesterol in the blood just seemed intuitively reasonable, and was introduced by two biochemists from Columbia University in 1937.

She also cites the same exact paper. You see the problem, right? Both Taubes and Teicholz claim opposite things yet cite the same paper as evidence. So who is correct? Actually neither of them are correct because the cited text doesn’t address the issue. The paper discusses experiments on animals such as rodents and chicks, not humans as is implied by both Taubes and Teicholz. Furthermore, the only thing these experiments demonstrate is that these animals are capable of synthesizing cholesterol, and has essentially nothing to do with dietary cholesterol influencing or not influencing serum cholesterol.

Continuing with the dietary cholesterol controversy, on page 23 immediately after the above statement Teicholz claims:

> It was Ancel Keys himself who first discredited this notion. Although in 1952 he stated that there was “overwhelming evidence” for the theory […]

She then accuses him of being a hypocritical flip-flopper for arrogantly walking the statement back three years later by saying that tremendous amounts of cholesterol have only a trivial effect on serum cholesterol and that “this point requires no further consideration.”

Damn, this guy sounds like an arrogant prick considering he wholly endorsed the theory a few years before, right?!

…Except if you look at the 1952 paper where Teicholz pulls that quote Keys says the EXACT OPPOSITE of what Teicholz claims [[7]](https://doi.org/10.1161/01.CIR.5.1.115). In the paper Keys argues that the animal experiments that have shown that feeding high cholesterol to, say, rabbits have no relevance to humans, going on to say

> No animal species close to man in metabolic habitus has been shown to be susceptible to the induction of atherosclerosis by cholesterol feeding.

AND

> From the animal experiments alone the most reasonable conclusion would be that the cholesterol content of human diets is unimportant in human atherosclerosis.

AND

> Direct evidence on man in this connection is unimpressive.

Besides, even if he did hold an erroneous belief beforehand, why would you want to knock a guy for simply following the evidence? This is _science_: you are supposed to always be self-correcting.

Page 23-24:

> In 1992, one of the most comprehensive analyses of this subject concluded that the vast majority of people will react to even a great deal of cholesterol in the diet by ratcheting down the amount of cholesterol the body itself produces. […] Responding to this evidence, health authorities in Britain and most other European nations **in recent years** have rescinded their advisories to cap dietary cholesterol.

Emphasis mine. The reason for bolding “in recent years” is because the evidence cited for this sentence is a paper published in 1987 [[8]](https://doi.org/10.1093/ajcn/45.5.1060)!! And of course the evidence they were apparently responding to was a meta-analysis published in 1992 [[9]](https://doi.org/10.1093/ajcn/55.6.1060). There’s a chronology problem here. A meta-analysis, by the way, whose first sentence of the summary states:

> **Serum cholesterol concentration is clearly increased by added dietary cholesterol** but the magnitude of predicted change is modulated by baseline dietary cholesterol.

Again, emphasis mine.

On page 25 you will see this structure:

This structure is incorrect on many levels. For one it is not even a fatty acid. It’s actually not anything because it is not a legitimate chemical structure, but it’s closest to pentane which no one would want to consume. In order for it to be a fatty acid it would at least need a carboxyl group at one end. However, that would make the structure into valeric acid which is not commonly found in foods (especially the butter, meat, and cheese Teicholz promotes) and not commonly consumed unless you’re eating valerian root.

I pointed this out on my Amazon review of the book and I got pilloried because apparently I was supposed to simply _know_ that the Fisher projection was not really supposed to be a fatty acid, but was supposed to be a simple, generic structure to illustrate how hydrogens are arranged around carbon atoms. However, if Teicholz didn’t want to put a proper carboxyl group because she was afraid it might confuse her audience she could have at least put an “R” or ellipses or something to indicate that part of the fatty acid is being left out in order to concentrate on the hydrogens. Even so, if it is indeed just a generic structure used to illustrate how hydrogens are arranged then why are the end hydrogens arranged incorrectly, and why are there two missing hydrogens? This structure is absolutely wrong no matter how you slice it.

The lies about Ancel Keys continue on page 27 when Teicholz discusses a paper of his called “Atherosclerosis: A Problem in Newer Public Health” and says this paper received “enormous attention” and was the genesis of America’s alleged fear of fat.

There is no evidence at all that Keys’s 1953 paper received “enormous attention.” In fact, the evidence that exists would suggest the opposite. There was no mention of the paper in the lay press. That is until relatively recently when [Gary Taubes began lying about it](https://thescienceofnutrition.wordpress.com/2014/04/21/fat-in-the-diet-and-mortality-from-heart-disease-a-plagiaristic-note/). What about academia? According to Google Scholar this highly influential paper has only been cited 247 times since its publication, which spans 61 years as of this writing. An average of four citations per year. It was cited merely 99 times from the time it was published to 1973, a full twenty years after its publication. For comparison, on page 159-160 Teicholz mentions a study whose results she claims were “ignored.” That study was published in 1992 and has received 682 citations.

Page 31:

> Keys found further ammunition for his hypothesis from a compelling observation made during World War II, which is that deaths from heart disease dropped dramatically across Europe during wartime and rebounded soon afterward. These events led Keys to presume that the food shortages— particularly of meat, eggs, and dairy—were very likely the cause. There were, however, other explanations: for instance, sugar and flour were also scarce during the war; people breathed fewer car-exhaust fumes due to gasoline shortages and got more exercise by cycling or walking to get around. Other scientists noted these alternative explanations for the decline in heart disease, but Keys dismissed them outright.

The paper that noted the alternative explanations was published in 1957, yet Keys was apparently dismissing them in 1956 [[10]](https://doi.org/10.1016/0002-9343(57)90325-x)[[11]](https://doi.org/10.1016/0021-9681(56)90040-6).

On page 34, Teicholz discusses a paper by Yerushalmy and Hilleboe that criticized a graph in Keys’s “Atherosclerosis: A Problem in Newer Public Health” paper mentioned above [[12]](https://pubmed.ncbi.nlm.nih.gov/13441073/).

> Yerushalmy’s objection was that Keys seemed to have selected only certain countries that fit his hypothesis. There were other factors that could equally well explain the trends in heart disease in all these countries, he asserted.

If you actually read Keys’s paper you will note that Keys mentioned that he left out some less-developed countries because they had very poor vital health statistics [[13]](https://pubmed.ncbi.nlm.nih.gov/13085148/). Some more developed European countries he claims he would have included if the Nazi’s had not very recently invaded, occupied, and rationed food which would confound his simple cross-sectional analysis. It wasn’t that he was a diabolical scientist bent on lying to the public about the cause of heart disease. Or if he was he had a damn fine excuse for not using those countries.

It’s funny because on pages 34-36 Teicholz criticizes Keys for not including more European countries like France and Switzerland (which incidentally would have fallen right in line with Keys’s graph). Then a few pages later on 37-38 Teicholz discusses Keys’s Seven Countries Study and criticizes him for including countries that Nazi Germany had invaded and occupied several years before.

At any rate, Yerushalmy and Hilleboe did indeed point out some other factors in their paper, most prominently they pointed out that [both _animal_ fat and _animal_ protein were far better correlated with heart disease than total fat](https://thescienceofnutrition.files.wordpress.com/2014/03/yh-2.jpg). Many different types of heart disease, in fact. This held true whether or not it was calculated as total amounts or as a percentage of total calories. Moreover, _vegetable_ protein and _vegetable_ fat were negatively correlated with heart disease.

Of course there was no way that Teicholz was ever going to mention this.

Page 40:

> I looked more closely into the dietary data on Greece, because it became the exemplar for the Mediterranean diet (see Chapter 7), and I found one of the most stunning and troubling errors. In that country. Keys had sampled the diets on Crete and Corfu more than once, in different seasons, in order to capture variations in the food eaten. Yet in an astonishing oversight, one of the three surveys on Crete fell during the forty-eight-day fasting period of Lent.

_Astonishing_ oversight? One of the most _stunning_ and _troubling_ errors? Despite what appears to be insincere hand-wringing over this gravest of all scientific errors, I don’t really see the problem here. The question that needs to be asked here is: Does collecting dietary information during a period where some Christians adhere to a quasi-fasting ritual invalidate the dietary data? I suppose it would if a sufficient number of participants were strictly adhering to the fast. Was this the case? According to the 211-page write-up of the study programs and objectives this was not the case [[14]](https://pubmed.ncbi.nlm.nih.gov/5442783/):

> The seasonal comparisons in Crete and Corfu were of interest because the survey in Crete in February and part of the survey in Corfu in March-April were in the 40-day fasting period of Lent of the Greek Orthodox church, but strict adherence did not seem to be common in the populations of the present study.

How would the scientists come to the conclusion that there was no strict adherence? They could simply compare the dietary data collected during the spring with the dietary data collected during other times of the year. What if the participants were all lying on their dietary surveys? The researchers also collected the actual foods eaten by participants, lyophilized them, and sent them out for chemical analysis. Apparently there were no significant differences with that data either.

Even if the alleged “error” of collecting data in the spring was so insurmountable it had to be thrown out, it would not invalidate the other two dietary collections in Crete and it would certainly not be nearly enough to nullify the entire study. It seems pretty clear to me that while writing this book Teicholz is actively searching for any hint of impropriety. She discovered a mention of Lent, decided to ignore the rest, and enthusiastically proclaimed that she had unearthed some alarming facts about the study.

Additionally she tries to make the case on pages 41 and 42 that Keys tried to sheepishly bury the flawed methodology of his crappy study. If this is true he did an exceedingly poor job of it, considering in addition to all of the analyses published from the data.

Keys published:

- A 211-page paper describing the study objectives and methods [[14]](https://pubmed.ncbi.nlm.nih.gov/5442783/).
    
- A 300+ page monograph describing the particular details of data collection in each country [[15]](https://doi.org/10.1111/j.0954-6820.1966.tb04737.x).
    
- A 300+ page book describing in great detail the study and its results [[16]](https://doi.org/10.1001/jama.1981.03310300063026).
    

In addition, there were other entire books published on the study [[17]](https://books.google.ca/books/about/The_Seven_Countries_Study.html?id=hgWRAAAACAAJ&redir_esc=y)[[18]](https://link.springer.com/book/10.1007/978-4-431-68269-1). Keys is not being obfuscatory.

## Chapter 3: The Low-Fat Diet Is Introduced to America

### Cribbing Taubes Alert

On page 49 Teicholz discusses an AHA nutrition committee report:

> Committee members went so far as to rap diet-heart supporters like Keys on the knuckles for taking “uncompromising stands based on evidence that does not stand up under critical examination.” The evidence, they concluded, did not permit such a “rigid stand.”

On page 20 of GCBC, Taubes makes a similar statement regarding the same report and uses the same quotes [[19]](https://doi.org/10.1001/jama.1957.62980180004013). I would argue that both get it wrong. The report seems to have somewhere between a neutral and a favorable view of Keys, as evidenced by the following quotes:

> Mayer et al. found that high-fat animal or vegetable diets increased and low-fat diets decreased serum cholesterol of normal subjects, confirming earlier data of Keys.

> Keys, in particular, has placed emphasis on the proportion of total dietary calories contributed by the common food fats […] Certainly there is an abundance of data, both clinical and experimental, that tends to relate excess fat intake to atherosclerosis.

### Cribbing Taubes Alert

On page 49 Teicholz continues discussing the views of the AHA:

> The AHA committee swung around in favor of their ideas, and the resulting report in 1961 argued that “the best scientific evidence available at the present time” suggested that Americans could reduce their risk of heart attacks and strokes by cutting the saturated fat and cholesterol in their diets.

On page 21 of GCBC Taubes says essentially the same thing and uses the same quote from the same paper [[20]](https://pubmed.ncbi.nlm.nih.gov/14447694/).

Continuing on the report, page 50:

> Keys himself thought that the 1961 AHA report he had helped write suffered from “some undue pussy-footing” because it had prescribed the diet only for high-risk people rather than the entire American population […]

On page 21 of GCBC Taubes says almost the exact same thing, including the undue pussy-footing quote. However, Taubes cites the quote correctly as being from _Time_ magazine’s article titled Fat in the Fire, whereas Teicholz cites it incorrectly as The Fat of the Land [[21]](https://content.time.com/time/subscriber/article/0,33009,895155,00.html)[[22]](https://content.time.com/time/subscriber/article/0,33009,828721,00.html).

### Cribbing Taubes Alert

There is getting to be too many of these alerts. On page 4 of GCBC Taubes states:

> “People should know the facts,” Keys told Time. “Then if they want to eat themselves to death, let them.”

BFS page 50:

> “People should know the facts,” he said. “Then, if they want to eat themselves to death, let them.”

This quote is found in _Time_ magazine’s The Fat of the Land article [[22]](https://content.time.com/time/subscriber/article/0,33009,828721,00.html). Continuing…

GCBC page 21:

> The _Time_ cover story, more than four pages long, contained only a single paragraph noting that Keys’s hypothesis was “still questioned by some researchers with conflicting ideas of what causes coronary disease.”

BFS page 51:

> In the _Time_ article, there is only a brief mention of the reality that Keys’s ideas were “still questioned” by “some researchers” with conflicting ideas about what causes coronary disease.

On page 54 Teicholz educates the reader on case-control studies:

> These studies are understood to suffer from “recall bias,” whereby patients may inaccurately remember past consumption.

_Immediately after_ this disclaimer Teicholz goes on to produce several case-control studies that fit her narrative. By doing so, this is what I hear as a reader: “Case-control studies suck. Don’t try to use them as evidence. But here are a few whose results I like and you should know about them.”

### Cribbing Taubes Alert

GCBC page 26:

> [JAMA] reported that the mostly Italian population of Roseto, Pennsylvania, ate copious animal fat – eating prosciutto with an inch-thick rim of fat, and cooking with lard instead of olive oil – and yet had a “strikingly low” number of deaths from heart disease, Keys said it warranted “few conclusions and certainly cannot be accepted as evidence that calories and fats in the diet are not important.”

BFS page 55:

> [T]he mostly Italian population living there had a “strikingly low” number of deaths from heart disease […] the local diet included copious amounts of animal fats, including prosciutto with fat an inch thick around the rim, and most meals cooked in lard. […] Keys concluded that the Roseto data “certainly cannot be accepted as evidence that calories and fats in the diet are not important.”

There is so much information in both those publications, yet surprisingly the same exact quotes are independently plucked by Teicholz [[23]](https://doi.org/10.1001/jama.1964.03060360005001)[[24]](https://doi.org/10.1001/jama.1966.03100020081018).

Page 56:

> F. W. Lowenstein, a medical officer for the World Health Organization in Geneva, collected every study he could find on men who were virtually free of heart disease, and concluded that their fat consumption varied wildly.

I suppose that’s true according to the study [[25]](https://doi.org/10.1159/000385025). Fat intake varied from 21 g/day to 355 g/day in the case of the Somalis. Although, if you remove the Somalis as something of an outlier (they also consume 6,247 kcals/day according to the paper), then fat intake in all the other populations drops to 100 grams of fat per day or less.

### Cribbing Taubes Alert

Teicholz uses Taubes’s Karl Popper quote.

GCBC page 24-25:

> [The scientific method requires] that scientists not just test their hypotheses, but try to prove them false. “The method of science is the method of bold conjectures and ingenious and severe attempts to refute them,” said Karl Popper.

BFS page 56-57:

> A scientist must always try to disprove his or her own hypothesis. Or, as one of the great science philosophers of the twentieth century, Karl Popper, described, “The method of science is the method of bold conjectures and ingenious and severe attempts to refute them.”

Page 57:

> […] Keys was not on the lookout for his own biases. He considered the burden of proof to be on those opposing him. He made no attempts to refute his own ideas, as Popper advised. He promoted the “idol of his mind” without hesitation.

OMG. _What?! How??_ Can I get some evidence for that?

Page 65 Teicholz then pivots to Framingham and mentions how they found cholesterol to be a big predictor of death [[26]](https://www.semanticscholar.org/paper/Carbohydrate-induced-and-fat-induced-lipemia.-Ahrens-Hirsch/e1e4cfdfe4d98afd5b711226cdc75bdb0c843cb9)[[27]](https://doi.org/10.1001/jama.257.16.2176). BUT…. Let’s face it. They are bound to happen. There are surely citation errors in this blog post.

> However, thirty years later, in the Framingham follow-up study—when investigators had more data because a greater number of people had died— it turned out that the predictive power of total cholesterol was not nearly as strong as study leaders had originally thought.

Is she talking about the paper that states in the conclusion “This study and the Coronary Drug Project results on nicotinic acid therapy show a direct association of cholesterol levels with mortality, which becomes stronger with lengthy follow-up” [[27]](https://doi.org/10.1001/archinte.1992.00400190013003)? Is that the one she’s talking about? Because that’s the one she cited. The paper also states that the association holds strong even after adjusting for individual differences in blood pressure, smoking, relative weight, and diabetes.

Page 67:

> Not until 1992, in fact, did a Framingham study leader publicly acknowledge the study’s findings on fat. “In Framingham, Mass, the more saturated fat one ate . . . the _lower_ the person’s serum cholesterol. . . and [they] weighed the _least_” wrote William P. Castelli […]

What the reader doesn’t know is that 1) this quote is taken from an editorial that extols the virtues not of meat and cheese, but of the unsaturated fat in nuts, 2) there is literally a half page of text between the ellipses, 3) the italics are not part of the original quote, and most importantly 4) Teicholz cuts off the quote immediately before the author mentions that he is talking about the people that were the _most physically active_ [[29]](https://doi.org/10.1001/archinte.1992.00400190013003). But I’m sure their physical activity has no effect on their weight or cholesterol levels, right?

### Cribbing Taubes Alert

On page 159 of GCBC Taubes quotes from a 1967 JAMA editorial [[30]](https://doi.org/10.1001/jama.1966.03110190079022):

> _JAMA_ published an editorial in response to Kuo’s article, suggesting that the “almost embarrassingly high number of researchers [who had] boarded the ‘cholesterol bandwagon'” had done a disservice to the field. “This fervent embrace of cholesterol to the exclusion of other biochemical alterations resulted in a narrow scope of study,” the editorial said.

On page 71 of BFS Teicholz uses the same exact quote:

> An “almost embarrassingly high number of researchers boarded the ‘cholesterol bandwagon,'” lamented the editors of the _Journal of the American Medical Association_ in 1967, referring to the narrow, “fervent embrace of cholesterol” to the “exclusion” of other biochemical processes that might cause heart disease.

## Chapter 4: The Flawed Science of Saturated versus Polyunsaturated Fat

### Cribbing Taubes Alert

Much like elsewhere in BFS, Teicholz appears to take an incredible amount of what might be called “inspiration” from GCBC. Much like in GCBC, Teicholz discusses the Anti-Coronary Club trial, plucks the same quotes, and discusses the same media reactions.

GCBC, page 36:

> The first and most highly publicized was the Anti-Coronary Club Trial, launched in the late 1950s by New York City Health Department Director Norman Jolliffe.

BFS, page 73:

> An early and celebrated trial was called the Anti-Coronary Club, launched by Norman Jolliffe, director of the New York City Health Department, in 1957.

GCBC, page 36:

> The eleven hundred middle-aged members of Jolliffe’s Anti-Coronary Club were prescribed what he called the “prudent diet,” which included at least one ounce of polyunsaturated vegetable oil every day. The participants could eat poultry or fish anytime, but were limited to four meals a week containing beef, lamb, or pork.

BFS, page 73:

> He signed up eleven hundred men to his Anti-Coronary Club and instructed them to reduce their consumption of red meat, such as beef, lamb, or pork, to no more than four times a week (which would be considered a lot by today’s standards!) while consuming as much fish and poultry as they liked.

GCBC, page 36:

> [T]wenty-six members of the club had died during the trial, compared with only six of the men whose diet had not been prudent.

BFS, page 74:

> [T]wenty-six members of the diet club had died during the trial, compared to only six men from the controls.

Okay, maybe both Taubes and Teicholz did independent research and came across the same study and both found it was compelling enough to include in their books. And maybe they both independently included the same info from the study as well. It’s certainly possible. But would they mention the same NYT article?

GCBC, page 36:

> “Diet Linked to Cut in Heart Attacks,” reported the New York Times in May 1962.

BFS, page 74:

> “Diet Linked to Cut in Heart Attacks,” reported the New York Times in 1962 […]

Would they independently use the same quote from the multiple trial publications?

GCBC, page 36:

> Eight members of the club died from heart attacks, but none of the controls. This appeared “somewhat unusual,” Christakis and his colleagues acknowledged.

BFS, page 74:

> [I]nvestigators began to find “somewhat unusual” results: […] Eight members of the club had died of heart attacks, but not one of the controls.

They also both misrepresent the study:

GCBC, page 36:

> They discussed the improvements in heart-disease risk factors (cholesterol, weight, and blood pressure decreased) and the significant reduction in debilitating illness “from new coronary heart disease,” but omitted further discussion of mortality.

BFS, page 74:

> In the discussion section of the final report, the authors […] emphasized the improved risk factors among the men in the diet club but ignored what those risk factors had blatantly failed to predict: their higher death rate.

Notice how both Taubes and Teicholz minimize the main results of the study, namely that the prudent diet did exactly what researchers imagined it would do: reduce not only risk factors for heart disease, but also actual coronary events [[31]](https://doi.org/10.2307/4592649)[[32]](https://doi.org/10.1093/ajcn/7.4.451)[[33]](https://doi.org/10.1056/NEJM196205172662001). Further, they both misrepresent the study by claiming those devious scientists omitted discussion of death rate when nothing could be further from the truth. Both cited publications discuss death rate and mortality among participants very clearly. In fact, the slight difference in death from causes other than heart disease was not even significant. From the 1966 publication31 (emphasis mine):

> The rates for these deaths in the 50-59 age group were 689 per 100,000 person-years in the experimental group, and 666 per 100,000 in the control group**. The difference between these two rates is slight and not statistically significant.**

### **Cribbing Taubes Alert**

In GCBC, Taubes immediately segues from discussing the Anti-Coronary Club trial to discussing the Dayton’s LA Veterans trial [[34]](https://doi.org/10.1016/S0140-6736(70)90868-8)[[35]](https://doi.org/10.1016/s0140-6736(71)91086-5)[[36]](https://doi.org/10.3945/ajcn.110.006643). Strangely enough Teicholz does the exact same thing.

GCBC, page 37:

> In July 1969, Seymour Dayton, a professor of medicine at the University of California, Los Angeles, reported the results of the largest diet-heart trial to that date. Dayton gave half of nearly 850 veterans residing at a local Veterans Administration hospital a diet in which corn, soybean, safflower, and cottonseed oils replaced the saturated fats in butter, milk, ice cream, and cheeses. The other half, the controls, were served a placebo diet in which the fat quantity and type hadn’t been changed. The first group saw their cholesterol drop 13 percent lower than the controls […]

BFS, page 75:

> It was conducted by UCLA professor of medicine Seymour Dayton on nearly 850 elderly men living in a local Veterans Administration (VA) home in the 1960s. For six years, Dayton fed half the men a diet in which corn, soybean, safflower, and cottonseed oils replaced the saturated fats in butter, milk, ice cream, and cheese. The other half of the men acted as controls and ate regular foods. The first group saw their cholesterol levels drop almost 13 percent more than did the controls.

Is it just me or do those paragraphs sound very similar?

GCBC, page 37:

> “Was it not possible,” Dayton asked, “that a diet high in unsaturated fat…might have noxious effects when consumed over a period of many years? Such diets are, after all, rarities […]”

BFS, page 75:

> “Was it not possible,” he asked, “that a diet high in unsaturated fat. . . might have noxious effects when consumed over a period of many years? Such diets are, after all, rarities.”

Both Taubes and Teicholz introduce the author and gives a brief background of the trial, then relate the conditions and methods of the study, then they cherry-pick from the results. They both then interpret the results for you. ONLY AFTER ALL THAT do Teicholz and Taubes reproduce a couple sentences from the journal article questioning whether a diet of unsaturated fat might have “noxious effects” presumably because of the study results. What they likely want the reader to think is that after the results of the study are in and the numbers have been crunched and the data has been analyzed Dr. Seymour Dayton is sitting at his desk and ruminating on what could have produced these results. As if he is asking a rhetorical question or providing a hypothesis for a future dietary trial.

In reality Dayton actually asks that question in the beginning of the paper to kind of whet the reader’s appetite. He then goes on to answer that very question in the text with an answer that would not be favorable to Teicholz’s (or Taubes’s) argument. Do you want to know if the experimental diet has noxious effects? Well there’s a section in the results portion of the study titled “Does the Experimental Diet Have Noxious Effects?” where Dr. Dayton states34:

> As indicated in table 29 and discussed in some detail above, the excess mortality in nonatherosclerotic categories was not sufficiently impressive to justify the conclusion that harmful effects had been demonstrated.

AND

> One may also wonder whether the experimental diet may have exerted its effect on mortality data primarily by accelerating nonatherosclerotic deaths (see table 28), decreasing the atherosclerotic mortality by inducing early death due to other cause. Such a mode of action would be associated with higher numbers of deaths in the experimental group compared with the controls, whereas the reverse was true in this trial (fig. 13).

AND

> The other observation which raised some question of a possible toxic effect was the low arachidonic acid concentrations in atheromata of long-term, high-adherence subjects on the experimental diet (tables 37 to 40). For reasons already cited, this may be more appropriately viewed as evidence of a salutary rather than a toxic effect.

Teicholz both 1) Uses the exact same quote Taubes does in GCBC, phrases it the exact same way, and removes the same exact words from within the quote; and 2) Takes the quote out of context just like Taubes does in order to imply something antithetical to what Dayton actually meant.

Moreover, both Taubes and Teicholz either minimize or outright ignore results of the study that they do not like (ironically, a trait they accuse the big, bad nutrition researchers of doing). Remember the _control_ group was **high in saturated animal fat**, and the _experimental_ group was **high in unsaturated fats from plants**.

> The number of men sustaining events in major categories, in the control and experimental groups, respectively, was: definite silent myocardial infarction, 4 and 9; definite overt myocardial infarction, 40 and 27; sudden death due to coronary heart disease, 27 and 18; definite cerebral infarction, 22 and 13. The difference in the primary end point of the study-sudden death or myocardial infarction was not statistically significant. However, when these data were pooled with those for cerebral infarction and other secondary end points, the totals were 96 in the control group and 66 in the experimental group; _P_ = 0.01. Fatal atherosclerotic events numbered 70 in the control group and 48 in the experimental group; _P_ < 0.05. Life-table analysis in general confirmed these conclusions. For all primary and secondary end points combined, eight year incidence rates were 47.7% and 31.3% for the control and experimental groups, respectively; _P_ value for the difference between the two incidence curves was 0.02.

If you don’t want to read the above block quote, I’ll summarize it for you: in all but one endpoint that was measured **the experimental diet of unsaturated fats had less overt myocardial infarction, sudden death, cerebral infarction, fatal ASCVD events, etc.** And not by a tiny margin – a significant margin.

Page 75:

> [V]egetable oils had been introduced into the food supply only in the 1920s, yet suddenly the oils were being recommended as a cure-all. In fact, the upward curve of vegetable oil consumption happened to coincide _perfectly_ with the rising tide of heart disease in the first half of the twentieth century […]

Not true. At least not true by the study she cites [[37]](https://doi.org/10.1161/01.CIR.102.25.3137). The study by Blasbalg et al simply shows the trends of fatty acid consumption from various sources. Specifically, it shows from which foods Americans have been getting their linoleic acid and alpha linolenic acid. It has absolutely zero analysis of heart disease or any other disease for that matter. Nor does she cite a separate paper that shows trends in heart disease to compare the paper on fatty acid consumption.

Just for kicks, let’s do Teicholz’s work for her. Let’s start with the Blasbalg paper. It appears that in the 20th century butter and lard dropped precipitously at about mid-century. Shortly afterward poultry and shortening consumption rose. Soybean oil also rose concurrently with shortening probably because it was a prominent ingredient. Canola oil consumption also increased in the 90s.

What about heart disease? According to a paper by Cooper et al CVD rose until about mid-century, but then begins a steady decline into the millennium [[38]](https://www.academia.edu/98361249/Serum_lipoprotein_levels_in_patients_with_cancer).

You have to ask yourself: does the vegetable oil correlate _perfectly_ with CVD?

### Cribbing Taubes Alert.

In GCBC, after discussing the LA Veterans trial, Taubes moves immediately to discuss the Helsinki Mental Hospital Study. Strangely enough Teicholz does the exact same thing! What are the odds that they would both independently discuss the same trials _in the very same order!_

GCBC, page 37:

> Ordinary milk was replaced with an emulsion of soybean oil in skim milk, and butter and ordinary margarine were replaced with a margarine made of polyunsaturated fats. These changes alone supposedly increased the ratio of polyunsaturated to saturated fats sixfold.

BFS, pages 76-77:

> Ordinary milk was replaced with an emulsion of soybean oil in skim milk, and butter was replaced by a special margarine high in polyunsaturated fats. The vegetable oil content of the special diet was six times higher than in a normal diet.

Page 86:

> [R]emarkably, when Jerry Stamler reissued his 1963 book. _Your Heart Has Nine Lives_, it was published as a “professional” red leather edition by the Corn Products Company and distributed free of charge to thousands of doctors. Inside, Stamler thanks both that company and the Wesson Fund for Medical Research for “significant” research support.

The very same point was also made by Taubes in GCBC, but that’s not my point here. What I’d like to say about this is that there is roughly half a page in the book that lists people and organizations that have lent financial support to the research in the book. Taubes and Teicholz, however, only list the vegetable oil manufacturers.

However, neither list the National Dairy Council which is also named among the research supporters. The reason for leaving out organizations like the NDC should be pretty obvious by now. But let me spell it out for you just in case you’re confused: Both Teicholz and Taubes are attempting to craft a narrative where Big Vegetable Oil and greedy nutrition researchers are in cahoots with each other (and also the government) to dupe the American consumer into eating less butter and cheese. If Teicholz or Taubes were to mention that the National Dairy Council funded the same research, well, then that conspiracy narrative would be weakened.

On pages 94-95, Teicholz makes the case that low cholesterol is associated with cancer in some studies, and strongly implies that low cholesterol might _cause_ cancer:

> By 1981, nearly a dozen sizable studies on humans had found a link between lowering cholesterol and cancer, principally for colon cancer.

This is kind of nit-picky, but notice how Teicholz uses the words “_lowering_ cholesterol” and not “low cholesterol.” This implies that the act of lowering cholesterol leads to cancer and not that the condition of low cholesterol is somehow linked to cancer. To the average Joe Schmo this may seem like I’m being petty and unreasonably contrarian, but if you ever take an epidemiology class you will know that how you phrase things matters a great deal.

Anyway, let’s take a look at the studies Teicholz cites…

### Cribbing Taubes Alert

The first thing to notice is that ALL of the studies she cites in favor of the link between low cholesterol and cancer are also cited by Gary Taubes in GCBC when he makes the same argument. Hmmm…

- Pearce and Dayton 1971 [[36]](https://doi.org/10.3945/ajcn.110.006643): Cited by Taubes, and as previously mentioned it is a bad study to cite in favor of this association.
    
- Nydegger and Butler 1970 [[39]](https://doi.org/10.1136/hrt.40.10.1069): Cited by Taubes. Does show a link between cancer and low lipoprotein levels. However, the authors point out this is likely due to some cholesterol-lowering effect of cancer and not the other way around, since people with chronically low cholesterol levels do not show an increased incidence of cancer.
    
- Oliver et al 1978 [[40]](https://doi.org/10.1136/bmj.280.6210.285): Cited by Taubes. The high-cholesterol group had a lower cancer rate than the two low-cholesterol groups, but it was not significant. From the paper: “These figures are surprisingly close to the rates observed in trial subjects in Groups I and III. Thus the data for all cancer do not give rise to special concern.”
    
- Beaglehole et al. 1980 [[41]](https://doi.org/10.1016/0021-9681(80)90026-0): Cited by Taubes. Shows a significant inverse relationship.
    
- Kark et al. 1980 [[42]](https://doi.org/10.1093/oxfordjournals.aje.a113171): Cited by Taubes. Also shows an inverse relationship between low cholesterol and cancer, but the authors suggest that the cancer is not likely a result of low cholesterol. From the paper: Were high cholesterol levels associated with improved survival, one would expect that those prevalent cases surviving until 1974 as well as live incident cases (surviving until 1974) would also have high cholesterols. The reverse was true.
    
- Garcia-Palmieri et al. 1981 [[43]](https://pubmed.ncbi.nlm.nih.gov/2025146/): Cited by Taubes. Shows a significant inverse relationship.
    
- Stemmermann et al. 1981 [[44]](https://doi.org/10.1093/jnci/67.2.297): Cited by Taubes. Shows a significant inverse relationship with colon cancer, but also shows a positive relationship with CHD but that, of course, is never mentioned.
    
- Miller et al. 1981 [[45]](https://doi.org/10.1093/oxfordjournals.aje.a113170): Cited by Taubes. Shows a significant inverse relationship, but makes it clear that suggesting low cholesterol might cause cancer is almost certainly wrong: “Although we found colon cancer patients to have significantly lower serum cholesterol levels than controls, the observed differences may partially reflect the metabolic influence of advancing disease, since there were no significant differences in serum cholesterol levels between controls and cases with early tumors. Our data suggest that low serum cholesterol levels in colon cancer patients do not necessarily precede tumor formation but may be a consequence thereof.”
    
- Kozarevic et al. 1981 [[46]](https://doi.org/10.1016/S0140-6736(74)92492-1): Cited by Taubes. Shows a non-significant relationship. Also mentions the following that is never discussed: “Serum cholesterol, as expected, was positively related to the incidence of coronary heart disease death.”
    
- Rose et al. 1974 [[47]](https://doi.org/10.1001/jama.1981.03310280023021): Cited by Taubes. Shows a significant inverse relationship with colon cancer. Again, the authors suggest that low-cholesterol might be a result of colon cancer. And again, this is not mentioned.
    
- Williams et al. 1981 [[48]](https://www.semanticscholar.org/paper/Effects-of-dietary-fat-on-the-uptake-and-clearance-Gammal-Carroll/c8c4012f8544114648842f1af6898b2ed197b0b7): Cited by Taubes. Shows a significant inverse relationship with colon cancer, but also mentions that it is possible that cancer of the colon can affect cholesterol absorption and excretion leading to low serum levels. I’m shocked this was not mentioned. Shocked.
    

Page 94:

> [E]ver since corn oil had been shown to double the rate of tumor growth in rats in 1968, there had been a baseline level of concern about vegetable oils and cancer.

There has? I don’t think so. But anyway, the study Teicholz cites is waaayy off the mark [[49]](https://pubmed.ncbi.nlm.nih.gov/5922675/). For some brief detail some rats were fed either a low-fat diet, a diet high in coconut oil, or a diet high in corn oil. Then they were injected with a carcinogen known to cause breast cancer. Turns that the rates of uptake and clearance of the carcinogen was equal on all three diets.

Page 94:

> Other studies from this time led to the supposition that corn oil might cause cirrhosis of the liver.

As evidence she cites a very obscure study on rats that were fed a diet _explicitly designed to induce cirrhosis_ where some were also supplemented with corn oil. The researchers found that the corn oil did not exert a protective effect [[50]](https://doi.org/10.1016/0091-7435(79)90033-1). Not preventing cancer DOES NOT EQUAL CANCER-PROMOTION. This is possibly the most twisted and misleading claim Teicholz has made so far in the chapter.

Page 94:

> NIH investigators found that Japanese people with cholesterol levels below 180 mg/dL suffered strokes at rates two to three times higher than those with higher cholesterol.

The cited text does state that the people with the lowest cholesterol did have the highest incidence of stroke [[51]](https://doi.org/10.1016/0091-7435(82)90059-7), but I want to note a few things. First, this is not a study, but a letter to the editor. Second, these were not NIH investigators nor do I think it had anything to do with the NIH considering the NIH is never mentioned and the studies discussed in the letter were conducted by Japanese researchers on Japanese participants in Japan. Why would American taxpayer money fund this effort? Please correct me if I am wrong.

Page 94-95:

> The NHLBI became so concerned about the cancer findings that it hosted three workshops in 1981, 1982, and 1983. The evidence on the topic was reviewed and rereviewed by an extremely prominent group of scientists […] One suggestion was that low cholesterol might be an early symptom of cancer, rather than a cause. It was a plausible bit of logic. In the end, however, although the assembled researchers could find no convincing explanation for the cancer findings, they concluded that they did “not present a public health challenge” and did not “contradict” the more urgent, “commonsense” public health message for everyone to lower their cholesterol.

A couple of minor things, but I could not find the word “commonsense” that was quoted above in either of the Feinleib papers that were cited [[52]](https://doi.org/10.1093/oxfordjournals.aje.a113173)[[53]](https://doi.org/10.1158/0008-5472.CAN-10-2953). Moreover, as I recall the public health message for everyone to lower their cholesterol never even existed. The message was/is for those with high cholesterol to lower their cholesterol. There’s a difference.

The scientists involved included nearly all of the above authors of the scary observational studies that indicated a link between low serum cholesterol and cancer.52 Furthermore, the consensus of the panelists was unanimous in that there was not nearly enough evidence to suggest that lowering cholesterol is a risky behavior.

If Teicholz was not an extraordinarily biased journalist, and she wanted to write BFS with a modicum of honesty she could have easily included some more recent studies that show no association with cholesterol-lowering and an increased risk of cancer [[54]](https://doi.org/10.1074/jbc.M110.184010)[[55]](https://doi.org/10.1093/annonc/mdr155)[[56]](https://doi.org/10.1136/gut.2009.190900)[[57]](https://doi.org/10.1136/bmj.308.6925.373)[[58]](https://doi.org/10.1056/NEJM198101083040201). Some even indicate a protective effect of low serum cholesterol on cancer. But when has the truth ever been able to move books?

Page 95:

> When I mentioned all this to Stamler, he didn’t remember any part of this cancer-cholesterol debate. In this way, he is a microcosm of a larger phenomenon that allowed the diet-heart hypothesis to move forward: inconvenient results were consistantly ignored; here again, “selection bias” was at work.

WOW… Pot. Kettle. _Black!_

### Cribbing Taubes Alert

GCBC, page 38:

> The principal investigator on the trial was Ivan Frantz, Jr., who worked in Keys’s department at the University of Minnesota. Frantz retired in 1988 and published the results a year later in a journal called _Arteriosclerosis_, which is unlikely to be read by anyone outside the field of cardiology. […] When I asked Frantz in late 2003 why the study went unpublished for sixteen years, he said, “We were just disappointed in the way it came out.”

BFS, page 96:

> Frantz, who worked in Keys’s university department, did not publish the study for sixteen years, until after he retired, and then he placed his results in the journal _Arteriosclerosis, Thrombosis, and Vascular Biology_, which is unlikely to be read by anyone outside the field of cardiology. When asked why he did not publish the results earlier, Frantz replied that he didn’t think he’d done anything wrong in the study. “We were just disappointed in the way it came out,” he said.

At least Teicholz cites Taubes as the source of the Frantz quote.

On page 97, Teicholz discusses the Western Electric study:

> But the results, after twenty years of study, actually showed that diet affected blood cholesterol only a tiny bit and that the “amount of saturated fatty acids in the diet was not significantly associated with risk of death from CHD [coronary heart disease],” as the authors wrote.

Strangely enough, on page 29 of GCBC Taubes discusses the very same study and quotes the very same line. Whooda thunk? Well, by now everyone shoulda thunk.

Anyway, it is true that Western Electric found only non-significant relationships between saturated fat and CHD mortality [[59]](https://doi.org/10.1093/ajcn/26.2.177). But yet again, evidence that runs contrary to the overall thesis is left out. From the paragraph _immediately before_ the saturated fat quote (emphasis mine):

> When the risk of death from CHD was analyzed in terms of the component dietary variables, it was **inversely related to intake of polyunsaturated fatty acids and positively related to intake of dietary cholesterol**.

Those evil vegetable oils that are toxic and cause all kinds of disease evidently protect from CHD death. And all that cholesterol from the butter, meat, and cheese that Teicholz wants people to eat is evidently increasing it. What was that Teicholz was saying about “selection bias”?

Page 98, Teicholz discusses a study in which participants were from either Hiroshima or Nagasaki and tries really hard to convince you to not to pay attention to the results by saying:

> The possible radiation exposure of these men to the atomic bombs dropped on their cities at the end of World War II was not factored into the analysis.

If the only participants are from areas with essentially the same amount of radiation then the results are controlled for. If one cohort had been from Osaka and the other from Nagasaki AND their diet or lifestyle was different then she would have a point, but that is clearly not the case. Not to mention the lead author on the publication in question is a STATISTICIAN working in Japan’s ATOMIC BOMB CASUALTY COMMISSION. Ladies and gentlemen, this is what you call a hail mary.

On page 99, Teicholz discusses the results of a large cohort study she refers to as the Ni-Hon-San. The results indicate that a diet high in saturated fat increases risk of pretty much “all manifestations of CHD” and that you might do well to eat less of it. Of course Teicholz will have none of this so she again scours the publications to find a molehill to portray as a mountain:

> So I dug up the paper on NiHonSan’s diet methodology, published two years earlier. It seems that the team in the San Francisco Bay Area had completely fallen down on the job. Not only did they get diet information from only 267 men, compared to the 2,275 interviewed in Japan and a whopping 7,963 in Honolulu, but they had done these interviews only one time and in only one way (a twenty-four-hour recall questionnaire), whereas the other two teams had assessed diet on two different occasions, several years apart, and in four different ways; this was clearly not the “same method” that the authors claimed. Yet these issues were never mentioned […]

These issues are “never mentioned” except where they are explicitly mentioned… in a published article… by the most widely-read nutrition journal… that is completely free and does not require any kind of subscription to access… which is where Teicholz found them.

A few things:

- Teicholz seems to think that because CA completed less diet records that somehow invalidates the results.
    
- California did more than just the 24-hour recall, according to the methods paper [[60]](https://books.google.ca/books/about/Honolulu_Heart_Program.html?id=hcT0HzpKe28C&redir_esc=y). They also did a 7-day food record and a food acculturation questionnaire.
    
- Teicholz assumes incompetence by the CA researchers, when in fact it was funding issues. It is described in detail in the book _Honolulu Heart Program_ [[61]](https://doi.org/10.2302/kjm.61.79). Books are often compiled from large cohort studies like these (e.g. The Seven Countries Study and The China-Cornell-Oxford Project). Investigative journalism, anyone?
    

Page 100:

> [T]he Japanese have recently been eating far more meat, eggs, and dairy than they used to since the end of World War II, rates of heart disease have dropped to levels seen by Keys in the 1950s. This means that although the story of diet and disease in Japan is complex, we can pretty well say that based on this trend alone, a diet low in saturated fat was not the factor that spared the Japanese from heart disease in the postwar years.

That’s a bold claim for which Teicholz cites a review article not on heart disease, but on stroke – a particular subset of heart disease [[62]](https://doi.org/10.1194/jlr.R400012-JLR200). Additionally, the article never even mentions meat or even fat consumption. It does mention cholesterol as a risk factor, though. Perhaps Teicholz should have read the conclusion:

> The atherogenic effect of hypercholesterolemia is well established and is based on evidence from numerous epidemiological, pathological, and biological studies. Furthermore, the proportion of atherothrombotic cerebral infarctions may have recently increased in Japan, because this subtype currently accounts for approximately one third of cerebral infarctions in the Japan Standard Stroke Registry Study (JSSRS). We should formulate a confirmed strategy for lipid management to prevent cerebral infarction.

Teicholz cites no evidence for the increased meat consumption, but no matter. I know how to Google. It seems that meat consumption has increased in Japan over the years, although it still pales in comparison to US consumption which is probably why Japanese heart disease remains comparatively low.

Source [here](http://karlandersson.se/2012/11/07/japan-vs-the-west-meat-consumption/) and [here](http://www.theguardian.com/environment/datablog/2009/sep/02/meat-consumption-per-capita-climate-change).

Page 101:

> Seymour Dayton was concerned about the extremely low levels of arachidonic acid, an essential fatty acid present mainly in animal foods, among his prudent dieters.

A few things: 1) Arachidonic acid is one of those evil polyunsaturates, 2) Arachidonic acid is not an EFA, and 3) Here’s what Dayton said about AA on the exact page from the exact paper Teicholz cites [[34]](https://doi.org/10.1016/S0140-6736(70)90868-8) (emphasis mine)

> The other observation which raised some question of a possible toxic effect was the low arachidonic acid concentrations in atheromata of long-term, high-adherence subjects on the experimental diet (tables 37 to 40). **For reasons already cited, this may be more appropriately viewed as evidence of a salutary rather than a toxic effect.**

Page 101-102:

> In the United States, Pete Ahrens, who was still the prudent diet’s most prominent critic, continued to publish his central point of caution: the diet-heart hypothesis “is still a hypothesis … I sincerely believe we should not. . . make broadscale recommendations on diets and drugs to the general public now.”

A few things:

1. Ahrens is not referring to the diet-heart hypothesis in this text, he is referring to the lipid hypothesis. There is a difference. Teicholz actually clips the words “lipid hypothesis” from one of the quotes: “The Lipid Hypothesis is still a hypothesis.” In fact, he never even mentions the diet-heart hypothesis in the text.
    
2. Ahrens does write those words, but all three phrases are out of order. As a journalist, are you allowed to do that? Take bits of text and arrange them however, as long as you put ellipses in there? If they were to go in order of appearance it would be “[…] make broadscale recommendations on diet and drugs to the general public now […] I sincerely believe we should not […] is still a hypothesis […]” By the way there is about one and a half paragraphs between the last two quotes.
    
3. This is another example of quote-mining by Teicholz, because Ahrens actually says some not-so-bad things about the Lipid hypothesis in the text and cholesterol-lowering in general. Two can play the quote-mining game. For instance:
    

> [When asked whether we should abandon the Lipid Hypothesis] My reply to the last question is “no”: I submit that the Lipid Hypothesis has never really been put to an adequate test, and that therefore we cannot conclude that the premise is false.

AND

> [I]t seems entirely logical, indeed essential, for internists to screen routinely for hyperlipidemia […] If hyperlipidemia persists, a full year’s evaluation should be made of a low-cholesterol, low-saturated, high polyunsaturated-fat diet, with moderation in alcohol intake.

AND

> The Lipid Hypothesis is still a hypothesis. I have tried to show that it is a viable one, and how in the future we may better put it to test.

### Cribbing Taubes Alert

On page 22 of GCBC Taubes states the following:

> The resulting literature very quickly grew to what one Columbia University pathologist in 1977 described as “unmanageable proportions.”

On page 102 of BFS Teicholz writes:

> By the late 1970s, however, the number of scientific studies had grown to such “unmanageable proportions,” as one Columbia University pathologist put it, that it was overwhelming.

This quote is from _Dietary Goals for the United States—Supplemental Views_ publication which is an 881-page text, at least in PDF format. What are the odds that Teicholz independently arrived at “unmanageable proportions” in that enormous publication? Do you think this is original research?

Page 102:

> The ambiguities inherent to nutrition studies opened the door for their interpretation to be influenced by bias— which hardened into a kind of faith. There were simply “believers” and “nonbelievers,” according to cholesterol expert Daniel Steinberg.

Interestingly enough, this person that Teicholz calls a “cholesterol expert” would almost certainly disagree with the entire thesis of BFS, since he appears to accept that high cholesterol plays a role in heart disease, and that serum cholesterol can be controlled to some degree via the diet. This is made pretty clear if you read the publication Teicholz cites for this [[63]](https://doi.org/10.1093/ajcn/32.12.2627). In fact, Ahrens himself (whom Teicholz describes above as the biggest critic of the diet-heart hypothesis – although she confuses it with the lipid hypothesis) was, according to Steinberg, one of the first to conduct “the definitive demonstration that saturated fats tend to raise while polyunsaturated fats tend to lower blood cholesterol in humans […]” Steinberg’s entire review series on the pathogenesis of ASCVD is both quite interesting and readable. I highly suggest reading them if you are interested in the topic. They are open access papers, but if you didn’t want to go to the trouble of finding them [here is a link to the series](http://ge.tt/9VafyxZ?c).

## Chapter 5: The Low-Fat Diet Goes to Washington

### Cribbing Taubes Alert

BFS, page 112:

> [W]hen Senator McGovern announced his Senate committee’s report, called Dietary Goals, at a press conference in 1977, he expressed a gloomy outlook about where the American diet was heading. “Our diets have changed radically within the past fifty years,” he explained, “with great and often harmful effects on our health.”

The problem here is that Teicholz cites the source of this quote as “Select Committee on Nutrition and Human Needs of the United States Senate, _Dietary Goals for the United States_ (Washington, DC: US Government Printing Office, 1977); 1.” However, this quote does not appear on page 1. It appears on page XIII. Normally I would chalk this up to a simple citation error [[64]](https://doi.org/10.1126/science.451563). The reason I mention it is because Taubes uses the same exact quote on page 10 of GCBC, and also mistakenly cites the source of the quote as being on page 1. I would argue (as I have done previously many times) that this is good evidence that Teicholz is simply lifting sentences from others and simply citing what they cite – likely without ever even seeing the source material.

### Cribbing Taubes Alert

Page 112:

> The _New York Times_ health columnist Jane Brody perfectly encapsulated this idea when she wrote, “Within this century, the diet of the average American has undergone a radical shift away from plant-based foods such as grains, beans and peas, nuts, potatoes, and other vegetables and fruits and toward foods derived from animals—meat, fish, poultry, eggs and dairy products.”

This Brody quote appears word-for-word on page 10 of GCBC. It’s from a book Brody published 30 years ago. Isn’t it astonishing that both Taubes and Teicholz can do completely independent research, find the exact same publications, and use the same quotes from those publications?

Page 122:

> Ahrens chose a nine-member task force representing the full range of scientific views on the diet-heart hypothesis. The panel deliberated for several months over each link in the chain of the diet-heart hypothesis, from eating saturated fat, to total cholesterol, to heart disease. The results, however, were not exactly welcome news to diet-heart supporters […] The final report from the Ahrens task force in 1979 made it clear that the majority of its members remained highly skeptical of the idea that reducing fat or saturated fat could deter coronary disease.

Actually, the task force didn’t make that clear at all.65 I actually blogged about this not too long ago because Taubes cites the same obscure paper and comes to a similar but still erroneous conclusion.

The paper gives a score of 0-100 to associations between a given dietary issue and atherosclerosis, where 0 is the weakest evidence for the association and 100 is the most rock-solid evidence. The final score is an aggregation of scores by several experts in the field based on epidemiological evidence, animal studies, human interventions, autopsies, biological plausibility, etc. Cholesterol alone received a score of 62. Saturated fat alone received a 58. Cholesterol and fat together received a 73. For comparison the association between alcohol and liver disease received an 88, and the association between carbohydrate and atherosclerosis got an 11. Carbohydrate and diabetes got a 13.

I don’t want to tell you how you should interpret that data, but it seems pretty clear to me that the evidence that cholesterol and fat play a role in ASCVD is quite strong: well above the halfway point and approaching the level of alcohol and liver disease. Teicholz, however, tells her readers that the committee was “highly skeptical” for reasons that should be pretty clear by now.

### Cribbing Taubes Alert

On pages 56-59 of GCBC Taubes discusses lipid trial and a consensus conference. On page 127-134 Teicholz discusses the same trial and conference in a strikingly similar way.

GCBC page 56:

> The second trial was the $150 million Lipid Research Clinics (LRC) Coronary Primary Prevention Trial. The trial was led by Basil Rifkind of the NHLBI and Daniel Steinberg, a specialist on cholesterol disorders at the University of California, San Diego. The LRC investigators had screened nearly half a million middle-aged men and found thirty-eight hundred who had no overt signs of heart disease but cholesterol levels sufficiently high-more than 265 mg/dl-that they could be considered imminently likely to suffer a heart attack.

BFS page 127:

> The other trial was the $150 million Lipid Research Clinic Coronary Primary Prevention Trial (LRC) […] LRC was led by Basil Rifkind, chief of NHLBl’s Lipid Metabolism Branch, together with Daniel Steinberg, a cholesterol specialist at the University of California, San Diego. They screened nearly half a million middle-aged men and found 3,800 with levels of cholesterol high enough (265 mg/dL or above) to be considered likely to have a heart attack soon […]

GCBC page 57:

> To call these results “conclusive,” as the University of Chicago biostatistician Paul Meier remarked, would constitute “a substantial misuse of the term.”

BFS page 130:

> The biostatistician Paul Meier commented that to call the results “conclusive” would constitute “a substantial misuse of the term.”

GCBC page 57:

> As Rifkind told _Time_ magazine, “It is now indisputable that lowering cholesterol with diet and drugs can actually cut the risk of developing heart disease and having a heart attack.”

BFS page 130:

> […] Rifkind told Time magazine, “It is now indisputable that lowering cholesterol with diet and drugs can actually cut the risk of developing heart disease and having a heart attack.”

## Chapter 6: How Women and Children Fare on a Low-Fat Diet

Page 136

> Jerry Stamler expressed in 1972, that fat was “excessive in calories… so that obesity develops.” This seemingly obvious but nonetheless unproven assumption was that fat made you fat.

If you actually read the cited paper you will note that Stamler was not referring to fat when he said that, he was instead referring to the “habitual diet of Americans” [[65]](https://doi.org/10.1016/0091-7435(72)90076-x). Stamler’s quote is taken out of context and put into a different context that is inaccurate.

### Cribbing Taubes Alert

Teicholz seemingly copy-pastes Taubes work on page 136 when she discusses an amazingly obscure 1995 pamphlet by the American Heart Association. According to Teicholz the pamphlet:

> told Americans to control their fat intake by increasing refined-carbohydrate consumption. Choose “snacks from other food groups such as . . . low-fat cookies, low-fat crackers, . . . unsalted pretzels, hard candy, gum drops, sugar, syrup, honey, jam, jelly, marmalade” […]

Leaving aside the fact that the pamphlet never actually advises anyone to increase their refined-carbohydrate consumption, what’s interesting is that Taubes says the very same thing in chapter sixteen of WWGF:

> [T]he AHA counseled, “choose snacks from other food groups such as … low-fat cookies, low-fat crackers … unsalted pretzels, hard candy, gum drops, sugar, syrup, honey, jam, jelly, marmalade (as spreads).”

The ellipses are even in the same place! What a coinkydink.

She goes on to say:

> In short, to avoid fat, people should eat sugar, the AHA advised.

No. That’s a gross and highly misleading oversimplification. There’s no way you can distill 20 pages of dietary advice into “eat sugar” and think you’re even approaching a decent approximation.

Page 138:

> Jane Brody, the health columnist for the New York Times and the most influential promoter of the low-fat diet in the press, wrote, “If there’s one nutrient that has the decks stacked against it, it’s fat” […]

That quote does not appear in the cited NYT article [[66]](https://www.nytimes.com/1980/02/05/archives/tending-to-obesity-inbred-tribe-aids-diabetes-study-inbred-tribe.html).

On page 143 Teicholz attempts to make the argument that plant-based diets aren’t all they’re cracked up to be:

> Vegetarian diets generally have not been shown to help people live longer. The 2007 report by the World Cancer Research Fund and the American Institute for Cancer Research, discussed in the last chapter, found that in no case was the evidence for the consumption of fruits and vegetables in the prevention of cancer “judged to be convincing.”

This has got to be one of the most selective and misleading interpretations of the Second Expert’s Report. I mean, my god… So for those who are not familiar with it, the Second Expert’s Report (what Teicholz discusses above) is an incredible text that outlines ALL of the quality evidence on a given cancer topic regarding food, nutrition, and the prevention of cancer. All the evidence is evaluated by a large panel of elite doctors and researchers and given one of the following labels: unlikely, suggestive, probable, and convincing. To reach the **convincing** level you really need some rock solid evidence [[67]](https://doi.org/10.1016/s0899-9007(99)00021-0). None of the evidence regarding fruits and vegetables reached the venerable **convincing** level, but there was a ton of **probable** and **suggestive** evidence for many cancers. You really have to scour that section to find a quote that might fit an anti-vegetable narrative. Read it yourself for all the juicy details, but here are some remarks from the conclusion on page 1144:

> Non-starchy vegetables probably protect against cancers of the mouth, pharynx, and larynx, and those of the oesophagus and stomach. There is limited evidence suggesting that they also protect against cancers of the nasopharynx, lung, colorectum, ovary, and endometrium. Allium vegetables probably protect against stomach cancer. Garlic (an allium vegetable, commonly classed as a herb) probably protects against colorectal cancer. Fruits in general probably protect against cancers of the mouth, pharynx, and larynx, and of the oesophagus, lung, and stomach. There is limited evidence suggesting that fruits also protect against cancers of the nasopharynx, pancreas, liver, and colorectum.

On page 144 Teicholz derides the study that low-carbers love to hate, T. Colin Campbell’s China Study [[68]](https://www.westonaprice.org/book-reviews/the-china-study-by-t-colin-campbell/):

> These books are based on one epidemiological study, with a number of significant methodological problems, that was never published in a peer-reviewed issue of a scientific journal. Campbell’s two papers were instead published as part of conference proceedings in journal “supplements,” which are subject to little or no peer review.

As evidence she cites a BLOG POST by then-undergraduate Chris Masterjohn, published by a private organization that is explicitly biased in favor of animal-based diets [[69]](https://www.westonaprice.org/book-reviews/the-china-study-by-t-colin-campbell/). Just let that sink in…

After discussing the Ornish diet for a bit, Teicholz mentions a paper on page 145 that reviews the evidence for (very) low-fat diets:

> Tufts University nutrition professor Alice Lichtenstein and a colleague reviewed the very low-fat diet for the AHA […] Lichtenstein concluded that very low-fat diets “are not beneficial and may be harmful.”

Teicholz both takes the quote out of context and mangles it somewhat. Here’s the actual quote (emphasis mine):

> At this time, no health benefits and possible harmful effects can be predicted from adherence to very low fat diets **in certain subgroups**.

The “in certain subgroups” part is vital to the accuracy of the statement. You can’t just cut it out. And Dr. Lichtenstein’s conclusion is quite a bit more nuanced than Teicholz would have you believe. The paper is actually a very objective look at low-fat diets [[70]](https://doi.org/10.1161/01.CIR.98.9.935). It acknowledges that more research needs to be done on these diets in order for a definitive recommendation. Moreover, as alluded to above, it states that until more evidence comes in young children, the elderly, pregnant women, and those with eating disorders should probably avoid the diet. However, it also acknowledges that a low fat diet can be beneficial and there is evidence for that. Here’s an actual quote from the conclusion:

> There is overwhelming evidence that reductions in saturated fat, dietary cholesterol, and weight offer the most effective dietary strategies for reducing total cholesterol, LDL-C levels, and cardiovascular risk.

On page 146 Teicholz discusses the idea of reducing cholesterol levels In young children:

> Indeed, in the late 1960s, the NHLBI had been putting children as young as four years old on cholesterol-lowering diets and also giving them cholestyramine, the same drug that would be used in the LRC trial. Convinced that cholesterol was a crucial part of the heart disease puzzle, the NHLBI went so far as to propose universal umbilical cord blood screening in order to start treatment as early as possible, even at birth. In 1970, mass screening of cord blood at “no more than” five dollars per baby was given serious consideration. Such was the preoccupation with heart disease that researchers believed healthy children ought to start out life in a position of defense.

_These people are obsessed with cholesterol! Won’t somebody pleeease think of the children??_ Of course to the average person reading this it sounds absurd, and it would be absurd if Teicholz had not removed a key bit of information that makes it very not absurd. According to the JAMA article she cited for this paragraph, the NHLBI was not proposing we give all or even most kids cholesterol-lowering drugs or cholesterol screenings [[71]](http://archive.org/details/sim_jama_1970_214_index). The article makes it abundantly clear that these are for children with familial type 2 hypercholesterolemia, a genetic condition associated with chronically high cholesterol levels. People with familial hypercholesterolemia have heart attacks and can die far younger than normal lipid folks. It makes perfect sense to go on a cholesterol-lowering diet or a statin if you have that condition. But of course, Teicholz wants to stoke your outrage a bit so you think the NHLBI are a bunch of incompetent boobs that are completely absorbed (pardon the pun) with the idea of cholesterol.

Teicholz continues to make the argument against vegetarianism on page 148:

> One of the more important early nutrition researchers looking at these questions was Elmer V. McCollum, an influential biochemist at Johns Hopkins University. He performed endless feeding studies on rats and pigs because they, like humans, are omnivores and are therefore considered instructive for human nutritional needs. His book The Newer Knowledge of Nutrition (1921) is populated with pictures of scrawny, scruffy-furred rats raised on poor nutrition, compared to large, lustrously furred ones raised on better nutrition. He found that animals on a vegetarian diet had an especially difficult time reproducing and rearing their young.

She goes on to quote a passage from the book with the author explaining that the vegetarian rats were smaller and lived about half as long as the non-vegetarians. While I was looking for the quoted passage I noticed that basically the entire book is a discussion of rats on deprivation diets of some sort; rat experiments where the researchers deprive them of one or more essential amino acids, vitamins, and breast milk at birth to see what happens [[72]](http://archive.org/details/newerknowledgeof00mcco_1). This is what the pics of “scrawny, scruffy-furred rats” she refers to are; not necessarily vegetarian rats.

Moreover, I’m not sure that rats make a great proxy for humans. At least I would not be convinced to change my diet based only on rat studies. At any rate, much like her scholarship on the Second Expert’s Report, to find a quote that puts vegetarianism in a negative light you have to ignore paragraphs immediately preceding and immediately following her quoted passage. I’ll give you an idea of what I’m talking about. On page 1599:

> The more valid arguments concerning the use of meat as contrasted with the fleshless dietary regimen, are based upon the view that meat is unwholesome and that it contains waste products, which, because of their poisonous properties, tend to do damage to the body tissues. This view is upheld by experimental results. Professor Irving Fisher of Yale conducted extensive experiments with flesh abstainers and flesh eaters, and found the former possessed of much greater endurance than the latter.

After discussing the experiment where the rats’ growth were stunted on a vegetarian diet, McCollum goes on to describe a different vegetarian diet that was more successful. Page 161:

> The diet induced growth at approximately the normal rate, and reproduction and rearing of a considerable number of young. The young grew up to the full adult size and were successful in rearing a considerable number of their offspring (see Chart 7).

McCollum then describes another successful experiment where rats were fed _another_ iteration of a vegetarian diet and thrived. He even explains why the diet that Teicholz mentioned did not permit the rats to thrive. Page 164:

> [T]he diet was of such a nature that the animals could hardly do otherwise than take a rather low protein intake. Secondly, the leaves, which formed the only components of the food supply containing enough mineral elements to support growth, were fed in the fresh condition. In this form the water content and bulk are so great that it would be practically impossible for an animal whose digestive apparatus is no more capacious than that of an omnivorous rat to eat a sufficient amount of leaf to correct the inorganic deficiencies of the rest of the mixture […]

Ladies and gentlemen, this is cherry-picking at its finest.

On page 152 Teicholz tries really hard to find a reason to dismiss the results of a study:

> [The NHLBI] funded a trial called the Dietary Intervention Study in Children (DISC). Starting in 1987, three hundred seven- to ten-year-old children were counseled, along with their parents, to eat a diet in which saturated fat was limited to 8 percent of calories and total fat to 28 percent, and this group was compared to an equal-sized group of controls. Investigators found that those put on the diet low in fat (and animal fat) grew just as well as the children eating normally during the three years of the experiment, and the authors emphasized this point. Yet it was problematic for the study that the boys and girls in the trial did not represent a normal sample. For their study population, the DISC leaders had selected children who had unusually high levels of LDL cholesterol (in the 80th to 98th percentile). In other words, these children could very well have had familial hypercholesterolemia, the genetic condition that causes heart disease through a metabolic defect, which is entirely different from the way that cholesterol is altered by diet. These at-risk children were chosen because they were thought to need help more urgently in fighting the early onset of a life-threatening disease, yet their unusually high cholesterol levels meant that the results could not be generalized to the larger population of normal children.

I would totally agree with Teicholz here if there was significant evidence that hypercholesterolemia would confound growth results in some way. However, it seems obvious to me that she is trying real hard to find reasons to discount the results, and if this is the best she can do then it must be a good study design. Also, the diet is nearly one-third fat. How is this a “diet low in fat”??

Teicholz hits the vegetarian diet from all possible angles, including the idea that vegetarian children show retarded growth. Page 153:

> Slightly stunted growth was consistently found among children eating vegetarian diets. Children were also found to experience growth spurts when incorporating more animal foods in their diets. Growth faltering was particularly pronounced among children on a vegan diet, which cuts out all animal foods.

As evidence she cites a twenty-two year-old review article on reduced fat diets [[73]](https://doi.org/10.1016/0091-7435(92)90004-2). Refreshingly, Teicholz doesn’t really misrepresent the article nearly as much as her other claims. However, she does leave out some important details. One important distinction that is not made by Teicholz and is only kind-of made in the review article is the content of the vegetarian diets. What I mean is that the terms vegetarian diet, low fat diet, and low calorie diet are used almost interchangeably in the text. Now a vegetarian diet can of course be low fat or low calorie or both, but it certainly does not have to be.

The author also implies that ridiculously strict vegetarian diets are also included in the review including the Zen macrobiotic diet which the author claims as consisting only of brown rice and water. He also mentions some limitations of this review stating “[N]early all of the studies focus on diets that provide undernourishment in nutrients.” I think it’s reasonable to assume that this can and almost certainly does account for the slight growth stunting and not, as Teicholz would claim, a lack of animal fat.

Similar to the DISC trial above there was also another study done to test the outcomes of a low saturated fat diet on kids called STRIP (Special Turku Coronary Risk Factor Intervention Project). Teicholz refers to it as a low fat diet, but it simply replaces saturated fat with unsaturated. Evidently there were no adverse effects to growth or coronary disease risk factors. But that doesn’t stop Teicholz from doing her damnedest to try and convince you that the study is bunk. Page 154:

> [A]lthough investigators found no vitamin deficiencies, the supplements they provided may have masked this problem. It is also significant that 20 percent of the families in both groups left before the end of the study.

Page 157:

> The British biochemist and nutrition expert Andrew M. Prentice, for instance, hypothesized that the lack of high-fat animal foods was possibly “the major contributor of growth failure” among babies he studied in Gambia. He compared some 140 Gambian infants to a slightly larger group of relatively affluent babies in Cambridge, England; early on, the Gambians and British infants grew almost equally well. When they started to be weaned off breast milk at six months of age, however, their growth curves steadily diverged. The Gambians ate an equal number of calories as did the Cambridge babies for the first eighteen months of life, but the fat content of their diet steadily declined to just 15 percent of calories by the age of two, and most of that fat was polyunsaturated from nuts and vegetable oils. The Cambridge babies, by contrast, ate a majority of calories from eggs, cow’s milk, and meat—a minimum of 37 percent of calories as fat, most of it saturated. By the age of three, the Gambian babies weighed 75 percent less than they should, according to standard growth charts, while the Cambridge babies were growing according to expectations and weighed on average 8 pounds more than the Gambians.

The cited paper does make the case that essential fatty acids are needed for proper growth, and that a lack of dietary fat might explain the stunted growth in the Gambians [[74]](https://doi.org/10.1093/ajcn/72.5.1253s). But some important details in the paper that Teicholz gets wrong or leaves out are that the Gambians did in fact consume less kcals and that Gambian children suffer from infection, disease, and diarrhea more than their Cambridge counterparts.

Page 158:

> Reports from poorer countries in Latin America and Africa, however, revealed that children were eating less fat, with clear implications for nutrition and growth: diets with less than 30 percent of calories as fat started to get nutritionally worrisome, and at 22 percent, they were associated with growth faltering.

If look at the paper she cites for this data, you will find that the poorer countries such as Haiti also consumed FAR LESS kcals than their counterparts from more developed countries. Of course this doesn’t rule out fat as a factor, but I would argue that it definitely confounds the results.

You should also note that this is a cross-sectional analysis which is extraordinarily weak in terms of showing any kind of cause-effect relationship. Teicholz makes this very argument earlier in the book, but you can see here that she has no problem invoking these types of studies when it fits her narrative.

Teicholz then tries to make the argument that HDL was ignored by the major institutions as a predictor of heart disease. Page 162:

> [W]hen diet and disease experts finally began to sidle away from total cholesterol, they did not turn to HDL-cholesterol. Instead, they chose to focus on LDL-cholesterol. By 2002, the NCEP was calling elevated LDL-cholesterol a “powerful” risk factor. The AHA and other professional associations agreed.

LDL was and is a powerful risk factor, but HDL was not ignored. The 2002 report that Teicholz cites has an entire section titled “Low HDL cholesterol as an independent risk factor for CHD.” I could quote about a hundred parts in that report that make the case that low HDL is a risk factor, but I’ll spare you. The report is open access so you can find it and peruse it if you’d like.

On page 165 Teicholz discusses the Boeing employees trial and its implication for women [[75]](https://doi.org/10.1046/j.1525-1373.2000.22524.x):

> [The Boeing women] had followed the most stringent NCEP guidelines for an entire year and had apparently increased their risk of having a heart attack.

She bases this on the study results that the women with high cholesterol levels had a 8% decrease in HDL levels. In the book she doesn’t mention that these HDL reductions only occurred in hypercholesterolemic women, instead implying that all women had their HDL levels go down. Moreover, if one’s HDL levels are at, say, 72 mg/dl and decrease 8% they are still high enough to be considered protective. High HDL levels are not unlikely for women with high cholesterol. HDL as a risk factor for heart disease only occurs when HDL levels get below 40 mg/dl.

Page 166:

> The idea that fat might lead to cancer was first aired at the McGovern committee hearings in 1976, when Gio Gori, director of the National Cancer Institute (NCI), testified that men and women in Japan had very low rates of breast and colon cancer and that those rates rose quickly upon emigrating to the United States. Gori showed charts demonstrating the parallel rising lines of fat consumption and cancer rates. “Now I want to emphasize that this is a very strong correlation, but that correlation does not mean causation,” he said. “I don’t think anybody can go out today, and say that food causes cancer.”

That’s not nearly all Gori presented, but the source of this quote, according to Teicholz is the testimony of Gori in Volume 2 of _Diet Related to Killer Diseases_ (July 28, 1976): 176-182. However, that quote is not found in those page numbers. A similar quote is found on page 185, but it doesn’t include the “I don’t think anybody can…” bit, so I don’t know where Teicholz got those page numbers or that quote.

Page 167:

> It is therefore surprising to learn that as far back as 1987, the epidemiologist Walter Willett at the Harvard School of Public Health had found fat consumption not to be positively linked to breast cancer among the nearly ninety thousand nurses whom he had been following for five years in the Nurses’ Health Study. In fact, Willett found just the opposite to be true, namely, that the more fat the nurses ate, particularly the more saturated fat they ate, the less likely they were to get breast cancer. These results held true even as the women aged. After fourteen years of study, Willett reported that his team had found “no evidence” that a reduction in fat overall nor of any particular kind of fat decreased the risk of breast cancer. Saturated fat actually appeared protective.

The first sentence is actually true. Willett found that breast cancer incidence among those that ate more total fat (even saturated fat) was not higher than those women that ate less total or saturated fat [[76]](https://doi.org/10.1056/NEJM198701013160105). But to claim that total fat or even saturated fat is _protective_ of breast cancer is not supported by the evidence.

### Cribbing Taubes Alert

Taubes makes the same mistake regarding the same study on page 72 of GCBC. Coincidence?

### Cribbing Taubes Alert

Page 167:

> [T]he most effective fats for growing tumors were polyunsaturated—the fats found in vegetable oils that Americans were being counseled to eat. Saturated fats fed to rats had little effect unless supplemented with these vegetable oils.

On page 73 of GCBC Taubes makes a similar statement:

> Adding fat to the diets of lab rats certainly induced tumors or enhanced their growth, but the most effective fats by far at this carcinogenesis process were polyunsaturated fats—saturated fats had little effect unless “supplemented with” polyunsaturated fats.

The reason I am suggesting Teicholz cribbed from Taubes here is not just that both statements are very similar, but also because both cite the same (somewhat obscure) paper and draw the same erroneous conclusion from it. It’s a rather long and dense (and old) paper that very persuasively makes the case that differences in dietary fat – both amount and type – lead to wildly variable results [[77]](https://pubmed.ncbi.nlm.nih.gov/3059048/). I looked real hard for any kind of definitive statement on dietary fat and cancer in lab rats and this was the best that I found:

> Corn and safflower oils and lard consistently enhance tumorigenesis when fed at high levels; coconut oil and fats high in n-3 fatty acids do not, and beef tallow and olive oil are variably effective […]

Go ahead and read the paper. I defy you to come to the honest conclusion that vegetable oils are the only fats that will cause cancer in lab rats.

Page 168:

> Even the NCI’s own studies came up empty-handed—the most recent of those being the Women’s Intervention Nutrition Study in 2006. This trial managed to get women to drop their fat intake to 15 percent or less, thereby answering criticisms that the women in earlier studies had not seen any results because they failed to lower their intake of fat enough. But even at 15 percent, the NCI still could not find a statistically significant association between fat reduction—of any kind or amount—and reduced rates of breast cancer.

The study in question does not study rates of breast cancer in general, but rather breast cancer relapses. And according to the results the lower fat intervention did in fact significantly reduce relapses by 24% [[78]](https://doi.org/10.1093/jnci/djj494).

Page 169-170, Teicholz discusses the Women’s Health Initiative (WHI) study results:

> Yet to everyone’s alarm and bafflement, the results, published in a series of articles in JAMA, did not come out remotely as expected. […] They had apparently met all their targets, but after a decade of following this diet, they were no less likely than a control group to contract breast cancer, colorectal cancer, ovarian cancer, endometrial cancer, stroke, or even heart disease. Nor did they lose more weight.

Teicholz is actually right about most of the results, except for the ovarian cancer and weight loss. Evidently there was significantly less ovarian cancer and weight among the intervention group [[79]](https://doi.org/10.1093/jnci/djm159)[[80]](https://doi.org/10.1001/jama.295.1.39).

### Cribbing Taubes Alert

Taubes makes this same mistake on page 75 of GCBC.

Page 172:

> A review in 2008 of all studies of the low-fat diet by the United Nation’s Food and Agriculture Organization concluded that there is “no probable or convincing evidence” that a high level of fat in the diet causes heart disease or cancer.

This is absolutely true. However, here are some other conclusions by the same text that were intentionally left out because it runs contrary to the saturated-fat-is-sacred-and-unsaturated-fat-is-the-devil narrative [[81]](https://www.who.int/news-room/events/detail/2008/11/10/default-calendar/fats-and-fatty-acids-in-human-nutrition):

- There is convincing evidence that replacing SFA with PUFA decreases the risk of CHD.
    
- There is convincing evidence that replacing carbohydrates with MUFA increases HDL cholesterol concentrations.
    
- There is insufficient evidence for relationships of MUFA consumption with chronic disease end points such as CHD or cancer.
    
- There is insufficient evidence for relationships of MUFA consumption and body weight and percent adiposity.
    
- There is insufficient evidence of a relationship between MUFA intake and risk of diabetes.
    
- There is insufficient evidence for relationships of MUFA consumption with chronic disease end points such as CHD or cancer.
    
- There is insufficient evidence for relationships of MUFA consumption and body weight and percent adiposity.
    
- There is insufficient evidence of a relationship between MUFA intake and risk of diabetes.
    
- There is insufficient evidence for establishing any relationship of PUFA consumption with cancer.
    
- There is insufficient evidence for relationships of PUFA consumption and body weight and percent adiposity.
    
- There is a possible positive relationship between SFA intake and increased risk of diabetes.
    

And these aren’t found in some obscure or deep part of the text. They are found in the EXACT same place she found the above quote.

Page 172:

> The USDA and AHA have both quietly eliminated any specific percent fat targets from their most recent lists of dietary guidelines.

If by “quietly” Teicholz means “publicly published in their popular journal that has received widespread attention and 1773 academic citations since 2006,” then, yes, they “quietly” did that. And if by “eliminated any specific percent fat targets” Teicholz means “recommended consuming no more than 7% of kcals from saturated fat,” then, yes, they “eliminated any specific percent fat targets” [[82]](https://doi.org/10.1161/CIRCULATIONAHA.106.176158).

## Chapter 7: Selling the Mediterranean Diet: What Is the Science?

Page 174:

> The Mediterranean diet is now so famous and celebrated that it barely needs introduction. The regime recommends getting most of the body’s energy from vegetables, fruits, legumes, and whole grains. Seafood or poultry may be eaten several times a week, along with moderate amounts of yogurt, nuts, eggs, and cheese, while red meat is allowed only rarely, and milk, never.

Anyway, I don’t know why she claims that milk is not allowed. Her cited source does not say that. In fact, it says that dairy is allowed on a daily basis in low to moderate amounts [[83]](https://doi.org/10.1093/ajcn/61.6.1402S).

Page 180:

> In a meticulous, landmark paper in 1989, Ferro-Luzzi tried to create a workable definition of the nutritional patterns characterizing European countries bordering the Mediterranean Sea.

This “landmark” paper has only 145 citations according to Google Scholar, and it’s 25 years old.

Page 182:

> Ferro-Luzzi also took a magnifying glass to Keys’s Greek data expressly to see if she could find some flaw with his 40-percent-fat number. She concluded that his data, like all of those available on the Greek diet of that period, were so scanty and unreliable that there were “few scientific grounds” for the claim of a traditional Greek diet ever being high in fat.

As evidence Teicholz cites a paper published by Ferro-Luzzi in 2002 in the European Journal of Clinical Nutrition [[84]](https://doi.org/10.1038/sj.ejcn.1601393). The paper makes the argument that the diets on Crete and Corfu are not exactly representative of Greek diets as a whole. She actually appears to conclude that Keys’s Seven Countries Study is THE ONLY reliable data on Greek diets so far; it’s _all the other data_ that is scanty and unreliable. Let’s take some quotes from the paper:

> In conclusion, the great value of these Key’s studies is that they provide a coherent basis for the only cohort diet-health study published from anywhere in Greece so far.

AND

> Our first finding is that there are few reliable dietary studies from Greece in relation to dietary fat, **other than the detailed Seven Countries Study**. [emphasis mine]

Ferro-Luzzi’s attempt here is to associate the Seven Countries Study with her lower fat version of the Greek diet, claiming that Keys’s data confirms her argument. Do you see how this is basically the opposite of what Teicholz claims?

On page 188 Teicholz makes the argument that because Willett’s Mediterranean Diet pyramid was published in a journal supplement, it should not be regarded as serious scientific work.

> The journal articles that Willett’s team wrote to establish the pyramid were not subject to the peer-review process that scientific papers normally undergo; they had only one reviewer, not the usual two to three. This was because the papers were published, along with the entire 1993 Cambridge conference proceedings, in a special supplement of the _American Journal of Clinical Nutrition_ funded by the olive oil industry. These kinds of journal supplements sponsored by industry are standard in the field of diet and disease research, although a lay reader is unlikely to be aware of this financial backing, because sponsorship is not noted in the articles themselves.

If this is the case, then surely Teicholz would never cite a journal supplement in favor of her arguments. Y’know, since the science is tainted by industry money and all. If she did that would make her a hypocrite, right?

- On page 111 Teicholz cites a supplement that favors meat in a healthy diet and downplays its effects on carcinogenesis [[85]](https://doi.org/10.1038/sj.ejcn.1601347).
    
- On page 75 and 101 she cites a supplemental paper to try and make the case that polyunsaturated fats are dangerous [[34]](https://doi.org/10.1016/S0140-6736(70)90868-8).
    
- On pages 165 and 367 she cites an _American Journal of Clinical Nutrition_ supplement [[86]](https://doi.org/10.1093/ajcn/66.4.965S).
    
- On page 281 she cites a supplement as part of a claim that unsaturated fats play a role in cancer [[87]](https://doi.org/10.1093/ajcn/57.5.779S).
    
- Page 109 she cites a supplement to argue that Seventh-Day Adventist vegetarians weren’t so better off than others [[88]](https://doi.org/10.1093/ajcn/48.3.833). (Note: I am not exactly sure this is a journal supplement, though. Teicholz cites it as one and so does PubMed, but the _AJCN_ does not.)
    
- Page 230 she uses a supplement to claim that tropical oils containing saturated fat are not harmful [[89]](https://doi.org/10.1080/07315724.2010.10719843).
    
- Page 160 she cites a supplement as evidence that low-fat diets can lower HDL [[90]](https://doi.org/10.1093/ajcn/66.4.974S).
    
- Page 202 when she discusses a food frequency questionnaire [[91]](https://doi.org/10.1093/ije/26.suppl_1.s118).
    
- Page 144 when she claims vegetarians are not any better off than non-vegetarians [[92]](https://doi.org/10.3945/ajcn.2009.26736L).
    
- When she discusses Ancel Keys and his research on pages 38, 39, 40, 195, and 205 [[14]](https://pubmed.ncbi.nlm.nih.gov/5442783/).
    
- On page 158 where she makes the claim that more fat = healthier children [[74]](https://doi.org/10.1093/ajcn/72.5.1253s)[[93]](https://doi.org/10.1093/ajcn/72.5.1392s)[[94]](https://doi.org/10.1093/ajcn/72.5.1399s)[[95]](https://doi.org/10.1093/ajcn/72.5.1379s)[[96]](https://doi.org/10.1093/ajcn/72.5.1354s).
    
- Page 318 when discussing LDL subfractions [[97]](https://doi.org/10.1093/ajcn/62.2.478S).
    
- On page 92 when she discusses two “scholarly estimates” claiming that polyunsaturated fat was nearly unheard of before 1910 [[98]](https://doi.org/10.1093/ajcn/71.1.179S).
    
- When talking about trans fats and 7-11 on 261 [[99]](https://doi.org/10.1038/ejcn.2009.14).
    
- When claiming that vegetarian women don’t fare better than omnivorous women on page 108 [[100]](https://doi.org/10.1093/ajcn/59.5.1136S).
    
- On page 324 when she claims that the evidence against SFAs is thin [[101]](https://doi.org/10.1093/ajcn/66.4.980S).
    
- On pages 221-222 when she claims that meat consumption in Spain has “skyrocketed” while heart disease has “plummeted” [[102]](https://doi.org/10.1093/ajcn/61.6.1351S).
    
- Page 223, when she claimed that sugar consumption in Spain fell dramatically [[103]](https://doi.org/10.1093/ajcn/72.5.1316s).
    
- On page 154 when she discusses a study on babies [[104]](https://doi.org/10.1093/ajcn/72.5.1343s).
    
- When she claims that children have reduced their intakes of fat in recent decades on page 158 [[105]](https://doi.org/10.1016/0002-9149(86)90262-6).
    

Page 191 Teicholz claims that Keys and Company wooed people to their way of thinking by inviting them to Greece and describing the diet in a very romantic way.

> [T]hese getaways were an easy sell. The enormous appeal of the Mediterranean had of course been a factor in influencing Keys and his colleagues from the start, and their rapture for the region came even to suffuse their scholarly work. Henry Blackburn, for instance, who worked closely with Keys, wrote a description of the Cretan male who was “free of coronary risk” for the _American Journal of Cardiology_ in 1986, using language that is unusually florid for a scientific journal: He walks to work daily and labors in the soft light of his Greek Isle, midst the droning of crickets and the bray of distant donkeys, in the peace of his land…. In his elder years, he sits in the slanting bronze light of the Greek sun, enveloped in a rich lavender aura from the Aegean sea and sky. He is handsome, rugged, kindly and virile. The beauty of the landscape and lifestyle, its people, and its diet became united in one, overwhelming swoon.

But what Teicholz does not mention is that the above passage was from an journal editorial and it was satirizing an earlier piece that took a sardonic look at the “Low Risk Coronary Male” [[106]](https://doi.org/10.1016/s0002-9343(01)00994-9).

Page 201:

> Experts suggested that olive oil might help prevent breast cancer, for instance, but the evidence so far is very weak.

I would argue that the paper she cites to support that statement doesn’t really say that [[107]](https://doi.org/10.1093/ajcn/88.1.38). Although the statement is phrased in such a way that reasonable people may disagree. Here’s the relevant portion of the paper. I’ll let you decide.

> Overall, these observations suggest that olive oil or other oils high in monounsaturated fatty acids may decrease the risk of breast cancer, although more work is necessary before such inferences can be made with confidence. A practical implication may be that animal fat sources in the diet should be minimized, whereas monounsaturated fat sources, such as olive oil, need not be restricted, a recommendation that would be consistent with those for dietary prevention of heart disease.

Teicholz also leaves out what the authors would consider strong evidence regarding red meat and cancer:

> In the case of colorectal cancer, associations with fat intake appear to be attributable to red meat intake; indeed, red meat intake is also strongly associated with colon cancer risk in international correlations. In the case of prostate cancer, red meat is also relatively consistently associated with risk, although whether some of this is the result of fat intake remains unclear.

On page 201, Teicholz claims that the bioactive compounds in olive oil have no benefits.

> In “extra-virgin” olive oil, investigators identified a host of “nonnutrients,” such as anthocyanins, flavonoids, and polyphenols, that are believed to work their own minor miracles. They are present in olives because the fruit is dark-colored, a defense developed over thousands of years against exposure to the hot sun. Not all of the effects of these nonnutrients have been adequately explored, but in one case, flavonoids, sizable clinical trials on humans have been unable to show benefits to health.

Apparently by “health” Teicholz actually means CVD and nothing else, since she cites a meta-analysis that only focuses on CVD [[108]](https://doi.org/10.1371/journal.pone.0054318). Not cancer, not diabetes, not anything else. In any case I think she misunderstood the meta-analysis because after reading it I get the distinct impression that flavonoids do, in fact, play a beneficial role in CVD. I’m not sure why I get that impression, but maybe it has something to do with the forest plots that nearly all favor flavonoids and bits of text like this:

> [T]his review provides evidence that some flavonoids or foods rich in flavonoids, such as chocolate or cocoa, and black tea, may modulate important risk factors.

AND

> The changes in risk factors observed after flavonoid intake are clinically significant.

Also, if you’re curious, meta-analyses on flavonoids and outcomes like cancer and diabetes conclude that they are indeed beneficial [[109]](https://doi.org/10.1016/j.clnu.2013.03.011)[[110]](https://doi.org/10.1371/journal.pone.0075604)[[111]](https://doi.org/10.1016/j.jacl.2012.04.077).

Page 203:

> [A] few recent studies on animals suggest that olive oil may even provoke heart disease, by stimulating the production of something called cholesterol esters.

This is classic. To support her war on olive oil, Teicholz cites a review article that states in no uncertain terms that unsaturated fatty acids are far more beneficial to cardiovascular health than saturated fatty acids [[112]](https://doi.org/10.1016/j.maturitas.2010.12.002). In fact, here’s a quote from the text:

> The best types of fat, in terms of improving the lipid ratio, were canola (rapeseed) oil, soybean oil, and olive oil, whereas the worst types were butter and stick margarine. Not surprisingly, all types of fat were better than pure SFA because even the worst of them do contain some unsaturated fats.

Hilarious, but getting back to the olive oil and provoking heart disease… There is a section that questions whether olive oil is as good as we think it is. The author discusses a couple studies using nonhuman primates that were fed dietary cholesterol (to induce ASCVD) and three types of oil: palm oil (saturated), safflower oil (monounsaturated), and some oil high in linoleic acid (polyunsaturated). The author then explains that the polyunsaturated oil had the most favorable outcomes in terms of atherosclerosis, while the monounsaturated fat _was as bad as the saturated fat_ in the promotion of ASCVD. So if Teicholz wants to use that as evidence that olive oil provokes heart disease because it is high in MUFAs like safflower oil then she better also say that saturated fat also provokes heart disease.

However, other than a couple of animal studies, the article is generally favorable toward both MUFAs and PUFAs and quite unfavorable to SFAs.

Page 203:

> Only because olive oil has been so wildly hyped does the disappointing news about actual scientific findings come as any surprise. Indeed, “surprisingly” is the word that two Spanish researchers used when confronting the data purporting to show olive oil’s heart-healthy effect, and concluding, in 2011, that there was “not much evidence.”

This may be another one of those reasonable-people-may-disagree type things, but I don’t really think the paper concludes that there is “not much evidence.” They do say those words in the abstract, though [[113]](https://doi.org/10.1016/s0140-6736(94)92580-1). Here is the sentence: “Surprisingly, there is not much evidence coming from analytical epidemiological studies about this issue.” Which is slightly, but I would argue distinctly, different from how Teicholz phrases that. They say there are not many epidemiological studies on the issue, while Teicholz claims they say there is not much evidence showing a heart-healthy effect. Is there a difference? You decide.

In any case, the paper actually goes on to describe the evidence that exists, and it seems quite positive for ol’ olive oil. In fact, Table 4 and Table 5 show the lowest odds ratios I have ever seen in real life. Most of the studies they review show a significant inverse association between olive oil and some form of heart disease.

On page 208 she mentions that Antonia Trichopoulou was steeped in bias and not a great scientist (which is… amusing coming from Teicholz).

> “Antonia is perhaps guilty, as we all were, of thinking with her heart,” says her former colleague Elisabet Helsing, who, as the Advisor on Nutrition for WHO Europe, was involved in all the early work on the Mediterranean diet.”Many of us in this field, we were led not by the head but by our hearts. The evidence was never so good.” Or, as Harvard epidemiologist Frank B. Hu wrote in 2003, in a break with his colleagues, the Mediterranean diet “has been surrounded by as much myth as scientific evidence.”

The first quote is personal correspondence, so it can’t be verified, but the Hu quote can be. It does appear in the paper, but this is another classic example of Teicholz’s quote-mining. The whole article is what I would consider the opposite of an indictment of the Mediterranean Diet: Hu makes the case that the diet is quite beneficial and versatile. That quote is the only sentence that – when taken out of context – could possibly be construed as incriminating. In fact, much of this book is based on personal interviews with people to which I am not privy. I am uncovering an uncomfortable level of quote-mining by Teicholz which really makes me skeptical of the interviews.

Page 209, Teicholz discusses a randomized clinical trial of the Mediterranean Diet (the Lyon Diet Heart Study).52 The results clearly indicate CHD benefits, so naturally Teicholz has to do some serious spinning to explain-away these results:

> Yet the study had enough methodological problems to give any reasonable person pause: It was small (“hopelessly underpowered,” meaning not enough subjects, as one researcher commented).

I find it strange that a trial that contains 605 participants would be characterized as “hopelessly underpowered.” Let’s take a look at that quote and see what the researcher’s explanation of this is… The cited source of that quote is a paper by Ness et al [[114]](https://doi.org/10.1038/sj.ejcn.1601342). The “hopelessly underpowered” quote does not appear in the text, nor is there anything similar that might be interpreted as hopelessly underpowered. In fact, there is no mention whatsoever of the Lyon Diet Heart study.

Teicholz continues the dubious Lyon-bashing on page 210 where she states the following:

> These problems are described in a paper for the American Heart Association, which found itself in the awkward position of trying to reconcile its own recommended low-fat diet with the success of the relatively high-fat diet used in the Lyon study. The authors concluded that the diet had been “so poorly assessed in both groups” […]

Much like the hopelessly underpowered quote the “so poorly assessed in both groups” quote does not appear in the cited source [[115]](https://doi.org/10.1161/01.cir.103.13.1823). Teicholz’s next sentence says:

> It’s quite possible that the better health outcomes seen in the experimental group were due entirely to what is called the “intervention effect,” they wrote.

They didn’t write that, either. I don’t even think a paraphrasing argument could be made here.

On page 211 Teicholz discusses the interesting case of Indian researcher Dr. Ram B. Singh who conducted a dietary trial in the early 90s examining common Indian fruits and vegetables and nuts and their effects on heart health [[116]](https://doi.org/10.1136/bmj.304.6833.1015)[[117]](https://doi.org/10.1016/0002-9149(92)90786-x). As it turns out Singh may have fabricated some of his data [[118]](https://doi.org/10.1136/bmj.331.7511.281)[[119]](https://doi.org/10.1016/S0140-6736(05)67014-6)[[120]](https://doi.org/10.1136/bmj.331.7511.267)[[121]](https://doi.org/10.1136/bmj.331.7511.245)[[123]](https://doi.org/10.1111/j.1753-4887.2006.tb00232.x). Scandalous!

Teicholz could have left it there as an interesting and accurate anecdote of research malfeasance exposed, but she has to take it a step further and lie about something that didn’t happen.

> Years later, however, the Singh study was still being included in scientific literature reviews of the Mediterranean Diet, including an influential one by Lluis Serra-Majem in 2006.

The review in question does not mention or cite that study at all [[124]](https://doi.org/10.1016/S0140-6736(02)11472-3). Teicholz is on a roll here with the lies. Maybe she thinks that if you have got this far reading the book then you’re pretty much on board with her arguments and doesn’t really need to provide actual evidence for her claims. The review does cite another study by Singh (presumably the same guy), but that study was published in 2002 and has not been linked with any kind of impropriety that I know of [[125]](https://doi.org/10.1016/j.amjmed.2011.04.024).

Page 215:

> If the Israeli trial had never existed, everyone could have assumed that the Mediterranean option in PREDIMED was the best possible regime for health. But that third, low-carb arm in Israel had revealed that an even better option was possible. (Previous shorter trials had found the same thing, as we will see in Chapter 10.)

As evidence for the parenthetical claim she cites a meta-analysis comparing **low-fat diets** to **Mediterranean diets** [**[**126]](https://doi.org/10.1093/ajcn/61.6.1338S)**!** There is no discussion or mention of any low-carb diets in that paper.

Page 218 Teicholz makes the argument that the people of the Mediterranean did not eat lean meats like Keys and others recorded, but fatty meats. As evidence she cites _a work of fiction_:

> Nor were the ancient Greeks feasting on chicken. The Iliad describes the dinner given by Achilles for Odysseus this way: “Patrokles put a big bench in the firelight and laid on it the backs of a sheep and a fat goat and the chine of a great wild hog rich in lard.”

Alright everyone, pack it up. Clearly Homer’s poetry written about a thousand years before Jesus was born trumps any kind of scientific research.

Page 221:

> As Italy and Greece slowly grew more prosperous following the war, they started to leave the near vegetarian diet behind. From 1960 to 1990, Italian men came to eat ten times more meat on average, which was by far the biggest change in the Italian diet, yet the sizable spike in heart disease rates that might have been expected did not occur; in fact, they declined. And the height of the average Italian male during this time increased by almost three inches.

Is Teicholz implying that meat consumption decreased heart disease rates and increased height? Is that what she is doing here? It sure looks like it. Is this evidence from a randomized controlled dietary trial? Does Teicholz need to be reminded that correlation does not equal causation?

Her supporting evidence for this is a paper by Ferro-Luzzi (remember her?) [[127]](https://doi.org/10.1136/jech.33.2.114). The paper states that meat consumption increased about 3X not 10X. Sugar consumption increased about 4X. Both fruit and vegetable consumption doubled, as did eggs and fish. At the risk of spelling this out for everyone, even if we were to assume that something like height was due only to diet (which is a stretch), Teicholz still has all her work ahead of her to find good reasons to eliminate all the other dietary changes as a possible factor.

Page 221-222:

> It was the same in Spain: since I960, meat and fat consumption have skyrocketed, while at the same time deaths from heart disease have plummeted. In fact, coronary mortality over the past three decades has halved in Spain, while saturated fat consumption during roughly this period increased by more than 50 percent.

Her evidence for this claim is a cross-sectional study by Lluís Serra-Majem [[102]](https://doi.org/10.1093/ajcn/61.6.1351S). If you were not aware, cross-sectional studies are the least informative and least robust of all epidemiology, except for perhaps a case series. Funny how Teicholz claims throughout the book that epi studies like this one are basically meaningless if they purport to show some link between meat or saturated fat and some negative health outcome, but are just fine to invoke if they fit her narrative.

Meanwhile, if you actually read the study the author says something that Teicholz might not want you to hear:

> This paradoxical situation can be explained by expanded access to clinical care, increased consumption of fruit and fish, improved control of hypertension, and a reduction in cigarette smoking.

The author goes on to state that bioactive compounds and dietary antioxidants such as beta-carotene likely also played a role in decreasing CHD. Of course Teicholz never informs her readers of this, because she wants you to think that meat brought CHD rates down.

Teicholz continues:

> The trends are the same in France and Switzerland, whose populations have long eaten a great deal of saturated fat yet never suffered much from heart disease. The Swiss ate 20 percent more animal fats in 1976 than in 1951 while deaths from heart disease and hypertension fell by 13 percent for men and 40 percent for women.

True according to one of those cross-sectional, observational studies she cites (which according to her are crappy and meaningless) [[128]](https://doi.org/10.1016/0091-7435(83)90163-9). But what she doesn’t say is that other things happened in Switzerland in those years as well according to the analysis: use of anti-hypertensive drugs increased, the economy got much better, there was a migration to urban areas, intake of vegetable fats increased, there was an increased use of oral contraceptives. All of these factors are associated in some way with the decline of heart disease. Do they play a role? It is unclear, and it CANNOT be clear from this study. But that doesn’t stop Teicholz from letting you think that meat may have caused the downturn.

Continuing to the very next paragraph:

> This apparent contradiction holds true even on the island of Crete. When the lead researcher for the Greek portion of the Seven Countries study, Christos Aravanis, went back to Crete in 1980, two decades after his initial research, he found that the farmers were eating 54 percent more saturated fat, yet heart attack rates remained extraordinarily low.

The Aravanis paper actually says the opposite of that [[129]](https://www.proquest.com/openview/c28f798b1072ff71722cc83a3e7c3ea6/1?pq-origsite=gscholar&cbl=18750):

> The 10-year adjusted MCHD [mortality from coronary heart disease] was correlated with total fat in the diet; the correlation with saturated fatty acids was much more significant.

AND

> The 10-year incidence rate for CHD was correlated in low order with percentage of calories from total fats, but its correlation with percentage of calories from saturated fatty acids was significantly positive.

## Chapter 8: Exit Saturated Fats, Enter Trans Fats

### Cribbing Schleifer Alert

It would appear that Teicholz lifts much of the first few pages of chapter 8 from the works of David Schleifer, specifically his 2010 dissertation and a 2012 article which is essentially a condensed version of his dissertation. Schleifer is not attributed in the references section, either. Teicholz doesn’t blatantly copy-paste verbatim, but instead changes enough words to where she has plausible deniability. If you read through both chapter 8 and Schleifer’s dissertation, though, you can easily tell that it’s the same information in the same order.

For instance, Schleifer writes on page 64 of his dissertation “Philip Sokolof founded NHSA in Omaha, Nebraska in 1985 to increase public awareness of cholesterol. Sokolof was motivated by a near-fatal heart attack to spend approximately $15 million of his own money on public education campaigns related to saturated fat and cholesterol" [[130]](https://doi.org/10.1353/tech.2012.0018). On page 228 of BFS Teicholz writes “Another force pushing food companies to ditch saturated fats for hydrogenated oils was a lone multimillionaire in Omaha, Nebraska, Philip Sokolof […] after suffering a near-fatal heart attack in his forties, made it his mission in his retirement to inform Americans about the dangers of saturated fats.”

- Schleifer: “He seems to have operated the organization mostly by himself, spending approximately $15 million of his own money […]” [[131]](https://doi.org/10.1016/0021-9150(73)90028-2).
    
- Teicholz, page 230: “Sokolof founded a group called the National Heart Saver Association, funded by his own millions, and ran it mostly by himself.”
    
- Schleifer: “Sokolof mailed ‘thousands of letters’ to food manufacturers urging them to eliminate saturated fats” [[131]](https://doi.org/10.1016/0021-9150(73)90028-2).
    
- Teicholz, page 230: “[Sokolof] had mailed ‘thousands of letters’ to food manufacturers urging them to eliminate tropical oils from their products […]”
    
- Schleifer, page 65: “Irritated at receiving form letters in response, he mounted his first of three ‘Poisoning of America’ campaigns in October 1988” [[130]](https://doi.org/10.1353/tech.2012.0018).
    
- Teicholz, page 230: “[A]n irritated Sokolof decided that a campaign to shame these manufacturers publicly was his best option.”
    
- Schleifer, page 65: “These consisted of full-page advertisements in _The New York Times, Washington Times, The New York Post, USA Today, The Wall Street Journal_ and other newspapers […]” [[130]](https://doi.org/10.1353/tech.2012.0018).
    
- Teicholz, references page 383: “Identical full-page ads were also placed in the _Wall Street Journal, Washington Times, New York Post_, and _USA Today_, among other papers.”
    

Both Teicholz and Schleifer even publish [the same Sokolof figure](https://thescienceofnutrition.files.wordpress.com/2014/06/1988-display-ad-27-no-title.pdf) and use the same quotes from it.

But that’s not all: it appears Teicholz also takes most of her information on CSPI from Schleifer as well.

- Both discuss the “Saturated Fat Attack” campaign, even though the source material from the 80s is nearly impossible to find.
    
- Both pluck the same quotes from the same texts, even though the original texts might be 200+ page books.
    
- Teicholz, page 228: “Hydrogenated oils were therefore ‘not a bad bargain’ when it came to heart disease, the group concluded.” Schleifer uses the same “not a bad bargain” quote in both of his texts [[130]](https://doi.org/10.1353/tech.2012.0018)[[131]](https://www.proquest.com/openview/c28f798b1072ff71722cc83a3e7c3ea6/). What are the odds?
    
- Schleifer: “But it praised Burger King for switching to vegetable shortening in 1986, which it described as ‘a great boon to Americans’ arteries’” [[131]](https://www.proquest.com/openview/c28f798b1072ff71722cc83a3e7c3ea6/).
    
- Teicholz, page 228: “Another CSPI campaign successfully convinced movie theaters across America to switch from butter and coconut oil to partially hydrogenated oils in their popcorn poppers. This was ‘a great boon to American arteries’ CSPI judged.” Notice how Teicholz makes it seem like CSPI is referring to movie theaters here, when in fact it is actually Burger King.
    

The argument about the fight between the American Soybean Association and the Malaysian palm oil industry is also part of Schleifer’s dissertation. They both use the same quote from a NYT article.

- Schleifer, page 71: “’A trade issue is not our concern,’ said Stuart Greenblatt, a spokesman for the Keebler Company, which has said it will remove tropical oils from all its products. ‘American consumers and their health is our concern, and they are telling us they don’t want it. We get piles of mail every day, from everywhere’” [[130]](https://doi.org/10.1353/tech.2012.0018).
    
- Teicholz, page 235: “’We are getting piles of mail every day, from everywhere,’ a spokesman for the Keebler Company told the _New York Times_. ‘American consumers and their health is our concern, and they are telling us they don’t want it [tropical oils].’”
    
- Schleifer, page 77-78: “[…] General Mills’ Bugles brand corn chips were the only major American brand that did not reformulate; they were unable to find a technically viable alternative and continued to use coconut oil” [[130]](https://doi.org/10.1353/tech.2012.0018).
    
- Teicholz, page 235: “Nor could Bugles, the cornucopia-shaped snack made by General Mills, be easily reformulated without coconut oil.”
    

It is amusing to me that Teicholz accuses Time magazine of lifting her arguments when she pretty blatantly lifts arguments from others. Again, I think it is worth mentioning that Schleifer is not mentioned in her references section, and he most definitely should be. To be fair, she puts his stuff in the bibliography, but never references it.

I have no idea why the references/bibliography sections are structured like they are. It just makes everything monumentally confusing when attempting to look something up, plus it takes up an extra 100 or so pages that would not be necessary if they were cited the traditional way. My guess is that she just copied the way Taubes did GCBC, references and all.

Page 234:

> In preliminary studies, palm oil seemed to protect against blood clots.

Nope. The study she cites actually shows that sunflower seed oil protects against blood clots… in rats [[132]](https://doi.org/10.1080/07315724.2010.10719839).

## Chapter 9: Exit Trans Fats, Enter Something Worse?

Page 275:

> Research over the past twenty years has allayed the health concerns raised about palm oil during the “tropical oil wars”; the oil may actually be beneficial for health in some ways […]

As evidence she cites a supplemental paper written by the Malaysian Palm Oil Council [[133]](https://doi.org/10.1093/ajcn/62.1.1).

Page 276:

> More speculatively, research over the past decades has shown that omega-6s are related to depression and mood disorders.

The cited paper actually makes the case that low omega-3 intake is related to depression [[134]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1924359/). I suppose if you really stretch your brain you can somehow argue that eating omega-6 will necessarily lead to low omega-3s, which might then cause depression, but that’s way out there.

## Chapter 10: Why Saturated Fat Is Good for You

On page 288 Teicholz discusses Dr. Atkins and says

> The diet was a tremendous success for him and then for his patients. Atkins tweaked the Wisconsin paper and expanded it into an article for Vogue magazine (his regime was called the Vogue Diet for a while).

I decided to check her bibliography and download the Vogue issue in question via ProQuest. She cites it as “Take It Off, Keep It Off Super Diet . . . Devised with the Guidance of Dr. Robert Atkins,” Vogue 155, no. 10 (1970): 84—85.” I looked on pages 84-85 and it wasn’t there. Strange, right? Where did she come up with that citation? As it turns out (as of this writing, at least) it is cited that way on Dr. Robert Atkins’s Wikipedia page: It turns out that the actual Vogue article is located in the same issue, but different page numbers. My guess is that Teicholz simply copy-pasted the Wikipedia reference and never even saw the original magazine issue.

Page 298, Teicholz discusses the memoirs of a physician:

> With patients on his meat-all-the-time diet, Donaldson found himself “less and less likely to resort to drugs” to combat these diseases.

Her references indicate that the quote is found on page three of his memoir _Strong Medicine_. It is not. Nor is it clear, if it was the case that Donaldson prescribed fewer drugs, that it was the result of his meat-heavy diets.

Teicholz then discusses a doctor and researcher Otto Schaefer who visited some “Eskimo” populations. On page 299 she states:

> To Schaefer, it seemed obvious that the Inuit were “unable to cope with starches and sugars” to which they had been introduced.

As the source of that quote Teicholz cites a paper titled “Glycosuria and Diabetes Mellitus in Canadian Eskimos” [[135]](https://doi.org/10.1172/JCI109945). The above quote does not exist in the text. The paper actually makes the case that diabetes was considerably overdiagnosed in Eskimo/Inuit populations, perhaps contrary to the case Teicholz is trying to make in this section, namely that CHOs led to chronic diseases in the Inuit.

Page 304:

> [F]or peak performance during long-distance efforts such as marathons, the common wisdom has been that athletes should eat a lot of carbohydrates the night before. His was the first idea that Phinney wanted to test. “We were pretty sure we’d prove that the carb-loading concept was correct” Phinney told me. To his surprise, he found just the opposite: athletes in his experiments could perform at their best on nearly zero carbohydrates.

These “athletes” were actually obese study subjects [[136]](https://doi.org/10.1172/JCI103265). These subjects were on a low-carb, calorie-restricted diet and there was no control group of moderate or high-carbohydrate dieters with which to compare the results. In my interpretation the best thing you can say about this study is that obese people are capable of exercise on a reduced-calorie, ketogenic diet.

Page 305:

> [O]ur bodies have no requirement for carbohydrates and can sustain themselves perfectly well, if not better, on ketones.

For this claim she cites a paper from 1956 that measured fatty acids in the blood [[137]](https://doi.org/10.1093/ajcn/84.6.1549). I don’t know if you can claim that we have no requirements for CHOs and can sustain ourselves as well or better on ketones. The only thing you can really say is that unesterified fatty acids exist in human plasma.

Page 306:

> [T]he ability of blood vessels to dilate (known as endothelial function,” which many experts believe to be an indicator of heart attack risk) has also been shown to improve on the low carbohydrate diet, compared to people on one low in fat. Surprised and skeptical, Volek wondered if all these gains could simply be due to weight loss, since his subjects inevitably slimmed down on the Atkins diet. So he did further experiments keeping his subjects weight constant and found that the low-carb diet yielded the same improvements, even so.

My beef here is with that last statement, the rest is just for context. If you look at the “further experiments” Teicholz mentions you’ll find that the source of this statement is no trial, but rather a letter to the editor of AJCN that criticizes another study that claims that weight loss causes the improved endothelial function and not a load of fat [[138]](https://doi.org/10.1093/ajcn/83.5.1025)[[139]](https://doi.org/10.2337/diab.25.6.494).

Page 306:

> Carbohydrate restriction as a cure for diabetes had been reported by physicians as far back as the late nineteenth century, but Westman’s trials were among the first to give solid scientific backing to the treatment.

On page 398 of the Notes section Teicholz cites a study that predated Westman’s that ostensibly gives “solid scientific backing” to the idea that diabetes could be cured via CHO restriction. Except that the study was done on obese patients and given only 300-700 kcals of nearly pure protein, plus Tums and iron supplements [[140]](https://doi.org/10.2337/diab.25.6.494). The patients ended up losing a lot of weight. A couple things: 1) “Cure” is certainly a strong word; 2) Can Teicholz be sure that it was not the weight loss alone or the Tums or the iron or the protein or the lack of fat that played a role in the improvement? It must be CHO restriction?

Page 307:

> [T]he American Diabetes Association (ADA) has stood by its low-fat advice, based on the fact that diabetics have a very high risk of heart disease, and since authorities advise a low-fat diet to fight that disease, that is what the ADA recommends to prevent diabetes, too.

The publication she cites actually favorably mentions both a low-fat AND a low-carbohydrate diet [[141]](https://doi.org/10.2337/dc08-S061). In fact, it seems that the ADA might be inclined toward a low-carb diet. Don’t believe me? From the text:

- For weight loss, either low-carbohydrate or low-fat calorie-restricted diets may be effective in the short term (up to 1 year).
    
- Although low-fat diets have traditionally been promoted for weight loss, two randomized controlled trials found that subjects on low-carbohydrate diets lost more weight at 6 months than subjects on low-fat diets.
    
- Another study of overweight women randomized to one of four diets showed significantly more weight loss at 12 months with the Atkins low-carbohydrate diet than with higher-carbohydrate diets.
    
- Changes in serum triglyceride and HDL cholesterol were more favorable with the low-carbohydrate diets.
    
- In one study, those subjects with type 2 diabetes demonstrated a greater decrease in A1C with a low-carbohydrate diet than with a low-fat diet.
    
- It is possible that reduction in other macronutrients (e.g., carbohydrates) would also be effective in prevention of diabetes through promotion of weight loss […]
    
- Low-carbohydrate diets might seem to be a logical approach to lowering postprandial glucose.
    

Why not mention this? I don’t know. I suppose it is in keeping with the narrative throughout the book that nutrition authorities are incompetent, corrupt, and/or extremely rigid in their advice.

Page 308:

> One of the more extraordinary experiments involved 146 men suffering from high blood pressure who went on the Atkins diet for almost a year. The group saw their blood pressure drop significantly more than did a group of low-fat dieters—who were also taking a blood-pressure medication.

Barely any of that statement is true: blood pressure went down a bit among the low-carbohydrate group, but not enough to be statistically significant [[142]](https://doi.org/10.7326/0003-4819-140-10-200405180-00006). Also there was no mention of any group taking a blood pressure lowering medication. I don’t know where she gets that. Also, the study lasted for 24 weeks, not one year. She must have cited the wrong article, this is just too wrong to even be lying.

Page 309-310:

> In 2008, results from a two-year trial were finally published. This was the study in Israel, discussed in the Mediterranean diet chapter, on 322 overweight men and women. The trial was exceptionally well controlled by the standards of nutrition research, with lunch, the principal meal of the day in Israel, provided at a company cafeteria. The study separated subjects into three groups: one eating the AHA’s prescribed low-fat diet, another on the Mediterranean diet, and a third on the Atkins diet. […] Shai found that for nearly every marker of heart disease that could be measured during the two years of the study, Atkins dieters looked the healthiest—and they lost the most weight. For the small subset of diabetics in the study, the results looked about equal for the Atkins and Mediterranean diets. And in every case, the low-fat diet performed the worst. […] Kidney function and bone density, two primary concerns, were found to be perfectly fine, if not improved, on the Atkins diet.

For these claims Teicholz cites a 2008 study by Shai published in NEJM [[143]](https://doi.org/10.1056/NEJMoa0708681). However, she leaves out a few facts from the study.

1. The participants in the low-carbohydrate arm of the study were counseled to consume **vegetarian sources of fat and protein**.
    
2. The participants were nearly all male, something Teicholz takes umbrage with in chapter 6.
    
3. Although the low-fat diet did seem to “perform” the worst of the three, the Mediterranean diet and the low-carbohydrate diet fared similarly in most respects, not just in diabetics. So I doubt you can say unequivocally that the Atkins dieters were the healthiest when there’s another diet that leads to the same measured outcomes.
    

Although not discussed by Teicholz there were some other papers published using data from this particular study (referred to as DIRECT, Dietary Intervention Randomized Controlled Trial). One of which was a four year follow-up to the study that showed that the Atkins-style dieters gained back most of the weight that was lost [[144]](https://doi.org/10.1056/NEJMoa0708681). The Mediterranean dieters ended up losing nearly double the weight of the Atkins dieters. The low-fat group still had the least amount of weight lost. The Mediterranean dieters also had the most favorable cholesterol and triglycerides.

In another publication kidney function was found to be similarly improved among all three diets [[145]](https://doi.org/10.1056/NEJMc1204792). Although the authors mention that since the improvement was so similar between diets, the improvement among participants was likely due to weight loss alone rather than the constituents of the diet. The same was said in a publication examining the effect of the three diets on ASCVD [[146]](https://doi.org/10.2337/dc12-1846).

Another publication from the same trial suggests that the Mediterranean diet is the most beneficial of the three for type 2 diabetics [[147]](https://doi.org/10.1161/CIRCULATIONAHA.109.879254).

For a more detailed discussion of the Shai study see CarbSane’s posts: [Part 1](http://carbsanity.blogspot.com/2014/06/nina-teiholz-shaister-part-i-diets-of.html), [Part 2](http://carbsanity.blogspot.com/2014/06/nina-teiholz-shaister-part-ii.html), [Part 3](http://carbsanity.blogspot.com/2014/06/nina-teiholz-shaister-part-iii-well.html), and [Part 4](http://carbsanity.blogspot.com/2014/06/nina-teiholz-shaister-part-iv-random.html).

Page 314:

> In 2011, a group of top nutrition experts published the first high-level, formal consensus paper stating that refined carbohydrates were worse than saturated fats in provoking heart disease and obesity (Astrup et al. 2011).

This is not true. The paper stated that no clear benefit of replacing saturated fatty acids with carbohydrates has been shown, not that refined carbohydrates are worse [[148]](https://doi.org/10.1016/S0168-8227(09)70008-7). Moreover, the authors state unambiguously that replacing saturated fatty acids with polyunsaturates does decrease risk of heart disease – something Teicholz unsurprisingly leaves out.

Page 315:

> Remember that the Shai study in Israel found that the Mediterranean diet group, eating a high proportion of calories as these “complex” carbohydrates, turned out to be less healthy and fatter than the group on the Atkins diet, although they were healthier than the low-fat alternative.

Umm… No.

Page 315:

> The Women’s Health Initiative, too, in which some 49,000 women were tested on a diet high in complex carbohydrates for nearly a decade, showed no reduction in disease risk or weight.

Wrong again. There was decreased risk in ovarian cancer and weight [[79]](https://doi.org/10.1093/jnci/djm159)[[80]](https://doi.org/10.1001/jama.295.1.39).

Page 317:

> [I]n more than a few major studies, LDL-cholesterol levels were found to be completely uncorrelated with whether people had heart attacks or not.

Let’s take a look at these “major studies” she cites, shall we?

The first is by de Lorgeril et al [[113]](https://doi.org/10.1016/s0140-6736(94)92580-1). I won’t go into detail, but those in the intervention group had fewer heart attacks and also had lower LDL. From the text: “[T]he trend with time was a decrease in total and low density lipoprotein (LDL) cholesterol […]” Although it was not statistically significant, so we’ll give this one to Teicholz.

The second is not a study, but a short commentary by Despres [[149]](https://doi.org/10.1016/S0140-6736(09)60448-7). It argues that we should not focus **exclusively** on LDL, which is not the same as saying LDL is not correlated with anything.

The third is a statin trial that showed reducing LDL cholesterol also reduced coronary events [[150]](https://doi.org/10.1001/archinternmed.2010.182). In other words, the opposite of Teicholz’s claim. Some choice quotes from the paper:

> This trial provides evidence that the use of intensive atorvastatin therapy to reduce LDL cholesterol levels below 100 mg per deciliter is associated with substantial clinical benefit in patients with stable CHD. Our findings indicate that the quantitative relationship between reduced LDL cholesterol levels and reduced CHD risk demonstrated in prior secondary-prevention trials of statins holds true even at very low levels of LDL cholesterol.

The fourth is a meta-analysis on statins and all-cause mortality [[151]](https://doi.org/10.1001/archinternmed.2010.182).  Basically irrelevant because it’s only slightly related to the claim of no relationship between LDL and heart attacks.

The fifth is by Castelli et al and is again pretty much the opposite of what Teicholz said [[152]](https://doi.org/10.1161/01.CIR.55.5.767). Want some more choice quotes?

> There is a very regular increase of CHD prevalence rates with increasing LDL cholesterol level at each level of HDL cholesterol. The inverse relationship between HDL cholesterol and CHD, when taken over the three levels of LDL cholesterol, is significant (P < 0.001) by a method of Mantel, as are the positive trends of CHD prevalence on LDL cholesterol level. Cross-classification of triglyceride with LDL cholesterol level (fig. 3) leads to the conclusion that either lipid has a statistically significant association with CHD prevalence […] In general, then, when contingency tables are constructed for the three lipids considered two at a time, HDL and LDL cholesterol emerge as consistently significant factors in CHD prevalence […]

Does Teicholz even read the studies she cites?

Page 324:

> Krauss and his colleagues concluded that “saturated fat was not associated with an increased risk” for heart disease or stroke.

Krauss did not conclude that, according to the cited study [[153]](https://doi.org/10.3945/ajcn.2008.26285). What was said is that **replacing saturated fat with carbohydrate** is not associated with an increased risk of heart disease **in women**. Notice how they are very different statements?

What about stroke? The paper mentions a couple things:

> [S]aturated fat intake may be inversely related to ischemic and/or hemorrhagic stroke, but a meta-analysis including results from 6 other studies did not yield a statistically significant risk reduction. Notably, in humans, the risk of stroke has been related to both the saturated and monounsaturated fatty acid content of plasma cholesteryl esters, which further supports the possibility that the dietary intake of these fatty acids may influence CVD risk by altering cholesteryl ester composition.

Bonus factoid: the paper also states that polyunsaturated fats are inversely associated with developing type 2 diabetes.

On page 325-326 she discusses a panel event at FNCE in 2010 and produces a quote by Mozaffarian: “its not really useful anymore to focus on saturated fats,” he said. She cites a subsequent publication of the event in the _Journal of the American Dietetic Association_ [[154]](https://doi.org/10.1016/j.jada.2011.03.030). However, that paper does not contain that quote. Nor does the quote appear in other publications by JADA discussing that event and quoting Mozaffarian [[155]](https://doi.org/10.1016/j.jada.2011.03.026)[[156]](https://doi.org/10.1016/j.jada.2011.03.031).

Page 326: “Americans have dutifiilly followed official dietary advice […]” Pretty sure that’s not true [[157]](https://doi.org/10.1097/JOM.0b013e31823ccafa). Even by the admission of the USDA [[158]](https://centerforinquiry.org/wp-content/uploads/sites/33/quackwatch/dga_advisory_2010.pdf).

## Conclusion

Some have called this review mere quibbling or nit-picking, and that the true thesis of BFS still stands. I would strenuously argue the opposite. If I was nit-picking I would have also brought up one or more instances where Teicholz misquotes someone, but the actual quote is not substantively different. I imagine those are innocent mistakes. Nor do I think that Teicholz’s main arguments still hold up.

The arguments in this book are scientific claims that are purportedly supported by scientific evidence. If it turns out, however, that the evidence was never really there in the first place then you can no longer make the claim. The issues I bring up in this review are too substantial and too numerous to be ignored. If you were to remove all of the instances where Teicholz deeply distorts a study or publication, and you were to remove all conclusions that she draws from the distortions you would be left with nothing but a pamphlet.

After reading The Big Fat Surprise by Nina Teicholz I am frankly disappointed – yet unsurprised – that a book like this was even published. We as readers need to start demanding better fact-checking from our publishers, especially the enormously successful ones like Simon & Schuster. Misinformation like this can actually affect people’s health in a potentially very negative way. I get that publishing companies want to make a profit, but you can publish a compelling pop science book that people buy without misinforming your audience.

##### BIBLIDDILYOODILYOGRAPHY

###### [REFERENCES LINK](https://docs.google.com/document/d/1AxmJfHhZe-9uINUmJdxAdj4k5NUfygXuzp4NDgcuYKw/edit?usp=sharing)