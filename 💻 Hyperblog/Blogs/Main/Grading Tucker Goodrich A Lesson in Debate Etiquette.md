Back in August of 2021, I invited [Tucker Goodrich](https://twitter.com/TuckerGoodrich) to a debate about the health value of seed oils. Tucker agreed to participate immediately, but after a brief back-and-forth (which resulted in Tucker [contradicting himself](https://twitter.com/The_Nutrivore/status/1428064287059324929?s=20&t=rmYpT72-5Tan31MnU_ptlw)), he eventually [withdrew](https://twitter.com/TuckerGoodrich/status/1428062578668830720?s=20&t=rmYpT72-5Tan31MnU_ptlw) without an explicit justification. Shortly thereafter he wrote a [blog article](https://yelling-stop.blogspot.com/2021/12/thoughts-on-nick-hieberts-comprehensive.html), wherein he paints a caricature of the events that transpired, which included everything from exaggerations to full-throated lies. Despite this, I reinvited him to debate multiple times since the incident, with either no response or outright refusals in reply.

My initial invitation was prompted by my discovery that Tucker was actually just straight up [fabricating evidence](https://twitter.com/The_Nutrivore/status/1489863311155810304?s=20&t=DhYUMrwmqG260Ft7uNHozg) in a debate with a friend of mine, [Alan Flanagan](https://www.alineanutrition.com/about/). Needless to say, Tucker appears to be a particularly dishonest actor and a coward. When he wants to engage with me he does so in the safety of his blog, and critiques my writings and public statements with strawman arguments and red herrings, as well as just attacking very low-hanging fruit. At this point I don't think it's unfair to speculate that he conducts himself in this manner because he knows his arguments would not withstand scrutiny in a debate against me.

Recently I was invited to set the record straight and shed some light on some of Tucker's more misleading claims on [Mark Bell's Power Project](https://www.youtube.com/watch?v=omzCi2CGoxo). Naturally Tucker felt compelled to write [a response](http://yelling-stop.blogspot.com/2022/02/thoughts-on-nick-hiebert-on-mark-bells.html) that was also particularly low-level and ultimately did not provide any stable defeaters for any of my positions. That didn't stop him from writing the article as though my arguments had been thoroughly dispatched, despite his counterarguments not even meaningfully interacting with what I had said to begin with.

At this point I figured that enough was enough. So, I contacted an acquaintance of mine (who through this whole ordeal would become a friend), [Matthew Nagra](https://drmatthewnagra.com). I knew Matt was experienced in empirical debate and didn't hold any whacky heterodox views about seed oils, so I asked him if he'd be interested in debating Tucker. Matt agreed and promptly extended an invitation to Tucker. After Tucker agreed to a debate against Matt on [Simon Hill](https://twitter.com/theproof)'s [podcast](https://theproof.com/podcast/), it was time to get to work. With some help from [Matthew Madore](https://twitter.com/MattMadore576) over at [My Nutrition Science](https://www.mynutritionscience.com/authors-page/), we developed about 200 pages worth of debate-prep, syllogisms, and dialogue trees over approximately six weeks.

As you will see throughout this article, our hard work paid off in the sweetest way possible. Matt was able to tease an absolutely enormous amount of dodges, strawman arguments, and straight up contradictions out of Tucker. It was more than any of us could have hoped for, and it truly reveals just how weak Tucker's arguments really are. The number of self-defeating bullets Tucker tried to bite in order to stay ahead in the debate was exquisite— a true treat. All in all, I'm very pleased with the results. Matt did a truly phenomenal job. Enjoy!

**MATTHEW NAGRA VS TUCKER GOODRICH**

Let's start. Tucker and Matt both agree to the following:

**Debate Rules:**

1) The debate will be open and conversation-style.

2) Both parties will avoid talking over one another.

3) Both parties must answer questions directly.

4) Both parties will make and address one claim at a time.

5) Both parties will refrain from using personal insults.

6) Both parties will provide adequate time for response.

**Matt's Debate Proposition:**

"It is more reasonable to believe that seed oils are beneficial, rather than harmful, for coronary heart disease risk."

**Tucker's Definition of Seed Oils:**

"Oils made from seeds, like canola oil, soybean oil, sunflower oil, etc. Not the fleshy part of the fruit, like olive oil, palm oil, avocado oil, or coconut oil. With the primary focus being high linoleic acid oils."

Now let's get into the actual debate.

**Rule violation (**[**30:20**](https://youtu.be/5nZSV-DyVRM?t=1820)**,** [**30:38**](https://youtu.be/5nZSV-DyVRM?t=1838)**):**

Tucker breaks rule three twice. Matt provides evidence from three independent analyses that shows a concordance rate of 65-67% between nutritional epidemiology and nutritional randomized controlled trials (RCTs). Matt then asks Tucker if nutritional epidemiological evidence is concordant with nutritional RCTs two thirds of the time, which is a yes or no question. Tucker responds by saying its irrelevant due to being a tangent. However, it's not clear exactly how Matt's question is an irrelevant tangent, since it directly interacts with Tucker's opening claim at the beginning of the debate.

**Strawman (**[**32:09**](https://youtu.be/5nZSV-DyVRM?t=1929)**):**

Tucker finally answers "no" in response to Matt's question. When Matt asks why not, Tucker responds by saying "it's just a meta-analysis", which doesn't provide us with any clarity on why he provided the answer that he did. Tucker further elaborates with a strawman of Matt's position, stating that the data provided by Matt doesn't provide us with any information about the likelihood of a high-LA intervention being beneficial. However, this wasn't Matt's point. Matt's point was that Tucker's characterization of nutritional epidemiology on the whole was a red herring and false.

  
**Rule violation (**[**37:24**](https://youtu.be/5nZSV-DyVRM?t=2239)**):**

Tucker breaks rule three for the third time. Matt shows Tucker the power calculation from MCE, and demonstrates that the trial was actually underpowered. Matt then asks Tucker if the trial had adequate power. Tucker again says that the question is irrelevant, stating that when a trial shows harm, it shouldn't be discounted based on a P-value. This is a truly bizarre answer, as Matt has yet to discount the trial.

**Rule violation (**[**38:50**](https://youtu.be/5nZSV-DyVRM?t=2330)**):**

Tucker breaks rule four. Instead of resolving Matt's point about MCE, Tucker wants to pivot to the LA Veterans Administration Hospital Trial (LAVAT).

**Rule violation (**[**43:20**](https://youtu.be/5nZSV-DyVRM?t=2600)**):**

Tucker breaks rule three for the fourth time. Matt presents a subgroup analysis from MCE that shows that older subjects who maintained the intervention diet for more one year tended to see a benefit when compared to the control diet. Matt asks Tucker if the older subjects in MCE who were consuming the intervention diet for longer saw a benefit compared to the control diet. Tucker rejects the question, saying that Matt is citing a "sub-population" analysis, and that it is superseded by the total outcome. This answer doesn't interact with Matt's question.

**Rule violation (**[**42:56**](https://youtu.be/5nZSV-DyVRM?t=2576)**,** [**43:30**](https://youtu.be/5nZSV-DyVRM?t=2610)**):**

Tucker breaks rule three for the fifth time. Matt rephrases his question, asking Tucker if harm would be more likely if subjects were to maintain the intervention diet for a longer period of time as opposed to a shorter period of time in MCE. Tucker again, dismisses a yes-or-no question as being irrelevant without any clarifying explanation.

**Rule violation (**[**44:06**](https://youtu.be/5nZSV-DyVRM?t=2646)**):**

Tucker breaks rule two. Matt wants to elaborate on why his question is important, but Tucker cuts him off. Matt allows it, but Tucker shouldn't have done it to begin with.

**Rule violation (**[**48:18**](https://youtu.be/5nZSV-DyVRM?t=2898)**):**  
Tucker breaks rule one and three. Tucker attempts an explanation for why MCE showed benefit for older subjects over two years. When Matt asks for further clarification, Tucker refuses to provide it. Not only is this directly dodging a question, this behaviour also calls into question whether or not Tucker is actually there for an open, conversation-style debate.

**Rule violation (**[**49:07**](https://youtu.be/5nZSV-DyVRM?t=2947)**):**

Tucker breaks rule four again. Instead of resolving Matt's point about MCE, Tucker wants to pivot to the Oslo Diet-Heart Study (ODHS).

  
**Strawman (**[**50:34**](https://youtu.be/5nZSV-DyVRM?t=3034)**):**

Tucker offers a strawman of Matt's position, claiming that Matt's conclusions were based on ODHS. In fact, Matt's position was not based on ODHS, and Matt even said that the evidence underpinning his position is virtually the same if ODHS is omitted completely.

**Strawman (**[**51:52**](https://youtu.be/oYsRgsJoZc4?t=3112)**):**

Tucker offers a strawman of Matt's position, claiming that the composition of fats in margarines would need to be known in order to conclude that trans-fats (TFA) were confounding in MCE. This isn't Matt's position. Matt's position was that TFA confounding is likely based on two pieces of evidence. Firstly, the vast majority of margarines during that time period contained TFA. Secondly, there was disagreement between the observed cholesterol changes and the predicted cholesterol changes in MCE. From these data Matt infers that TFA confounding was likely, which is merely a truism that can be soundly inferred a priori. Matt doesn't conclude that TFA confounding did in fact occur. He merely takes the position that is likely.

**Rule violation (**[**55:27**](https://youtu.be/oYsRgsJoZc4?t=3327)**):**

Tucker breaks rule three. Matt asks Tucker if he thinks that a cooking oil with 1% TFA would be equivalent to a cooking oil with 15% TFA, which is a yes or no question. Tucker responds by saying that TFA confounding would have to be a systematic issue across all of the RCTs using Matt's assumptions. However, this doesn't interact with Matt's question.

**Rule violation (**[**1:02:00**](https://youtu.be/oYsRgsJoZc4?t=3720)**):**

Tucker breakers rule three again. Tucker cites a result from the LAVAT that is incongruent with Matt's figures, so Matt asks him what Tucker's figure is representing. After Matt finishes explaining the incongruency, he asks Tucker where he is getting the number from. Instead of answering, Tucker just asks him what his point is.

**Potential contradiction (**[**1:08:00**](https://youtu.be/oYsRgsJoZc4?t=4080)**):**

Tucker concedes that LAVAT showed a slight benefit for vegetable oils compared to animal fats. However, earlier he characterized MCE as showing "harm", but it wasn't qualified as slight. However, LAVAT showed a statistically significant 49% increase in CVD mortality risk in the control group, but MCE showed a non-significant 24% increase in CVD mortality risk in the intervention group. If a non-significant 24% increase in CVD mortality is noteworthy in Tucker's view, why is a statistically significant 49% increase in CVD mortality risk only slight in his view as well?

Without further elaboration, would have to either accept that the increase in CVD mortality in LAVAT is noteworthy, or accept that the CVD mortality increase in MCE is at least just as unnoteworthy as LAVAT, if he wishes to stay consistent. We can syllogize the logical entailments of accepting the CVD mortality risk in MCE as "not slight" like this:

**Definitions:**

C := the trial has a CVD mortality risk that is over 23%

M := the trial has a CVD mortality risk that is not slight

e := MCE

v := LAVAT

**P1)** If the trial has a CVD mortality risk that is over 23%, then the trial has a CVD mortality risk that is not slight.

**(∀x(Cx→Mx)**

**P3)** MCE and LAVAT are trials that have CVD mortality risks that are over 23%.

**(Ce∧Cv)**

**C)** Therefore, MCE and LAVAT are trials that have CVD mortality risks that are not slight.

**(∴Me∧Mv)**

**Rule violation (**[**1:08:22**](https://youtu.be/oYsRgsJoZc4?t=4102)**,** [**1:08:34**](https://youtu.be/oYsRgsJoZc4?t=4114)**):**

Tucker breaks rule three two more times. Matt presented data from LAVAT that divulged that the intervention diet resulted in statistically significant benefits to CVD mortality and all-cause mortality. Matt asked Tucker if he though the data shows that the intervention diet in LAVAT resulted in a benefit to CVD mortality and all-cause mortality, which is again a yes or no question. First, Tucker dismisses the question, saying that LAVAT also saw an increase in cancer. When Matt asks again, Tucker says that he doesn't agree that studies can be "sliced and diced", which doesn't interact with Matt's question.

**Rule violation (**[**1:10:44**](https://youtu.be/oYsRgsJoZc4?t=4244)**,** [**1:14:22**](https://youtu.be/oYsRgsJoZc4?t=4462)**):**

Tucker breaks rule four two more times. Instead of resolving Matt's point about LAVAT, Tucker tries to pivot to talking about the standard American diet (SAD), which is tangential to the claim being discussed at that moment. When asked again, Tucker responds by talking about rates of acute myocardial infarction (AMI) in Africans, which is also tangential to the claim being discussed at that moment.

**Rule violation (**[**1:18:03**](https://youtu.be/QGNNsiINehI?t=4683)**):**

Tucker breaks rule three yet again. Matt attempts an internal critique by asking if Tucker would believe that adding vegetable oils to the SAD would be a benefit, based on the results of LAVAT. Tucker replies by saying that Lee Hooper would conclude that there is little to no benefit. A truly bizarre reply that doesn't interact with Matt's question at all.

**Potential contradiction (**[**1:21:11**](https://youtu.be/QGNNsiINehI?t=4871)**):**

Matt responds to ecological data that Tucker presented earlier, stating that he disagrees with the notion that it can be used to determine independent effects of seed oils. Tucker denies that he is making such a claim, and clarifies that he is speculating off that data. However, earlier in the debate Tucker objected to Matt's speculation regarding TFA confounding in MCE. Why is it OK for Tucker to submit speculation as evidence but not OK for Matt to submit speculation as evidence?

Without further elaboration, it's unclear why Tucker would not be OK with a priori inferences about the potentially confounding effects of TFA in MCE being used in debate, when he also relies on such a priori inferences. Such as when he infers from ecological data that seed oils are likely to be detrimental. We can syllogize the logical entailments of accepting the use of a priori inferences in debate like this:

**Definitions:**

N := speculation counts as evidence

B := speculation can be used in debate

n := a priori inferences about the effects of seed oils from ecological data

m := a priori inferences about TFA in MCE

**P1)** If speculation counts as evidence, then speculation can be used in debate.  
**(∀x(Nx→Bx))**  
**P4)** A priori inferences about the effects of seed oils from ecological data and a priori inferences about TFA in MCE are speculations that count as evidence.  
**(Nn∧Nm)**  
**C)** Therefore, a priori inferences about the effects of seed oils from ecological data and a priori inferences about TFA in MCE are speculations can be used in debate.  
**(∴Bn∧Bm)**

**Rule violation (**[**1:32:15**](https://youtu.be/QGNNsiINehI?t=5535)**):**

Tucker breaks rule four once more. At this point, both are discussing Lyon Diet-Heart Study (LDHS), and the differential effects of the various dietary modifications that were made in that study, such as the reduction in LA. Matt takes the position that it is unlikely the the 73% reduction in AMI risk seen in LDHS is attributable to LA-reduction due to many other dietary variables changing alongside the reduction in LA. Tucker takes the position that reduction in LA explains the majority of the effect. Instead of resolving Matt's point about LDHS, Tucker wants to pivot to discussing the mechanisms of atherosclerosis.

**Potential contradiction (**[**1:35:45**](https://youtu.be/QGNNsiINehI?t=5745)**):**

Tucker holds the view that alpha-linolenic acid reduces risk, but he also holds the view that risk is mediated solely by LA-specific metabolites. However, this is true of all non-LA fatty acids. If the metabolites that confer harm are LA-specific, then shouldn't all non-LA fatty acids be equally non-atherogenic? He tries to reconcile this by saying that ALA "blocks" the negative effects of LA. Does that mean that ALA is an antioxidant? Does the mean that ALA detoxifies LA-specific metabolites somehow? He offers no further explanation.

**Rule violation (**[**1:47:53**](https://youtu.be/QGNNsiINehI?t=6473)**):**

Tucker breaks both rule two and rule six. In response to an objection from Tucker, Matt wanted to ask a question so that he could have specific clarity on Tucker's position, but Tucker cut Matt off before Matt could ask.

**Potential contradiction (**[**1:39:20**](https://youtu.be/QGNNsiINehI?t=5960)**):**

Matt uses one of Tucker's references to present an internal critique. The reference appears to contradict Tucker's model of CVD by stating that hyperlipidemia is sufficient to explain the development of CVD in all its manifestations. Tucker objects to this, saying that the paper is referring to the "genetic hypothesis" of CVD, which involves CVD risk being conferred via genetically mediated concentrations of LDL. Tucker elaborates by stating that if the hypothesis were true, there wouldn't be observable differences in CVD rates between people with the same genetic background between different environments. This appears to be discounting the possibility that LDL can vary between individuals within a genetically homogenous group. Yet, here he affirms that environmental factors like dietary modification can affect LDL levels.

**Strawman (**[**1:49:00**](https://youtu.be/QGNNsiINehI?t=6540)**):**

Matt claims that oxidation of LDL is virtually inevitable after LDL are irreversibly retained within the subendothelial space. Tucker objects, saying it is not inevitable. Matt asks for clarification, requesting evidence that oxidation can be abolished in the subendothelial space. Tucker informs Matt that the fat composition of the diet can influence LDL oxidation rates. After Matt tells Tucker that this isn't what he's asking about, Tucker insists that this is indeed what Matt is asking about, without any further explanation.

**Potential contradiction (**[**1:53:46**](https://youtu.be/QGNNsiINehI?t=6826)**):**

Tucker correctly states that the LA-derived metabolite, malondialdehyde (MDA), is responsible for oxidative modification of LDL particles. However, he also states that ALA can produce this metabolite as well. If Tucker's position is that MDA-mediated oxidative modification of LDL particles initiates CVD, then why would it matter if an intervention involves both LA and ALA? At **58:32**, Tucker states that the inclusion of ALA confounded LAVAT.

If Tucker wants to remain consistent, he'll have to explain why the atherogenic properties of ALA that are entailed from his stated position don't seem to matter. He seems to singling out LA based on characteristics that he admits are shared by ALA. Without further elaboration, Tucker should have to accept ALA as atherogenic, and the reasoning can be syllogized like this:

**Definitions:**

M := MDA is atherogenic

F := increase heart disease risk  
x := MDA-producing fatty acids

n := ALA

**P1)** If MDA is atherogenic, then all MDA-producing fatty acids increase heart disease risk.

**(M→∀x(Fx))**

**P2)** MDA is atherogenic.

**(M)**

**C)** Therefore, ALA is an MDA-producing fatty acid that increases heart disease risk.

**(∴Fn)**

**Direct contradiction (2:18:19):**

Tucker directly contradicts himself when he suggests that corn oil is not a seed oil. At **1:43:50**, he cited two primary interventions that demonstrate that seed oils produce harm, but the sole oil used in those trials was corn oil.

**P1)** If MCE and RCOT used corn oil and MCE and RCOT demonstrate the harms of seed oils, then corn oil is a seed oil.

**(H∧R→O)**

**P2)** MCE and RCOT used corn oil.

**(H)**

**P3)** MCE and RCOT demonstrate the harms of seed oils.

**(R)**

**C)** Therefore, corn oil is a seed oil.

**(∴O)**

**Potential contradiction (**[**2:09:08**](https://youtu.be/QGNNsiINehI?t=7748)**):**

Tucker again concedes that LAVAT showed a "fairly small" benefit of vegetable oils compared to animal fats. Earlier in the debate, Tucker stated that other trials such as MCE as showed "harm". However, LAVAT showed a statistically significant 49% increase in CVD mortality risk in the control group, but MCE showed a non-significant 24% increase in CVD mortality risk in the intervention group. If a non-significant 24% increase in CVD mortality is noteworthy in Tucker's view, why is a statistically significant 49% increase in CVD mortality risk only "fairly small" in Tucker's view as well?

**Rule violation (**[**2:10:08**](https://youtu.be/QGNNsiINehI?t=7808)**,** [**2:10:41**](https://youtu.be/QGNNsiINehI?t=7841)**):**

Tucker breaks rule three another three times. After Tucker implies that the benefits seen in LAVAT are inconsequential, Matt once again presents the findings of LAVAT. Matt asks Tucker if he believes that a 33% reduction to all-cause mortality and a 35% reduction to CVD mortality is "small", which is a yes or no question. Rather than answering, Tucker starts talking about Christopher Ramsden's meta-analysis. When Matt asks again, Tucker continues talking about the Ramsden meta-analysis.

**Rule violation (**[**2:10:52**](https://youtu.be/QGNNsiINehI?t=7852)**):**

Tucker breaks rule one again. Matt attempts to explain what his question for Tucker is, but Tucker mind-reads and tries to tell Matt what he means instead of listening.

**Direct contradiction (**[**2:10:59**](https://youtu.be/QGNNsiINehI?t=7859)**):**

Tucker objects to Matt's use of LAVAT to demonstrate a potential benefit of seed oils, saying that "you can't take one single RCT and prove an effect". However, this is the exact manner in which Tucker relies on LDHS to demonstrate the benefits of LA reduction in the context of high ALA. There is no other study that included such an intervention.

Without additional clarification, Tucker would need to reject his own reliance on LDHS to prove the effect of LA-reduction in the context of an ALA-rich diet. Such an entailment can be syllogized like this:

**P1)** If a single study cannot be used to prove an effect, then one cannot use LDHS to prove the effect of LA-reduction in the context of an ALA-rich diet.

**(P→¬U)**

**P2)** A single study cannot be used to prove an effect.

**(P)**

**C)** Therefore, one cannot use LDHS to prove the effect of LA-reduction in the context of an ALA-rich diet.

**(∴****¬****U)**

**Rule violation (**[**2:13:03**](https://youtu.be/QGNNsiINehI?t=7983)**):**

In an astonishing feat, Tucker breaks rules two, four, five, and six all at the same time. Rather than engaging with Matt's question about the clinical significance of the LAVAT results, Tucker dodges by attacking Matt's intellectual integrity. When Matt attempts to interject, Tucker cuts Matt off, attempting to pivot to discussing meta-analyses despite agreeing to systematically discuss the relevant trials one by one.

**Strawman (**[**2:14:21**](https://youtu.be/QGNNsiINehI?t=8061)**):**

Tucker finishes his rant by suggesting that the LAVAT results don't show that increasing seed oils can "reduce heart disease by 30%", which was not at all what Matt was suggesting. All Matt asked Tucker was whether or not Tucker believed that the effects observed in LAVAT were small.

**Strawman (**[**2:14:33**](https://youtu.be/QGNNsiINehI?t=8073)**):**

Tucker claims that Matt agreed that you cannot "prove" something based on one study, and in fact meta-analysis is required for proof. Matt never committed himself to such a concept for causal inference. This is a truly bizarre move on Tucker's part.

**Rule violation (**[**2:16:32**](https://youtu.be/QGNNsiINehI?t=8192)**):**

Tucker breaks rules two and six again. Now discussing the paper by Hooper et al. (2020), Tucker asks Matt where in the paper can the evidence for his claims be found. Matt attempts to respond, but Tucker cuts Matt off again by asking the exact same question he just asked, but with a slightly more crazed inflection.

**Rule violation (**[**2:19:30**](https://youtu.be/QGNNsiINehI?t=8370)**,** [**2:19:38**](https://youtu.be/QGNNsiINehI?t=8378)**,** [**2:19:45**](https://youtu.be/QGNNsiINehI?t=8385)**):**

Tucker breaks rule three three more times. Tucker criticizes the Hooper (2020) meta-analysis by stating that the analysis found "little to no effect" of reducing saturated fat on CVD mortality. Not only is this tangential, but Matt humours the objection long enough to make the point that events is a much more sensitive endpoint than mortality, and events is where the benefit can be seen. Matt follows up by asking Tucker if he thinks reducing total CVD events is beneficial if all else was held equal, which is a simple yes or no question. Rather than answering, Tucker starts painting a caricature of Matt's question instead. When it is clear that Matt is not getting a straight answer, the moderator interjects and allows the yes-or-no question to be asked again. Again, Tucker dodges.

**Direct contradiction (**[**2:21:19**](https://youtu.be/QGNNsiINehI?t=8479)**):**

Tucker takes the position that silent and non-fatal AMIs are not important outcomes and we need not care about them. Yet, at [**1:16:22**](https://youtu.be/QGNNsiINehI?t=4582), Tucker makes it clear that silent and non-fatal AMIs are important and that we should care about them.

This one is pretty straight forward. Either silent and non-fatal AMIs are important and we should care about them, or they are not important and we shouldn't care about them. If Tucker wishes to remain consistent while also preserving his own arguments, we'd need to accept that silent and non-fatal AMIs are important, and that we should care about them. This entailment can by syllogized like this:

**P1)** If silent and non-fatal AMIs are important, then we should care about silent and non-fatal AMIs.

**(N→W)**

**P2)** Silent and non-fatal AMIs are important

**(N)**

**C)** Therefore, we should care about silent and non-fatal AMIs.

**(∴W)**

**Rule violation (**[**2:25:20**](https://youtu.be/QGNNsiINehI?t=8720)**):**

Tucker breaks rule four for the seventh time. After agreeing to discuss the results of Hooper (2020), Tucker suddenly attempts to steer the debate toward some sort of meta-level discussion about seed oil consumption in the general population.

**Rule violation (**[**2:30:31**](https://youtu.be/QGNNsiINehI?t=9031)**):**

Tucker breaks both rules four and six. The moderator poses a question to both Matt and Tucker, and gets a satisfactory answer from both Matt and Tucker. However, rather than returning the floor to the moderator or continuing with the debate, Tucker takes the opportunity to address points that Matt hasn't even made yet in the debate, and doesn't give Matt any amount of time to respond.

**Rule violation (**[**2:37:06**](https://youtu.be/QGNNsiINehI?t=9426)**):**

Tucker breaks rules two and four again. The moderator recognizes that Tucker's incoherent flow-of-consciousness monologue has been going on for nearly ten minutes, and the moderator tries to interject. Tucker cuts the moderator off and proceeds to ramble about vitamin E for another minute.

**Rule violation (**[**2:38:42**](https://youtu.be/QGNNsiINehI?t=9522)**):**

Tucker can't help but break rule four again. Tucker interjects, before Matt can even finish a single sentence, in order to tell Matt that ecological studies are the worst form of epidemiology. At this point it is fair to say that Tucker is breaking rule one as well. It's clear that Tucker is no longer here for an "open, conversation-style" debate.

**Direct contradiction (**[**2:38:50**](https://youtu.be/QGNNsiINehI?t=9530)**):**

Tucker takes the position that ecological studies are of critical importance, superseding all other forms of epidemiology. Yet, only moments earlier, Tucker took the position that ecological studies are the worst form of epidemiological evidence.

Again, we have another relatively simple inconsistency to address. In order for Tucker to make his case against seed oils, he systematically rejected all nutritional epidemiology except for ecological studies. Yet, claiming that ecological studies are the worst form of epidemiology entails a contradiction, as it means that Tucker is simultaneously holding the view that it is both the best and the worst form of epidemiology at the same time. If Tucker wished to resolve the inconsistency, he'd probably have to acknowledge that ecological studies are not the worst form of epidemiology. This can be syllogized like this:

**P1)** If ecological studies are the best form of nutritional epidemiology, then ecological studies are not the worst form of nutritional epidemiology.

**(B→¬W)**

**P2)** Ecological studies are the best form of nutritional epidemiology

**(B)**

**C)** Therefore, ecological studies are not the worst form of nutritional epidemiology.

**(∴¬W)**

**Direct contradiction (**[**2:42:37**](https://youtu.be/QGNNsiINehI?t=9757)**):**

Tucker makes the claim that we can't know whether or not the margarine used in Sydney Diet-Heart Study (SDHS) contained TFA. Moments later, at [**2:44:23**](https://youtu.be/QGNNsiINehI?t=9863) he suggests that it is valid to argue that we absolutely can know the margarine in SDHS did not contain TFA.

If Tucker maintains that it cannot be known whether or not TFA was confounding in SDHS, then he cannot posit that it can be known one way or the other. Either position that Tucker took could work for his argument on this subject, but they're just not compatible with each other, and that can be illustrated like this:

**P1)** If it cannot be known whether TFA was confounding in SDHS, then it is not reasonable to posit that we absolutely know that TFA wasn't confounding in SDHS.

**(¬T→¬S)**

**P2)** It cannot be known whether TFA was confounding in SDHS.

**(¬T)**

**C)** Therefore, it is not reasonable to posit that we absolutely know that TFA wasn't confounding in SDHS.

**(∴¬S)**

  
**Rule violation (**[**2:57:59**](https://youtu.be/QGNNsiINehI?t=10679)**,** [**2:58:53**](https://youtu.be/QGNNsiINehI?t=10733)**):**

Tucker breaks rule three two more times. While discussing a paper that models substitutions of olive oil for various other fats, Matt asks Tucker if he has any problems with the paper. Rather than answering, Tucker starts discussing issues he has with another paper published by the same group investigating dairy fat. Matt attempts to interject, but Tucker continues to discuss previous research on potatoes that was also published by this group.

  
**Rule violation (**[**2:59:44**](https://youtu.be/QGNNsiINehI?t=10784)**):**

Tucker breaks rules two, three, and four again. Matt once again asks Tucker if he has any issues with the olive oil substitution analysis. Rather than answering, Tucker cuts Matt off before he can finish asking his question, and Tucker starts asking why Matt included the paper in his references.

  
**Rule violation (**[**3:00:44**](https://youtu.be/QGNNsiINehI?t=10844)**):**

Tucker once again breaks rule three. Matt asks his yes-or-no question yet again, to which Tucker responds by stating that the mechanisms favour his position. This is just another dodge.

  
**Rule violation (**[**3:00:53**](https://youtu.be/QGNNsiINehI?t=10853)**,** [**3:02:18**](https://youtu.be/QGNNsiINehI?t=10938)**):**

Tucker breaks rule three again, despite it now being enforced by the moderator. The moderator notices that Matt is not getting an answer to his question, and the moderator steps and reminds Tucker of the question being asked. Rather than answering the question, Tucker continues to discuss mechanisms. The moderator again notices that this doesn't answer Matt's question, and implores Tucker to answer. Again, Tucker dodges the question and starts talking about the paper itself, rather than addressing Matt's question about Tucker's interpretation of the paper.

  
**Rule violation (**[**3:06:00**](https://youtu.be/QGNNsiINehI?t=11160)**):**

Tucker breaks rules one and six again. Not even one minute after Tucker grants Matt the floor to make a point about mechanistic data, Tucker interrupts Matt on the basis that Matt's point isn't relevant. Though, only moments before, the moderator gave Matt the floor to complete his point. It is clear that Tucker has no respect for the moderator's wishes, nor the debate parameters.

  
**Rule violation (**[**3:06:52**](https://youtu.be/QGNNsiINehI?t=11212)**):**

Tucker breaks rules one and six again. For the second time, Tucker interrupts Matt when he's been given the floor by the moderator to complete his point.

  
**Rule violation (**[**3:13:34**](https://youtu.be/QGNNsiINehI?t=11614)**):**

Tucker breaks rules two again. Rather than letting Matt complete his point about the olive oil substitution analysis, Tucker interrupts him.

**Direct contradiction (**[**3:15:10**](https://youtu.be/QGNNsiINehI?t=11710)**):**

Tucker concedes that margarine and mayonnaise are not the same thing as isolated vegetable oils. However, at [**3:14:30**](https://youtu.be/QGNNsiINehI?t=11670), Tucker rejects that there are differences between margarine, mayonnaise, and isolated vegetable oils. Another layer of hilarity would be to point out that if Tucker maintains that mayonnaise is the same as an isolated seed oil, he'd be holding the position that corn oil is not a seed oil, but mayonnaise is.

**Definitions:**

S := seed oils are the topic of debate

T := non-seed oil topics are not the topic of debate

m := mayonnaise and margarine

**P1)** If seed oils are the topic of debate, then non-seed oil topics are not the topic of debate.

**(S→(∀x(Tx))**

**P2)** Seed oils are the topic of debate.

**(S)**

**C)** Therefore, mayonnaise and margarine are non-seed oil topics that are not the topic of debate.

**(∴Tm)**

This is where the debate portion of the episode ends, with Matt and Tucker both giving their closing statements. Altogether, Tucker violated the rules at least 50 times and committed at least 18 fallacies, and that's not counting the various hilarious empirical and epistemic claims that Tucker made. For anyone interested in reading more about Tucker's errors, Matt published a comprehensive rebuttal to his personal blog, which can be found on [Matt's blog](https://drmatthewnagra.com/seed-oil-debate-with-tucker-goodrich/). From what I could tell, Matt did not really break the rules at all, except for perhaps a few minor instances when he attempted to interject when Tucker was rambling. Other than that, he conducted himself according to the rules.

Whether you're skeptical, supportive, or unsure of Tucker's work, I hope that this debate, as well as the breakdown contained within this article, gives you a decent perspective on just how bad his arguments actually are. I'm truly failing to imagine any good reasons for why someone with a stable position, which is truly robust to scrutiny, should struggle this hard to stay consistent in a debate. Tucker's performance was truly terrible, and should be eye-opening to anyone who thought that he might have even a scrap of credibility within this domain.

Thanks for reading! If you enjoy my writing and want more content like this, consider pledging to my [Patreon](https://www.patreon.com/thenutrivore)!