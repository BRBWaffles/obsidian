Firstly, I'd like to thank all of the people who volunteered days of their time in order to make this article possible!

Some time ago, the YouTube channel, [What I've Learned](https://www.youtube.com/@WhatIveLearned), release the video called "[Vegan diets don't work. Here's why](https://www.youtube.com/watch?v=MpxgZGnEF7E)", wherein the channel's host, [Joseph Everett](https://twitter.com/JEverettLearned), tells a cautionary tale regarding the health-related pitfalls of so-called "vegan" diets. In this article, Joseph will be exposed as the shameless fabulist that he is. Most of the references are taken out of context or cherry-picked, while others actually directly contradict the claims that they were cited to support.

In order to save on characters, I'm forced to truncate my introduction. So, I'll just take this time to say that you, Joseph, are a complete sophist, and you should be utterly ashamed for misleading the millions of people who consume your content. The effort that it took to write this article is more than you deserve, but its contents are more than deserved by your viewers. Your viewers deserve the truth— not cringey narratives about maverick dentists and cave paintings. With that, let's begin.

**Claim #1 (**[**00:00**](https://youtu.be/MpxgZGnEF7E?t=1)**):**

> It’s well understood that each of these vitamins have many functions in the body, but what does this have to do with nice teeth or looking attractive? Well, research later confirmed that along with things like protein and calcium, these vitamins indeed work together to transport minerals to support proper formation of the bones. [[1](https://pubmed.ncbi.nlm.nih.gov/29138634/)][[2](https://pubmed.ncbi.nlm.nih.gov/33801011/)] And of course your facial structure which includes the dental arch depends on proper development of your facial bones.

This is incredibly unscientific evidence on this hypothesis. The potential for selection bias with this particular body of evidence is enormously high, and on multiple levels. Of course we do not want to assume bad faith on the part of the researcher himself, but no credible scientific establishment would consider this evidence to be anything above laughable on the hypothesis specified. The specified hypothesis is that vitamin K2 (presumably in conjunction with other fat soluble vitamins) is responsible for teeth being straight rather than crooked.

It's not clear at all how the evidence presented even interacts with the hypothesis. These traditional peoples were not screened for vitamin K2 status or intake. There were no prospective analysis. No intake measurements. No biomarker measurements. Just storytelling. In fact, four years ago Joseph published a video that present a far more plausible hypothesis for the appearance of strong jaws and straight teeth in traditional populations eating traditional diets. In the video he cites [a researcher named Clark Spencer Larson from Ohio State University](https://youtu.be/zbzT00Cyq-g?t=591), who claims that stronger jaws and straighter teeth seen in ancestral populations are a consequence of the repetitive chewing of tough, fibrous plant material.

There is also evidence that [straighter teeth tend to be more worn down in traditional populations](https://www.youtube.com/watch?v=ybRD4UPN3D4&t=505s), which supports the chewing hypothesis. [[3](https://www.aegisdentalnetwork.com/cced/2009/06/interdisciplinary-analysis-origins-of-dental-crowding-and-malocclusions-an-anthropological-perspective)] In fact, Joseph references the pictures from Weston A Price's Nutrition and Physical Degeneration as evidence for the chewing hypothesis. [He remarks that the straight teeth in the traditional populations appear more worn down](https://www.patreon.com/file?h=24534404&i=3265485).

Has Joseph changed his view? It's unclear why, as this is a far more plausible hypothesis, and can actually subsume the vitamin K2 hypothesis. If vitamin K2 intake is a correlate for traditional diets, and traditional diets are more likely to be high in fibrous, tough plant foods, and straighter teeth tend to be more worn then crooked teeth, then the chewing hypothesis would appear to be more parsimonious.

Also, if insufficiency of K2 and the other fat soluble vitamins are to blame for crowded teeth due to their role in bone formation, why is there no mention of any of these people have any other issues associated with those sorts of nutritional insufficiencies? Like rickets, blindness, poor skin, or even haemophilia? It doesn't make sense.

**Claim #2 (**[**01:01**](https://youtu.be/MpxgZGnEF7E?t=61)**):**

> If eating zero animal foods improves health so much, why would a 2016 study find that 84% of vegans eventually quit their diet? [[4](https://faunalytics.org/a-summary-of-faunalytics-study-of-current-and-former-vegetarians-and-vegans/#:~:text=84%25%20of%20vegetarians%2Fvegans%20abandon,former%20vegetarians%2Fvegans%20was%20health)]

First of all, the claim of 84% of vegans quitting is just blatantly incorrect according to his own citation, which found a 70% dropout rate for vegans. Second of all, this is a ridiculous question. People stop doing health promoting things for all sorts of reasons, and it probably would have been a good idea for him to read the goddamn reference instead of resorting to conspiracy theories. The study includes an inventory of explanations for participant recidivism, and [the craving of animal products only occurred in the minority of recidivists](https://faunalytics.org/wp-content/uploads/2016/02/Faunalytics-Study-of-Current-and-Former-Vegetarians-and-Vegans-%E2%80%93-Secondary-Findings-.pdf).

Furthermore the study includes a table of motivations for pursuing veganism. Only a measly 1-3% of participants had motivations that were purely ethical in nature. While most motivations were health related. So, it seems like people are generally going on plant-based diets for health related reasons, and abandoning the diets without generally suffering health related costs. This isn't the only reason the question is stupid. Plenty of health promoting behaviours have high recidivism rates, such as exercise.

The Faunalytics study [from the previous year](https://faunalytics.org/wp-content/uploads/2015/06/Faunalytics_Current-Former-Vegetarians_Full-Report.pdf) actually explicitly stated that health did not present a noticeable difficulty for study participants, with the exception of vitamin B12 monitoring. Very few people actually reported any health issues at all, and it's not clear how many of them were actually supplementing B12.

The study had 54 current vegans and 129 former vegans. Of those 129 former vegans, 123 gave reasons. Of those 123, 104 reported no health issues. Of the remaining 19, 7 had no health issues, but rather just "felt" like they weren't getting enough nutrients or were concerned for no particular reason. One seemed to quit because they doubted the benefits. Only 11 reported actual health issues.

Of those 11, 6 were vague like "sick", "lightheaded", "not healthy", and "health issues", and doctors and/or dietitians were rarely, or never, involved to actually confirm that the diet was the issue. In conclusion, if we're being outrageously generous to his position, 6% of those who tried a vegan diet reported any health issues at all. But as we discussed above, it's unclear if this is just a result of poor supplementation practices.

It's not clear why we should accept the implication that a diet is healthy if and only if it can be generally adhered to. There are many examples of diets with poor adherence rates that I don't think Joseph would sign off on being unhealthy, and there are plenty of examples of diets with high adherence rates that Joseph wouldn't consider healthy at all.

**Definitions:**

**D** := (x) diet is great

**G** := people generally adhere to (x) diet

**s** := Standard American Diet

**k** := Ketogenic Diets

**P1)** A diet is great if and only if people generally adhere to it.

**(∀x(Dx↔Gx))**

**P2)** People generally adhere to SAD and not keto.

**(Gs∧¬Gk)**

**C)** Therefore, SAD is great and keto isn't.

**(∴Ds∧¬Dk)**

**Claim #3 (**[**04:59**](https://youtu.be/MpxgZGnEF7E?t=299)**):**

> So it’s interesting to observe that the Dutch are competing with Montenegrins for the tallest people in the world title (animation source: USA data) and they happen to be 2nd and 3rd on the list for the most milk consumed per capita in the world. A study of 105 countries in the journal Economics & Human Biology noted that animal food, particularly dairy, most correlated with increases in height. [[5](https://pubmed.ncbi.nlm.nih.gov/26948573/)]

This is actually the first claim where he actually starts citing peer reviewed research and not just the 1930s equivalent of a blog article with no citations.

Firstly, this is an ecological fallacy. Essentially, Joseph is looking at two temporally concurrent variables and implying a causal relationship. Specially, the research that Joseph cites does not look at individuals, their animal food/dairy consumption in childhood, nor their attained height in adulthood. These heights aren't compared to height projections or heights of peers consuming different diets.

Instead, this research is just looking at the average height of each entire country and the average intake of said foods. Such studies are susceptible to something called the [ecological fallacy](https://en.wikipedia.org/wiki/Ecological_fallacy), meaning what applies on a country average level may not apply on an individual level.

In this graph on average as X increases, so does Y. But if you look at each cluster separately, as X increases Y decreases. A real life example of this is the relationship between smoking and longevity.

If you plot each country's smoking rate and lifespan you'll see that [the more people smoke the longer they live](https://www.thefunctionalart.com/2018/07/visualizing-amalgamation-paradoxes-and.html). This correlation, of course, breaks at an individual level.

I very much doubt that Joseph would approve of this ecological study. [[6](https://pubmed.ncbi.nlm.nih.gov/1140864/)]

The study makes no attempt to adjust for socioeconomic or genetic differences in the countries involved. Richer countries tend to have access to more expensive sources of calories, but also have fewer infections and cleaner water, among many other things conducive to better health. They also tend to eat meat. This doesn't tell us what's happening within each of those populations across the spectrum of meat consumption.

Also it's likely that the reason animal protein correlates strongly with height in Joseph's reference is that it's the main source of protein calories in most diets around the world. For example, in Europe where they essentially consume no plant protein (most plant foods consumed lack significant protein), highly correlated protein (defined by the authors as milk, eggs, pork, beef and potatoes), as well as animal protein and total meat associate more with height than total calories and total protein. If you look at Asia where people eat more plants protein, you'll see total protein and total calories correlate better. Also, inequality adjusted human development index unsurprisingly was more strongly correlated with height than any food.

**Claim #4 (**[**05:30**](https://youtu.be/MpxgZGnEF7E?t=330)**):**

> I recently interviewed Yovana Mendoza who had essentially made a career based around her vegan lifestyle when she had health issues she tried her best to solve them while staying on the diet, using all kinds of supplements and troubleshooting strategies but she eventually had to prioritize her health and quit the diet after 6 years … even though she had every motivation to keep being vegan. Reintroducing animal foods fixed her health issues. Yovana’s case is a peak at how complex it can be to replace animal foods in your diet.

Yovana Mendoza was a so-called "raw" vegan. As you can see from the picture he flashed, she was on a meme starvation diet of only raw food averaging an abysmal ~1000 calories a day.

Raw food diets are associated with many stupid beliefs revolving around self-purification, including extensive fasting periods (49% of study participants), not supplementing B12 (7% took any supplement at all), and enemas (16% of them). [[7](https://pubmed.ncbi.nlm.nih.gov/10436305/)]

Some believe once you are "purified" you lose your period which is a sign you're clean, for example. As you approach 100% raw food, pretty much half of them complain of amenorrhea, probably due to insufficient calories.

As you can see, raw diets associate with considerably low BMI scores.

Rawvana also [made a video](https://youtu.be/hMO4m0rZAB8?t=27) telling us how since she's gotten healthier on her raw vegan diet her "eyes have become greener", so I don't know how much stock we should put in her health advice, Joseph.

As illustrated by [Anna's analysis](https://youtu.be/iqDK_0iaVCE), this seems to be an extremely common pipeline. People go raw vegan, influenced by social media morons, they don't eat enough calories, because their diets aren't formulated correctly, and then they return to some omnivorous diet and claim that veganism failed them. No. Veganism didn't fail you. You played a stupid game and you won a stupid prize.

**Claim #6 (**[**06:18**](https://youtu.be/MpxgZGnEF7E?t=378)**):**

> Take Vitamin A - you might think the average vegan has way more vitamin A because it comes from vegetables like carrots or sweet potatoes - but that’s not vitamin A, that’s beta carotene that has to be converted into vitamin A …and the conversion rate is very poor - about 12:1. [[8](https://pubmed.ncbi.nlm.nih.gov/20237064/)] Though it’s more like 21:1 when you account for the hampering effect of fiber in the diet. [[9](https://pubmed.ncbi.nlm.nih.gov/12221270/)] Not only that, the more you eat, the worse the conversion rate becomes. Further, depending on your genes, your conversion rate could be even lower - [this is the case for me](https://www.youtube.com/watch?v=gWiC4ZCS55Y&t=421s) and for potentially as much as 37% of people of European descent (See also, 57% lower. [[101](https://pubmed.ncbi.nlm.nih.gov/35571879/)][[1](https://pubmed.ncbi.nlm.nih.gov/35571879/)[12](https://pubmed.ncbi.nlm.nih.gov/19103647/)] Actual vitamin A only comes from animal foods or synthetic supplements.

Firstly, let's cut to the chase. Is there a risk of vitamin A deficiency among those only relying on non-retinol sources of vitamin A? That's the question. We'll get to the mechanistic speculation, and why it's so dumb, but for now let's focus on actual outcomes. To my knowledge there are two studies assessing retinol status in vegans after excluding those who supplement. [[13](https://pubmed.ncbi.nlm.nih.gov/24394311/)][[14](https://pubmed.ncbi.nlm.nih.gov/26502280/)]

Both studies showed that vegans had statistically significantly lower retinol status than omnivores, but the differences were not clinically significant. We're free to speculate all we want, but at the end of the day there is no reason to believe that the differences in status actually amount to any differences in expected health outcomes between the two groups.

Now, let's get to the mechanistic fuckery. Joseph claims that there are genetic differences in carotenoid to retinol conversion capacity that could lead to deficiency in some who are relying on carotenoids over retinol. Right off the bat, Joseph's own reference contradicts his claim, as the entire variance in the population sample had fasting plasma retinol within the reference range.

> _...the lowest plasma concentration was 963.8 nM, indicating that all volunteers had adequate serum vitamin A concentrations._

Even the people with the worst impairments maintain adequate retinol status, despite only getting an average of a measly 133mcg/day of retinol. The only thing that changes is that the ratio of beta-carotene to retinol is increased based on the severity of the impairment in conversion rates. That's all.

But, we can take this a step further and actually see what happens when we try to correct vitamin A deficiency using dietary carotenoids in human subjects. [[15](https://pubmed.ncbi.nlm.nih.gov/15883432/)][[16](https://pubmed.ncbi.nlm.nih.gov/17413103/)][[17](https://pubmed.ncbi.nlm.nih.gov/9808223/)][[18](https://pubmed.ncbi.nlm.nih.gov/10584052/)][[19](https://pubmed.ncbi.nlm.nih.gov/15321812/)][[20](https://pubmed.ncbi.nlm.nih.gov/16210712/)] On the whole, eating foods rich in carotenoids reliably improves and/or normalizes vitamin A status. Even foods that have been genetically engineered to have higher levels of carotenoids reliably improve vitamin A status in humans. [[21](https://pubmed.ncbi.nlm.nih.gov/19369372/)] In fact, a newer study published in 2020 found no differences in retinol status between BCO1 genotypes in a population consuming low amounts of preformed retinol. [[22](https://pubmed.ncbi.nlm.nih.gov/32560166/)]

Let me explain what's happening here. These genetic variants likely aren't changing the total amount of retinol converted from carotenoids. Likely the only thing that's happening here is that the conversion curve is changing shape without a change to the area under that curve. Meaning that across these genetic variants, people are capable of converting the same amount of retinol from carotenoids, but the rate at which they make that conversion is slightly longer in those with the so-called impairments. In my opinion, this is the most parsimonious way to reconcile the data.

As a side note, the only cases of vitamin A toxicity (hypervitaminosis A) from whole foods that I could find in the literature involved the consumption of preformed retinol from liver. [[23](https://pubmed.ncbi.nlm.nih.gov/25850632/)][[24](https://pubmed.ncbi.nlm.nih.gov/21902932/)][[25](https://pubmed.ncbi.nlm.nih.gov/10424294/)][[26](https://pubmed.ncbi.nlm.nih.gov/31089689/)][[27](https://pubmed.ncbi.nlm.nih.gov/3655980/)] In one case, a child died from consuming chicken liver pate sandwiches. I could find no case reports of such vitamin A toxicity related to carotenoids.

**Claim #7 (**[**06:58**](https://youtu.be/MpxgZGnEF7E?t=418)**):**

> A 2021 study found vegan Finnish children had insufficient vitamin A and a 2020 German study found vegans to have a lower vitamin A level than omnivores. [[28](https://pubmed.ncbi.nlm.nih.gov/33471422/)][[29](https://pubmed.ncbi.nlm.nih.gov/33161940/)]

In both studies, vegan participants had lower vitamin A status, but again, this seemed to be clinically irrelevant. With the authors of the Finnish study concluding that none of the vegans could be classified as deficient, and the the authors of the German study reporting that Vitamin A status among vegans was still well within the reference range.

We should be starting to see a pattern here, and this should lead us to question the utility of having higher vitamin A status as a consequence of consuming preformed retinol. Even if we granted that you will boast a higher vitamin A status as an omnivore, it's not clear that possessing a higher status actually has any clinical benefit or advantage. I don't know why it would be desirable. Is Joseph prepared to resign himself to affirming that more is simply better? Shall we coin this the Everett Fallacy?

**Claim #8 (**[**07:26**](https://youtu.be/MpxgZGnEF7E?t=446)**):**

> Vitamin D is pretty much only found in animal foods with some exceptions like some mushrooms and some algae. Some people can get enough vitamin D from the sun, but if you live at latitudes above 37 degrees, your skin barely makes any vitamin D from the sun except for in summer. [[30](https://www.health.harvard.edu/staying-healthy/time-for-more-vitamin-d#:~:text=Except%20during%20the%20summer%20months,risk%20for%20vitamin%20D%20deficiency)]

This is one of those times where I won't say anything I will just let his citation speak for itself.

> "Lack of sun exposure would be less of a problem if diet provided adequate vitamin D. But there aren't many vitamin D–rich foods (see chart, below), and you need to eat a lot of them to get 800 to 1,000 IU per day...For these and other reasons, a surprising number of Americans — more than 50% of women and men ages 65 and older in North America — are vitamin D–deficient, according to a consensus workshop held in 2006."

His own citation notes that a massive portion of the population is deficient and the treatment for that is sun exposure or supplementation, not goose liver.

**Claim #9 (**[**07:40**](https://youtu.be/MpxgZGnEF7E?t=460)**)**

> A 2016 Finnish study found vegans’ levels of vitamin D to be 34% lower than omnivores. [[31](https://pubmed.ncbi.nlm.nih.gov/26840251/)]

They had lower intake and therefore lower levels. According to Joseph's previous reference, they should probably be supplementing more.

**Claim #10 (**[**07:52**](https://youtu.be/MpxgZGnEF7E?t=472)**):**

> Unless you’re eating fermented foods, you’ll only find vitamin K2 in animal foods. The richest sources of K2 are going to be animal livers (especially goose liver), egg-yolks, hard cheese and full-fat dairy. Unfortunately New York City made it illegal for schools to serve whole milk in 2006. [[32](https://www.politifact.com/factchecks/2021/jun/07/lorraine-lewandrowski/whole-milk-prohibited-being-offered-new-york-schoo/)] The fermented soybean dish natto does in fact have a ton of K2 and sauerkraut has some too. Vitamin K2 helps put calcium into the right places like your bones and keeps it out of your heart which is thought to be one reason higher vitamin K2 strongly correlated with reduced risk of heart disease. [[33](https://pubmed.ncbi.nlm.nih.gov/34785587/)][[34](https://pubmed.ncbi.nlm.nih.gov/28639365/)]

Here's a quick intro for vitamin K2. There are many different vitamin K isomers (or vitamers). We have vitamin K1 from plants and vitamin K2, primarily from fermented foods or animal products. There is only one vitamin K1, but vitamin K2 has many forms, including MK4 and MK7, which have been studied most. The form of K2 you can not obtain from fermented foods is MK4, but it doesn't appear to be bioavailable at nutritional doses.

First, we'll look at bioavailability of MK4. He mentions animal livers (especially goose liver), egg-yolks, hard cheese and full-fat dairy. I couldn’t find any study on bioavailability of MK4 from foods rather than supplements. But, we can look at studies with doses one could plausibly obtain from diet alone. In probably the best study on this subject, researchers assessed **420 μg of MK-4 compared to 420 μg of MK-7**. [[35](https://pubmed.ncbi.nlm.nih.gov/23140417/)] As you can see from this chart, the only reasonable way to obtain this is with goose liver. [[36](https://pubmed.ncbi.nlm.nih.gov/11356998/)]

The other things he recommended simply don’t have enough. To get this amount it would take **8.9 kilograms of hard cheese, 52.5 liters of whole milk, or 52.4 average eggs.** The amount of goose liver it would take to get this dose of MK-4 is roughly **115g of liver,** however this has **over 10,000μg of retinol, the upper limit is 3000μg** and hypervitaminosis A is no joke.

But regardless, let’s say someone managed to eat this amount of MK-4 regularly. Is it actually absorbed? It wouldn't appear so.

At no time point after the oral administration of 420μg of MK4 is it actually detectable in the blood.

In summary, Joseph suggests food items like hard cheese and whole milk for vitamin K2 when they have abysmal amounts that probably aren't even absorbed. Worth noting that the natto he dismissed contains MK7 which is actually bioavailable, as evidenced by the same reference. Natto also contains it in concentrations over **700 times that of hard cheese**, which is the richest source of MK7 out of the foods he listed.

**Claim #11 (**[**08:25**](https://youtu.be/MpxgZGnEF7E?t=505)**):**

> Speaking of all these nutrients for the skeleton, a 2021 polish study found vegan children to have weaker bones and were 3cm shorter than their meat eating counterparts. [[37](https://pubmed.ncbi.nlm.nih.gov/33740036/)]

What Joseph didn't mention was that 29% of the vegan children did not consume vitamin B12 supplements or fortified foods, and only 32.7% used vitamin D supplements. Those who did actually consume vitamins B12 and D had comparable B12 and D status to omnivores. It's entirely plausible, if not probable, that this high percentage of non-supplementing participants was enough to drag down the average for the entire group.

I mention this because it's clear that a large chunk of the cohort was not actually supplementing vitamin B12 and vitamin D. Both of which are nutrients that are strongly associated with normal growth. There was also little to no consideration for other dietary variables, and the analysis itself is cross-sectional with extremely small sample-sizes.

Perhaps also worth noting are the similar lean mass, lower fat mass, and preferable LDL-C and hs-CRP values of the vegans. But he didn't mention that.

**Claim #12 (**[**08:27**](https://youtu.be/MpxgZGnEF7E?t=507)**):**

> A British study and a Dutch study also found vegan children to be shorter. [[38](https://pubmed.ncbi.nlm.nih.gov/3414589/)]

Let's start with the British study. These results are trivially explainable by the lower caloric intakes for vegans in his reference.

Also, while the vegans tended to fall below the 50th percentile for weight, the vast majority experienced normal growth, and the lower weight (and in some cases height) may be attributable to the lower caloric intake. The authors actually take note of this, but Joseph didn't mention it.

The authors also suggest that the lower fat intake may be the reason they had a lower caloric intake, and go on to state that dense fat sources can be important for children for that reason (eat your avocados, kids). They concluded that children can grow up to be "normal" and that there's no evidence of impairment to cognitive development.

Just as a side note, it's also funny to mention that the vegan diets were more nutrient dense than the average omnivorous UK diet. Even more hilariously, they explicitly state that there are healthy and unhealthy versions of vegan diets and that clowns will run with the occasional case reports on unhealthy vegan kids fed **inappropriate** diets. These authors were calling out Joseph in 1988.

On to the Dutch study. Firstly, the so-called "vegan" diet was actually a type of meme "vegan" diet called the macrobiotic diet. This diet is highly restrictive and often very low in protein. This is not representative of what a well-balanced "vegan" diet would look like.

Thus far, the studies he cited either haven't supported his claim or provide clear reasons for why there may be differences in growth. With the reasons provided not being intractable characteristics of vegan diets themselves. He also left out the studies where we do see similar growth between vegan and omnivorous kids with adequate diets.

In the Farm Study was a 1989 study involving children ages four months to ten years residing in a community in Tennessee. [[39](https://pubmed.ncbi.nlm.nih.gov/2771551/)] 75% of mothers were vegan through pregnancy and 73% of children were vegan since birth. These mothers consumed well balanced diets with fortified foods (soy milk and nutritional yeast), which they fortified themselves!

> The Farm community was generally well informed regarding issues related to vegetarianism, including complementing different protein sources, for example, grains and legumes and nonanimal sources of vitamins and minerals. Until 1983, the population followed a vegan diet, with soybeans being their primary source of protein. Supplements of vitamins A, D, and B12 were added to the soy milk produced on The Farm. Nutritional yeast (containing vitamin B12) and other vitamin and mineral supplements were also used. In the fall of 1983, some members of the community introduced eggs and dairy products into their diets.

Across vegan children, growth was skirting the 50th percentile on average. This is exactly where these growth trajectories should be.

Same for weight.

We also have the VeChi Diet Study. [[40](https://pubmed.ncbi.nlm.nih.gov/31013738/)] Vegan and omnivorous children had similar caloric intakes. Omnivores had the highest protein, fat, and added sugar intake, while vegans had the highest total carb and fibre intake. In fact, the vegans were still able to consume a median of 2.25g of protein per kg bodyweight. While there were a few outliers in each group, growth was generally very similar overall.

There are explanations for the children who may have been stunted or wasted, and they're nothing that is necessarily inherent to vegan diets themselves. These reasons include: short parents, inadequate caloric intake, exclusively breastfeeding longer than recommended (probably due to hippie vegan parents doing dumb hippie things).

> Regarding these eight children classified as stunted, two had very low reported energy intakes (534 kcal/day and 598 kcal/day, respectively), and both were exclusively breastfed >6 months (7 and 9 months, respectively). An overly long period of exclusively breastfeeding can result in an insufficient intake of complementary foods and inadequate low TEI because, after a certain age, human milk alone cannot supply energy and all nutrients in adequate amounts to meet a child’s requirements [71]. Furthermore, one of the two children as well as three other children classified as stunted had parents with a BH (mother: 161 cm, father: 170 cm) below the German average (167 cm and 180–181 cm of 25–55-year-old women or men, respectively) that might have influenced the child’s BH. The other child with low energy intake was also categorized as SGA, which is considered a risk factor for stunting [72]. Another stunted child was categorized as SGA, and its birthweight was only slightly above 2500 g (2545 g). The seventh child was exclusively breastfed for twelve months (the eighth child was breastfed for eight months), and it had parents with BHs (mother: 160 cm, father: 178 cm) below the German average.

We also have data on intake of micronutrients and fatty acids in the 1-3 year olds in this cohort. [[41](https://pubmed.ncbi.nlm.nih.gov/34855006/)] All diet groups had low iodine intake, and the vegans had the lowest intakes of saturated fat, cholesterol, and DHA (although omnivores had low intakes too), but higher intakes of ALA and LA. They also mention that vegan and vegetarian children had the more favourable intakes of several micronutrients and fatty acids.

In addition to evaluating nutrient intake, they also measured status in 6-18 year olds. [[42](https://pubmed.ncbi.nlm.nih.gov/34069944/)] Ultimately, the results are very similar to the those of the VeChi Diet Study that was previously mentioned, with preferable blood lipids in the vegans.

A further study on mothers consuming various dietary patterns supports that a vegan diet can support "normal and physiological growth" through pregnancy and the first year of life. Also of note, 95.2% (20/21) of the vegan mothers took supplements through pregnancy. [[43](https://www.minervamedica.it/en/journals/minerva-pediatrics/article.php?cod=R15Y9999N00A21041604)]

So some of Joseph's own references suggest vegan diets can support growth and development, and that is consistent with other research where vegans are consuming a balanced and appropriately supplemented diet.

**Claim #13 (**[**09:34**](https://youtu.be/MpxgZGnEF7E?t=574)**):**

> Most vegans know they need to supplement B12 which is very important for proper brain function. Yet, one study looking at B12 status in vegetarians and vegans found that 7% of vegetarians and 52% of vegans were not getting enough B12. [[44](https://pubmed.ncbi.nlm.nih.gov/20648045/)] However, in another study with a more sensitive testing method - they found a whopping 77% of vegetarians and 92% of vegans had insufficient B12 whereas only 11% of omnivores did. [[45](https://pubmed.ncbi.nlm.nih.gov/12816782/)] Perhaps these B12 supplements don’t work exactly like animal foods do. Also it can take years to deplete the body’s B12 store, so people can be lacking B12 for a while without realizing it.

Right off the bat, 81% of vegans did not supplement in the first study. In the second, the authors did not assess how many of them were supplementing, but we know 59% supplemented "B vitamins".

Joseph then concludes (from two studies were the majority did not supplement B12) that B12 supplements "don’t work exactly like animal foods do". If Joseph wanted to know if B12 supplements work at all he could've simply read his previous reference, Elorinne, et al. (2016). [[31](https://pubmed.ncbi.nlm.nih.gov/26840251/)] Had he done so, he would have noticed that 91% of that cohort took B12 supplements, and as you'd expect they were not B12 deficient.

If Joseph wanted to know if B12 supplements work differently than animal foods, he could turn his attention to this interventional study that found that fortified cereal was more effective at raising B12 than pork. [[46](https://pubmed.ncbi.nlm.nih.gov/31519167/)]

Also, various doses of cheapo, vanilla-ass cyanocobalamin rescue vitamin B12 deficiency in clinically deficient vegans. [[47](https://pubmed.ncbi.nlm.nih.gov/29499976/)] This is confirmed by clinically meaningful reductions in both methylmalonic acid and total homocysteine. If Joseph knows of any better biological correlates for B12 absorption and utilization, as well as evidence that they're uniquely affected by animal foods, I'd love to hear from him about it.

**Claim #14 (**[**11:38**](https://youtu.be/MpxgZGnEF7E?t=698)**):**

> ...another possibility is the vegan diet has impaired digestion.

The term "digestion" here is so unclear and nebulous, that it is uncertain what exactly to look for in the literature in order to test the hypothesis. However, if we assume that the hypothesis is referring to any symptoms related to digestion, we should expect to see increased rates of digestion-related symptoms, as reported as adverse events, in any of the randomized controlled trials that have been done on so-called "vegan" diets. But we can find close to none, which calls into question whether or not this is even an effect, let alone a generalizable effect.

**Claim #15 (**[**12:25**](https://youtu.be/MpxgZGnEF7E?t=745)**):**

> ...many [vegans] do quit the diet because of health issues.

If "many" is meant to be some sort of generalization, then his claim is straightforwardly contradicted by a study discussed in one of his own references on vegan recidivism rates, the 2015 Faunalytics study.

> Interestingly, health did not present a noticeable difficulty for study participants, with the exception of vitamin B12 monitoring. 2) Consider increasing awareness about the importance of B12: a far greater percentage of former (76%) than current (42%) vegetarians/vegans never had their B12 levels checked while they were adhering to the diet.

His only evidence for this claim is a montage of ex-vegan YouTubers who already have a demonstrable history of lying to people's faces. What the fuck are we even doing here, Joseph? These people were telling their audiences that they had newfound health on a vegan diet, and now they are once again telling their audiences that they have newfound health, but on a non-vegan diet. Joseph expects us to believe them. I can only guess that's because he is an idiot and doesn't understand what evidence is.

**Claim #16 (**[**13:21**](https://youtu.be/MpxgZGnEF7E?t=801)**):**

> A 2012 study found in 63 patients with constipation, reducing fiber intake improved symptoms but eating a zero fiber diet completely eliminated all symptoms. [[48](https://pubmed.ncbi.nlm.nih.gov/22969234/)]

This is a category mistake. Constipation isn't indigestion. Digestion precedes stool formation and colonic transit. Also, there is no mention of vitamin B12 deficiency or its related symptoms among the subjects in the reference Joseph provided. It's not clear how this is interacting with the claim.

I'll briefly entertain the tangent, though. The trial that Joseph references is not easily generalizable, because the subjects had idiopathic constipation. It's also not clear at all what this has to do with "vegan" diets. Additionally the researchers did not actually assess fibre intake. Fibre intake was assumed based on the researchers instructions to the subjects, which naturally is a very poor measurement to fibre intake.

Meanwhile, we see very consistently that increased consumption of fibre associates with a decrease in bowel transit time and improving symptoms of constipation. [[49](https://pubmed.ncbi.nlm.nih.gov/26950143/)][[50](https://pubmed.ncbi.nlm.nih.gov/35816465/)]

**Claim #17 (**[**13:46**](https://youtu.be/MpxgZGnEF7E?t=826)**):**

> As for B12, you need to have strong enough stomach acid to properly absorb it and dietary fiber is known to weaken the stomach acid. [[51](https://pubmed.ncbi.nlm.nih.gov/2823869/)][[52](https://www.tandfonline.com/doi/abs/10.3109/00365528709095891)][[53](https://pubmed.ncbi.nlm.nih.gov/6095709/)]

For the former claim, there is no reference. But what Joseph is probably referring to here is the requirement for a lower stomach pH in digesting food normally in general. Without a sufficiently acidic stomach acid, it is true that vitamin B12 may not be adequately liberated from a given food matrix.

However, this doesn't apply to supplements (as supplements do not have a food matrix that requires a particularly low pH stomach acid to digest), and therefore doesn't apply to "vegan" diets. Also, we can easily see from previously cited research that vegans can achieve and maintain normal B12 status on high fibre diets.

In fact, you can even absorb B12 adequately and rescue frank B12 deficiency syndromes by shoving it directly up your ass. [[54](https://pubmed.ncbi.nlm.nih.gov/5924495/)] Sublingual B12 supplements are effective in rescuing B12 deficiency. [[55](https://pubmed.ncbi.nlm.nih.gov/14749150/)] Both of these methods bypass the stomach completely.

**Claim #18 (**[**13:56**](https://youtu.be/MpxgZGnEF7E?t=836)**):**

> So the context matters - what else are you getting with the nutrients? For example, there are plenty of plant sources of iron, but plant foods like whole grains, legumes and nuts contain phytic acid that impairs iron absorption. [[56](https://pubmed.ncbi.nlm.nih.gov/12936958/)] Spinach is thought to be a great source of iron but you can only absorb 2% of it because of the oxalate in it. [[57](https://pubmed.ncbi.nlm.nih.gov/1745900/)]

Once again, Joseph shows that he either doesn't read the studies he cites or ignores where they contradict him. From the first study:

> Iron deficiency anemia appears to be no more prevalent among vegetarian women than among nonvegetarian women...Thus, although several reports indicate that vegetarians in Western societies have lower iron stores and may have lower hemoglobin concentrations, they do not indicate a greater incidence of iron deficiency anemia...Lowering iron stores without increasing the risk of iron deficiency anemia may confer a health advantage when vegetarian diets are chosen from an abundant food supply.

Joseph further shows that he is really good at constructing strawmen. What official public health authority in any developed country actually recommends spinach as a significant source of iron? Regardless, it's also a non-sequitur that just because spinach has a particularly poor bioavailability of iron that there exist no vegan sources of iron with good bioavailability. There even exist other green vegetables with good bioavailability, such as broccoli and cabbage. [[58](https://pubmed.ncbi.nlm.nih.gov/31394334/)]

**Claim #19 (**[**14:00**](https://youtu.be/MpxgZGnEF7E?t=840)**)**

> Then, where the heme-iron in animal foods is very easily absorbed, the non heme iron in plants and supplements is quite poorly absorbed. Two different literature reviews suggest that vegans are at greater risk for iron deficiency than omnivores [[59](https://pubmed.ncbi.nlm.nih.gov/28319940/)][[60](https://pubmed.ncbi.nlm.nih.gov/30783404/)].

No citation was provided for this claim and he says it as if it follows logically from what he said beforehand. Which it doesn't. Phytic acid in isolation impairing iron absorption in some plant foods high in phytic acid having low bioavailability of iron also doesn't imply that iron is poorly absorbed from all plant sources.

Firstly, despite whole wheat flour being higher in phytic acid than white wheat flour, it has better bioavailability of iron. [[61](https://pubmed.ncbi.nlm.nih.gov/10655952/)] Granted this is in animal models, but this is evidence Joseph has been known to accept in the past.

Secondly, other compounds that are common in plant foods but are absent (or virtually absent) from animal foods may have pleiotropic effects that mitigate or even overcome the effect of phytates on iron absorption, with vitamin C probably being the most prominent example. In regards of counteracting phytic acid, 50mg (less than an orange worth) does more than 50g of meat. [[62](https://pubmed.ncbi.nlm.nih.gov/2911999/)]

It also doesn't follow that one needs to consume animal products to meet iron needs, which Joseph heavily implies. Increased intake through diet and/or supplementation are clearly possible.

First of all, both the literature reviews Joseph cites are looking at vegetarians, not vegans. I thought his video was on so-called "vegan" diets, not vegetarianism. Even the authors of his own references disagree with his interpretation. Here's a quote from Pawlak, et al. (2017):

> Findings regarding individuals who adhere to specific vegetarian diet type, such as vegans, were underrepresented and thus, conclusions regarding iron status among such individuals were not possible.

And from another of Joseph's references, Pawlak, et al. (2018):

> Considering the limitations, it is reasonable to conclude that the findings are most likely not representative of the entire vegetarian populations nor are they representative of any one specific vegetarian subgroup. 3 of the studies were published in the 1990s and one in 1982. It is reasonable to assume that iron fortification practices have changed since the time of food availability has improved due to globalization. Consequently, this makes the generalization of the findings difficult

So, what is the point in wrongly applying or generalizing these findings with already questionable external validity from vegetarians onto vegans? That being said, just because the studies Joseph cites don't appropriately support his claim does not imply that his claim is wrong. For that, we need to go deeper.

There are three studies that were published before the two reviews that Joseph cited that differentiate between vegans and vegetarians, while also comparing them to omnivores with measurements of plasma ferritin. There are four that were published afterward. It would be valuable to go through them one by one.

In Schüpbach, et al. (2017), omnivores had higher plasma ferritin, but there was a lower percentage of vegans than omnivores in the range of deficiency, while vegans had almost double the iron intake of omnivores. [[14](https://pubmed.ncbi.nlm.nih.gov/26502280/)]

The more curious finding with respect to iron was that for omnivores and vegetarians, the correlation between iron intake and plasma ferritin was fairly strong and statistically significant (r = 0.247, p = 0.030; r = 0.331, p = 0.030, respectively), but not so for vegans (r = 0.168, p = 0.281).

The more concerning finding outside of iron was that despite a similar zinc intake across groups almost half of vegans were below the normal range compared to just 10% of omnivores. However, almost 60% of omnivores had folate levels below the normal range as well.

Vegans in Elorinne, et al. (2016) had much lower plasma ferritin than non-vegetarians despite higher intake. [[31](https://pubmed.ncbi.nlm.nih.gov/26840251/)] It's unclear whether there was a single subject with low ferritin and this result was not discussed anywhere in the text. However, there were funny sections about selenium in fertilizers and how a low LA intake might help vegans convert LNA to DHA.

Less than half the vegan children in Desmond, et al. (2021) used B12 supplements, almost a third got neither B12 supplements nor B12 fortified foods. [[37](https://pubmed.ncbi.nlm.nih.gov/33740036/)] Under their second adjustment model, the vegan children had 25% lower ferritin levels. 30% of the vegan children had ferritin levels below the cut-off, compared to 13% of the omnivore children. 2% and 6% of vegan children had moderate and mild iron deficiency anemia, respectively, compared to none of the omnivore children. The authors were (rightfully) much more concerned about the differences in bone mineral content and B12 deficiency. They also get props for looking at cardiovascular risk factors in children.

Another study, by Slywitch, et al. (2021), gets props for a 10 year long recruitment period and for differentiating between menstruating and non-menstruating women. [[63](https://pubmed.ncbi.nlm.nih.gov/34578841/)] However, for unknown reasons, these authors only differentiated between vegans and vegetarians for the analysis on BMI but not for ferritin. To be fair, they were more interested in how inflammation may mask iron deficiency, but still. It would have been nice to have that data.

Weikert, et al. (2020) represents one of the better studies on the subject, because the groups were similar in their characteristics and all but one vegan were actually using supplements. [[29](https://pubmed.ncbi.nlm.nih.gov/33161940/)] Plasma iron and ferritin levels were on average lower in vegans than omnivores, but not statistically significantly so, with the vegans having a 50% higher iron intake. 11% of vegans showed signs of iron deficiency compared to 8% of omnivores. Vegans who substituted iron had higher average ferritin than omnivores who did not substitute iron.

Another study by Alexy, et al. (2021) found statistically significantly lower plasma ferritin than omnivores despite 50% higher iron intake, but prevalence below the cut-off was called "low" by the authors and thrown into the supplemental. [[42](https://pubmed.ncbi.nlm.nih.gov/34069944/)] Weirdly, the authors were concerned about the high prevalence of B2 deficiency in all groups, using a cut-off of 199 µg/l.

In Wilsen, et al. (1999), despite the 50% higher iron intake, the vegans had almost 50% lower serum ferritin than omnivores, with 20% falling below 12 ng/ml. [[64](https://pubmed.ncbi.nlm.nih.gov/10201799/)] The difference in hemoglobin between vegans and omnivores was also statistically significant. On the other hand, 20% of omnivores had ferritin levels elevated above 200 ng/ml, which is indicative of inflammation.

In conclusion, even though Josephs citations don't support his claim, there is some truth to the matter. The literature is in unanimous agreement that iron in vegan diets has much lower bioavailability, but this is not due to phytates alone. Dietary fiber and polyphenols are important as well. Of course this is not enough to infer an outcome such as iron deficiency, let alone iron deficiency anemia, since vegans also have a much higher iron intake than omnivores. Furthermore, in many of the studies lower iron status is observed without a particularly increased risk of deficiency.

Vegans, especially those who menstruate, might want to err on the side of caution by regularly getting their ferritin checked whenever they get their B12 checked, and then supplementing accordingly. Pretty much the same is true for vegetarians, or even omnivores for that matter. But what these studies show overall is that no matter their diet, people kind of suck at hitting reference ranges of biomarkers for all nutrients.

**Claim #20 (**[**14:30**](https://youtu.be/MpxgZGnEF7E?t=870)**):**

> Now before we continue, why should we assume a meat containing diet was the natural default for humans rather than a plant-based diet? Well, to get a wide variety of nutrients, vegans have to eat a huge variety of modern fruits and vegetables, but the fruits and vegetables early humans had access to were nothing like modern ones. Before cultivation, they had far less actually edible material and far more fiber and seeds. Paleoanthropologist Daniel Lieberman has said that the sweetest fruit available would have been no sweeter than a modern day carrot.

Previously having less edible material is trivially true of animal foods produced from modern animal agriculture.

**Claim #21 (**[**14:58**](https://youtu.be/MpxgZGnEF7E?t=898)**):**

> We have stable isotope studies finding we ate pretty much whatever meat we could get our hands on… our earliest art is cave paintings of hunts. Lastly, the brain is a disproportionately energy expensive organ, hogging 20% of our oxygen and calories. [[65](https://pubmed.ncbi.nlm.nih.gov/30872714/)][[66](https://pubmed.ncbi.nlm.nih.gov/12149485/)] Our guts (also energy expensive) shrank in size to allocate more resources to the brain. [[67](https://pubmed.ncbi.nlm.nih.gov/22174868/)] Thus, to fuel our big brains, the more energy efficient animal fat became favored over fibrous plants that took time and energy to chew and digest.

What point is Joseph trying to make here? We could try to formalize it, perhaps.

**Definitions:**

**B** := (x) food make brain big

**G** := (x) food gud

**O** := ooga booga

**m** := meat

**p** := plants

**P1)** If food gud, then food make brain big.

**(∀x(Gx→Bx))**

**P2)** Meat gud.

**(Gm)**

**P3)** Plants no make brain big.

**(¬Bp)**

**P4)** Ooga booga.

**(O)**

**C)** Therefore, meat make brain big, plants no gud, ooga booga.

**(∴Bm∧¬Gp∧O)**

**Claim #22 (**[**15:34**](https://youtu.be/MpxgZGnEF7E?t=934)**):**

> Most people are not aware that animal foods are packed with far more of a huge variety of nutrients, especially ones critical for brain function. This may have a role in why a 2021 study found people who don’t eat meat to have significantly higher risk of depression and anxiety. [[68](https://www.tandfonline.com/doi/full/10.1080/10408398.2020.1741505)]

Again, in his typical style, the review Joseph linked contained only one randomized controlled trial and guess what that one trial showed. I'll quote the authors directly.

> Restricting meat, fish, and poultry improved some domains of short-term mood state in modern omnivores. To our knowledge, this is the first trial to examine the impact of restricting meat, fish, and poultry on mood state in omnivores.

Also, the authors overlooked a couple more trials, and didn't even mention them. I personally don't really see a reason for their exclusion. [[69](https://pubmed.ncbi.nlm.nih.gov/24524383/)][[70](https://pubmed.ncbi.nlm.nih.gov/20389060/)] They also rated the one RCT that they did include as "low quality" without providing a decent justification. In both trials, a benefit of so-called "vegan" diets was observed.

Not only that, but in a 2022 systematic review including more studies, they note that higher quality studies and studies that can distinguish temporal relationships (such as RCTs and cohort studies) the effects of plant-based diets on mental health are either positive or non-significant. This review contained two RCTs, one on vegetarians (which can be interpretated as a meat-restriction intervention, so it is still relevant) which showed improved confusion and stress and one on vegans which showed improvements in depression indicators.

Lastly, one of the two "high quality" studies in the review that Joseph cited evaluated the temporal relationship and suggests that the so-called vegan diet came **after** the development of mental health issues. [[71](https://pubmed.ncbi.nlm.nih.gov/22676203/)] When vegan, vegetarian, and semi-vegetarian diets are separated, meta-analyses suggest that semi-vegetarian diets are associated with higher prevalence of depression, while there is no statistically significant relationship between "vegan" or vegetarian diets and depression. [[72](https://pubmed.ncbi.nlm.nih.gov/33822140/)]

Here is the prevalence of depression forest plot from that meta-analysis.

And mean depression scores.

Also, just for flavour, I'll point out that Joseph's reference was funded by the beef industry.

> This study was funded in part via an unrestricted research grant from the Beef Checkoff, through the National Cattlemen’s Beef Association. The sponsor of the study had no role in the study design, data collection, data analysis, data interpretation, or writing of the report.

**Claim #23 (**[**17:17**](https://youtu.be/MpxgZGnEF7E?t=1037)**):**

> The peoples Weston price studied had an intuitive understanding of the importance of nutrient dense foods - especially in pregnancy and childhood. Even without a nutrition label, they knew that certain animal foods encouraged proper robust growth.

There is an implicit claim here that animal foods have some kind of special importance for pregnancy and childhood. Ultimately, this is just an anecdote, barely distinguishable from an appeal to authority. So, let's throw one back from the American Dietetic Association:

> ...appropriately planned vegetarian, including vegan, diets are appropriate for all stages of the life cycle, including pregnancy, lactation, infancy, and childhood

To be clear, I think hinging the truth value of any claims about the health value of either co-called "vegan" diets or omnivorous diets on either of these opinions is incredibly cringe. It's just not clear to me why we can't just counter anecdotes with anecdotes.

**Claim #24 (**[**18:12**](https://youtu.be/MpxgZGnEF7E?t=1092)**):**

> As Michael Pollan has argued in his book In Defense of Food - we frequently fall victim to this concept of 'nutritionism' that we don’t necessarily need whole foods, we just need their components.

Joseph, what does it mean to fall victim to a concept that is arguably true? Also, what do you mean by need? Is there some sort of necessity relation between whole foods and human health that you'd like to tell me about?

**Claim #25 (**[**18:34**](https://youtu.be/MpxgZGnEF7E?t=1114)**):**

> According to a 2012 study, despite taking prenatal supplements, 58% of pregnant woman had iron levels below normal. [[73](https://pubmed.ncbi.nlm.nih.gov/22113871/)]

Joseph makes this claim to vaguely support the notion that we should not rely on supplements. However, the study cited to support this claim is a study of 19 pregnant women who were given labelled iron supplements in order to better characterise placental iron transport. It is not a large scale study of prevalence of iron deficiency in pregnant women who take prenatal supplements and therefore does not support the claim.

On the contrary, a meta-analysis involving 43274 women shows that preventative daily oral iron supplementation reduces iron deficiency and iron deficiency anemia at term by 57% and 70%, respectively. [[74](https://pubmed.ncbi.nlm.nih.gov/26198451/)]

**Claim #26 (**[**18:40**](https://youtu.be/MpxgZGnEF7E?t=1120)**):**

> 90% of Americans are not getting enough [choline]. [[75](https://pubmed.ncbi.nlm.nih.gov/30853718/)]

While it is clear that choline is an essential nutrient for proper liver, muscle, and brain function, and this study does estimate 90% of Americans to fall short of the adequate intake, the same study caveats that:

> Current intakes cannot be deemed inadequate based upon the [adequate intake] value alone. Although [adequate intakes] may be useful in guiding individual dietary plans, by definition, they are established when the evidence is insufficient to calculate an [estimated average requirement]. Therefore it is not possible to conclusively assess the risk of inadequacy in a population.

In short, the way Joseph presents this study is essentially fear-mongering.

**Claim #27 (**[**18:47**](https://youtu.be/MpxgZGnEF7E?t=1127)**):**

> Choline from egg-yolk is better absorbed than choline from the common supplement, choline bitartrate. 150 calories of egg yolk (a little over 2 eggs) are enough to meet ones adequate choline intake.

While choline absorption from eggs is particularly high, choline intake in the American population is driven by egg intake and vegetarians have the lowest intakes among the US population, this does not support the notion that choline requirements cannot be met with proper supplementation.

On the other hand, the same authors as from the paper before state that choline is a precursor to betaine, another 'methyl donor' largely present in plant foods such as wheat bran, beets and spinach. Higher intakes of betaine may spare some of the potential negative consequences of low choline intake among vegetarian populations. [[76](https://pubmed.ncbi.nlm.nih.gov/31385730/)]

The main concern with choline deficiency is non-alcoholic fatty liver disease (NAFLD). If choline deficiency was an issue among vegans we'd expect a positive correlation between higher adherence to so-called "vegan" diets and NAFLD, an inverse correlation between consumption of animal foods and NAFLD, no change or an increase in liver enzymes for NAFLD patients being told to eat a "vegan" diet, and lower odds of NAFLD in people who eat more than 2 eggs per day (calculated from the calories from egg yolk needed to achieve adequate choline intake as given by Joseph (keep this number in mind).

However, we see the opposite. Higher adherence to plant-based diets, especially healthful plant-based diets is associated with lower likelihood of fatty liver. [[77](https://pubmed.ncbi.nlm.nih.gov/30578029/)][[78](https://pubmed.ncbi.nlm.nih.gov/36235752/)] Higher consumption of animal foods correlated with a higher prevalence of NAFLD, while a higher consumption of grains and vegetables was correlated with a lower prevalence of NAFLD in Chinese Adults. [[79](https://pubmed.ncbi.nlm.nih.gov/26083112/)] Also, in a pilot study with 26 NAFLD patients who agreed to eat a vegan diet for six months 20 normalised their liver function tests, independently of their improvements in body weight. [[80](https://pubmed.ncbi.nlm.nih.gov/33548123/)] Meanwhile, in a case-control study of 951 patients who had been referred to hepatology clinics, those participants who consumed 2-3 eggs per week (not per day) had over 3-times higher odds of having NAFLD than those who consumed less than 2 eggs. [[81](https://pubmed.ncbi.nlm.nih.gov/28443155/)]

It doesn't actually seem like consuming choline from animal products, particularly eggs, is even an effective means of avoiding NAFLD. This may be because animal-derived sources of choline are also typically high in fat, which would likely increase the choline requirement.

Consuming more fats means activating the bodies lipid transport system, such as lipoproteins, to a greater degree. Phosphatidylcholine is one of the primary phospholipids that make up the membranes of lipoproteins. The more fat you eat, the more choline you require. This is basically why choline deficiency can cause fatty liver.

So, even from a mechanistic standpoint, it would be unclear whether or not high fat animal-based diets would have an advantage over low fat plant-based diets.

**Claim #28 (**[**19:02**](https://youtu.be/MpxgZGnEF7E?t=1142)**):**

> Dietary calcium reduces risk of heart attack, calcium supplements increase the risk of heart attack.

This claim, which Joseph bases off of a single study that he fails to cite in his substack document, is not supported by the larger literature. A multitude of meta-analyses of prospective cohort studies with combined sample sizes of hundreds of thousands of patients show an inverse correlation of total calcium intake and all-cause mortality in the short term (≤ 10 years), but no statistically significant correlation in the long term (> 10 years). This difference is likely explained by the positive correlation between both dietary and supplemental calcium with cardiovascular mortality, following a U-shaped dose response curve that becomes statistically significant for intakes exceeding ~1200 mg/day. [[82](https://pubmed.ncbi.nlm.nih.gov/25912278/)][[83](https://pubmed.ncbi.nlm.nih.gov/33382441/)][[84](https://pubmed.ncbi.nlm.nih.gov/25252963/)]

  
The only statistically significant finding of the only meta-analysis of RCTs of calcium supplementation was a 9% increased risk of coronary heart disease incidence, driven by dosages exceeding 1000 mg/day in men, but no statistically significant difference in all-cause mortality. [[85](https://www.tandfonline.com/doi/full/10.1080/07315724.2019.1649219)]

  
Therefore, calcium supplementation in a range similar to reasonable dietary intakes should be regarded as safe. It is also effective at improving bone mineral density in both preadolescent children and adults with osteoporosis. [[86](https://pubmed.ncbi.nlm.nih.gov/36808216/)][[87](https://pubmed.ncbi.nlm.nih.gov/36810543/)]

**Claim #29 (**[**19:12**](https://youtu.be/MpxgZGnEF7E?t=1152)**):**

> Vegans have weaker bones and a 43% higher risk for fractures than omnivores. [[88](https://pubmed.ncbi.nlm.nih.gov/33222682/)]

On average, vegans do in fact tend to have a lower bone mineral density and higher hip fracture risk compared to non-vegans. This alone is uninteresting, though. We know that, on average, omnivores have a higher BMI than vegans. In this study the difference in BMI between the groups was 2.4 points, in the general population it is more than double that.

BMI has been shown to causally increase bone mineral density, which in turn has been shown to causally decrease risk of fracture. This effect mediation has been found independently in observational studies, and the differences in bone mineral density seem to align with what we would expect based on the differences in BMI. [[89](https://pubmed.ncbi.nlm.nih.gov/33784428/)][[90](https://pubmed.ncbi.nlm.nih.gov/36260985/)][[91](https://pubmed.ncbi.nlm.nih.gov/24862213/)][[92](https://pubmed.ncbi.nlm.nih.gov/15817133/)]

So the question that is interesting here, from a causal perspective, is whether vegans have weaker bones and higher risk of fracture independently of BMI and other important confounders such as calcium and vitamin D. Vitamin D has not been taken into account at all in the EPIC-Oxford study. BMI and dietary calcium were adjusted for, but only via categorisation, which is known to bias results when examining continuous variables with non-linear responses. [[93](https://pubmed.ncbi.nlm.nih.gov/17938055/)]

In the Adventist Health Study 2, a better prospective cohort study (more recent, bigger sample size, higher proportion of vegans, longer follow-up, etc), the same question was investigated. When adjusting for all known confounders (and unlike EPIC-Oxford using a proper adjustment model), only female vegans who did not supplement calcium and vitamin D were at a higher risk of hip fractures. [[94](https://pubmed.ncbi.nlm.nih.gov/33964850/)]

On the other hand, in another prospective cohort study, a higher ratio of animal protein to plant protein was found to increase rates of bone loss and fracture risk in postmenopausal women. [[95](https://pubmed.ncbi.nlm.nih.gov/11124760/)]

**Claim #30 (**[**19:52**](https://youtu.be/MpxgZGnEF7E?t=1192)**):**

> Dr. Francis Pottenger had been trying to formulate a healthy diet for his laboratory cats ... This had Dr. Pottenger conduct a 10 year study to puzzle out the effects of cooked meat versus raw meat on hundreds of cats. He found that the cooked meat cats consistently had health problems but the problems were even worse for their kittens.

This might be interesting if we continued to have an incomplete understanding of feline nutrition. But this isn't 1932. In fact we have such a strong understanding of feline nutrition these days, that we can even formulate nutritionally complete animal-free diets for cats.

There are plenty of plausible explanations for what happened to the cats. One plausible explanation is that cooking the meat destroyed the taurine content, as we understand that even mild temperatures can significantly reduce the taurine content of meat. [[96](https://pubmed.ncbi.nlm.nih.gov/22060873/)] Taurine is an essential nutrient in cats, and most of the symptoms described can be explained by a taurine deficiency.

At present, our understanding of human nutrition is such that many can live for years on total parenteral nutrition (TPN), which is a type of intravenous total dietary replacement. There has been plenty of literature pointing out that TPN is associated with a wide range of negative health outcomes. But, it's difficult enough to disambiguate the effects of the TPN and the effects of whatever led to the patient requiring TPN to begin with, let alone the ostensible effects of unknown, spooky mystery meat nutrients.

**Claim #31 (**[**25:05**](https://youtu.be/MpxgZGnEF7E?t=1505)**):**

> A huge 2020 review explained that saturated fat rich foods like whole-fat dairy or unprocessed meat themselves are not associated with an increase risk of heart disease and a 2022 systematic review found the previous evidence that shows unprocessed meat is linked to chronic diseases like cancer or heart disease to be far too weak to make the recommendation to reduce meat consumption.

The evidence used to buttress the JACC paper is the same sort of evidence that the BOP paper authors considers too weak to be reliable. Why even point it out if the evidence is shit?

But, just for clarification, it is understood that whole fat dairy blunts the effect of saturated fats on blood lipids, due to its unique food matrix and the presence of something called the milk fat globule membrane. [[97](https://pubmed.ncbi.nlm.nih.gov/26016870/)] It is also understood that chocolate, while high in saturated fat, is high in a particular type of saturated fat called stearic acid. This particular saturated fat does not have a significant effect on blood lipids. [[98](https://pubmed.ncbi.nlm.nih.gov/32998517/)]

This is just Joseph using these exceptions to the rule to obfuscate the effect that meat itself has on blood lipids, which is significant and replicable. [[99](https://pubmed.ncbi.nlm.nih.gov/31161217/)] Overall, we understand that the relationship between meat and heart disease is likely mediated by blood lipids, particularly LDL. Meat has a tendency to raise LDL, which just straightforwardly explains its strong association with heart disease.

**Claim #32 (**[**25:44**](https://youtu.be/MpxgZGnEF7E?t=1544)**):**

> Yet, the anti-meat push has gotten so strong that as investigative journalist Nina Teicholz reveals, a recent Tufts University ranking system bogusly ranks Reese’s Peanut Butter Cups as healthier than Eggs, Cheese or Ground Beef.

These data don't represent any official guidelines. And I'm not sure how shitty input data and shitty methodology constitutes an "anti-meat push", honestly. But Joseph is not even representing the results of the Food Compass accurately. While it's true that on average animal products get a mediocre score, it's not true that they rank lower than snacks and desserts on average. [[100](https://pubmed.ncbi.nlm.nih.gov/37117986/)] What he presented in his video is an example of cherry picking.

In fact, some animal foods rank higher than some plant foods, and in the aggregate there is non-inferiority between some animal foods and some plant foods. For example, seafood ranks particularly high, and is non-inferior to both fruits and vegetables. Both meat and dairy are also non-inferior to grains. If Joseph wants to make some kind of claim about a vegan conspiracy to suppress the health value of animal foods, he'll have to explain why seafood gets such a remarkable score here.

**Claim #33 (**[**26:04**](https://youtu.be/MpxgZGnEF7E?t=1564)**):**

> It’s easy to assume that our understanding of individual nutrients is so advanced that we don’t need to rely on outdated meat-based diets - we can make replacements. But, while the amount of knowledge on nutrition that’s been accumulated is incredible, is it as complete as we assume?

While this has the appearance of being a sound inductive sort of argument, it ultimately seems to be an argument that is halfway between an appeal to ignorance and some nutrition-focused flavour of Pascal's wager. I'm not sure why we should find this persuasive. If we don't know if there are any nutrients in meat that currently render meat indispensable, I would just be agnostic about the existence of those nutrients.

**Claim #34 (**[**27:26**](https://youtu.be/MpxgZGnEF7E?t=1646)**):**

> It wasn’t even until 1998 that the nutrient Choline was recognized to be essential. Liver disease, atherosclerosis and neurological dysfunction taught us that Choline is pretty important. [[101](https://pubmed.ncbi.nlm.nih.gov/19906248/)]  
>   
> A paper from just last year in 2022, suggests we underestimate the optimal intake of choline. [[102](https://pubmed.ncbi.nlm.nih.gov/34962672/)] This Cornell study found that seven-year-old children had better attention span if their mothers consumed twice the recommended amount of choline during their pregnancy.

It is known that prenatal DHA supplementation positively influences attention of infants and preschool children in a similar manner as found for choline in the Cornell study. [[103](https://pubmed.ncbi.nlm.nih.gov/27362506/)][[104](https://pubmed.ncbi.nlm.nih.gov/27604770/)] Additionally, there is high-quality emerging evidence that prenatal choline supplementation improves DHA status in pregnant women but not in lactating women (also no statistically significant difference for DHA concentration in breast milk) making it likely that the beneficial effect of prenatal choline supplementation on sustained attention is mediated through DHA. [[105](https://pubmed.ncbi.nlm.nih.gov/35575618/)][[106](https://pubmed.ncbi.nlm.nih.gov/33516092/)]

Therefore, it may be prudent for both vegan and non-vegan women, especially those with certain PEMT genotypes, to supplement choline in addition to DHA during the second and third trimester of their pregnancies. [[107](https://pubmed.ncbi.nlm.nih.gov/36145177/)]

**Claim #35 (**[**28:33**](https://youtu.be/MpxgZGnEF7E?t=1713)**):**

> If a mother can’t breastfeed or get donor milk, of course modern infant formula is basically a miracle. But even as a representative of Abbott, a leading infant formula manufacturer admits: “to mimic and replicate breast milk is not possible.” Yes, a newborn will have far more sensitive nutrient requirements than an adult or even a child, but it’s an example the difficulty of trying to make a complete replacement of a natural food.

The two scenarios are not analogous. In the case of vegan replacements for animal based foods, we generally don't see people on properly planned "vegan" diets experiencing negative health outcomes as a consequence. So, even if the association between formula feeding and negative health outcomes was strong, it's not clear how one functions as a plausible analogy for the other.

**Claim #36 (**[**29:28**](https://youtu.be/MpxgZGnEF7E?t=1768)**):**

> ...there is evidence that [insert non-essential animal nutrient here] has beneficial effects. [[108](https://pubmed.ncbi.nlm.nih.gov/32072297/)]

In most studies wherein a benefit of these animal-derived compounds is found, supranutritional doses are given to subjects. Not only that, but the minimum doses required to achieve maximum benefits (when benefits are even present) is often well above and beyond what we could reasonably obtain from diet alone. This just leaves us asking why omnivore shouldn't be relying on supplements too.

**Claim #37 (**[**29:38**](https://youtu.be/MpxgZGnEF7E?t=1778)**):**

> Just to look at two This 2002 paper argues that taurine may be essential in certain circumstances, and creatine supplementation has benefits for brain function in adults like improving memory, intelligence and mood and it reduces the negative effects of sleep deprivation. [[109](https://pubmed.ncbi.nlm.nih.gov/12514918/)][[110](https://pubmed.ncbi.nlm.nih.gov/29704637/)][[111](https://pubmed.ncbi.nlm.nih.gov/16416332/)] Further, creatine is transferred from the mother to her baby during pregnancy providing several benefits to the baby. [[112](https://pubmed.ncbi.nlm.nih.gov/24766646/)]

The "certain circumstances" here were premature infants on total parenteral nutrition. The authors say this is due to them being unable to synthesize their own taurine at this stage and depending on breast milk. How is this related to vegan diets or even animal foods?

Regarding creatine, all studies he cited were on supplemental creatine with doses that would require over 1kg of beef a day to get. For most individuals, beyond their caloric needs.

**Claim #38 (**[**30:14**](https://youtu.be/MpxgZGnEF7E?t=1814)**):**

> You could take substantial amounts of soy or pea protein powders to make up for the fact that most plant proteins are poorly absorbed and have lower amounts of amino acids and so on and so on.

I'm sorry, Joseph. But tracer studies disagree. [[113](https://pubmed.ncbi.nlm.nih.gov/33693735/)] In fact, tofu appears to be on par with pork, and better than eggs, in terms of its contribution to total positive protein balance in the human body.

If you're going to try to make this point to shit on tofu, would you please be consistent and also shit on pork and eggs?

**Claim #39 (**[**30:28**](https://youtu.be/MpxgZGnEF7E?t=1828)**):**

> Again, meat is so nutrient dense that you can get a decent amount of well absorbed zinc, iron, selenium, choline, various B-vitamins, vitamin A, calcium and other nutrients just eating a crappy cheeseburger.

Here, in another certified bruh moment, Joseph relies on vague language to make a point that nobody should care about. You can only eat around three Burger King Whopper cheese burgers before exceeding 2200 Kcal, but you'd also be deficient in most nutrients.

While it's true that you would get the RDA of a number of nutrients, you would also exceed your sodium RDA by 268%, the AI for potassium would be undershot by 50%, and only 50% of the conservative 600 IU of vitamin D would be obtained. Additionally, you would only get about 40% of the RDA of vitamin A, in exchange for over 40g of saturated fat.

While I am aware that Joseph is not advocating for cheeseburger consumption, it's just an incredibly stupid point to make. Which leads me to the next claim.

**Claim #40 (**[**30:40**](https://youtu.be/MpxgZGnEF7E?t=1840)**):**

> Ideally people shouldn’t eat crappy cheeseburgers… but the average busy person doesn’t have time to craft the perfect meal - convenience is important. This is evidenced by the fact that a 2021 paper found that the more people avoided animal products in their diet - the more they ate convenient ultra-processed foods with vegans eating the most processed foods. [[114](https://pubmed.ncbi.nlm.nih.gov/32692345/)]

In yet another instance of Joseph failing to read his own references, possibly because he's rushing for that confirmation bias induced YouTube money, he mentions convenience ultra-processed foods (UPF) but links a paper where the reason vegans seemingly ate more UPF (not convenience foods) was that the authors classified meat alternatives and plant based milks like soy milk as UPF.

> Concomitantly with the increased numbers of vegetarians or vegans, the offer of industrial plant-based meat and dairy substitutes on the market has been growing during the past decade in Western countries (e.g., tofu, textured vegetable foods such as vegetarian sausages or patties, and plant-based drinks such as soy “milk”) (9, 10). Most of these substitutes are ultra-processed foods (UPFs). The development of this industrial plant-based meat and dairy substitutes market (11, 12, 13, 14) may have contributed to the growing consumption of UPFs in countries such as France (15) by specific populations. For example, persons avoiding most animal-based foods may have high intakes of UPFs, driven by higher consumptions of plant-based meat and dairy substitutes.

The difference was so tiny as to be entirely explained by that classification.

> The proportion of energy from UPFs was significantly higher for vegetarians (37.0% of the total energy intake) and vegans (39.5%) than for meat eaters (33.0%) (Figure 1A). Comparing vegans to meat eaters, for example, vegans consumed a greater proportion of UPFs (+6.41%) (Figure 1B).

In addition, Joseph failed to mention that vegans eat more unprocessed foods than omnivores.

> The proportion of energy from unprocessed foods was significantly higher for vegans (31.2% of the total energy intake) than for meat eaters (29.0%) (Figure 1A and B).

**Claim #41 (**[**31:13**](https://youtu.be/MpxgZGnEF7E?t=1873)**):**

> Eating tons of processed soy protein and vegetable oils like sunflower oil is quite new to the human stomach. Sunflower oil seems like a simple swap for animal fat … but they are totally different. Many animal fats can be a good source of vitamin K2, but vegetable oil in fact hampers the activity of vitamin K, increasing your need for it. [[115](https://pubmed.ncbi.nlm.nih.gov/12032162/)][[116](https://pubmed.ncbi.nlm.nih.gov/27251151/)][[117](https://pubmed.ncbi.nlm.nih.gov/28962307/)][[118](https://pubmed.ncbi.nlm.nih.gov/29353277/)] It also oxidizes very easily so it will increase your need for the antioxidant vitamin E. [[119](https://pubmed.ncbi.nlm.nih.gov/26291567/)] Vegetable oil has several other negative effects which I have talked about in another video.

We already discussed vitamin K2 earlier, and touched on how animal foods are generally a pathetic source, but even if they weren't, it's not clear that animal-derived vitamin K2 is even bioavailable. Additionally, vegetable oil consumption is not an entailment of veganism, so I'm not entirely sure why it's being discussed here. Nor is UPF consumption, unless you're dense enough to categorize B12 and D3 supplements as UPFs. At best this is a red herring, at worst this is a non sequitur.

**Claim #42 (**[**31:47**](https://youtu.be/MpxgZGnEF7E?t=1907)**):**

> Vegans and vegetarians tend to rely on soy for protein a lot. The hormone disrupting effects of increased soy consumption is somewhat controversial, but it may explain why a study on almost 8000 boys found boys born to vegetarian mothers had a higher risk for a specific deformity in the genitals called hypospadias. [[120](https://pubmed.ncbi.nlm.nih.gov/10619956/)]

Risk is not exactly what is being assessed in this study. This is a case-control study, which means we're looking at odds ratios. This is the ratio of the odds of the outcome of interest between two groups at varying levels of exposure. Case-control studies are also missing a temporal component, which is often considered critical for making sound causal inferences. With case-control studies, the direction of causality is extremely difficult to ascertain. Additionally, the study did not even measure or assess soy intake.

In this case, the odds ratio for hypospadias from low to high soy exposure was actually non-significant. However, in a similar case-control study from 2013, phytoestrogen consumption was inversely associated with hypospadias incidence. [[121](https://pubmed.ncbi.nlm.nih.gov/23752918/)] At best I think we can say that the literature on soy is pretty mixed, ranging from non-significant increases in the odds of hypospadias to statistically significant decreases in the odds of hypospadias.

**Claim #43 (**[**32:09**](https://youtu.be/MpxgZGnEF7E?t=1929)**):**

> Soy contains the isoflavonoid genistein, which studies show has “detrimental effects on the male reproductive system…” [[122](https://pubmed.ncbi.nlm.nih.gov/35760341/)]

Bruh, this is a meta-analysis of rodent studies with a blurb about how the results may or may not translate to humans.

**Claim #44 (**[**32:17**](https://youtu.be/MpxgZGnEF7E?t=1937)**):**

> Lastly, impossible burger has tried to make their product taste meatier with something called leghemoglobin from the roots of genetically modified soy plants. [[123](https://www.canada.ca/en/health-canada/services/food-nutrition/genetically-modified-foods-other-novel-foods/approved-products/soy-leghemoglobin/document.html)]  
>   
> GMOscience.org writes that:  
>   
> A 28-day study commissioned by Impossible Foods in 2017 on soy leghemoglobin found that soy leghemoglobin caused statistically significant changes in weight gain, changes in the blood that can indicate the onset of inflammation or kidney disease, and possible signs of anaemia in the rats.

In isolation, leghemoglobin actually had better bioavailability than iron(II)-sulfate, and when part of a food matrix, here as fortification for corn tortillas, had similar (ie non stat sig different) bioavailability compared to bovine heme iron. [[124](https://pubmed.ncbi.nlm.nih.gov/16478282/)]

As an added fun fact that may blow up the brains of Joseph's audience, leghemoglobin is actually evolutionarily just as old as hemoglobin. See the following figure from Biochemistry 6th edition by Berg, Tymoczko and Styer.

**Claim #45 (**[**32:43**](https://youtu.be/MpxgZGnEF7E?t=1963)**):**

> Further studies did eventually persuade the FDA to designate leghemoglobin as safe, but Impossible Foods admitted that a quarter their new ingredient was composed of 46 “unexpected” additional proteins, none of which were assessed for safety in the dossier. [[125](https://twitter.com/ImpossibleFoods/status/1000397509196505089?s=20)]

These "unexpected additional" proteins are just unknown proteins, which in this context simply means that their biological behaviour is not yet known. If you analyse any food, including beef, there will be many, many such "unknown" proteins. It's not spooky. This characteristic likely quantifies over all foods, and as such we have no good reason to consider this a cause for any concern.

**Claim #46 (**[**33:27**](https://youtu.be/MpxgZGnEF7E?t=2007)**):**

> This is another big issue with trying to replace animal foods. The replacement almost always comes with plenty of other stuff. Kidney beans are a good source of protein need to be soaked and cooked to reduce the lectin content. Some boys in the UK showed up in the hospital with profuse diarrhea and vomiting because they ate 4 kidney beans that were soaked, but not cooked. [[126](https://pubmed.ncbi.nlm.nih.gov/2249712/)]

Okay, Joseph. Either you're not reading your references at all or you're just memeing at this point. The results are relevant to raw beans, which everyone agrees shouldn't be eaten. This isn't revelatory. Soaking your legumes is some level-zero Weston A Price bullshit. They need to be cooked, dude. **COOKED**.

Even without soaking, and instead pressure cooking, the time required to deactivate the lectins was well below the time required to make them edible. [[127](https://www.researchgate.net/publication/229968837_Effect_of_Heat_Processing_on_Hemagglutinin_Activity_in_Red_Kidney_Beans)]

Additionally, in the years between 1976 and 1989, the UK only saw 50 suspected incidents related to un- or under-cooked legumes were registered. Meanwhile, [according to the USDA](https://www.fsis.usda.gov/inspection/inspection-programs/inspection-poultry-products/reducing-salmonella-poultry/salmonella), poor meat preparation can account for approximately 1.35 million salmonella-related infections, 26,500 salmonella-related hospitalizations, and 420 salmonella-related deaths every year just in the United States.

Joseph also tries to address those who react to lectins in cooked kidney beans too. News flash. There are no fucking lectins in cooked kidney beans. [[128](https://pubmed.ncbi.nlm.nih.gov/34829077/)]

**Claim #47 (**[**33:56**](https://youtu.be/MpxgZGnEF7E?t=2036)**):**

> Phytic acid found in beans, seeds, nuts and grains inhibits fat digestion and the absorption of calcium, magnesium, phosphorus and Zinc. How much? Well, this study found about 35% less Zinc is absorbed in a vegetarian diet. [[129](https://pubmed.ncbi.nlm.nih.gov/31095149/)][[130](https://pubmed.ncbi.nlm.nih.gov/16401188/)][[56](https://pubmed.ncbi.nlm.nih.gov/12936958/)] Fiber itself worsens the activity of pancreatic lipase which is important for the absorption of fat soluble vitamins like A, D and K2. [[131](https://pubmed.ncbi.nlm.nih.gov/2819858/)]

Joseph's reference for this claim is the same paper that he cited for his claim about phytate-mediated inhibition of iron absorption. However, straight from the author's conclusions, we can read:

> The iron and zinc from vegetarian diets are generally less bioavailable than from nonvegetarian diets because of reduced meat intake as well as the tendency to consume more phytic acid and other plant-based inhibitors of iron and zinc absorption. However, in Western countries with varied and abundant food supplies, it is not clear that this reduced bioavailability has any functional consequences.

What's worse, Joseph's reference for phytate-mediated inhibition of hepatic lipase activity is an in vitro study. It's not clear why we should care.

**Claim #48 (**[**34:19**](https://youtu.be/MpxgZGnEF7E?t=2059)**):**

> A high intake of goitrogenic foods like cabbage, kale and turnips can interfere with iodine functioning.

Show me the study that actually divulges that any of these sorts of foods have any goitrogenic effects, please. We've tried to look for this effect in foods that are highest in these so-called goitrogens, such as broccoli sprouts, and we so far haven't found any generalizable effect. [[132](https://pubmed.ncbi.nlm.nih.gov/30735751/)] In fact, the number of subjects with subclinical hypothyroidism actually went down in the broccoli sprout group, compared to baseline.

**Claim #49 (**[**34:25**](https://youtu.be/MpxgZGnEF7E?t=2065)**):**

> Food Scientist Dr. Frederic Leroy has written an extensive article on the various examples of why it’s a challenge to acquire enough of certain nutrients just from plants due to inhibiting compounds like these. [[133](https://aleph-2020.blogspot.com/2019/05/animal-source-foods-provide-nutrients.html)] This nutrient challenge is part of the reason why The German Nutrition Society in 2016 and the French-speaking Pediatric HGN Group in 2019 recommend against a vegan diet for adolescents, children or mothers. [[134](https://www.ernaehrungs-umschau.de/english-articles/15-06-2016-vegan-diet/)][[135](https://pubmed.ncbi.nlm.nih.gov/31615715/)]

Well, as long as we're appealing to authorities, we should point out that both the French and German governments recommend limiting meat intake to no more than 500g per week and 600g per week, respectively.

**Claim #50 (**[**34:49**](https://youtu.be/MpxgZGnEF7E?t=2089)**):**

> Several studies have found babies born to vegan mothers to have a lower birth weight than babies of omnivore mothers. [[136](https://pubmed.ncbi.nlm.nih.gov/30909771/)][[137](https://journals.lww.com/ijcm/Abstract/1999/24020/A_STUDY_OF_EFFECT_OF_MATERNAL_NUTRITION_ON.4.aspx)][[138](https://pubmed.ncbi.nlm.nih.gov/32776295/)][[139](https://pubmed.ncbi.nlm.nih.gov/8172120/)][[140](https://pubmed.ncbi.nlm.nih.gov/33232446/)][[141](https://www.nichd.nih.gov/newsroom/news/122120-vegetarian-diets)][[142](https://pubmed.ncbi.nlm.nih.gov/28745335/)][[143](https://sciendo.com/article/10.5604/01.3001.0014.9343)]  
> Birth weight can be a predictor of infant health and growth. [[144](https://pubmed.ncbi.nlm.nih.gov/15703531/)][[145](https://pubmed.ncbi.nlm.nih.gov/32928144/)][[146](https://pubmed.ncbi.nlm.nih.gov/26288495/)][[147](https://pubmed.ncbi.nlm.nih.gov/28840655/)] In fact, one study that meticulously analyzed the records of 4,300 adults who were in the Danish Medical Birth Register found that the lower their weight at birth, the shorter they would be as adults. [[148](https://pubmed.ncbi.nlm.nih.gov/10206622/)]  
> This study points out that the growth of vegetarian children was adequate, but less than average. [[39](https://pubmed.ncbi.nlm.nih.gov/2771551/)] I wonder how people would react to a doctor saying “your son won’t be as tall as he could be, but don’t worry his height will be adequate.”

Firstly, it's probably not actually birth weights we should be caring about necessarily. It's whether or not the infants are considered small for gestational age. Secondly, Joseph's references don't actually provide very persuasive evidence to support the notion that vegan diets increase the risk of small for gestational age. One particularly strong reason for this is poor B12 supplementation practices, which is a known risk factor among vegans that has not been adequately accounted for.

For instance, Ferrara et al. found that only 15% of the vegan cohort supplemented B12, while only 5% supplemented D3. Similarly, Yisahak et al. conducted a tangential study on vegetarians, but even then no assessment for B12 or D3 supplementation was made.

In addition, the study by R.K. Sharma et al. was done in 1999 before fortification and supplementation standards were established. Although they found that low birth weights were largely explained by the height and weight of the mother and that anemia was a risk factor for small for gestational age. These authors also did not account for B12 supplementation.

Kesary et al. lumped B12, iron, folate, and multivitamins together, and participants were said to be taking supplements if they took supplements more than once per week. However, the odds of small for gestational age between vegans and omnivores was not significant after an adjustment for BMI. Therefore, it is difficult to draw a definitive conclusion about the relationship between "vegan" diets and small for gestational age from this study.

Basically if vegans tend to have lower baseline BMI and tend to gain less weight during pregnancy, normally they will give birth to smaller babies. By definition, since you shifted the distribution curve, more of them will fall bellow the 10th percentile. Any unique effect of vegan diets here are probably with respect to more powerfully resisting weight gain and than typical omnivorous diets.

Finally, two cautionary narrative review articles by Miedziaszczyk et al. and Pawlak et al. highlighted that most of the vegan populations mentioned had either high MMA, low B12 intake or status, or poor supplementation practices. These observations further emphasize the importance of adequate B12 supplementation when considering the risks associated with vegan diets during pregnancy. Here's a quote from one of the authors:

> It should be thus concluded that vegan diets are appropriate for pregnant and lactating women only if these women habitually use reliable B12 sources, preferably oral supplements.

**Claim #51 (**[**38:18**](https://youtu.be/MpxgZGnEF7E?t=2118)**):**

> What is the difference between enough nutrients and the optimal amount of nutrients...cutting out nutrient dense animal foods doesn’t seem like a move in the right direction for health

This just seems like pure speculation. Maybe optimal is not achievable without a supplement on any natural diet. I mean, think about it. What's the argument for omnivorous diets necessarily providing optimal amounts of all nutrients? If there's no argument for that, then it's possible that even the diet that he's recommending is horribly insufficient in some way.

How has Joseph determined that the optimal range for nutrient intakes aren't above what could be obtained from omnivorous diets? Seems like his argument here is begging the question. If it's the case that optimal levels of nutrients are only practical to obtain from supplements, then we'd all benefit— not just vegans.

It's also true that this works in reverse. If Joseph is arguing that we should eat more meat to hit some nebulous "optimal" ranges for all nutrients, what's the argument that this is not true for plant foods? Perhaps eating more meat displaces plant foods and keeps us from achieving an optimal intake of some other plant-derived nutrients as well, notably vitamin C, folate, fibre, potassium, manganese, or polyphenols. Yes, Joseph, I live in the real world where getting ample fibre intake is actually beneficial for the vast majority of people.

So perhaps the optimal intake of many of plant-derived nutrients cannot be achieved if you are eating a significant amount of meat. Since Joseph insists on discussing non-essential, animal-derived nutrients for which the evidence for benefit is paltry at best. There is literally more evidence of benefit for polyphenols than there is for carnitine, anserine, taurine, and perhaps even creatine.

**Claim #52 (**[**36:44**](https://youtu.be/MpxgZGnEF7E?t=2204)**):**

> For 99% of human history we relied on animal foods for nutrients - the an animal food containing diet has a strong track record that spans arguably over 1.7 million years. [[149](https://pubmed.ncbi.nlm.nih.gov/32508752/)][[150](https://bigthink.com/the-past/brain-evolution/)] Various cultures viewed animal foods as important to growth and despite the challenging circumstances they lived in, they were protected from infectious diseases, they didn’t have the modern diseases of civilization, and they enjoyed proper growth in their body, faces and mouths.

There is no source provided for Joseph's claim that "various cultures" do not suffer from the modern diseases of civilization. Joseph characterizes these diseases as "heart disease, cancer, osteoporosis, diabetes, and so on", but there is plenty of evidence against the notion that these diseases are somehow modern.

Cancer in humans is a phenomenon that is over a million and a half years old, for example. [[151](https://carta.anthropogeny.org/libraries/bibliography/earliest-hominin-cancer-17-million-year-old-osteosarcoma-swartkrans-cave)] Heart disease is prevalent in nearly every population we study, whether traditional, ancient, or modern. [[152](https://pubmed.ncbi.nlm.nih.gov/23489753/)] Even the Tsimane have advanced atherosclerosis. [[153](https://pubmed.ncbi.nlm.nih.gov/28320601/)] Even diabetes dates back millennia. [[154](https://pubmed.ncbi.nlm.nih.gov/26788261/)]

**Claim #53 (**[**37:07**](https://youtu.be/MpxgZGnEF7E?t=2227)**):**

> With that in mind, a plant-based diet is an experiment without any meaningful track record. It’s been a couple decades at best that people have been doing vegan diets, yet already many people quit for health reasons. Research is a promising story of progress - maybe one day we’ll learn enough to make sufficient plant-based replacements for animal foods. But it’s probably not happening any time soon.

It seems like the word "experiment" is being used in a strange way here. From what I can gather, Joseph is either trying to convey that there is significant risk entailed by being on a "vegan" diet OR that there is no historical precedent for animal-free diets and that we should apply some precautionary principle OR veganism is being used to test a hypothesis.

Either of these three propositions requires an argument. If "experiment" is just being used as it is commonly used, as something done to test a hypothesis, then it is not clear that veganism is an experiment on that construal. If "experiment" just means that there is a possibility of some undesirable outcome actualizing, then it seems trivially true and misleading to refer to veganism as an experiment. If "experiment" means that there is some demonstrably entailment to a poor outcome, then he would actually need to provide decent evidence for that.

Thanks for reading! If you enjoy my writing and want more content like this, consider pledging to either my [Patreon](https://www.patreon.com/thenutrivore) or [LiberaPay](https://liberapay.com/TheNutrivore/), or becoming a [YouTube Member](https://www.youtube.com/c/thenutrivore/join)!

**References**

[1] van Ballegooijen, Adriana J., et al. ‘The Synergistic Interplay between Vitamins D and K for Bone and Cardiovascular Health: A Narrative Review’. International Journal of Endocrinology, vol. 2017, 2017, p. 7454376. PubMed, [https://doi.org/10.1155/2017/7454376](https://doi.org/10.1155/2017/7454376).

[2] Yee, Michelle Min Fang, et al. ‘Vitamin A and Bone Health: A Review on Current Evidence’. _Molecules (Basel, Switzerland)_, vol. 26, no. 6, Mar. 2021, p. 1757. _PubMed_, [https://doi.org/10.3390/molecules26061757](https://doi.org/10.3390/molecules26061757).

[3] MS, AEGIS Communications, By Jerome C. Rose, PhD, Richard D. Roblee, DDS. _Interdisciplinary Analysis: Origins of Dental Crowding and Malocclusions - An Anthropological Perspective_. [https://www.aegisdentalnetwork.com/cced/2009/06/interdisciplinary-analysis-origins-of-dental-crowding-and-malocclusions-an-anthropological-perspective](https://www.aegisdentalnetwork.com/cced/2009/06/interdisciplinary-analysis-origins-of-dental-crowding-and-malocclusions-an-anthropological-perspective).

[4] Faunalytics. ‘A Summary Of Faunalytics’ Study Of Current And Former Vegetarians And Vegans’. _Faunalytics_, 24 Feb. 2016, [https://faunalytics.org/a-summary-of-faunalytics-study-of-current-and-former-vegetarians-and-vegans/](https://faunalytics.org/a-summary-of-faunalytics-study-of-current-and-former-vegetarians-and-vegans/).

[5] Grasgruber, P., et al. ‘Major Correlates of Male Height: A Study of 105 Countries’. _Economics and Human Biology_, vol. 21, May 2016, pp. 172–95. _PubMed_, [https://doi.org/10.1016/j.ehb.2016.01.005](https://doi.org/10.1016/j.ehb.2016.01.005).

[6] Armstrong, B., and R. Doll. ‘Environmental Factors and Cancer Incidence and Mortality in Different Countries, with Special Reference to Dietary Practices’. _International Journal of Cancer_, vol. 15, no. 4, Apr. 1975, pp. 617–31. _PubMed_, [https://doi.org/10.1002/ijc.2910150411](https://doi.org/10.1002/ijc.2910150411).

[7] Koebnick, C., et al. ‘Consequences of a Long-Term Raw Food Diet on Body Weight and Menstruation: Results of a Questionnaire Survey’. _Annals of Nutrition & Metabolism_, vol. 43, no. 2, 1999, pp. 69–79. _PubMed_, [https://doi.org/10.1159/000012770](https://doi.org/10.1159/000012770).

[8] Novotny, Janet A., et al. ‘Beta-Carotene Conversion to Vitamin A Decreases as the Dietary Dose Increases in Humans’. _The Journal of Nutrition_, vol. 140, no. 5, May 2010, pp. 915–18. _PubMed_, [https://doi.org/10.3945/jn.109.116947](https://doi.org/10.3945/jn.109.116947).

[9] West, Clive E., et al. ‘Consequences of Revised Estimates of Carotenoid Bioefficacy for Dietary Control of Vitamin A Deficiency in Developing Countries’. _The Journal of Nutrition_, vol. 132, no. 9 Suppl, Sept. 2002, pp. 2920S-2926S. _PubMed_, [https://doi.org/10.1093/jn/132.9.2920S](https://doi.org/10.1093/jn/132.9.2920S).

[10] Novotny, Janet A., et al. ‘Beta-Carotene Conversion to Vitamin A Decreases as the Dietary Dose Increases in Humans’. _The Journal of Nutrition_, vol. 140, no. 5, May 2010, pp. 915–18. _PubMed_, [https://doi.org/10.3945/jn.109.116947](https://doi.org/10.3945/jn.109.116947).

[11] Suzuki, Masako, and Meika Tomita. ‘Genetic Variations of Vitamin A-Absorption and Storage-Related Genes, and Their Potential Contribution to Vitamin A Deficiency Risks Among Different Ethnic Groups’. _Frontiers in Nutrition_, vol. 9, 2022. _Frontiers_, [https://www.frontiersin.org/articles/10.3389/fnut.2022.861619](https://www.frontiersin.org/articles/10.3389/fnut.2022.861619).

[12] Leung, W. C., et al. ‘Two Common Single Nucleotide Polymorphisms in the Gene Encoding Beta-Carotene 15,15’-Monoxygenase Alter Beta-Carotene Metabolism in Female Volunteers’. _FASEB Journal: Official Publication of the Federation of American Societies for Experimental Biology_, vol. 23, no. 4, Apr. 2009, pp. 1041–53. _PubMed_, [https://doi.org/10.1096/fj.08-121962](https://doi.org/10.1096/fj.08-121962).

[13] Li, D., et al. ‘Selected Micronutrient Intake and Status in Men with Differing Meat Intakes, Vegetarians and Vegans’. _Asia Pacific Journal of Clinical Nutrition_, vol. 9, no. 1, Mar. 2000, pp. 18–23. _PubMed_, [https://doi.org/10.1046/j.1440-6047.2000.00129.x](https://doi.org/10.1046/j.1440-6047.2000.00129.x).

[14] Schüpbach, R., et al. ‘Micronutrient Status and Intake in Omnivores, Vegetarians and Vegans in Switzerland’. _European Journal of Nutrition_, vol. 56, no. 1, Feb. 2017, pp. 283–93. _PubMed_, [https://doi.org/10.1007/s00394-015-1079-7](https://doi.org/10.1007/s00394-015-1079-7).

[15] van Jaarsveld, Paul J., et al. ‘Beta-Carotene-Rich Orange-Fleshed Sweet Potato Improves the Vitamin A Status of Primary School Children Assessed with the Modified-Relative-Dose-Response Test’. _The American Journal of Clinical Nutrition_, vol. 81, no. 5, May 2005, pp. 1080–87. _PubMed_, [https://doi.org/10.1093/ajcn/81.5.1080](https://doi.org/10.1093/ajcn/81.5.1080).

[16] Ribaya-Mercado, Judy D., et al. ‘Carotene-Rich Plant Foods Ingested with Minimal Dietary Fat Enhance the Total-Body Vitamin A Pool Size in Filipino Schoolchildren as Assessed by Stable-Isotope-Dilution Methodology’. _The American Journal of Clinical Nutrition_, vol. 85, no. 4, Apr. 2007, pp. 1041–49. _PubMed_, [https://doi.org/10.1093/ajcn/85.4.1041](https://doi.org/10.1093/ajcn/85.4.1041).

[17] de Pee, S., et al. ‘Orange Fruit Is More Effective than Are Dark-Green, Leafy Vegetables in Increasing Serum Concentrations of Retinol and Beta-Carotene in Schoolchildren in Indonesia’. _The American Journal of Clinical Nutrition_, vol. 68, no. 5, Nov. 1998, pp. 1058–67. _PubMed_, [https://doi.org/10.1093/ajcn/68.5.1058](https://doi.org/10.1093/ajcn/68.5.1058).

[18] Tang, G., et al. ‘Green and Yellow Vegetables Can Maintain Body Stores of Vitamin A in Chinese Children’. _The American Journal of Clinical Nutrition_, vol. 70, no. 6, Dec. 1999, pp. 1069–76. _PubMed_, [https://doi.org/10.1093/ajcn/70.6.1069](https://doi.org/10.1093/ajcn/70.6.1069).

[19] Haskell, Marjorie J., et al. ‘Daily Consumption of Indian Spinach (Basella Alba) or Sweet Potatoes Has a Positive Effect on Total-Body Vitamin A Stores in Bangladeshi Men’. _The American Journal of Clinical Nutrition_, vol. 80, no. 3, Sept. 2004, pp. 705–14. _PubMed_, [https://doi.org/10.1093/ajcn/80.3.705](https://doi.org/10.1093/ajcn/80.3.705).

[20] Tang, Guangwen, et al. ‘Spinach or Carrots Can Supply Significant Amounts of Vitamin A as Assessed by Feeding with Intrinsically Deuterated Vegetables’. _The American Journal of Clinical Nutrition_, vol. 82, no. 4, Oct. 2005, pp. 821–28. _PubMed_, [https://doi.org/10.1093/ajcn/82.4.821](https://doi.org/10.1093/ajcn/82.4.821).

[21] Tang, Guangwen, et al. ‘Golden Rice Is an Effective Source of Vitamin A’. _The American Journal of Clinical Nutrition_, vol. 89, no. 6, June 2009, pp. 1776–83. _PubMed_, [https://doi.org/10.3945/ajcn.2008.27119](https://doi.org/10.3945/ajcn.2008.27119).

[22] Graßmann, Sophie, et al. ‘SNP Rs6564851 in the BCO1 Gene Is Associated with Varying Provitamin a Plasma Concentrations but Not with Retinol Concentrations among Adolescents from Rural Ghana’. _Nutrients_, vol. 12, no. 6, June 2020, p. 1786. _PubMed_, [https://doi.org/10.3390/nu12061786](https://doi.org/10.3390/nu12061786).

[23] Homma, Yosuke, et al. ‘A Case Report of Acute Vitamin A Intoxication Due to Ocean Perch Liver Ingestion’. _The Journal of Emergency Medicine_, vol. 49, no. 1, July 2015, pp. 15–17. _PubMed_, [https://doi.org/10.1016/j.jemermed.2014.12.056](https://doi.org/10.1016/j.jemermed.2014.12.056).

[24] Dewailly, E., et al. ‘Vitamin A Intoxication from Reef Fish Liver Consumption in Bermuda’. _Journal of Food Protection_, vol. 74, no. 9, Sept. 2011, pp. 1581–83. _PubMed_, [https://doi.org/10.4315/0362-028X.JFP-10-566](https://doi.org/10.4315/0362-028X.JFP-10-566).

[25] Nagai, K., et al. ‘Vitamin A Toxicity Secondary to Excessive Intake of Yellow-Green Vegetables, Liver and Laver’. _Journal of Hepatology_, vol. 31, no. 1, July 1999, pp. 142–48. _PubMed_, [https://doi.org/10.1016/s0168-8278(99)80174-3](https://doi.org/10.1016/s0168-8278(99)80174-3).

[26] van Stuijvenberg, Martha E., et al. ‘South African Preschool Children Habitually Consuming Sheep Liver and Exposed to Vitamin A Supplementation and Fortification Have Hypervitaminotic A Liver Stores: A Cohort Study’. _The American Journal of Clinical Nutrition_, vol. 110, no. 1, July 2019, pp. 91–101. _PubMed_, [https://doi.org/10.1093/ajcn/nqy382](https://doi.org/10.1093/ajcn/nqy382).

[27] Carpenter, T. O., et al. ‘Severe Hypervitaminosis A in Siblings: Evidence of Variable Tolerance to Retinol Intake’. _The Journal of Pediatrics_, vol. 111, no. 4, Oct. 1987, pp. 507–12. _PubMed_, [https://doi.org/10.1016/s0022-3476(87)80109-9](https://doi.org/10.1016/s0022-3476(87)80109-9).

[28] Hovinen, Topi, et al. ‘Vegan Diet in Young Children Remodels Metabolism and Challenges the Statuses of Essential Nutrients’. _EMBO Molecular Medicine_, vol. 13, no. 2, Feb. 2021, p. e13492. _PubMed_, [https://doi.org/10.15252/emmm.202013492](https://doi.org/10.15252/emmm.202013492).

[29] Weikert, Cornelia, et al. ‘Vitamin and Mineral Status in a Vegan Diet’. _Deutsches Arzteblatt International_, vol. 117, no. 35–36, Aug. 2020, pp. 575–82. _PubMed_, [https://doi.org/10.3238/arztebl.2020.0575](https://doi.org/10.3238/arztebl.2020.0575).

[30] ‘Time for More Vitamin D’. _Harvard Health_, 1 Sept. 2008, [https://www.health.harvard.edu/staying-healthy/time-for-more-vitamin-d](https://www.health.harvard.edu/staying-healthy/time-for-more-vitamin-d).

[31] Elorinne, Anna-Liisa, et al. ‘Food and Nutrient Intake and Nutritional Status of Finnish Vegans and Non-Vegetarians’. _PloS One_, vol. 11, no. 2, 2016, p. e0148235. _PubMed_, [https://doi.org/10.1371/journal.pone.0148235](https://doi.org/10.1371/journal.pone.0148235).

[32] Washington, District of Columbia 1800 I. Street NW and Dc 20006. ‘PolitiFact - Is Whole Milk Prohibited from Being Offered in New York Schools? Yes’. _@politifact_, [https://www.politifact.com/factchecks/2021/jun/07/lorraine-lewandrowski/whole-milk-prohibited-being-offered-new-york-schoo/](https://www.politifact.com/factchecks/2021/jun/07/lorraine-lewandrowski/whole-milk-prohibited-being-offered-new-york-schoo/). Accessed 30 Apr. 2023.

[33] Hariri, Essa, et al. ‘Vitamin K2-a Neglected Player in Cardiovascular Health: A Narrative Review’. _Open Heart_, vol. 8, no. 2, Nov. 2021, p. e001715. _PubMed_, [https://doi.org/10.1136/openhrt-2021-001715](https://doi.org/10.1136/openhrt-2021-001715).

[34] Siltari, Aino, and Heikki Vapaatalo. ‘Vascular Calcification, Vitamin K and Warfarin Therapy - Possible or Plausible Connection?’ _Basic & Clinical Pharmacology & Toxicology_, vol. 122, no. 1, Jan. 2018, pp. 19–24. _PubMed_, [https://doi.org/10.1111/bcpt.12834](https://doi.org/10.1111/bcpt.12834).

[35] Sato, Toshiro, et al. ‘Comparison of Menaquinone-4 and Menaquinone-7 Bioavailability in Healthy Women’. _Nutrition Journal_, vol. 11, Nov. 2012, p. 93. _PubMed_, [https://doi.org/10.1186/1475-2891-11-93](https://doi.org/10.1186/1475-2891-11-93).

[36] Schurgers, L. J., and C. Vermeer. ‘Determination of Phylloquinone and Menaquinones in Food. Effect of Food Matrix on Circulating Vitamin K Concentrations’. _Haemostasis_, vol. 30, no. 6, 2000, pp. 298–307. _PubMed_, [https://doi.org/10.1159/000054147](https://doi.org/10.1159/000054147).

[37] Desmond, Małgorzata A., et al. ‘Growth, Body Composition, and Cardiovascular and Nutritional Risk of 5- to 10-y-Old Children Consuming Vegetarian, Vegan, or Omnivore Diets’. _The American Journal of Clinical Nutrition_, vol. 113, no. 6, June 2021, pp. 1565–77. _PubMed_, [https://doi.org/10.1093/ajcn/nqaa445](https://doi.org/10.1093/ajcn/nqaa445).

[38] Sanders, T. A. ‘Growth and Development of British Vegan Children’. _The American Journal of Clinical Nutrition_, vol. 48, no. 3 Suppl, Sept. 1988, pp. 822–25. _PubMed_, [https://doi.org/10.1093/ajcn/48.3.822](https://doi.org/10.1093/ajcn/48.3.822).

[39] O’Connell, J. M., et al. ‘Growth of Vegetarian Children: The Farm Study’. _Pediatrics_, vol. 84, no. 3, Sept. 1989, pp. 475–81.

[40] Weder, Stine, et al. ‘Energy, Macronutrient Intake, and Anthropometrics of Vegetarian, Vegan, and Omnivorous Children (1−3 Years) in Germany (VeChi Diet Study)’. _Nutrients_, vol. 11, no. 4, Apr. 2019, p. 832. _PubMed_, [https://doi.org/10.3390/nu11040832](https://doi.org/10.3390/nu11040832).

[41] Weder, Stine, et al. ‘Intake of Micronutrients and Fatty Acids of Vegetarian, Vegan, and Omnivorous Children (1-3 Years) in Germany (VeChi Diet Study)’. _European Journal of Nutrition_, vol. 61, no. 3, Apr. 2022, pp. 1507–20. _PubMed_, [https://doi.org/10.1007/s00394-021-02753-3](https://doi.org/10.1007/s00394-021-02753-3).

[42] Alexy, Ute, et al. ‘Nutrient Intake and Status of German Children and Adolescents Consuming Vegetarian, Vegan or Omnivore Diets: Results of the VeChi Youth Study’. _Nutrients_, vol. 13, no. 5, May 2021, p. 1707. _PubMed_, [https://doi.org/10.3390/nu13051707](https://doi.org/10.3390/nu13051707).

[43] _Length-Weight Growth Analysis up to 12 Months of Age in Three Groups According to the Dietary Pattern Followed from Pregnant Mothers and Children during the First Year of Life - Minerva Pediatrics 2021 Apr 16_. [https://www.minervamedica.it/en/journals/minerva-pediatrics/article.php?cod=R15Y9999N00A21041604](https://www.minervamedica.it/en/journals/minerva-pediatrics/article.php?cod=R15Y9999N00A21041604). Accessed 30 Apr. 2023.

[44] Gilsing, A. M. J., et al. ‘Serum Concentrations of Vitamin B12 and Folate in British Male Omnivores, Vegetarians and Vegans: Results from a Cross-Sectional Analysis of the EPIC-Oxford Cohort Study’. _European Journal of Clinical Nutrition_, vol. 64, no. 9, Sept. 2010, pp. 933–39. _PubMed_, [https://doi.org/10.1038/ejcn.2010.142](https://doi.org/10.1038/ejcn.2010.142).

[45] Herrmann, Wolfgang, et al. ‘Vitamin B-12 Status, Particularly Holotranscobalamin II and Methylmalonic Acid Concentrations, and Hyperhomocysteinemia in Vegetarians’. _The American Journal of Clinical Nutrition_, vol. 78, no. 1, July 2003, pp. 131–36. _PubMed_, [https://doi.org/10.1093/ajcn/78.1.131](https://doi.org/10.1093/ajcn/78.1.131).

[46] Sheng, Xiaoyang, et al. ‘Effects of Dietary Intervention on Vitamin B12 Status and Cognitive Level of 18-Month-Old Toddlers in High-Poverty Areas: A Cluster-Randomized Controlled Trial’. _BMC Pediatrics_, vol. 19, no. 1, Sept. 2019, p. 334. _PubMed_, [https://doi.org/10.1186/s12887-019-1716-z](https://doi.org/10.1186/s12887-019-1716-z).

[47] Del Bo’, Cristian, et al. ‘Effect of Two Different Sublingual Dosages of Vitamin B12 on Cobalamin Nutritional Status in Vegans and Vegetarians with a Marginal Deficiency: A Randomized Controlled Trial’. _Clinical Nutrition (Edinburgh, Scotland)_, vol. 38, no. 2, Apr. 2019, pp. 575–83. _PubMed_, [https://doi.org/10.1016/j.clnu.2018.02.008](https://doi.org/10.1016/j.clnu.2018.02.008).

[48] Ho, Kok-Sun, et al. ‘Stopping or Reducing Dietary Fiber Intake Reduces Constipation and Its Associated Symptoms’. _World Journal of Gastroenterology_, vol. 18, no. 33, Sept. 2012, pp. 4593–96. _PubMed_, [https://doi.org/10.3748/wjg.v18.i33.4593](https://doi.org/10.3748/wjg.v18.i33.4593).

[49] de Vries, Jan, et al. ‘Effects of Cereal, Fruit and Vegetable Fibers on Human Fecal Weight and Transit Time: A Comprehensive Review of Intervention Trials’. _Nutrients_, vol. 8, no. 3, Mar. 2016, p. 130. _PubMed_, [https://doi.org/10.3390/nu8030130](https://doi.org/10.3390/nu8030130).

[50] van der Schoot, Alice, et al. ‘The Effect of Fiber Supplementation on Chronic Constipation in Adults: An Updated Systematic Review and Meta-Analysis of Randomized Controlled Trials’. _The American Journal of Clinical Nutrition_, vol. 116, no. 4, Oct. 2022, pp. 953–69. _PubMed_, [https://doi.org/10.1093/ajcn/nqac184](https://doi.org/10.1093/ajcn/nqac184).

[51] Tadesse, K. ‘The Effect of Dietary Fibre Isolates on Gastric Secretion, Acidity and Emptying’. _The British Journal of Nutrition_, vol. 55, no. 3, May 1986, pp. 507–13. _PubMed_, [https://doi.org/10.1079/bjn19860058](https://doi.org/10.1079/bjn19860058).

[52] Rydning, Andreas, and Arnold Berstad. ‘Dietary Fibre and Peptic Ulcer’. _Scandinavian Journal of Gastroenterology_, vol. 22, no. sup129, Jan. 1987, pp. 232–40. _Taylor and Francis+NEJM_, [https://doi.org/10.3109/00365528709095891](https://doi.org/10.3109/00365528709095891).

[53] Harju, E. ‘Guar Gum Benefits Duodenal Ulcer Patients by Decreasing Gastric Acidity and Rate of Emptying of Gastric Contents 60 to 120 Minutes Postprandially’. _The American Surgeon_, vol. 50, no. 12, Dec. 1984, pp. 668–72.

[54] Okuda, K., and H. Takedatsu. ‘Absorption of Vitamin B12 in a Rectal Suppository’. _Proceedings of the Society for Experimental Biology and Medicine. Society for Experimental Biology and Medicine (New York, N.Y.)_, vol. 123, no. 2, Nov. 1966, pp. 504–06. _PubMed_, [https://doi.org/10.3181/00379727-123-31527](https://doi.org/10.3181/00379727-123-31527).

[55] Bolaman, Zahit, et al. ‘Oral versus Intramuscular Cobalamin Treatment in Megaloblastic Anemia: A Single-Center, Prospective, Randomized, Open-Label Study’. _Clinical Therapeutics_, vol. 25, no. 12, Dec. 2003, pp. 3124–34. _PubMed_, [https://doi.org/10.1016/s0149-2918(03)90096-8](https://doi.org/10.1016/s0149-2918(03)90096-8).

[56] Hunt, Janet R. ‘Bioavailability of Iron, Zinc, and Other Trace Minerals from Vegetarian Diets’. _The American Journal of Clinical Nutrition_, vol. 78, no. 3 Suppl, Sept. 2003, pp. 633S-639S. _PubMed_, [https://doi.org/10.1093/ajcn/78.3.633S](https://doi.org/10.1093/ajcn/78.3.633S).

[57] Scrimshaw, N. S. ‘Iron Deficiency’. _Scientific American_, vol. 265, no. 4, Oct. 1991, pp. 46–52. _PubMed_, [https://doi.org/10.1038/scientificamerican1091-46](https://doi.org/10.1038/scientificamerican1091-46).

[58] Rodriguez-Ramiro, I., et al. ‘Estimation of the Iron Bioavailability in Green Vegetables Using an in Vitro Digestion/Caco-2 Cell Model’. _Food Chemistry_, vol. 301, Dec. 2019, p. 125292. _PubMed_, [https://doi.org/10.1016/j.foodchem.2019.125292](https://doi.org/10.1016/j.foodchem.2019.125292).

[59] Pawlak, Roman, and Kami Bell. ‘Iron Status of Vegetarian Children: A Review of Literature’. _Annals of Nutrition & Metabolism_, vol. 70, no. 2, 2017, pp. 88–99. _PubMed_, [https://doi.org/10.1159/000466706](https://doi.org/10.1159/000466706).

[60] Pawlak, Roman, et al. ‘Iron Status of Vegetarian Adults: A Review of Literature’. _American Journal of Lifestyle Medicine_, vol. 12, no. 6, 2018, pp. 486–98. _PubMed_, [https://doi.org/10.1177/1559827616682933](https://doi.org/10.1177/1559827616682933).

[61] Levrat-Verny, M. A., et al. ‘Wholewheat Flour Ensures Higher Mineral Absorption and Bioavailability than White Wheat Flour in Rats’. _The British Journal of Nutrition_, vol. 82, no. 1, July 1999, pp. 17–21. _PubMed_, [https://doi.org/10.1017/s0007114599001075](https://doi.org/10.1017/s0007114599001075).

[62] Hallberg, L., et al. ‘Iron Absorption in Man: Ascorbic Acid and Dose-Dependent Inhibition by Phytate’. _The American Journal of Clinical Nutrition_, vol. 49, no. 1, Jan. 1989, pp. 140–44. _PubMed_, [https://doi.org/10.1093/ajcn/49.1.140](https://doi.org/10.1093/ajcn/49.1.140).

[63] Slywitch, Eric, et al. ‘Iron Deficiency in Vegetarian and Omnivorous Individuals: Analysis of 1340 Individuals’. _Nutrients_, vol. 13, no. 9, Aug. 2021, p. 2964. _PubMed_, [https://doi.org/10.3390/nu13092964](https://doi.org/10.3390/nu13092964).

[64] Wilson, A. K., and M. J. Ball. ‘Nutrient Intake and Iron Status of Australian Male Vegetarians’. _European Journal of Clinical Nutrition_, vol. 53, no. 3, Mar. 1999, pp. 189–94. _PubMed_, [https://doi.org/10.1038/sj.ejcn.1600696](https://doi.org/10.1038/sj.ejcn.1600696).

[65] Wißing, Christoph, et al. ‘Stable Isotopes Reveal Patterns of Diet and Mobility in the Last Neandertals and First Modern Humans in Europe’. _Scientific Reports_, vol. 9, no. 1, Mar. 2019, p. 4433. _PubMed_, [https://doi.org/10.1038/s41598-019-41033-3](https://doi.org/10.1038/s41598-019-41033-3).

[66] Raichle, Marcus E., and Debra A. Gusnard. ‘Appraising the Brain’s Energy Budget’. _Proceedings of the National Academy of Sciences of the United States of America_, vol. 99, no. 16, Aug. 2002, pp. 10237–39. _PubMed_, [https://doi.org/10.1073/pnas.172399499](https://doi.org/10.1073/pnas.172399499).

[67] Ben-Dor, Miki, et al. ‘Man the Fat Hunter: The Demise of Homo Erectus and the Emergence of a New Hominin Lineage in the Middle Pleistocene (ca. 400 Kyr) Levant’. _PloS One_, vol. 6, no. 12, 2011, p. e28689. _PubMed_, [https://doi.org/10.1371/journal.pone.0028689](https://doi.org/10.1371/journal.pone.0028689).

[68] Dobersek, Urska, et al. ‘Meat and Mental Health: A Systematic Review of Meat Abstention and Depression, Anxiety, and Related Phenomena’. _Critical Reviews in Food Science and Nutrition_, vol. 61, no. 4, Feb. 2021, pp. 622–35. _Taylor and Francis+NEJM_, [https://doi.org/10.1080/10408398.2020.1741505](https://doi.org/10.1080/10408398.2020.1741505).

[69] Agarwal, Ulka, et al. ‘A Multicenter Randomized Controlled Trial of a Nutrition Intervention Program in a Multiethnic Adult Population in the Corporate Setting Reduces Depression and Anxiety and Improves Quality of Life: The GEICO Study’. _American Journal of Health Promotion: AJHP_, vol. 29, no. 4, 2015, pp. 245–54. _PubMed_, [https://doi.org/10.4278/ajhp.130218-QUAN-72](https://doi.org/10.4278/ajhp.130218-QUAN-72).

[70] Katcher, Heather I., et al. ‘A Worksite Vegan Nutrition Program Is Well-Accepted and Improves Health-Related Quality of Life and Work Productivity’. _Annals of Nutrition & Metabolism_, vol. 56, no. 4, 2010, pp. 245–52. _PubMed_, [https://doi.org/10.1159/000288281](https://doi.org/10.1159/000288281).

[71] Michalak, Johannes, et al. ‘Vegetarian Diet and Mental Disorders: Results from a Representative Community Survey’. _The International Journal of Behavioral Nutrition and Physical Activity_, vol. 9, June 2012, p. 67. _PubMed_, [https://doi.org/10.1186/1479-5868-9-67](https://doi.org/10.1186/1479-5868-9-67).

[72] Fazelian, Siavash, et al. ‘Adherence to the Vegetarian Diet May Increase the Risk of Depression: A Systematic Review and Meta-Analysis of Observational Studies’. _Nutrition Reviews_, vol. 80, no. 2, Jan. 2022, pp. 242–54. _PubMed_, [https://doi.org/10.1093/nutrit/nuab013](https://doi.org/10.1093/nutrit/nuab013).

[73] Young, Melissa F., et al. ‘Maternal Hepcidin Is Associated with Placental Transfer of Iron Derived from Dietary Heme and Nonheme Sources’. _The Journal of Nutrition_, vol. 142, no. 1, Jan. 2012, pp. 33–39. _PubMed_, [https://doi.org/10.3945/jn.111.145961](https://doi.org/10.3945/jn.111.145961).

[74] Peña-Rosas, Juan Pablo, et al. ‘Daily Oral Iron Supplementation during Pregnancy’. _The Cochrane Database of Systematic Reviews_, vol. 2015, no. 7, July 2015, p. CD004736. _PubMed_, [https://doi.org/10.1002/14651858.CD004736.pub5](https://doi.org/10.1002/14651858.CD004736.pub5).

[75] Wallace, Taylor C., et al. ‘Choline: The Underconsumed and Underappreciated Essential Nutrient’. _Nutrition Today_, vol. 53, no. 6, 2018, pp. 240–53. _PubMed_, [https://doi.org/10.1097/NT.0000000000000302](https://doi.org/10.1097/NT.0000000000000302).

[76] Wallace, Taylor C., et al. ‘Choline: The Neurocognitive Essential Nutrient of Interest to Obstetricians and Gynecologists’. _Journal of Dietary Supplements_, vol. 17, no. 6, 2020, pp. 733–52. _PubMed_, [https://doi.org/10.1080/19390211.2019.1639875](https://doi.org/10.1080/19390211.2019.1639875).

[77] Mazidi, Mohsen, and Andre Pascal Kengne. ‘Higher Adherence to Plant-Based Diets Are Associated with Lower Likelihood of Fatty Liver’. _Clinical Nutrition (Edinburgh, Scotland)_, vol. 38, no. 4, Aug. 2019, pp. 1672–77. _PubMed_, [https://doi.org/10.1016/j.clnu.2018.08.010](https://doi.org/10.1016/j.clnu.2018.08.010).

[78] Li, Xiude, et al. ‘A Healthful Plant-Based Diet Is Associated with Lower Odds of Nonalcoholic Fatty Liver Disease’. _Nutrients_, vol. 14, no. 19, Oct. 2022, p. 4099. _PubMed_, [https://doi.org/10.3390/nu14194099](https://doi.org/10.3390/nu14194099).

[79] Yang, Chao-Qun, et al. ‘Dietary Patterns Modulate the Risk of Non-Alcoholic Fatty Liver Disease in Chinese Adults’. _Nutrients_, vol. 7, no. 6, June 2015, pp. 4778–91. _PubMed_, [https://doi.org/10.3390/nu7064778](https://doi.org/10.3390/nu7064778).

[80] Chiarioni, Giuseppe, et al. ‘Vegan Diet Advice Might Benefit Liver Enzymes in Nonalcoholic Fatty Liver Disease: An Open Observational Pilot Study’. _Journal of Gastrointestinal and Liver Diseases: JGLD_, vol. 30, no. 1, Mar. 2021, pp. 81–87. _PubMed_, [https://doi.org/10.15403/jgld-3064](https://doi.org/10.15403/jgld-3064).

[81] Mokhtari, Zeinab, et al. ‘Egg Consumption and Risk of Non-Alcoholic Fatty Liver Disease’. _World Journal of Hepatology_, vol. 9, no. 10, Apr. 2017, pp. 503–09. _PubMed_, [https://doi.org/10.4254/wjh.v9.i10.503](https://doi.org/10.4254/wjh.v9.i10.503).

[82] Asemi, Z., et al. ‘Total, Dietary, and Supplemental Calcium Intake and Mortality from All-Causes, Cardiovascular Disease, and Cancer: A Meta-Analysis of Observational Studies’. _Nutrition, Metabolism, and Cardiovascular Diseases: NMCD_, vol. 25, no. 7, July 2015, pp. 623–34. _PubMed_, [https://doi.org/10.1016/j.numecd.2015.03.008](https://doi.org/10.1016/j.numecd.2015.03.008).

[83] Pana, Tiberiu A., et al. ‘Calcium Intake, Calcium Supplementation and Cardiovascular Disease and Mortality in the British Population: EPIC-Norfolk Prospective Cohort Study and Meta-Analysis’. _European Journal of Epidemiology_, vol. 36, no. 7, July 2021, pp. 669–83. _PubMed_, [https://doi.org/10.1007/s10654-020-00710-8](https://doi.org/10.1007/s10654-020-00710-8).

[84] Wang, Xia, et al. ‘Dietary Calcium Intake and Mortality Risk from Cardiovascular Disease and All Causes: A Meta-Analysis of Prospective Cohort Studies’. _BMC Medicine_, vol. 12, Sept. 2014, p. 158. _PubMed_, [https://doi.org/10.1186/s12916-014-0158-6](https://doi.org/10.1186/s12916-014-0158-6).

[85] Yang, Chao, et al. ‘The Evidence and Controversy Between Dietary Calcium Intake and Calcium Supplementation and the Risk of Cardiovascular Disease: A Systematic Review and Meta-Analysis of Cohort Studies and Randomized Controlled Trials’. _Journal of the American College of Nutrition_, vol. 39, no. 4, May 2020, pp. 352–70. _Taylor and Francis+NEJM_, [https://doi.org/10.1080/07315724.2019.1649219](https://doi.org/10.1080/07315724.2019.1649219).

[86] Arasu, Kanimolli, et al. ‘Effect of Soluble Corn Fibre and Calcium Supplementation on Bone Mineral Content and Bone Mineral Density in Preadolescent Malaysian Children-a Double-Blind Randomised Controlled Trial (PREBONE-Kids Study)’. _Osteoporosis International: A Journal Established as Result of Cooperation between the European Foundation for Osteoporosis and the National Osteoporosis Foundation of the USA_, vol. 34, no. 4, Apr. 2023, pp. 783–92. _PubMed_, [https://doi.org/10.1007/s00198-023-06702-0](https://doi.org/10.1007/s00198-023-06702-0).

[87] Voulgaridou, Gavriela, et al. ‘Vitamin D and Calcium in Osteoporosis, and the Role of Bone Turnover Markers: A Narrative Review of Recent Data from RCTs’. _Diseases (Basel, Switzerland)_, vol. 11, no. 1, Feb. 2023, p. 29. _PubMed_, [https://doi.org/10.3390/diseases11010029](https://doi.org/10.3390/diseases11010029).

[88] Tong, Tammy Y. N., et al. ‘Vegetarian and Vegan Diets and Risks of Total and Site-Specific Fractures: Results from the Prospective EPIC-Oxford Study’. _BMC Medicine_, vol. 18, no. 1, Nov. 2020, p. 353. _PubMed_, [https://doi.org/10.1186/s12916-020-01815-3](https://doi.org/10.1186/s12916-020-01815-3).

[89] Ma, Baoshan, et al. ‘Causal Associations of Anthropometric Measurements With Fracture Risk and Bone Mineral Density: A Mendelian Randomization Study’. _Journal of Bone and Mineral Research: The Official Journal of the American Society for Bone and Mineral Research_, vol. 36, no. 7, July 2021, pp. 1281–87. _PubMed_, [https://doi.org/10.1002/jbmr.4296](https://doi.org/10.1002/jbmr.4296).

[90] Nethander, Maria, et al. ‘Assessment of the Genetic and Clinical Determinants of Hip Fracture Risk: Genome-Wide Association and Mendelian Randomization Study’. _Cell Reports. Medicine_, vol. 3, no. 10, Oct. 2022, p. 100776. _PubMed_, [https://doi.org/10.1016/j.xcrm.2022.100776](https://doi.org/10.1016/j.xcrm.2022.100776).

[91] Chan, Mei Y., et al. ‘Relationship between Body Mass Index and Fracture Risk Is Mediated by Bone Mineral Density’. _Journal of Bone and Mineral Research: The Official Journal of the American Society for Bone and Mineral Research_, vol. 29, no. 11, Nov. 2014, pp. 2327–35. _PubMed_, [https://doi.org/10.1002/jbmr.2288](https://doi.org/10.1002/jbmr.2288).

[92] Castro, Jonathan P., et al. ‘Differential Effect of Obesity on Bone Mineral Density in White, Hispanic and African American Women: A Cross Sectional Study’. _Nutrition & Metabolism_, vol. 2, no. 1, Apr. 2005, p. 9. _PubMed_, [https://doi.org/10.1186/1743-7075-2-9](https://doi.org/10.1186/1743-7075-2-9).

[93] Filardo, Giovanni, et al. ‘Categorizing BMI May Lead to Biased Results in Studies Investigating In-Hospital Mortality after Isolated CABG’. _Journal of Clinical Epidemiology_, vol. 60, no. 11, Nov. 2007, pp. 1132–39. _PubMed_, [https://doi.org/10.1016/j.jclinepi.2007.01.008](https://doi.org/10.1016/j.jclinepi.2007.01.008).

[94] Thorpe, Donna L., et al. ‘Dietary Patterns and Hip Fracture in the Adventist Health Study 2: Combined Vitamin D and Calcium Supplementation Mitigate Increased Hip Fracture Risk among Vegans’. _The American Journal of Clinical Nutrition_, vol. 114, no. 2, Aug. 2021, pp. 488–95. _PubMed_, [https://doi.org/10.1093/ajcn/nqab095](https://doi.org/10.1093/ajcn/nqab095).

[95] Sellmeyer, D. E., et al. ‘A High Ratio of Dietary Animal to Vegetable Protein Increases the Rate of Bone Loss and the Risk of Fracture in Postmenopausal Women. Study of Osteoporotic Fractures Research Group’. _The American Journal of Clinical Nutrition_, vol. 73, no. 1, Jan. 2001, pp. 118–22. _PubMed_, [https://doi.org/10.1093/ajcn/73.1.118](https://doi.org/10.1093/ajcn/73.1.118).

[96] Purchas, R. W., et al. ‘Concentrations in Beef and Lamb of Taurine, Carnosine, Coenzyme Q(10), and Creatine’. _Meat Science_, vol. 66, no. 3, Mar. 2004, pp. 629–37. _PubMed_, [https://doi.org/10.1016/S0309-1740(03)00181-5](https://doi.org/10.1016/S0309-1740(03)00181-5).

[97] Rosqvist, Fredrik, et al. ‘Potential Role of Milk Fat Globule Membrane in Modulating Plasma Lipoproteins, Gene Expression, and Cholesterol Metabolism in Humans: A Randomized Study’. _The American Journal of Clinical Nutrition_, vol. 102, no. 1, July 2015, pp. 20–30. _PubMed_, [https://doi.org/10.3945/ajcn.115.107045](https://doi.org/10.3945/ajcn.115.107045).

[98] Rodríguez-Morató, Jose, et al. ‘Comparison of the Postprandial Metabolic Fate of U-13C Stearic Acid and U-13C Oleic Acid in Postmenopausal Women’. _Arteriosclerosis, Thrombosis, and Vascular Biology_, vol. 40, no. 12, Dec. 2020, pp. 2953–64. _PubMed_, [https://doi.org/10.1161/ATVBAHA.120.315260](https://doi.org/10.1161/ATVBAHA.120.315260).

[99] Bergeron, Nathalie, et al. ‘Effects of Red Meat, White Meat, and Nonmeat Protein Sources on Atherogenic Lipoprotein Measures in the Context of Low Compared with High Saturated Fat Intake: A Randomized Controlled Trial’. _The American Journal of Clinical Nutrition_, vol. 110, no. 1, July 2019, pp. 24–33. _PubMed_, [https://doi.org/10.1093/ajcn/nqz035](https://doi.org/10.1093/ajcn/nqz035).

[100] Mozaffarian, Dariush, et al. ‘Food Compass Is a Nutrient Profiling System Using Expanded Characteristics for Assessing Healthfulness of Foods’. _Nature Food_, vol. 2, no. 10, Oct. 2021, pp. 809–18. [_www.nature.com_](http://www.nature.com/), [https://doi.org/10.1038/s43016-021-00381-y](https://doi.org/10.1038/s43016-021-00381-y).

[101] Zeisel, Steven H., and Kerry-Ann da Costa. ‘Choline: An Essential Nutrient for Public Health’. _Nutrition Reviews_, vol. 67, no. 11, Nov. 2009, pp. 615–23. _PubMed_, [https://doi.org/10.1111/j.1753-4887.2009.00246.x](https://doi.org/10.1111/j.1753-4887.2009.00246.x).

[102] Bahnfleth, Charlotte L., et al. ‘Prenatal Choline Supplementation Improves Child Sustained Attention: A 7-Year Follow-up of a Randomized Controlled Feeding Trial’. _FASEB Journal: Official Publication of the Federation of American Societies for Experimental Biology_, vol. 36, no. 1, Jan. 2022, p. e22054. _PubMed_, [https://doi.org/10.1096/fj.202101217R](https://doi.org/10.1096/fj.202101217R).

[103] Colombo, John, et al. ‘Prenatal DHA Supplementation and Infant Attention’. _Pediatric Research_, vol. 80, no. 5, Nov. 2016, pp. 656–62. _PubMed_, [https://doi.org/10.1038/pr.2016.134](https://doi.org/10.1038/pr.2016.134).

[104] Ramakrishnan, Usha, et al. ‘Prenatal Supplementation with DHA Improves Attention at 5 y of Age: A Randomized Controlled Trial’. _The American Journal of Clinical Nutrition_, vol. 104, no. 4, Oct. 2016, pp. 1075–82. _PubMed_, [https://doi.org/10.3945/ajcn.114.101071](https://doi.org/10.3945/ajcn.114.101071).

[105] Klatt, Kevin C., et al. ‘Prenatal Choline Supplementation Improves Biomarkers of Maternal Docosahexaenoic Acid (DHA) Status among Pregnant Participants Consuming Supplemental DHA: A Randomized Controlled Trial’. _The American Journal of Clinical Nutrition_, vol. 116, no. 3, Sept. 2022, pp. 820–32. _PubMed_, [https://doi.org/10.1093/ajcn/nqac147](https://doi.org/10.1093/ajcn/nqac147).

[106] Jackson, Kristina Harris, et al. ‘Baseline Red Blood Cell and Breast Milk DHA Levels Affect Responses to Standard Dose of DHA in Lactating Women on a Controlled Feeding Diet’. _Prostaglandins, Leukotrienes, and Essential Fatty Acids_, vol. 166, Mar. 2021, p. 102248. _PubMed_, [https://doi.org/10.1016/j.plefa.2021.102248](https://doi.org/10.1016/j.plefa.2021.102248).

[107] Loinard-González, Aura Alex P., et al. ‘Genetic Variants in One-Carbon Metabolism and Their Effects on DHA Biomarkers in Pregnant Women: A Post-Hoc Analysis’. _Nutrients_, vol. 14, no. 18, Sept. 2022, p. 3801. _PubMed_, [https://doi.org/10.3390/nu14183801](https://doi.org/10.3390/nu14183801).

[108] Wu, Guoyao. ‘Important Roles of Dietary Taurine, Creatine, Carnosine, Anserine and 4-Hydroxyproline in Human Nutrition and Health’. _Amino Acids_, vol. 52, no. 3, Mar. 2020, pp. 329–60. _PubMed_, [https://doi.org/10.1007/s00726-020-02823-6](https://doi.org/10.1007/s00726-020-02823-6).

[109] Lourenço, R., and M. E. Camilo. ‘Taurine: A Conditionally Essential Amino Acid in Humans? An Overview in Health and Disease’. _Nutricion Hospitalaria_, vol. 17, no. 6, 2002, pp. 262–70.

[110] Avgerinos, Konstantinos I., et al. ‘Effects of Creatine Supplementation on Cognitive Function of Healthy Individuals: A Systematic Review of Randomized Controlled Trials’. _Experimental Gerontology_, vol. 108, July 2018, pp. 166–73. _PubMed_, [https://doi.org/10.1016/j.exger.2018.04.013](https://doi.org/10.1016/j.exger.2018.04.013).

[111] McMorris, T., et al. ‘Effect of Creatine Supplementation and Sleep Deprivation, with Mild Exercise, on Cognitive and Psychomotor Performance, Mood State, and Plasma Concentrations of Catecholamines and Cortisol’. _Psychopharmacology_, vol. 185, no. 1, Mar. 2006, pp. 93–103. _PubMed_, [https://doi.org/10.1007/s00213-005-0269-z](https://doi.org/10.1007/s00213-005-0269-z).

[112] Dickinson, Hayley, et al. ‘Creatine Supplementation during Pregnancy: Summary of Experimental Studies Suggesting a Treatment to Improve Fetal and Neonatal Morbidity and Reduce Mortality in High-Risk Human Pregnancy’. _BMC Pregnancy and Childbirth_, vol. 14, Apr. 2014, p. 150. _PubMed_, [https://doi.org/10.1186/1471-2393-14-150](https://doi.org/10.1186/1471-2393-14-150).

[113] Park, Sanghee, et al. ‘Metabolic Evaluation of the Dietary Guidelines’ Ounce Equivalents of Protein Food Sources in Young Adults: A Randomized Controlled Trial’. _The Journal of Nutrition_, vol. 151, no. 5, May 2021, pp. 1190–96. _PubMed_, [https://doi.org/10.1093/jn/nxaa401](https://doi.org/10.1093/jn/nxaa401).

[114] Gehring, Joséphine, et al. ‘Consumption of Ultra-Processed Foods by Pesco-Vegetarians, Vegetarians, and Vegans: Associations with Duration and Age at Diet Initiation’. _The Journal of Nutrition_, vol. 151, no. 1, Jan. 2021, pp. 120–31. _PubMed_, [https://doi.org/10.1093/jn/nxaa196](https://doi.org/10.1093/jn/nxaa196).

[115] Schurgers, Leon J., et al. ‘Novel Effects of Diets Enriched with Corn Oil or with an Olive Oil/Sunflower Oil Mixture on Vitamin K Metabolism and Vitamin K-Dependent Proteins in Young Men’. _Journal of Lipid Research_, vol. 43, no. 6, June 2002, pp. 878–84.

[116] Okuyama, Harumi, et al. ‘Medicines and Vegetable Oils as Hidden Causes of Cardiovascular Disease and Diabetes’. _Pharmacology_, vol. 98, no. 3–4, 2016, pp. 134–70. _PubMed_, [https://doi.org/10.1159/000446704](https://doi.org/10.1159/000446704).

[117] Hashimoto, Yoko, et al. ‘Canola and Hydrogenated Soybean Oils Accelerate Ectopic Bone Formation Induced by Implantation of Bone Morphogenetic Protein in Mice’. _Toxicology Reports_, vol. 1, 2014, pp. 955–62. _PubMed_, [https://doi.org/10.1016/j.toxrep.2014.10.021](https://doi.org/10.1016/j.toxrep.2014.10.021).

[118] Okuyama, Harumi, et al. ‘A Critical Review of the Consensus Statement from the European Atherosclerosis Society Consensus Panel 2017’. _Pharmacology_, vol. 101, no. 3–4, 2018, pp. 184–218. _PubMed_, [https://doi.org/10.1159/000486374](https://doi.org/10.1159/000486374).

[119] Raederstorff, Daniel, et al. ‘Vitamin E Function and Requirements in Relation to PUFA’. _The British Journal of Nutrition_, vol. 114, no. 8, Oct. 2015, pp. 1113–22. _PubMed_, [https://doi.org/10.1017/S000711451500272X](https://doi.org/10.1017/S000711451500272X).

[120] North, K., and J. Golding. ‘A Maternal Vegetarian Diet in Pregnancy Is Associated with Hypospadias. The ALSPAC Study Team. Avon Longitudinal Study of Pregnancy and Childhood’. _BJU International_, vol. 85, no. 1, Jan. 2000, pp. 107–13. _PubMed_, [https://doi.org/10.1046/j.1464-410x.2000.00436.x](https://doi.org/10.1046/j.1464-410x.2000.00436.x).

[121] Carmichael, Suzan L., et al. ‘Hypospadias and Maternal Intake of Phytoestrogens’. _American Journal of Epidemiology_, vol. 178, no. 3, Aug. 2013, pp. 434–40. _PubMed_, [https://doi.org/10.1093/aje/kws591](https://doi.org/10.1093/aje/kws591).

[122] Rashid, Rumaisa, et al. ‘Genistein Lowers Fertility with Pronounced Effect in Males: Meta-Analyses on Pre-Clinical Studies’. _Andrologia_, vol. 54, no. 9, Oct. 2022, p. e14511. _PubMed_, [https://doi.org/10.1111/and.14511](https://doi.org/10.1111/and.14511).

[123] Canada, Health. _Soy Leghemoglobin (LegH) Preparation as an Ingredient in a Simulated Meat Product and Other Ground Beef Analogues_. 4 May 2021, [https://www.canada.ca/en/health-canada/services/food-nutrition/genetically-modified-foods-other-novel-foods/approved-products/soy-leghemoglobin/document.html](https://www.canada.ca/en/health-canada/services/food-nutrition/genetically-modified-foods-other-novel-foods/approved-products/soy-leghemoglobin/document.html).

[124] Proulx, Amy K., and Manju B. Reddy. ‘Iron Bioavailability of Hemoglobin from Soy Root Nodules Using a Caco-2 Cell Culture Model’. _Journal of Agricultural and Food Chemistry_, vol. 54, no. 4, Feb. 2006, pp. 1518–22. _PubMed_, [https://doi.org/10.1021/jf052268l](https://doi.org/10.1021/jf052268l).

[125] ImpossibleFoods. _Twitter_, [https://twitter.com/ImpossibleFoods/status/1000397509196505089?s=20](https://twitter.com/ImpossibleFoods/status/1000397509196505089?s=20).

[126] Rodhouse, J. C., et al. ‘Red Kidney Bean Poisoning in the UK: An Analysis of 50 Suspected Incidents between 1976 and 1989’. _Epidemiology and Infection_, vol. 105, no. 3, Dec. 1990, pp. 485–91. _PubMed_, [https://doi.org/10.1017/s095026880004810x](https://doi.org/10.1017/s095026880004810x).

[127] Thompson, Lilian U., et al. ‘Effect of Heat Processing on Hemagglutinin Activity in Red Kidney Beans’. _Journal of Food Science_, vol. 48, no. 1, Jan. 1983, pp. 235–36. _DOI.org (Crossref)_, [https://doi.org/10.1111/j.1365-2621.1983.tb14831.x](https://doi.org/10.1111/j.1365-2621.1983.tb14831.x).

[128] Adamcová, Anežka, et al. ‘Lectin Activity in Commonly Consumed Plant-Based Foods: Calling for Method Harmonization and Risk Assessment’. _Foods (Basel, Switzerland)_, vol. 10, no. 11, Nov. 2021, p. 2796. _PubMed_, [https://doi.org/10.3390/foods10112796](https://doi.org/10.3390/foods10112796).

[129] Pei, Yaqiong, et al. ‘Impact of Plant Extract on the Gastrointestinal Fate of Nutraceutical-Loaded Nanoemulsions: Phytic Acid Inhibits Lipid Digestion but Enhances Curcumin Bioaccessibility’. _Food & Function_, vol. 10, no. 6, June 2019, pp. 3344–55. _PubMed_, [https://doi.org/10.1039/c9fo00545e](https://doi.org/10.1039/c9fo00545e).

[130] Yuangklang, C., et al. ‘Effect of Sodium Phytate Supplementation on Fat Digestion and Cholesterol Metabolism in Female Rats’. _Journal of Animal Physiology and Animal Nutrition_, vol. 89, no. 11–12, Dec. 2005, pp. 373–78. _PubMed_, [https://doi.org/10.1111/j.1439-0396.2005.00525.x](https://doi.org/10.1111/j.1439-0396.2005.00525.x).

[131] Hansen, W. E. ‘Effect of Dietary Fiber on Pancreatic Lipase Activity in Vitro’. _Pancreas_, vol. 2, no. 2, 1987, pp. 195–98. _PubMed_, [https://doi.org/10.1097/00006676-198703000-00012](https://doi.org/10.1097/00006676-198703000-00012).

[132] Chartoumpekis, Dionysios V., et al. ‘Broccoli Sprout Beverage Is Safe for Thyroid Hormonal and Autoimmune Status: Results of a 12-Week Randomized Trial’. _Food and Chemical Toxicology: An International Journal Published for the British Industrial Biological Research Association_, vol. 126, Apr. 2019, pp. 1–6. _PubMed_, [https://doi.org/10.1016/j.fct.2019.02.004](https://doi.org/10.1016/j.fct.2019.02.004).

[133] ‘Animal Source Foods in Ethical, Sustainable & Healthy Diets: Nutrients Are Not Always Easily Obtained from Plants Only’. _Animal Source Foods in Ethical, Sustainable & Healthy Diets_, [https://aleph-2020.blogspot.com/2019/05/animal-source-foods-provide-nutrients.html](https://aleph-2020.blogspot.com/2019/05/animal-source-foods-provide-nutrients.html). Accessed 30 Apr. 2023.

[134] Watzl, Margrit Richter, Heiner Boeing, Dorle Grünewald-Funk, Helmut Heseker, Anja Kroke, Eva Leschik-Bonnet, Helmut Oberritter, Daniela Strohm, Bernhard. _Vegan Diet_. 15 June 2016, [https://www.ernaehrungs-umschau.de/english-articles/15-06-2016-vegan-diet/](https://www.ernaehrungs-umschau.de/english-articles/15-06-2016-vegan-diet/).

[135] Lemale, J., et al. ‘Vegan Diet in Children and Adolescents. Recommendations from the French-Speaking Pediatric Hepatology, Gastroenterology and Nutrition Group (GFHGNP)’. _Archives De Pediatrie: Organe Officiel De La Societe Francaise De Pediatrie_, vol. 26, no. 7, Oct. 2019, pp. 442–50. _PubMed_, [https://doi.org/10.1016/j.arcped.2019.09.001](https://doi.org/10.1016/j.arcped.2019.09.001).

[136] Ferrara, Pietro, et al. ‘The Impact of Lacto-Ovo-/Lacto-Vegetarian and Vegan Diets during Pregnancy on the Birth Anthropometric Parameters of the Newborn’. _The Journal of Maternal-Fetal & Neonatal Medicine: The Official Journal of the European Association of Perinatal Medicine, the Federation of Asia and Oceania Perinatal Societies, the International Society of Perinatal Obstetricians_, vol. 33, no. 23, Dec. 2020, pp. 3900–06. _PubMed_, [https://doi.org/10.1080/14767058.2019.1590330](https://doi.org/10.1080/14767058.2019.1590330).

[137] Sharma, R. K., et al. ‘A STUDY OF EFFECT OF MATERNAL NUTRITION ON INCIDENCE OF LOW BIRTH WEIGHT’. _Indian Journal of Community Medicine_, vol. 24, no. 2, June 1999, p. 64. _journals.lww.com_, [https://journals.lww.com/ijcm/Abstract/1999/24020/A_STUDY_OF_EFFECT_OF_MATERNAL_NUTRITION_ON.4.aspx](https://journals.lww.com/ijcm/Abstract/1999/24020/A_STUDY_OF_EFFECT_OF_MATERNAL_NUTRITION_ON.4.aspx).

[138] Kesary, Yuval, et al. ‘Maternal Plant-Based Diet during Gestation and Pregnancy Outcomes’. _Archives of Gynecology and Obstetrics_, vol. 302, no. 4, Oct. 2020, pp. 887–98. _PubMed_, [https://doi.org/10.1007/s00404-020-05689-x](https://doi.org/10.1007/s00404-020-05689-x).

[139] Sanders, T. A., and S. Reddy. ‘Vegetarian Diets and Children’. _The American Journal of Clinical Nutrition_, vol. 59, no. 5 Suppl, May 1994, pp. 1176S-1181S. _PubMed_, [https://doi.org/10.1093/ajcn/59.5.1176S](https://doi.org/10.1093/ajcn/59.5.1176S).

[140] Yisahak, Samrawit F., et al. ‘Vegetarian Diets during Pregnancy, and Maternal and Neonatal Outcomes’. _International Journal of Epidemiology_, vol. 50, no. 1, Mar. 2021, pp. 165–78. _PubMed_, [https://doi.org/10.1093/ije/dyaa200](https://doi.org/10.1093/ije/dyaa200).

[141] ‘Science Update: Vegetarian Diets during Pregnancy Associated with Small Infant Birth Weight’. [_Https://Www.Nichd.Nih.Gov/_](https://www.nichd.nih.gov/), 21 Dec. 2020, [https://www.nichd.nih.gov/newsroom/news/122120-vegetarian-diets](https://www.nichd.nih.gov/newsroom/news/122120-vegetarian-diets).

[142] Pawlak, R. ‘To Vegan or Not to Vegan When Pregnant, Lactating or Feeding Young Children’. _European Journal of Clinical Nutrition_, vol. 71, no. 11, Nov. 2017, pp. 1259–62. _PubMed_, [https://doi.org/10.1038/ejcn.2017.111](https://doi.org/10.1038/ejcn.2017.111).

[143] Miedziaszczyk, Miłosz, et al. ‘The Safety of a Vegan Diet During Pregnancy’. _Postępy Higieny i Medycyny Doświadczalnej_, vol. 75, no. 1, Jan. 2021, pp. 417–25. _sciendo.com_, [https://doi.org/10.5604/01.3001.0014.9343](https://doi.org/10.5604/01.3001.0014.9343).

[144] Islam, M. Mazharul. ‘The Effects of Low Birth Weight on School Performance and Behavioral Outcomes of Elementary School Children in Oman’. _Oman Medical Journal_, vol. 30, no. 4, July 2015, pp. 241–51. _PubMed_, [https://doi.org/10.5001/omj.2015.50](https://doi.org/10.5001/omj.2015.50).

[145] Falcão, Ila R., et al. ‘Factors Associated with Low Birth Weight at Term: A Population-Based Linkage Study of the 100 Million Brazilian Cohort’. _BMC Pregnancy and Childbirth_, vol. 20, no. 1, Sept. 2020, p. 536. _PubMed_, [https://doi.org/10.1186/s12884-020-03226-x](https://doi.org/10.1186/s12884-020-03226-x).

[146] Misra, Akshay, et al. ‘A Longitudinal Study to Determine Association of Various Maternal Factors with Neonatal Birth Weight at a Tertiary Care Hospital’. _Medical Journal, Armed Forces India_, vol. 71, no. 3, July 2015, pp. 270–73. _PubMed_, [https://doi.org/10.1016/j.mjafi.2015.03.001](https://doi.org/10.1016/j.mjafi.2015.03.001).

[147] Sinha, Bireshwar, et al. ‘Low-Birthweight Infants Born to Short-Stature Mothers Are at Additional Risk of Stunting and Poor Growth Velocity: Evidence from Secondary Data Analyses’. _Maternal & Child Nutrition_, vol. 14, no. 1, Jan. 2018, p. e12504. _PubMed_, [https://doi.org/10.1111/mcn.12504](https://doi.org/10.1111/mcn.12504).

[148] Sørensen, H. T., et al. ‘Birth Weight and Length as Predictors for Adult Height’. _American Journal of Epidemiology_, vol. 149, no. 8, Apr. 1999, pp. 726–29. _PubMed_, [https://doi.org/10.1093/oxfordjournals.aje.a009881](https://doi.org/10.1093/oxfordjournals.aje.a009881).

[149] Wells, Jonathan C. K., and Jay T. Stock. ‘Life History Transitions at the Origins of Agriculture: A Model for Understanding How Niche Construction Impacts Human Growth, Demography and Health’. _Frontiers in Endocrinology_, vol. 11, 2020, p. 325. _PubMed_, [https://doi.org/10.3389/fendo.2020.00325](https://doi.org/10.3389/fendo.2020.00325).

[150] ‘Our Ancestors First Developed Humanlike Brains 1.7 Million Years Ago’. _Big Think_, 14 Apr. 2021, [https://bigthink.com/the-past/brain-evolution/](https://bigthink.com/the-past/brain-evolution/).

[151] Odes, E., et al. ‘Earliest Hominin Cancer: 1.7-Million-Year-Old Osteosarcoma from Swartkrans Cave, South Africa’. _South African Journal of Science_, 2016, [https://doi.org/http://dx.doi.org/10.17159/sajs.2016/20150471](https://doi.org/http://dx.doi.org/10.17159/sajs.2016/20150471).

[152] Thompson, Randall C., et al. ‘Atherosclerosis across 4000 Years of Human History: The Horus Study of Four Ancient Populations’. _Lancet (London, England)_, vol. 381, no. 9873, Apr. 2013, pp. 1211–22. _PubMed_, [https://doi.org/10.1016/S0140-6736(13)60598-X](https://doi.org/10.1016/S0140-6736(13)60598-X).

[153] Kaplan, Hillard, et al. ‘Coronary Atherosclerosis in Indigenous South American Tsimane: A Cross-Sectional Cohort Study’. _Lancet (London, England)_, vol. 389, no. 10080, Apr. 2017, pp. 1730–39. _PubMed_, [https://doi.org/10.1016/S0140-6736(17)30752-3](https://doi.org/10.1016/S0140-6736(17)30752-3).

[154] Karamanou, Marianna, et al. ‘Milestones in the History of Diabetes Mellitus: The Main Contributors’. _World Journal of Diabetes_, vol. 7, no. 1, Jan. 2016, pp. 1–7. _PubMed_, [https://doi.org/10.4239/wjd.v7.i1.1](https://doi.org/10.4239/wjd.v7.i1.1).