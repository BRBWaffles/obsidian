It's basically a sound induction at this point that anti-seed oil clowns won't debate me. My comprehensive, 15,000+ word, 200+ reference [article](https://www.the-nutrivore.com/post/a-comprehensive-rebuttal-to-seed-oil-sophistry) on seed oils is almost three years old, and since it was published, nobody from the opposition has offered even a shred of substantive criticism toward it. A few have tried, but ultimately their efforts were akin to taking halfhearted swats at low-hanging fruit and ultimately failing, rather than actually meaningfully engaging with my core findings and the arguments underpinning my interpretations. They really are just a pathetically weak lot of human beings.

At the risk of sounding self-aggrandizing, I think I have a pretty good idea why these people won't debate me—I have the tools to dismantle them. I'm well-versed on the relevant empirics, I'm intimately familiar with several formal systems of reasoning, I use semantic analysis to make sure my interlocutor's term meanings and usages are understood and remain consistent, I have decent knowledge of philosophy and, more specifically, scientific epistemology, and I deploy an algorithmic debate strategy. I am essentially kryptonite to anti-seed oil quacks. That's probably why they never show up to debate.

My most recent exposure to anti-seed oil cuckery was so profoundly egregious that it warranted writing about it here on my main blog. This extra special example of subhuman behavior comes from Cate Shanahan herself, or as I like to call her, Cate Shenanigans. If you want more info on Cate Shenanigans' history of debate-dodging, I have compiled an inventory [here](https://www.the-nutrivore.com/dodgers). For now, let me start from the beginning.

Nearly a year ago, I was [invited](https://x.com/zbitter/status/1668413822388916224) by Zach Bitter, via X, to debate the proposition "seed oils are not a significant, independent concern for public health" on his podcast with a willing interlocutor. Cate Shenanigans was [tagged](https://x.com/NTxLoneRider/status/1673811898645598211) in this post but did not reply. In fact, every single high-profile seed oil skeptic who responded to the thread declined my invite, while others, like Cate Shenanigans, were pinged and never replied. The nutrition debate arena was a ghost town that day. Not an ounce of courage was to be found among the anti-seed oil clowns.

However, nearly a year after that, Cate Shenanigans put out a general [invite](https://x.com/drcateshanahan/status/1773836787196346592) to debate. Even going so far as to [suggest](https://x.com/drcateshanahan/status/1774490576362062311) that I was scared to face her, to which I replied. Shortly after, she actually decided to engage and [replied](https://x.com/drcateshanahan/status/1774681584907559220) to me, encouraging me to DM her (presumably to work out the details of the debate). After some minor miscommunications, she instructed me in another [reply](https://x.com/drcateshanahan/status/1774830896006185446) to contact her via email. I did. I never received a reply. Some time passed, and Zach Bitter once again started [prodding](https://x.com/zbitter/status/1778171569967190413) at Cate Shenanigans about the status of her debate invitation to me (presumably with the intention of once again offering to host it). A day later, after a brief back and forth, Cate Shenanigans [replied](https://x.com/drcateshanahan/status/1778430104617603232) with what seemed like an alteration in the conditions to debate. At least this is how I interpreted it, as someone who has an interest in her arguments. I [pressed](https://x.com/TheNutrivore/status/1778456234078662984) her for clarification, and even quoted her in a [post](https://x.com/TheNutrivore/status/1778490825124552875) later that day about it, and received no reply. But not before she made her own [post](https://x.com/drcateshanahan/status/1778483241755787308) in a different thread, which I didn't initially see, wherein she withdrew from the debate that day. What a bizarre series of behaviours.

Fast forward to June 2nd, 2024, and I was DMed by another third party looking to coordinate yet another debate between Cate Shenanigans and myself. I won't reveal this person's identity and throw this individual under the bus, because they were nothing but cordial and respectful to me. After a short exchange, I was CC'd into an email chain with Cate Shenanigans, and this is where the fun begins. But rather than narrate the exchange, I'll just post it here:

**Host:**

> Hi cate  
>   
> Nick, CCed, would be down to do the seed oil debate for my show… Are you game? If so my team, CCed, can help to arrange…

**Nick:**

> If the debate proposition is clear and I disagree with it, then absolutely. I generally won't debate anything vague or normative, though. I'll happily debate a proposition like "seed oil consumption increases the risk of heart disease", for example.

**Shenanigans:**

> HI Host  
>   
> Someone else wanted to set up a debate on their podcast between myself and Nick. But what was proposed looked more like just an unstructured argument, and that is not something I’m interested in. I don’t know if the disconnect was with that host or with Nick….? So I’d like to make sure we’re all on the same page about what a debate is.  
>   
> A debate starts with an assertion and the two participants who are debating take opposing sides.  
>   
> So if Nick wants to debate, then the assertion could be “RBD vegetable oils are part of a healthy diet.”  
>   
> Sound good?

**Host:**

> What do you think Nick?

**Nick:**

> First of all, it's not just that someone else has previously wanted to set up a debate between Cate and myself. Cate herself put out what I (quite reasonably) interpreted as a general invitation to those challenging her positions, which can be found [here](https://x.com/drcateshanahan/status/1773836787196346592), on March 29th. I accepted the invite, to which Cate replied on March 31st with an invitation to me to DM her (presumably to set up a debate), which can be found [here](https://x.com/drcateshanahan/status/1774681584907559220). I responded via email on April 1st, after she invited me to do so [here](https://x.com/drcateshanahan/status/1774830896006185446), on the same day. There was no third party present at this time. This was Cate clearly inviting me, personally, to debate. Just to be clear about the facts.  
>   
> Furthermore, while we're being clear about the facts, Cate also presumably withdrew from the debate for what would be completely bizarre reasons that are ultimately orthogonal to the debate itself, which can be found [here](https://x.com/drcateshanahan/status/1778483241755787308). But, not before implying that she had altered the necessary conditions for a debate, which can be found [here](https://x.com/TheNutrivore/status/1778490825124552875). Whatever the case, declining to debate someone on an empirical position because they disagree with you on an unrelated normative position is **beyond** strange, and honestly it was so outlandish that I just flagged it as a blatant excuse not to debate. I hope, Host, you can also see that this is the most reasonable interpretation of her behaviour.  
>   
> Now that that's out of the way and clarified, I'd be happy to debate Cate on a number of propositions. So long as she's done making excuses. Specifically, I might want to debate against [this](https://x.com/drcateshanahan/status/1516786219841073153) proposition:  
>   
> "current levels of seed oil consumption are the main driver of the obesity and chronic disease epidemics that now represent an existential threat to human populations around the world.  
>   
> Out of all of her claims, this could easily be one of the most egregious, depending on how the semantics are unpacked. So, I'd need some of those semantics clarified. Cate, when you say seed oils are the main driver, do you mean that in a counterfactual scenario wherein all the seed oils in the food supply were replaced with low-PUFA alternatives like butter or tallow, those same diseases epidemics would be less likely to occur? If yes, I disagree with the proposition and will happily debate it. If not, what do you mean?  
>   
> As for the stuff about what the debate would be. I typically don't do "structured" debates in the traditional sense. They seem closer to theatrics than debates, with time limits and scheduled topic shifts, opening statements, closing statements, etc. I prefer a linear, continuous Socratic debate format where the truth value of a proposition can be scrutinized to completion with no get-out-of-jail free cards granted by the clock. If it takes many hours to reach a concession, so be it. Other formats typically just make sophistry and dodging easier to get away with, because one party can just filibuster until the clock runs out.  
>   
> Also, Cate, my debate style is far from unstructured. My debate algorithm can be found [here](https://drive.google.com/file/d/1QQaN6HRwzp3kY2DAcnHVBxeX6jBhrvkw/view). So everyone knows what to expect of me. No rational person would look at this and conclude that my approach to debate is unstructured. I just refuse to debate with my hands tied behind my back, or debate under conditions where the proposition can't be scrutinized to completion.

**Host:**

> Thanks Nick!  
> Cate, you game to schedule?

**Shenanigans:**

> Dear Nick and Host,  
>   
> Nick, my tweet "sure I’ll take all comers,” was a casual proposition subject to agreeing on further details, which we have yet to do.  
>   
> Host, from Nick's email it looks like he wants to argue against a primary thesis of my book, without having read my book. That doesn’t seem like a debate to me.   
>   
> In Dark Calories I lay out the groundwork to support my claims across multiple chapters, each with scores of scientific references to support the arguments I make in the book. I can’t skip that information and jump right into a debate on my conclusions.   
>   
> Maybe an analogy will help, here. Let’s say, hypothetically, that after publishing his theory of relativity, Einstein announced in a public forum “Hey everyone I think E=MC squared. Read my paper for supporting arguments." And then, someone wanted to debate his paper without having read it. Einstein would have been in a position to have to spell out many details of his complex thesis, including technical information that would probably not be interesting to a lay audience. (Not when presented in debate format. A good documentarian or science writer would be able to make it interesting, however.) In order to “win” the debate, Einstein may even have had to defend the veracity of fundamental principles of physics. That would take the form of a lecture, or a series of lectures. That’s not a debate. It’s a college course.  
>   
> Therefore, I’ve proposed we debate this: “Vegetable oils are heart healthy.” After all, that is what the AHA claims. It’s also what docctors learn, and it is influencing public policy, what American farmers grow, and what every child and adult eats. It’s an important topic that merits debate. I would argue against and Nick would argue for.  
>   
> Nick, are you willing to do that?

**Host:**

> I love that proposition “Vegetable oils are heart healthy.”  
>   
> What do u think Nick?

**Nick:**

> Just ignoring the fact that, in a blatant display of unbridled narcissism, Cate has unabashedly compared herself to Einstein, and her work to Einstein's theory of relativity, I'd like to ask Cate a few questions:  
>   
> 1. If I read your book, Dark Calories, will you debate the proposition I suggested (provided you actually unpack your semantics in a way that leads me to disagree with it)? If yes, surely you'd be willing to provide me with a free PDF of Dark Calories for me to scrutinize, no?  
>   
> 2. If you wish to debate what I believe on the subject, surely you've done me the likewise courtesy of reading my [article](https://www.the-nutrivore.com/post/a-comprehensive-rebuttal-to-seed-oil-sophistry) on the subject, right? If yes, why did you not suggest a proposition directly from my work?  
>   
> I also have some further objections to Cate's reasoning. Unlike Einstein's theories and models, the predictions of which could only be confirmed many decades later (if not a century later in some cases), Cate's thesis has been tested (and ultimately falsified with respect to human outcome data), which is why the debate would likely lead heavily into empirics and not require a whole book's worth of lectures on fundamentals. So no, I don't need to read her book to show that the proposition is false. Her reasoning here is ridiculous.  
>   
> Respectfully, Host and Cate, "vegetable oils are heart healthy" is an awful proposition, for a number of reasons:  
>   
> 1. It's not my proposition.  
> 2. I don't represent the AHA.  
> 3. I don't even care what the AHA says about seed oils.  
> 4. I don't even know what it means because it's so semantically vague.  
> 5. I can think and reason for myself and can defend my own propositions, thank you.  
>   
> How about we narrow the scope of the proposition I suggested down to a single disease? Since Cate quantifies the detrimental effects of seed oils over the entire scope of chronic diseases that qualify as epidemics, it is entailed logically that we could choose any disease within that scope. So, how about we meet half way on the subject and debate this proposition:  
>   
> "current levels of seed oil consumption are the main driver of heart disease."  
>   
> To the degree that Cate affirms that heart disease is an epidemic, she should affirm that the proposition is true. I affirm that the proposition is false. So, we simply exchange the relevant empirics and have the debate. EZPZ.

**Shenanigans:**

> Dear Host and Nick,  
>   
> I am interested in a debate on the topic I proposed. But I feel this particular email chain is no longer constructive.   
>   
> Resorting to character attacks during what should be a civil conversation is outside the realm of what I consider acceptable discourse.  
>   
> Respectfully,  
> Cate

**Nick:**

> You've attacked my character on multiple occasions, Cate. I don't care about that. Let's just discuss the arguments, and hammer out a path toward a productive debate.  
>   
> It's also ridiculous to imply that I'm not contributing productively to the conversation. I literally suggested a proposition that seems to satisfy us both. If I've failed to do that, please explain how so I can improve upon further suggestions.  
>   
> Just give me a yay or nay, Cate. I don't want to waste my time if you're not interested in actually defending your views or answering any clarifying questions.

**Shenanigans:**

> Nick,  
>   
> As I said two emails ago, because most people, including you, are not familiar with the scientific underpinnings of my work I would first need to explain them, and that is not a debate.  
>   
> What I proposed is a debate. If you are not interested in that debate then I believe we have reached the conclusion of this discussion.  
>   
> Best wishes to you.

**Nick:**

> And I explained to you how that's a dodge. Again, I don't need a lecture on the theoretical underpinnings when the predictions of the hypothesis have been tested and there is tons of empirical data on it. We only need to have a discussion about the empirics that test the hypothesis' fruitfulness, not the theoretical underpinnings. Insisting that I read your book is just filibustering.  
>   
> But hey, I'm still super curious to hear the answer to this question that I already asked you. If I were to read your book, would you debate the prop? Yes or no. If yes, then would you be willing to supply me with a free PDF of your book as a good faith gesture that you're interested in having your work scrutinized? Yes or no.  
>   
> I'm likewise super curious to hear the answer to this further question that I also already asked you. If you instead want to debate my beliefs, have you paid me the likewise courtesy of reading my work? Yes or no. If yes, then why haven't you picked a prop that has been extracted from my work directly? Why in the world do you want me to defend the AHA's prop and not a prop of my own?

**Shenanigans:**

> Nick I am responding to your questions and only your questions and I hope that can be the end of it.  
>   
> "And I explained to you how that's a dodge. Again, I don't need a lecture on the theoretical underpinnings when the predictions of the hypothesis have been tested and there is tons of empirical data on it. We only need to have a discussion about the empirics that test the hypothesis' fruitfulness, not the theoretical underpinnings. Insisting that I read your book is just filibustering.  
>   
> But hey, I'm still super curious to hear the answer to this question that I already asked you. If I were to read your book, would you debate the prop? Yes or no."  
>   
> No. For reasons I explained. Just because you’ve read the book does not mean the audience has and I will still be in the position to essentially give a lecture on the topics in my books. Its simply not suitable for normal debate format. Furthermore, I’m not interested in debating someone who thinks familiarizing himself with the basic underpinnings of the topic being debated is optional.  
>   
> "If yes, then would you be willing to supply me with a free PDF of your book as a good faith gesture that you're interested in having your work scrutinized? Yes or no.  
>   
> N/A  
>   
> "I'm likewise super curious to hear the answer to this further question that I also already asked you. If you instead want to debate my beliefs, have you paid me the likewise courtesy of reading my work?  
>   
> I am familiar with your thoughts. You cite the same type of evidence that the AHA uses to support its heart healthy claim.  
>   
> "Yes or no. If yes, then why haven't you picked a prop that has been extracted from my work directly?  
>   
> Not all propositions are equally interesting. I want to debate something interesting.  
>   
> "Why in the world do you want me to defend the AHA's prop and not a prop of my own?  
>   
> See above. Additionally, keep in mind that I did not bring you into this conversation.

**Nick:**

> So basically you're just dodging. Got it.  
>   
> Before we killscreen the entire enterprise, I would like one more answer to a question, because it might provide for a way forward. With respect to the AHA prop that you bizarrely want me to defend, what does it mean to say that seed oils are heart healthy? For example, does that mean that seed oils decrease the risk of heart disease?  
>   
> Because if that prop cashes out into a prop that I'd be willing to defend, then I don't see a reason why we couldn't debate it. If it means what I think it means, it'll just lead into the exact same empirical debate that you're currently dodging, so it makes no difference to me.

**Host's Producer:**

> Hi Nick and Cate,  
>   
> Host's Producer here; I'm the producer at [redacted]. Host and I agreed that we would like to conclude this discussion and explore other debate topics. The tone of the email discourse is not what we had in mind for our debut episode of this new podcast.  
>   
> We appreciate your consideration.

**Nick:**

> I'd be happy to debate cordially with somebody who is not a sophist. Cate, unfortunately, is one of the most dishonest actors in this entire space and deserves zero respect from anybody (though I would give her more than what she deserves and actually be respectful to her in a verbal discussion for the betterment of your podcast). There are people I could recommend who disagree with me who I don't think are dishonest but rather just confused about the empirics. If you guys are still interested in having a productive seed oil debate, I'd be happy to provide a list.  
>   
> Also if you're worried about my tone during the debate, you need not be concerned. I don't think anybody can actually find more than a single example of me being rude to my interlocutor across what is probably dozens of verbal debates by now. Other than that one exception, which I actually think was justified anyway, I am always polite and cordial with my interlocutor in verbal discussions.

This is where the email exchange ended. So much of what Cate Shenanigans said was just brain-dead lunacy. Let's go through the list.

**A) Suggesting that I defend a proposition that isn't my own**

Why? Why in the world would I be asked to defend a proposition that I, myself, am not even sure I affirm? I'm not even sure what the proposition "seed oils are heart healthy" even means. Does it mean that seed oils will increase your VO2 max? Does it mean seed oils will cure hypertension? Who the hell knows. It was just beyond strange that it was even suggested and demonstrates to me that Cate Shenanigans is operating with a level-zero debate meta.

Furthermore, if she was truly interested in contesting my views, why did she not pay me the likewise courtesy of reading my article and selecting a proposition directly from my own writing? Seems not only to be ridiculously stupid, but also a ridiculously stupid double-standard. Yet another example of Shenanigans-level cuckery.

**B) Shifting the goalpost three times**

Months ago, she dodged me on the grounds that I haven't read her book and that reading her book was necessary to have a debate. I [offered](https://x.com/TheNutrivore/status/1778490825124552875) to read her book then to satisfy the criteria and got no reply. Now, she dodged me for the second time on these grounds, and when I directly offered to read her book and satisfy her criteria, she shifted the goalpost a third time. Apparently, it's not good enough that I read her book, but now the entire audience needs to read her book in order for us to debate. This is just the most cuckish form of sophistry imaginable, and she should be ashamed.

**C) Her goalposts are dumb anyway**

To anyone with even a modicum of understanding with respect to scientific epistemology, it should be quite clear that Cate Shenanigans is dodging. Essentially, the strength of a scientific hypothesis does not rest upon its complexity, its elegance, its creativity, or any other such quality. It rests upon how scientifically virtuous it is, and this is determined by testing the hypothesis' compatibility with the theoretical virtues of science. I wrote a lengthy article on the subject [here](https://www.the-nutrivore.com/post/the-hitchhiker-s-guide-to-quack-smashing).

To determine if a hypothesis is strong, we should ask three fundamental questions, compared to other hypotheses:

1. Does this hypothesis make more novel predictions?
    
2. Does this hypothesis explain a wider breadth of phenomena?
    
3. Does this hypothesis make fewer overall assumptions?
    

There are other questions some philosophers think we should ask, but generally speaking, philosophers of science do not disagree about these three core questions. So, let's take them one by one.

Firstly, the hypothesis doesn't really seem to make very many novel predictions at all. In fact, as I had previously discussed in another blog [article](https://www.the-nutrivore.com/post/a-comprehensive-rebuttal-to-seed-oil-sophistry), the overall weight of the literature heavily favours the negation of Cate Shenanigans' hypothesis. When confronted with this fact on a [podcast](https://gimletmedia.com/shows/science-vs/mehwdgww) (33:00), Cate Shenanigans had no compelling answers and merely insisted that it "didn't make any sense" and that every single study on the subject (150-300 studies by her count) was "flawed". Very persuasive, Dr. Shenanigans. Gold fucking star.

As for the second question, the scope of her hypothesis is actually quite impressive. As she stated in the linked podcast above, seed oils literally have a causal role to play in every disease. That is quite an impressive scope. Think of all the phenomena this accounts for. Looks great for the hypothesis, except when we try to answer the third question.

With respect to the final question we're asking, every disease Cate Shenanigans blames on seed oils in an attempt to inflate her scope, she piles on more and more assumptions. Assumptions that empirically verifiable pathophysiological mechanisms that explain a number of diseases, that have nothing to do with seed oils, are either wrong or so incomplete that they might as well be wrong. This produces an insane amount of assumptions. And that's not even counting the assumptions that are stacked in virtue of the hypothesis' failures to make novel predictions.

As I explained in a previous article, The Hitchhiker's Guide to Quack-Smashing, quacks will often inflate scope at the expense of parsimony and/or fruitfulness. They want to be able to capture the widest scope of phenomena with their hypothesis without actually having evidence or having to explain a damn thing.

No, Dr. Shenanigans, I don't need to read your book in order to debate your proposition that seed oils are responsible for all disease, or chronic disease, or even just heart disease. As I explained in the email exchange, the only thing that is required is an investigation into the empirical data that tests your hypothesis. We can test the fruitfulness, we can test the scope, and we can test the parsimony. You're just dodging because you are scared.

Thanks for reading! If you enjoy my writing and want more content like this, consider pledging to my [Patreon](https://www.patreon.com/thenutrivore)!