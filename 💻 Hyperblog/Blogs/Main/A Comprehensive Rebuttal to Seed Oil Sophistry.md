The popular consciousness has accepted many dietary villains over the course of the last half century, ranging from fat, to protein, to salt, to carbohydrates. More often than not, dietary constituents that have fallen under such scrutiny have been exonerated in time, as more and more scientific data is brought to light. I suspect that there is a growing number of people who are now wrongfully demonizing vegetable oils as well. Both skepticism and generally negative attitudes toward these oils appear to have skyrocketed in recent years, and it can be seen seemingly everywhere.

From what I can tell, almost all of the claims regarding the negative health effects of vegetable oils are essentially rooted in either mechanistic or ecological research. Mechanistic research includes studies such as cell culture studies, animal studies, in-vitro studies, or even some short term human experiments. Ecological research is merely investigating largely unadjusted associations between population-level exposures and outcomes, such as the association between vegetable oil availability in the food supply and type 2 diabetes incidence, for example.

Despite the fact that it is absolutely true that these types of research can be incredibly valuable, it is also almost always extremely inappropriate to extrapolate from from these types of research to population-level health effects. In other words, in the absence of corroborating human outcome data, it is almost always dubious to make claims about what outcomes _will_ happen, based on speculation about intuitive mechanisms. Especially if there is no high internal validity population-level outcome data that actually agrees with the mechanistic speculation to begin with.

  
Ultimately, mechanistic studies carry virtually no information about actual human disease risk itself. Keep this in mind as we parse through the lower- and higher-quality evidence as we go along. This will be important as we explore the various claims made about vegetable oils and their interactions with human health. Let’s start with something familiar and dive into cardiovascular disease.

##### **TABLE OF CONTENTS**

1. [**Abbreviations**](#fhu0m)
    
2. [**Cardiovascular Disease**](#6310b)
    
    1. [Lipoprotein Oxidation](#7m0a)
        
    2. [Heart Disease](#45vog)
        
    
3. [**Cancer**](#5v3p4)
    
    1. [Total Cancer](#2fqaq)
        
    2. [Skin Cancer](#2spd4)
        
    
4. [**Adiposity**](#2ef8o)
    
    1. [2-Arachidonoylglycerol](#enlnb)
        
    2. [Energy Intake](#dfe27)
        
    3. [Thermogenesis](#e5513)
        
    4. [Type 2 Diabetes Mellitus](#87esu)
        
    
5. [**Fatty Liver Diseases**](#cejcv)
    
    1. [Fat Overload Syndrome](#3jgml)
        
    2. [Non-alcoholic Fatty Liver Disease](#anp0q)
        
    
6. [**Autoimmune Diseases**](#71boj)
    
    1. [Rheumatoid Arthritis](#4aseu)
        
    2. [Systemic Lupus Erythematosus](#1ba51)
        
    
7. [**Degenerative Diseases**](#d0li0)
    
    1. [Age-related Macular Degeneration](#dmjlr)
        
    2. [Cognitive Decline](#6uint)
        
    
8. [**Mechanistic Autofellatio**](#eudhi)
    
    1. [Inflammation](#59npn)
        
    2. [Lipid Peroxidation](#bfcqv)
        
    
9. [**Discussion**](#dno0t)
    
10. [**Bibliography**](#347t3)
    

##### **ABBREVIATIONS**

2-AG, 2-arachidonoylglycerol; 8-oxodG, 8-oxo-7,8-dihydro-20-deoxyguanosine; ACR, acrylamide; AMD, age-related macular degeneration; AMI, acute myocardial infarction; ANA, anti-nuclear antibody; ApoB, apolipoprotein B; BCC, basal cell carcinoma; BCSO, blackcurrant seed oil; CAO, canola oil; CAT, catalase; CB1, cannabinoid receptor 1; CD, conjugated dienes; CRP, C-reactive protein; EE, energy expenditure; EPO, evening primrose; FADS, fatty acid desaturase; FO, fish oil; GLA, gamma-linolenic acid; GPx, glutathione peroxidase; GR, glutathione reductase; HDL-C, high density lipoprotein cholesterol; HUB, healthy-user bias; IVLE, intravenous lipid emulsion; LA, linoleic acid; LAVAT, La Veterans Administration Hospital Study; LBM, lean body mass; LDL, low density lipoprotein; MCE, Minnesota Coronary Experiment; MDA, malondialdehyde; MS, multiple sclerosis; NSAID, nonsteroidal anti-inflammatory drugs; O2H2, hydroperoxides; O6:O3, omega-6/omega-3 ratio; PUFA, polyunsaturated fat; RA, rheumatoid arthritis; RCT, randomized controlled trials; RMR, resting metabolic rate; SCC; squamous cell carcinoma; SDHS, Sydney Diet Heart Study; SLE, systemic lupus erythematosus; SO, soybean oil; SOD, superoxide dismutase; SU, sunflower oil; T2DM, type 2 diabetes mellitus; TC, total cholesterol; TEF, thermic effect of feeding; TFA, trans-fat; TNF, tumor necrosis factor; TPN, total parenteral nutrition; VAT, visceral adipose tissue.

##### **CARDIOVASCULAR DISEASE**

###### **LIPOPROTEIN OXIDATION**

The primary mechanism by which vegetable oils are suggested to increase the risk of cardiovascular disease (CVD) is through the oxidation of polyunsaturated fats (PUFA), particularly linoleic acid (LA), in low density lipoprotein (LDL) phospholipid membranes. In the literature, this hypothesis seems to be largely spearheaded by DiNicolantonio and O'Keefe (2018) [[1](https://pubmed.ncbi.nlm.nih.gov/30364556/)]. The authors' case against vegetable oils begins with the claim that vegetable oils contain fatty acids, particularly LA, which increase the susceptibility of LDL to oxidize, and that CVD risk is mediated through the oxidation of LDL.

It is also suggested that saturated fatty acids (SFA) are resistant to this sort of oxidative destruction, which subsequently implies that the substitution of SFA for LA in the diet might lower CVD risk. This hypothesis would seem to have many entailments that run contrary to the common understanding of how these different fats influence CVD risk. So, let's see if these claims and implication actually hold up to scrutiny.

As it turns out, we've thoroughly investigated the effects of altering the fatty acid composition of the diet on LDL oxidation rates in humans. For example, Mata et al. (1996) explored this question with one of the most rigorous studies of its kind. No statistically significant differences between the effects of high-SFA diets and high-PUFA diets on the lag time to LDL oxidation were observed [[2](https://pubmed.ncbi.nlm.nih.gov/8911273/)].

However, diets high in monounsaturated fat (MUFA) diets might actually increase the lag time to LDL oxidation more than either SFA-rich or PUFA-rich diets. It would appear as though this effect could be explained due to the fact that MUFA might be better than SFA at replacing LA in LDL particles. So, it seems as though the implication that SFA could offer unique protection against LDL oxidation is less straightforward than originally suggested.

When comparing LDL that were enriched with PUFA to LDL that were enriched with MUFA, Kratz et al. (2002) observed a stepwise decrease in LDL lag time to oxidation with increasing PUFA, specifically linoleic acid (LA) [[3](https://pubmed.ncbi.nlm.nih.gov/11840183/)]. These results cohere with the results of Mata el al., suggesting that MUFA increases the lag time to LDL oxidation more than SFA or PUFA.

In short, it would at least seem to be true that vegetable oil consumption, when investigated in isolation, increases the susceptibility of LDL to oxidize. But, don't get too excited. As we will discuss, these are paltry changes compared to what can be accomplished with antioxidants.

Despite Kratz et al. claiming that vitamin E doesn't mask the effects of dietary fatty acids on LDL susceptibility to oxidation, their reference for this claim does not clearly support this conclusion. The work of Reaven et al. (1994) was their supporting reference for this claim and it actually paints a slightly different picture [[4](https://pubmed.ncbi.nlm.nih.gov/8148354/)]. The Lag time to LDL oxidation with vitamin E supplementation was more than double that which was observed in the previous study (or nearly any other random population sample from any other study that I've seen for that matter).

Reaven et al. discovered that the lag time to LDL oxidation was enormously high in all groups after a run-in diet that included 1200mg/day of vitamin E for three months before randomization. Compared to the previous study by Kratz et al. (2002), the LDL lag time to oxidation was 150% higher (~60 minutes to ~150 minutes). This is consistent with other research showing that the vitamin E content of LDL particles linearly increases the lag-time to LDL oxidation [[5](https://pubmed.ncbi.nlm.nih.gov/1985404/)].

The effect of dosing vitamin E on increasing the lag time to LDL oxidation observed by Reaven et al. is approximately 7.5x stronger than the effects observed by either Kratz et al. or Mata et al., which involved altering the fatty acid composition of the diet. This strongly suggests that antioxidants are a much stronger lever to pull if one wants to decrease the susceptibility of LDL to oxidize.

All three studies used the same copper sulfate solution methodology to measure the lag-time to oxidation, as they both reference the same source for the methods developed by Esterbauer et al. (1989) [[6](https://pubmed.ncbi.nlm.nih.gov/2722022/)]. As such, it wouldn't seem that the increase in LDL lag-time to oxidation seen with vitamin E supplementation would be due to a difference in measurement methods. So we have a decent proof of concept that antioxidants like vitamin E can probably modulate the lag-time to LDL oxidation to an enormous degree.

Which brings me back to the trial by Kratz et al. (2002), which compared olive oil (OO) versus sunflower oil (SU). Despite the fact that all diet groups were receiving roughly the same amount of vitamin E, the OO group ended up with significantly higher vitamin E status compared to the SU group. This is consistent with the observations that MUFA increases the lag time to LDL oxidation to a greater degree than SFA or PUFA. Which leads to higher vitamin E representation in the end.

It is well understood that higher PUFA intakes increase vitamin E requirements in humans [[7](https://pubmed.ncbi.nlm.nih.gov/26291567/)]. However, the OO group also started with much higher status to begin with, which likely contributed to the effect. Lastly, the run-in diet was high in SFA. Such a diet is almost assuredly to be lower in vitamin E, which could exaggerate the effects of high-PUFA diets.

As discussed earlier, SFA is not vulnerable to lipid peroxidation in any way similar to that of PUFA. However, SFA being less vulnerable to lipid peroxidation typically means that sources of SFA contain very low amounts of antioxidants [[8](https://pubmed.ncbi.nlm.nih.gov/14100992/)]. For example, both coconut oil and butter contain negligible vitamin E and have minimal polyphenols. The potential importance of polyphenol antioxidants in protecting LDL from oxidation has been demonstrated in research investigating OO [[9](https://pubmed.ncbi.nlm.nih.gov/15168036/)].

Figure 2 from Marrugat et al. (2004) shows that virgin OO increases the lag time to oxidation to a greater degree than that of refined OO, with the primary differences between these different OOs being their polyphenol content. Many polyphenols act as antioxidants once they're inside our bodies, so these results are not particularly surprising.

It is likely that the overall dietary pattern matters more than PUFA, or even LA, in altering the susceptibility of LDL to oxidation. This principle has been demonstrated multiple times [[1](https://pubmed.ncbi.nlm.nih.gov/28371298/)0-[1](https://pubmed.ncbi.nlm.nih.gov/30282925/)3].

For example, in a trial by Aronis et al. (2007), diabetic subjects were placed on either a fast food diet that was formulated to be "Mediterranean diet-like" or assigned to follow their usual diet. There was also a third arm of non-diabetics placed on the Mediterranean-style fast food diet. Lag time to LDL oxidation was measured according to the same methodology as described above, with a copper sulfate solution.

It should be noted that the foods were specifically formulated to increase the lag time to LDL oxidation. However, what makes these results more impressive is that the fast food groups (groups A and B) were consuming twice as much PUFA than they were at baseline. Despite this we see some of the longest lag times to LDL oxidation observed in the literature in a population that has not been primed with megadoses of vitamin E for three months.

Altogether this would seem to divulge that diet quality matters more than PUFA, or even LA, for LDL oxidation in the aggregate. We've seen multiple times that low-PUFA or low-LA diets can be outperformed by high-PUFA or high-LA diets of better overall quality. Little things add up, and the effect of diet is greater than that of MUFA alone, SFA alone, or even polyphenols alone. Perhaps not vitamin E alone, though.

But let's be clear. Measuring the lag time to LDL oxidation in this way is highly contrived, and probably not reflective of normal physiological conditions. When you plop the LDL particles in a copper sulfate solution, eventually all of the LDL will oxidize. There's no escaping it. Naturally, the high-PUFA LDL will oxidize marginally sooner. But ultimately, this does not tell us anything about what would happen to these LDL particles under physiological conditions. For that, it might be more useful to look at markers like oxLDL.

A marker like oxLDL can give us a better sense of just how many oxidized LDL are likely to form in the blood after a particular intervention or in a particular context. This is important because merely looking at the lag time to oxidation could give us an exaggerated sense of what is likely to happen in vivo.

For example, it may be the case that when you expose LDL particles to copper sulfate, they oxidize in under an hour. But under physiological conditions, that same oxidation could potentially take days. If the LDL are cleared from the blood before oxidation can occur, then the results of the copper sulfate test are probably not very informative.

One particularly long crossover-style RCT by Palomäki et al. (2010) compared the effects of butter to that of canola oil (CAO) on oxLDL [[1](https://pubmed.ncbi.nlm.nih.gov/21122147/)4]. Overall the butter diet resulted in higher LDL and higher oxLDL. I wasn't able to find many PUFA-SFA substitution trials that measured oxLDL beyond this one study, unfortunately.

Again, I speculate that this is likely the result of SFA being a poor source of antioxidants. Or perhaps it's because baseline diets could have been higher in PUFA, and reducing vegetable oil intakes might just be cutting off robust sources of antioxidants and increasing LDL oxidation. There are not many studies investigating this, so it's not clear at the moment.

But, just for the sake of argument let's assume that high-PUFA diets do increase LDL oxidation relative to high-SFA diets. Would that actually be a bad thing? Perhaps not. One study by Oörni et al. (1997) has identified that oxidized LDL are less likely to be retained within the arterial intima when compared to native LDL [[1](https://pubmed.ncbi.nlm.nih.gov/9261142/)5]. If the LDL are oxidizing in the serum, perhaps this just opens up more disposal pathways for LDL and lowers its chances of getting retained in the subendothelial space to begin with.

Lastly, while we have established that vegetable oil consumption does appear to have an independent impact on LDL oxidation (though the effect is dwarfed by the effect of antioxidants), it is also true that oxLDL isn't actually a robust risk factor for CHD. Wu et al. (2006) discovered that the association between oxLDL and CHD does not survive adjustment for traditional risk factors like apolipoprotein (ApoB) or TC/high density lipoprotein cholesterol (HDL-C) [[1](https://pubmed.ncbi.nlm.nih.gov/16949489/)6].

Essentially this means that after accounting for ApoB or TC/HDL, risk is more closely tracking ApoB or TC/HDL-C, and is not particularly likely to be tracking oxLDL at all. So even if it were the case that diets high in vegetable oils simply increased oxLDL, it wouldn't appear that it moves the needle for risk in the real world. It would also suggest that the hypothetical detriments of increasing LDL oxidation aren't significant enough to offset the LDL-lowering benefits of a high-PUFA diet. As we will discuss later in this section.

In the above study by Wu et al. (2006), the Mercodia 4E6 antibody assay was used to measure oxLDL. Some have argued that this assay is invalid due to supposedly making poor distinctions between native LDL and oxLDL [[1](https://www.ahajournals.org/doi/full/10.1161/01.CIR.0000164264.00913.6D?related-urls=yes&legid=circulationaha%3B111%2F18%2Fe284)7]. However, if the 4E6 assay was truly making poor distinctions between oxLDL and native LDL, the two biomarkers would essentially be proxying for one another to the point of being either interchangeable or even being the same thing. In this scenario, the results of the model would suggest extreme multicollinearity as indicated by similarly (extremely) wide confidence intervals for both results.

If oxLDL and native LDL were truly proxying for one another in the model in this fashion, we'd expect the confidence intervals for each relative risk to be inflated and more likely non-significant. But, there is no evidence of extreme multicollinearity in the results. Therefore, it is unlikely that the 4E6 antibody assay is actually making poor distinctions between oxLDL and native LDL. This is important to consider, because the argument for extreme multicollinearity is the primary criticism used against the 4E6 antibody assay's usefulness. But the argument doesn't actually pan out.

It is instead suggested by skeptics of the 4E6 antibody assay that measures of oxidized phospholipids, such as the E06 antibody assay, are more robust measures of oxLDL than the 4E6 antibody assay. However, the E06 antibody assay is actually more vulnerable to the exact type of confounding that has been suggested for the 4E6 antibody assay. This is explained on Mercodia's website [[1](https://www.mercodia.com/product/oxidized-ldl-elisa/#:~:text=Mercodia%20Oxidized%20LDL%20ELISA%20is,epitope%20in%20oxidized%20ApoB%2D100.&text=Substituting%20aldehydes%20can%20be%20produced,the%20generation%20of%20oxidized%20LDL)8].

_“The proprietary mouse monoclonal antibody 4E6 is developed by professors Holvoet and Collen at the University of Leuven in Belgium. It is directed against a conformational epitope in the ApoB100 moiety of LDL that is generated as a consequence of substitution of at least 60 lysine residues of Apo B100 with aldehydes (Holvoet 2006). This number of substituted lysines corresponds to the minimal number required for scavenger-mediated uptake of oxidized LDL.”_

Essentially, the 4E6 antibody assay makes a clear distinction between native LDL and oxLDL by only binding to ApoB that has been modified sufficiently, as to not be recognizable by LDL receptors. As described on the Mercodia website, the 4E6 antibody only binds to ApoB particles that have >60 of their lysine residues modified by aldehydes, which is the threshold for initiating binding affinity with scavenger receptors, and foreclosing binding affinity with LDL receptors. This makes the 4E6 antibody assay an excellent assay for clearly distinguishing between native LDL and oxLDL.

Mercodia goes on to discuss the reasons for why the E06 antibody assay could be problematic for assessing oxLDL.

_“Substituting aldehydes can be produced by peroxidation of lipids of LDL, resulting in the generation of oxidized LDL. However, lipid peroxidation is not required. Indeed, aldehydes that are released by endothelial cells under oxidative stress or by activated platelets may also induce the oxidative modification of Apo B100 in the absence of lipid peroxidation of LDL.”_

Because lipid peroxidation of the LDL particle's phospholipid membrane is not required for an LDL particle to oxidize, a measurement of oxPL could easily mistake a minimally oxidized LDL particle as an oxLDL. For this reason, it is likely that the 4E6 antibody assay is likely to better reflect the actual number of oxLDL [[1](https://cms.mercodia.com/wp-content/uploads/2019/06/poster-oxldl-know-what-you-measure.pdf)9].

This is relevant because the immune cells that mediate the formation of atherosclerotic plaques only tend to take up maximally oxidized LDL particles, not minimally oxidized LDL particles [[2](https://pubmed.ncbi.nlm.nih.gov/6838433/)0][[2](https://pubmed.ncbi.nlm.nih.gov/24891335/)1]. Maximally oxidized LDL have to be disposed of through scavenger receptor-mediated pathways, rather than LDL receptor-mediated pathways.

If minimally oxidized LDL likely contribute as little to foam cell formation as native LDL, why favour measures of minimally oxidized LDL such as the E06 antibody assay over measures of maximally oxidized LDL such as the 4E6 antibody assay?

Unfortunately, so far no studies have attempted to explore the relationship between oxLDL, as measured by the E06 antibody assay, and CHD outcomes after adjustment for total ApoB. The closest we have is a single study by Tsimikas et al. (2006) that found no correlation between oxPL/ApoB and ApoB [[2](https://pubmed.ncbi.nlm.nih.gov/16750687/)2].

However, if ApoB and oxPL tend to vary in tandem, the oxPL/ApoB ratio might not be expected to change very much from subject to subject. If that is the case, then we would not expect oxPL/ApoB to correlate very well at all. It would be nice to see univariable and multivariable models presented that test for independent effects of these biomarkers.

Nevertheless, if merely having more LA in your body meant that you would increase lipoprotein oxidation and thus get more CHD, we would not expect results like those of the meta-analysis conducted by Marklund et al. (2019) [[2](https://pubmed.ncbi.nlm.nih.gov/30971107/)3]. When investigated closely, the higher your tissue representation, the lower your risk of both CHD and ischemic stroke.

It’s important to note that there was not a single statistically significant increase in risk observed in any of the included cohorts when comparing the lowest tissue LA to the highest tissue LA. One of the more interesting findings was from a regression analysis of the Scottish Heart Health Extended Cohort by Wood et al. (1984), which found a strong inverse correlation between adipose LA and CHD incidence [[2](https://pubmed.ncbi.nlm.nih.gov/6146032/)4].

This certainly isn't what we would expect if the chain of causality is LA -> OxLDL -> CHD. However, as we'll discuss in the next section, this is what we'd expect if the chain of causality is SFA -> LDL -> CHD. So, let's go ahead and dive into that data next.

###### **HEART DISEASE**

Now that we've established that there is scant evidence validating oxLDL as a robust, independent marker of CVD risk, let's move on to actual human outcomes. It is a common belief among certain diet camps that PUFA actually increases CVD risk, and that SFA may even lower the risk of CVD. But is there evidence for this claim?

Let's start with the epidemiological evidence, being sure to keep the original question in mind— does substituting PUFA for SFA lower CVD rates? There have been multiple meta-analyses that have aimed to answer this question, however the application of meta-analysis methods in this context can produce some significant interpretative challenges. Let me explain why.

The relationship between SFA and CVD is nonlinear, and significantly influenced by the degree of replacement with PUFA. As such, the linear summation of prospective cohort studies can hide the nonlinear effect. For example, let's have a look at one of the most heavily-cited meta-analyses by Siri-Tarino et al. (2010), which investigated the relationship between SFA and CVD in prospective cohort studies [[2](https://pubmed.ncbi.nlm.nih.gov/20071648/)5].

As we can see, the aggregated results are null (P = 0.22). However, the I² (a measure of heterogeneity) is 41%, and no attempt was made to investigate the source of that heterogeneity. If we take the time to calculate the intake ranges of each cohort study (when possible), we can test for nonlinearity.

After stratifying the included studies by intake range, we see a statistically significant 30% increase in risk in subgroup one (~25±15g/day) and null results for subgroups two and three (35±15g/day and 45±15g/day, respectively). This is precisely what we'd expect to see if the risk threshold presupposed by the Dietary Guidelines was correct. McGee et al. (1984) needed to be removed for this analysis because SFA intake ranges could not be determined from the provided data.

But perhaps this is an isolated finding. There are many other meta-analyses on this subject [[2](https://pubmed.ncbi.nlm.nih.gov/27697938/)6-[2](https://pubmed.ncbi.nlm.nih.gov/19752542/)9]. Maybe the other meta-analyses on this subject would find something different? Let's find out.

Applying the same methodology to all of the available meta-analyses yields the same result. This time subgroup two is representing the intake threshold presupposed by the Dietary Guidelines (~25±15g/day). We see similar increases in risk in the same subgroup across all published meta-analyses investigating the relationship between SFA and CVD (RR 1.14 [1.07-1.22], P<0.0001).

You may be asking why subgroups one, three, and four are null, whereas subgroup two is not. The answer is quite simple. It's because those ranges are not crossing the threshold of effect. The range of intake represented in subgroup one exists below the threshold of effect, whereas subgroups three and four exist above the threshold of effect. As such, comparing lower intakes to higher intakes within those ranges does not show an additional increase in risk. The relationship between SFA and CVD is sigmoidal.

Comparing intakes that exist on either the floor or ceiling of the risk curve will typically produce null results. Only when the middle intake range is investigated is the increase in risk observable.

But how does PUFA play into this? To answer this question, let's make our own meta-analyses. Let's take all of the cohort studies that fall within that middle range and plot them out for both SFA and PUFA.

**Inclusion Criteria:**

- Prospective cohort studies investigating the relationship between SFA and/or PUFA and CVD.
    
- Endpoints directly related to CVD, coronary heart disease, ischemic heart disease, or myocardial infarction (events, mortality, incidences, etc) are acceptable.
    
- Risk estimates stratified from lowest to highest SFA and/or PUFA intakes.
    
- Cohorts must fit into one of four intake categories:
    

1. 0-10g/day (lower range) to 20-30g/day (upper range)
    
2. 10-20g/day (lower range) to 30-40g/day (upper range)
    
3. 20-30g/day (lower range) to 40-50g/day (upper range)
    
4. >30g/day (lower range). Min. 20g/day difference from lower to upper range
    

**Exclusion Criteria:**

- Studies pooling results across multiple cohorts from different countries.
    
- Risk adjustments controlling for serum cholesterol and hyperlipidemic medications.
    
- Cohorts consisting of people with pre-existing CVD and/or currently taking hyperlipidemic medications.
    
- Studies investigating the same cohorts as other included studies. Tie-breakers are decided based on differences in study quality (e.g., chosen subgroups, endpoints, multivariate adjustment models, etc).
    

Drag-netting the literature yielded a total of 74 studies. 20 studies were excluded due to reporting irrelevant endpoints (e.g., stroke, cerebral hemorrhage, atrial fibrillation, etc). Of the 41 remaining studies, 32 studies were excluded due to including duplicate cohorts, adjusting for blood lipids, being multinational, failing to specify intake ranges, or having an intake range that did not fall into one of the prespecified subgroups.

**Results for Saturated Fat:**

Altogether there were 21 studies that met all of the inclusion criteria [[3](https://pubmed.ncbi.nlm.nih.gov/15668366/)0-[5](https://pubmed.ncbi.nlm.nih.gov/15668366/)0]. Cohorts were stratified across four subgroups, based on absolute grams-per-day intakes of SFA. Total pooled results across all subgroups showed a non-significant increase in risk (RR 1.07 [0.97-1.18], P=0.16). Subgroup two was the only subgroup to reach statistical significance (RR 1.24 [1.10-1.40], P=0.0005).

Again, we see the exact same thing. The increase in risk occurs in the same subgroup, in the same intake range— the same intake range presupposed by the Dietary Guidelines to increase risk.

**Results for Polyunsaturated Fat:**

Altogether there were 10 studies that met all of the inclusion criteria. In order to preserve the PUFA to SFA ratio, cohorts were stratified across four subgroups, based on absolute grams-per-day intakes of SFA. Total pooled results across all subgroups showed a statistically significant reduction in CVD risk (RR 0.91 [0.83-1.00], P=0.04). Subgroup two was the only subgroup to reach statistical significance (RR 0.87 [0.80-0.93], P=0.0002).

This second forest plot was not stratified by PUFA intake. As mentioned above, it was instead stratified by SFA intake. This way the ratio of SFA to PUFA would be better preserved. This is important for understanding the relationship between these two dietary fats.

Both SFA and PUFA have inverse sigmoidal relationships with CVD. The more PUFA you replace with SFA, the higher your risk. The more SFA you replace with PUFA, the lower your risk.

This is exactly the same relationship we see in the RCTs as well [[5](https://pubmed.ncbi.nlm.nih.gov/32827219/)1]. Recently, Hooper et al. (2020) published an enormous meta-analysis and meta-regression analysis for the Cochrane Collaboration, which investigated CVD-related outcomes of many PUFA-SFA substitution RCTs.

Their analysis shows that substituting SFA for PUFA, and crossing the 10% of energy threshold as SFA, increases CVD risk in the aggregate. Especially for CVD events, CVD mortality, CHD mortality, and non-fatal acute myocardial infarction (AMI).

Cochrane also performed multiple meta-regression analyses on the included data. Meta-regression is a tool used to investigate the relationship between two variables when they are controlled by a moderator variable. In this case, the two variables in question are SFA intakes (or PUFA-SFA substitution) and CVD. Multiple moderator variables were modeled.

The only statistically significant moderator variable was total cholesterol (TC) (P=0.04). This suggests that the chain of causality is SFA -> TC -> CVD. This therefore suggests that to the extent that a dietary modification reduces TC, reductions in CVD should follow. This is confirmed in their exploratory analysis later on in the paper.

Additional exploratory meta-analyses by Hooper et al. (2020) also further divulge that SFA reduction lowers total CVD events (analysis 1.35), the best replacement for SFA is PUFA from vegetable oils (analysis 1.44), and the effect is likely via lowering TC (analysis 1.51). This evidence dovetails perfectly with the epidemiological evidence discussed above.

Not included in many of Cochrane's analyses were two studies that are often offered up as damning counterevidence, and often cited in support the notion that vegetable oils increase CVD risk. These two trials are the Sydney Diet Heart Study (SDHS) by Woodhill et al. (1978) and the Minnesota Coronary Experiment (MCE) by Frantz et al. (1989) [[5](https://pubmed.ncbi.nlm.nih.gov/727035/)2-[5](https://pubmed.ncbi.nlm.nih.gov/2643423/)3]. These trials were both designed such that SFA was to be replaced with PUFA in the intervention groups in the form of vegetable oil-based margarines, and PUFA was to be replaced with SFA in the control groups.

Both trials achieved significant differences in TC during the course of both of the trials. However, the SDHS did not achieve a significant difference in the magnitude of the reduction, and the final difference in TC was only around 12mg/dL. While statistically significant, it is questionable whether or not either of these changes in TC were clinically significant. On the other hand, the MCE saw significant reductions in TC.

Neither trial saw a statistically significant difference in either CVD mortality or total mortality.

However, in the aggregate, there is a significant increase in CVD mortality between the two trials. This is concerning, seeing as though these two trials are often touted as the best designed trials that have been conducted in the investigation of this research question, aside from the LA Veterans Administration Hospital Study (LAVAT) by Dayton et al. (1969) [[5](https://www.ahajournals.org/doi/10.1161/01.CIR.40.1S2.II-1)4]. However, the MCE's design ended up allowing participants to enter in and exit out of the trial at their leisure, and this ended up resulting in a mean follow-up time of around 1.5 years. Which is abysmal.

It should be noted that the SDHS was a secondary prevention trial, which means that the subjects had already had a single CVD event at the time of enrollment. Given the questionable clinical relevance of the final differences in TC, it is even more ambiguous how meaningful the findings were. It has been observed that differences in SFA intake don't always change CVD outcomes in high-risk populations [[5](https://www.nature.com/articles/s41598-021-86324-w)5].

Nevertheless, we're left wondering precisely why we didn't see the predicted effect in either of these trials. Aside from the revolving door protocol that was used in the MCE, the designs were not particularly terrible as far as large-scale RCTs go. So, why didn't we see the effect that we see in nearly every other trial of similar design and quality, as mentioned above?

The answer likely lies in the types of fat-based products that were provided to the vegetable oil groups in these two trials. The vegetable oil groups were the groups that were meant to increase PUFA intake. PUFA was provided to subjects primarily in the form of corn oil (CO) based margarines. This is a problem. Margarines are typically made primarily from unsaturated fats, which are liquid at room temperature. In order to make the product solid and spreadable, the oils need to be hardened. There are essentially two standard ways to achieve this effect.

First, we can harden oils through hydrogenation or through emulsification. However, non-hydrogenated margarines were not commonplace on the market prior to the 1970s [[5](https://www.soyinfocenter.com/HSS/margarine2.php)6]. Both the SDHS and the MCE were conducted during the 1960s. At the time, hydrogenation was the preferred method to harden margarines. However, this has the secondary effect of generating trans fatty acids (TFA).

TFAs are associated with an increased risk of many diseases, including CVD. However, TFAs are the only known fatty acid subtypes that have been associated with increased CVD mortality when replacing SFA on a population-level [[5](https://pubmed.ncbi.nlm.nih.gov/30636521/)7].

Basically, TFAs are fucking dangerous— more dangerous than SFA. The CO-based margarine used in the SDHS was "Miracle" brand margarine [[5](https://pubmed.ncbi.nlm.nih.gov/23386268/)8]. According to Dr. Robert Grenfell of the National Cardiovascular Health Director at the Australian Heart Foundation and the Deputy Chairman of the Sydney University Nutrition Research Foundation, Bill Shrapnel, Miracle brand margarine was up to 15% TFA at the time the study was conducted [[5](https://www.ausfoodnews.com.au/2013/02/11/heart-foundation-takes-swipe-at-butter-and-new-study-on-margarine.html)9].

_“When this study began, Miracle margarine contained approximately 15 per cent trans fatty acids, which have the worst effect on heart disease risk of any fat. The adverse effect of the intervention in this study was almost certainly due to the increase in trans fatty acids in the diet”_

If Woodhill et al. (1978) truly fed the subjects in the vegetable oil group (group F) as much of that margarine as they seem to be claiming to in their publication, that could be around 5.7g/day of TFA.

The margarine used in the MCE was likely to be Fleischmann's Original, due to the availability and popularity at the time of the intervention. As well as the fact that the product was developed in direct response to pro-PUFA research that was being conducted at the time.

The only other two margarines that were potentially available in the region at the time the MCE was conducted were Imperial margarine and Mazola margarine. Some Imperial products remained high in TFA until very recently [[6](https://abcnews.go.com/Business/story?id=8182555&page=1)0]. Mazola was pretty much the only non-hydrogenated margarine on the market at the time.

The answer to the question of which margarine was used in the MCE is anybody's guess at this point. But if I were generous, I'd say that based on the margarines that were available (and most likely to be used) at the time the MCE was conducted, there is roughly a 67% chance that the trial was potentially confounded by TFA in the vegetable oil diet. Though Ramsden et al. (2016) remain skeptical that confounding such as this was likely [[6](https://pubmed.ncbi.nlm.nih.gov/27071971/)1].

However, even if the MCE was not confounded by TFA, we would still have a good reason to exclude it from consideration. Like I mentioned earlier, the trial was designed in such a way that the participants had the liberty to enter and exit the trial at their leisure. In the aggregate, there was only a 12-18 month follow-up time. As such, the trial has significantly less power than all other trials that investigated this particular sort of dietary substitution.

Ramsden et al. have argued at length that the control group was likely confounded by TFA to a greater degree than the vegetable oil group. In their view, this could indicate that LA is actually worse for you than TFA. Essentially they claim that vegetable shortening was added to the control group in order to boost the SFA content of the diet, and because vegetable shortening is hydrogenated, it must have contained a large amount of TFA. This is dubious.

Fully hydrogenated oils contain very little TFA [[6](https://pubmed.ncbi.nlm.nih.gov/26048727/)2]. However, partially hydrogenated oils such as most soft margarines of the period contain high levels of TFA. Here is a selection of SOs taken from the USDA's SR28 Nutrient Database [[6](https://www.ars.usda.gov/northeast-area/beltsville-md-bhnrc/beltsville-human-nutrition-research-center/methods-and-application-of-food-composition-laboratory/mafcl-site-pages/sr11-sr28/)3].

As you can see, a tablespoon of partially hydrogenated SO margarine contains 2200% more TFA than a tablespoon of fully hydrogenated SO shortening.

Ramsden et al. (2016) also acknowledge that the vegetable oil group's diet used soft margarines that were likely to contain some TFA. However, they also under-appreciate the fact that fully hydrogenated vegetable shortening contains pretty much the same amount of TFA as its unadulterated, non-hydrogenated counterpart. In fact, this is precisely why fully hydrogenated shortenings are still on the market despite the TFA ban in most developed countries. It's because those fats never had much TFA to begin with.

_“Because the trans fatty acid contents of MCE study diets are not available, one could speculate that the lack of benefit in the intervention group was because of increased consumption of trans fat. Indeed, in addition to liquid corn oil the intervention diet also contained a serum cholesterol lowering soft corn oil polyunsaturated margarine, which likely contained some trans fat. The MCE principal investigator (Ivan Frantz) and co-principal investigator (Ancel Keys), however, were well aware of the cholesterol raising effects of trans fat prior to initiating the MCE.77 Moreover, Frantz and Keys previously devised the diets used in the institutional arm of the National Diet Heart Feasibility Study (NDHS), which achieved the greatest reductions in serum cholesterol of all NDHS study sites.2 Hence, it is highly likely that this experienced MCE team selected products containing as little trans fat as possible to maximize the achieved degree of cholesterol lowering. Perhaps more importantly, it is clear from the MCE grant proposal that common margarines and shortenings (major sources of trans fat) were important components of the baseline hospital diets and the control diet (but not the intervention diet). Thus, confounding by dietary trans fat is an exceedingly unlikely explanation for the lack of benefit of the intervention diet.”_

Given that we can soundly infer that the SDHS study was confounded by TFA, and the MCE was likely to be confounded by TFA, the results of these trials start to make a lot more sense. The effect sizes, and the direction of effect, mirror what we see in epidemiological research, as well. When TFA replaces SFA, risk goes up. When PUFA replaces SFA, risk goes down. If it is still to be maintained by skeptics that vegetable oils increase the risk of CVD, rather than lowering it, they must satisfy the burden of proof with extraordinary evidence.

I would surely be remiss if I did not briefly acknowledge one trial that was left out of the analysis by Hooper et al. (2020), which was the Lyon Diet Heart Study [[6](https://pubmed.ncbi.nlm.nih.gov/7911176/)4]. The reason this trial is noteworthy is due to it being the only experimental investigation of the diet-heart hypothesis that actually made an effort to reduce LA alongside SFA. The trial also saw one of the largest reductions in CVD risk ever recorded in this body of literature, with a massive 73% reduction in CVD risk over four years. Skeptics of vegetable oils would like to attribute this effect to the lowering of LA in the diet, but is there any merit to this?

The trial did achieve statistically significant differences in LA intake between groups, with 5.3% of energy as LA in the control group and only 3.6% of energy as LA in the intervention group [[6](https://pubmed.ncbi.nlm.nih.gov/9989963/)5]. However, if we calculate the absolute intakes of LA for both groups using each group’s total daily calories, we discover that the absolute differences in LA intake between groups amounted to about 4.5g/day— equal to approximately 1.5 teaspoons of SO. In the most clinical, scientific vernacular, this would be referred to as sweet fuck all.

So, why did the trial see such a massive reduction in risk? Likely because, as far as multifactorial interventions go, this trial did its best to knock the ball out of the park. Just take a look at the diets.

Firstly, the groups achieved a difference of 10g/day in SFA and the between-group differences in SFA cross the 25g/day threshold that we discussed earlier. Secondly, the intervention group was consuming significantly fewer foods that are positively associated with CVD, such as whole meat, processed meat, and butter. Lastly, the intervention group was consuming significantly higher amounts of fruit and vegetables (considered together), as well as unsaturated fats, which are strongly inversely associated with CVD.

Bottom line is that there is a lot going on in these diets that could be strongly modifying CVD risk. To attribute the massive 73% reduction in CVD risk to 4.5g/day of LA is frankly dubious, and not replicated anywhere else in the literature, despite it being directly tested. Virtually all other trials that were well controlled found the opposite effect of increasing LA. Honestly, based on what is currently known about LA, I would sooner suspect that the lower LA in the intervention group was likely working against them, not for them.

Lastly, the relationship between LA and CVD has been investigated using Mendelian randomization (MR). You can think of MR as being a method of investigation that is halfway between an RCT and an epidemiological study. Essentially, you find people with gene variants that influence a particular exposure (such as higher tissue LA) and compare their outcomes to other people who do not have those gene variants.

The study methods of MR also assume that genes are randomly distributed in the population, and this should significantly mitigate the potential for certain types of confounding like the healthy-user bias (HUB). It is generally accepted that this methodology offers superior internal validity for these sorts of research questions than typical methods used in the field of epidemiology. It's essentially giving you the best idea of the magnitude of the effect, independent of confounding, that is achievable without lifelong RCTs.

Even though MR data doesn't really need to be mentioned, because we have actual RCT data on this subject. However, it's worthwhile to add it in here just to hammer home how utterly consistent this evidence actually is. This time the data is being sourced from the UK Biobank, which is a massive prospective cohort study that also leverages a repository of biological samples from over 500,000 subjects [[6](https://pubmed.ncbi.nlm.nih.gov/33924952/)6].

Here we see that Park et al. (2021) observed the same relationship yet again. Genetically elevated serum LA was inversely associated with AMI, whereas genetically elevated serum AA was positively associated with AMI. The gene variants that were investigated were related to the function of fatty acid desaturase (FADS) enzymes.

It would seem that those who are less able to convert LA to AA have a decreased risk of AMD, whereas those who are more able to convert LA to AA have an increased risk. Considering the fact that AA conversion is highly regulated, as well as the fact that dietary LA is virtually unavoidable, it seems unlikely that modulating dietary LA would be a reliable way to modulate the conversion of LA into AA [[6](https://pubmed.ncbi.nlm.nih.gov/21663641/)7]. On balance, these findings support increasing dietary LA for CVD prevention.

Overall, the available evidence overwhelmingly favours the inclusion of vegetable oils in the diet, especially if consumed to the exclusion of SFA, for reducing CVD risk. The evidence that would need to be bought forward to topple this paradigm would need to be truly extraordinary.

##### **CANCER**

###### **TOTAL CANCER**

The data most often cited in support of the notion that vegetable oils increase the risk of cancer comes from a post-hoc analysis of the aforementioned study, the LAVAT, by Pearce et al. (1971) [[6](https://pubmed.ncbi.nlm.nih.gov/4100347/)8]. This is likely because this is some of the only randomized controlled trial (RCT) data that actually shows this increase in risk [[6](https://pubmed.ncbi.nlm.nih.gov/21090384/)9]. As we will discuss, this is quite easy to cherry-pick.

The LAVAT was a double-blind RCT was first reported on by Dayton et al. (1969), and aimed to investigate the effects of substituting vegetable oils for animal fat on the risk of CVD. However, cancer was among their secondary endpoints. The researchers actually took enormous care to ensure that the substitution of vegetable oil for animal fat was the only substitution the subjects were making. Even going so far as providing ice cream made out of vegetable oils rather than dairy fat.

It's important to preface this discussion with an acknowledgment that the trial observed statistically significant increases in dietary LA and LA tissue representation in the vegetable oil group. LA was higher across all measured tissue compartments when comparing subjects with an adherence of at least 88% to control.

Essentially, the concern that increasing vegetable oils in the diet increases cancer risk originates from the following figure. Figure 1 from the original analysis by Pearce at al (1971) clearly shows that the cumulative deaths due to carcinoma rise faster in the vegetable oil group than in the control group after around two years into the study.

I must admit that the graph looks pretty scary, and narratives surrounding the findings of this study had me convinced for a while too. However, this graph is incredibly misleading when taken out of context.

Firstly, cancer deaths were not a pre-specified primary endpoint of the trial itself, which means it is questionable whether or not the study was even appropriately powered or equipped to investigate this endpoint in any rigorous way. Secondly, the results are not statistically significant, despite the above figure showing what appears to be a massive divergence in cancer outcomes.

_“During the diet phase (see figure) there were 31 carcinoma deaths in the experimental group and 17 in the control group (χ² = 3.668, p = 0.06).”_

Pearce et al. also performed an analysis wherein they adjusted for the differences in relative adherence between groups. After the adjustment, the findings were consistent with what would be expected from random distribution.

_“Many of the cancer deaths in the experimental group were among those who did not adhere closely to the diet. This reduces the possibility that the feeding of polyunsaturated oils was responsible for the excess carcinoma mortality observed in the experimental group. However, there were significantly more low adherers in the entire experimental group than in the controls (table VI). In both groups, the numbers of cancer deaths among the various adherence strata are compatible with random distribution (table V). A high incidence among high adherers would be expected if some constituent of the experimental diet were contributing to cancer fatality.”_

This basically means that the excess cancer deaths seen in the vegetable oil group were very likely due to chance, and wrongly attributed to poor adherence. For this reason the authors conclude that the high vegetable oil diet was not likely to be the cause of the increase in cancer mortality. In fact, they specifically mention that their findings were an outlier in the context of the wider literature of the time.

Pearce et al. also included a table that stratifies cancer outcomes by the degree of adherence per group.

As mentioned above, the vegetable oil group had significantly more low adherers. As we can see, about 33% of the excess cancer was occurring among low adherers in the vegetable oil group. Let's see what happens when we not only remove the low adherer subgroup, but also compare the between-group difference in carcinoma risk among the highest adherers.

Removing the low adherers nullifies the effect of the vegetable oil diet on cancer outcomes. There are also other plausible explanations for the effect as well. For example, the vegetable oil group contained many more moderate smokers than the animal fat group.

Moderate smokers were defined as those smoking 0.5-1 packs of cigarettes per day. As you can see, observed cancer among moderate smokers was 3x higher in the vegetable oil group, and this is where all the excess cancer risk is accumulating.

This means that low adherers in the vegetable oil group were likely to be smokers, and the smokers were likely to get more cancer. As we would expect [[7](https://pubmed.ncbi.nlm.nih.gov/22943444/)0].

To suggest that the results of the LAVAT demonstrate a higher cancer risk for those consuming vegetable oils is just to misunderstand or misrepresent the data that Pearce et al. reported. The study did not actually divulge an independent effect of vegetable oil intake on cancer mortality. In fact, if we only look at high adherers, there are no statistically significant differences in cancer.

As the authors reported, if the increase in cancer risk was actually a consequence of the vegetable oil diet, we would see more cancer risk among higher adherers. But we don't. We see no statistically significant differences.

Overall, there is insufficient evidence to declare higher vegetable oil intakes to be an independent risk factor for cancer. The available data suggests that, at worst, higher vegetable oil intakes likely have a neutral effect compared to high animal fat intakes. But, the LAVAT was not the only trial to report cancer as a secondary endpoint. Three other trials that substituted vegetable oils for SFA also reported cancer outcomes.

When considering all of the studies in the aggregate (subgroup 2.1.1), we see that the effect of substituting vegetable oils for SFA on cancer is null. But, the LAVAT is still boasting its (dubiously) large effect size. When we remove the moderate smokers, the increased risk is nullified (subgroup 2.1.2). However, further excluding all regular smokers pushes the aggregated results to a non-significant decrease in cancer risk with substituting vegetable oils for SFA (subgroup 2.1.3).

The direction of effect that we see is actually consistent with the wider epidemiology investigating the relationship between LA biomarkers and cancer mortality. Here we see a strong inverse association between tissue LA representation and cancer risk overall [[7](https://pubmed.ncbi.nlm.nih.gov/32020162/)1].

However, this is total cancer. There is still some controversy surrounding whether or not vegetable oils may increase the risk of other cancers. So, let's dive into that literature next.

###### **SKIN CANCER**

There is a meta-analysis by Ruan et al. (2020) investigating the limited epidemiological research on the subject. The results suggest that higher intakes of PUFA could also increase the risk of skin cancer in prospective cohort studies [[7](https://pubmed.ncbi.nlm.nih.gov/31298947/)2]. However, if we take a look at the author's data, we have some troubling findings.

Increasing PUFA intake seems to increase the risk of squamous cell carcinoma (SCC). However, a single study by Park et al. (2018) is contributing 92.6% of the weight [[7](https://pubmed.ncbi.nlm.nih.gov/29636341/)3].

This could be due to the fact that the study's adjustment model lacked several important covariates that may have plausibly changed the association. For example, the multivariate adjustment model adjusted for hair colour rather than skin tone. This is problematic because hair colour does not sufficiently capture the differential effects of skin tone on skin cancer [[7](https://pubmed.ncbi.nlm.nih.gov/23107311/)4].

In at least one of the included prospective cohort studies by Ibiebele et al. (2009) that investigated the relationship between LA and skin cancer included adjustments for skin colour, and their results were null [[7](https://pubmed.ncbi.nlm.nih.gov/19462452/)5]. This further casts doubt on the veracity of the findings reported by Park et al. (2018).

In fact, when tissue representation of LA was investigated by Wallingford et al. (2013) in a separate cohort study, non-significant reduction in the risk of basal cell carcinoma (BCC) can be observed [[7](https://pubmed.ncbi.nlm.nih.gov/23885039/)6]. Additionally, among those with previous skin cancers, higher tissue representation of LA was associated with a statistically significant decrease in risk of re-occurrence (RR 0.54 [0.35-0.82]) A very odd finding if having more LA inside your body is supposed to predispose you to developing skin cancer.

There is also one study by Harris et al. (2005) that investigates the relationship between red blood cell cis-LA content and squamous cell carcinoma [[7](https://pubmed.ncbi.nlm.nih.gov/15824162/)7]. While it is a case-control study and lacks the temporal component that is necessary to establish causality, it is some of the only human outcome data we have on the subject, and the results are null.

If one still believes that higher intakes of LA increases the risk of skin cancer, I have a treat for them. Do you remember the LAVAT we mentioned in the cancer section? It turns out that skin cancer was actually one of their secondary endpoints.

In table II from the post-hoc analysis by Pearce et al. (1971), we see that in the vegetable oil group, there were ten cases of skin cancer, whereas in the animal fat group there were 21. This produces a statistically significant increase in skin cancer risk for the animal fat group, whether or not we exclude or include the post-diet period (RR: 2.11 [1.01-4.43] and 2.09 [1.07-4.11], respectively).

As we discussed earlier, there were no statistically significant differences in cancer risk among heavy smokers, of which there were only two cases in the animal fat group. However, there was a non-significant increase in cancer risk among moderate smokers, of which there were 19 cases in the vegetable oil group.

This suggests that the increase in skin cancer seen in the animal fat group is not due to heavy smoking, as there were only two cases of cancer among heavy smokers in the animal fat group, and we have 21 cases of skin cancer. The vegetable oil group had lower rates of skin cancer despite the fact that there were more moderate smokers, and more cancer risk among moderate smokers, in the vegetable oil group. This increase in risk seen in the animal fat group can't be explained by differences in smoking habits.

Additionally, the methods of MR have also been used by Seviiri et al. (2021) to investigate the relationship between elevated plasma LA and skin cancer [[7](https://pubmed.ncbi.nlm.nih.gov/34088753/)8]. Data was collected for two different keratinocyte cancers, which were BCC and SCC, and the results are consistent with those found in the LAVAT.

There was a statistically significant 6% reduction in BCC risk with genetically elevated plasma LA, whereas AA showed a statistically significant 4% increase in risk. Oddly, the effect size of eicosapentaenoic acid (EPA) was actually larger than that of AA, which would seem to be at odds with the RCT data on the subject [[7](https://pubmed.ncbi.nlm.nih.gov/24265065/)9].

For SCC, the only statistically significant relationship between genetically elevated plasma PUFA was a 4% increased risk with plasma AA. The results for all other PUFA were null.

In conclusion, it is true that there are some cohort studies suggesting that higher intakes of PUFA may increase the risk of skin cancer. However, the results from the LAVAT and MR are actually stronger evidence and concordant in the opposite direction. As such, it would not appear likely that increasing vegetable oil consumption is an independent risk factor for developing skin cancer. In fact, increasing vegetable oil intake could actually reduce the risk of skin cancer.

##### **ADIPOSITY**

###### **2-ARACHIDONOYLGLYCEROL**

The notion that vegetable oils are responsible for the obesity epidemic is extremely pervasive across a wide variety of niche diet communities. The hypothesis that dietary LA increases hunger through the passive production of an endocannabinoid known as 2-arachidonoylglycerol (2-AG) has been articulated by Watkins et al. (2015) [[8](https://pubmed.ncbi.nlm.nih.gov/25610411/)0]. They suggest that this endocannabinoid interacts with cannabinoid receptor type 1 (CB1) and facilitates obesity by stimulating appetite. Essentially, vegetable oils supposedly give us "the munchies" similarly to marijuana, as the story goes. But, does this mechanism actually work?

Indeed, this mechanism does appear to work— in mice [[8](https://pubmed.ncbi.nlm.nih.gov/22334255/)1]. In this study by Alvheim et al. (2012), mice were fed two different diets with varying fatty acid compositions. Essentially, mice were randomized to two diets that contained either moderate fat (35% of energy) or high fat (60% of energy). Within each diet group there were three distinct diet conditions. One of the diet conditions was low in LA (1% of energy), and the two remaining diet conditions were "high" in LA (8% of energy), with one of which also being supplemented with long-chain omega-3s.

By the end of the study, the mice receiving 8% of their energy from LA had consistently higher body weight, with a slightly mitigating effect of supplemented omega-3s in the mice fed a high-fat diet (chart e). The increases in body weight in the mice that were fed high-LA diets was commensurate with increases in 2-AG. Ergo, consuming a high-LA diet will increase 2-AG and facilitate obesity— in mice. But what about humans?

A study by Blüher et al. (2006) involving 60 subjects explored the correlation between body fatness and a number of markers related to the endocannabinoid system. There ended up being significant correlations between circulating 2-AG and obesity [[8](https://pubmed.ncbi.nlm.nih.gov/17065342/)2].

However, there is an issue. Unlike mice, circulating levels of the 2-AG precursor, ARA, did not differ between lean and obese subjects. For this reason, the authors go on to express skepticism toward the hypothesis that 2-AG synthesis is driven passively by the supply of precursors in humans. Instead, they point out that there is more evidence that obesity itself acts to inhibit the degradation of 2-AG.

_“Which mechanisms lead to increased endocannabinoid levels in abdominal obesity? One possibility is the increased supply of precursors for endocannabinoid biosynthesis and/or increased activity of enzymes involved in endocannabinoid synthesis. When studying circulating levels of the precursor arachidonic acid and of oleoylethanolamide, a molecule with endocannabinoid structure and synthesized by the same enzymes that do not activate [cannabinoid] receptors, we did not find any significant correlation with measures of adiposity._

_Thus, decreased endocannabinoid degradation must be considered as a second possibility. Given the overwhelming mass of adipose tissue compared with other organs, a contribution of adipocytes to endocannabinoid inactivation, which may be disturbed in visceral obese subjects, is an attractive hypothesis.”_

This would not be surprising, considering that the impaired clearance and/or degradation of metabolic substrates is a well-characterized phenomenon in human obesity. Not only that, but there is also a large body of research divulging that the production of 2-AG from its precursors is regulated, not passive [[8](https://pubmed.ncbi.nlm.nih.gov/14595399/)3].

While somewhat tangential to the point, it is interesting to note that we have tested the effects of selective CB1-antagonism in humans with a pharmaceutical called Rimonabant [[8](https://pubmed.ncbi.nlm.nih.gov/17054276/)4]. Overall, this drug does appear to reduce energy intake and result in weight loss that is equal to just over a quarter-pound per week.

The reason this is tangential is because CB-antagonists such as Rimonabant are not specifically targeting LA metabolism. All this research tells us is that the endocannabinoid system is involved with the regulation of body weight in humans, but it does not tell us what independent contributions are being made by 2-AG, or even dietary LA for that matter.

###### **ENERGY INTAKE**

If we wish to explore the effects of dietary LA on appetite, there have been a number of short-term fatty acid substitution trials investigating this question. One trial by Naughton et al. (2018) showed an effect of LA on hunger hormones, and a non-significant reduction in prospective energy intake compared to control and high-oleate diets [[8](https://www.ncbi.nlm.nih.gov/pubmed/30261617)5]. However, the authors spuriously claim that there was no effect of the high-LA diet in reducing prospective energy intake. This is an example of the interaction fallacy, because there was actually non-inferiority between the high-LA diet and the other diet conditions.

Of the trials that actually reported ad libitum energy intake in response to diets of varying fatty acid composition, no consistent effects are observed [[8](https://www.ncbi.nlm.nih.gov/pubmed/14694208)6-[9](https://pubmed.ncbi.nlm.nih.gov/28760423/)1]. All together, the short-term evidence is largely a wash. In fact, this was remarked upon by Strik et al. (2010) in table 4 of one such publication, and they included the aggregated findings across a number of different trials investigating the relationship between fatty acid saturation, satiety, and energy intake [[9](https://pubmed.ncbi.nlm.nih.gov/20492735/)2]. Overall, when there was an effect of LA, it tended to decrease energy intake.

In one particularly well-done trial by Strik et al., participants in the three groups were given unlimited access to muffins made using either SFA, MUFA, or PUFA. They would consume these muffins ad libitum one variety at a time, with a washout period between different muffin types.

By the end, all three groups experienced all three muffin types. Each time researchers collected subjective data on satiety and the general satisfaction of the food experience. No differences in any parameters reached statistical significance.

There were no statistically significant differences in hunger, fullness, satisfaction, or prospective consumption (how much more subjects suspected they could eat at that moment). It appeared as though SFA trended toward a decrease in fullness, perhaps. There were also no observed differences in ad libitum energy intake.

_“Mean total [energy intake] and energy contributed by CHO, fat and protein respectively at the ad lib lunch is presented for each treatment in Figure ​3. There was no significant difference in total [energy intake] between lipid treatments (treatment, P > 0.05). Mean (SEM) [energy intake] at lunch was 5275.9 (286.5) kJ, 5227.7 (403.9) kJ, and 5215.6 (329.5) kJ following the SFA-, PUFA-, and MUFA-rich breakfasts respectively.”_

One interesting criticism is that the subjects' diets may have already been so high in LA that the hunger effects were blunted. This supposedly creates the illusion of equal satiety effects between fatty acids, because the hunger-generating effects are masked by a sort of LA tolerance, so to say. While this is possible, it doesn't seem like a particularly strong criticism. The researchers used a crossover-style design specifically to minimize this type of confounding.

I have also heard it proposed that in order to study this effect, researchers may need to run-in the subjects on an LA-depleting diet for months (or even years) leading up to the satiety tests. Luckily, that's not necessary, as this was also investigated in the previously referenced trial by Dayton et al. (1969), the LAVAT. In this ad libitum double-blind RCT, normal weight subjects were randomized to one of two groups. The first group was placed on a vegetable oil containing diet that yielded approximately 42.3g/day of LA, whereas the second group was placed on an animal fat containing diet that yielded only approximately 10.8g/day.

This difference in LA intake produced an enormous increase in adipose LA in the vegetable oil group. It's probably safe to say that these people reached a maximal saturation point, as evidenced by the hyperbolic curve in adipose LA representation over time.

The median LA representation in adipose tissue increased from around 8-11% of total fatty acids to over 30% of total fatty acids across the eight year study. The baseline levels are consistent with levels found in traditional populations like the Tsimane, Inuit, and the Navajo aboriginals [[9](https://pubmed.ncbi.nlm.nih.gov/22624983/)3-[9](https://onlinelibrary.wiley.com/doi/10.1002/cphy.cp050117)5]. While the data published by Martin et al. (2012) is representing the fatty acid composition of breast milk in the Tsimane, it is also true that the LA contents of both breast milk and adipose tissue correlate extremely tightly [[9](https://pubmed.ncbi.nlm.nih.gov/16829413/)6-[9](https://pubmed.ncbi.nlm.nih.gov/9684741/)8].

There were no statistically significant differences between the baseline tissue LA presentation in the LAVAT and measurements taken from either the Navajo or Tsimane (SD estimated for Navajo). However, tissue LA was statistically significantly higher among Inuit when compared to the baseline measurements observed in the LAVAT.

From this, we can likely infer that the subjects in the LAVAT were largely starting from ancestral levels of tissue LA, and increasing tissue LA to approximately threefold higher levels over the eight year trial period. So how did this threefold increase in tissue LA affect their body weight over time? Long story short, it didn't.

Some may speculate that perhaps the 10.8g of LA being consumed in the control group was simply too high, and that the hyperphagic effects of the LA could have been masked by both groups exceeding a particular threshold. This is difficult to reconcile with the fact that the LA intake of the animal fat group was perfectly within bounds when compared to all known estimates of preagricultural LA intakes, as mentioned above [[9](https://pubmed.ncbi.nlm.nih.gov/20860883/)9]. Also, this was an ad libitum trial in normal weight subjects and body weight remained within 2% of baseline.

The LA intakes in the vegetable oil group universally overshoot the upper bounds of all of those same estimates of preagricultural LA intakes. This means that the animal fat group was consuming levels of LA that were consistent with those consumed before the obesity epidemic occurred. However, the vegetable oil group was consuming a level of LA that far surpassed all known preagricultural estimates of LA consumption.

This wasn't the only RCT that substituted vegetable oils for animal fat to measure body weight over time. According to the secondary endpoint analysis done by Hooper et al. 2020 with the Cochrane Collaboration, Olso Diet-Heart saw a 2.5kg reduction in body weight during their study period, whereas the Medical Research Council saw no change in body weight as well [[1](https://pubmed.ncbi.nlm.nih.gov/2607071/)00-[1](https://pubmed.ncbi.nlm.nih.gov/4175085/)01].

Altogether, it does not appear as though vegetable oils uniquely increase body weight in humans. So, while vegetable oils may increase 2-AG and induce obesity in mice, this does not appear to pan out in humans. Large scale RCTs in humans do not support the hypothesis that vegetable oils lead to weight gain over time in humans.

###### **THERMOGENESIS**

The effect of varying PUFA and SFA in the diet on measures of energy expenditure (EE) have been tested numerous times and show unambiguously consistent results [[1](https://pubmed.ncbi.nlm.nih.gov/24363161/)02]. Overall, diets higher in PUFA and lower in SFA tend to increase EE in humans [[1](https://pubmed.ncbi.nlm.nih.gov/1556946/)03-[1](https://pubmed.ncbi.nlm.nih.gov/9467221/)04].

Here is an example from that body of literature. In this study by Lichtenbelt et al. (1997), we can clearly see a consistent effect across all six subjects with higher PUFA intakes increasing postprandial EE. The same trend was also observed for resting metabolic rate (RMR). Ultimately, high-SFA, low-PUFA diets tend to lower EE and RMR compared to high-PUFA, low-SFA diets. This finding is incredibly consistent, though PUFA and MUFA seem to trade blows in some studies, the overall trend of SFA being least thermogenic is clear.

The effects of high-PUFA feeding on fat oxidation have also been studied by Casas-Agustench et al. (2009) as well [[1](https://pubmed.ncbi.nlm.nih.gov/19010571/)05]. When using the respiratory quotient to compare the effects of diets that are high in PUFA, MUFA, and SFA on EE and fat oxidation, we see a similar trend emerge once again.

High-PUFA feeding resulted in higher postprandial EE as well as a higher thermic effect of feeding (TEF). Though the differences in the rate of fat oxidation did not reach significance between groups, there was an obvious trend that reflected the degree of fatty acid saturation. PUFA was the most thermogenic, SFA was the least thermogenic, and MUFA was somewhere in between.

The same results were observed by DeLany et al. (2000), only this time different dietary fats containing labeled carbon isotopes are used [[1](https://pubmed.ncbi.nlm.nih.gov/11010930/)06]. You can measure these isotopes in the breath in order to measure how much of the dietary fat a subject has consumed was burned in the time after a meal.

Using this methodology, we see that there is one type of SFA that is preferentially oxidized over all other FAs that were tested, and that is lauric acid. However, lauric acid might have ketogenic properties, so interpret with caution. Looking over the rest of the tested FAs, we see that PUFA once again has the highest oxidation rate, followed by MUFA, and SFA once again comes in last place.

Vegetable oils don't appear to make you fat. But even if they did, it does not appear that their effects on thermogenesis and EE are likely to be mediating factors. In fact, vegetable oils appear as though they could potentially have the opposite effect.

###### **TYPE 2 DIABETES MELLITUS**

The idea that vegetable oils are the primary driver of obesity and/or type 2 diabetes mellitus (T2DM) has deep roots in many diet communities. However, the evidence cited to support this assertion is typically animal research investigating the effects of LA on hypothalamic function and energy intake. Not to mention that, as we discussed earlier, altering the fatty acid saturation of the diet has no discernable effect on ad libitum energy intake. So, let's investigate the effects on insulin sensitivity.

When the relationship between tissue LA and insulin sensitivity was investigated by Iggman et al. (2010), the results are null for leaner individuals. However for overweight individuals, adipose LA is associated with a statistically significant increase in insulin sensitivity [[1](https://pubmed.ncbi.nlm.nih.gov/20127308/)07].

This study into T2DM incidence is the closest thing I've been able to find investigating the association between adipose tissue fatty acids and insulin sensitivity that also adjusts for the fewest mediators of T2DM, such as energy intake.

Again, we can turn our attention to well-conducted crossover RCTs for clearer answers to these sorts of questions. Fortunately, we have one such trial by Heine et al. (1989), investigating the effects of altering fatty acid saturation on measures of insulin sensitivity and glucose homeostasis [[1](https://pubmed.ncbi.nlm.nih.gov/2923077/)08]. Subjects were placed on either a high-LA diet (10.9% of energy) or a low-LA diet (4.2% of energy) for 30 weeks, after which measures of plasma insulin and glucose clearance were taken.

While there were no statistically significant differences between groups in most ways, the high-LA group has a significantly higher glucose clearance rate during the first infusion test. Overall, there was an obvious trend toward a benefit with the high-LA diet.

Another trial by Zibaeenezhad et al. (2016) compared 15g of walnut oil (containing approximately 8g of LA) to no intervention, and measured a number of endpoints relevant to T2DM [[1](https://pubmed.ncbi.nlm.nih.gov/28115966/)09]. Presumably, randomization balanced baseline LA between groups, so we can assume that the trial is effectively testing the effects of adding a tablespoon of walnut oil to the diet.

By the end of the trial, the walnut oil diet improved both HbA1C and fasting blood glucose. There were also no statistically significant changes in body weight or BMI. Which suggests that these effects were independent of weight loss. This is not what we'd expect if the vegetable oils were increasing the risk of developing T2DM.

We also have at least one study by Pertiwi et al. (2020) investigating the relationship between LA and both glycemic control and liver function [[1](https://pubmed.ncbi.nlm.nih.gov/32546275/)10]. LA seems to have no association with glycemic control, and is associated with better liver function in the minimally adjusted model. The association is then null in model 2, which adjusts for a few T2D mediators. Lastly, after a better adjustment for diet quality, many of the associations gain significance again.

There are a few more studies that do not adjust for many T2D mediators. In this study by Kröger et al. (2011), the relationship between red blood cell LA and T2D incidence in the EPIC-Potsdam cohort was investigated [[1](https://pubmed.ncbi.nlm.nih.gov/20980488/)11]. We see that the minimally adjusted model (which did not adjust for any T2D mediators) shows a statistically significant reduction in the risk of T2D. Whereas in the fully adjusted model the association is null, as we would expect. However, the trend toward a benefit of LA reached statistical significance.

Ultimately, it appears that neither LA biomarkers nor intake associate with negative outcomes with regards to insulin sensitivity, obesity, T2D, or liver function. These results are pretty consistent with existing meta-analyses on this question (which I combed through to find minimally adjusted data) [[112](https://pubmed.ncbi.nlm.nih.gov/29032079/)].

These are not at all the results that we would expect to see if merely having more LA in either your body or your diet increased the risk of T2DM. Although, there are some who would claim that associations between LA biomarkers and diseases are unreliable.

Essentially, the idea is that LA can oxidize for various reasons and potentially skew the biomarker in different directions. For example, if higher tissue representation of LA is inversely associated with a disease, but the disease itself predisposes LA to oxidation (and thus creates the potential for the LA biomarker to appear artificially lower), the data could be confounded. However, there is fascinating data that would seem to contradict this [[113](https://pubmed.ncbi.nlm.nih.gov/30987358/)].

In analysis of a prospective cohort study by Yepes-Calderón et al. (2019), levels of the LA peroxidation products, malondialdehyde (MDA), was strongly and inversely associated with new onset T2DM after kidney transplant. Even after applying seven different multivariate adjustment models, no adjustment succeeded in nullifying the effect.

Perhaps the reason LA is inversely associated with T2DM is _because_ it undergoes peroxidation. Perhaps MDA is a signaling molecule that has some protective benefit for T2DM in the long run, despite lipid peroxidation sounding very bad and spooky. Who knows. I don't feel particularly bad for speculating. Many of my opponents in this debate often base entire positions almost entirely on speculation, and refer to much weaker evidence to boot.

Lastly, in a multinational MR study of almost a million participants by Chen et al. (2017), there was an inverse association between elevated plasma LA levels and incidence of T2DM [[114](https://pubmed.ncbi.nlm.nih.gov/28032469/)].

Once again, we see the same thing that we saw in the MR studies investigating CVD. Genetically higher plasma LA is inversely associated with lower fasting blood glucose and T2DM incidence, whereas genetically higher plasma AA is positively associated with both higher fasting blood glucose and T2DM incidence. We also see in their subgroup analysis that the conversion of LA to AA through FADS is once again mediating.

When excluding gene variants affecting the function of FADS, the association between genetically elevated plasma LA and T2DM is null. However, when FADS gene variants are considered, the association is statistically significant. This once again suggests a causal relationship between the conversion of LA to AA and disease incidence. Strangely, long chain omega-3s are positively associated as well, which is in contrast to the RCT data on the subject [[115](https://pubmed.ncbi.nlm.nih.gov/31434641/)].

Overall it would appear that higher intakes of LA are inversely associated with T2DM. In conclusion, LA does not appear to negatively affect body weight, ad libitum intake, EE, insulin sensitivity, or T2DM rates in humans. The current evidence does not support lowering vegetable oil intake for the purposes of preventing T2DM, and the evidence may even suggest that vegetable oils may play a role in T2DM prevention.

##### **FATTY LIVER DISEASES**

###### **FAT OVERLOAD SYNDROME**

Increased intakes of LA have been proposed by Santoro et al. (2013) as a mechanism by which non-alcoholic fatty liver disease (NALFD) may occur [[116](https://pubmed.ncbi.nlm.nih.gov/26405460/)]. The most direct evidence cited in support of this hypothesis comes from parenteral feeding studies. Typically these studies divulge that soybean oil (SO) based intravenous lipid emulsions (IVLE) have a tendency to produce a particular liver pathology called cholestasis in children on total parenteral nutrition (TPN) under certain conditions [[117](https://pubmed.ncbi.nlm.nih.gov/23520135/)]. On the other hand, Gura and Puder (2010) discovered that fish oil (FO) based IVLEs tend to prevent and/or abolish cholestasis during parenteral feeding [[118](https://pubmed.ncbi.nlm.nih.gov/20702846/)].

However, within this body of literature, the primary hypothesis that has been put forth to explain the cholestasis observed with SO-IVLEs implicates phytosterols as the primary driver [[119](https://pubmed.ncbi.nlm.nih.gov/26374182/)-[120](https://pubmed.ncbi.nlm.nih.gov/9437703/)]. Overall the evidence does not seem to strongly support LA as uniquely pathological in this regard, and most authors seem to reject this hypothesis.

Because it would be unethical to truly test the effects of IVLE overfeeding in humans, we have to rely on animal models to provide us with insights. In one well-designed mouse study by Ksami et al. (2013), both SO- and FO-IVLEs were fed to mice with intestinal injuries [[121](https://pubmed.ncbi.nlm.nih.gov/24107776/)]. Except there were additional groups that were fed FO-IVLEs that also contained SO-derived phytosterols. Markers of liver pathology were comparable between phytosterol-containing FO-IVLEs and controls fed SO-IVLEs.

When tested in humans at matched, eucaloric dosages, there are no clinically meaningful differences between SO-IVLEs and FO-IVLEs [[122](https://pubmed.ncbi.nlm.nih.gov/23770843/)-[123](https://pubmed.ncbi.nlm.nih.gov/22796064/)]. Across all of the markers investigated by Nehra et al. (2014), the only significant changes were increases in alkaline phosphatase, but they occurred in both groups.

Since FO-IVLEs are extremely low in LA, it is unlikely that LA is the mediator of this change. Despite the fact that dosages were titrated down to tolerable levels, such to be compliant with current standards of care for IVLEs, tissue LA increased significantly in subjects receiving SO-IVLEs.

To recap, the fatty liver observed in children receiving IVLEs while on TNP is likely a function of the direct intravenous administration of phytosterols, and it is unlikely that LA has any unique role to play here.

###### **NON-ALCOHOLIC FATTY LIVER DISEASE**

Outside of parenteral feeding, Van Name et al. (2020) has conducted a single-arm, non-randomized, unblinded trial involving 20 youths testing the effects of a low omega-6/omega-3 ratio (O6:O3) diet over 12 weeks [[124](https://pubmed.ncbi.nlm.nih.gov/32652034/)]. Aside from the obvious limitations of such a study, we'll give credit where it is due. The study overcame a significant hurdle that plagues literature of this sort. The subjects were kept largely weight stable during the 12 weeks, which is a success. However, despite this, they saw improvements in lipid peroxidation markers and insulin sensitivity with the low O6:O3 diet.

There is just a tiny little problem, though. They actually weren't testing the effects of a low-LA diet, specifically. They were testing the effects of modifying the ratio of total omega-6 to total omega-3. They specifically achieved the lower O6:O3 by adding sources of omega-3, rather than omitting sources of omega-6. Information about the specific dietary changes can be found in their supplement [[125](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7467848/bin/nxaa183_supplemental_files.zip)].

Ultimately, they likely did not significantly alter the LA content of the diet itself, as evidenced by the fact that tissue LA went largely unchanged throughout the duration of the trial.

Fortunately, we do have better, more direct experiments investigating the effects of dietary fatty acid composition and fatty acid saturation on measures of liver fat in humans [[126](https://pubmed.ncbi.nlm.nih.gov/22492369/)]. First is an isocaloric feeding study by Bjermo et al. (2012) that largely found non-inferiority between LA and SFA on measures of visceral adipose tissue (VAT). However, the high-LA diet resulted in a greater overall improvement in the subjects' metabolic profile.

The high-LA diet did result in lower waist circumference, PCSK9, cholesterol, serum insulin, and ALT, though it did not necessarily result in a difference in VAT compared to the high-SFA diet. The results are slightly different when looking at human overfeeding [[127](https://pubmed.ncbi.nlm.nih.gov/24550191/)]. In this trial by Rosqvist et al. (2014), which compared hypercaloric diets that were either high-LA or high-SFA, the high-LA diet did appear to be protective against VAT accumulation.

The high-LA diet seemed to be uniquely protective against liver fat accumulation in particular. Additionally, the high-LA diet seemed to be associated with a greater increase in lean body mass (LBM) when compared to the high-SFA diet. But not only that, the high-LA diet seemed more resistant to fat gain overall compared to the high-SFA diet. These are not the results we'd expect if LA was uniquely causal in NAFLD.

These human experimental finding are perfectly consistent with the observational evidence on the subject as well, which typically shows that either high-PUFA or high-LA diets protect against NAFLD at the population level as well [[128](https://pubmed.ncbi.nlm.nih.gov/22209679/)-[129](https://pubmed.ncbi.nlm.nih.gov/27618908/)].

Overall there is an inverse association between high-PUFA intake and measures of hepatic lipid concentrations. There is, however, an association between high-SFA diets, just like the experimental literature. There is also evidence from Wehmeyer et al. (2016) that suggests that the effect of total energy intake on NAFLD is greater than either high-PUFA or high-SFA diets [[130](https://pubmed.ncbi.nlm.nih.gov/27281105/)]. Regardless, there does not appear to be any clear or persuasive evidence that LA is uniquely causal of NAFLD.

##### **AUTOIMMUNE DISEASE**

###### **RHEUMATOID ARTHRITIS**

If you've spent any amount of time in any low carb or vegan diet community or blogosphere, you've undoubtedly heard someone claim that cutting out vegetable oils "cured" their rheumatoid arthritis (RA). It's a pervasive belief that these oils cause inflammatory pain, and may even be one of the primary causes of age-related inflammatory conditions, such as RA. Luckily, there is a boatload of literature on this. Even experimental literature. So, let's see what it says.

There is an analysis of the Epidemiological Investigation of Rheumatoid Arthritis cohort by Lourdudoss et al. (2018) that investigated the relationship between dietary fatty acids and RA pain [[131](https://pubmed.ncbi.nlm.nih.gov/28371257/)]. As per the recurring theme throughout much of the dietary fat literature, a high O6:O3 ratio is associated with an increased risk of both unacceptable pain and refractory pain (RR 1.70 [1.03-2.82] and 2.33 [1.28-4.24], respectively.

However, if we look at omega-3 we see that there is a statistically significant decrease in both unacceptable pain and refractory pain (RR 0.57 [0.35-0.95] and 0.47 [0.26-0.84], respectively). Whereas for omega-6, results for all three endpoints are null. If we do a little math (1/0.54 and 1/0.47), we see that the risk increase when going from the highest to lowest intakes of omega-3 for both unacceptable pain and refractory pain are pretty much the same as the risk ratios for the lowest to highest O6:O3 ratio. Which are 1.75 and 2.12, respectively.

When considered together, this likely means that the effect of a high O6:O3 ratio has more to do with insufficient omega-3 than it has to do with excessive omega-6, which is consistent with the experimental literature on the subject [[132](https://pubmed.ncbi.nlm.nih.gov/29017507/)-[133](https://pubmed.ncbi.nlm.nih.gov/28067815/)]. However, these results are not consistent across all measures of RA, such as bone marrow lesions in those without RA at baseline [[134](https://pubmed.ncbi.nlm.nih.gov/19426478/)].

In an analysis of this Australian cohort by Wang et al. (2009), neither omega-3, omega-6, nor the O6:O3 ratio had a statistically significant effect on the incidence of bone marrow lesions. However, intakes of saturated fat were associated with a statistically significant increase in risk in both multivariate models (RR 2.62 [1.11-6.17] and 2.56 [1.03-6.37], respectively).

As far as experimental evidence goes, the effects of all sorts of high-LA oils on RA-related endpoints have been investigated over the years. First we have 3-10.5g/day of blackcurrant seed oil (BCSO) [[135](https://pubmed.ncbi.nlm.nih.gov/8081671/)-[136](https://pubmed.ncbi.nlm.nih.gov/1397534/)]. BCSO has an LA content in excess of 40% by weight [[137](https://pubmed.ncbi.nlm.nih.gov/23341215/)]. Though most of these findings were null, there is one trial by Watson et al. (1993) that did have interesting results [[138](https://pubmed.ncbi.nlm.nih.gov/8252313/)].

Patients with RA were divided into two groups that received either 3g of BCSO or 3g of SO, and were compared to two healthy control groups consuming the same oils at the same dosages. The BCSO group, including both healthy subjects as well as those with RA saw a statistically significant decrease in tumor necrosis factor (TNF), whereas the SU group didn't.

What's interesting about this study isn't really the BCSO group's results, if you're someone who thinks that vegetable oils increase the risk of RA. The group receiving SU saw no significant changes in their inflammatory markers in neither the RA patients nor the healthy controls, which is not what we'd expect if LA exposure is driving RA.

Next up is evening primrose oil (EPO), which contains approximately 72% LA by weight [[139](https://pubmed.ncbi.nlm.nih.gov/30110920/)]. This oil has been tested at least twice in RA patients and consistently shows promising results for multiple endpoints. In a trial by Brzeski et al. (1991) using 6/day of EPO versus 6g/day of OO, there was a statistically significant reduction in morning stiffness and a non-significant reduction in articular index (a measure of flexibility) in the EPO group [[140](https://pubmed.ncbi.nlm.nih.gov/1913008/)]. However, the OO group saw statistically significant reductions in pain and articular index, but not morning stiffness. Suggesting potential complementary effects of both of these oils.

However, EPO trials using this design have been criticized by Horrobin (1989) for having a number of potential methodological errors [[141](https://pubmed.ncbi.nlm.nih.gov/2688567/)]. In another trial by Belch et al. (1988) using more robust methodology and more complex comparisons, EPO seems to do quite well in moderate doses [[142](https://pubmed.ncbi.nlm.nih.gov/2833184/)]. In this trial, 49 subjects with RA, managed with nonsteroidal anti-inflammatory drugs (NSAID), were randomized to three groups. One group had 12g/day of EPO, another group received 12g/day of EPO+FO, and the final group was placed on a placebo.

While EPO alone resulted in significant reduction in the use of NSAIDs, the largest reductions in NSAID usage was seen when EPO was paired with FO. Not only does this detract from the notion that LA increases RA, this also lends further credibility to the beneficial effects of increasing dietary omega-3.

However, the double blind RCT conducted by Volker et al. (2000) would seem to contradict this finding [[143](https://pubmed.ncbi.nlm.nih.gov/11036827/)]. In this trial, it is claimed that FO may improve symptoms as long as the background diet is low in omega-6. However, it is unclear if these results are actually compared to a high omega-6 diet, or if all of the study subjects were on low omega-6 diets. These results seem odd, as there are plenty of studies using high omega-6 oils that show positive results [[144](https://pubmed.ncbi.nlm.nih.gov/28699499/)-[146](https://pubmed.ncbi.nlm.nih.gov/29705470/)].

Bear in mind that most of these trials discussed in this section were small in size and relatively short in duration, and are plagued by a number of challenges that are common in nutrient supplement studies. However, one thing that seems clear is that omega-6 is not associated with the development of RA, nor is it associated with worsening RA symptoms. In clinical trials, high omega-6 oils do not uniquely increase the prevalence of RA symptoms, and may even be useful for RA management.

Some would like to attribute the benefits of these oils to their gamma-linolenic acid (GLA) content. Which may be true, seeing as though pure SO didn't seem to do anything to inflammatory markers and serves as a decent control in this case. However, the primary sources of GLA in the diet are CAO, SO, and margarine. As such, attributing the anti-inflammatory effect of EPO to GLA seems tantamount to suggesting that the aforementioned commodity oils are also anti-inflammatory. In fact, some of them have less LA than EPO. Personally, I think it is likely that the effect is conferred via GLA, but I also don't think this evidence suggests that LA is uniquely inflammatory, either.

Seeing as though high-LA SO was not uniquely pro-inflammatory in healthy subjects, it seems more and more unlikely that LA itself confers an independent inflammatory effect. Rather, it seems likely that sources of GLA could be anti-inflammatory, and some of those sources are common commodity vegetable oils that are supposedly making everyone sick and inflamed. See the issue?

###### **SYSTEMIC LUPUS ERYTHEMATOSUS**

Very little is understood about how dietary inputs affect systemic lupus erythematosus (SLE). Especially regarding exposures as esoteric as LA, PUFA, or vegetable oils. However, there is some data on the subject, and it's a mess. The studies are mostly cross-sectional or case-control in design and largely show contradictory findings that are difficult to reconcile.

The first study, by Gorczyca et al. (2021), is cross-sectional and exploratory in design, and found a significant correlation between serum LA and anti-nuclear antibody (ANA) [[147](https://pubmed.ncbi.nlm.nih.gov/34169789/)].

While ANA is an important part of the diagnostic criteria for SLE, I would take this correlation with a grain of salt. There was no assessment of baseline diet, and subjects were not followed prospectively and thus the study lacks the temporal component necessary to inform a causal inference. This is something the authors acknowledge.

_“Another drawback is the assessment of PUFA profiles at a single time-point. A prospective analysis in this type of study would be more informative. Our findings suggest the need for a profound metabolomic analysis.”_

Not only that, but there was no multivariable adjustment to ascertain whether or not high serum LA was merely a correlate for other dietary habits that actually do increase risk. It's impossible to tell from this data alone.

Puzzlingly, in stark contrast to these findings, other cross-sectional data seems to suggest that LA has either a neutral or beneficial effect on SLE, at least once people are diagnosed [[148](https://pubmed.ncbi.nlm.nih.gov/31074595/)-[149](https://pubmed.ncbi.nlm.nih.gov/26848399/)]. In the first paper by Charoenwoodhipong et al. (2020), it was observed that LA had no association with worsening SLE-related symptoms.

As you can see in Figure 3, there is no consistent relationship between LA and SLE-related symptoms in either direction. However, a consistent trend toward favourable outcomes was observed with higher omega-3, and the inverse of which was observed with a high O6:O3. This suggests that while a high O6:O3 is a correlate for worse outcomes, it does not appear to be a function of LA, but rather a function of insufficient omega-3. This is a consistent trend in the literature so far.

There is also evidence from Vordenbäumen et al. (2020) suggesting that among SLE patients, LA is associated with higher C-reactive protein (CRP), which is a marker of systemic inflammation [[150](https://pubmed.ncbi.nlm.nih.gov/32188303/)].

However, this correlation may be spurious considering that people diagnosed with SLE who maintain higher CRP are more likely to have poor dietary habits overall [[151](https://pubmed.ncbi.nlm.nih.gov/30282476/)]. Without multivariable adjustment to help control for some of these factors, it is again unclear if LA is merely acting as a correlate for a poor diet or if omega-3 is merely acting as a correlate for a good diet.

Again, at the risk of sounding like a broken record, the inverse of this has been suggested in additional research by Lourdudoss et al. (2016) investigating the relationship between LA and the risk of increasing glucocorticoids [[152](https://pubmed.ncbi.nlm.nih.gov/26848399/)].

This is particularly odd since LA was associated with systemic inflammation in the previous study. Like I said in the beginning, the data is a mess. Nothing seems congruent, because most of it is either cross-sectional or completely uncontrolled or unadjusted. However, there are a few prospective studies investigating SLE and vegetable oil intake.

First up is a study by Shin et al. (2017) involving 82 subjects which aimed to explore the differences in plasma fatty acids between those with SLE and those without [[153](https://pubmed.ncbi.nlm.nih.gov/30830319/)]. Ultimately, they discovered that plasma SFAs are more likely to be elevated in those with SLE, whereas plasma PUFA— particularly LA— was more likely to be reduced.

At first glance, this may seem like a win for PUFA, or LA more specifically, but there are some interpretive challenges here. For example, PUFA concentrations in tissue could plausibly reduce in the presence of lipid peroxidation cascades, and there is evidence from Shah et al. (2014) that lipid peroxidation metabolites are higher in subjects with SLE [[154](https://pubmed.ncbi.nlm.nih.gov/24636579/)].

However, the authors remarked that antioxidant status plays an important role in the severity of lipid peroxidation when it does occur. It has been shown that antioxidant supplementation significantly reduces markers of lipid peroxidation in subjects with SLE [[155](https://pubmed.ncbi.nlm.nih.gov/17143589/)-[156](https://pubmed.ncbi.nlm.nih.gov/17143589/)]. It has also been shown by Bae et al. (2002) that antioxidant status can be impaired in subjects with SLE [[157](https://pubmed.ncbi.nlm.nih.gov/12426662/)].

The second prospective study, analyzed by Minami et al. (2003), is quite a bit larger than the previous studies, with a decent follow-up time [[158](https://pubmed.ncbi.nlm.nih.gov/12672194/)]. Basically, the relationship between dietary intakes and active SLE incidence were investigated over four years in 279 female participants with diagnosed SLE. Overall, there was no association between SLE activity and vegetable fat intake.

However, echoing the sentiments toward antioxidants that have been discussed in some of the wider SLE literature, there was a statistically significant protective effect of vitamin C in reducing SLE activity. There were also non-significant risk reductions with fibre and vitamin A (but not retinol). To hammer this point home, Western dietary patterns are generally considered to be high in LA, but neither Western dietary patterns nor healthier dietary patterns associate either positively or negatively with SLE [[159](https://pubmed.ncbi.nlm.nih.gov/32936999/)-[160](https://pubmed.ncbi.nlm.nih.gov/31718449/)].

Much to my surprise, the relationship between both SLE and RA have been investigated using MR. In this MR study by Zhao and Schooling (2019), three different statistical tests across two gene variant subgroups were performed [[161](https://pubmed.ncbi.nlm.nih.gov/30409829/)].

Generally speaking, statistically significant inverse associations between genetically elevated plasma LA and both RA and SLE were consistently found. The only inconsistency was when the Egger MR or the MR PRESSO methods were applied. The application of the Egger MR method seemed to nullify the effect in all cases, and the application of the MR PRESSO method only seemed to nullify the effect for RA in the second subgroup analysis.

Overall, the epidemiological data seems suggestive that antioxidant status likely matters more for SLE than PUFA or LA intakes, or even Western diets for that matter. However, the most robust evidence we have on the subject is the MR data, which suggests that genetically elevated plasma LA may even lower the risk of both SLE and RA.

##### **DEGENERATIVE DISEASES**

###### **AGE-RELATED MACULAR DEGENERATION**

Knobbe and Stojanoska (2017) have suggested that "harmful" vegetable oils such as oils derived from soybeans, corn, cottonseed, and rapeseed, are the primary culprits for age-related macular degeneration (AMD) [[162](https://pubmed.ncbi.nlm.nih.gov/29150284/)]. However, the data presented is purely ecological, and not very robust. One of the authors, Chris Knobbe, true to his name has published an entire book that details the pathophysiology of AMD, and cites a number of epidemiological studies to support his inferences [[163](https://www.amazon.ca/Ancestral-Dietary-Strategy-Prevent-Degeneration/dp/0578579545)].

When I checked his supporting references, I noticed that a number of prospective cohort studies were missing, and had seemed to be replaced with a hodgepodge of case-control studies. This was odd, because a number of cohort studies don't show the same results as the ones he cited. There are many prospective cohort studies (which are stronger evidence than case-control studies) that investigate the relationship between vegetable oils and AMD.

So, as I do, I decided to gather all the data I could and compile a meta-analysis.

**Inclusion Criteria:**

- Prospective cohort studies investigating the relationship between either vegetable oils, PUFA, LA or TFA and early- or late-AMD.
    

**Results:**

Altogether, 10 prospective cohort studies were included in the analysis. With respect to the exposures specified in the inclusion criteria, Delcourt et al. (2007) reported only on PUFA [[164](https://pubmed.ncbi.nlm.nih.gov/17299457/)]. Whereas Seddon et al. (2003) investigated both PUFA and vegetable fat [[165](https://pubmed.ncbi.nlm.nih.gov/14662593/)]. Chong et al. (2009), Chua et al. (2006), Cho et al. (2000), and Tan et al. (2009) all explored LA, PUFA, and TFA in their analyses [[166](https://pubmed.ncbi.nlm.nih.gov/19433719/)-[169](https://pubmed.ncbi.nlm.nih.gov/19433717/)]. Parekh et al. (2009) additionally included vegetable fat in their analysis, but did not investigate TFA [[170](https://pubmed.ncbi.nlm.nih.gov/19901214/)]. Similarly, Sasaki et al. (2020) reported on LA and PUFA, but did not report on TFA or vegetable fat [[171](https://pubmed.ncbi.nlm.nih.gov/32181798/)]. Both Christen et al. (2011) and Mares-Perlman et al. (1995) studied associations pertaining to both LA and PUFA [[172](https://pubmed.ncbi.nlm.nih.gov/21402976/)-[173](https://pubmed.ncbi.nlm.nih.gov/7786215/)]. There was significant heterogeneity across all investigated exposures, and no single exposure reached statistical significance.

Among the studies with the longest follow-up time, largest cohort size, best adjustment models, and the widest exposure contrasts, the results tended to be null. For example, Chong et al. (2009) adjusted for lutein, zeaxanthin, and sources of omega-3, which are inversely associated with late AMD [[174](https://pubmed.ncbi.nlm.nih.gov/21899805/)-[175](https://pubmed.ncbi.nlm.nih.gov/18541848/)]. Their results were null for every exposure.

The strongest study of all was Christen et al. (2011). Their analysis included three different adjustment models that help us better ascertain the relationship between AMD and vegetable oils. For example, their analysis showed that LA was associated with AMD only before adjustment for AMD risk factors, and that the association was likely a function of insufficient omega-3.

_“Women in the highest tertile of LA intake, relative to the lowest, had an age- and treatment-adjusted RR of 1.41 (95% CI, 1.03-1.94; P for trend=.03). However, the RR was attenuated and no longer significant after additional adjustment for AMD risk factors and other fats. The ratio of -6 to -3 fatty acids was directly associated with the risk of AMD, and the association was strengthened when the denominator term for-3 fatty acids included only DHA and EPA (Table 2).”_

Essentially, vegetable oils may be acting as a correlate for poor diet quality overall. The vegetable oils themselves are not independently increasing risk, but they tend to associate with dietary patterns that lack characteristics that decrease risk. Diets that are high in vegetable oils tend to be low in carotenoids and omega-3, for example. After adjusting for those confounders, the association vanishes.

There is also MR data from Wang et al. (2021) investigating the relationship between genetically elevated LA biomarkers and AMD [[176](https://pubmed.ncbi.nlm.nih.gov/33982092/)]. In this multinational MR study of 8631 participants they applied three different statistical tests across two subgroups investigating different combinations of gene variants.

In the aggregate, genetically increased plasma LA was consistently associated with a reduced risk of AMD, regardless of the test or gene variant subgrouping. Interestingly enough, tissue AA was actually positively associated with AMD, like we saw earlier with the previously mentioned MR research.

Altogether, prospective cohort studies investigating the relationship between LA and AMD show a null association. However, more robust evidence from MR suggests that higher plasma LA might actually protect against AMD in the long run. The current evidence does not support lowering vegetable oil intake for AMD prophylaxis, and may actually suggest that increasing vegetable oil intake could have benefits.

###### **COGNITIVE DECLINE**

I almost forgot to investigate the relationship between vegetable oils and cognitive disorders like dementia and Alzheimer's disease. It must be all of these vegetable oils in my diet that are clouding my memory. Fortunately, after eating two coconuts and half a beehive, I'm as sharp as a tack and ready to dive into it.

I'm not sure what sort of mechanistic speculation buttresses the narrative that vegetable oils make one demented, but seeing as though this is a popular claim that is made by many vegetable oil skeptics, the question seemed worthwhile to investigate nonetheless. So let's dive in.

**Inclusion Criteria:**

- Prospective cohort studies or RCTs investigating the relationship between vegetable oils, PUFA, or LA and either cognitive decline and/or dementia and/or Alzheimer's disease.
    
- Exposure can be measured either by biomarkers or by assessment of dietary intake.
    
- In the case of cognitive decline, composite endpoints such as global cognition scores, rather than verbal memory scores in particular, will be favoured.
    

**Exclusion Criteria:**

- Trials that compare PUFA to other forms of PUFA (such as omega-3 supplements versus a high-LA placebo) will be excluded.
    

**Results:**

Altogether 43 studies were obtained from a PubMed and Google Scholar literature search. All but 15 prospective cohort studies were excluded due to either having irrelevant endpoints or insufficient specificity with regards to vegetable oil related exposures. No RCTs were found. Of the 15 studies, four were included in an analysis on Alzheimer's disease [[177](https://pubmed.ncbi.nlm.nih.gov/16710090/)-[180](https://pubmed.ncbi.nlm.nih.gov/22713770/)]. Whereas the remainder were included in an analysis of general dementia, which included global cognitive decline. The majority of the included studies reported on both dietary LA and dietary PUFA [[181](https://pubmed.ncbi.nlm.nih.gov/17413112/)-[185](https://pubmed.ncbi.nlm.nih.gov/33386799/)]. However, both Heude et al. (2003) and Samieri et al. (2008) used biomarker LA and PUFA in their analysis, rather than dietary intake [[186](https://pubmed.ncbi.nlm.nih.gov/12663275/)-[187](https://pubmed.ncbi.nlm.nih.gov/18779288/)]. Dietary PUFA, rather than dietary LA or vegetable fat, was investigated by Okereke et al. (2012), Roberts et al. (2012), and Solfrizzi et al. (2006) [[188](https://pubmed.ncbi.nlm.nih.gov/22605573/)-[190](https://pubmed.ncbi.nlm.nih.gov/22810099/)]. Lastly, only Laitinen et al. (2006) explored the association between vegetable fat and cognitive decline [[191](https://pubmed.ncbi.nlm.nih.gov/16710090/)].

**Alzheimer's Disease:**

For Alzheimer's disease, results across all exposures were null. However, there were not many studies per subgroup. When the subgrouping is removed the results still don't reach statistical significance (RR 0.74 [0.43-1.26], P=0.26). There is also decently high heterogeneity between the included studies. For example, Rönnemaa et al. (2012), saw a non-significant increase in Alzheimer's risk with increasing LA intake, but Morris saw a statistically significant decrease in Alzheimer's risk with increasing vegetable oil intake.

The adjustment model used by Rönnemaa et al. (2012) includes no adjustments for dietary covariates. As a consequence, it would be dubious to infer that we're observing an independent effect of LA, as LA might simply be a correlate for a different, undisclosed dietary exposure that is actually increasing risk. It is unknown. However, Morris et al. (2003) found an opposite, statistically significant effect of vegetable fats and did include some adjustments for diet quality.

Included in the fully adjusted model were adjustments for other dietary fatty acids. Among these findings, only two were statistically significant. The first is a statistically significant decrease in the risk of Alzheimer's disease with vegetable fats after an adjustment for other fatty acids. The other is a statistically significant increase in the risk of Alzheimer's disease with TFA, after an adjustment for other fatty acids.

Given that the two datasets that were investigated in these two studies were primarily including data that was collected during the 90s, before the TFA ban, it's possible that the increase in Alzheimer's risk with LA in Rönnemaa et al. (2012) is a consequence of confounding from TFA in many vegetable oil products.

**Dementia:**

Again, we see null findings across all of the investigated exposures. The only statistically significant study, Beydoun et al. (2007), found a 23% reduction in the risk of cognitive decline (RR 0.77 [0.65-0.91], P=0.002). What sets this study apart from most of the others was its robust multivariate adjustment model. In fact, the adjustment model was so comprehensive, it was given its own section in the paper.

In fairness, Okereke et al. (2012), found a non-significant increase in cognitive decline with increasing PUFA intake, and also had a reasonably good multivariate adjustment model. The multivariate adjustment model included TFA as a covariate, and even considered a number of micronutrients and comorbidities.

However, the cohort study had half as much follow-up time and used different methods for assessing cognitive function. Beydoun et al. (2007) used in-person interviewers to assess cognitive function, whereas Okereke et al. (2012) used telephone interviews to assess cognitive impairment. Though this might not matter very much [[192](https://pubmed.ncbi.nlm.nih.gov/29042492/)]. Regardless, their findings were null and the aggregated results of this meta-analysis are also null.

**Total Dementia:**

Results are slightly different when Alzheimer's disease, cognitive decline, and general dementia are all considered together, however. It is noteworthy to point out that when all of the studies are aggregated, vegetable fat associates with a statistically significant 58% reduction in total dementia (RR 0.42 [0.21-0.84], P=0.01). Though, to be clear, more than two thirds of the weight are derived from a single study, so interpret with caution.

In conclusion, it does not appear as though LA, PUFA, or vegetable fat are associated with an increased risk of Alzheimer's disease, cognitive decline, or dementia in humans. These results are consistent with previous meta-analyses by Cao et al. (2019) and Ruan et al. (2018) investigating these particular relationships [[193](https://pubmed.ncbi.nlm.nih.gov/31062836/)-[194](https://pubmed.ncbi.nlm.nih.gov/29701155/)]. However, when all endpoints are considered together, vegetable fat seems to be associated with a reduced risk of total dementia.

##### **MECHANISTIC AUTOFELLATIO**

###### **INFLAMMATION**

The notion that modern culinary oils containing high amounts of PUFA, particularly LA, are inflammatory is an idea that has managed to permeate virtually every diet camp imaginable. The paleo/ancestral nuts believe it. The vegan/plant-based nuts believe it. The keto/carnivore nuts believe it. Zealots belonging to Ray Peat's diet cult definitely believe it too. But is there actually evidence for this claim from human experiments?

I scoured the literature for as many LA substitution trials as I could find. Altogether I found 15 trials that used high-LA oils as either the intervention or the control, with CRP as a primary or secondary endpoint. From those 15 trials, I had to exclude ten of them.

**Inclusion Criteria:**

1. Design must be a RCT.
    
2. High-LA diet must have >20g/day of LA.
    
3. Both pre- and post-intervention CRP must be reported.
    

**Exclusion Criteria:**

1. High-LA diet must not be compared to other PUFA or other types of LA.
    
2. Low-LA diet must not have >10g/day of LA.
    

Four studies met the inclusion criteria and were included in the analysis [[195](https://pubmed.ncbi.nlm.nih.gov/11246548/)-[198](https://pubmed.ncbi.nlm.nih.gov/25319187/)]. The LA intakes in the high-LA diets ranged from 22.2g/day with Vafeiadou et al. (2015) to 50.8g/day with Junker et al. (2001), with an average of 33.5g/day, across all included studies. The LA intakes in the low-LA diets ranged from 4.8g/day with Iggman et al. (2014) to 7.8g/day with Junker et al. (2001), with an average of 6.4g/day, across all included studies. The average contrast in LA intake was 27g/day across all included studies.

There were 12 studies that were captured by the exclusion criteria and had to be excluded from the analysis.

**Results:**

Altogether, high-LA diets yielded a non-significant increase in CRP when compared to control (P=0.15). The results are ultimately null. However, it may still be possible that the true effect of LA on CRP is hidden. It's plausible that high-LA diets do increase CRP, but they don't tend to increase CRP much more than control.

When the effects of high-LA diets are compared to baseline, the results are almost squarely null (P=0.62). Even Junker, 2001, which saw the widest contrast in LA intake ultimately had a null result. But, we're not out of the woods yet. There are still other possibilities to consider. What if the subjects' usual diets are already high in LA that dosing more in an intervention trial doesn't actually do anything to CRP? Perhaps the comparator diets could still prove to be anti-inflammatory in some way.

Comparing the effects of low-LA diets to baseline yielded a non-significant decrease in CRP (P=0.21). Again, the results are ultimately null. So, at present it does not appear as though low-LA diets are terribly protective compared to high-LA diets on measures of CRP.

But there is one last thing to discuss. Junker et al. (2001), saw the widest contrast in LA intake and was also the only study to show a statistically significant increase in CRP when comparing a high-LA diet to a low-LA diet. However, this effect likely isn't accurate. Let me explain.

_“For all parameters, mean and standard deviation (SD) or median and range (if non-normally distributed) were calculated. Intragroup differences between week 4 and week 0 were compared using the Mann-Whitney-Wilcoxon test (non-normally distributed variables) or the t-test for unpaired samples (if normally distributed), whereas intergroup differences at week 0 and at week 4 were compared using the Mann-Whitney-U test (non-normally distributed variables) or the t-test (normally distributed variables).”_

As described in the Statistical Analyses section, the Mann-Whitney-Wilcoxon test was used to test for statistical significance when distributions were non-normal. This test is specifically designed for non-normal distributions, and applying this test is also best practice in this case.

Here we see that CRP is actually reported as median and range, because the distributions of CRP were non-normal. We can also see that all three groups saw non-significant decreases in median CRP from baseline, falling from 0.9 (0.17-5.9) to 0.75 (0.17-7.7). However, the upper bounds for CRP during the starting week of the trial are unusually high in the OO group. This means that estimating the mean and standard deviation will actually yank the variance and the mean much higher than it rightfully should be.

This gives the illusion that the drop in CRP in the OO group was much larger than it actually was, which in turn bloats the treatment effect seen when using estimated means and standard deviations [[199](https://pubmed.ncbi.nlm.nih.gov/15840177/)]. This also happened in the SU group. The range is much narrower at baseline than it was post-intervention, which pulled the variance and the mean toward showing an increase in CRP that didn't actually happen. In reality, the OO and SU groups saw a non-significant decrease in CRP.

In conclusion, both high-LA and low-LA diets appear to have no particularly noteworthy effect on CRP, even when LA intakes are extremely high. The results of this meta-analysis suggest that avoiding high-LA diets on the basis of inflammation appears to be unwarranted.

###### **LIPID PEROXIDATION**

An unbelievable number of diet gurus, quacks, and ne'er-do-wells believe that PUFA found in vegetable oils are toxic to humans, especially at higher doses. To these clowns, PUFA are responsible for everything bad that could ever happen to the human body— doesn't matter if it's obesity, diabetes, heart disease, or limp dick. If you got it, PUFA caused it.

"But why?", you might ask. Good question. The primary mechanism of action proposed by these lunatics is lipid peroxidation. PUFA are unique among fatty acids in that they have multiple double-bonds. The carbons between these double-bonds are uniquely vulnerable to oxidation. This essentially destroys the PUFA and creates a lot of byproducts— toxic byproducts to be fair.

It is not at all disputed that PUFA behaves in this fashion. We know they do. They're fragile and they break apart really easily in isolation when exposed to different sorts of reactants, like oxygen. That is an indisputable fact. However, indisputable mechanisms don't necessarily translate into indisputable clinical relevance.

To test this, I scoured the literature for any randomized controlled trials (RCT) that investigated the effects of different dietary fatty acids on measures of lipid peroxidation. The outcomes assessed were markers of lipid peroxidation, including 8-oxo-7,8-dihydro-20-deoxyguanosine (8-oxodG), glutathione reductase (GR), glutathione peroxidase (GPx), superoxide dismutase (SOD), conjugated dienes (CD), malondialdehyde (MDA), and catalase (CAT).

**Inclusion Criteria:**

1. RCT design.
    
2. At least one of the chosen endpoints must be reported.
    
3. Study design must be comparing high-PUFA to low-PUFA controls.
    
4. Low-PUFA controls must replace PUFA with monounsaturated or saturated fat.
    

Altogether, five studies were included in the analysis. The vast majority of data was collected from Freese et al. (2008), which included data for 8-oxodG, GR, GPx, and CAT [[200](https://pubmed.ncbi.nlm.nih.gov/17671440/)]. Whereas Jenkinson et al. (1999) additionally investigated SOD and LO2 in their investigation [[201](https://pubmed.ncbi.nlm.nih.gov/10452406/)]. Exploration of MDA was contributed by both de Kok et al. (2003) and Södergren et al. (2001) [[202](https://pubmed.ncbi.nlm.nih.gov/12504167/)-[203](https://pubmed.ncbi.nlm.nih.gov/11641740/)]. Lastly, Parfitt et al. (1994) contributed data for CD [[204](https://pubmed.ncbi.nlm.nih.gov/8181259/)].

**Results:**

Normally when I see a bunch of null results I feel like I may have wasted my time. However, these results make me giggle. The only significant finding was an effect of PUFA lowering 8-oxodG compared to baseline (P=0.02), however the results were null when compared to control (P=0.83). This means that both the high-PUFA and low-PUFA diets lowered this marker of lipid peroxidation.

**8-oxo-7,8-dihydro-20-deoxyguanosine (8-oxodG) vs Control**

**8-oxo-7,8-dihydro-20-deoxyguanosine (8-oxodG) vs Baseline**

**Glutathione Reductase (GR) vs Control**

**Glutathione Reductase (GR) vs Baseline**

**Glutathione Peroxidase (GPx) vs Control**

**Glutathione Peroxidase (GPx) vs Baseline**

**Superoxide Dismutase (SOD) vs Control**

**Superoxide Dismutase (SOD) vs Baseline**

**Malondialdehyde (MDA) vs Control**

**Malondialdehyde (MDA) vs Baseline**

**Catalase (CAT) vs Control**

**Catalase (CAT) vs Baseline**

**Conjugated Dienes (CD) vs Control**

**Conjugated Dienes (CD) vs Baseline**

The only statistically significant finding was a reduction in 8-oxodG with high-PUFA diets (P=0.02). Which is interesting, because 8-oxodG is a marker of DNA damage, and as such its implications extend far beyond oxidative stress. I thought PUFA was supposed to destroy your DNA with all of those highly reactive, toxic byproducts they create when they oxidize? Well, apparently, like most mechanistic reasoning, that shit just doesn't pan out in the real world.

In fact, even if we ignored the P-values and just looked at trends or directionality, the results would overwhelmingly favour high-PUFA diets over low-PUFA diets. Especially for MDA, which saw non-significant decreases for high-PUFA diets compared to baseline (P=0.11).

In conclusion, mechanistic speculation regarding the toxic, in vivo effects of PUFA on lipid peroxidation in humans doesn't pan out in real life. PUFA does not seem to negatively affect markers of lipid peroxidation, and may even improve some of them.

##### DISCUSSION

It may be true that vegetable oils are food items that represent a stark divergence from traditional dietary norms. However, it does not appear as though these oils are uniquely causal of the chronic diseases we face in the modern, industrialized world. In fact, vegetable oils likely have a beneficial role to play in the prevention of many diseases— particularly heart disease, type 2 diabetes mellitus, and potentially skin cancer. Vegetable oils could plausibly have a beneficial role to play in preventing degeneration of the macula, fatty liver disease, and perhaps some auto-immune diseases.

This in-depth analysis could find no persuasive evidence of harm with vegetable oils for any of the investigated endpoints. Vegetable oils seemed to have no consistent effect on the risk of breast cancer, colorectal cancer, obesity, or cognitive decline. Though there are many proposed mechanisms by which vegetable oils have been suggested to be harmful to humans, the actual human outcome data does not seem to support the importance of these mechanisms for these endpoints. If the outcomes consistently contradict our mechanistic speculations, the mechanisms are probably not interacting with those outcomes in the way we suspect.

In conclusion, vegetable oils appear to be a health-promoting addition to the diet, and seem to offer a range of health benefits and little to no apparent health risks to the general population. However, one should exercise caution when navigating the current food environment, as vegetable oils are included in many foods that are not particularly health-promoting. If one chooses to consume vegetable oils, it would probably be wise to integrate them into a healthy eating pattern in ways that do not promote the overconsumption of calories. Some possible healthy ways to include vegetable oils in the diet could be in the form of salad dressings or as cooking oils for sauteed vegetables.

##### BIBLIDDILYOODILYOGRAPHY

###### [REFERENCES LINK](https://docs.google.com/document/d/1sR4p0InZmKJ0_w5GH1-lfMFFhJSMyI-GZnkYwAgO_lY/edit?usp=sharing)

1. DiNicolantonio, James J., and James H. O’Keefe. “Omega-6 Vegetable Oils as a Driver of Coronary Heart Disease: The Oxidized Linoleic Acid Hypothesis.” Open Heart, vol. 5, no. 2, 2018, p. e000898. PubMed, [https://doi.org/10.1136/openhrt-2018-000898](https://doi.org/10.1136/openhrt-2018-000898).
    

  

2. Mata, P., et al. “Effect of Dietary Fat Saturation on LDL Oxidation and Monocyte Adhesion to Human Endothelial Cells in Vitro.” Arteriosclerosis, Thrombosis, and Vascular Biology, vol. 16, no. 11, Nov. 1996, pp. 1347–55. PubMed, [https://doi.org/10.1161/01.atv.16.11.1347](https://doi.org/10.1161/01.atv.16.11.1347).
    

  

3. Kratz, M., et al. “Effects of Dietary Fatty Acids on the Composition and Oxidizability of Low-Density Lipoprotein.” European Journal of Clinical Nutrition, vol. 56, no. 1, Jan. 2002, pp. 72–81. PubMed, [https://doi.org/10.1038/sj.ejcn.1601288](https://doi.org/10.1038/sj.ejcn.1601288).
    

  

4. Reaven, P. D., et al. “Effects of Linoleate-Enriched and Oleate-Enriched Diets in Combination with Alpha-Tocopherol on the Susceptibility of LDL and LDL Subfractions to Oxidative Modification in Humans.” Arteriosclerosis and Thrombosis: A Journal of Vascular Biology, vol. 14, no. 4, Apr. 1994, pp. 557–66. PubMed, [https://doi.org/10.1161/01.atv.14.4.557](https://doi.org/10.1161/01.atv.14.4.557).
    

  

5. Esterbauer, H., et al. “Role of Vitamin E in Preventing the Oxidation of Low-Density Lipoprotein.” The American Journal of Clinical Nutrition, vol. 53, no. 1 Suppl, Jan. 1991, pp. 314S-321S. PubMed, [https://doi.org/10.1093/ajcn/53.1.314S](https://doi.org/10.1093/ajcn/53.1.314S).
    

  

6. Esterbauer, H., et al. “Continuous Monitoring of in Vitro Oxidation of Human Low Density Lipoprotein.” Free Radical Research Communications, vol. 6, no. 1, 1989, pp. 67–75. PubMed, [https://doi.org/10.3109/10715768909073429](https://doi.org/10.3109/10715768909073429).
    

  

7. Raederstorff, Daniel, et al. “Vitamin E Function and Requirements in Relation to PUFA.” The British Journal of Nutrition, vol. 114, no. 8, Oct. 2015, pp. 1113–22. PubMed, [https://doi.org/10.1017/S000711451500272X](https://doi.org/10.1017/S000711451500272X).
    

  

8. Herting, D. C., and E. J. Drury. “VITAMIN E CONTENT OF VEGETABLE OILS AND FATS.” The Journal of Nutrition, vol. 81, no. 4, Dec. 1963, pp. 335–42. PubMed, [https://doi.org/10.1093/jn/81.4.335](https://doi.org/10.1093/jn/81.4.335).
    

  

9. Marrugat, Jaume, et al. “Effects of Differing Phenolic Content in Dietary Olive Oils on Lipids and LDL Oxidation--a Randomized Controlled Trial.” European Journal of Nutrition, vol. 43, no. 3, June 2004, pp. 140–47. PubMed, [https://doi.org/10.1007/s00394-004-0452-8](https://doi.org/10.1007/s00394-004-0452-8).
    

  

10. Hernáez, Álvaro, et al. “The Mediterranean Diet Decreases LDL Atherogenicity in High Cardiovascular Risk Individuals: A Randomized Controlled Trial.” Molecular Nutrition & Food Research, vol. 61, no. 9, Sept. 2017. PubMed, [https://doi.org/10.1002/mnfr.201601015](https://doi.org/10.1002/mnfr.201601015).
    

  

11. Fitó, Montserrat, et al. “Effect of a Traditional Mediterranean Diet on Lipoprotein Oxidation: A Randomized Controlled Trial.” Archives of Internal Medicine, vol. 167, no. 11, June 2007, pp. 1195–203. PubMed, [https://doi.org/10.1001/archinte.167.11.1195](https://doi.org/10.1001/archinte.167.11.1195).
    

  

12. Aronis, Pantelis, et al. “Effect of Fast-Food Mediterranean-Type Diet on Human Plasma Oxidation.” Journal of Medicinal Food, vol. 10, no. 3, Sept. 2007, pp. 511–20. PubMed, [https://doi.org/10.1089/jmf.2006.235](https://doi.org/10.1089/jmf.2006.235).
    

  

13. Kiokias, Sotirios, et al. “Effect of Natural Food Antioxidants against LDL and DNA Oxidative Changes.” Antioxidants (Basel, Switzerland), vol. 7, no. 10, Oct. 2018, p. E133. PubMed, [https://doi.org/10.3390/antiox7100133](https://doi.org/10.3390/antiox7100133).
    

  

14. Palomäki, Ari, et al. “Effects of Dietary Cold-Pressed Turnip Rapeseed Oil and Butter on Serum Lipids, Oxidized LDL and Arterial Elasticity in Men with Metabolic Syndrome.” Lipids in Health and Disease, vol. 9, Dec. 2010, p. 137. PubMed, [https://doi.org/10.1186/1476-511X-9-137](https://doi.org/10.1186/1476-511X-9-137).
    

  

15. Oörni, K., et al. “Oxidation of Low Density Lipoprotein Particles Decreases Their Ability to Bind to Human Aortic Proteoglycans. Dependence on Oxidative Modification of the Lysine Residues.” The Journal of Biological Chemistry, vol. 272, no. 34, Aug. 1997, pp. 21303–11. PubMed, [https://doi.org/10.1074/jbc.272.34.21303](https://doi.org/10.1074/jbc.272.34.21303).
    

  

16. Wu, Tianying, et al. “Is Plasma Oxidized Low-Density Lipoprotein, Measured with the Widely Used Antibody 4E6, an Independent Predictor of Coronary Heart Disease among U.S. Men and Women?” Journal of the American College of Cardiology, vol. 48, no. 5, Sept. 2006, pp. 973–79. PubMed, [https://doi.org/10.1016/j.jacc.2006.03.057](https://doi.org/10.1016/j.jacc.2006.03.057).
    

  

17. van Tits, Lambertus J., et al. “Letter Regarding Article by Tsimikas et al, ‘High-Dose Atorvastatin Reduces Total Plasma Levels of Oxidized Phospholipids and Immune Complexes Present on Apolipoprotein B-100 in Patients With Acute Coronary Syndromes in the MIRACL Trial.’” Circulation, vol. 111, no. 18, May 2005, pp. e284–85. ahajournals.org (Atypon), [https://doi.org/10.1161/01.CIR.0000164264.00913.6D](https://doi.org/10.1161/01.CIR.0000164264.00913.6D).
    

  

18. Mercodia Oxidized LDL ELISA monoclonal antibody 4E6: [https://www.mercodia.com/product/oxidized-ldl-elisa/#:~:text=Mercodia%20Oxidized%20LDL%20ELISA%20is,epitope%20in%20oxidized%20ApoB%2D100.&text=Substituting%20aldehydes%20can%20be%20produced,the%20generation%20of%20oxidized%20LDL](https://www.mercodia.com/product/oxidized-ldl-elisa/#:~:text=Mercodia%20Oxidized%20LDL%20ELISA%20is,epitope%20in%20oxidized%20ApoB%2D100.&text=Substituting%20aldehydes%20can%20be%20produced,the%20generation%20of%20oxidized%20LDL).
    

  

19. Annika Carlsson and Jeanette, “Oxidized LDL - Know what you measure.” 3rd Joint Meeting of the French, German and Swiss Atherosclerosis Societies, Saint Gervais les Bains, France January 31-February 2, 2008
    

[Nikushttps://cms.mercodia.com/wp-content/uploads/2019/06/poster-oxldl-know-what-you-measure.pdf](https://cms.mercodia.com/wp-content/uploads/2019/06/poster-oxldl-know-what-you-measure.pdf)

  

20. Henriksen, T., et al. “Enhanced Macrophage Degradation of Biologically Modified Low Density Lipoprotein.” Arteriosclerosis (Dallas, Tex.), vol. 3, no. 2, Apr. 1983, pp. 149–59. PubMed, [https://doi.org/10.1161/01.atv.3.2.149](https://doi.org/10.1161/01.atv.3.2.149).
    

  

21. Meyer, Jason M., et al. “Minimally Oxidized LDL Inhibits Macrophage Selective Cholesteryl Ester Uptake and Native LDL-Induced Foam Cell Formation.” Journal of Lipid Research, vol. 55, no. 8, Aug. 2014, pp. 1648–56. PubMed, [https://doi.org/10.1194/jlr.M044644](https://doi.org/10.1194/jlr.M044644).
    

  

22. Tsimikas, Sotirios, et al. “Oxidized Phospholipids Predict the Presence and Progression of Carotid and Femoral Atherosclerosis and Symptomatic Cardiovascular Disease: Five-Year Prospective Results from the Bruneck Study.” Journal of the American College of Cardiology, vol. 47, no. 11, June 2006, pp. 2219–28. PubMed, [https://doi.org/10.1016/j.jacc.2006.03.001](https://doi.org/10.1016/j.jacc.2006.03.001).
    

  

23. Marklund, Matti, et al. “Biomarkers of Dietary Omega-6 Fatty Acids and Incident Cardiovascular Disease and Mortality.” Circulation, vol. 139, no. 21, May 2019, pp. 2422–36. PubMed, [https://doi.org/10.1161/CIRCULATIONAHA.118.038908](https://doi.org/10.1161/CIRCULATIONAHA.118.038908).
    

  

24. Wood, D. A., et al. “Adipose Tissue and Platelet Fatty Acids and Coronary Heart Disease in Scottish Men.” Lancet (London, England), vol. 2, no. 8395, July 1984, pp. 117–21. PubMed, [https://doi.org/10.1016/s0140-6736(84)91044-4](https://doi.org/10.1016/s0140-6736(84)91044-4).
    

  

25. Siri-Tarino, Patty W., et al. “Meta-Analysis of Prospective Cohort Studies Evaluating the Association of Saturated Fat with Cardiovascular Disease.” The American Journal of Clinical Nutrition, vol. 91, no. 3, Mar. 2010, pp. 535–46. PubMed, [https://doi.org/10.3945/ajcn.2009.27725](https://doi.org/10.3945/ajcn.2009.27725).
    

  

26. Harcombe, Zoë, et al. “Evidence from Prospective Cohort Studies Does Not Support Current Dietary Fat Guidelines: A Systematic Review and Meta-Analysis.” British Journal of Sports Medicine, vol. 51, no. 24, Dec. 2017, pp. 1743–49. PubMed, [https://doi.org/10.1136/bjsports-2016-096550](https://doi.org/10.1136/bjsports-2016-096550).
    

  

27. Zhu, Yongjian, et al. “Dietary Total Fat, Fatty Acids Intake, and Risk of Cardiovascular Disease: A Dose-Response Meta-Analysis of Cohort Studies.” Lipids in Health and Disease, vol. 18, no. 1, Apr. 2019, p. 91. PubMed, [https://doi.org/10.1186/s12944-019-1035-2](https://doi.org/10.1186/s12944-019-1035-2).
    

  

28. Mazidi, Mohsen, et al. “Association of Types of Dietary Fats and All-Cause and Cause-Specific Mortality: A Prospective Cohort Study and Meta-Analysis of Prospective Studies with 1,164,029 Participants.” Clinical Nutrition (Edinburgh, Scotland), vol. 39, no. 12, Dec. 2020, pp. 3677–86. PubMed, [https://doi.org/10.1016/j.clnu.2020.03.028](https://doi.org/10.1016/j.clnu.2020.03.028).
    

  

29. Skeaff, C. Murray, and Jody Miller. “Dietary Fat and Coronary Heart Disease: Summary of Evidence from Prospective Cohort and Randomised Controlled Trials.” Annals of Nutrition & Metabolism, vol. 55, no. 1–3, 2009, pp. 173–201. PubMed, [https://doi.org/10.1159/000229002](https://doi.org/10.1159/000229002).
    

  

30. Henderson, Sean O., et al. “Established Risk Factors Account for Most of the Racial Differences in Cardiovascular Disease Mortality.” PloS One, vol. 2, no. 4, Apr. 2007, p. e377. PubMed, [https://doi.org/10.1371/journal.pone.0000377](https://doi.org/10.1371/journal.pone.0000377).
    

  

31. Yasuyuki, Nakamura, et al. “Fatty Acid Intakes and Coronary Heart Disease Mortality in Japan: NIPPON DATA90, 1990-2005.” Current Nutrition & Food Science, vol. 9, no. 1, Jan. 2013, pp. 26–32.
    

  

32. Zhuang, Pan, et al. “Dietary Fats in Relation to Total and Cause-Specific Mortality in a Prospective Cohort of 521 120 Individuals With 16 Years of Follow-Up.” Circulation Research, vol. 124, no. 5, Mar. 2019, pp. 757–68. PubMed, [https://doi.org/10.1161/CIRCRESAHA.118.314038](https://doi.org/10.1161/CIRCRESAHA.118.314038).
    

  

33. Goldbourt, U., et al. “Factors Predictive of Long-Term Coronary Heart Disease Mortality among 10,059 Male Israeli Civil Servants and Municipal Employees. A 23-Year Mortality Follow-up in the Israeli Ischemic Heart Disease Study.” Cardiology, vol. 82, no. 2–3, 1993, pp. 100–21. PubMed, [https://doi.org/10.1159/000175862](https://doi.org/10.1159/000175862).
    

  

34. Guasch-Ferré, Marta, et al. “Dietary Fat Intake and Risk of Cardiovascular Disease and All-Cause Mortality in a Population at High Risk of Cardiovascular Disease.” The American Journal of Clinical Nutrition, vol. 102, no. 6, Dec. 2015, pp. 1563–73. PubMed, [https://doi.org/10.3945/ajcn.115.116046](https://doi.org/10.3945/ajcn.115.116046).
    

  

35. Jakobsen, Marianne U., et al. “Dietary Fat and Risk of Coronary Heart Disease: Possible Effect Modification by Gender and Age.” American Journal of Epidemiology, vol. 160, no. 2, July 2004, pp. 141–49. PubMed, [https://doi.org/10.1093/aje/kwh193](https://doi.org/10.1093/aje/kwh193).
    

  

36. Zong, Geng, et al. “Intake of Individual Saturated Fatty Acids and Risk of Coronary Heart Disease in US Men and Women: Two Prospective Longitudinal Cohort Studies.” BMJ (Clinical Research Ed.), vol. 355, Nov. 2016, p. i5796. PubMed, [https://doi.org/10.1136/bmj.i5796](https://doi.org/10.1136/bmj.i5796).
    

  

37. Mann, J. I., et al. “Dietary Determinants of Ischaemic Heart Disease in Health Conscious Individuals.” Heart (British Cardiac Society), vol. 78, no. 5, Nov. 1997, pp. 450–55. PubMed, [https://doi.org/10.1136/hrt.78.5.450](https://doi.org/10.1136/hrt.78.5.450).
    

  

38. Ricci, Cristian, et al. “Type of Dietary Fat Intakes in Relation to All-Cause and Cause-Specific Mortality in US Adults: An Iso-Energetic Substitution Analysis from the American National Health and Nutrition Examination Survey Linked to the US Mortality Registry.” The British Journal of Nutrition, vol. 119, no. 4, Feb. 2018, pp. 456–63. PubMed, [https://doi.org/10.1017/S0007114517003889](https://doi.org/10.1017/S0007114517003889).
    

  

39. Posner, B. M., et al. “Dietary Lipid Predictors of Coronary Heart Disease in Men. The Framingham Study.” Archives of Internal Medicine, vol. 151, no. 6, June 1991, pp. 1181–87.
    

  

40. Praagman, Jaike, et al. “The Association between Dietary Saturated Fatty Acids and Ischemic Heart Disease Depends on the Type and Source of Fatty Acid in the European Prospective Investigation into Cancer and Nutrition-Netherlands Cohort.” The American Journal of Clinical Nutrition, vol. 103, no. 2, Feb. 2016, pp. 356–65. PubMed, [https://doi.org/10.3945/ajcn.115.122671](https://doi.org/10.3945/ajcn.115.122671).
    

  

41. Praagman, Jaike, et al. “Dietary Saturated Fatty Acids and Coronary Heart Disease Risk in a Dutch Middle-Aged and Elderly Population.” Arteriosclerosis, Thrombosis, and Vascular Biology, vol. 36, no. 9, Sept. 2016, pp. 2011–18. PubMed, [https://doi.org/10.1161/ATVBAHA.116.307578](https://doi.org/10.1161/ATVBAHA.116.307578).
    

  

42. Santiago, S., et al. “Fat Quality Index and Risk of Cardiovascular Disease in the Sun Project.” The Journal of Nutrition, Health & Aging, vol. 22, no. 4, 2018, pp. 526–33. PubMed, [https://doi.org/10.1007/s12603-018-1003-y](https://doi.org/10.1007/s12603-018-1003-y).
    

  

43. Schoenaker, D. a. J. M., et al. “Dietary Saturated Fat and Fibre and Risk of Cardiovascular Disease and All-Cause Mortality among Type 1 Diabetic Patients: The EURODIAB Prospective Complications Study.” Diabetologia, vol. 55, no. 8, Aug. 2012, pp. 2132–41. PubMed, [https://doi.org/10.1007/s00125-012-2550-0](https://doi.org/10.1007/s00125-012-2550-0).
    

  

44. Leosdottir, Margret, et al. “Cardiovascular Event Risk in Relation to Dietary Fat Intake in Middle-Aged Individuals: Data from The Malmö Diet and Cancer Study.” European Journal of Cardiovascular Prevention and Rehabilitation: Official Journal of the European Society of Cardiology, Working Groups on Epidemiology & Prevention and Cardiac Rehabilitation and Exercise Physiology, vol. 14, no. 5, Oct. 2007, pp. 701–06. PubMed, [https://doi.org/10.1097/HJR.0b013e3282a56c45](https://doi.org/10.1097/HJR.0b013e3282a56c45).
    

  

45. Pietinen, P., et al. “Intake of Fatty Acids and Risk of Coronary Heart Disease in a Cohort of Finnish Men. The Alpha-Tocopherol, Beta-Carotene Cancer Prevention Study.” American Journal of Epidemiology, vol. 145, no. 10, May 1997, pp. 876–87. PubMed, [https://doi.org/10.1093/oxfordjournals.aje.a009047](https://doi.org/10.1093/oxfordjournals.aje.a009047).
    

  

46. Nagata, Chisato, et al. “Total Fat Intake Is Associated with Decreased Mortality in Japanese Men but Not in Women.” The Journal of Nutrition, vol. 142, no. 9, Sept. 2012, pp. 1713–19. PubMed, [https://doi.org/10.3945/jn.112.161661](https://doi.org/10.3945/jn.112.161661).
    

  

47. Yamagishi, Kazumasa, et al. “Dietary Intake of Saturated Fatty Acids and Incident Stroke and Coronary Heart Disease in Japanese Communities: The JPHC Study.” European Heart Journal, vol. 34, no. 16, Apr. 2013, pp. 1225–32. PubMed, [https://doi.org/10.1093/eurheartj/eht043](https://doi.org/10.1093/eurheartj/eht043).
    

  

48. Yamagishi, Kazumasa, et al. “Dietary Intake of Saturated Fatty Acids and Mortality from Cardiovascular Disease in Japanese: The Japan Collaborative Cohort Study for Evaluation of Cancer Risk (JACC) Study.” The American Journal of Clinical Nutrition, vol. 92, no. 4, Oct. 2010, pp. 759–65. PubMed, [https://doi.org/10.3945/ajcn.2009.29146](https://doi.org/10.3945/ajcn.2009.29146).
    

  

49. Virtanen, Jyrki K., et al. “Dietary Fatty Acids and Risk of Coronary Heart Disease in Men: The Kuopio Ischemic Heart Disease Risk Factor Study.” Arteriosclerosis, Thrombosis, and Vascular Biology, vol. 34, no. 12, Dec. 2014, pp. 2679–87. PubMed, [https://doi.org/10.1161/ATVBAHA.114.304082](https://doi.org/10.1161/ATVBAHA.114.304082).
    

  

50. Laaksonen, David E., et al. “Prediction of Cardiovascular Mortality in Middle-Aged Men by Dietary and Serum Linoleic and Polyunsaturated Fatty Acids.” Archives of Internal Medicine, vol. 165, no. 2, Jan. 2005, pp. 193–99. PubMed, [https://doi.org/10.1001/archinte.165.2.193](https://doi.org/10.1001/archinte.165.2.193).
    

  

51. Hooper, Lee, et al. “Reduction in Saturated Fat Intake for Cardiovascular Disease.” The Cochrane Database of Systematic Reviews, vol. 8, Aug. 2020, p. CD011737. PubMed, [https://doi.org/10.1002/14651858.CD011737.pub3](https://doi.org/10.1002/14651858.CD011737.pub3).
    

  

52. Woodhill, J. M., et al. “Low Fat, Low Cholesterol Diet in Secondary Prevention of Coronary Heart Disease.” Advances in Experimental Medicine and Biology, vol. 109, 1978, pp. 317–30. PubMed, [https://doi.org/10.1007/978-1-4684-0967-3_18](https://doi.org/10.1007/978-1-4684-0967-3_18).
    

  

53. Frantz, I. D., et al. “Test of Effect of Lipid Lowering by Diet on Cardiovascular Risk. The Minnesota Coronary Survey.” Arteriosclerosis (Dallas, Tex.), vol. 9, no. 1, Feb. 1989, pp. 129–35. PubMed, [https://doi.org/10.1161/01.atv.9.1.129](https://doi.org/10.1161/01.atv.9.1.129).
    

  

54. Dayton, Seymour, et al. “A Controlled Clinical Trial of a Diet High in Unsaturated Fat in Preventing Complications of Atherosclerosis.” Circulation, vol. 40, no. 1s2, July 1969, p. II–1. ahajournals.org (Atypon), [https://doi.org/10.1161/01.CIR.40.1S2.II-1](https://doi.org/10.1161/01.CIR.40.1S2.II-1).
    

  

55. Laguzzi, Federica, et al. “Intake of Food Rich in Saturated Fat in Relation to Subclinical Atherosclerosis and Potential Modulating Effects from Single Genetic Variants.” Scientific Reports, vol. 11, no. 1, Apr. 2021, p. 7866. www.nature.com, [https://doi.org/10.1038/s41598-021-86324-w](https://doi.org/10.1038/s41598-021-86324-w).
    

  

56. William Shurtleff and Akiko Aoyagi, “History of Soy Oil Margarine - Part 2.” A Chapter from the Unpublished Manuscript, History of Soybeans and Soyfoods, 1100 B.C. to the 1980s, 2004, [https://www.soyinfocenter.com/HSS/margarine2.php](https://www.soyinfocenter.com/HSS/margarine2.php)
    

  

57. Zhuang, Pan, et al. “Dietary Fats in Relation to Total and Cause-Specific Mortality in a Prospective Cohort of 521 120 Individuals With 16 Years of Follow-Up.” Circulation Research, vol. 124, no. 5, Mar. 2019, pp. 757–68. PubMed, [https://doi.org/10.1161/CIRCRESAHA.118.314038](https://doi.org/10.1161/CIRCRESAHA.118.314038).
    

  

58. Ramsden, Christopher E., et al. “Use of Dietary Linoleic Acid for Secondary Prevention of Coronary Heart Disease and Death: Evaluation of Recovered Data from the Sydney Diet Heart Study and Updated Meta-Analysis.” BMJ (Clinical Research Ed.), vol. 346, Feb. 2013, p. e8707. PubMed, [https://doi.org/10.1136/bmj.e8707](https://doi.org/10.1136/bmj.e8707).
    

  

59. Heart Foundation Takes Swipe at Butter and New Study on Margarine | Australian Food News. 11 Feb. 2013, [https://www.ausfoodnews.com.au/2013/02/11/heart-foundation-takes-swipe-at-butter-and-new-study-on-margarine.html](https://www.ausfoodnews.com.au/2013/02/11/heart-foundation-takes-swipe-at-butter-and-new-study-on-margarine.html).
    

  

60. News, A. B. C. “Unilever Gets All the Trans Fat out of Its Margarines.” ABC News, [https://abcnews.go.com/Business/story?id=8182555&page=1](https://abcnews.go.com/Business/story?id=8182555&page=1).
    

  

61. Ramsden, Christopher E., et al. “Re-Evaluation of the Traditional Diet-Heart Hypothesis: Analysis of Recovered Data from Minnesota Coronary Experiment (1968-73).” BMJ (Clinical Research Ed.), vol. 353, Apr. 2016, p. i1246. PubMed, [https://doi.org/10.1136/bmj.i1246](https://doi.org/10.1136/bmj.i1246).
    

  

62. Kadhum, Abdul Amir H., and M. Najeeb Shamma. “Edible Lipids Modification Processes: A Review.” Critical Reviews in Food Science and Nutrition, vol. 57, no. 1, Jan. 2017, pp. 48–58. PubMed, [https://doi.org/10.1080/10408398.2013.848834](https://doi.org/10.1080/10408398.2013.848834).
    

  

63. SR11-SR28 : USDA ARS. [https://www.ars.usda.gov/northeast-area/beltsville-md-bhnrc/beltsville-human-nutrition-research-center/methods-and-application-of-food-composition-laboratory/mafcl-site-pages/sr11-sr28/](https://www.ars.usda.gov/northeast-area/beltsville-md-bhnrc/beltsville-human-nutrition-research-center/methods-and-application-of-food-composition-laboratory/mafcl-site-pages/sr11-sr28/).
    

  

64. de Lorgeril, M., et al. “Mediterranean Alpha-Linolenic Acid-Rich Diet in Secondary Prevention of Coronary Heart Disease.” Lancet (London, England), vol. 343, no. 8911, June 1994, pp. 1454–59. PubMed, [https://doi.org/10.1016/s0140-6736(94)92580-1](https://doi.org/10.1016/s0140-6736(94)92580-1).
    

  

65. de Lorgeril, M., et al. “Mediterranean Diet, Traditional Risk Factors, and the Rate of Cardiovascular Complications after Myocardial Infarction: Final Report of the Lyon Diet Heart Study.” Circulation, vol. 99, no. 6, Feb. 1999, pp. 779–85. PubMed, [https://doi.org/10.1161/01.cir.99.6.779](https://doi.org/10.1161/01.cir.99.6.779).
    

  

66. Park, Sehoon, et al. “Causal Effects of Serum Levels of N-3 or n-6 Polyunsaturated Fatty Acids on Coronary Artery Disease: Mendelian Randomization Study.” Nutrients, vol. 13, no. 5, Apr. 2021, p. 1490. PubMed, [https://doi.org/10.3390/nu13051490](https://doi.org/10.3390/nu13051490).
    

  

67. Rett, Brian S., and Jay Whelan. “Increasing Dietary Linoleic Acid Does Not Increase Tissue Arachidonic Acid Content in Adults Consuming Western-Type Diets: A Systematic Review.” Nutrition & Metabolism, vol. 8, June 2011, p. 36. PubMed, [https://doi.org/10.1186/1743-7075-8-36](https://doi.org/10.1186/1743-7075-8-36).
    

  

68. Pearce, M. L., and S. Dayton. “Incidence of Cancer in Men on a Diet High in Polyunsaturated Fat.” Lancet (London, England), vol. 1, no. 7697, Mar. 1971, pp. 464–67. PubMed, [https://doi.org/10.1016/s0140-6736(71)91086-5](https://doi.org/10.1016/s0140-6736(71)91086-5).
    

  

69. Ederer, F., et al. “Cancer among Men on Cholesterol-Lowering Diets: Experience from Five Clinical Trials.” Lancet (London, England), vol. 2, no. 7717, July 1971, pp. 203–06. PubMed, [https://doi.org/10.1016/s0140-6736(71)90911-1](https://doi.org/10.1016/s0140-6736(71)90911-1).
    

  

70. Lee, Peter N., et al. “Systematic Review with Meta-Analysis of the Epidemiological Evidence in the 1900s Relating Smoking to Lung Cancer.” BMC Cancer, vol. 12, Sept. 2012, p. 385. PubMed, [https://doi.org/10.1186/1471-2407-12-385](https://doi.org/10.1186/1471-2407-12-385).
    

  

71. Li, Jun, et al. “Dietary Intake and Biomarkers of Linoleic Acid and Mortality: Systematic Review and Meta-Analysis of Prospective Cohort Studies.” The American Journal of Clinical Nutrition, vol. 112, no. 1, July 2020, pp. 150–67. PubMed, [https://doi.org/10.1093/ajcn/nqz349](https://doi.org/10.1093/ajcn/nqz349).
    

  

72. Ruan, Liang, et al. “Dietary Fat Intake and the Risk of Skin Cancer: A Systematic Review and Meta-Analysis of Observational Studies.” Nutrition and Cancer, vol. 72, no. 3, 2020, pp. 398–408. PubMed, [https://doi.org/10.1080/01635581.2019.1637910](https://doi.org/10.1080/01635581.2019.1637910).
    

  

73. Park, Min Kyung, et al. “Fat Intake and Risk of Skin Cancer in U.S. Adults.” Cancer Epidemiology, Biomarkers & Prevention: A Publication of the American Association for Cancer Research, Cosponsored by the American Society of Preventive Oncology, vol. 27, no. 7, July 2018, pp. 776–82. PubMed, [https://doi.org/10.1158/1055-9965.EPI-17-0782](https://doi.org/10.1158/1055-9965.EPI-17-0782).
    

  

74. Gogia, Ravinder, et al. “Fitzpatrick Skin Phototype Is an Independent Predictor of Squamous Cell Carcinoma Risk after Solid Organ Transplantation.” Journal of the American Academy of Dermatology, vol. 68, no. 4, Apr. 2013, pp. 585–91. PubMed, [https://doi.org/10.1016/j.jaad.2012.09.030](https://doi.org/10.1016/j.jaad.2012.09.030).
    

  

75. Ibiebele, Torukiri I., et al. “Dietary Fat Intake and Risk of Skin Cancer: A Prospective Study in Australian Adults.” International Journal of Cancer, vol. 125, no. 7, Oct. 2009, pp. 1678–84. PubMed, [https://doi.org/10.1002/ijc.24481](https://doi.org/10.1002/ijc.24481).
    

  

76. Wallingford, Sarah C., et al. “Plasma Omega-3 and Omega-6 Concentrations and Risk of Cutaneous Basal and Squamous Cell Carcinomas in Australian Adults.” Cancer Epidemiology, Biomarkers & Prevention: A Publication of the American Association for Cancer Research, Cosponsored by the American Society of Preventive Oncology, vol. 22, no. 10, Oct. 2013, pp. 1900–05. PubMed, [https://doi.org/10.1158/1055-9965.EPI-13-0434](https://doi.org/10.1158/1055-9965.EPI-13-0434).
    

  

77. Harris, Robin B., et al. “Fatty Acid Composition of Red Blood Cell Membranes and Risk of Squamous Cell Carcinoma of the Skin.” Cancer Epidemiology, Biomarkers & Prevention: A Publication of the American Association for Cancer Research, Cosponsored by the American Society of Preventive Oncology, vol. 14, no. 4, Apr. 2005, pp. 906–12. PubMed, [https://doi.org/10.1158/1055-9965.EPI-04-0670](https://doi.org/10.1158/1055-9965.EPI-04-0670).
    

  

78. Seviiri, Mathias, et al. “Polyunsaturated Fatty Acid Levels and the Risk of Keratinocyte Cancer: A Mendelian Randomization Analysis.” Cancer Epidemiology, Biomarkers & Prevention: A Publication of the American Association for Cancer Research, Cosponsored by the American Society of Preventive Oncology, vol. 30, no. 8, Aug. 2021, pp. 1591–98. PubMed, [https://doi.org/10.1158/1055-9965.EPI-20-1765](https://doi.org/10.1158/1055-9965.EPI-20-1765).
    

  

79. Noel, Sophie E., et al. “Consumption of Omega-3 Fatty Acids and the Risk of Skin Cancers: A Systematic Review and Meta-Analysis.” International Journal of Cancer, vol. 135, no. 1, July 2014, pp. 149–56. PubMed, [https://doi.org/10.1002/ijc.28630](https://doi.org/10.1002/ijc.28630).
    

  

80. Watkins, Bruce A., and Jeffrey Kim. “The Endocannabinoid System: Directing Eating Behavior and Macronutrient Metabolism.” Frontiers in Psychology, vol. 5, 2014, p. 1506. PubMed, [https://doi.org/10.3389/fpsyg.2014.01506](https://doi.org/10.3389/fpsyg.2014.01506).
    

  

81. Alvheim, Anita R., et al. “Dietary Linoleic Acid Elevates Endogenous 2-AG and Anandamide and Induces Obesity.” Obesity (Silver Spring, Md.), vol. 20, no. 10, Oct. 2012, pp. 1984–94. PubMed, [https://doi.org/10.1038/oby.2012.38](https://doi.org/10.1038/oby.2012.38).
    

  

82. Blüher, Matthias, et al. “Dysregulation of the Peripheral and Adipose Tissue Endocannabinoid System in Human Abdominal Obesity.” Diabetes, vol. 55, no. 11, Nov. 2006, pp. 3053–60. PubMed, [https://doi.org/10.2337/db06-0812](https://doi.org/10.2337/db06-0812).
    

  

83. Piomelli, Daniele. “The Molecular Logic of Endocannabinoid Signalling.” Nature Reviews. Neuroscience, vol. 4, no. 11, Nov. 2003, pp. 873–84. PubMed, [https://doi.org/10.1038/nrn1247](https://doi.org/10.1038/nrn1247).
    

  

84. Curioni, C., and C. André. “Rimonabant for Overweight or Obesity.” The Cochrane Database of Systematic Reviews, no. 4, Oct. 2006, p. CD006162. PubMed, [https://doi.org/10.1002/14651858.CD006162.pub2](https://doi.org/10.1002/14651858.CD006162.pub2).
    

  

85. Naughton, Shaan S., et al. “The Acute Effect of Oleic- or Linoleic Acid-Containing Meals on Appetite and Metabolic Markers; A Pilot Study in Overweight or Obese Individuals.” Nutrients, vol. 10, no. 10, Sept. 2018, p. E1376. PubMed, [https://doi.org/10.3390/nu10101376](https://doi.org/10.3390/nu10101376).
    

  

86. Flint, Anne, et al. “Effects of Different Dietary Fat Types on Postprandial Appetite and Energy Expenditure.” Obesity Research, vol. 11, no. 12, Dec. 2003, pp. 1449–55. PubMed, [https://doi.org/10.1038/oby.2003.194](https://doi.org/10.1038/oby.2003.194).
    

  

87. Stevenson, Jada L., et al. “Hunger and Satiety Responses to High-Fat Meals of Varying Fatty Acid Composition in Women with Obesity.” Obesity (Silver Spring, Md.), vol. 23, no. 10, Oct. 2015, pp. 1980–86. PubMed, [https://doi.org/10.1002/oby.21202](https://doi.org/10.1002/oby.21202).
    

  

88. Lawton, C. L., et al. “The Degree of Saturation of Fatty Acids Influences Post-Ingestive Satiety.” The British Journal of Nutrition, vol. 83, no. 5, May 2000, pp. 473–82.
    

  

89. French, S. J., et al. “The Effects of Intestinal Infusion of Long-Chain Fatty Acids on Food Intake in Humans.” Gastroenterology, vol. 119, no. 4, Oct. 2000, pp. 943–48. PubMed, [https://doi.org/10.1053/gast.2000.18139](https://doi.org/10.1053/gast.2000.18139).
    

  

90. MacIntosh, Beth A., et al. “Low-n-6 and Low-n-6 plus High-n-3 Diets for Use in Clinical Research.” The British Journal of Nutrition, vol. 110, no. 3, Aug. 2013, pp. 559–68. PubMed, [https://doi.org/10.1017/S0007114512005181](https://doi.org/10.1017/S0007114512005181).
    

  

91. Stevenson, Jada L., et al. “Hunger and Satiety Responses to High-Fat Meals after a High-Polyunsaturated Fat Diet: A Randomized Trial.” Nutrition (Burbank, Los Angeles County, Calif.), vol. 41, Sept. 2017, pp. 14–23. PubMed, [https://doi.org/10.1016/j.nut.2017.03.008](https://doi.org/10.1016/j.nut.2017.03.008).
    

  

92. Strik, Caroline M., et al. “No Evidence of Differential Effects of SFA, MUFA or PUFA on Post-Ingestive Satiety and Energy Intake: A Randomised Trial of Fatty Acid Saturation.” Nutrition Journal, vol. 9, May 2010, p. 24. PubMed, [https://doi.org/10.1186/1475-2891-9-24](https://doi.org/10.1186/1475-2891-9-24).
    

  

93. Martin, Melanie A., et al. “Fatty Acid Composition in the Mature Milk of Bolivian Forager-Horticulturalists: Controlled Comparisons with a US Sample.” Maternal & Child Nutrition, vol. 8, no. 3, July 2012, pp. 404–18. PubMed, [https://doi.org/10.1111/j.1740-8709.2012.00412.x](https://doi.org/10.1111/j.1740-8709.2012.00412.x).
    

  

94. McLaughlin, Joe, et al. “Adipose Tissue Triglyceride Fatty Acids and Atherosclerosis in Alaska Natives and Non-Natives.” Atherosclerosis, vol. 181, no. 2, Aug. 2005, pp. 353–62. PubMed, [https://doi.org/10.1016/j.atherosclerosis.2005.01.019](https://doi.org/10.1016/j.atherosclerosis.2005.01.019).
    

  

95. Hirsch, Jules. “Fatty Acid Patterns in Human Adipose Tissue.” Comprehensive Physiology, American Cancer Society, 2011, pp. 181–89. Wiley Online Library, [https://doi.org/10.1002/cphy.cp050117](https://doi.org/10.1002/cphy.cp050117).
    

  

96. Heldenberg, D., et al. “Breast Milk and Adipose Tissue Fatty Acid Composition in Relation to Maternal Dietary Intake.” Clinical Nutrition (Edinburgh, Scotland), vol. 2, no. 2, July 1983, pp. 73–77. PubMed, [https://doi.org/10.1016/0261-5614(83)90036-5](https://doi.org/10.1016/0261-5614(83)90036-5).
    

  

97. Martin, J. C., et al. “Dependence of Human Milk Essential Fatty Acids on Adipose Stores during Lactation.” The American Journal of Clinical Nutrition, vol. 58, no. 5, Nov. 1993, pp. 653–59. PubMed, [https://doi.org/10.1093/ajcn/58.5.653](https://doi.org/10.1093/ajcn/58.5.653).
    

  

98. Demmelmair, H., et al. “Metabolism of U13C-Labeled Linoleic Acid in Lactating Women.” Journal of Lipid Research, vol. 39, no. 7, July 1998, pp. 1389–96.
    

  

99. Kuipers, Remko S., et al. “Estimated Macronutrient and Fatty Acid Intakes from an East African Paleolithic Diet.” The British Journal of Nutrition, vol. 104, no. 11, Dec. 2010, pp. 1666–87. PubMed, [https://doi.org/10.1017/S0007114510002679](https://doi.org/10.1017/S0007114510002679).
    

  

100. Leren, P. “Prevention of Coronary Heart Disease: Some Results from the Oslo Secondary and Primary Intervention Studies.” Journal of the American College of Nutrition, vol. 8, no. 5, Oct. 1989, pp. 407–10. PubMed, [https://doi.org/10.1080/07315724.1989.10720315](https://doi.org/10.1080/07315724.1989.10720315).
    

  

101. “Controlled Trial of Soya-Bean Oil in Myocardial Infarction.” Lancet (London, England), vol. 2, no. 7570, Sept. 1968, pp. 693–99.
    

  

102. Krishnan, Sridevi, and Jamie A. Cooper. “Effect of Dietary Fatty Acid Composition on Substrate Utilization and Body Weight Maintenance in Humans.” European Journal of Nutrition, vol. 53, no. 3, Apr. 2014, pp. 691–710. PubMed, [https://doi.org/10.1007/s00394-013-0638-z](https://doi.org/10.1007/s00394-013-0638-z).
    

  

103. Jones, P. J., et al. “Influence of Dietary Fat Polyunsaturated to Saturated Ratio on Energy Substrate Utilization in Obesity.” Metabolism: Clinical and Experimental, vol. 41, no. 4, Apr. 1992, pp. 396–401. PubMed, [https://doi.org/10.1016/0026-0495(92)90074-k](https://doi.org/10.1016/0026-0495(92)90074-k).
    

  

104. van Marken Lichtenbelt, W. D., et al. “The Effect of Fat Composition of the Diet on Energy Metabolism.” Zeitschrift Fur Ernahrungswissenschaft, vol. 36, no. 4, Dec. 1997, pp. 303–05. PubMed, [https://doi.org/10.1007/BF01617803](https://doi.org/10.1007/BF01617803).
    

  

105. Casas-Agustench, P., et al. “Acute Effects of Three High-Fat Meals with Different Fat Saturations on Energy Expenditure, Substrate Oxidation and Satiety.” Clinical Nutrition (Edinburgh, Scotland), vol. 28, no. 1, Feb. 2009, pp. 39–45. PubMed, [https://doi.org/10.1016/j.clnu.2008.10.008](https://doi.org/10.1016/j.clnu.2008.10.008).
    

  

106. DeLany, J. P., et al. “Differential Oxidation of Individual Dietary Fatty Acids in Humans.” The American Journal of Clinical Nutrition, vol. 72, no. 4, Oct. 2000, pp. 905–11. PubMed, [https://doi.org/10.1093/ajcn/72.4.905](https://doi.org/10.1093/ajcn/72.4.905).
    

  

107. Iggman, D., et al. “Adipose Tissue Fatty Acids and Insulin Sensitivity in Elderly Men.” Diabetologia, vol. 53, no. 5, May 2010, pp. 850–57. PubMed, [https://doi.org/10.1007/s00125-010-1669-0](https://doi.org/10.1007/s00125-010-1669-0).
    

  

108. Heine, R. J., et al. “Linoleic-Acid-Enriched Diet: Long-Term Effects on Serum Lipoprotein and Apolipoprotein Concentrations and Insulin Sensitivity in Noninsulin-Dependent Diabetic Patients.” The American Journal of Clinical Nutrition, vol. 49, no. 3, Mar. 1989, pp. 448–56. PubMed, [https://doi.org/10.1093/ajcn/49.3.448](https://doi.org/10.1093/ajcn/49.3.448).
    

  

109. Zibaeenezhad, Mohammadjavad, et al. “The Effect of Walnut Oil Consumption on Blood Sugar in Patients With Diabetes Mellitus Type 2.” International Journal of Endocrinology and Metabolism, vol. 14, no. 3, July 2016, p. e34889. PubMed, [https://doi.org/10.5812/ijem.34889](https://doi.org/10.5812/ijem.34889).
    

  

110. Pertiwi, Kamalita, et al. “Associations of Linoleic Acid with Markers of Glucose Metabolism and Liver Function in South African Adults.” Lipids in Health and Disease, vol. 19, no. 1, June 2020, p. 138. PubMed, [https://doi.org/10.1186/s12944-020-01318-3](https://doi.org/10.1186/s12944-020-01318-3).
    

  

111. Kröger, Janine, et al. “Erythrocyte Membrane Phospholipid Fatty Acids, Desaturase Activity, and Dietary Fatty Acids in Relation to Risk of Type 2 Diabetes in the European Prospective Investigation into Cancer and Nutrition (EPIC)-Potsdam Study.” The American Journal of Clinical Nutrition, vol. 93, no. 1, Jan. 2011, pp. 127–42. PubMed, [https://doi.org/10.3945/ajcn.110.005447](https://doi.org/10.3945/ajcn.110.005447).
    

  

112. Wu, Jason H. Y., et al. “Omega-6 Fatty Acid Biomarkers and Incident Type 2 Diabetes: Pooled Analysis of Individual-Level Data for 39 740 Adults from 20 Prospective Cohort Studies.” The Lancet. Diabetes & Endocrinology, vol. 5, no. 12, Dec. 2017, pp. 965–74. PubMed, [https://doi.org/10.1016/S2213-8587(17)30307-8](https://doi.org/10.1016/S2213-8587(17)30307-8).
    

  

113. Yepes-Calderón, Manuela, et al. “Plasma Malondialdehyde and Risk of New-Onset Diabetes after Transplantation in Renal Transplant Recipients: A Prospective Cohort Study.” Journal of Clinical Medicine, vol. 8, no. 4, Apr. 2019, p. E453. PubMed, [https://doi.org/10.3390/jcm8040453](https://doi.org/10.3390/jcm8040453).
    

  

114. Chen, Cai, et al. “Association between Omega-3 Fatty Acids Consumption and the Risk of Type 2 Diabetes: A Meta-Analysis of Cohort Studies.” Journal of Diabetes Investigation, vol. 8, no. 4, July 2017, pp. 480–88. PubMed, [https://doi.org/10.1111/jdi.12614](https://doi.org/10.1111/jdi.12614).
    

  

115. Brown, Tracey J., et al. “Omega-3, Omega-6, and Total Dietary Polyunsaturated Fat for Prevention and Treatment of Type 2 Diabetes Mellitus: Systematic Review and Meta-Analysis of Randomised Controlled Trials.” BMJ (Clinical Research Ed.), vol. 366, Aug. 2019, p. l4697. PubMed, [https://doi.org/10.1136/bmj.l4697](https://doi.org/10.1136/bmj.l4697).
    

  

116. Santoro, Nicola, et al. “Oxidized Metabolites of Linoleic Acid as Biomarkers of Liver Injury in Nonalcoholic Steatohepatitis.” Clinical Lipidology, vol. 8, no. 4, Aug. 2013, pp. 411–18. PubMed, [https://doi.org/10.2217/clp.13.39](https://doi.org/10.2217/clp.13.39).
    

  

117. Hojsak, Iva, and Sanja Kolaček. “Fat Overload Syndrome after the Rapid Infusion of SMOFlipid Emulsion.” JPEN. Journal of Parenteral and Enteral Nutrition, vol. 38, no. 1, Jan. 2014, pp. 119–21. PubMed, [https://doi.org/10.1177/0148607113482001](https://doi.org/10.1177/0148607113482001).
    

  

118. Gura, Kathleen M., and Mark Puder. “Rapid Infusion of Fish Oil-Based Emulsion in Infants Does Not Appear to Be Associated with Fat Overload Syndrome.” Nutrition in Clinical Practice: Official Publication of the American Society for Parenteral and Enteral Nutrition, vol. 25, no. 4, Aug. 2010, pp. 399–402. PubMed, [https://doi.org/10.1177/0884533610373770](https://doi.org/10.1177/0884533610373770).
    

  

119. Fell, Gillian L., et al. “Intravenous Lipid Emulsions in Parenteral Nutrition.” Advances in Nutrition (Bethesda, Md.), vol. 6, no. 5, Sept. 2015, pp. 600–10. PubMed, [https://doi.org/10.3945/an.115.009084](https://doi.org/10.3945/an.115.009084).
    

  

120. Clayton, P. T., et al. “The Role of Phytosterols in the Pathogenesis of Liver Complications of Pediatric Parenteral Nutrition.” Nutrition (Burbank, Los Angeles County, Calif.), vol. 14, no. 1, Jan. 1998, pp. 158–64. PubMed, [https://doi.org/10.1016/s0899-9007(97)00233-5](https://doi.org/10.1016/s0899-9007(97)00233-5).
    

  

121. El Kasmi, Karim C., et al. “Phytosterols Promote Liver Injury and Kupffer Cell Activation in Parenteral Nutrition-Associated Liver Disease.” Science Translational Medicine, vol. 5, no. 206, Oct. 2013, p. 206ra137. PubMed, [https://doi.org/10.1126/scitranslmed.3006898](https://doi.org/10.1126/scitranslmed.3006898).
    

  

122. Nehra, Deepika, et al. “A Comparison of 2 Intravenous Lipid Emulsions: Interim Analysis of a Randomized Controlled Trial.” JPEN. Journal of Parenteral and Enteral Nutrition, vol. 38, no. 6, Aug. 2014, pp. 693–701. PubMed, [https://doi.org/10.1177/0148607113492549](https://doi.org/10.1177/0148607113492549).
    

  

123. Klek, Stanislaw, et al. “Four-Week Parenteral Nutrition Using a Third Generation Lipid Emulsion (SMOFlipid)--a Double-Blind, Randomised, Multicentre Study in Adults.” Clinical Nutrition (Edinburgh, Scotland), vol. 32, no. 2, Apr. 2013, pp. 224–31. PubMed, [https://doi.org/10.1016/j.clnu.2012.06.011](https://doi.org/10.1016/j.clnu.2012.06.011).
    

  

124. Van Name, Michelle A., et al. “A Low ω-6 to ω-3 PUFA Ratio (n-6:N-3 PUFA) Diet to Treat Fatty Liver Disease in Obese Youth.” The Journal of Nutrition, vol. 150, no. 9, Sept. 2020, pp. 2314–21. PubMed, [https://doi.org/10.1093/jn/nxaa183](https://doi.org/10.1093/jn/nxaa183).
    

  

125. [https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7467848/bin/nxaa183_supplemental_files.zip](https://www.ncbi.nlm.nih.gov/labs/pmc/articles/PMC7467848/bin/nxaa183_supplemental_files.zip)
    

  

126. Bjermo, Helena, et al. “Effects of N-6 PUFAs Compared with SFAs on Liver Fat, Lipoproteins, and Inflammation in Abdominal Obesity: A Randomized Controlled Trial.” The American Journal of Clinical Nutrition, vol. 95, no. 5, May 2012, pp. 1003–12. PubMed, [https://doi.org/10.3945/ajcn.111.030114](https://doi.org/10.3945/ajcn.111.030114).
    

  

127. Rosqvist, Fredrik, et al. “Overfeeding Polyunsaturated and Saturated Fat Causes Distinct Effects on Liver and Visceral Fat Accumulation in Humans.” Diabetes, vol. 63, no. 7, July 2014, pp. 2356–68. PubMed, [https://doi.org/10.2337/db13-1622](https://doi.org/10.2337/db13-1622).
    

  

128. Petit, J. M., et al. “Increased Erythrocytes N-3 and n-6 Polyunsaturated Fatty Acids Is Significantly Associated with a Lower Prevalence of Steatosis in Patients with Type 2 Diabetes.” Clinical Nutrition (Edinburgh, Scotland), vol. 31, no. 4, Aug. 2012, pp. 520–25. PubMed, [https://doi.org/10.1016/j.clnu.2011.12.007](https://doi.org/10.1016/j.clnu.2011.12.007).
    

  

129. Cheng, Yipeng, et al. “Associations between Dietary Nutrient Intakes and Hepatic Lipid Contents in NAFLD Patients Quantified by 1H-MRS and Dual-Echo MRI.” Nutrients, vol. 8, no. 9, Aug. 2016, p. E527. PubMed, [https://doi.org/10.3390/nu8090527](https://doi.org/10.3390/nu8090527).
    

  

130. Wehmeyer, Malte H., et al. “Nonalcoholic Fatty Liver Disease Is Associated with Excessive Calorie Intake Rather than a Distinctive Dietary Pattern.” Medicine, vol. 95, no. 23, June 2016, p. e3887. PubMed, [https://doi.org/10.1097/MD.0000000000003887](https://doi.org/10.1097/MD.0000000000003887).
    

  

131. Lourdudoss, Cecilia, et al. “Dietary Intake of Polyunsaturated Fatty Acids and Pain in Spite of Inflammatory Control Among Methotrexate-Treated Early Rheumatoid Arthritis Patients.” Arthritis Care & Research, vol. 70, no. 2, Feb. 2018, pp. 205–12. PubMed, [https://doi.org/10.1002/acr.23245](https://doi.org/10.1002/acr.23245).
    

  

132. Navarini, Luca, et al. “Polyunsaturated Fatty Acids: Any Role in Rheumatoid Arthritis?” Lipids in Health and Disease, vol. 16, no. 1, Oct. 2017, p. 197. PubMed, [https://doi.org/10.1186/s12944-017-0586-3](https://doi.org/10.1186/s12944-017-0586-3).
    

  

133. Senftleber, Ninna K., et al. “Marine Oil Supplements for Arthritis Pain: A Systematic Review and Meta-Analysis of Randomized Trials.” Nutrients, vol. 9, no. 1, Jan. 2017, p. E42. PubMed, [https://doi.org/10.3390/nu9010042](https://doi.org/10.3390/nu9010042).
    

  

134. Wang, Yuanyuan, et al. “Dietary Fatty Acid Intake Affects the Risk of Developing Bone Marrow Lesions in Healthy Middle-Aged Adults without Clinical Knee Osteoarthritis: A Prospective Cohort Study.” Arthritis Research & Therapy, vol. 11, no. 3, 2009, p. R63. PubMed, [https://doi.org/10.1186/ar2688](https://doi.org/10.1186/ar2688).
    

  

135. Leventhal, L. J., et al. “Treatment of Rheumatoid Arthritis with Blackcurrant Seed Oil.” British Journal of Rheumatology, vol. 33, no. 9, Sept. 1994, pp. 847–52. PubMed, [https://doi.org/10.1093/rheumatology/33.9.847](https://doi.org/10.1093/rheumatology/33.9.847).
    

  

136. Byars, M. L., et al. “Blackcurrant Seed Oil as a Source of Polyunsaturated Fatty Acids in the Treatment of Inflammatory Disease.” Biochemical Society Transactions, vol. 20, no. 2, May 1992, p. 139S. PubMed, [https://doi.org/10.1042/bst020139s](https://doi.org/10.1042/bst020139s).
    

  

137. Šavikin, Katarina P., et al. “Variation in the Fatty-Acid Content in Seeds of Various Black, Red, and White Currant Varieties.” Chemistry & Biodiversity, vol. 10, no. 1, Jan. 2013, pp. 157–65. PubMed, [https://doi.org/10.1002/cbdv.201200223](https://doi.org/10.1002/cbdv.201200223).
    

  

138. Watson, J., et al. “Cytokine and Prostaglandin Production by Monocytes of Volunteers and Rheumatoid Arthritis Patients Treated with Dietary Supplements of Blackcurrant Seed Oil.” British Journal of Rheumatology, vol. 32, no. 12, Dec. 1993, pp. 1055–58. PubMed, [https://doi.org/10.1093/rheumatology/32.12.1055](https://doi.org/10.1093/rheumatology/32.12.1055).
    

  

139. Timoszuk, Magdalena, et al. “Evening Primrose (Oenothera Biennis) Biological Activity Dependent on Chemical Composition.” Antioxidants (Basel, Switzerland), vol. 7, no. 8, Aug. 2018, p. E108. PubMed, [https://doi.org/10.3390/antiox7080108](https://doi.org/10.3390/antiox7080108).
    

  

140. Brzeski, M., et al. “Evening Primrose Oil in Patients with Rheumatoid Arthritis and Side-Effects of Non-Steroidal Anti-Inflammatory Drugs.” British Journal of Rheumatology, vol. 30, no. 5, Oct. 1991, pp. 370–72. PubMed, [https://doi.org/10.1093/rheumatology/30.5.370](https://doi.org/10.1093/rheumatology/30.5.370).
    

  

141. Horrobin, D. F. “Effects of Evening Primrose Oil in Rheumatoid Arthritis.” Annals of the Rheumatic Diseases, vol. 48, no. 11, Nov. 1989, pp. 965–66. PubMed, [https://doi.org/10.1136/ard.48.11.965](https://doi.org/10.1136/ard.48.11.965).
    

  

142. Belch, J. J., et al. “Effects of Altering Dietary Essential Fatty Acids on Requirements for Non-Steroidal Anti-Inflammatory Drugs in Patients with Rheumatoid Arthritis: A Double Blind Placebo Controlled Study.” Annals of the Rheumatic Diseases, vol. 47, no. 2, Feb. 1988, pp. 96–104. PubMed, [https://doi.org/10.1136/ard.47.2.96](https://doi.org/10.1136/ard.47.2.96).
    

  

143. Volker, D., et al. “Efficacy of Fish Oil Concentrate in the Treatment of Rheumatoid Arthritis.” The Journal of Rheumatology, vol. 27, no. 10, Oct. 2000, pp. 2343–46.
    

  

144. Essouiri, Jamila, et al. “Effectiveness of Argan Oil Consumption on Knee Osteoarthritis Symptoms: A Randomized Controlled Clinical Trial.” Current Rheumatology Reviews, vol. 13, no. 3, 2017, pp. 231–35. PubMed, [https://doi.org/10.2174/1573397113666170710123031](https://doi.org/10.2174/1573397113666170710123031).
    

  

145. Khallouki, F., et al. “Consumption of Argan Oil (Morocco) with Its Unique Profile of Fatty Acids, Tocopherols, Squalene, Sterols and Phenolic Compounds Should Confer Valuable Cancer Chemopreventive Effects.” European Journal of Cancer Prevention: The Official Journal of the European Cancer Prevention Organisation (ECP), vol. 12, no. 1, Feb. 2003, pp. 67–75. PubMed, [https://doi.org/10.1097/00008469-200302000-00011](https://doi.org/10.1097/00008469-200302000-00011).
    

  

146. Tuna, Halil Ibrahim, et al. “Investigation of the Effect of Black Cumin Oil on Pain in Osteoarthritis Geriatric Individuals.” Complementary Therapies in Clinical Practice, vol. 31, May 2018, pp. 290–94. PubMed, [https://doi.org/10.1016/j.ctcp.2018.03.013](https://doi.org/10.1016/j.ctcp.2018.03.013).
    

  

147. Gorczyca, D., et al. “Serum Levels of N-3 and n-6 Polyunsaturated Fatty Acids in Patients with Systemic Lupus Erythematosus and Their Association with Disease Activity: A Pilot Study.” Scandinavian Journal of Rheumatology, June 2021, pp. 1–7. PubMed, [https://doi.org/10.1080/03009742.2021.1923183](https://doi.org/10.1080/03009742.2021.1923183).
    

  

148. Charoenwoodhipong, Prae, et al. “Dietary Omega Polyunsaturated Fatty Acid Intake and Patient-Reported Outcomes in Systemic Lupus Erythematosus: The Michigan Lupus Epidemiology and Surveillance Program.” Arthritis Care & Research, vol. 72, no. 7, July 2020, pp. 874–81. PubMed, [https://doi.org/10.1002/acr.23925](https://doi.org/10.1002/acr.23925).
    

  

149. Lourdudoss, Cecilia, et al. “The Association between Diet and Glucocorticoid Treatment in Patients with SLE.” Lupus Science & Medicine, vol. 3, no. 1, 2016, p. e000135. PubMed, [https://doi.org/10.1136/lupus-2015-000135](https://doi.org/10.1136/lupus-2015-000135).
    

  

150. Vordenbäumen, Stefan, et al. “Erythrocyte Membrane Polyunsaturated Fatty Acid Profiles Are Associated with Systemic Inflammation and Fish Consumption in Systemic Lupus Erythematosus: A Cross-Sectional Study.” Lupus, vol. 29, no. 6, May 2020, pp. 554–59. PubMed, [https://doi.org/10.1177/0961203320912326](https://doi.org/10.1177/0961203320912326).
    

  

151. Pocovi-Gerardino, G., et al. “Diet Quality and High-Sensitivity C-Reactive Protein in Patients With Systemic Lupus Erythematosus.” Biological Research for Nursing, vol. 21, no. 1, Jan. 2019, pp. 107–13. PubMed, [https://doi.org/10.1177/1099800418803176](https://doi.org/10.1177/1099800418803176).
    

  

152. Lourdudoss, Cecilia, et al. “The Association between Diet and Glucocorticoid Treatment in Patients with SLE.” Lupus Science & Medicine, vol. 3, no. 1, 2016, p. e000135. PubMed, [https://doi.org/10.1136/lupus-2015-000135](https://doi.org/10.1136/lupus-2015-000135).
    

  

153. Shin, Tae Hwan, et al. “Analysis of the Free Fatty Acid Metabolome in the Plasma of Patients with Systemic Lupus Erythematosus and Fever.” Metabolomics: Official Journal of the Metabolomic Society, vol. 14, no. 1, Dec. 2017, p. 14. PubMed, [https://doi.org/10.1007/s11306-017-1308-6](https://doi.org/10.1007/s11306-017-1308-6).
    

  

154. Shah, Dilip, et al. “Oxidative Stress and Its Biomarkers in Systemic Lupus Erythematosus.” Journal of Biomedical Science, vol. 21, Mar. 2014, p. 23. PubMed, [https://doi.org/10.1186/1423-0127-21-23](https://doi.org/10.1186/1423-0127-21-23).
    

  

155. Tam, Lai S., et al. “Effects of Vitamins C and E on Oxidative Stress Markers and Endothelial Function in Patients with Systemic Lupus Erythematosus: A Double Blind, Placebo Controlled Pilot Study.” The Journal of Rheumatology, vol. 32, no. 2, Feb. 2005, pp. 275–82.
    

  

156. Maeshima, Etsuko, et al. “The Efficacy of Vitamin E against Oxidative Damage and Autoantibody Production in Systemic Lupus Erythematosus: A Preliminary Study.” Clinical Rheumatology, vol. 26, no. 3, Mar. 2007, pp. 401–04. PubMed, [https://doi.org/10.1007/s10067-006-0477-x](https://doi.org/10.1007/s10067-006-0477-x).
    

  

157. Bae, Sang-Cheol, et al. “Impaired Antioxidant Status and Decreased Dietary Intake of Antioxidants in Patients with Systemic Lupus Erythematosus.” Rheumatology International, vol. 22, no. 6, Nov. 2002, pp. 238–43. PubMed, [https://doi.org/10.1007/s00296-002-0241-8](https://doi.org/10.1007/s00296-002-0241-8).
    

  

158. Minami, Yuko, et al. “Diet and Systemic Lupus Erythematosus: A 4 Year Prospective Study of Japanese Patients.” The Journal of Rheumatology, vol. 30, no. 4, Apr. 2003, pp. 747–54.
    

  

159. Barbhaiya, Medha, et al. “Association of Dietary Quality With Risk of Incident Systemic Lupus Erythematosus in the Nurses’ Health Study and Nurses’ Health Study II.” Arthritis Care & Research, vol. 73, no. 9, Sept. 2021, pp. 1250–58. PubMed, [https://doi.org/10.1002/acr.24443](https://doi.org/10.1002/acr.24443).
    

  

160. Tedeschi, S. K., et al. “Dietary Patterns and Risk of Systemic Lupus Erythematosus in Women.” Lupus, vol. 29, no. 1, Jan. 2020, pp. 67–73. PubMed, [https://doi.org/10.1177/0961203319888791](https://doi.org/10.1177/0961203319888791).
    

  

161. Zhao, Jie V., and C. Mary Schooling. “Role of Linoleic Acid in Autoimmune Disorders: A Mendelian Randomisation Study.” Annals of the Rheumatic Diseases, vol. 78, no. 5, May 2019, pp. 711–13. PubMed, [https://doi.org/10.1136/annrheumdis-2018-214519](https://doi.org/10.1136/annrheumdis-2018-214519).
    

  

162. Knobbe, Chris A., and Marija Stojanoska. “The ‘Displacing Foods of Modern Commerce’ Are the Primary and Proximate Cause of Age-Related Macular Degeneration: A Unifying Singular Hypothesis.” Medical Hypotheses, vol. 109, Nov. 2017, pp. 184–98. PubMed, [https://doi.org/10.1016/j.mehy.2017.10.010](https://doi.org/10.1016/j.mehy.2017.10.010).
    

  

163. Knobbe, Chris a. Ancestral Dietary Strategy to Prevent and Treat Macular Degeneration: Full-Color Hardcover Edition. 2nd ed. edition, Cure AMD Foundation, 2019.
    

  

164. Delcourt, C., et al. “Dietary Fat and the Risk of Age-Related Maculopathy: The POLANUT Study.” European Journal of Clinical Nutrition, vol. 61, no. 11, Nov. 2007, pp. 1341–44. PubMed, [https://doi.org/10.1038/sj.ejcn.1602685](https://doi.org/10.1038/sj.ejcn.1602685).
    

  

165. Seddon, Johanna M., et al. “Progression of Age-Related Macular Degeneration: Association with Dietary Fat, Transunsaturated Fat, Nuts, and Fish Intake.” Archives of Ophthalmology (Chicago, Ill.: 1960), vol. 121, no. 12, Dec. 2003, pp. 1728–37. PubMed, [https://doi.org/10.1001/archopht.121.12.1728](https://doi.org/10.1001/archopht.121.12.1728).
    

  

166. Chong, Elaine W. T., et al. “Fat Consumption and Its Association with Age-Related Macular Degeneration.” Archives of Ophthalmology (Chicago, Ill.: 1960), vol. 127, no. 5, May 2009, pp. 674–80. PubMed, [https://doi.org/10.1001/archophthalmol.2009.60](https://doi.org/10.1001/archophthalmol.2009.60).
    

  

167. Chua, Brian, et al. “Dietary Fatty Acids and the 5-Year Incidence of Age-Related Maculopathy.” Archives of Ophthalmology (Chicago, Ill.: 1960), vol. 124, no. 7, July 2006, pp. 981–86. PubMed, [https://doi.org/10.1001/archopht.124.7.981](https://doi.org/10.1001/archopht.124.7.981).
    

  

168. Cho, E., et al. “Prospective Study of Dietary Fat and the Risk of Age-Related Macular Degeneration.” The American Journal of Clinical Nutrition, vol. 73, no. 2, Feb. 2001, pp. 209–18. PubMed, [https://doi.org/10.1093/ajcn/73.2.209](https://doi.org/10.1093/ajcn/73.2.209).
    

  

169. Tan, Jennifer S. L., et al. “Dietary Fatty Acids and the 10-Year Incidence of Age-Related Macular Degeneration: The Blue Mountains Eye Study.” Archives of Ophthalmology (Chicago, Ill.: 1960), vol. 127, no. 5, May 2009, pp. 656–65. PubMed, [https://doi.org/10.1001/archophthalmol.2009.76](https://doi.org/10.1001/archophthalmol.2009.76).
    

  

170. Parekh, Niyati, et al. “Association between Dietary Fat Intake and Age-Related Macular Degeneration in the Carotenoids in Age-Related Eye Disease Study (CAREDS): An Ancillary Study of the Women’s Health Initiative.” Archives of Ophthalmology (Chicago, Ill.: 1960), vol. 127, no. 11, Nov. 2009, pp. 1483–93. PubMed, [https://doi.org/10.1001/archophthalmol.2009.130](https://doi.org/10.1001/archophthalmol.2009.130).
    

  

171. Sasaki, Mariko, et al. “Dietary Saturated Fatty Acid Intake and Early Age-Related Macular Degeneration in a Japanese Population.” Investigative Ophthalmology & Visual Science, vol. 61, no. 3, Mar. 2020, p. 23. PubMed, [https://doi.org/10.1167/iovs.61.3.23](https://doi.org/10.1167/iovs.61.3.23).
    

  

172. Christen, William G., et al. “Dietary ω-3 Fatty Acid and Fish Intake and Incident Age-Related Macular Degeneration in Women.” Archives of Ophthalmology (Chicago, Ill.: 1960), vol. 129, no. 7, July 2011, pp. 921–29. PubMed, [https://doi.org/10.1001/archophthalmol.2011.34](https://doi.org/10.1001/archophthalmol.2011.34).
    

  

173. Mares-Perlman, J. A., et al. “Dietary Fat and Age-Related Maculopathy.” Archives of Ophthalmology (Chicago, Ill.: 1960), vol. 113, no. 6, June 1995, pp. 743–48. PubMed, [https://doi.org/10.1001/archopht.1995.01100060069034](https://doi.org/10.1001/archopht.1995.01100060069034).
    

  

174. Ma, Le, et al. “Lutein and Zeaxanthin Intake and the Risk of Age-Related Macular Degeneration: A Systematic Review and Meta-Analysis.” The British Journal of Nutrition, vol. 107, no. 3, Feb. 2012, pp. 350–59. PubMed, [https://doi.org/10.1017/S0007114511004260](https://doi.org/10.1017/S0007114511004260).
    

  

175. Chong, Elaine W. T., et al. “Dietary Omega-3 Fatty Acid and Fish Intake in the Primary Prevention of Age-Related Macular Degeneration: A Systematic Review and Meta-Analysis.” Archives of Ophthalmology (Chicago, Ill.: 1960), vol. 126, no. 6, June 2008, pp. 826–33. PubMed, [https://doi.org/10.1001/archopht.126.6.826](https://doi.org/10.1001/archopht.126.6.826).
    

  

176. Wang, Kai, et al. “Causal Effects of N-6 Polyunsaturated Fatty Acids on Age-Related Macular Degeneration: A Mendelian Randomization Study.” The Journal of Clinical Endocrinology and Metabolism, vol. 106, no. 9, Aug. 2021, pp. e3565–72. PubMed, [https://doi.org/10.1210/clinem/dgab338](https://doi.org/10.1210/clinem/dgab338).
    

  

177. Laitinen, M. H., et al. “Fat Intake at Midlife and Risk of Dementia and Alzheimer’s Disease: A Population-Based Study.” Dementia and Geriatric Cognitive Disorders, vol. 22, no. 1, 2006, pp. 99–107. PubMed, [https://doi.org/10.1159/000093478](https://doi.org/10.1159/000093478).
    

  

178. Morris, Martha Clare, et al. “Dietary Fats and the Risk of Incident Alzheimer Disease.” Archives of Neurology, vol. 60, no. 2, Feb. 2003, pp. 194–200. PubMed, [https://doi.org/10.1001/archneur.60.2.194](https://doi.org/10.1001/archneur.60.2.194).
    

  

179. Luchsinger, Jose A., et al. “Caloric Intake and the Risk of Alzheimer Disease.” Archives of Neurology, vol. 59, no. 8, Aug. 2002, pp. 1258–63. PubMed, [https://doi.org/10.1001/archneur.59.8.1258](https://doi.org/10.1001/archneur.59.8.1258).
    

  

180. Rönnemaa, E., et al. “Serum Fatty-Acid Composition and the Risk of Alzheimer’s Disease: A Longitudinal Population-Based Study.” European Journal of Clinical Nutrition, vol. 66, no. 8, Aug. 2012, pp. 885–90. PubMed, [https://doi.org/10.1038/ejcn.2012.63](https://doi.org/10.1038/ejcn.2012.63).
    

  

181. Beydoun, May A., et al. “Plasma N-3 Fatty Acids and the Risk of Cognitive Decline in Older Adults: The Atherosclerosis Risk in Communities Study.” The American Journal of Clinical Nutrition, vol. 85, no. 4, Apr. 2007, pp. 1103–11. PubMed, [https://doi.org/10.1093/ajcn/85.4.1103](https://doi.org/10.1093/ajcn/85.4.1103).
    

  

182. González, Sonia, et al. “The Relationship between Dietary Lipids and Cognitive Performance in an Elderly Population.” International Journal of Food Sciences and Nutrition, vol. 61, no. 2, Mar. 2010, pp. 217–25. PubMed, [https://doi.org/10.3109/09637480903348098](https://doi.org/10.3109/09637480903348098).
    

  

183. Kalmijn, S., et al. “Polyunsaturated Fatty Acids, Antioxidants, and Cognitive Function in Very Old Men.” American Journal of Epidemiology, vol. 145, no. 1, Jan. 1997, pp. 33–41. PubMed, [https://doi.org/10.1093/oxfordjournals.aje.a009029](https://doi.org/10.1093/oxfordjournals.aje.a009029).
    

  

184. Vercambre, Marie-Noël, et al. “Long-Term Association of Food and Nutrient Intakes with Cognitive and Functional Decline: A 13-Year Follow-up Study of Elderly French Women.” The British Journal of Nutrition, vol. 102, no. 3, Aug. 2009, pp. 419–27. PubMed, [https://doi.org/10.1017/S0007114508201959](https://doi.org/10.1017/S0007114508201959).
    

  

185. Nozaki, Shoko, et al. “Association Between Dietary Fish and PUFA Intake in Midlife and Dementia in Later Life: The JPHC Saku Mental Health Study.” Journal of Alzheimer’s Disease: JAD, vol. 79, no. 3, 2021, pp. 1091–104. PubMed, [https://doi.org/10.3233/JAD-191313](https://doi.org/10.3233/JAD-191313).
    

  

186. Heude, Barbara, et al. “Cognitive Decline and Fatty Acid Composition of Erythrocyte Membranes--The EVA Study.” The American Journal of Clinical Nutrition, vol. 77, no. 4, Apr. 2003, pp. 803–08. PubMed, [https://doi.org/10.1093/ajcn/77.4.803](https://doi.org/10.1093/ajcn/77.4.803).
    

  

187. Samieri, Cécilia, et al. “Low Plasma Eicosapentaenoic Acid and Depressive Symptomatology Are Independent Predictors of Dementia Risk.” The American Journal of Clinical Nutrition, vol. 88, no. 3, Sept. 2008, pp. 714–21. PubMed, [https://doi.org/10.1093/ajcn/88.3.714](https://doi.org/10.1093/ajcn/88.3.714).
    

  

188. Okereke, Olivia I., et al. “Dietary Fat Types and 4-Year Cognitive Change in Community-Dwelling Older Women.” Annals of Neurology, vol. 72, no. 1, July 2012, pp. 124–34. PubMed, [https://doi.org/10.1002/ana.23593](https://doi.org/10.1002/ana.23593).
    

  

189. Solfrizzi, Vincenzo, et al. “Dietary Fatty Acids Intakes and Rate of Mild Cognitive Impairment. The Italian Longitudinal Study on Aging.” Experimental Gerontology, vol. 41, no. 6, June 2006, pp. 619–27. PubMed, [https://doi.org/10.1016/j.exger.2006.03.017](https://doi.org/10.1016/j.exger.2006.03.017).
    

  

190. Roberts, Rosebud O., et al. “Relative Intake of Macronutrients Impacts Risk of Mild Cognitive Impairment or Dementia.” Journal of Alzheimer’s Disease: JAD, vol. 32, no. 2, 2012, pp. 329–39. PubMed, [https://doi.org/10.3233/JAD-2012-120862](https://doi.org/10.3233/JAD-2012-120862).
    

  

191. Laitinen, M. H., et al. “Fat Intake at Midlife and Risk of Dementia and Alzheimer’s Disease: A Population-Based Study.” Dementia and Geriatric Cognitive Disorders, vol. 22, no. 1, 2006, pp. 99–107. PubMed, [https://doi.org/10.1159/000093478](https://doi.org/10.1159/000093478).
    

  

192. Zietemann, Vera, et al. “Validation of the Telephone Interview of Cognitive Status and Telephone Montreal Cognitive Assessment Against Detailed Cognitive Testing and Clinical Diagnosis of Mild Cognitive Impairment After Stroke.” Stroke, vol. 48, no. 11, Nov. 2017, pp. 2952–57. PubMed, [https://doi.org/10.1161/STROKEAHA.117.017519](https://doi.org/10.1161/STROKEAHA.117.017519).
    

  

193. Cao, G. Y., et al. “Dietary Fat Intake and Cognitive Function among Older Populations: A Systematic Review and Meta-Analysis.” The Journal of Prevention of Alzheimer’s Disease, vol. 6, no. 3, 2019, pp. 204–11. PubMed, [https://doi.org/10.14283/jpad.2019.9](https://doi.org/10.14283/jpad.2019.9).
    

  

194. Ruan, Yue, et al. “Dietary Fat Intake and Risk of Alzheimer’s Disease and Dementia: A Meta-Analysis of Cohort Studies.” Current Alzheimer Research, vol. 15, no. 9, 2018, pp. 869–76. PubMed, [https://doi.org/10.2174/1567205015666180427142350](https://doi.org/10.2174/1567205015666180427142350).
    

  

195. Junker, R., et al. “Effects of Diets Containing Olive Oil, Sunflower Oil, or Rapeseed Oil on the Hemostatic System.” Thrombosis and Haemostasis, vol. 85, no. 2, Feb. 2001, pp. 280–86.
    

  

196. Freese, Riitta, et al. “No Difference in Platelet Activation or Inflammation Markers after Diets Rich or Poor in Vegetables, Berries and Apple in Healthy Subjects.” European Journal of Nutrition, vol. 43, no. 3, June 2004, pp. 175–82. PubMed, [https://doi.org/10.1007/s00394-004-0456-4](https://doi.org/10.1007/s00394-004-0456-4).
    

  

197. Vafeiadou, Katerina, et al. “Replacement of Saturated with Unsaturated Fats Had No Impact on Vascular Function but Beneficial Effects on Lipid Biomarkers, E-Selectin, and Blood Pressure: Results from the Randomized, Controlled Dietary Intervention and VAScular Function (DIVAS) Study.” The American Journal of Clinical Nutrition, vol. 102, no. 1, July 2015, pp. 40–48. PubMed, [https://doi.org/10.3945/ajcn.114.097089](https://doi.org/10.3945/ajcn.114.097089).
    

  

198. Iggman, David, et al. “Role of Dietary Fats in Modulating Cardiometabolic Risk during Moderate Weight Gain: A Randomized Double-Blind Overfeeding Trial (LIPOGAIN Study).” Journal of the American Heart Association, vol. 3, no. 5, Oct. 2014, p. e001095. PubMed, [https://doi.org/10.1161/JAHA.114.001095](https://doi.org/10.1161/JAHA.114.001095).
    

  

199. Hozo, Stela Pudar, et al. “Estimating the Mean and Variance from the Median, Range, and the Size of a Sample.” BMC Medical Research Methodology, vol. 5, Apr. 2005, p. 13. PubMed, [https://doi.org/10.1186/1471-2288-5-13](https://doi.org/10.1186/1471-2288-5-13).
    

  

200. Freese, R., et al. “No Effect on Oxidative Stress Biomarkers by Modified Intakes of Polyunsaturated Fatty Acids or Vegetables and Fruit.” European Journal of Clinical Nutrition, vol. 62, no. 9, Sept. 2008, pp. 1151–53. PubMed, [https://doi.org/10.1038/sj.ejcn.1602865](https://doi.org/10.1038/sj.ejcn.1602865).
    

  

201. Jenkinson, A., et al. “Dietary Intakes of Polyunsaturated Fatty Acids and Indices of Oxidative Stress in Human Volunteers.” European Journal of Clinical Nutrition, vol. 53, no. 7, July 1999, pp. 523–28. PubMed, [https://doi.org/10.1038/sj.ejcn.1600783](https://doi.org/10.1038/sj.ejcn.1600783).
    

  

202. de Kok, T. M. C. M., et al. “Analysis of Oxidative DNA Damage after Human Dietary Supplementation with Linoleic Acid.” Food and Chemical Toxicology: An International Journal Published for the British Industrial Biological Research Association, vol. 41, no. 3, Mar. 2003, pp. 351–58. PubMed, [https://doi.org/10.1016/s0278-6915(02)00237-5](https://doi.org/10.1016/s0278-6915(02)00237-5).
    

  

203. Södergren, E., et al. “A Diet Containing Rapeseed Oil-Based Fats Does Not Increase Lipid Peroxidation in Humans When Compared to a Diet Rich in Saturated Fatty Acids.” European Journal of Clinical Nutrition, vol. 55, no. 11, Nov. 2001, pp. 922–31. PubMed, [https://doi.org/10.1038/sj.ejcn.1601246](https://doi.org/10.1038/sj.ejcn.1601246).
    

  

204. Parfitt, V. J., et al. “Effects of High Monounsaturated and Polyunsaturated Fat Diets on Plasma Lipoproteins and Lipid Peroxidation in Type 2 Diabetes Mellitus.” Diabetic Medicine: A Journal of the British Diabetic Association, vol. 11, no. 1, Feb. 1994, pp. 85–91. PubMed, [https://doi.org/10.1111/j.1464-5491.1994.tb00235.x](https://doi.org/10.1111/j.1464-5491.1994.tb00235.x).