We've all heard that LDL causes heart disease, and it is actually true. But why? The short answer is that evolution sucks at selecting out physiological traits that are detrimental to our health if they occur after reproductive age. The long answer is quite a bit more complicated.

Essentially, each LDL particle in your bloodstream has on it a protein called apolipoprotein beta-100— ApoB for short. This ApoB protein is made up on amino acids in a particular sequence, because it needs to be able to bind to LDL receptors in various tissues to be pulled out of circulation and used. But, there is a downside. ApoB is built in such a way that it has a similar binding affinity for other structures in the body as well, not just the LDL receptor. 

In the artery, we have an endothelium, which is a thin layer of cells that separate the arterial lumen from the sub-endothelial space. These endothelial cells don't always fit tightly together, so many objects in our blood have the capacity to penetrate the endothelium and enter the sub-endothelial space. This isn't actually a problem in and of itself. All sorts of objects and molecules flow in and out of that space all day through small cracks in our endothelium—  not a big deal. However, ApoB has a unique affinity for structures in the sub-endothelial space called proteoglycans [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC508856/). 

Proteoglycans are long hair-like structures that line the subendothelial space. ApoB uniquely binds to these structures. ApoB binding to proteoglycans serves no meaningful physiological function, but is the initiating event that causes atherosclerosis [2](https://www.ncbi.nlm.nih.gov/pubmed/22176921). We know this because there have been mouse experiments wherein ApoB has been modified such that it doesn't bind to proteoglycans, but it still binds the LDL receptor.

![[Pasted image 20221123152557.png]]

The mice with modified ApoB get much, much less atherosclerosis [3](https://www.ncbi.nlm.nih.gov/pubmed/12066187)[4](https://www.ncbi.nlm.nih.gov/pubmed/14726411). You can drive their LDL sky high and virtually nothing happens. In the end it all comes down to the absolute ApoB concentration in the blood.

**Key points:**

-   Native ApoB is necessary to initiate atherosclerosis.
-   Modifying ApoB such that it doesn't bind to intimal proteoglycans inhibits the development of atherosclerosis.
-   Get your ApoB checked.

**References:**

[1] J Borén, et al. Identification of the principal proteoglycan-binding site in LDL. A single-point mutation in apo-B100 severely affects proteoglycan interaction without affecting LDL receptor binding. J Clin Invest. 1998 Jun. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC508856/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC508856/) 

[2] Fogelstrand P and Borén J.  Retention of atherogenic lipoproteins in the artery wall and its role in atherogenesis. Nutr Metab Cardiovasc Dis. 2012 Jan. [https://www.ncbi.nlm.nih.gov/pubmed/22176921](https://www.ncbi.nlm.nih.gov/pubmed/22176921) 

[3] Skålén K, et al. Subendothelial retention of atherogenic lipoproteins in early atherosclerosis. Nature. 2002 Jun 13. [https://www.ncbi.nlm.nih.gov/pubmed/12066187](https://www.ncbi.nlm.nih.gov/pubmed/12066187) 

[4] Flood C, et al. Molecular mechanism for changes in proteoglycan binding on compositional changes of the core and the surface of low-density lipoprotein-containing human apolipoprotein B100. Arterioscler Thromb Vasc Biol. 2004 Mar. [https://www.ncbi.nlm.nih.gov/pubmed/14726411](https://www.ncbi.nlm.nih.gov/pubmed/14726411)

#patreon_articles 
#disease 
#LDL 
#ApoB 
#proteoglycans 
#LDL_retention 