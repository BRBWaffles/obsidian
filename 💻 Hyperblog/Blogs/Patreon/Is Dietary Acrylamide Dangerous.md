This article is section that was ultimately cut from my blog article, [A Comprehensive Rebuttal to Seed Oil Sophistry](https://www.the-nutrivore.com/post/a-comprehensive-rebuttal-to-seed-oil-sophistry). It was necessary to cut it out in order to save space, but it will live on as a Patreon exclusive until I'm finished writing the book. Enjoy!  
  
Not all speculation regarding the supposed health harms of vegetable oils revolve around the passive peroxidation of linoleic acid (LA) in human tissues. There are also hypotheses specific to certain byproducts produced from certain high heat cooking methods that often involve vegetable oils. Acrylamide (ACR) is one such compound that is formed from the Maillard reaction, which is responsible for the browning of food and occurs between the temperatures of 120-140°C.

Technically, ACR is not formed from the oils themselves, but rather from sugars and amino acids during heating. It just so happens that vegetable oils serve as a common medium for cooking and can uniquely retain higher levels of acrylamide after cooking [1](https://pubmed.ncbi.nlm.nih.gov/27374529/).

![[_0PYseBzWVeP8wLMMxgdUegJ_1o0FP6-il42QgkIpGAS-2MokM_geBROUmXSJ9KzQ4pZ7cpSVpzB8wcfIuZRzjWv3WAnuIFBgTHp.png]]

However, a food merely having a higher amount of a spooky compound doesn’t necessarily make it dangerous to consume that food. While it is true that ACR has shown to be carcinogenic in rodents, it is also the case that rodents and humans digest and metabolize dietary ACR differently [2](https://pubmed.ncbi.nlm.nih.gov/16492914/)][3](https://pubmed.ncbi.nlm.nih.gov/19166901/). The rate of excretion of ACR detoxification metabolites is much higher in humans than it is in rodents, which suggests that the total area under the curve exposure to ACR and its toxic metabolites (like glycidamide) are also much lower in humans than in rodents. As such, it is likely that the carcinogenicity of ACR is also attenuated in humans.

In an exploratory analysis of the Women’s Health Initiative conducted by Sun et al. (2019), it was found that when comparing weekly intake of fried foods to zero intake, there was no statistically significant increase in cancer mortality after adjustment for a number of confounding variables, including diet quality [4](https://pubmed.ncbi.nlm.nih.gov/30674467/).

![[BSXbgLjZodsPMQ8QwwoO1igq9hVgvcvYrJ0_wokx4qek_Nf9MAen_vlHNkBOcTYjfZ__KLdCHhHlOjspZVH-fUGaecCKLpbVws0R.png]]

Personally, I’d like to see better exposure contrasts than this. But it’s some of the only prospective data we have on this specific question. Additionally, there is an analysis of another prospective cohort study showing an increased risk of gastric cancer with increasing fried food consumption from less than twice per week to greater than twice per week [5](https://pubmed.ncbi.nlm.nih.gov/29429272/).

However, referring back to the analysis of the LA Veterans trial from Dayton et al. (1969), the intervention group did not see a statistically significant increase in the risk of digestive cancers despite the fact that heated vegetable oils were liberally included in their diets [6](https://pubmed.ncbi.nlm.nih.gov/4100347/).

_“Vegetable oils were incorporated into the experimental diet in the form of filled milk,* imitation ice cream, "unsaturated" margarine, special sausage products, and filled cheeses. Vegetable oils were used liberally in cooking and baking. Meat fat was minimized by the use of specially trimmed lean cuts. Further dietetic details are given in a separate publication._

_*Filled milk is ordinary fresh milk from which butterfat has been removed and replaced by another fat, in this instance soybean or safflower oil.”_

It may be useful to just cut to the chase and investigate the relationship between cancer and dietary ACR itself. Luckily, there is a fairly recent meta-analysis by Pelucchi et al. (2015) that investigated the association between dietary ACR and eight different cancer subtypes [7](https://pubmed.ncbi.nlm.nih.gov/25403648/). The meta-analysis only found a non-significant increase in the risk of kidney cancer with increasing dietary ARC.

However, a later meta-analysis of prospective cohort studies by Jiang et al. (2020) explored this particular relationship in greater depth and discovered no increase in renal cell carcinoma risk with increasing dietary ACR [8](https://pubmed.ncbi.nlm.nih.gov/32077494/).

![[0qoul9ESAnkpaP_s_mm0LYUcTL7MgDjE30RIlvc3z2RnAbs7tVwsz4l_ODCm0JTNxd4vd5hjbZVTrvKOOTijk3pR-yUKhWb7vB3z.png]]

Additionally, Adani et al. (2020) also produced a number of dose response curves investigating the relationship between ACR and the risk of breast, endometrial, and ovarian cancer [9](https://pubmed.ncbi.nlm.nih.gov/32169997/). Not only did their analysis show no increased risk of any of the investigated cancers with increasing ACR exposure, they actually found an inverse association between ACR exposure and the risk of breast cancer.

It could also be worthwhile to explore some other disease endpoints. In a systematic review by Sayon-Orea et al. (2015), the association between heated oils and a number of disease outcomes was thoroughly investigated [10](https://pubmed.ncbi.nlm.nih.gov/26148920/). Overall, their analysis found that even when highly unsaturated oils were used, there was a consistently lower risk of disease when using PUFA-rich cooking oils as opposed to SFA-rich cooking oils.  

![[CwjedKwEdHg9L6O6RYYHxE7bSVxt5bsqOxCQjZDIlOrDmifKgk-lmX6NhH9xeB32a7wSbJOlknoBngnR5n_uU-DKdRvjvm1zM7Ju.png]]

Paying attention to the annotations below the forest plot, we see from Rastogi et al. (2004) that even heated SU lowers the risk of cardiovascular disease (CVD), despite being exceedingly high in ACR [11](https://pubmed.ncbi.nlm.nih.gov/15051601/). We can also see from Kabagambe et al. (2005) that when heated palm oil (PO) is primarily replacing heated SO, we see a statistically significant increase in the risk of CVD [12](https://pubmed.ncbi.nlm.nih.gov/16251629/). This being in spite of the fact that heated SO can have up to 70% more ACR compared to PO.

We also see inconsistent findings with fried foods. These foods seem to increase the risk of type 2 diabetes mellitus (T2DM) and obesity, but not CVD. However, we might expect this to be the case, as many fried foods are hyperpalatable, and statistical adjustment for total energy intake may not be sufficient to fully capture potential confounding due to overconsumption [13](https://pubmed.ncbi.nlm.nih.gov/15251058/)[14](https://pubmed.ncbi.nlm.nih.gov/12571660/). As such, it may not be possible to explore this relationship robustly.

To better explore the relationship between ACR and CVD, it could be interesting to explore the association between fried potato products and disease risk. This is because fried potatoes have some of the highest ACR levels of common foods [15](https://pubmed.ncbi.nlm.nih.gov/33338370/). Fried potatoes are also one of the largest sources of dietary ACR in most countries [16](https://pubmed.ncbi.nlm.nih.gov/16708866/).

I was unable to find any decent data on potato chip consumption and disease outcomes, but I was able to find data on french fries [17](https://pubmed.ncbi.nlm.nih.gov/27680993/). In this study of two prospective cohorts by Larsson and Wolk (2016), it was observed that daily consumption of either french fries or fried potatoes did not increase the risk of any CVD-related endpoint when compared to weekly consumption.

Altogether, the case for dietary ACR increasing the risk of any particular disease is weak at best, and is null more often than not. There does not seem to be a robust evidential basis for the suggestion that heated vegetable oils increase the risk of cancer in particular either.

There is, however, evidence that despite the load of dietary ACR, vegetable oils continue to be consistently inversely associated with many diseases. Not to mention the fact that if the ACR content of vegetable oils was really such a danger, we would not expect to see the strong inverse associations between heated vegetable oils and disease risk.

**Key points:**

-   Humans seem to detoxify acrylamide very rapidly.
-   Acrylamide is not significantly associated with an increased risk of cancer.
-   The benefits of vegetable oils seem to largely survive cooking/heating.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/27374529/](https://pubmed.ncbi.nlm.nih.gov/27374529/)

[2] [https://pubmed.ncbi.nlm.nih.gov/16492914/](https://pubmed.ncbi.nlm.nih.gov/16492914/)

[3] [https://pubmed.ncbi.nlm.nih.gov/19166901/](https://pubmed.ncbi.nlm.nih.gov/19166901/)

[4] [https://pubmed.ncbi.nlm.nih.gov/30674467/](https://pubmed.ncbi.nlm.nih.gov/30674467/)

[5] [https://pubmed.ncbi.nlm.nih.gov/29429272/](https://pubmed.ncbi.nlm.nih.gov/29429272/)

[6] [https://pubmed.ncbi.nlm.nih.gov/4100347/](https://pubmed.ncbi.nlm.nih.gov/4100347/) 

[7] [https://pubmed.ncbi.nlm.nih.gov/25403648/](https://pubmed.ncbi.nlm.nih.gov/25403648/)

[8] [https://pubmed.ncbi.nlm.nih.gov/32077494/](https://pubmed.ncbi.nlm.nih.gov/32077494/)

[9] [https://pubmed.ncbi.nlm.nih.gov/32169997/](https://pubmed.ncbi.nlm.nih.gov/32169997/)

[10] [https://pubmed.ncbi.nlm.nih.gov/26148920/](https://pubmed.ncbi.nlm.nih.gov/26148920/)

[11] [https://pubmed.ncbi.nlm.nih.gov/15051601/](https://pubmed.ncbi.nlm.nih.gov/15051601/)

[12] [https://pubmed.ncbi.nlm.nih.gov/16251629/](https://pubmed.ncbi.nlm.nih.gov/16251629/)

[13] [https://pubmed.ncbi.nlm.nih.gov/15251058/](https://pubmed.ncbi.nlm.nih.gov/15251058/)

[14] [https://pubmed.ncbi.nlm.nih.gov/12571660/](https://pubmed.ncbi.nlm.nih.gov/12571660/)

[15] [https://pubmed.ncbi.nlm.nih.gov/33338370/](https://pubmed.ncbi.nlm.nih.gov/33338370/)

[16] [https://pubmed.ncbi.nlm.nih.gov/16708866/](https://pubmed.ncbi.nlm.nih.gov/16708866/)

[17] [https://pubmed.ncbi.nlm.nih.gov/27680993/](https://pubmed.ncbi.nlm.nih.gov/27680993/)

#patreon_articles 
#nutrition 
#acrylamide
#vegetable_oil 
#fried_foods 
#disease 
#type_2_diabetes 
#cardiovascular_disease 
#obesity 