My decision to [meta-analyze](https://www.patreon.com/posts/olive-oil-and-39379256) olive oil and cardiovascular disease (CVD) was prompted by a few exchanges I had with a few no-oil vegans. For those who are unaware, there is a flavour of veganism that espouses the complete avoidance of all oils on the basis that oils, regardless of the variety, increase the risk of cardiovascular disease. However, if this narrative was true, my meta-analysis likely would not have found a dose-dependent decrease in CVD risk with higher and higher olive oil intakes.

The official dietary recommendations are to replace saturated fat (SFA) with unsaturated (UFA) oils. But no-oil vegans take this a step further and suggest that we should avoid all added fats and perhaps even nuts and seeds. They recommend that we consume whole food carbohydrates (WFC) instead. However, WFCs are not always found to be as effective for CVD prevention as UFAs [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4593072/).

![[Pasted image 20221123152759.png]]

There is some evidence that replacing dairy fats with WFC actually decreases CVD risk more than UFA oils [2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5081717/). However, that same analysis also divulged that UFA oils protect against stroke to a greater degree than WFC.

![[Pasted image 20221123152803.png]]

There are some issues with this analysis. The primary issue being that all dairy fat is lumped together rather than delineated by type. It is well understood that butter increases CVD risk, whereas cheese does not. A proposed mechanism for this effect is the presence of the milk fat globule membrane in whole dairy foods [3](https://pubmed.ncbi.nlm.nih.gov/26016870/). Homogenization, churning, or any other process that breaks down this membrane seems to predispose the food to perturbing blood lipids.

For this reason I conducted my own meta-analysis of cheese intake and CVD risk, just for fun. 

![[Pasted image 20221123152812.png]]

It appears as though eating more cheese is preferable to less cheese. While it is true that the results for butter and CVD can come up null [4](https://pubmed.ncbi.nlm.nih.gov/27355649/). If you look into the included cohorts, it is revealed that even the lowest butter intakes are typically occurring in the context of high SFA intakes anyway. 

However, cheese intake in my included cohorts is also typically occurring in the context of high SFA intakes. However, the risk reductions persist. Suggesting that the food is beneficial despite its SFA content.

But, back to the two previously mentioned papers. In one paper, we see that replacing SFA with UFAs such as monounsaturated fats (MUFA), and especially polyunsaturated fats (PUFA), decrease CVD risk more than WFC. In the other paper, we also see that PUFA decreases risk of stroke more than WFC, and has nearly the same magnitude of effect at reducing CVD risk as WFC as well. Are we really going to break balls over a 4% difference in risk reduction? I wouldn't.

In conclusion, the takeaway message is clear. **EAT BOTH**. Eat WFCs _and_ UFAs to potentially achieve a maximal benefit. Lastly, as a relevant caveat, it's also probably not rational to fear cheese on the basis of its SFA content either. So, hell, eat all three!

**Key points:**

-   There is a group of vegans who espouse complete abstinence from all added oils.
-   However, unsaturated oils reduce heart disease more than whole grains when compared to saturated fats.
-   Unsaturated oils also reduce stroke more than whole grains when compared to dairy fat.
-   As a caveat, cheese likely reduces heart disease regardless of its saturated fat content.
-   Eat whole grains, unsaturated oils, and cheese to confer greater reductions in heart disease risk.

**References:**

[1] Yanping Li, et al. Saturated Fat as Compared With Unsaturated Fats and Sources of Carbohydrates in Relation to Risk of Coronary Heart Disease: A Prospective Cohort Study. J Am Coll Cardiol. 2015 Oct. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4593072/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4593072/)

[2] Mu Chen, et al. Dairy fat and risk of cardiovascular disease in 3 cohorts of US adults. Am J Clin Nutr. 2016 Nov. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5081717/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5081717/) 

[3] Fredrik Rosqvist, et al. Potential role of milk fat globule membrane in modulating plasma lipoproteins, gene expression, and cholesterol metabolism in humans: a randomized study. Am J Clin Nutr. 2015 July. [https://pubmed.ncbi.nlm.nih.gov/26016870/](https://pubmed.ncbi.nlm.nih.gov/26016870/) 

[4] Laura Pimpin, et al. Is Butter Back? A Systematic Review and Meta-Analysis of Butter Consumption and Risk of Cardiovascular Disease, Diabetes, and Total Mortality. PLoS One. 2016 June. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4927102/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4927102/)

#patreon_articles 
#nutrition 
#vegetable_oil 
#whole_foods 
#whole_grains 
#nuts 
#clownery 
#disease 
