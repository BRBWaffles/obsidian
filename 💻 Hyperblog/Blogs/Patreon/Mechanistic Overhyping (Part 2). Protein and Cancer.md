Next on the chopping block is a YouTuber named Gojiman. He appears to be a poop-obsessed nutrition science PhD candidate who thinks his credentials imbue him with absolute authority on any topic related to nutrition. Suffice it to say, he's bit of a wanker and a major pseudoscience peddler.

In a video he published to YouTube last year, he describes how he will never consume a high protein diet because of how higher protein diets stimulate a hormone called insulin-like growth factor 1 (IGF-1). He believes the consumption of protein will increase IGF-1 levels in the body to dangerous levels and lead to cancer. Seems straight forward. But is it true?

Well, first off let's look at the human outcome data on dietary protein and the general risk of cancer [1](https://www.bmj.com/content/370/bmj.m2412). As we can see from the dose response graphs below, no source of protein was correlated with cancer risk to a statistically significant degree. 

![[Pasted image 20221123150240.png]]

Lower intakes of both total protein and plant protein were both associated with an increased risk for all-cause and cardiovascular disease mortality. Modest intakes of animal protein were inversely associated with all-cause and cardiovascular disease mortality, and positively associated at higher levels. However, it is plausible this could be confounded by higher saturated fat intakes with increasing animal protein intakes.

There is no clear association between total, animal, or plant proteins and the risk of cancer. In fact, higher intakes of plant protein trend toward a reduction in risk. If the goalpost is protein, why aren't all protein sources correlated? Why don't any of them reach statistical significance? A legitimate criticism would be that these are reflecting mortality rather than total incidence. Fair. But I've yet to see any meta-analyses which include dose-response curves for dietary protein and cancer incidence, rather than mortality.

But, on to the basis for his claim. The evidence he uses to support his claim is a mendelian randomization study that showed that higher, genetically mediated levels of IGF-1 associated with an increased risk of colorectal cancer. It's odd that he uses this to extrapolate effects out to all cancers. But let's just use colorectal cancer as the goalpost. A meta-analysis was conducted to ascertain the effects of total protein intake on the risk of colorectal cancer in particular [2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5591555/).

![[Pasted image 20221123150247.png]]

Although they could not perform a dose-response analysis, they did perform a bunch of subgroup analyses, including gender, protein type, study design, geography, and cancer type. No statistically significant correlations were found in any of the data.

But let's wrap this up by looking at this mendelian randomization study. The first quintile to strongly and consistently associate with risk was quintile five. The average circulating IGF-1 levels in this subgroup was 25.8nmol/L. This translates to about 197.3ng/mL. This is well above the median reference range for IGF-1 in at-risk age groups. So how applicable is this to diet? 

I managed to find a single study in the relevant age categories investigating the relationship between IFG-1 and protein intake [3](https://academic.oup.com/ajcn/article/81/5/1163/4649659). The highest levels of protein intake yielded a maximum IGF-1 level of 173ng/mL. Not quite up to our reliable risk-generating 197.3ng/mL. 

In conclusion, can we say that high protein diets cause cancer? Probably not. I'm not sure the data is there to necessarily support that. Certainly there are dietary sources of protein that associate strongly with cancer, but that's not the same as protein itself. I think it is plausible that in those who are sufficiently genetically predisposed, higher protein intakes may add to the total pool of risk for certain cancers perhaps. But I don't think the data supports a direct link between total dietary protein and total cancer.

**Key points:**

-   A cocky coprophilic YouTuber named Gojiman claims that dietary protein causes cancer through increased IGF-1.
-   His supporting evidence is a mendelian randomization study which shows that enormous levels of IGF-1 that seem potentially unattainable through diet alone.
-   Population data does not divulge an association between total protein and total cancer.
-   Perhaps if one is genetically predisposed to high IGF-1 levels, the highest levels of protein intake could add to one's total risk.

**References:**

[1] Naghshi, et al. Dietary intake of total, animal, and plant proteins and risk of all cause, cardiovascular, and cancer mortality: systematic review and dose-response meta-analysis of prospective cohort studies. BMJ 2020.  [https://www.bmj.com/content/370/bmj.m2412](https://www.bmj.com/content/370/bmj.m2412) 

[2] Renxu Lai, et al. The association between dietary protein intake and colorectal cancer risk: a meta-analysis. World J Surg Oncol. 2017. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5591555/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5591555/) 

[3] Susanna C Larsson, et al. Association of diet with serum insulin-like growth factor I in middle-aged and elderly men. Am J Clin Nutr. 2005 May. [https://academic.oup.com/ajcn/article/81/5/1163/4649659](https://academic.oup.com/ajcn/article/81/5/1163/4649659)

#patreon_articles 
#nutrition 
#disease 
#protein 
#cancer 
