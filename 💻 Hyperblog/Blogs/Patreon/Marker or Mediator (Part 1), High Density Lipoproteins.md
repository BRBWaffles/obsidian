On the back of my recent [article](https://www.patreon.com/posts/does-keto-heart-47567091) detailing the power of the triglyceride (TG) to high density lipoprotein cholesterol (HDL) ratio with regards to predicting cardiovascular disease (CVD) risk, I figured I'd take this opportunity to start a new series. In this new series, I will be discussing a number of different biomarkers and their relationship to associated disease states. 

Sometimes biomarkers are just disease correlates, and contribute nothing to disease risk in and of themselves. Other times, these biomarkers directly contribute causally to the development of a particular disease state. First up we have HDL. Is HDL a marker or a mediator? Let's find out.

The association between elevated HDL and lower CVD rates dates all the way back to the Framingham Heart Study [1](https://pubmed.ncbi.nlm.nih.gov/3196218/). It was observed that HDL seemed to have particular predictive power with regards to coronary heart disease (CHD). Having low HDL seemed to dramatically increase the risk of CVD and CHD. However, the association was much stronger with CHD.

![[Pasted image 20221123155244.png]]

At this point, some will astutely point out that correlation does not equal causation. There have since been a number of investigations into HDL's role in the prevention of CVD, using much more sensitive methodology. This typically comes in the form of Mendelian randomization (MR) studies. 

MR is a type of epidemiology that investigates the relationship between genes and health outcomes. MR studies are typically more robust than prospective cohort studies, but less robust than randomized controlled trials. 

The idea is based on an assumption that genes are randomly distributed in the population, and not everyone has all of the gene variants you may be interested in. So, not only do you get likely get a cleanly randomized group to observe, you also get a control group to which you can compare. This is a very elegant form of epidemiology.

In the case of HDL, we're observing people with genes that specifically modulate HDL up or down, and seeing how that affects the risk of CVD. This gives us the ability to make stronger causal inferences, because these HDL markers are genetically mediated and less vulnerable to residual confounding after adjustments are made.

When we investigate the relationships between HDL and CVD through the lens of MR, we see that the association completely dissolves [2](https://pubmed.ncbi.nlm.nih.gov/32203549/)[3](https://pubmed.ncbi.nlm.nih.gov/32113648/).

![[Pasted image 20221123155250.png]]

![[Pasted image 20221123155253.png]]

In blue we see the association between HDL and CVD in epidemiology, and in red we see the association as it is divulged using MR. As we can see, despite the powerfully protective effect HDL initially appeared to have using traditional methods of epidemiological investigation, the results of the MR studies would seem to contradict it. The results would seem to indicate that risk is actually following apolipoprotein B more than anything, as I discussed in the last article.

In conclusion, HDL in and of itself likely has very little independent causal role in the development of, or protection against, CVD.

**Key points:**

-   Higher HDL has been associated with lower risk of CVD in observational literature.
-   However, more robust Mendelian randomization studies divulge no causal, protective link between HDL and CVD.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/3196218/](https://pubmed.ncbi.nlm.nih.gov/3196218/)

[2] [https://pubmed.ncbi.nlm.nih.gov/32203549/](https://pubmed.ncbi.nlm.nih.gov/32203549/)

[3] [https://pubmed.ncbi.nlm.nih.gov/32113648/](https://pubmed.ncbi.nlm.nih.gov/32113648/)

#patreon_articles 
#disease 
#HDL 
#cardiovascular_disease 
#LDL 
#ApoB 
