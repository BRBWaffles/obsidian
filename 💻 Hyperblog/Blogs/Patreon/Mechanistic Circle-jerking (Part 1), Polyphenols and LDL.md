I don't spend a lot of time speculating about mechanisms and how they might relate to human health or disease outcomes, but I felt compelled to write this one. A while ago I compiled a [meta-analysis](https://www.patreon.com/posts/olive-oil-and-39379256) investigating the relationship between olive oil consumption and cardiovascular disease (CVD) in prospective cohort studies. The analysis showed a 24% reduction in CVD risk and a 31% reduction in stroke risk with increasing olive oil consumption.

Recently I was asked whether or not canola oil would be better than extra virgin olive oil (EVOO) for CVD-related outcomes in humans. It's actually difficult to say. Canola oil is higher in polyunsaturated fat (PUFA), which has the reliable effect of reducing CVD risk in humans. But EVOO seems to do the same, despite the fact that it has a pretty neutral effect on low density lipoproteins (LDL) in humans. LDL is by far the best risk predictor we have for CVD beyond age. So, if olive oil doesn't reliably lower LDL, but it lowers CVD, how might it do this?

Firstly, according to [Phenol Explorer](http://phenol-explorer.eu/), EVOO is much, much higher in polyphenols than canola oil.

**EVOO:**

![[1-41.png]]

**Canola oil:**

![[Pasted image 20221123155814.png]]

In a previous [article](https://www.patreon.com/posts/why-does-ldl-33915357) that I wrote almost a year ago, I explain the mechanism by which LDL causes CVD. They bind to structures called proteoglycans in the subendothelial space. Once bound, the LDL oxidize and are taken up by immune cells. Eventually, the immune cells get overwhelmed, die, and the result is an atherosclerotic lesion.

Polyphenols appear to have many effects on LDL in particular. These effects could help explain the cardio-protective effects of EVOO despite the fact that EVOO does not reduce the total concentration of LDL. I suspect that EVOO exerts its effects on reducing CVD risk by modifying LDL _behaviour_, rather than LDL concentration.

In a human experiment investigating the effects of polyphenols from pomegranate juice on LDL behaviour, up to 60% of the subjects saw reductions in the binding of LDL to proteoglycans during ex-vivo testing [1](https://pubmed.ncbi.nlm.nih.gov/10799367/). The aggregated effect was null, but researchers noticed that the cohort could be divided into responders and non-responders. Meaning that some people got a reliable benefit of the pomegranate juice polyphenols, while others did not. Perhaps this could relate to people coming into the study with different baseline levels of polyphenols already. Who knows.

![[1-40.png]]

It has also been demonstrated that certain polyphenols can bind to the apolipoprotein-B moiety of the LDL particle itself, and thus favourably alter the LDL's behaviour [2](https://www.sciencedirect.com/science/article/abs/pii/S0003986120305981?via%3Dihub). Among the polyphenols studied, some originate from olive oil and can protect LDL from oxidation. So, it could be the case that polyphenols from olive oil could not only attenuate proteoglycan-binding by LDL, they may also protect the LDL from oxidation. 

This is potentially beneficial for two reasons. Firstly, if fewer LDL bind to proteoglycans, there is a reduced chance that those LDL will oxidize and thus contribute to foam cell formation and plaque buildup. Secondly, if an LDL particle does become bound to a proteoglycan, increasing the lag-time to oxidation increases the chances that the LDL particle could dissociate from the proteoglycan and return to circulation.

**Key points:**

-   Olive oil is associated with reductions in heart disease despite the fact that it does not seem to have a reliable effect on atherogenic lipoprotein concentration.
-   The polyphenols in foods like olive oil could modify lipoprotein behaviour, rather than concentration, such that the lipoproteins themselves become _less_ atherogenic.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/10799367/](https://pubmed.ncbi.nlm.nih.gov/10799367/)

[2] [https://www.sciencedirect.com/science/article/abs/pii/S0003986120305981?via%3Dihub](https://www.sciencedirect.com/science/article/abs/pii/S0003986120305981?via%3Dihub)

#patreon_articles 
#nutrition 
#LDL 
#olive_oil 
#polyphenols 
#oxLDL 
