Human beings eat according to heuristics, meaning that we apply principles to our decision-making regarding food consumption. Some people eat merely to satisfy hunger, or they eat for pleasure. Some people merely avoid animal foods when they eat. Others eat anything as long as it low in saturated fat. These days, a great many people eat to lose weight, and that is the focal point for a lot of dietary controversy on the internet. Many principles are heralded as panaceas for weight loss.

Low-carb fanatics will tell us that sugar and insulin make us fat, so avoiding carbohydrates necessarily leads to weight loss. Vegans will tell us that animal products make us fat, and that avoiding animal products necessarily leads to weight loss. Ray Peat acolytes will have us believing that polyunsaturated fats make us fat. People who are really into the gut microbiome will sometimes try to convince us that diets high in saturated fat will spill endotoxin into our blood and cause us to gain weight. Wow— so many different opinions. Pretty much all of them are bullshit, and at the end of the day, calories drive weight gain or weight loss.

It is true, however, that employing any one of these strategies will usually guide one to significant weight loss. But why? Let's explore the foods that are statistically most likely to lead to weight gain. According to the Dietary Guidelines Advisory Committee, these are the foods from which Americans derive a majority of their calories:

-   Grain-based desserts
-   Yeast breads
-   Chicken and chicken-mixed dishes
-   Soda, energy drinks, and sports drinks
-   Pizza
-   Alcoholic beverages
-   Pasta and pasta dishes
-   Mexican mixed dishes
-   Beef and beef-mixed dishes
-   Dairy desserts

Notice anything interesting? Whether we're low-carb, vegan, paleo, or carnivore, almost everything on that list is off-limits. If we're vegan, we must avoid over half of the list. If we're low-carb, we have to avoid more than half of the list as well. If we're paleo, we have to avoid virtually everything on that list except for the meat. Virtually all roads that lead to excluding these calorie-dense foods lead to weight loss as well. I interpret this to mean that if we make a conscious effort to mostly avoid junk food, we'll probably lose weight and have better outcomes. I made a post about this [here](https://www.patreon.com/posts/do-processed-us-31529282).

There's nothing magical about any one of these approaches. They all force us to employ some sort of restraint that causes us to reduce our caloric intake unconsciously (ostensibly by limiting junk foods). What they all have in common is restricting calorie-dense junk food. We shouldn't really be against anybody employing any one of these approaches, because they all more or less work (depending on the individual). Just remember that they all work for the same reason. 

The funny thing is that once we understand how and why these principles work, we can probably include some junk food in our diet at little cost. If we know it boils down to calories we have incredible leeway with what we actually include in our diet. If one wishes to track their caloric intake, it becomes really easy to budget for junk food and maintain your weight, or even lose weight. At the time I'm writing this, I'm six weeks into a cut and I ate a Twix bar today. I'm still under my 1600 kcal cap for today, and I'm still losing weight steadily. The Twix bar contains sugar, saturated fat, animal foods, plant foods, and refined grains. But because I'm controlling calories I don't gain any weight from it, and because my diet is primarily whole foods I have a lot of low-calorie, filling foods to fall back on if I get hungry.

**Key points:**

-   There are many explanations for obesity, but most of them are bullshit.
-   Excess calories from hyper-palatable, Western junk food likely drives obesity.
-   Limiting junk food is something almost all popular weight loss diet have in common.
-   Over-eating is difficult on a diet consisting only of whole foods.

#patreon_articles 
#nutrition 
#disease 
#weight_loss 
#healthy_diets 
#obesity 