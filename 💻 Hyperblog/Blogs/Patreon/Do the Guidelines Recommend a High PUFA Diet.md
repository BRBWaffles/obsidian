This is a pretty common trope that emerges from many different diet camps— particularly from the low carb community. The idea is that the guidelines led us to replace our saturated fats (SFA) with polyunsaturated fats (PUFA), which led to all sorts of poorer health outcomes. I'm not going to explore the health claims today, but I will talk about whether or not the guidelines actually do necessitate eating enormous amounts of PUFA.

I was only able to find a couple definitions for high PUFA diets in the literature. Some studies define high-PUFA diets as diets containing 10-21% of energy from PUFA [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6444462/)[2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7050740/). So, let's take an average of 15.5% of energy. I could definitely agree with this. That can certainly be a lot of fucking PUFA, haha.

To start, I plugged the 2000 calorie [Mediterranean Dietary Pattern](https://health.gov/our-work/food-nutrition/2015-2020-dietary-guidelines/guidelines/appendix-4/) recommended by the [Dietary Guidelines for Americans, 2015-2020](https://health.gov/our-work/food-nutrition/2015-2020-dietary-guidelines/guidelines/), into Cronometer. Some of the dietary recommendations set weekly goals, while others set daily goals. But all together this is what the dietary pattern looks like if intakes are followed to the letter.

![[Pasted image 20221123153829.png]]

To my surprise, the guidelines allow for a decent whack of fat, and a pretty hefty amount of protein as well. The macronutrient breakdown is about 50% carbohydrates, 30% fat, and 20% protein. Not bad at all, really. But what about the PUFA?

![[Pasted image 20221123153832.png]]

The diet provides 18.8g of PUFA per day and 18.8g of SFA per day. Personally, I find it hilarious that the guidelines seem to permit equal quantities of total PUFA and total SFA.

All together this means that PUFA and SFA comprise about 8.4% of energy each. Since the most conservative definition of a high-PUFA diet that I could find in the literature is 10% of energy, this would not seem to be a high PUFA diet.

"But, wait, Nick. You didn't disclose the foods you inputted."

Get fucked, lol. Fine. Here they are:

![[Pasted image 20221123153835.png]]

For added fats, the guidelines give no specific qualitative recommendations other than to replace solid fats with oils. They give a list of oils that are commonly used, but do not say which ones are to be favoured over others. So I included all of the oils they mention toward the allotment of 27g/day that they suggested, but in equal proportions. Seems fair to me.

This is actually just one interpretation of the guidelines. It's probably one of the most fair interpretations, too. But, we can interpret the guidelines differently. If we replace some of the eggs with chicken, replace some of the salmon with shrimp, and replace all of the oils with avocado oil, we get this:

![[Pasted image 20221123153840.png]]

This is perfectly compatible with the guidelines, and actually provides more SFA than PUFA. But, to be fair this isn't the only way to stretch our interpretation of the guidelines. If you tweak everything to maximize the PUFA, you can get this after some trial and error:

![[Pasted image 20221123153844.png]]

In this interpretation, most of the meat is salmon and pork, the nuts and seeds are selected for their PUFA content, and the oil of choice is sunflower oil. All together this gives us 28.8g/day of total PUFA, which amounts to 12.4% of energy. 

By one definition I was able to find, this is a high intake. By another definition it is not. If the average threshold of 15.5% of energy is considered, this highly contrived, worse-case-scenario interpretation of the guidelines doesn't meet the criteria either.

So, in conclusion, if certain people still want to claim that the guidelines yield high PUFA intakes, I guess the people making this claim should just set their goalpost and justify it credibly— what is a high PUFA intake and why is it undesirable? Until they do that, I will maintain that the guidelines do not recommend a diet that is especially high in PUFA.

**Key points:**

-   Some claim that the guidelines suggest a high omega-6/PUFA diet.
-   Following the guidelines as literally as possible, this isn't true.
-   Only absurd interpretations of the guidelines yield high PUFA intakes.

**References:**

[1] [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6444462/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6444462/)

[2] [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7050740/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7050740/)

#patreon_articles 
#nutrition 
#polyunsaturated_fat 
#dietary_guidelines 