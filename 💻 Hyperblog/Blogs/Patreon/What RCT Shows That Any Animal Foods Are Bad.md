This is a question that I've encountered in many debates and discussions that I have had with people who argue for the health value of animal products. When asked to elaborate on why they have formed these beliefs, their answers will generally cash out into an appeal to nature fallacy. I won't be covering why appeal to nature fallacies are bullshit here, though.

Today I'm going to be talking about an actual randomized controlled trial that did substitute non-animal foods for animal foods and did actually show a massive reduction in heart disease risk. This, of course, is the Lyon Diet Heart study (LDHS) [1](https://pubmed.ncbi.nlm.nih.gov/7911176/).

This study has also been used as a launching pad for many motivated claims about the so-called deleterious effects of vegetable oils, as the investigators made a point of reducing the linoleic acid (LA) content of the experimental diet. We'll get into why that's bullshit in a moment.

Firstly, let's talk about the study. This was a moderately sized randomized controlled trial that aimed to investigate the relationship between a Mediterranean dietary pattern and secondary prevention of acute myocardial infarction (AMI). That means the subjects had already had a single AMI at baseline.

One of the primary aims of the trial was to increase the omega-3 content of the experimental diet specially by providing a margarine that was high in alpha-linolenic acid. But, let's check out what else these people were eating.

![[1-96.png]]

The experimental diet differed from the control diet by an extra bite of: bread, legumes, vegetables, and margarine, and one fewer bite of meat, delicatessen, butter, and cream. Even through the differences in daily intake were equal to about a bite per food group, the aggregate of that would be largely equal to substituting one or more plant-based meals per week for one or two animal-based meals per week.

This substitution was commensurate with a 73% decrease in the risk of AMI. The total subject number and event rates were also decently high given the 2.25-year mean follow-up of the trial. So it's probably not likely that we're just seeing the result of poor statistical power here.

![[1-97.png]]

So, the next time somebody asks you for such a study, this is the paper that should come to mind. To my knowledge, this is the only time this particular research question has been investigated in this way.

For those who subscribe to appeal to nature fallacies, what should be most striking about the results is the fact that processed foods actually comprised quite a bit of the substitution for the experimental group. Yet, a significant risk reduction was still observed.

However, for me personally, the most interesting result was that the difference in AMI risk occurred without significant differences in LDL. Which leads me to one more thing before we finish. The researchers actually did make an attempt to reduce LA in the experimental diet.

> _At randomisation, the diet of the experimental group (table 3) was assumed to be that of controls, ie, close to the prudent diet of the American Heart Association (total lipids, 31 % energy; saturated fats, 105%; polyunsaturated/saturated ratio, 078). Eight weeks later, the experimental group had decreased their intake of saturated fat, cholesterol, and_ _**linoleic acid**_ _while increasing that of oleic and alpha-linolenic acid._

This fact has been used by many motivated reasoners to argue that the reduction in LA explains the reduction in events despite no significant differences in LDL. However, considering how many dietary changes there were in this study, to chalk the results up to a difference in linoleic acid intake seems pretty reductionist and silly, in my view.

See, linoleic acid intake was only about 7.7g/day in the experimental group. So if the difference in risk was explained by this difference in LA intake, it would mean that consuming more linoleic acid than this would increase your risk of AMI. But we know from wider research that this is just ridiculous [2](https://pubmed.ncbi.nlm.nih.gov/32428300/)[3](https://pubmed.ncbi.nlm.nih.gov/30488422/)[4](https://pubmed.ncbi.nlm.nih.gov/25161045/). In a systematic review an meta-analysis by Hooper et al. (2018), these were the author's conclusions about LA and AMI:

> _In spite of its limitations, the weak evidence we collected in this review appears to suggest that omega-6 fats are not harmful. There is no evidence for increasing omega-6 fats to reduce cardiovascular outcomes other than myocardial infarction. Although the potential benefit of omega-6 fats in reducing myocardial infarction remains to be proven, increasing omega-6 fats may be of benefit in patients with high risk of myocardial infarction._

It is absurdly reductionist to suggest that the differences in outcomes in the LDHS are attributable to the differences in LA intake. But, to hammer this home, we can actually use the stronger contrary evidence to create a defeater for this position with a pretty hilarious modus tollens:

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> If increasing LA beyond 7.7g/day increases AMI risk, then increasing LA beyond 7.7g/day doesn't lower AMI risk..
<br />
<font color="CC6600">
<b>(P→¬Q)</b>
<br />
<b>P2)</b></font> Increasing LA beyond 7.7g/day lowers AMI risk.
<br />
<font color="CC6600">
<b>(Q)</b>
<br />
<b>C)</b></font> Therefore, increasing LA beyond 7.7g/day doesn't increase AMI risk.
<br />
<font color="CC6600">
<b>(∴¬P)</b>
<br />
<br />
</font>
</div>

In conclusion, it is unlikely that the results on the LDHS are attributable to reductions in LA in the experimental group. It is more likely that the 73% reduction in AMI risk in the experimental group is attributable to replacing animal products with healthier foods.

**Key points:**

-   The only study that broadly investigated the effect of animal food replacement on acute myocardial infarction found a 73% reduction in risk.
-   It has been argued that the reduction in linoleic acid intake was responsible for the effect, but it's extremely unlikely.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/7911176/](https://pubmed.ncbi.nlm.nih.gov/7911176/) 

[2] [https://pubmed.ncbi.nlm.nih.gov/32428300/](https://pubmed.ncbi.nlm.nih.gov/32428300/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/30488422/](https://pubmed.ncbi.nlm.nih.gov/30488422/) 

[4] [https://pubmed.ncbi.nlm.nih.gov/25161045/](https://pubmed.ncbi.nlm.nih.gov/25161045/)

#patreon_articles 
#meat 
#coronary_heart_disease 
#nutrition 