With so much chatter about how processed foods are bad for us, we often forget that this isn't always the case. In some cases the processing of foods actually enhances their healthfulness and quality overall. I will be exploring some examples here.

**Whey protein** isolate is a popular protein supplement and anabolic aid used athletes to help meet their daily protein requirements. Whey is helpful not only because it is high in leucine, the principle amino acid responsible for stimulating muscle protein synthesis, but because the food itself is easy to consume and digest due to the processing itself. Ease of consumption is usually problematic with foods, but in this case we can actually leverage this characteristic to our advantage. Whey protein has been used to augment lean body mass and resist losses of lean body mass in elderly adults [1](https://www.ncbi.nlm.nih.gov/pubmed/22338070)[2](https://www.ncbi.nlm.nih.gov/pubmed/30289425). The elderly often find it difficult to consume adequate protein due to diminished appetite and difficulty chewing, but because of the heavily processed nature of whey protein isolate, they can navigate around this problem.

**Yeast extract** is another heavily processed food that can be made from certain byproducts of beer fermentation. As a liquid it is a salty, somewhat gross-tasting condiment marketed as Marmite in the Europe and Canada, and as Vegemite in Australia. A similar product known as nutritional yeast is also available as dry flakes that taste an awful lot like fish food. Bizarrely enough, researchers have attempted to investigate the effects of supplementing some of these yeast-based products in humans. Marmite supplementation has been shown to affect that balance of excitatory and inhibitory neurotransmitters in the brain, and could potentially have clinical benefits for epilepsy [3](https://www.ncbi.nlm.nih.gov/pubmed/28376309). Brewer's yeast, which is the same species of yeast used to make nutritional yeast, has also been shown to reduce blood pressure and improve blood lipids in human subjects with type II diabetes [4](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3744257/).

**Fruit juices** of all sorts have been studied numerous times for their health-promoting characteristics. Say what you want about sugar or liquid carbs, but orange juice can improve endothelial function, blood lipids, and inflammatory markers in humans [5](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4045306/). Carrot juice can reduce lipid peroxidation cascades in humans [6](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3192732/). Lastly, V8, a type of tomato-based vegetable juice, has the potential to lower blood pressure in pre-hypertensive adults [7](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2949782/). There are plenty of other similar findings related to beet juice, pomegranate juice, and grape juice [8](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5372571/).

**Collagen** comes in two forms. The first being collagen hydrolysate, which is a collagen protein isolate that is made from artificially hydrolyzed collagen, using digestive enzymes or heat-treatment. Gelatin is a similar product, and is offered as a crystalline substance that is extracted from the tendons of animals. However, despite these cold descriptions, consuming collagen in these forms is incredibly safe and seems to have nothing but benefits. Effects of collagen supplementation range from increased tendon strength to improved skin appearance to better wound healing [9](https://www.ncbi.nlm.nih.gov/pubmed/27852613)[10](https://www.ncbi.nlm.nih.gov/pubmed/30681787).

**Key points:**

-   There are certain processed foods that contribute positively to health.
-   Protein isolates such as whey protein can help the elderly meet their protein needs.
-   Yeast extracts are rich in B-vitamins and can improve certain markers of health.
-   Many fruit juices have been shown to have positive health benefits.
-   Consuming collagen peptides can improve tendon and skin health overall.

**References:**

[1] Pennings B, et al. Amino acid absorption and subsequent muscle protein accretion following graded intakes of whey protein in elderly men. Am J Physiol Endocrinol Metab. 2012 Apr.  [https://www.ncbi.nlm.nih.gov/pubmed/22338070](https://www.ncbi.nlm.nih.gov/pubmed/22338070) 

[2] Oikawa SY, et al. A randomized controlled trial of the impact of protein supplementation on leg lean mass and integrated muscle protein synthesis during inactivity and energy restriction in older persons. Am J Clin Nutr. 2018 Nov.  [https://www.ncbi.nlm.nih.gov/pubmed/30289425](https://www.ncbi.nlm.nih.gov/pubmed/30289425) 

[3] Smith AK, et al. Dietary modulation of cortical excitation and inhibition. J Psychopharmacol. 2017 May.  [https://www.ncbi.nlm.nih.gov/pubmed/28376309](https://www.ncbi.nlm.nih.gov/pubmed/28376309) 

[4] Payam H, et al. Brewer’s Yeast Improves Blood Pressure in Type 2 Diabetes Mellitus. Iran J Public Health. 2013 Jun.  [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3744257](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3744257/)

[5] Sedigheh A, et al. Effect of Fresh Orange Juice Intake on Physiological Characteristics in Healthy Volunteers. ISRN Nutr. 2014 Mar.  [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4045306](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4045306/)

[6] Andrew S P, et al. Drinking carrot juice increases total antioxidant status and decreases lipid peroxidation in adults. Nutr J. 2011 Sept.  [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3192732](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3192732/)

[7] Sonia F S, et al. The use of a commercial vegetable juice as a practical means to increase vegetable intake: a randomized controlled trial. Nutr J. 2010 Sep.  [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2949782](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2949782/)

[8] Jie Zheng, et al. Effects and Mechanisms of Fruit and Vegetable Juices on Cardiovascular Diseases. Int J Mol Sci. 2017 Mar.  [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5372571](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5372571/)

[9] Shaw G, et al. Vitamin C-enriched gelatin supplementation before intermittent activity augments collagen synthesis. Am J Clin Nutr. 2017 Jan.  [https://www.ncbi.nlm.nih.gov/pubmed/27852613](https://www.ncbi.nlm.nih.gov/pubmed/27852613)

[10] Choi FD, et al. Oral Collagen Supplementation: A Systematic Review of Dermatological Applications. J Drugs Dermatol. 2019 Jan.  [https://www.ncbi.nlm.nih.gov/pubmed/30681787](https://www.ncbi.nlm.nih.gov/pubmed/30681787)

#patreon_articles 
#nutrition 
#healthy_diets 
#processed_food 
#whey 
#yeast
#fruit_juices
