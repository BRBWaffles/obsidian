I've received a few private messages over the last year from people asking me if protein distribution matters for muscle protein synthesis (MPS) or muscle hypertrophy. The answer is, yes and no. But mostly no. Let's discuss.

In the fitness world we hear all sorts of stories about how we need to meticulously plan and space these crazy high-protein per meals throughout the day in order to magically unlock our MPS and gAiNz. While protein distribution _does_ have an effect on relative MPS, protein distribution is in no way limiting for MPS or muscle hypertrophy in absolute terms.

Probably the best study that we have on this topic investigated the effects of protein distribution on MPS by dividing people into three groups [1](https://pubmed.ncbi.nlm.nih.gov/23459753/). One group received two meals containing 40g of protein spaced six hours apart. Another group received four meals containing 20g of protein spaced three hours apart. The last group received eight meals containing 10g of protein spaced ninety minutes apart. 

![[Pasted image 20221123151015.png]]

Overall, the group receiving 20g of protein every three hours had the biggest increase in MPS. However, if you look carefully, all three diet conditions actually achieved a statistically significant increase in MPS. They all increased MPS, it's just that the protocol that fed 20g of protein per meal every three hours showed an optimal response. 

![[Pasted image 20221123151022.png]]

These findings are further supported by other research investigating protein distribution and MPS. One of the only other studies on this subject compared "even" protein intakes throughout the day to "skewed" protein intakes throughout the day [2](https://pubmed.ncbi.nlm.nih.gov/24477298/). In the EVEN group, subjects consumed three meals containing 30g of protein spaced evenly apart. In the SKEW group, subjects consumed 10g of protein for breakfast, 15g for lunch, and 65g for dinner.

![[Pasted image 20221123151028.png]]

However, measuring 24-hr MPS is not the same as measuring actual muscle hypertrophy over time. Luckily, we have data on this as well. In a study wherein protein distributions were skewed heavily toward later in the day (similar to the previous study we just discussed), both the evenly distributed group (HBR) and the skewed group (LBR) saw an increase in muscle mass overall [3](https://pubmed.ncbi.nlm.nih.gov/32321161/). Predictably, the group with evenly distributed protein did better.

![[Pasted image 20221123151033.png]]

The bottom line is that protein distribution is not limiting for absolute changes MPS or muscle hypertrophy. Whether you prefer to eat one or two high-protein meals in a day or lots of low-protein snack-like meals while you're resistance training, that's fine. You likely won't gain muscle mass as fast as you would if you optimized the protein distribution, but you will still gain. Period. Do whatever suits you, as long as you hit your total protein targets (discussed [here](https://www.patreon.com/posts/protein-targets-44006622)).

**Key points:**

-   Protein distribution is not limiting for muscle protein synthesis or muscle hypertrophy.
-   Consuming three meals, each containing ~0.5g/kg body weight of protein, is optimal.
-   Hitting your total daily protein target is most important for muscle mass gains.
-   How you distribute your protein is less important for muscle mass gains.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/23459753/](https://pubmed.ncbi.nlm.nih.gov/23459753/)

[2] [https://pubmed.ncbi.nlm.nih.gov/24477298/](https://pubmed.ncbi.nlm.nih.gov/24477298/)

[3] [https://pubmed.ncbi.nlm.nih.gov/32321161/](https://pubmed.ncbi.nlm.nih.gov/32321161/)

#patreon_articles 
#protein 
#exercise 
#hypertrophy 
#nutrition 