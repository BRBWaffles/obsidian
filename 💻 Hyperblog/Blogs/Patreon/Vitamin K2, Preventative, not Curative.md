Observational research suggests that dietary vitamin K2 intake is powerfully protective against certain forms of cardiovascular disease. Particularly diseases of arterial calcification [1](https://www.ncbi.nlm.nih.gov/pubmed/15514282)[2](https://www.ncbi.nlm.nih.gov/pubmed/27927636)[3](https://www.ncbi.nlm.nih.gov/pubmed/31103344). However, recent randomized controlled trials involving nutritional doses of vitamin K2 in the form of MK-7 discovered that vitamin K2 actually, if anything, worsens the progression of pre-existing arterial calcification [4](https://www.ncbi.nlm.nih.gov/pubmed/31387121)[5](https://www.ncbi.nlm.nih.gov/pubmed/31529295). But, how can this be? How can a nutrient that is so protective be so ineffective against the disease that it is so good at preventing? I will attempt to reconcile these two seemingly contradictory findings.

Essentially, I suspect it is precisely because vitamin K2 is also very effective at building bone and keeping bones strong. This is because vitamin K2 activates a protein, osteocalcin, secreted by special cells in our bones, called osteoblasts. These special cells use this activated protein to lay down and form new bone. 

Many lines of evidence suggest that arterial calcification is an active bone-forming process mediated by osteoblasts, or osteoblast-like cells, found near the endothelium [6](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC208734). This is supported by the fact that atherosclerotic lesions are often found to have bone-remodeling proteins inside them in and around sites of calcification [7](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3423589).

If arterial calcification is an active process, akin to bone formation, that is mediated by osteoblasts, the question becomes, why would we _not_ expect vitamin K2 to accelerate the progression of arterial calcification? Vitamin K2 is effective in the building and maintenance of bone. Once the arterial calcification is there, it is very possible that it is functionally no different than other bone structures in the body and vitamin K2 will act on it just the same.

However, I'm not convinced that this means that vitamin K2 supplementation should necessarily be contraindicated for those with higher coronary artery calcification (CAC) scores. Let me explain.

If one has a higher degree of CAC, it is very unlikely that one would be able to reliably regress it with any dietary or pharmacological intervention at all. At least not with any known strategy. No diet, nutrient, or drug has ever been shown to flat-out regress CAC in humans. However, it may be the case that depending on the extent of your CAC, your risk profile may be better served by increasing the burden of CAC, rather than reducing it.

Now, I'm not saying having CAC is necessarily a good thing, and if we had an intervention that actually did regress CAC safely and effectively, I would probably fully support that. But, currently it is much more complicated than that, and sometimes more calcification is better for your risk profile [8](https://www.ncbi.nlm.nih.gov/pubmed/29301708).

Here is a graphic from the paper:

![[Pasted image 20221123151723.png]]

As you can see, the risk of rupture (seen in red) is lowest in people with either no CAC score or the highest CAC score. For all intents and purposes, they're roughly equal. This means your chances of getting a heart attack are, on average, greatest during the initial stages of CAC development.

Vitamin K2 may be relevant here, and I still believe it is very likely preventative. However, perhaps getting a higher CAC score is bad reason to stop talking vitamin K2, as it may help to reduce window of time in which the plaque is most vulnerable.

**Key points:**

-   Vitamin K2 may help to prevent arteries from calcifying.
-   Vitamin K2 may accelerate the progression of preexisting CAC.
-   Those with the highest and lowest CAC scores have the lowest chances of a heart attack.
-   Vitamin K2 may help ensure greater plaque stability by increasing the burden of CAC.

**References:**

[1] Geleijnse JM, et al. Dietary intake of menaquinone is associated with a reduced risk of coronary heart disease: the Rotterdam Study. J Nutr. November 2004. [https://www.ncbi.nlm.nih.gov/pubmed/15514282](https://www.ncbi.nlm.nih.gov/pubmed/15514282) 

[2] Nagata C, et al. Dietary soy and natto intake and cardiovascular disease mortality in Japanese adults: the Takayama study. Am J Clin Nutr. February 2017. [https://www.ncbi.nlm.nih.gov/pubmed/27927636](https://www.ncbi.nlm.nih.gov/pubmed/27927636) 

[3] Zwakenberg SR, et al. Circulating phylloquinone, inactive Matrix Gla protein and coronary heart disease risk: A two-sample Mendelian Randomization study. Clin Nutr. May 2019. [https://www.ncbi.nlm.nih.gov/pubmed/31103344](https://www.ncbi.nlm.nih.gov/pubmed/31103344) 

[4] Zwakenberg SR, et al. The effect of menaquinone-7 supplementation on vascular calcification in patients with diabetes: a randomized, double-blind, placebo-controlled trial. Am J Clin Nutr. October 2019. [https://www.ncbi.nlm.nih.gov/pubmed/31387121](https://www.ncbi.nlm.nih.gov/pubmed/31387121) 

[5] Oikonomaki T, et al. The effect of vitamin K2 supplementation on vascular calcification in haemodialysis patients: a 1-year follow-up randomized trial. Int Urol Nephrol. November 2019. [https://www.ncbi.nlm.nih.gov/pubmed/31529295](https://www.ncbi.nlm.nih.gov/pubmed/31529295) 

[6] Terence M, et al. Doherty. Calcification in atherosclerosis: Bone biology and chronic inflammation at the arterial crossroads. Proc Natl Acad Sci U S A. September 2003. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC208734](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC208734) 

[7] Bithika Thompson and Dwight A. Towler.  Arterial calcification and bone physiology: role of the bone-vascular axis. Nat Rev Endocrinol. September 2013. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3423589](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3423589)

[8] Mori H, et al. Coronary Artery Calcification and its Progression: What Does it Really Mean? JACC Cardiovasc Imaging. 2018 Jan. [https://www.ncbi.nlm.nih.gov/pubmed/29301708](https://www.ncbi.nlm.nih.gov/pubmed/29301708)

#patreon_articles 
#nutrition 
#disease 
#coronary_artery_calcification 
#cardiovascular_disease 
#vitamin_K2 
#clownery 