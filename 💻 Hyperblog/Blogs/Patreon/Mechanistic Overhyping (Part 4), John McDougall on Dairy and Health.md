John McDougall just released [a video](https://www.youtube.com/watch?v=txfU_bdI8hU) illustrating the dangers of dairy in the diet. Personally, I think this video illustrates the dangers of not knowing what the fuck you're talking about, haha. Let's dig in.

McDougall's first empirical claim regarding the effects of dairy on health for which he provides a supporting reference starts at [17:26](https://youtu.be/txfU_bdI8hU?t=1046). Essentially he references an analysis from 20 years ago to support the claim that dairy has no benefit to bone health in humans in randomized controlled trials (RCT), particularly post-menopausal women [1](https://pubmed.ncbi.nlm.nih.gov/10966884/). He claims that nobody else has published such a review in 20 years. This is untrue. I think what he means is that nobody else has published such a review with findings he liked.

There have been many reviews in the last twenty years. The first meta-analysis investigates dairy intakes as they relate to bone growth trajectories in children in RCTs [2](https://pubmed.ncbi.nlm.nih.gov/18539555/). The findings divulge that baseline calcium is relevant at the time of the intervention. Studies wherein participants had higher baseline calcium intakes at the time of the intervention were less likely to see a benefit. We should expect this finding. Ultimately it would seem that dairy is an adequate delivery system for nutrients that support bone health, like calcium.

![[Pasted image 20221123150502.png]]

The second investigates bone mineral density specifically in post-menopausal women [3](https://pubmed.ncbi.nlm.nih.gov/32185512/). The results are quite similar. Increasing dairy products in the diet yields better bone growth trajectories, even in post-menopausal women. Pooled results are statistically significant across the board.

![[Pasted image 20221123150508.png]]

Lastly, the third meta-analysis is investigating the relationship between dairy and markers of bone health in RCTs [4](https://clinicalnutritionespen.com/article/S2212-8263(12)00055-3/abstract). The analysis is not limited to any particular subset of the population. The results showed a statistically significant increase in bone mineral content with increasing dairy consumption.

![[Pasted image 20221123150513.png]]

As an interesting aside, one of the included studies specifically investigated cow's milk compared to soy milk [5](https://pubmed.ncbi.nlm.nih.gov/22282300/). They found a statistically significant increase in both hip and femoral head bone mineral density with cow's milk.

![[Pasted image 20221123150517.png]]

Both the cow's milk and the soy milk contained 250mg of calcium. This could suggest an additional, beneficial property of milk in particular, such as additional protein.

At [19:32](https://youtu.be/txfU_bdI8hU?t=1172), McDougall goes on to isolate a single study from the review he cites [6](https://pubmed.ncbi.nlm.nih.gov/3838218/). He takes great care to let you know that the authors are "employed" by the dairy industry. Whatever that means. He claims that the results show a greater loss of bone for the dairy group, which is untrue. It is clearly shown that the P value for every diet condition is "NS" or "non-significant."

![[Pasted image 20221123150525.png]]

But we don't have to take the author's word for it. We can take the means and standard deviations provided and analyze the data ourselves.

![[Pasted image 20221123150529.png]]

There is no statistically significant differences between the groups using either assessment of bone mass. The results are not statistically relevant, nor clinically relevant. This is a pretty significant error on McDougall's part. As a researcher himself, he should know better than to misreport findings to his lay-audience the way he has here.

At [21:05](https://youtu.be/txfU_bdI8hU?t=1265) McDougall cites a very recent epidemiological study purportedly showing no association between bone health or hip fracture risk with increasing dairy consumption. However, the authors themselves offer an explanation:

> _Given that there were no significant differences in calcium supplement use across dairy intake groups, it is likely that dairy intakes across SWAN participants did not influence total calcium intake of participants sufficiently enough to impact overall femoral neck and lumbar spine BMD outcomes._

This should sound familiar. Basically what they're saying is that calcium supplementation was so widespread in their cohort, that isolating an effect of dairy would be challenging. We discussed above how baseline calcium intake matters.

But this is all beside the point. This is a single epidemiological study that presents some methodological and interpretive challenges. We have RCT data on this very question, cited above. Overall dairy consumption improves markers of bone health in post-menopausal women.

From [21:48](https://youtu.be/txfU_bdI8hU?t=1308) to [26:03](https://youtu.be/txfU_bdI8hU?t=1563), McDougall goes on a long, poorly supported rant about the acid-base balance of the diet. Essentially his position is that animal foods (particularly dairy) provide an acid load to the body that leeches calcium from our bones. Right off the bat, if this were true we would not see all of the benefits of dairy to bone mineral density and/or bone mineral content in the meta-analyses of RCTs cited above. His mechanistic speculation doesn't pan out in the real world.

Dietary acid load does not associate with poorer bone health outcomes in epidemiology, and the association does not meet the Bradford Hill criteria for causality based in epidemiological data [7](https://pubmed.ncbi.nlm.nih.gov/21529374/). No study in the analysis actually found a benefit to alkaline diets, and the total pooled results are null (P=0.5)

However, there is _some_ validity to the idea. Back in 2014, it was discovered that the negative effects on bone sometimes seen with foods that have a high dietary acid load is modulated by the presence or absence of calcium itself [8](https://pubmed.ncbi.nlm.nih.gov/23873776/). The negative effects are only seen in those consuming insufficient dietary calcium. This means that high dietary acid load foods are not necessarily detrimental to bone health in those already consuming adequate calcium.

Now we move on to the "serious" part of the conversation at [26:03](https://youtu.be/txfU_bdI8hU?t=1563). He presents a whole whack of bullet-points detailing the supposed harms of dairy. Let's take a look.

![[Pasted image 20221123150541.png]]

We can essentially lump these claims up into four disease categories; obesity, type 2 diabetes (T2DM), cancer, and cardiovascular disease (CVD).

Let's start with obesity. Observational research typically shows that high-fat dairy protects against overweight and obesity [9](https://pubmed.ncbi.nlm.nih.gov/22810464/). However, in one of the only RCTs on the subject, the opposite effect is seen [10](https://pubmed.ncbi.nlm.nih.gov/33184632/). High fat dairy seems to encourage weight gain.

Because full-fat dairy may contribute to weight gain, it is plausible that full-fat dairy may also contribute to T2DM, cancer, and CVD. However, that's just an extrapolation. Let's take a look at the current evidence. For T2DM, after adjustments for energy intake, we see low-fat dairy appears to be protective, whereas high fat dairy appears to have no association at all [11](https://pubmed.ncbi.nlm.nih.gov/23945722/).

![[Pasted image 20221123150548.png]]

For cancer, it gets complicated. Extensive analyses have been done investigating the relationship between dairy and cancer [12](https://pubmed.ncbi.nlm.nih.gov/30782711/). We have to remember that cancer isn't just a single disease. It is many diseases.

![[Pasted image 20221123150552.png]]

Ultimately dairy appears to be a mixed bag of risks and benefits, just like any other food. The cancer that shows the most obvious effect is prostate cancer. But let's also remember that, just as cancer is not a single disease, dairy is not a single food either. Dairy is many foods.

![[Pasted image 20221123150556.png]]

When we consider the effects of dairy on prostate cancer when stratified by source, a truly puzzling picture emerges. It is completely unclear what aspect of the dairy is conferring the effect. Is it the fat? No. Is it the protein? No. Is it the lactose? No. What in the world is going on here? Virtually all of the risk is being conveyed through soft cheese and milk. But wait, there's more! Milk is the primary source of the benefits for colorectal cancer! Truly, truly fascinating results. But lets say both are 100% true. Well, maybe just replace the milk and soft cheeses in your diet with fibre, and reduce your red meat intake to make up for the losses in colorectal cancer benefits. That way we can avoid the risk to prostate cancer. Who knows. Whatever the case, cancer risk does not seem to be a general effect of dairy.

Last up, we have CVD. I've already covered dairy and CVD [here](https://www.patreon.com/posts/when-saturated-42924072). So I won't rehash it now. Long story short, it's the same as the cancer results. It depends on the source of dairy and the type of CVD.

From [26:53](https://youtu.be/txfU_bdI8hU?t=1613) to [30:10](https://youtu.be/txfU_bdI8hU?t=1810), McDougall makes a series of rapid fire empirical claims that are difficult to corroborate, investigate, or verify. But the next hilarious claim occurs at [32:41](https://youtu.be/txfU_bdI8hU?t=1961), where he attempts to scare us with the fact that there is a maximum white blood cell count quality control threshold for dairy products. He deliberately refers to the white blood cells as "pus cells" in order to scaremonger. Listen, just because there are white blood cells in dairy foods doesn't actually mean they're bad for us. There's trace fecal matter on lettuce. I'm still going to eat lettuce.

McDougall spends an enormous amount of time, [33:31](https://youtu.be/txfU_bdI8hU?t=2011) to [45:15](https://youtu.be/txfU_bdI8hU?t=2715), talking about how dairy acts as a vector for the transmission of zoonotic diseases. Absolutely true. Virtually all of the infectious disease risk associated with food production are zoonotic in origin [13](https://pubmed.ncbi.nlm.nih.gov/32219187/). So, McDougall does have a point. Animal food production, distribution, and consumption, all come with an inherent risk of exposure to zoonotic pathogens. But, what's the alternative? People need adequate nutrition, which in many cases cannot be obtained without foods of animal origin. For now, it seems the juice is worth the squeeze, and the risk of foodborne illness is probably worth it on a population level.

McDougall continues with an egregious act of cherry-picking at [46:35](https://youtu.be/txfU_bdI8hU?t=2795). He cites a study conducted on constipated children [14](https://pubmed.ncbi.nlm.nih.gov/9770556/). He then claims that the study showed that removing cow's milk from the diets of constipated children cured 68% of them. This is a half-truth. The study was conducted on children who may have been specifically enrolled on the basis of an existing diagnosis or suspicion of cow's milk intolerance. The authors acknowledge this limitation, and the potential risk of bias.

![[Pasted image 20221123150607.png]]

McDougall speculates that it's actually a result of cow's milk protein. However, this has been investigated [15](https://pubmed.ncbi.nlm.nih.gov/23340316/). Altering the cow's milk protein casein has no effect on constipation in children.

At [51:02](https://youtu.be/txfU_bdI8hU?t=3062), McDougall makes the outrageous assertion that the cow's milk protein predisposes populations to type 1 diabetes. The only evidence for this claim that I could find was a single narrative review paper specifically investigating A1 β-casein [16](https://pubmed.ncbi.nlm.nih.gov/28504710/). Probably the most persuasive evidence they present is a regression model exploring the relationship between A1 β-casein exposure by country and type 1 diabetes.

![[Pasted image 20221123150613.png]]

What I find interesting here is that this seems to be tracking more than just country of origin. It seems to be tracking ethnicity, too. It could be that countries with populations more likely to get type 1 diabetes are also more likely to consume dairy. It would be impossible to know without doing similar regressions within each population itself.

Again, from [52:09](https://youtu.be/txfU_bdI8hU?t=3129) to [57:15](https://youtu.be/txfU_bdI8hU?t=3435), McDougall treats us to a long, passionate rant about the devastating effects of dairy without providing a single citation. Instead, the bottom righthand corner of his slides are marked with "search at: [pubmed.gov](https://www.pubmed.gov/)." Well, Dr. McDougall, that which is asserted without evidence can be dismissed without evidence.

The last claim I'll touch on occurs at [1:00:51](https://youtu.be/txfU_bdI8hU?t=3651). He claims that the "Canadian Dietary Guidelines 2019" encourage cessation of dairy foods. This is not true. Firstly, there's no such thing as the Canadian Dietary Guidelines. Canada has "Canada's Food Guide." Secondly, yogurt can clearly be seen on the plate used to showcase Canada's Food Guide. 

Additionally, McDougall claims that Canada's Food Guide encourages us to ditch milk in favour of water. This is also not true. In fact, they specifically list white milk as a healthy alternative to water [here](https://food-guide.canada.ca/en/healthy-eating-recommendations/make-water-your-drink-of-choice/).

![[Pasted image 20221123150649.png]]

Canada's Food Guide also includes recipes. Many of which include dairy foods such as cheese, which can be seen [here](https://www.canada.ca/en/health-canada/services/canada-food-guide/tips-healthy-eating/meal-planning-cooking-healthy-choices/recipes.html). They also include many dairy foods as healthy sources of protein, which can be seen [here](https://food-guide.canada.ca/en/healthy-eating-recommendations/make-it-a-habit-to-eat-vegetables-fruit-whole-grains-and-protein-foods/eat-protein-foods/).

In conclusion, Dr. John McDougall is a dubious source of nutrition information, haha.

**Key points:**

-   Dairy improves bone health in humans, including postmenopausal women.
-   High fat dairy products may encourage weight gain.
-   Many dairy foods are protect against type 2 diabetes and cardiovascular disease.
-   Certain dairy foods provide a combination of risks and benefits for different cancers.
-   Dairy is a vector for zoonotic foodborne pathogens.
-   There is no clear evidence that dairy generally increases the risk of constipation in children.
-   Canada's Food Guide does not encourage the cessation of dairy food consumption.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/10966884/](https://pubmed.ncbi.nlm.nih.gov/10966884/)

[2] [https://pubmed.ncbi.nlm.nih.gov/18539555/](https://pubmed.ncbi.nlm.nih.gov/18539555/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/32185512/](https://pubmed.ncbi.nlm.nih.gov/32185512/) 

[4] [https://clinicalnutritionespen.com/article/S2212-8263(12)00055-3/abstract](https://clinicalnutritionespen.com/article/S2212-8263(12)00055-3/abstract)

[5] [https://pubmed.ncbi.nlm.nih.gov/22282300/](https://pubmed.ncbi.nlm.nih.gov/22282300/) 

[6] [https://pubmed.ncbi.nlm.nih.gov/3838218/](https://pubmed.ncbi.nlm.nih.gov/3838218/)

[7] [https://pubmed.ncbi.nlm.nih.gov/21529374/](https://pubmed.ncbi.nlm.nih.gov/21529374/) 

[8] [https://pubmed.ncbi.nlm.nih.gov/23873776/](https://pubmed.ncbi.nlm.nih.gov/23873776/) 

[9] [https://pubmed.ncbi.nlm.nih.gov/22810464/](https://pubmed.ncbi.nlm.nih.gov/22810464/) 

[10] [https://pubmed.ncbi.nlm.nih.gov/33184632/](https://pubmed.ncbi.nlm.nih.gov/33184632/) 

[11] [https://pubmed.ncbi.nlm.nih.gov/23945722/](https://pubmed.ncbi.nlm.nih.gov/23945722/) 

[12] [https://pubmed.ncbi.nlm.nih.gov/30782711/](https://pubmed.ncbi.nlm.nih.gov/30782711/) 

[13] [https://pubmed.ncbi.nlm.nih.gov/32219187/](https://pubmed.ncbi.nlm.nih.gov/32219187/) 

[14] [https://pubmed.ncbi.nlm.nih.gov/9770556/](https://pubmed.ncbi.nlm.nih.gov/9770556/) 

[15] [https://pubmed.ncbi.nlm.nih.gov/23340316/](https://pubmed.ncbi.nlm.nih.gov/23340316/) 

[16] [https://pubmed.ncbi.nlm.nih.gov/28504710/](https://pubmed.ncbi.nlm.nih.gov/28504710/)

#patreon_articles 
#dairy 
#bones 
#disease 
#clownery 