There are many so-called "optimal" micronutrient ratios that have been described in the literature. However very few of them have any sort of real validation with regards to human physiology, let alone human health outcomes. Often times ratios are simply reflecting having either too much of one nutrient or not enough of another nutrient, and not actually reflecting a particular ratio that nutrients need to be maintained within.

For example, with the omega-3 to omega-6 ratio, virtually all of the negative associations with a low omega-3 to omega-6 ratio can be explained by having insufficient omega-3 intakes. Rather than higher omega-6 intakes being harmful, the skewed ratio is merely reflecting that one nutrient intake is inadequate. All the ratio does is confuse the issue, because lowering omega-6 to within an "optimal" omega-3 to omega-6 ratio doesn't address the inadequate omega-3 intake.

I'm usually not a big fan of ratios in nutrition for reasons that I explained in [this](https://thenutrivore.blogspot.com/2020/01/measuring-nutrient-density-calories-vs.html) blog article. Ratios don't give you enough information. Foods that are enormously high in both nutrients could get similar scores to foods that are dismally low in both nutrients. For example, 100/10=10, and 1/0.1=10. Both of these examples give us identical scores, but there is an order of magnitude difference between the input values.

Nevertheless, the academic debate around these ratios continues. But the least I could do to add some sanity to the debate is to help us visualize the ratios in a more productive way. So I've prepared a series of charts using a selection of common nutrient ratios. The charts are set up to actually tell us which types of foods are highest in whichever nutrient, in order to actually optimize the ratios.

**Omega-3 vs Omega-6**

![[Pasted image 20221123151445.png]]

**Vitamin E vs Polyunsaturated Fat**

![[Pasted image 20221123151451.png]]

**Polyunsaturated Fat vs Saturated Fat**

![[Pasted image 20221123151458.png]]

**Potassium vs Sodium**

![[Pasted image 20221123151503.png]]

**Calcium vs Magnesium**

![[Pasted image 20221123151508.png]]

**Calcium vs Phosphorus**

![[Pasted image 20221123151511.png]]

**Zinc vs Copper**

![[Pasted image 20221123151516.png]]

#patreon_articles 
#nutrients 
#nutrition 
