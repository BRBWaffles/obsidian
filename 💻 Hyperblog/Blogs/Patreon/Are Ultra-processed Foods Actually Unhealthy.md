No matter where you stand in the nutrition blogosphere, it is extremely common to hear from all corners that the standard Western diet (SWD) is unhealthy. The justification for this claim is that the diet is comprised solely of ultra-processed foods (UPF), and these UPFs are inherently harmful in and of themselves. But the latter statement doesn't necessarily have to follow from the former statement.

![[1-13.png]]

Looking at this 3x3 grid, every single possibility is a plausible hypothesis. Let's look at the claim currently being made, which is cell E— the SWD is unhealthy and UPFs are also unhealthy.

Firstly, there is no question that the SWD is characterized by UPF consumption, and that UPF consumption correlates well with poor health outcomes [1](https://pubmed.ncbi.nlm.nih.gov/31099480/). It definitely seems to be the case that the SWD is an unhealthy dietary pattern. But I think we can use a thought experiment to help us understand why this isn't the same thing as saying that the UPFs themselves are inherently unhealthy or harmful.

I think we can agree that fruit is healthy. But what if you ate nothing but fruit? What if you ate fruit to the exclusion of everything else? I'm pretty confident that terrible things would happen to virtually anyone who attempted that diet. But does that make fruit unhealthy? Of course not!

Similarly, the SWD is for all intents and purposes a mono-diet. It is characterized by the exclusive consumption of UPF, which have an incredibly homogeneous composition— wheat flour, vegetable oils, sugar, animal by-products, and salt. So why the double-standard? Why do we assume that fruit is healthy despite the fact that bad things are likely happen on a diet of nothing but fruit, but we also assume UPFs are unhealthy because bad things are likely to happen on a diet of nothing but UPF? Seems weird.

We need a symmetry-breaker to justify the disparity between these positions. If we suggest that the symmetry-breaker is that fruit correlates with positive health outcomes from the lowest to highest intakes in epidemiology, but UPFs correlate with negative health outcomes from the lowest to highest intakes, I'd say we're not actually comparing like with like. 

In epidemiology investigating UPFs and health, the highest quantiles of intake will be representative of people eating almost exclusively UPFs. In essence, these people would be eating mono-diets. Whereas in epidemiology investigating fruit and health, the upper quantiles of intake will almost never be representative of people eating exclusively fruit. The people eating the most fruit will still have very heterogeneous diets. In one instance we're dealing with a mono-diet, and in the other instance we're not. So I don't think that works as a symmetry-breaker.

Plus, imagine what we would see in the epidemiology if we did manage to isolate a large subset of the population who've been eating nothing but fruit for the last forty years. Do you think we'd see their health status improve or worsen compared to the general population? It's likely that the fruit-only diet would probably be worse for health in a lot of ways that the SWD would be.

So, what do I think? I think diet-health relationships are often about substitution effects. Every time you add something to your diet, you have to remove something else. Every time you remove something from your diet, you have to add something else. If someone is eating a single food item/category to the exclusion of everything else that could be conferring additional benefits, we'd expect worse outcomes. It doesn't matter if the food is "healthy" or "unhealthy", to be perfectly honest.

The fact that when someone eats nothing but UPFs their health outcomes tend to worsen doesn't shock me. But I'm not convinced that it is because UPFs are inherently harmful. Because we can't disassociate the negative effects of the foods themselves from the negative effects of such a drastic substitution.

But before I sign off here, let me give you one more hypothetical. Say I'm eating a diet that has: 20 servings of fruits and vegetables, adequate protein (mostly from lean meats and pulses), micronutrient sufficiency, and saturated fat under control. It doesn't get much better than that, honestly. Maximal benefit of fruits and vegetables occurs around 10 servings per day, so this diet has a lot of redundancy. What if I wanted to shave off two servings of fruits and vegetables and add in UPFs like candy, biscuits, or potato chips? I don't think there is data to suggest that such a substitution would cause negative health outcomes. In fact, the diet I'm describing is likely the diet that is representative of the reference diet in many epidemiological studies on So, why suggest that UPFs are inherently harmful?

So are UPFs unhealthy? Personally, I'm agnostic about it. I haven't seen any good data to suggest that they are inherently harmful, and I can make plenty of arguments for their health value as well. So, I remain open to persuasion in either direction.

**Key points:**

-   Many claim that ultra-processed foods are inherently harmful.
-   There is evidence that diets of primarily ultra-processed foods are harmful.
-   Diets of primarily fruit would be likely to be equally harmful.
-   Nevertheless, fruit are still assumed healthy and ultra-processed foods are still assumed unhealthy.
-   Justifications for this double-standard are not obvious and require elucidation.
-   There doesn't seem to be any robust evidence that ultra-processed foods are inherently harmful.

**References:**

[1] Stefanie Vandevijvere, et al. Global trends in ultraprocessed food and drink product sales and their association with adult body mass index trajectories. Obes Rev. 2019 Nov. [https://pubmed.ncbi.nlm.nih.gov/31099480/](https://pubmed.ncbi.nlm.nih.gov/31099480/)

#patreon_articles 
#ultraprocessed_foods 
#healthy_diets 
#western_diets
#disease 
