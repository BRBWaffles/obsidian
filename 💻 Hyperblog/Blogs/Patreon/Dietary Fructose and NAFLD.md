It's been proposed that the primary cause of NAFLD in the population is the overconsumption of dietary fructose. Physicians such as Robert Lustig and Peter Attia have outright claimed that fructose is overwhelmingly the culprit in the NAFLD epidemic. But, how true is this?

Let's look at some of the available evidence for this claim, such as controlled feeding studies using hypercaloric (weight-gaining) and eucaloric (weight-maintaining) diets. Let's start with hypercaloric feeding experiments. When subjects are overfed 1000 extra calories, consisting of 50% glucose and 50% fructose (equal to approximately 125g of fructose per day), there is a marked increase in liver fat— around 27% [1](https://www.ncbi.nlm.nih.gov/pubmed/22952180). This is because fructose stimulates de novo lipogenesis (DNL) to a greater degree than glucose. The effect reduces insulin sensitivity and definitely contributes to visceral adiposity.

However, what happens if we're maintaining our weight on a diet equally rich in fructose? Even under these conditions it has been shown that dietary fructose uniquely produces a 25% increase in liver fat on average [2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4454806). However, because the diet is eucaloric, these effects are actually transient. Fasting insulin, triglycerides, and DNL did not differ significantly between diet periods. This means that, yes, fructose did increase postprandial liver fat. But, it was burned off by the next day, as evidenced by the fact that the subjects maintained their weight. So, who cares about the transient increase in liver fat?

Overwhelmingly the driver behind NAFLD is excess weight, which is a function of caloric intake [3](https://www.ncbi.nlm.nih.gov/pubmed/29221645). When those with NAFLD lose weight, their NAFLD either improves or resolves, regardless of whether they are consuming a high carbohydrate diet or not [4](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2677125). But, I'll direct your attention to the fact that during overfeeding, fructose consumption only increases liver fat by 27%. So, where did the baseline liver fat come from, if not from fructose? It came from dietary fat. Without a doubt the most lipogenic energy substrate to the liver is dietary fat. Hands down. A protocol that maximally reduces liver fat is likely a protocol that restricts dietary fat above all other macros.

If we were somehow able to learn the history of every fatty acid in the liver of someone with NAFLD, we'd very likely discover that the vast majority of those lipids ended up there as fat brought in as dietary fat, not dietary fructose.

**Key points:**

-   Fructose does increase liver fat content in hypercaloric conditions.
-   Fructose only transiently increases liver fat content in eucaloric conditions.
-   Regardless of fructose intake, obesity is the primary cause of NAFLD.
-   Weight loss reverses NAFLD regardless of the macronutrient ratios of the diet.

**References:**

[1] Sevastianova K, et al. Effect of short-term carbohydrate overfeeding and long-term weight loss on liver fat in overweight humans. Am J Clin Nutr. October 2012. [https://www.ncbi.nlm.nih.gov/pubmed/22952180](https://www.ncbi.nlm.nih.gov/pubmed/22952180)

[2] Jean-Marc Schwarz, et al. Effect of a High-Fructose Weight-Maintaining Diet on Lipogenesis and Liver Fat. J Clin Endocrinol Metab. June 2015. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4454806](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4454806/)

[3] Lean ME, et al. Primary care-led weight management for remission of type 2 diabetes (DiRECT): an open-label, cluster-randomised trial. Lancet. February 2018. [https://www.ncbi.nlm.nih.gov/pubmed/29221645](https://www.ncbi.nlm.nih.gov/pubmed/29221645)

[4] Erik K, et al. Dietary fat and carbohydrates differentially alter insulin sensitivity during caloric restriction. Gastroenterology. May 2009. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2677125](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2677125/)

#patreon_articles 
#nutrition 
#disease 
#fructose 
#non_alcoholic_fatty_liver_disease 
#clownery 