A meta-analysis from 2017 investigated the differential effects of low carb (LC) and low fat (LF) diets on measures of energy expenditure and fat mass loss [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5568065/). Some time ago, David Ludwig PhD implied in a Twitter post that if the meta-analysis was stratified by duration, the longest studies would end up favouring low carb diets. He took the tweet down only moments after posting it. He likely knew that he was potentially making a bold assumption and came to his senses. 

For some reason, I anticipated him removing the tweet and I decided to save it. 

![[Pasted image 20221123151853.png]]

I then contacted Kevin Hall, the lead author of the meta-analysis in question. I asked if I could have access to the raw data so that I could stratify his included studies by duration. He was extremely polite and accommodating, and passed me his data. We had a lengthy back-and-forth about the data, and the implications of diet composition on fat mass loss. He was a pleasure to speak with.

After inputting the data and digging through each study individually for duration information, I broke Kevin Hall's meta-analysis up into four subgroups: <1 week, 1-2 weeks, 2-4 weeks, and >4 weeks. Here are the results.

**Figure 1**  
LC vs. LF (Body Fat Changes)

![[Pasted image 20221123151857.png]]

**Figure 2  
**LC vs. LF (Energy Expenditure Changes)

![[Pasted image 20221123151901.png]]

For body fat changes, the results start off statistically significant in favour of LF diets for study durations under four weeks.  At four weeks and greater, the results are completely null. One interesting observation is that the P-value steady increases as the trial duration increases. Meaning that LF diets have an advantage in the short term, but are likely a total wash in the long term.

For energy expenditure (EE) changes, things get complicated. Results are statistically significant in favour of LF diets for studies under one week in duration and between two to four weeks in duration. However, beyond four weeks there is a statistically significant increase in EE in favour of LC diets. The pooled results suggest that LC diets could increase EE by ~103 kcal per day. Sounds like it could be a victory for LC diets here. Or maybe not.

Let's get practical here. ~103 kcal/day is about 11.4g of fat per day. Which translates to one pound of fat ever 39 days. That's 9.3lb per year. Nothing to write home about, really. But it's an effect, for sure. However, there are some troubling issues with two papers in subgroup four of Figure 2.

Firstly, Ebbeling et al, 2012 measured EE using a method known as doubly-labeled water (DLW). This essentially involves giving subjects water that contains uncommon isotopes of both hydrogen and oxygen. You can then measure the rate at which these isotopes leave the body, though breath and urine, and calculate someone's metabolic rate. 

The only trouble is that the amount of CO2 that subjects evolve on LC diets is different than it is on LF diets. Kevin Hall himself has published on his issues in the past [2](https://www.biorxiv.org/content/10.1101/403931v1). Ebbeling et al, 2012 reported weak differences in CO2 production between LC and LF based on DLW. The massive effect on EE is only seen when they use their calculated model, wherein the RQ differences are calculated based on the food the subjects were provided. This model assumes perfect diet adherence, and could potentially overestimate the effect of the LC diet on EE.

![[Pasted image 20221123151907.png]]

Just for the sake of curiosity, let's remove Ebbeling et al, 2012 from the forest plot.

![[Pasted image 20221123151911.png]]

As we can see, 99.8% of the weight is coming from a Hall et al, 2016. There is an additional issue with this study as well. Their measurements of EE showed a steady decline toward a null effect, but the study duration wasn't sufficient to see the regression in EE through to the end. By day 20, some subjects were showing a decrease in total EE. It is also worth noting that the bulk of the effect is seen within the first 10 days of the study, and did not take four weeks to achieve.

![[Pasted image 20221123151917.png]]

But let's assume the effect is real, and turn a blind eye to the limitations I've discussed. The estimated difference in EE without Ebbeling et al, 2012 would be equal to about ~57 kcal per day in favour of LC diets. That is approximately 6.3g of fat burned per day— equal to ~5lb per year. Is this an advantage? Technically yes. Practically, probably not. But it might be something—  practically meaningless. But something nonetheless.

Overall, the two studies that contribute the most statistical weight to subgroup four in Figure 2 have methodological limitations that make it difficult to tell if the effect we're seeing would actually pan out over time. My guess is that they probably wouldn't. Personally, I'm not sure if I'm willing to give this subgroup the benefit of the doubt, given the limitations I've discussed. 

For me, the effects of macronutrient composition on fat loss and energy expenditure remain an open question. If you're persuaded by the results, feel free to trumpet a victory for LC diets. However, I think the celebrations are premature.

**Key points:**

-   It has been suggested that low carb diets could independently increase energy expenditure and thus increase fat mass loss if given enough time.
-   When stratified by study duration, low carb diets show no detectable advantages on fat mass loss, but a statistically significant increase in energy expenditure.
-   However, these findings could be the result of methodological shortcomings within the primary studies that contribute the most statistical weight.

**References:**

[1] Kevin D. Hall and Juen Guo. Obesity Energetics: Body Weight Regulation and the Effects of Diet Composition. Gastroenterology. Feb 2017.  [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5568065/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5568065/) 

[2] Kevin D. Hall, et al. Potential Bias of Doubly Labeled Water for Measuring Energy Expenditure Differences Between Diets Varying in Carbohydrate. Biorxiv. Aug 2018.  [https://www.biorxiv.org/content/10.1101/403931v1](https://www.biorxiv.org/content/10.1101/403931v1)

#patreon_articles 
#nutrition 
#disease 
#weight_loss 
#low_carb 
#low_fat 