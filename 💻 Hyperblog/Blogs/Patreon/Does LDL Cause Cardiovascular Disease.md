If we spend any time in the low carb or ketogenic corners of Twitter, we'll undoubtedly encounter the suggestion that low density lipoproteins (LDL) do not actually cause atherosclerotic cardiovascular disease (ASCVD). So, I'm going to just quickly go over the evidence for why this is complete bullshit, and I will also explain why medical practice wouldn't change even if it wasn't bullshit. It would also be a good idea to get a refresher on ASCVD pathophysiology from my previous blog article [here](https://www.patreon.com/posts/why-does-ldl-33915357).

Let's start off with the randomized controlled trial (RCT) data. In biomedical science, RCT data gives us the best possible reflection of cause and effect relationships between variables. For example, if we administer a drug in one random group of people and a placebo in a likewise random group of people, we can measure differences in endpoints between these groups and ascertain the independent effects of the drug. If the intervention group yields consistent, statistically significant effects, this gives us a strong basis for causal inference.

We have a number of RCTs testing different mechanisms for lowering LDL in humans. When you consider all of these interventions together in a meta-regression analysis, it is revealed that an intervention lowers ASCVD events in proportion to the LDL-lowering effect of the intervention itself [1](https://pubmed.ncbi.nlm.nih.gov/27673306/). This means that the reductions in ASCVD events can be accurately predicted by the degree of LDL lowering achieved. The sole exception is CETP-inhibitors, but they did not actually lower LDL. They lowered the cholesterol content of LDL, and also inhibited reverse cholesterol transport. 

![[1-15.jpg]]

Is this just an amazing coincidence, or are we looking at a cause and effect relationship? Imagine what would need to be true in order for this to **not** be a cause and effect relationship. All of these mechanisms would need to be operating through independent pleiotropic mechanisms that all conspire to produce reductions in ASCVD events that can be reliably predicted by the degree of LDL-lowering achieved. That's crazy. But, even if it were true, it looks like LDL would still be a reasonable target for therapy, because it is the common denominator between all of those hypothetical pleiotropic mechanisms.

Next we have more natural experiments. These come in the form of Mendelian randomization (MR) studies, which are types of observational research that investigate the relationships between gene variants and outcomes in free-living populations. Basically, we presume that gene variants are randomly distributed across the population, such that relevant covariates  are also randomized. This essentially creates the next best thing to an RCT, and likely would sit just under RCTs on the hierarchy of evidence.

Again, we have plenty of gene variants that modulate LDL up or down. When we look at gene variants that reduce LDL, we see the same hierarchy of effect that we see with the RCTs [2](https://pubmed.ncbi.nlm.nih.gov/30694319/). The ASCVD event reduction is also a function of the LDL-lowering that resulted from the particular gene variant. If you have a gene variant that reduces LDL by a little, you see a little effect. If you have a gene variant that reduces LDL by a lot, you see a larger effect.

![[1-14.png]]

Again, is this just a remarkable coincidence that these two lines of data converge so perfectly? I'll make the same argument again. What would need to be true in order for LDL to **not** be causal here? Again, all of these gene variants would need to be operating through independent pleiotropic mechanisms that all conspire to produce reductions in ASCVD events that can be reliably predicted by the degree of LDL-lowering achieved. Again, that's crazy. But again, even if it were true it would still be a good idea to target LDL to lower ASCVD events.

**Key points:**

-   It has been suggested that LDL are not causal in ASCVD.
-   Randomized controlled trials using eight different mechanisms to lower LDL all see reductions in ASCVD events that are predicted by the degree of LDL-lowering achieved.
-   Mendelian randomization studies investigating almost two dozen different mechanisms that lower LDL all see reductions in ASCVD events that are predicted by the degree of LDL-lowering achieved.
-   LDL is causal in ASCVD, but even if it wasn't it would still be a good idea to target LDL to reduce ASCVD events. 

**References:**

[1] Michael G Silverman, et al. Association Between Lowering LDL-C and Cardiovascular Risk Reduction Among Different Therapeutic Interventions: A Systematic Review and Meta-analysis. JAMA. 2016 Sep. [https://pubmed.ncbi.nlm.nih.gov/27673306/](https://pubmed.ncbi.nlm.nih.gov/27673306/) 

[2] Brian A Ference, et al. Association of Triglyceride-Lowering LPL Variants and LDL-C-Lowering LDLR Variants With Risk of Coronary Heart Disease. JAMA. 2019 Jan. [https://pubmed.ncbi.nlm.nih.gov/30694319/](https://pubmed.ncbi.nlm.nih.gov/30694319/)

#patreon_articles 
#LDL 
#cardiovascular_disease 
#disease 
#ApoB 
