Hi everyone! Now that school's over I have some time to write more. So, what better topic to discuss than one about which I have recently changed my mind!

Back in 2020, I published a meta-analysis to my blog that, in retrospect, was actually pretty poorly done [1](https://thenutrivore.blogspot.com/2020/10/low-carbohydrate-diets-and-health.html#BG)]. This last semester I took a graduate studies data synthesis course on systematic review and meta-analysis, so now I'm hyper-critical of much of my previous meta-analyses.

I think the data from the low carb blog article is still valuable, despite the fact that it definitely wasn't done according to best practice. The findings of that meta-analysis suggested that reductions in blood glucose on ketogenic diets could be mediated by weight loss, so that is what I had believed for a long time.

However, there was only one study in the <1kg weight loss subgroup in my weight loss sensitivity analysis. This study was Myette-Côté et al. (2018) [2](https://pubmed.ncbi.nlm.nih.gov/30303707/). According to my analysis, both the between-group treatment effect and the within-group change score for keto was non-significant, and nowhere close to even being borderline significant.

But this was actually just an artifact of the particular statistical tests I was using. In the actual paper, the authors used specific normality tests in conjunction with an ANOVA test, which is a continuous outcome test that is more robust than the one I was using. The inverse variance test that I was using assumes normal distributions.

![[1-90.png]]

This is paper we're comparing the control diet (GL) to the ketogenic diet without exercise (LC). It is clear from this data that the distribution for LC is non-normal, which violates the assumptions of the test that I used. The distribution almost looks bimodal, which is something my test could not account for. It's easy to see how just comparing the mean and standard deviation of both GL and LC could produce non-significant treatment effects.

Using better tests, the authors found a statistically significant reduction in fasting blood glucose with LC compared to control, without weight loss. In fact, the LC did not lose a statistically significant amount of weight, which could imply that the LC subjects may have even gained body fat, because normally some non-fat weight loss is expected in ketosis due to water loss.

As far as I know this is the most controlled test of the hypothesis that ketogenic levels of carbohydrate-restriction cause reductions in blood glucose. So, it looks like my previous inferences about how carbohydrate-restriction relates to blood glucose were probably incorrect. Though, I think the effect is probably unique to ketogenic diets. In this pre-print manuscript by Ozairi et al. (2021), the effect of a number of non-ketogenic levels of carbohydrate restriction on blood glucose were tested [3](https://www.medrxiv.org/content/10.1101/2021.05.30.21258049v1)].

![[1-92.png]]

Overall, even levels of carbohydrate restriction as low as 10% of energy did not yield statistically significant differences in average blood glucose. The authors suggest that this could be due to a unique glucose-lowering effect of ketones themselves:

> _"There is growing evidence that ketone bodies themselves independently lower glucose at least partly by reducing hepatic glucose output and long-term randomised and open-label trials which have used nutritional ketosis (>0.5mmol) as a therapeutic goal and a measure of compliance suggest that carbohydrate restriction in the context of significant weight loss and sufficient to induce ketosis may produce large reductions in both HbA1c and diabetes medication use."_

One of the primary studies used as the basis for this speculation is a ketone supplementation trial that suggested that ketones could uniquely lower blood glucose [4](https://pubmed.ncbi.nlm.nih.gov/33367782/). In this study one group received ketone monoester of beta-hydroxybutyrate, and the other group received placebo. There was a consistent, statistically significant effect of the ketone esters on lowering blood glucose, even outside the context of a carbohydrate-restricted ketogenic diet or weight loss.

![[1-91.png]]

To wrap things up, it's likely the case that ketosis has a unique effect of lowering blood glucose, independent of changes in body weight. This effect can likely be achieved through extreme carbohydrate restriction or ketone supplementation. Though, ketone supplementation could potentially be dangerous for those with impaired beta-cell function. Though, that's just speculation on my part.

**Key points:**

-   Both ketogenic diets and ketone supplementation appear to independently lower blood glucose.
-   Blood glucose lowering appear to occur independent of weight loss.

**References:**

[1] [https://thenutrivore.blogspot.com/2020/10/low-carbohydrate-diets-and-health.html#BG](https://thenutrivore.blogspot.com/2020/10/low-carbohydrate-diets-and-health.html#BG) 

[2] [https://pubmed.ncbi.nlm.nih.gov/30303707/](https://pubmed.ncbi.nlm.nih.gov/30303707/) 

[3] [https://www.medrxiv.org/content/10.1101/2021.05.30.21258049v1](https://www.medrxiv.org/content/10.1101/2021.05.30.21258049v1) 

[4] [https://pubmed.ncbi.nlm.nih.gov/33367782/](https://pubmed.ncbi.nlm.nih.gov/33367782/)

#patreon_articles 
#keto 
#blood_glucose 
#hba1c 
#nutrition 
#type_2_diabetes 