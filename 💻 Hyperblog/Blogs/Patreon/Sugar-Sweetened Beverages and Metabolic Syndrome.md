The idea that sugar-sweetened beverages (SSBs) uniquely cause metabolic syndrome (MetS) is an idea that seems to circulate around low carbohydrate diet camps, and was primarily spearheaded by researchers like Robert Lustig and David Ludwig. However, how robust are the data?  
  
If we look at associations between SSBs and MetS, we see a fairly linear increase in risk that is pretty striking [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7348689/). Even more striking is the lack of association between MetS and other liquid sources of sugar, like with fruit juices.

![[1-48.png]]

However, I've criticized epidemiological findings relating specific foods to outcomes of this nature, particularly type 2 diabetes (T2D). Despite the best efforts on the part of the researchers, sometimes the data are not robust enough to allow for an adequate adjustment for certain confounders. 

In this case, adjustments for energy intake simply aren't able to fully capture the effect of calories on MetS and T2D risk, which I have previously discussed [here](https://www.patreon.com/posts/dietary-fructose-31876052). An adjustment for energy intake should reduce the association between a food and MetS or T2D to null. I'll explain why.

We have randomized controlled trials feeding different types of SSBs and measuring MetS-related outcomes in both eucaloric and ad libitum conditions [2](https://pubmed.ncbi.nlm.nih.gov/27023594/)[[3]](https://pubmed.ncbi.nlm.nih.gov/32696704/). Essentially, under eucaloric (weight maintaining) conditions, SSBs do not really adversely affect measures of MetS. However, when people are given SSBs and allowed to consume them ad libitum (at their own leisure), people tend to overconsume them (because they're fucking delicious and poorly satiating). When SSB consumption results in a calorie surplus, risk factors of MetS tend to worsen.

Rigorously controlled human experiments divulge that increases in body fat as a consequences of SSB overconsumption are perfectly predicted by calories [4](https://pubmed.ncbi.nlm.nih.gov/3165600/). In this experiment, fruit juices were used to achieve a profound calorie surplus in humans. The degree of fat accumulation was perfectly predicted by total calories. So, we can say with confidence that SSBs aren't affecting adiposity independent of calories.

![[1-47.png]]

In fact, one group of researchers used varying levels of high fructose corn syrup (HFCS) or sucrose in an isocaloric, hypocaloric (weight loss) diet [5](https://pubmed.ncbi.nlm.nih.gov/22866961/). There were five different diet conditions— 10% HFCS, 20% HFCS, 10% sucrose, 20% sucrose, and a eucaloric control with added exercise. They discovered no statistically significant between-group differences in weight loss, and weight loss was also predicted by calorie intake.

In conclusion, do SSBs "cause" MetS? To the extent that SSBs contribute to excessive caloric intake, I would say yes. That is to say, of those who currently have MetS, the ones who would **not** have developed MetS had it _not_ been for the SSBs in their diet, we can say that the SSBs were causal in the development of their MetS. However, SSBs are likely not unique here. Any food would increase the risk of MetS if it was overconsumed to a sufficient degree.

**Key points:**

-   SSB consumption associates with MetS even after adjustments for energy intake.
-   In human experiments using SSBs, changes in MetS risk factors are a function of weight changes and calorie intake.
-   Epidemiology often fails to adequately capture the effect of calories on certain disease outcomes.
-   SSBs cause MetS to the extent that SSBs contribute to excessive caloric intake.

**References:** 

[1] [https://pubmed.ncbi.nlm.nih.gov/32644139/](https://pubmed.ncbi.nlm.nih.gov/32644139/)

[2] [https://pubmed.ncbi.nlm.nih.gov/27023594/](https://pubmed.ncbi.nlm.nih.gov/27023594/)

[3] [https://pubmed.ncbi.nlm.nih.gov/32696704/](https://pubmed.ncbi.nlm.nih.gov/32696704/)

[4] [https://pubmed.ncbi.nlm.nih.gov/3165600/](https://pubmed.ncbi.nlm.nih.gov/3165600/)

[5] [https://pubmed.ncbi.nlm.nih.gov/22866961/](https://pubmed.ncbi.nlm.nih.gov/22866961/)

#patreon_articles 
#nutrition 
#metabolic_syndrome 
#type_2_diabetes 
#sugar_sweetened_beverages 
#sugar 
#non_alcoholic_fatty_liver_disease 