As a follow up to my independent [meta-analysis](https://www.patreon.com/posts/cheese-vs-tofu-44223669) on cheese and tofu, I decided to tackle this question more directly. We understand from previous research that the ratio of polyunsaturated fat (PUFA) to saturated fat (SFA) changes the concentration of low density lipoprotein cholesterol (LDL-C). However, does this relationship hold true in the context of cheese? 

If the claim is that the lipid-modulating effects of the PUFA to SFA ratio does **not** apply to cheese, then the relationship should not hold true— altering the PUFA to SFA ratio in cheese should yield no differential effects on LDL-C. That is to say, substituting PUFA and SFA should make no difference with regards to LDL-C if the SFA from cheese is expected to have no impact on LDL-C.

Here we have two studies that attempted to answer this question in a pretty clever way [1](https://pubmed.ncbi.nlm.nih.gov/8363165/)[2](https://pubmed.ncbi.nlm.nih.gov/12428175/). In both cases full-fat cheeses were compared to cheeses that had their saturated fat content replaced with unsaturated fats. Unfortunately, I cannot access one of the papers. So I'll discuss the paper I can access.

![[1-23.png]]

As we can see it does appear as though the PUFA to SFA ratio is relevant, even when cheese is the food being compared. But how meaningful is it?

![[Pasted image 20221123154201.png]]

Here are the study results. Neither at two weeks nor four weeks does the PUFA-enriched cheese lower LDL-C to a statistically significant degree. However, results are statistically significant when both measurements are considered together as a composite (P=0.03).

![[Pasted image 20221123154205.png]]

We can also see that, much like the cheese and tofu analysis, cheese fails to increase LDL-C relative to baseline. So, it would be accurate to say that cheese probably won't increase LDL-C, but if you're replacing cheese with PUFA, you can probably expect LDL-C to decrease. 

![[Pasted image 20221123154154.png]]

In the aggregate, the PUFA-enriched cheese lowers LDL-C compared to baseline. Which means that even in the context of cheese, the ratio of PUFA and SFA still maintain the same relationship with LDL-C. When PUFA replaces SFA, LDL-C tends to drop.

However, since cheese does not increase LDL-C to the degree we would expect given its SFA content, it would be most accurate to say that cheese likely blunts the hyperlipidemic effect normally observed with SFA consumption.

**Key points:**

-   SFA-rich cheese does not seem to increase LDL-C
-   PUFA-enriched cheese lowers LDL-C.
-   The ratio of PUFA to SFA still affects LDL-C, even when cheese is the exposure.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/8363165/](https://pubmed.ncbi.nlm.nih.gov/8363165/) 

[2] [https://pubmed.ncbi.nlm.nih.gov/12428175/](https://pubmed.ncbi.nlm.nih.gov/12428175/)

#patreon_articles 
#nutrition 
#cheese 
#dairy 
#LDL 
#ApoB 