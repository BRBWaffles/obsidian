We've likely heard before that low carb high fat diets (LCHF) are good for diabetes because they supposedly improve insulin sensitivity. The idea that tends to make the rounds within LCHF circles is that insulin resistance is a consequence of prolonged exposure to insulin. It isn't, as I've discussed [here](https://thenutrivore.blogspot.com/2019/10/sugar-doesnt-cause-diabetes-and-ketosis.html). But, that's their idea. Leaving aside questions about whether or not LCHF has independent utility for diabetes treatment or management, let's just tackle this central question— do LCHF diets reverse insulin resistance?

Luckily, there have been a number of studies looking into this question [1](https://www.ncbi.nlm.nih.gov/pubmed/11237931)[2](https://www.ncbi.nlm.nih.gov/pubmed/15310747)[3](https://www.ncbi.nlm.nih.gov/pubmed/11679437). Across the board, isocaloric substitutions of carbs for fat yield consistent and predictable effects on glucose disposal and insulin sensitivity. Using a technique called a hyperinsulinemic euglycemic clamp test, researchers can actually measuring the differences in insulin sensitivity by observing glucose disposal in real-time. 

As a general principle, LCHF feeding tends to reduce insulin sensitivity and glucose disposal on average. Whereas high carb low fat (HCLF) feeding tends to increase insulin sensitivity and glucose disposal on average. 

So, from where does the confusion arise? Well, insulin sensitivity is typically reported using HOMA-IR in the LCHF literature [4](https://www.ncbi.nlm.nih.gov/pubmed/31231311)[5](https://www.ncbi.nlm.nih.gov/pubmed/23155696)[6](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3981696/). HOMA-IR is an indirect measurement of insulin sensitivity. It is merely a calculation that considers fasting glucose and fasting insulin in order to generate an insulin sensitivity score. However, this is not a gold standard tool and is not validated for use in LCHF subjects. LCHF diets reduce fasting insulin and fasting glucose, so HOMA-IR tends to produce unrealistically favourable results for LCHF diets. 

**Key points:** 

-   LCHF diets induce a state of insulin resistance and HCLF diets improve insulin sensitivity, as demonstrated by direct measurements of insulin sensitivity.
-   LCHF studies tend to report insulin sensitivity via HOMA-IR, which is not a direct measurement of insulin sensitivity and isn't validated in LCHF subjects.

**References**

[1] Bisschop PH, et al. Dietary fat content alters insulin-mediated glucose metabolism in healthy men. Am J Clin Nutr. 2001 Mar. [https://www.ncbi.nlm.nih.gov/pubmed/11237931](https://www.ncbi.nlm.nih.gov/pubmed/11237931) 

[2] Pehleman TL, et al. Enzymatic regulation of glucose disposal in human skeletal muscle after a high-fat, low-carbohydrate diet. J Appl Physiol (1985). 2005 Jan. [https://www.ncbi.nlm.nih.gov/pubmed/15310747](https://www.ncbi.nlm.nih.gov/pubmed/15310747) 

[3] Bachmann OP, et al. Effects of intravenous and dietary lipid challenge on intramyocellular lipid content and the relation with insulin sensitivity in humans. Diabetes. 2001 Nov. [https://www.ncbi.nlm.nih.gov/pubmed/11679437](https://www.ncbi.nlm.nih.gov/pubmed/11679437) 

[4] Athinarayanan SJ, et al. Long-Term Effects of a Novel Continuous Remote Care Intervention Including Nutritional Ketosis for the Management of Type 2 Diabetes: A 2-Year Non-randomized Clinical Trial. Front Endocrinol (Lausanne). 2019 Jun. [https://www.ncbi.nlm.nih.gov/pubmed/31231311](https://www.ncbi.nlm.nih.gov/pubmed/31231311) 

[5] Partsalaki I, et al. Metabolic impact of a ketogenic diet compared to a hypocaloric diet in obese children and adolescents. J Pediatr Endocrinol Metab. 2012. [https://www.ncbi.nlm.nih.gov/pubmed/23155696](https://www.ncbi.nlm.nih.gov/pubmed/23155696) 

[6] Laura R. Saslow, et al. A Randomized Pilot Trial of a Moderate Carbohydrate Diet Compared to a Very Low Carbohydrate Diet in Overweight or Obese Individuals with Type 2 Diabetes Mellitus or Prediabetes. PLoS One. 2014. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3981696/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3981696/)

#patreon_articles 
#nutrition 
#disease 
#insulin_sensitivity 
#insulin 
#low_carb 
#keto 
#type_2_diabetes 
#metabolic_syndrome 