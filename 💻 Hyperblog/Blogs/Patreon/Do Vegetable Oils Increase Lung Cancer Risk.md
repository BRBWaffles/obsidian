The primary evidence used to support the claim that vegetable oils increase the risk of lung cancer usually comes from an IARC report that was published back in 2010 [1](https://www.ncbi.nlm.nih.gov/books/NBK385523/pdf/Bookshelf_NBK385523.pdf). All in all, the report includes 24 case-control studies and an enormous amount of mechanistic speculation. I'll explain why it doesn't significantly move my needle on the question. 

Right off the bat, I should say that this document is largely an equivocation. Because I don't think that people deep-frying >3 meals a day in small Chinese bedrooms is typically what vegetable oil alarmists are referring to when they claim vegetable oils increase "our" risk of lung cancer. But we can explore the findings anyway. 

First off, going from a high-PUFA oils to a high-MUFA oils in these case-control studies pretty consistently increases the relative risk of lung cancer. Which would seem to contradict the hypothesis that MUFA would have much superiority over PUFA for this endpoint. 

![[1-95.png]]

You may have also noticed that boiling food was also associated with an increased risk of lung cancer. Which seems very odd. But, the results could be reconciled when we realize that a lot of the people included in these studies were using things like wood- and coal-burning stoves to cook their food.

It's plausible that the risk is just tracking cooking frequency, and that people who cooked at home more often with these methods were more likely to be exposed to wood smoke. 

Some of the case-control studies didn't even report relative risks relevant to vegetable oils. As some of them only report relative risk for years spent cooking in a bedroom, and the type of cooking fuel was not adjusted for. There are many more ways to produce smoke than heating vegetable oils in a pan. Hell, you can produce "kitchen fumes" by scorching butter in a pan or burning food with or without the use of oils.

Most of the remaining case-control studies report their relative risks as function of deep frying with no adjustment for anthropometrics or confounders like ventilation or the use of wood- or coal-burning stoves. One study actually failed to report the confidence internals for their relative risk and had zero adjustments, haha.

This one is truly hilarious, as they perform multiple sensitivity analyses that test for the effect of a few variables that could be potentially confounding. Such as windows, ventilation, and socioeconomic status. Unfortunately the type of cooking fuel was not adjusted for.

![[1-94.png]]

A significant increase in risk was only found in those consuming two or more meals per day cooked in the home. Again, wood smoke could be confounding here. But even if the association was reflecting a "real" effect, so to speak, there are no dietary guidelines from around the world encouraging anyone to eat the majority of their meals fried in a pan.

The next one probably has one of the best adjustment models out of the lot, and they find no statistically significant increase in risk among non-smokers. Which is interesting, because it raises the possibility that in-home cooking may also be a correlate for cigarette smoking for some reason.

![[Pasted image 20221123154022.png]]

Now that we've reached the end of the list, it is important to emphasize that these are case-control studies. Case-control studies are retrospective in nature and cannot be used to assess temporality. This means that they are extremely ill-equipped to inform causal inference. The associations are interesting, but it's not clear whether or not they are particularly useful.

What dissatisfies me the most with these case-control studies is that in most of the analyses, it is unknown if vegetable oils are truly the source of the "kitchen fumes" or "kitchen smoke", as I've discussed already. It's even possible that the risk could still be tracking cigarette smoking.

To try to get around this, I aggregated all of the data that was specific to cooking with vegetable oils. In an attempt to make sure that lower PUFA oils were always the comparator, I had to make some of the risk ratios inverse. But here are the results:

**Random Effects Model**

RR 0.93 (CI 0.68-1.27), P=0.64

![[Pasted image 20221123154041.png]]

**Fixed Effects Model:**

RR 0.91 (CI 0.78-1.05), P=0.20

![[Pasted image 20221123154044.png]]

In the aggregate, cooking with higher PUFA oils results in a non-significant decrease in lung cancer risk. Neither of these results should cause us to run for the hills when we see a deep fryer. Chances are good that the results of these case-control studies are tracking some other exposure. Like kang use, coal stoves, wood stoves, overconsumption, or even smoking. Especially since two studies showed an increased risk of boiling food, haha.

Thankfully, there is also a meta-analysis of prospective cohort studies investigating the relationship between PUFA and lung cancer, which showed a linear, non-significant decrease in risk with higher intakes [2](https://pubmed.ncbi.nlm.nih.gov/24925369/).

![[Pasted image 20221123154050.png]]

However, many of the included risk ratios were specific to fish. If we limit the risk ratios to just those that investigated total PUFA, and not fish specifically, we see no significant association with lung cancer risk. 

![[Pasted image 20221123154053.png]]

This is very poor evidence for an effect in either direction. But it certainly doesn't appear as though PUFA consumption is associated with a statistically significant increase in the risk of lung cancer.

**Key points:**

-   The evidence for vegetable oils increasing lung cancer risk is very poor quality with a high risk of confounding.
-   Higher quality evidence shows no significant effect of PUFA on lung cancer risk.

**References:**

[1] [https://www.ncbi.nlm.nih.gov/books/NBK385523/pdf/Bookshelf_NBK385523.pdf](https://www.ncbi.nlm.nih.gov/books/NBK385523/pdf/Bookshelf_NBK385523.pdf)

[2] [https://pubmed.ncbi.nlm.nih.gov/24925369/](https://pubmed.ncbi.nlm.nih.gov/24925369/)

#patreon_articles 
#vegetable_oil 
#lung_cancer 
#disease 
#nutrition 