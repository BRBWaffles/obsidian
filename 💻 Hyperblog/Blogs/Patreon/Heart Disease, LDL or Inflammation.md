It's an age old question. Well, no, not really. It's actually just some fuckery that low carb knobs like to talk about. The question is whether or not low density lipoproteins (LDL) are causal of atherosclerotic cardiovascular disease (ASCVD) in the context of low inflammation. Luckily we have plenty of data on this question. So let's dive in.

Firstly, we have epidemiological data that aims to divide a population up into quartiles related to acute myocardial infarction (AMI) [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7343474/). These quartiles have varying levels and combinations of exposure to either high LDL, or high C-reactive protein (CRP): high LDL and high CRP, high LDL and low CRP, low LDL and high CRP, and low LDL and low CRP.

![[Pasted image 20221123154714.png]]

Looking carefully at this graph, we see that risk is lowest when both LDL and CRP are both low. But risk does go up in the context of low LDL and high CRP as well. The absolute worst situation to be in is with high LDL and high CRP. That group is in its own league in terms of risk.

So, these data would seem to suggest that, yes, high inflammation does carry an independent risk of increased AMI, and presumably ASCVD. However, let's take another look at the graph. The green line is representing the risk of AMI in the context of high LDL and low CRP. This is the context low carb people love to speculate about, and it shows that LDL carries an independent risk all on its own as well.

Recently, subgroup analyses on the FOURIER trial have also been done investigating the relationship between inflammation and ASCVD endpoints [2](https://www.ahajournals.org/doi/full/10.1161/CIRCULATIONAHA.118.034032). The results essentially show the same thing.

![[Pasted image 20221123154709.png]]

Again, we see that having both high LDL and high CRP together yields the worst outcomes, having either elevated in isolation increases risk, and having both as low as possible maximally decreases risk. Now, granted we can't technically call this experimental data as far as CRP is concerned, since this is a post-hoc, observational analysis. Nevertheless, it shows the exact same hierarchy of effect as the epidemiology.

Here's a representation of the data that is a little easier on the eyes:

![[Pasted image 20221123154659.png]]

Now, why isn't inflammation a target for therapy if it's so damn important? Well, the truth is we have attempted to target inflammation. The results have been almost universally unsuccessful [3](https://www.wjgnet.com/2220-6132/full/v8/i1/1.htm)[4](https://pubmed.ncbi.nlm.nih.gov/30560921/). We target LDL specifically because it is the most reliable target for therapy at this time. Interventions aimed to lower LDL reduce the risk of AMI and ASCVD, but interventions aimed to lower inflammation do not.

**Key points:**

-   LDL and inflammation are both independently causal of ASCVD.
-   Inflammation causes ASCVD in both high and low LDL contexts.
-   LDL causes ASCVD in the context of both high or low inflammation.
-   Interventions to lower inflammation have largely been unsuccessful in reducing the risk of ASCVD.
-   Interventions to lower LDL have been overwhelmingly successful in reducing the risk of ASCVD.

**References:**

[1] [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7343474/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7343474/)

[2] [https://www.ahajournals.org/doi/full/10.1161/CIRCULATIONAHA.118.034032](https://www.ahajournals.org/doi/full/10.1161/CIRCULATIONAHA.118.034032)

[3] [https://www.wjgnet.com/2220-6132/full/v8/i1/1.htm](https://www.wjgnet.com/2220-6132/full/v8/i1/1.htm)

[4] [https://pubmed.ncbi.nlm.nih.gov/30560921/](https://pubmed.ncbi.nlm.nih.gov/30560921/)

#patreon_articles 
#disease 
#LDL 
#inflammation 
#cardiovascular_disease 
