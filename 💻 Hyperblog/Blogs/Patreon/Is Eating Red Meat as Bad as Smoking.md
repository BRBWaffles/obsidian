Many in the vegan world have drawn many dubious parallels between animal food consumption and cigarette smoking. However, red meat and smoking is the only comparison that I've seen that actually seems to have a spark of validity to it. Omnivores, please hear me out, haha.

It all started when I stumbled across this meta-analysis investigating the effects of low to moderate smoking on coronary heart disease (CHD) risk [1](https://pubmed.ncbi.nlm.nih.gov/29367388/). If we turn our attention to men, we see that the relative risk of CHD with smoking one cigarette per day is 1.48 (1.30 to 1.69). 

![[1-75.png]]

This is interesting, because it is often claimed by a great number of diet quacks from all corners of the internet that if a relative risk is below two, then the result is noise. However, with smoking and CHD, that goalpost isn't met until smoking rates exceed 20 cigarettes per day. That's a lot. Are we really willing to say, on the basis of these risk ratios that smoking 1-5 cigarettes per day is just noise, and doesn't increase risk? If the answer is "no", then I'd like you to consider the next piece of evidence carefully.

Moving on, let's look at this Japanese cohort study investigating the relationship between CHD and red meat in both men and women [2](https://pubmed.ncbi.nlm.nih.gov/33320898/). Let's stick to comparing men to men, so that we're actually comparing apples to apples as best we can. As we can see, the relative risk of CHD with increasing red meat intake is actually higher than it is for smoking, at 1.51 (1.11 to 2.06).

![[1-74.png]]

Now, I know what you're going to say. Perhaps red meat is merely a correlate for other unhealthy behaviours, like it is here in the West. I'm afraid not. Healthy and unhealthy behaviours were extremely well balanced between the quartiles of red meat intake. In fact, it seems that red meat consumption was, rather counter-intuitively, a correlate for many _healthy_ behaviours.

![[Pasted image 20221123155039.png]]

Among the behaviours that trended in a presumably unhealthy direction, they did not differ by much. For example, differences in fruit intake were equal to approximately 1/3 of a bite of an apple. Differences in egg consumption were equal to about a 1/6 of an egg. Vegetables differed by a few leaves of spinach. There is no persuasive evidence that the healthy user bias is confounding here.

This is also the reason why I did not select a meta-analysis on the association between red meat and CHD, even though those meta-analyses also tend to show an increase in risk [3](https://pubmed.ncbi.nlm.nih.gov/29039970/). 

![[Pasted image 20221123155050.png]]

However, many cohorts in this particular meta-analysis could still be confounded by the healthy user bias. Also, not all of the cohort studies in this meta-analysis used particularly robust adjustment models, either. In this instance, I would trust a single, well-designed, well-powered prospective cohort study over an entire meta-analysis of prospective cohort studies on the same research question. 

In conclusion, I do believe that intakes of red meat exceeding approximately 90g/day do actually robustly associate with the risk of CHD, and the effect size is not terribly dissimilar to that of smoking one cigarette per day. However, it is unclear what sort of effect the healthy user bias could be having on the relationship between CHD and smoking. Unfortunately, prospective cohort studies investigating smoking and disease outcomes rarely report (or even adjust for) many of those covariates.

**Key points:**

-   The relative risk of CHD with smoking one cigarette per day is 1.48 (1.30 to 1.69).
-   The relative risk of CHD with eating >90g of red meat per day is 1.51 (1.11 to 2.06).
-   There is no obvious reason to weight the validity of these findings differently.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/29367388/](https://pubmed.ncbi.nlm.nih.gov/29367388/) 

[2] [https://pubmed.ncbi.nlm.nih.gov/33320898/](https://pubmed.ncbi.nlm.nih.gov/33320898/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/29039970/](https://pubmed.ncbi.nlm.nih.gov/29039970/)

#patreon_articles 
#nutrition 
#meat 
#red_meat 
#smoking 
