It's December! Which means that many of us are likely to be sucking down discount chocolate like it's going out of style, haha. So, as a special Christmas blog article, I'll be discussing why you might not need to worry about what that chocolate is doing to your LDL. Enjoy!

First off, chocolate does not seem to increase LDL, despite it being loaded with saturated fat. But why? Since chocolate is basically just two main ingredients (ground cocoa and cocoa butter) we should explore this question by looking at the effects of these ingredients together as well as in isolation, one at a time.

In the aggregate, cocoa products like chocolate counterintuitively tend to lower LDL in humans [1](https://pubmed.ncbi.nlm.nih.gov/21559039/). However, a handful of studies have attempted to investigate potential explanations for this phenomenon, and have isolated a few components within the cocoa products themselves to have LDL-lowering properties [2](https://pubmed.ncbi.nlm.nih.gov/18716168/)[3](https://pubmed.ncbi.nlm.nih.gov/15190043/). 

Grassi et al. (2008) investigated the effects of dark chocolate on LDL and blood pressure compared to white chocolate, which is excellent methodology if you want to isolate the effect of the cocoa itself. This is because white chocolate is basically just sweetened cocoa butter without the ground cocoa. Ultimately the researchers found that the dark chocolate lowered LDL by 7%, as well as improved blood pressure.

Engler et al. (2004) employed a similar methodology, but using low-flavanol verses high-flavanol cocoa drinks. For both the intervention and control groups, the within-group differences in LDL were non-significant. However, the between group treatment effect was statistically significant.

Altogether this would suggest that ground cocoa is at least one of the components of chocolate that could plausibly contribute to its LDL-lowering effects.

Now on to cocoa butter. The primary saturated fat in cocoa butter is stearic acid, which hasn't really been persuasively shown to increase LDL in controlled human feeding studies [4](https://apps.who.int/iris/handle/10665/246104). But why is this? To answer this, we have to explore what happens to stearic acid once it is consumed.

Rodríguez-Morató et al. (2020) recently elucidated plausible explanations for the neutral effect of stearic acid on blood lipids by feeding subjects radio-labeled stearic acid to determine its metabolic fate [5](https://pubmed.ncbi.nlm.nih.gov/32998517/).

Basically, the mechanism seems to be twofold. Firstly, much of the stearic acid seems to be readily converted to oleic acid and palmitoleic acid, which are LDL-lowering monounsaturated fats. The minority of the stearic acid is converted to LDL-raising palmitic acid.

![[1-83.png]]

Secondly, stearic acid seems to be preferentially esterified to cholesterol to form cholesteryl esters, which is a similar mechanism to the one that is responsible for the LDL-lowering effects of polyunsaturated fat.

Interestingly enough, this study is probably one of the most well-controlled stearic acid feeding experiments ever done, and it showed that feeding stearic acid actually lowered LDL by 13%. However, LDL was quite high to begin with in this subject pool. For normolipidemic subjects, the effects of stearic acid on LDL would likely be non-significant.

In conclusion, dark chocolate and cocoa products may be high in saturated fat, but they actually tend to lower LDL through a constellation of mechanisms. These mechanisms likely include pleiotropic effects of polyphenols, inhibited enterohepatic cholesterol circulation by dietary fibre, and unique biochemical properties of the stearic acid found in cocoa butter.

**Key points:**

-   Cocoa products like dark chocolate tend to lower LDL.
-   Cocoa itself tends to lower LDL via polyphenols and fibre.
-   Cocoa butter doesn't tend to raise LDL due to unique effects of stearic acid.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/21559039/](https://pubmed.ncbi.nlm.nih.gov/21559039/) 

[2] [https://pubmed.ncbi.nlm.nih.gov/18716168/](https://pubmed.ncbi.nlm.nih.gov/18716168/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/15190043/](https://pubmed.ncbi.nlm.nih.gov/15190043/) 

[4] [https://apps.who.int/iris/handle/10665/246104](https://apps.who.int/iris/handle/10665/246104) 

[5] [https://pubmed.ncbi.nlm.nih.gov/32998517/](https://pubmed.ncbi.nlm.nih.gov/32998517/)

#patreon_articles 
#nutrition 
#chocolate 
#LDL 
#stearic_acid 
#polyphenols 
#metabolism 
