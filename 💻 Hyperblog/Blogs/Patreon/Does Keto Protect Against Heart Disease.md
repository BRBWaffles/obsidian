The low carb crackpot, Dave Feldman, has popularized the idea that ketogenic diets may offer unique protection against cardiovascular disease (CVD), due to the diet's tendency to lower triglycerides (TG) and increase high-density lipoprotein cholesterol (HDL). This is because it has been shown that lower TG/HDL ratios are more reliable disease correlates than most other traditional metrics [1](https://pubmed.ncbi.nlm.nih.gov/18719750/).

![[1-42.png]]

But why? The TG/HDL ratio is essentially a reflection of atherogenic dyslipidemia. Atherogenic dyslipidemia is a lipid phenotype that is characterized by high TG, low HDL, and either high or low low-density lipoprotein cholesterol (LDL). The lipid phenotype is typically concomitant with metabolic syndrome and type 2 diabetes.

The notion that ketogenic diets uniquely correct this deleterious lipid phenotype, and thus protect against cardiovascular disease (CVD), seems to center around the fact that ketogenic diets tend to increase HDL and reduce TG. But does this make ketogenic diets less atherogenic? Let's find out.

The reason the TG/HDL ratio is a decent correlate for CVD outcomes is not because the ratio itself does something bad. It's because the ratio is also a robust correlate for low-density lipoprotein particle number (LDLp) [2](https://pubmed.ncbi.nlm.nih.gov/15296705/). LDLp can also be represented as apolipoprotein B (ApoB), and is probably the most robust CVD risk predictor other than age.

![[Pasted image 20221123154342.png]]

Essentially, the higher your TG and the lower your HDL, the more likely it is that you will have a high ApoB. As I've discussed [before](https://www.patreon.com/posts/does-ldl-cause-43573104), ApoB is the causal agent in CVD. As such, even if one were to correct their TG/HDL ratio with a ketogenic diet, it would be reasonable to suspect that if ApoB remained high they haven't done much to mitigate their CVD risk. So, what do ketogenic diets typically do to ApoB?

![[1-43.png]]

This is from my [meta-analysis](https://thenutrivore.blogspot.com/2020/10/low-carbohydrate-diets-and-health.html) of ketogenic diets. In the aggregate, ketogenic diets don't appear to increase ApoB to a statistically significant degree (P=0.14). However, non-ketogenic low carb diets do not appear to have this effect. In fact, they appear to lower ApoB by 0.05 g/L, and the results were statistically significant (P=0.02). This is probably good news.

Not everyone reports this effect of a ketogenic diet, however. Many report extremely high LDL levels on the diet [3](https://thenutrivore.blogspot.com/2020/09/dave-feldmans-rhetoric-is-dangerous.html). Not to mention the fact that we do have epidemiology investigating the relationship between high LDL and CVD endpoints in the context of a low TG/HDL ratio [4](https://pubmed.ncbi.nlm.nih.gov/25458651/).

![[1-17.jpg]]

Of all of the high-LDL phenotypes, it is certainly optimal to maintain lower TG and higher HDL. However, it is still not as good as having normal lipids overall. This looks great for ketogenic diets, if you think about it. Higher TG could carry a residual, independent CVD risk, even after controlling for ApoB [5](https://pubmed.ncbi.nlm.nih.gov/32203549/). However, this is not always observed [6](https://pubmed.ncbi.nlm.nih.gov/30694319/). 

I think we all appreciate that ketogenic diets have a tendency to lower TG. So, for the sake of argument let's just grant that lowering TG is independently beneficial for CVD risk reduction. With that in mind, let's investigate the relationship between ketogenic diet and TG when stratified by weight loss:

![[1-44.png]]

According to this analysis, ketogenic diets fail to lower TG unless weight loss was achieved (P=0.67). However, if weight loss is achieved, a ketogenic diet may lower TG by 22-47mg/dL. So, there doesn't seem to be anything uniquely protective about a ketogenic diet with regards to how it affects either TG or ApoB, independent of weight loss. For this reason I would suspect that ketogenic diets likely don't improve CVD risk by improving the TG either.

In conclusion, do ketogenic diets uniquely protect against CVD as a function of correcting the TG/HDL ratio? Probably not. That being said, ketogenic diets don't seem to have any uniquely atherogenic tendencies, either. However, if you are on a ketogenic diet and experience a pathological increases to your ApoB, you're probably worse off than you were.

**Key points:**

-   Ketogenic diets typically lower TG and increase HDL.
-   The TG/HDL ratio is a strong correlate for CVD outcomes because it is also a correlate for ApoB.
-   ApoB is a causal agent in the pathogenesis and pathophysiology of CVD.
-   Ketogenic diets do not tend to increase ApoB.
-   Ketogenic diets do not improve TG independent of weight loss.
-   Ketogenic diets are not likely to be uniquely protective against CVD as a function of their "tendency" to lower the TG/HDL ratio.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/18719750/](https://pubmed.ncbi.nlm.nih.gov/18719750/)

[2] [https://pubmed.ncbi.nlm.nih.gov/15296705/](https://pubmed.ncbi.nlm.nih.gov/15296705/)

[3] [https://thenutrivore.blogspot.com/2020/09/dave-feldmans-rhetoric-is-dangerous.html](https://thenutrivore.blogspot.com/2020/09/dave-feldmans-rhetoric-is-dangerous.html)

[4] [https://pubmed.ncbi.nlm.nih.gov/25458651/](https://pubmed.ncbi.nlm.nih.gov/25458651/)

[5] [https://pubmed.ncbi.nlm.nih.gov/32203549/](https://pubmed.ncbi.nlm.nih.gov/32203549/)

[6] [https://pubmed.ncbi.nlm.nih.gov/30694319/](https://pubmed.ncbi.nlm.nih.gov/30694319/)

#patreon_articles 
#nutrition 
#disease 
#keto 
#cardiovascular_disease 
#coronary_heart_disease 
#LDL 
#ApoB 
#HDL 
#triglycerides 
