In continuation of what I wrote [here](https://www.patreon.com/posts/anti-nutrients-1-32218661), I'd like to expand on the role of phytate in the human diet. Last time we talked about its negative effects on mineral bioavailability and absorption. This time we're going to talk about all of the ways in which phytate can be good for us, and why some people would be wise to include it in their diet.

The first reason phytate is great is also the same reason it sucks. It binds minerals and keeps us from absorbing them. But, for some people that's a good thing. People with a condition called hemochromatosis often hyper-absorb the iron in their diet. This can lead to over-saturation of their iron stores, resulting in increased oxidative stress and a number of other unpleasant symptoms. 

It has been speculated that if those with hemochromatosis made an effort to pair their iron-rich foods with their phytate-rich foods, they could very well make traction against their symptoms [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4680572/). As a proof-of-principle, I'll direct your attention to a drug called [Deferasirox](https://en.wikipedia.org/wiki/Deferasirox). It operates according to a very similar mechanism to that of phytate, and is used for managing hemochromatosis symptoms.

The second way phytate may be good for us is through its somewhat puzzling ability to reduce advanced glycation end-products (AGEs) in the body [2](http:). Diabetic patients given an oral supplement of a type of phytic acid known as myo-inositol hexaphosphate showed marked reductions in AGEs as a consequence of the intervention. Total iron, ferritin, transferrin, and transferrin saturation did not differ between diet conditions. 

Additionally, the more phytate you consume, the more adept your body becomes at mitigating its negative effects [3](https://www.ncbi.nlm.nih.gov/pubmed/23551617). This is said to be operating through the gut microbiome, which adjusts its composition and enzymatic activity to accommodate for higher levels of phytate in the diet. This isn't to suggest that a persistently high phytate diet leads to phytate being a non-issue. It merely means that the more phytate you consume, the less of an issue phytate appears to be. This is good news, considering that phytate may have a number of additional benefits of in terms of disease prevention and management [4](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5067332/).

**Key points:**

-   Phytate may help those with hemochromatosis avoid symptoms of iron overload.
-   Phytate seems to help mitigate the burden of AGEs in diabetic patients.
-   Eating more phytate may make you better at mitigating its harmful effects.
-   There are many promising benefits of phytate that are being actively researched.

**References:**

[1] Robin F. Irvine, et al. There is no ‘Conundrum’ of InsP6. Open Biol. 2015 Nov. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4680572/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4680572/) 

[2] Pilar Sanchis, et al. Phytate Decreases Formation of Advanced Glycation End-Products in Patients with Type II Diabetes: Randomized Crossover Trial. Sci Rep. 2018; 8. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6018557/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6018557/) 

[3] Markiewicz LH, et al. Diet shapes the ability of human intestinal microbiota to degrade phytate--in vitro studies. J Appl Microbiol. 2013 Jul. [https://www.ncbi.nlm.nih.gov/pubmed/23551617](https://www.ncbi.nlm.nih.gov/pubmed/23551617) 

[4] Mariano Bizzarri, et al. Broad Spectrum Anticancer Activity of Myo-Inositol and Inositol Hexakisphosphate. Int J Endocrinol. 2016. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5067332/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5067332/)

#patreon_articles 
#nutrition 
#disease 
#antinutrients 
#phytate 
#clownery 
