For those who listen to [Sigma Nutrition Radio](https://sigmanutrition.com/podcasts/), you'll know that Danny always closes his podcast by asking his guests to recommend a single thing they could do each day that would improve their life. If you listened to [my interview](https://sigmanutrition.com/episode360/) on Danny's podcast, you undoubtedly remember my answer. I suggested to Danny's listeners that they masturbate to improve their lives, haha. This is a follow-up to that recommendation.

It has been observed that ejaculation frequency seems to be inversely associated with prostate cancer incidence. To the best of my knowledge, this particular research question was first investigated in a 1986 case-control study [1](https://pubmed.ncbi.nlm.nih.gov/3739124/). 

![[Pasted image 20221123153604.png]]

There seemed to be a potential protective effect of more frequent ejaculations on prostate carcinoma. The author speculates that the effect may be conveyed through higher rates of prostate epithelial cell turnover with increasing ejaculation frequency. 

Though I can't comment on epithelial cell turnover, I can say from personal experience that ejaculation volume seems to be a function of the total area under the curve of arousal during a sexual event. So, the cells are certainly doing something as a function of sexual stimulation/arousal that they otherwise wouldn't be doing, haha.

This question was revisited in a 2002 meta-analysis of case-control studies that found somewhat contradictory results [2](https://pubmed.ncbi.nlm.nih.gov/11805589/). The highest octile of ejaculations yielded a 44% and 440% increase in risk for population-based case-control studies and hospital-based case-control studies, respectively. Overall they observed an aggregated relative risk of 1.53, which is a 53% increase in the risk of prostate cancer with the highest frequency of ejaculations.

![[Pasted image 20221123153610.png]]

However, case-control studies are generally considered one of the weakest forms of observational evidence, as the assessments are all post-hoc. There are no prospective measurements that actually quantify exposures and outcomes in real-time in the populations studied.

Prospective cohort studies are considered much stronger evidence than case-control studies, and at least one prospective cohort study has investigated the relationship between ejaculation frequency and prostate cancer incidence [3](https://pubmed.ncbi.nlm.nih.gov/15069045/). The Health Professionals Follow-up Study (HPFS) was an extremely thorough, multi-decade prospective cohort study conducted in the United States. 

Ejaculation frequency was robustly, inversely associated with prostate cancer incidence. For those wanking off more than 21 times per month, a 33% reduction in the risk of prostate cancer was observed. There was also a 52% reduction in the risk of organ confined prostate cancer.

![[Pasted image 20221123153614.png]]

Twelve years later, different researchers would publish an additional ten years of prostate cancer outcome data related to ejaculation frequency in the HPFS cohort [4](https://pubmed.ncbi.nlm.nih.gov/27033442/). The same associations were observed, except this time around the authors provided pretty compelling survival curves.

![[Pasted image 20221123153618.png]]

So, based on the best available evidence, increasing the frequency of ejaculations seems to be associated with lower rates of prostate cancer overall in a relatively dose-dependent manner. 

**Key points:**

-   Ejaculating once every 1.42 days may reduce the risk of prostate cancer.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/3739124/](https://pubmed.ncbi.nlm.nih.gov/3739124/)

[2] [https://pubmed.ncbi.nlm.nih.gov/11805589/](https://pubmed.ncbi.nlm.nih.gov/11805589/)

[3] [https://pubmed.ncbi.nlm.nih.gov/15069045/](https://pubmed.ncbi.nlm.nih.gov/15069045/)

[4] [https://pubmed.ncbi.nlm.nih.gov/27033442/](https://pubmed.ncbi.nlm.nih.gov/27033442/)

#patreon_articles 
#prostate_cancer 
#masturbation
#ejactulation
#sexuality 
#disease 
