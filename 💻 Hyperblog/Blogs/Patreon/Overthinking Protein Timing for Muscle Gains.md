So, I had this awesome plan of gathering a bunch of studies investigating the effect of pre- and/or post-workout protein consumption on muscle hypertrophy during resistance training. I was going to scour the literature for studies that controlled for protein so I could fire up RevMan and write this cool, unique blog article about the anabolic window. But, after a hilarious [public shaming](https://twitter.com/kevinnbass/status/1400884888224428032?s=20) on Twitter, I discovered that the exact analysis I was planning on doing has already been done, but better than I could have done.

Basically the idea behind the so-called "anabolic window" is that shortly after resistance training, the affected muscle tissue is primed to rebuild itself, and providing protein during this period of time maximizes muscle protein synthesis (MPS). This is said to produce either more mass, more strength, or both. However, many studies that investigate this question are asymmetrically dosing protein between groups, with one group consuming a bolus of extra protein within the anabolic window.

Clearly this is problematic, because it confounds the effect. If there is an increase in MPS observed, is it due to the fact that one group is getting more protein overall? Or is it due to the fact that one group is consuming that protein within the anabolic window? Or is it some combination of the two? Thankfully, the [muscle Gods](https://twitter.com/mackinprof/status/1400880367704346626?s=20) have ascended me to a new plane of existence by granting me a link to this meta-analysis on protein timing and hypertrophy [1](https://pubmed.ncbi.nlm.nih.gov/24299050/).

Their literature search did not uncover very many studies that had actually controlled for protein intake between groups. Only about four papers out of a couple dozen actually met that criteria. But, here is the overall, unadjusted effect of protein consumption within the anabolic window.

![[1-63.png]]

The analysis would suggest that there is a statistically significant effect of eating protein within the anabolic window on measures of hypertrophy. Which would seem to make sense. But keep in mind that the vast majority of these studies are not controlling for protein. 

Limiting the analysis to the four studies that did control for protein could potentially produce issues with regards to statistical power. The resistance training literature is usually so wishy-washy that a great many studies may be required to observe some effects.

A secondary analysis was done that attempted to adjust for total protein between groups. This way we could estimate what the effect might be, had the included studies actually controlled for protein in the first place. It certainly isn't going to be perfect, but I think it is likely sufficient to inform us as to the potential effect of protein timing on hypertrophy.

![[1-64.png]]

When adjusting for total protein between groups, the effect of the "anabolic window" seems to be nullified. Which isn't surprising to me, really. I had concluded in my [previous article](https://www.patreon.com/posts/overthinking-for-49333926) on protein distribution that eating your total daily protein in an unbalanced distribution (such as one or two meals per day) does not nullify the effect of total protein on hypertrophy. While eating in a balanced distribution would appear to be optimal, you'll gain lean mass no matter how you distribute your daily protein.

The anabolic window should be viewed differently. There is a period of time ranging from about three to four hours before your workout to three to four hours after your workout wherein protein ingestion maximizes MPS [2](https://pubmed.ncbi.nlm.nih.gov/23360586/). However, eating outside of this window is not limiting for hypertrophy, as long as your total daily protein requirement is met.

In conclusion, it appears once again that simply hitting your total daily protein target is of greater importance than micromanaging your protein intake in some contrived way. Unless you're an elite athlete or striving to eek out the most gains possible, muscle hypertrophy is not that complicated— lift heavy things until your muscles feel fatigued and eat sufficient protein. If you'd like to know more about what sufficient protein could mean for you, I have also [written about this](https://www.patreon.com/posts/protein-targets-44006622) subject previously.

**Key points:**

-   It has been suggested that protein ingestion needs to be target around the time of your workout in order to make gains.
-   In reality, merely hitting your total recommended target for protein in a day is far more important.
-   As long as you are hitting your protein target and providing a sufficient anabolic stimulus through resistance training, you will make gains.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/24299050/](https://pubmed.ncbi.nlm.nih.gov/24299050/) 

[2] [https://pubmed.ncbi.nlm.nih.gov/23360586/](https://pubmed.ncbi.nlm.nih.gov/23360586/)

#patreon_articles 
#nutrition 
#exercise 
#protein 
#hypertrophy 