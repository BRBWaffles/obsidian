For those of you who are unaware, Kyle Manounis is a nutrition scientist who frequently publishes on the effects of dietary fatty acids in rodent models. He holds a number of radical views regarding diet, disease, and diet-disease relationships. For example, he has particular disdain for polyunsaturated fatty acids, and considers them undesirable, despite the overwhelming evidence of benefit in humans. 

Apparently Kyle has published the [first video](https://www.youtube.com/watch?v=h_9aG01n2Oc) in a new series on heart disease. Given how egregious it was, I've decided to make it my mission to fact-check each of the videos in his series as they are published. So let's get into the first one.

[**1:26**](https://youtu.be/h_9aG01n2Oc?t=86)

**Claim:** The lipid hypothesis is untenable if several carefully selected datasets reveal results contrary to its principles.

**Fact:** This is an example of a fallacy in the philosophy of science known as naive falsification. Naive falsification is invalid under the Duhem–Quine thesis, which argues that hypotheses can never be tested in truly assumption-free conditions. This means that if our hypotheses fail to predict results, we are never truly certain whether or not this is due to an error with the hypothesis or an error with our knowledge. 

For this reason, we assess the likelihood of causality using confidence, because absolute certainty will always be unattainable. Confidence is built on what we observe in the aggregate. As such, outliers do not overthrow hypotheses.

[**6:50**](https://youtu.be/h_9aG01n2Oc?t=410)

**Claim:** The diet-heart hypothesis would posit that dietary cholesterol consumption would predict serum cholesterol concentration.

**Claim:** The lipid hypothesis and diet-heart hypothesis should be able to predict heart disease, as a function of serum cholesterol levels.

**Claim:** Interventions to lower serum cholesterol should predict reductions in heart disease outcomes.

**Fact:** He’s equivocating by using serum cholesterol and LDL-cholesterol as interchangeable terms, and he has even gone so far as to mention that LDL particles (LDL-P) are the focus of both the lipid hypothesis and the diet-heart hypothesis. No credible, modern interpretation of either hypothesis is concerned with total serum cholesterol (TC). Apolipoprotein B-containing lipoproteins (ApoB) are the current targets for therapy.

[**9:04**](https://youtu.be/h_9aG01n2Oc?t=545) 

**Claim:** The 1988 paper titled “Cholesterol and lipids in the risk of coronary artery disease - the Framingham Heart Study” authored by WP Castelli states that "35 years of data suggest factors other than total or low density lipoprotein (LDL) cholesterol must be considered when evaluating [coronary artery disease] (CHD) risk. Low levels of high density lipoprotein cholesterol (HDL-C) needed for predictive power: total cholesterol/HDL".

**Fact**: The paper actually states that LDL-cholesterol is predictive of CHD, and that non-HDL-cholesterol (non-HDL-C) is most robustly predictive. This is perfectly consistent with what the modern lipid hypothesis would predict, as it recognizes non-HDL-C as one of the most validated marker of ASCVD risk [1](https://pubmed.ncbi.nlm.nih.gov/28444290/).

![[0L5tTbvNaPndZVTxy5kEH0b7wZVWT7S_muIE6Jp2geu4UBg5pte940rR4FrTcEVPvlLv-73QNKmmLMvcJQqdjz_pPMvUkNQhvuBT.png]]

This is because non-HDL-C is a near-perfect correlate for ApoB. Remember, ApoB is the primary target for therapy in the prevention of ASCVD.

![[m9kF7u1MTifBZL5jccuheHUTKq6Fh2KAiNHjJhofzY7GT8tDDLX1xX74IyxFJXp7kbahmj3tcS0kpmJQyRXm9t2w2qQk1wKHptrJ.png]]

However, that actually isn't to say that the older risk correlates are no longer reliable. TC and HDL are still a tight correlate for CVD events (AMI) in a dose-dependent manner [2](https://pubmed.ncbi.nlm.nih.gov/25568296/). In terms of risk, non-HDL-C has better predictive power than LDL-C [3](https://pubmed.ncbi.nlm.nih.gov/22453571/). So naturally it is considered over LDL-C in an assessment of ASCVD risk. Again, this is because non-HDL-C is an extremely tight correlate for ApoB [4](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3056766/).

![[E4J9gGioMg-RMWKRBf9DYzwqh2sLmsGcLZYsMBOeIj5X7G9seK8xPoKRvjz_8KALdYsGd4KJ-6erYdQ-uPC-4mgaO0HLZ5pDwzsf.png]]

[**11:21**](https://youtu.be/h_9aG01n2Oc?t=679)

**Claim:** More markers for ASCVD progression are being proposed, not because these markers are actually better discoveries, but because it is an attempt to explain heterogeneity in the results.

**Fact:** Yes, as discussed above, there is heterogeneity when looking at LDL-C, TC, or even HDL-C in isolation. However, they are all still correlates for risk. We just have better correlates these days. The discovery of better correlates does not invalidated previously discovered correlates. Right now, the best correlate we have is either LDL-P or ApoB [5](https://pubmed.ncbi.nlm.nih.gov/21392724/)[6](https://pubmed.ncbi.nlm.nih.gov/30694319/)[7](https://pubmed.ncbi.nlm.nih.gov/32203549/).

![[kfdabcw1EzLqsFb12DJo3ss6D4R3bmQcnlfm8G_FD_Jm6En0AJMEwHutl53p0L_t6ohRtEwUPRvchu-a7l7wIgBrM01FuBm4zufU.png]]

The only correlate that survives multivariable adjustment is ApoB at the end of the day.

[**11:50**](https://youtu.be/h_9aG01n2Oc?t=709) 

**Claim:** No statistically significant differences in serum cholesterol with regards to egg consumption, which contradicts the diet-heart hypothesis.

**Fact:** The relationship between dietary cholesterol and TC is a hyperbolic curve [8](https://pubmed.ncbi.nlm.nih.gov/1534437/). If the background diet of the subjects was already sufficiently high enough in dietary cholesterol, you would not expect to see a robust effect.

![[6vNu8uFpKZVX3qh4bcBlWMPmKy-yYR_BW-h7hAm-WGG7zV4OWs-iEdlSLH6bVC3mVOiWy_euM13LClwrB-n4DQ5UiANHGsrkshDO.png]]

Meta-regression analyses on cholesterol-feeding studies divulge the non-linearity in the effect of dietary cholesterol on blood lipids, particularly LDL-C [9](https://pubmed.ncbi.nlm.nih.gov/30596814/). With intakes of dietary cholesterol exceeding 300mg/day, it is statistically unlikely that you would continue to see increases in LDL-C.

![[HVxrrrcwIH4Kgu8Kv0Px3wA1lvgeg1v1SMJDKWyI9RtazULQokAqgvD_M4sE1tPjcEd_Rm1AGfFALscrnwzfZRee8EVYcLSO4B28.png]]

While dietary cholesterol is part of the Keys equation, which is an equation that is used to predict the effect of dietary lipids on blood lipids, dietary cholesterol was never a primary consideration for the diet-heart hypothesis [10](https://academic.oup.com/jn/article-abstract/59/1/39/4722525).

![[pjLYKPrQCY2wj6J6yh63jktj6e5H4j7pomxz9wGDIVe9sQo8cWhnI2XEznOci2xzYBbni46tPyweiG2ultqAV9sjinzFiYY5pX8T.png]]

In fact, Ancel Keys himself had stated that dietary cholesterol was not likely to be a significant contributor to ASCVD risk.

![[HbWG2hVKTppNaob8CJS_CpDOYd9Qujg--p-aXWQwrWSDCwozmz1Wo2OJ90KkShxN_mPjpI2QFaBN2gYoPks_8FyUQT5T2fylvGS4.png]]

![[vMAtMf7I_llynE4plxW-QkBeMlk3GHOKm_iQj4PImEOLx3vdQU5G3I6nP3vzPDh3lecF0pHHV6y3aeEylQZURGZGxwRBnzB-5U1y.png]]

[**13:43**](https://youtu.be/h_9aG01n2Oc?t=823)

**Claim:** Contradictory findings mean that you “don’t have anything”. Which is to say that the hypothesis is wrong if it fails to predict an outcome.

**Fact:** As previously mentioned, this would only makes sense if scientific investigation was assumption-free. Inconsistent findings could result from covariates or confounders that have not been identified and accounted for in some datasets versus other datasets.

Also, lacking an association does not necessarily mean that there is nothing there. It could just as easily mean that more investigation is needed so we can recalibrate our assumptions. For example, testing for linear effects when the effect is non-linear can easily hide associations.

[**17:10**](https://youtu.be/h_9aG01n2Oc?t=1031) 

**Claim:** Decreases in cardiovascular disease mortality were concomitant with increases in serum cholesterol levels in Japanese populations.

**Fact:** The 1980s saw an explosion in advancements to medical technology that led to better treatment of ASCVD. In relation to the diet-heart hypothesis, this is best observed by looking at the North Karelia Project [11](https://pubmed.ncbi.nlm.nih.gov/19959603/).

![[nB9abdWnslsyX6h1PqWB07bJcY_FTGvvz-OgaWrLicDvzIgosObIUrb8M6I_MVkp7wjDf873yIkenfg4J6fBatDEMr8GYwjxQQmb.png]]

Reductions in serum cholesterol as a consequence of replacing butter with unsaturated fats accounted for 50% of the 30% reduction in CHD mortality between 1972 and 1985. However, the modeled reductions in CHD mortality diverge from the observed reductions in CHD mortality in 1985 due to advancements in medical technology.

![[8gpMO2vFWF3kOOYHECmMqpMfWVPGxSligaCll3LT9iXedc3yo8OGsliw66uEHgw7cWHx7jPv2qrGXSeZe0Ej1oEiT4rJ7i-BfPF2.png]]

[**19:00**](https://youtu.be/h_9aG01n2Oc?t=1140) 

**Claim:** There is an inverse association between serum cholesterol and acute myocardial infarction or unstable angina in high risk populations over <70 years of age.

**Fact:** Even if acute myocardial infarction (AMI) is the leading cause of mortality in your population, the age of the population matters. The risk of AMI peaks between the ages of 45-65. Beyond 65, your risk of dying from ASCVD is lower by virtue of the fact that the risk of dying due to other diseases is going up. If high cholesterol doesn’t kill you between the ages of 45-65, it is unlikely to kill you before something like cancer does, for example.

Also, many of the diseases that would be likely to kill you instead of ASCVD result in lower cholesterol, so reverse causality is still a valid explanation without appropriate adjustment.

![[oGhWZ9iBql5WsxhWpHgcIdOTyO2FXtLx8_0WxI3meXVxEIEw2EVKrJ90iHXyT31J7hx_Qjng3pwQqlNZugfHoTUXIgdlEWUYT0l6.png]]

[**26:30**](https://youtu.be/h_9aG01n2Oc?t=1590) 

**Claim:** In reference to the relationship between coronary artery calcification and statin medication, there was more coronary artery calcification in the group receiving a higher dose of atorvastatin.

**Fact:** The effect of statin therapy on CAC progression appears to be a function of the baseline level of calcification present [12](https://pubmed.ncbi.nlm.nih.gov/33426001/). In the aggregate, subjects on statin therapy with CAC scores exceeding 400 see a statistically significant decrease in calcium scores by the end of the study period (P=0.009).

![[9QhaGijiaFvLR5Yd-5Pk1rxyqAkDVWfps-TK2Q73_PRkKRwSgSgTKOnTyadV5_0R_tWPAoz6cWFaV0hy25MRxk3VoqzRCwlFl9DD.png]]

Kyle’s referenced study was among the studies in the statistically significant subgroup, and showed a non-significant trend toward a benefit of statins. Kyle is misreading the figure provided in the original paper [13](https://pubmed.ncbi.nlm.nih.gov/16415377/).

![[uFF2GlLwgjQXPH4jhka9x_1J3H--pUube2oP0p-tXHKyWnJqcLHEQfVLdOBUcC5tBBV00ccfleVPseQyrw32K9J7W6GOea-TB90j.png]]

It looks like the group receiving the higher dose of atorvastatin had more CAC progression, however these are representing medians and percentiles. Generally statistics like this are reported as means and confidence intervals or standard deviations, like they are in the meta-analysis above. When represented this way, there is a trend toward a benefit of statins consistent with the majority of the literature on this subject.

[**28:08**](https://youtu.be/h_9aG01n2Oc?t=1688) 

**Claim:** Both the diet-heart hypothesis and lipid hypotheses fail to explain all observations pertaining to lipids and atherogenesis.

**Fact:** Again, for the reasons we discussed throughout this article, this is essentially just naive falfisicationism. A hypothesis failing to predict an outcome does not necessarily invalidate the hypothesis.

[**28:32**](https://youtu.be/h_9aG01n2Oc?t=1712)

**Claim:** Heterogeneity in the results of studies using similar methodologies usually means there is a third unmeasured variable that is interacting with the variables you’re measuring and/or manipulating.

**Fact:** It is true that a third unmeasured confounder could explain heterogeneity in some cases. But it depends on the extent of the heterogeneity and how likely it is that unmeasured confounders could explain the effects that we observe. 

With regards to statin medication and CAC, there is some heterogeneity, but overall statins are beneficial. With regards to the prevention of ASCVD events, the effect of statins is **overwhelmingly** beneficial.

![[WTJMMBj5ctIOzC4fdGvfNQFyV258Ys-ww572VORdwV8bbOAN9-U4Zc2UAlzt1JnWZsuYv44suFLaWFUq7BLM7a6DUlyzkrV4KXWd.png]]

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/28444290/](https://pubmed.ncbi.nlm.nih.gov/28444290/) 

[2] [https://pubmed.ncbi.nlm.nih.gov/25568296/](https://pubmed.ncbi.nlm.nih.gov/25568296/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/22453571/](https://pubmed.ncbi.nlm.nih.gov/22453571/) 

[4] [https://pubmed.ncbi.nlm.nih.gov/21356116/](https://pubmed.ncbi.nlm.nih.gov/21356116/) 

[5] [https://pubmed.ncbi.nlm.nih.gov/21392724/](https://pubmed.ncbi.nlm.nih.gov/21392724/) 

[6] [https://pubmed.ncbi.nlm.nih.gov/30694319/](https://pubmed.ncbi.nlm.nih.gov/30694319/) 

[7] [https://pubmed.ncbi.nlm.nih.gov/32203549/](https://pubmed.ncbi.nlm.nih.gov/32203549/) 

[8] [https://pubmed.ncbi.nlm.nih.gov/1534437/](https://pubmed.ncbi.nlm.nih.gov/1534437/) 

[9] [https://pubmed.ncbi.nlm.nih.gov/30596814/](https://pubmed.ncbi.nlm.nih.gov/30596814/) 

[10] [https://academic.oup.com/jn/article-abstract/59/1/39/4722525](https://academic.oup.com/jn/article-abstract/59/1/39/4722525) 

[11] [https://pubmed.ncbi.nlm.nih.gov/19959603/](https://pubmed.ncbi.nlm.nih.gov/19959603/) 

[12] [https://pubmed.ncbi.nlm.nih.gov/33426001/](https://pubmed.ncbi.nlm.nih.gov/33426001/)

#patreon_articles 
#clownery 
#cardiovascular_disease 
#statins 
#dietary_cholesterol 
#LDL 
