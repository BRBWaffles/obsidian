There is a sizable community of vegans out there comprised of those who believe that the gut-derived metabolite trimethylamine N-oxide (TMAO) uniquely predisposes people to cardiovascular disease (CVD). It's understandable why they might be attracted to an idea such as this, as it is typically animal products like eggs that tend to increase markers of TMAO status [1](https://pubmed.ncbi.nlm.nih.gov/24944063/).

![[1-67.png]]

On its own this shouldn't mean very much to us. But meta-analyses have showed pretty robust associations with elevated TMAO status and both inflammation and CVD [2](https://pubmed.ncbi.nlm.nih.gov/32462890/)[3](https://pubmed.ncbi.nlm.nih.gov/31918665/). In the case of inflammation, it has been observed that TMAO and C-reactive protein tend to move in tandem and associate together strongly.

![[1-65.png]]

As for CVD, the association is similarly tight. In this meta-analysis assessing the relationship between TMAO and CVD, the higher your TMAO status, the higher the rates of CVD.

![[1-66.png]]

For a long time, these data contended to reconcile a lot of associations between certain dietary patterns and CVD outcomes. When markers like low density lipoproteins could not explain associations between different animal foods and CVD, it was often TMAO that came to the rescue to reconcile the data. That is until a method of investigation known as mendelian randomization (MR) was used to investigate the relationship [4](https://pubmed.ncbi.nlm.nih.gov/31167879/).

MR is a type of epidemiological investigation that aims to investigate the relationship between genetically mediated characteristics and outcomes. Generally speaking, these methods are stronger than prospective cohort studies, but weaker than randomized controlled trials. In the case of LDL, MR has proven itself to be highly valuable [5](https://pubmed.ncbi.nlm.nih.gov/30694319/).

When the relationship between higher levels of genetically mediated TMAO status and CVD is investigated through the lens of MR, it tells us a different story.

![[Pasted image 20221123145942.png]]

As we can see, elevated TMAO status does not robustly associate with any of the measured CVD outcomes, which included atrial fibrillation, coronary artery disease, myocardial infarction, and stroke. These results suggest that TMAO is not a mediator of risk, but its tight association with CVD outcomes might make it a good marker of CVD risk.

However, if you were still so inclined to try to reduce TMAO as much as possible, there are some practical solutions (and no you don't need to go vegan, lol). According to a recent crossover study, TMAO concentrations are a function of whether or not your diet has sufficient whole plant foods [6](https://pubmed.ncbi.nlm.nih.gov/32780794/).

![[Pasted image 20221123145949.png]]

When subjects started off eating high-plant diets, they did not experience an increase in TMAO when switching to a high-animal diet. However, TMAO increased dramatically when subjects were started on the high-animal diet, and the effect was abolished with a high-plant diet.

In conclusion, TMAO is a reasonably good predictor of CVD outcomes. Possibly due to it being elevated as a consequence of diets very high in animal foods and very low in plant foods. However, TMAO itself does not seem to mediate CVD risk.

**Key points:**

-   It has been claimed that TMAO increases the risk of cardiovascular disease.
-   TMAO is very strongly associated with cardiovascular disease.
-   TMAO loses all predictive power when investigated using more robust methods.
-   TMAO is a cardiovascular disease marker, not a cardiovascular disease mediator.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/24944063/](https://pubmed.ncbi.nlm.nih.gov/24944063/)

[2] [https://pubmed.ncbi.nlm.nih.gov/32462890/](https://pubmed.ncbi.nlm.nih.gov/32462890/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/31918665/](https://pubmed.ncbi.nlm.nih.gov/31918665/) 

[4] [https://pubmed.ncbi.nlm.nih.gov/31167879/](https://pubmed.ncbi.nlm.nih.gov/31167879/)

[5] [https://pubmed.ncbi.nlm.nih.gov/30694319/](https://pubmed.ncbi.nlm.nih.gov/30694319/)

[6] [https://pubmed.ncbi.nlm.nih.gov/32780794/](https://pubmed.ncbi.nlm.nih.gov/32780794/)

#patreon_articles 
#tmao 
#cardiovascular_disease 
#nutrition 
#animal_foods 
#choline
#vegan_talking_points 