I've slowly become a fan of more plant-focused diets since I escaped the clutches of low carb lunacy. The data is hard to argue with— the more plants you have in your diet, the better your health outcomes are likely to be. However, there are some pretty loud voices in the plant-based/vegan community that love to promulgate utter nonsense.  
  
First up we have Evan Allen, a prominent plant-based physician who apparently has a bone to pick with saturated fat (SFA). Regardless of the evidence quality, he seems more than willing to toss out anything that can make any SFA look bad. Here's a recent gem from him:

![[1-12.jpg]]

He basically links to some [mechanistic research](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-020-07003-0) about palmitate's interactions with β-cells. He then extrapolates to human outcomes and cobbles together an anti-dairy narrative, suggesting that dairy fat causes type II diabetes (T2DM). Listen, there are many legitimate reasons to be against the consumption of dairy. Palmitate-induced lipotoxicity in pancreatic β-cells is not one of them.

Luckily, we don't even need mechanisms to investigate how dairy affects human health. If we refer to the epidemiology, we see that going from the lowest to highest intakes of dairy products actually reduces the risk of T2DM [1](https://pubmed.ncbi.nlm.nih.gov/23945722/). 

![[Pasted image 20221123150148.png]]

Overall, dairy consumption yields about a 7% reduction in T2DM risk. Maximal reductions in risk appear to be obtained within the first 100g/day. It's not a huge effect, but it's real. The effect certainly doesn't show that higher and higher intakes of dairy show greater and greater increases in risk. The opposite it seen.

This is why we can't extrapolate from mechanisms to human outcomes. In the vast majority of cases the mechanisms do not pan out, and human outcome data directly contradicts those mechanistic hypotheses.

**Key points:**

-   It is claimed that palmitate is "toxic" to pancreatic β-cells due to some mechanistic interactions between the two.
-   This is used as the basis for the claim that dairy products cause type II diabetes.
-   However, aggregated human outcome data and dose-response curves support an inverse association between dairy consumption and type II diabetes.

**References:**

[1] Dagfinn Aune, et al. Dairy products and the risk of type 2 diabetes: a systematic review and dose-response meta-analysis of cohort studies. Am J Clin Nutr. 2013 Oct.  [https://pubmed.ncbi.nlm.nih.gov/23945722/](https://pubmed.ncbi.nlm.nih.gov/23945722/)

#patreon_articles 
#dairy 
#type_2_diabetes 
#clownery 
#nutrition 
#disease 
