As some of you may know, I'm in the processes of writing a meta-analysis for publication. If all goes well it should be published by late 2021. It will investigate low carbohydrate diets as they relate to a number of different biomarkers. This is one biomarker I don't expect will make it in to the publication, but it is super interesting nonetheless. So I'll share the results with you guys here.

It's relatively well accepted that the state of ketosis is characterized by a set of metabolic processes which could, in theory, lead to a reduction in bone mineral density (BMD). These metabolic processes include a slightly higher acid load in the blood as well as an upregulation of gluconeogenesis. Another possible pathway is through possible disruptions to cellular calcium efflux.

There's quite a bit of debate about whether or not ketogenic diets could actually lead to reduced bone mineral density over time. The level of debate about this is actually pretty hilarious considering we have plenty of good data on the subject. Luckily, I'm unemployed as fuck and I have plenty of time on my hands to investigate such questions in depth, haha.

I managed to find six randomized controlled trials investigating ketogenic diets as they relate to BMD. Here are the results:

![[1-3.png]]

Results were not statistically significant. Overall, there is a non-significant trend toward ketogenic diets lowering BMD (P=0.24).

I also threw in some non-ketogenic data. For clarification, Brehm 2003 includes both three month and six month data for their low carb subjects. At three months the subjects were ketogenic. They were no longer ketogenic at six months, but still eating a low carbohydrate diet. For this reason their three month data is included in the first subgroup, and their six month data is included in the non-ketogenic subgroup.

**Key points:**

-   There are good reasons to suspect that ketogenic diets may reduce bone mineral density.
-   When meta-analyzed, the available data seems to suggest that ketogenic diets don't seem to lower bone mineral density.

#patreon_articles 
#keto 
#bones 
#disease 
#nutrition 
