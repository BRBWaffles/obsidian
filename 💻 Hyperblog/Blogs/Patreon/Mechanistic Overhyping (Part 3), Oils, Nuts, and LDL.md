This week's harebrained mechanistic nonsense comes to from Anna Borek, the "ScepticalDoctor" on Twitter. She contends that nuts are preferable to olive oil for cardiovascular disease (CVD) prevention due to their supposed superior effects on LDL cholesterol (LDL-C).

![[Pasted image 20221123150315.png]]

So let's take a look at the study she's referencing [1](https://pubmed.ncbi.nlm.nih.gov/21421296/). As we can see, almonds certainly do reduce LDL-C relative to baseline to a statistically significant degree.

![[Pasted image 20221123150318.png]]

However, this isn't the whole story. Let's see what happens when we compare between-group differences in LDL-C lowering. Here we see olive oil compared to either almonds or walnuts. As we can see, despite the statistically significant change from baseline we saw from almonds in the previous analysis, we see there is no statistically significant between-group differences.

![[Pasted image 20221123150323.png]]

But let's say there was a statistically significant within-group and between-group difference in LDL-C changes that benefitted nuts. Does this actually mean nuts carry a higher risk of CVD? Maybe not.

Two extremely comprehensive mendelian randomization studies divulge that risk is not tracking LDL-C [2](https://pubmed.ncbi.nlm.nih.gov/32203549/)[3](https://pubmed.ncbi.nlm.nih.gov/30694319/). Risk unambiguously tracks a protein on the LDL particle called apolipoprotein B (ApoB). Luckily, this paper also reports ApoB concentrations in addition to LDL-C. Here are the within-group changes, as well as the between group changes, in ApoB.

![[Pasted image 20221123150330.png]]

![[Pasted image 20221123150333.png]]

No statistically significant differences at all. Which is actually pretty interesting. Neither olive oil nor nuts actually lower ApoB to a statistically significant degree. However, both olive oil and nut consumption both lower CVD mortality to the same degree in prospective cohort studies [4](https://pubmed.ncbi.nlm.nih.gov/27916000/)[5](https://pubmed.ncbi.nlm.nih.gov/31856379/).

![[Pasted image 20221123150338.png]]

![[Pasted image 20221123150340.png]]

So, in conclusion, consume both. They both reduce risk. Don't tolerate dry-ass salads. Dress them with olive oil if you so please. Don't tolerate bland oatmeal. Dress it with nuts. There is no persuasive reason to avoid either on the basis of CVD prevention.

**Key points:**

-   It has been asserted that nuts are preferable to olive oil for CVD prevention due to a superior capacity to lower LDL-C.
-   Almonds do lower LDL-C more than olive oil relative to baseline.
-   Both nuts and olive oil lower CVD risk to approximately the same degree.
-   CVD risk primarily tracks ApoB, not LDL-C.
-   Neither nuts nor almonds raise or lower ApoB to a statistically significant degree.

**References:**

[1] N R T Damasceno, et al. Crossover study of diets enriched with virgin olive oil, walnuts or almonds. Effects on lipids and other cardiovascular risk markers. Nutr Metab Cardiovasc Dis. 2011 Jun. [https://pubmed.ncbi.nlm.nih.gov/21421296/](https://pubmed.ncbi.nlm.nih.gov/21421296/)

[2] Tom G Richardson, et al. Evaluating the relationship between circulating lipoprotein lipids and apolipoproteins with risk of coronary heart disease: A multivariable Mendelian randomisation analysis. PLoS Med. 2020 Mar 23. [https://pubmed.ncbi.nlm.nih.gov/32203549/](https://pubmed.ncbi.nlm.nih.gov/32203549/)

[3] Brian A Ference, et al. Association of Triglyceride-Lowering LPL Variants and LDL-C-Lowering LDLR Variants With Risk of Coronary Heart Disease. 2019 Jan 29. [https://pubmed.ncbi.nlm.nih.gov/30694319/](https://pubmed.ncbi.nlm.nih.gov/30694319/)

[4] Dagfinn Aune, et al. Nut consumption and risk of cardiovascular disease, total cancer, all-cause and cause-specific mortality: a systematic review and dose-response meta-analysis of prospective studies. BMC Med. 2016 Dec 5. [https://pubmed.ncbi.nlm.nih.gov/27916000/](https://pubmed.ncbi.nlm.nih.gov/27916000/)

[5] V P Campos. Effects of a healthy diet enriched or not with pecan nuts or extra-virgin olive oil on the lipid profile of patients with stable coronary artery disease: a randomised clinical trial. J Hum Nutr Diet. 2020 Jun. [https://pubmed.ncbi.nlm.nih.gov/31856379/](https://pubmed.ncbi.nlm.nih.gov/31856379/)

#patreon_articles 
#nutrition 
#disease 
#nuts 
#vegetable_oil 
#LDL 
#cardiovascular_disease 