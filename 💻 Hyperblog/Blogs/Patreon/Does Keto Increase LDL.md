Even though my meta-analysis on low carbohydrate diets and health showed that keto tends to increase low density lipoprotein cholesterol (LDL-C), I had a suspicion that there might be more to the story. I wasn't quite ready to take for granted that keto increased LDL-C, in and of itself.

I noticed a tendency among the studies that actually reported the saturated fat (SFA) intakes of the subjects in the trial. I noticed that when keto resulted in only modest changes in SFA intake compared to baseline, LDL-C was also virtually unchanged. In fact, sometimes LDL-C would decrease. That is to say, I ended up having a hunch that the differences in LDL-C observed on keto were likely a function of SFA intake.

So I gathered all of the literature used in my low carb meta-analysis that I could, as well as some new publications that have been released within the last two years. I required that studies report both the SFA intakes of the subjects as well as LDL-C, compared to either control, baseline, or both. The results were pretty interesting.

![[Pasted image 20221123154254.png]]

First off, here we see the differences in SFA intake between keto and control plotted against the differences in LDL-C between keto and control. As you can see, no keto intervention resulted in lower SFA intake compared to control, but some did result in lower LDL-C compared to control. The correlation between the ΔSFA intake and ΔLDL-C is actually pretty high.

![[Pasted image 20221123154257.png]]

Here we see the changes in SFA intake with keto compared to baseline plotted against the changes in LDL-C with keto compared to baseline. There are fewer studies included because there were fewer studies that reported baseline SFA intakes and LDL-C. Looking at the data this way (which is likely a better angle to investigate), we see a tighter correlation.

But something is off. There is a clear outlier at the bottom of the chart. Participants increased SFA intake by approximately 11g/day but saw a non-trivial reduction in LDL-C. This is the only instance of this happening, so a remove-one analysis may be warranted.

![[Pasted image 20221123154306.png]]

Removing the outlier pushes the R^2 from 0.655 to 0.833. Which is pretty close to a linear correlation.

I also investigated this question in the same way with apolipoprotein B-100 (ApoB). 

![[Pasted image 20221123154309.png]]

Here are the differences in SFA intake between keto and control plotted against the differences in ApoB between keto and control. There were no studies that actually showed reductions in ApoB with keto compared to control. And the correlation, while technically "smaller", shows us pretty much what we would expect to see. As SFA intake goes up, so does ApoB.

However, like the previous plots, there is an obvious outlier. We see an instance were ApoB increases disproportionate to SFA intake, so perhaps we should see what would happen if we remove it.

![[Pasted image 20221123154314.png]]

Again, we see the R^2 increase. This time from 0.325 to 0.644. Which is a decently tight correlation. Also, again, this is what we would expect to see.

I should stress that this analysis is exploratory and observational. It doesn't really have much explanatory power in and of itself. But, it corroborates what we understand from the greater body of literature regarding the effect of SFA intake on blood lipids.

In conclusion, it is unlikely that keto uniquely increases LDL-C or ApoB in and of itself. Rather, it is more likely that ApoB and LDL-C respond to SFA intake on keto precisely as we would expect them to, in the general population.

**Key points:**

-   Keto does not appear to increase ApoB or LDL-C any differently than any other diet.
-   The increases in ApoB and/or LDL-C observed on keto can be largely explained by increases in SFA intake.

**Supplementary Materials:**

[https://docs.google.com/spreadsheets/d/1vtuzsp8PDq2s0zliKdlcX8536iwt3OdoT9oW6os6mnY/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1vtuzsp8PDq2s0zliKdlcX8536iwt3OdoT9oW6os6mnY/edit?usp=sharing)

#patreon_articles 
#nutrition 
#keto 
#LDL 
#ApoB 
#saturated_fat 
