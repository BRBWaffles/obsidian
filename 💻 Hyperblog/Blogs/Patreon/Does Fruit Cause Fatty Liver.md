By now, you may be aware that there is a study that has been recently published to the Scandinavian Journal of Gastroenterology by Alami et al. (2022), purporting to show that fruit increases the risk of non-alcoholic fatty liver disease (NALFD) [1](https://pubmed.ncbi.nlm.nih.gov/35710164/). Many low carb influencers have used the results of this study to make the claim that fruit causes NALFD. However, this isn't a correct interpretation. Let's get into it.

Essentially, 80 NAFLD patients were randomized to receive either additional fruit in a fruit-rich diet (FRD) group, or restricted fruit in a control group. They stayed on their respective diets for six months, and many biomarkers of insulin resistance and fatty liver were incrementally collected during that time.

The FRD group was instructed to add fruit to their diet with no specific instructions to replace anything else that they were currently eating. Whereas the control group was instructed to remove a certain amount of fruit from their diet with no specific instructions to eat anything else in place of the fruit. Non-adherers were also excluded from the final analysis.

> _"The participants in the [fruit-rich diet] group were recommended to consume at least 4 servings of fruits per day and the control group was asked not to consume more than 2 servings of fruit per day...those who received less than 4 servings of fruits in the intervention group or more than 2 servings of fruits in the control group were excluded from the analyses."_

A potential issue may already be obvious to you. Perhaps the FRD group just ended up consuming more calories than the control group. Based on the design this is certainly plausible. In fact, it's probable. A quick scan of Table 2 confirms our suspicions, with the control group consuming anywhere from ~202-338 kcal more than the FRD group throughout the study.

![[1-84.png]]

At this point it should be clear why the FRD group performed worse than the control group. However, there is an additional issue. The association between fruit intake and biomarkers of liver fat survived adjustment for BMI.

> _"After 6 months, the FRD group had higher serum levels of ALT, AST, ALP, and GGT compared to the control group. Adjustments for the effect of change in BMI, energy, bread and cereals, meats, vegetables, dairies, sugars, fats, and oils intake did not change the results...the present study showed that the main findings on the adverse effects of fruits in patients with NAFLD are independent from changes in the BMI, energy or other food group’s intake."_

However, this isn't the whole story here. The subjects in the study started off on the cusp of being obese, with an average BMI of 28.1. If we turn our attention to Table 3, we see that the FRD group actually became prediabetic during the study, based on their fasting blood glucose, which went from 96.9 mg/dL to 115.5 mg/dL. This is a potentially critical piece of information.

Based on the work of Taylor and Holman (2015) and Johansson et al. (2019), we understand that the relationship between liver fat and BMI is almost linear [2](https://pubmed.ncbi.nlm.nih.gov/25515001/)[3](https://pubmed.ncbi.nlm.nih.gov/21656330/)[4](https://pubmed.ncbi.nlm.nih.gov/31685793/). This is hypothesized to be what initiates the development of the diabetic phenotype in humans [5](https://pubmed.ncbi.nlm.nih.gov/23075228/).

Alami et al. adjusted for changes in BMI, which should have rendered the relationship between fruit intake and liver fat non-significant. But it didn't, the effect remained statistically significant even after the adjustment for BMI. Since BMI should mediate the relationship between calorie intake and liver fat, there are only two ways I can think to interpret this; either fruit has such a unique effect on liver fat that there is a second, unmeasured mediator that absolutely dominates over calories, or BMI differences were not enough to produce the sensitivity needed for the adjustment to affect the relationship, or there was a clerical error in their modeling somewhere.

Even though fructose is an independent mediator of liver fat in humans, we know from wider research that the calorie-independent effect of fructose on liver fat is miniscule at best [6](https://pubmed.ncbi.nlm.nih.gov/33381794/)[7](https://pubmed.ncbi.nlm.nih.gov/35889803/). So, it's unclear why an adjustment for BMI changes would not render the relationship non-significant. So, I'm personally leading toward the second or third explanation, that the BMI differences in the study weren't sensitive enough or there was some sort of clerical error somewhere in their modeling.

Maybe randomization failed somehow. Maybe someone inputted some data wrong when formulating the adjustment models. Who knows. It just seems utterly implausible that the effect would survive adjustment for BMI changes.

**Key Points:**

-   The association between fruit and liver fat survived adjustment for BMI changes.
-   These results are difficult to interpret and probably do not show us that fruit increases the risk of fatty liver.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/35710164/](https://pubmed.ncbi.nlm.nih.gov/35710164/) 

[2] [https://pubmed.ncbi.nlm.nih.gov/25515001/](https://pubmed.ncbi.nlm.nih.gov/25515001/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/21656330/](https://pubmed.ncbi.nlm.nih.gov/21656330/)

[4] [https://pubmed.ncbi.nlm.nih.gov/31685793/](https://pubmed.ncbi.nlm.nih.gov/31685793/)  

[5] [https://pubmed.ncbi.nlm.nih.gov/23075228/](https://pubmed.ncbi.nlm.nih.gov/23075228/)

[6] [https://pubmed.ncbi.nlm.nih.gov/33381794/](https://pubmed.ncbi.nlm.nih.gov/33381794/)

[7] [https://pubmed.ncbi.nlm.nih.gov/35889803/](https://pubmed.ncbi.nlm.nih.gov/35889803/)

#patreon_articles 
#fruit 
#non_alcoholic_fatty_liver_disease 
#disease 
#nutrition 
