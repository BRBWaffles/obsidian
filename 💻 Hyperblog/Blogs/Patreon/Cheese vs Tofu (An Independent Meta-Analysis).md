I'm not sure why, but this comparison rears its head in many nutrition conversations related to dairy and cardiovascular disease (CVD) risk through changes in low density lipoprotein cholesterol (LDL-C). Many claim that cheese should be avoided in favour of tofu because cheese does not improve blood lipids compared to tofu.

Personally, I don't think it's a very interesting or fair comparison. The two foods are not matched for protein, carbohydrates, fat, or fatty acid composition in the least. But nonetheless it makes for good debate fodder for reductionist ideologues, and maybe it will be interesting to see what the data says.

Oddly enough, there are three studies that have measured blood lipid changes between groups fed either tofu or cheese [1](https://pubmed.ncbi.nlm.nih.gov/8780967/)[2](https://www.sciencedirect.com/science/article/pii/S0271531786800197)[3](https://pubmed.ncbi.nlm.nih.gov/2621294/). The first is the most often cited as evidence of the LDL-C raising effects of cheese, but many of their data points are confusing.

![[1-19.png]]

Seriously, though. What in the world is going on with these lipid metrics? There is no consistency in the findings except for LDL-C, and even that seems to have some issues.

We can see that cheese increases LDL-C compared to baseline, but it also seems to have the capacity to lower it. Unless that middle measurement was just randomly low for some reason. It is interesting to note that egg whites lowered LDL-C reliably compared to tofu. So, as we tumble down the slippery slope of diet reductionism, we're left concluding that egg whites are preferable to tofu, and that tofu should be avoided as well.

Just out of curiosity I decided to meta-analyze the results from all of these trials.

![[Pasted image 20221123153712.png]]

In the aggregate, tofu results in lower LDL-C than what is achieved by cheese, and the results are statistically significant (P=0.03). I chose my words here very carefully, because it is not the case that cheese _increases_ LDL-C, either.

![[Pasted image 20221123153715.png]]

When we consider the effects of cheese on LDL-C compared to baseline, we cannot say that cheese increases LDL-C. It just doesn't lower LDL-C, either. Its effects are neutral. This is good and bad for cheese-lovers. It could conceivably be the case that cheese could be an obstacle to lower LDL-C, but it is unlikely to make your LDL-C worse all by itself. 

In fact, two studies saw cheese either decrease LDL-C, or leave it unchanged, from a baseline of <102mg/dL. Meaning that it likely isn't the case that the neutral effects of cheese are just an artifact generated from higher baseline LDL-C to begin with. But let's take a look at the data for tofu compared to baseline, too.

![[Pasted image 20221123153719.png]]

This seems way more definitive to me. Tofu almost certainly lowers LDL-C. Which is fantastic news for me, because I love tofu with a passion, haha. But it also doesn't necessarily mean that we need to avoid cheese on the basis of its effects on LDL-C. Since cheese does not reliably affect LDL-C, it could still be the case that the effects of tofu on lowering LDL-C could have more to do with the presence of the tofu itself, and less to do with the absence of cheese.

Y'know what this sounds like to me?

![[1-21.png]]

**Key points:**

-   Tofu tends to lower LDL-C.
-   Cheese doesn't necessarily increase LDL-C.
-   Eat tofu, but don't sweat the cheese either.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/8780967/](https://pubmed.ncbi.nlm.nih.gov/8780967/) 

[2] [https://www.sciencedirect.com/science/article/pii/S0271531786800197](https://www.sciencedirect.com/science/article/pii/S0271531786800197) 

[3] [https://pubmed.ncbi.nlm.nih.gov/2621294/](https://pubmed.ncbi.nlm.nih.gov/2621294/)

#patreon_articles 
#nutrition 
#LDL 
#cheese 
#tofu
#dairy 
#soy 