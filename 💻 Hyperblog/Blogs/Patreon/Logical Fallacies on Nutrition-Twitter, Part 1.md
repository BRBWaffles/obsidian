Simply stated, logical fallacies are errors in reasoning or thought that are so common that they have their own names. Some can be quite obvious, while others are obnoxiously subtle. So, to help us all understand when these fallacies occur, I'm going to use the nutrition Twittersphere as a case study for many of these fallacies. So let's get into it!

**Appeal to Nature Fallacy**

This fallacy occurs when someone suggests that something is preferable merely because it is natural or occurs in nature. Here we see MacroFour imply that natural diets are preferable for maintaining calorie balance, because other animals don't need to rely on calorie-tracking to maintain their weight. Even if it were true that other animals do not require calorie-tracking to maintain their weight, that doesn't tell us anything about best practices for human beings.

![[Pasted image 20221123145556.png]]

**Appeal to Ignorance Fallacy**

This fallacy occurs when someone suggests that something is true merely because it has not yet been falsified. Here we see CoachChris suggesting that vegetable oil products are considered suspect and/or harmful until they have been investigated in all possible contexts. This essentially creates an unfalsifiable hypothesis that leaves vegetable oil products being forever damned regardless of how beneficial they are actually found to be.

![[Pasted image 20221123145608.png]]

**Appeal to Emotion Fallacy**

This fallacy occurs when someone suggests that something is true merely because they believe it to be. Here we see Barry Pearson suggest that his diet, which significantly elevates his LDL, must be good for him because he believes that LDL is actually beneficial. But of course, elevated LDL is a significant, independent risk favour for a number of vascular diseases.

![[Pasted image 20221123145614.png]]

**Appeal to Worse Problems Fallacy**

This fallacy occurs when someone dismisses the severity of a problem merely because there are worse problems that exist. Here we see Defender interrupt a conversation about adequate sources of B12 on a vegan diet with the suggestion that our discussion is made trivial in the light of the horrors of animal agriculture. Yes, animal agriculture is horrible, and it should eventually be brought to an end. But that doesn't negate the fact that implying that vegans can get adequate B12 from the soil residue on their carrots is dangerous bullshit.

![[Pasted image 20221123145620.png]]

**Argument from Incredulity Fallacy**

This fallacy occurs when someone suggests that something is either true or untrue merely because they cannot personally understand or comprehend how it could not be either true or untrue. Here we see Elie Jarrouge, MD, suggest elevated LDL on a low carb diet can't possibly be bad if it occurs in the context of commensurate improvements in everything else.

![[Pasted image 20221123145624.png]]

**Motte and Bailey Fallacy**

This fallacy occurs when someone suggests something fallacious, but substitutes a less controversial version of their position once their fallacious thinking has been exposed. Here we see CoachChris make the claim that margarine is toxic. When pressed on the issue, he eventually flips his position and makes the claim that he's merely questioning the validity of the current evidence base regarding margarine and human health. The latter position is much easier to defend than the former position.

![[Pasted image 20221123145629.png]]

![[Pasted image 20221123145633.png]]

**Appeal to Tradition Fallacy**

This fallacy occurs when someone suggests that something is preferable merely because it has a long-standing history of being done. Here we see MacroFour suggest that we should eat like our grandmas, because our grandmas might not be able to recognize the foods we're currently eating.

![[Pasted image 20221123145637.png]]

**Exception Fallacy**

This fallacy occurs when someone extrapolates from an exceptional case to make generalizations across all cases. Here we see Dr. Jay Wrigley suggest that gluten increases the prevalence of certain autoimmune disorders. The implication being that since gluten is the catalyst for autoimmune flairs in those with celiac disease, gluten must also increase instances of other autoimmune disease due to its association in epidemiology. When in reality, already having an autoimmune disease is a risk factor for developing any number of other autoimmune diseases, and gluten likely plays no independent role in the development of other autoimmune diseases.

![[Pasted image 20221123145641.png]]

**Sunk Cost Fallacy**

This fallacy occurs when someone persists with a behaviour merely because they are invested in it, even if that behaviour continues to produce negative outcomes for them. Here we see Bret Scher, MD, suggest that increasing LDL is of little concern so long as a number of other things are also improving. He's suggesting that we commit to a particular behaviour, even if that behaviour produces negative outcomes. It is likely that he should be working toward also lowering LDL in his patients when it is indicated. 

![[Pasted image 20221123145652.png]]

**Appeal to Authority Fallacy**

This fallacy occurs when someone suggests that something is true merely because an authority deems it true. Here we see MacroFour defend a position on the basis of Tim Noakes' credentials, rather than the veracity of the empirical claims being made.

![[Pasted image 20221123145656.png]]

**No True Scotsman Fallacy**

This fallacy occurs when someone changes the criteria required for credibility. Here we see David Diamond suggest that a particular study can't be counted as a credible investigation into low carbohydrate diets because not only were the carbohydrate intakes were not low enough, the blood ketone levels were not high enough. However, the subjects in the paper were actually ketogenic to a statistically significant degree due to consuming less than 50g/day of carbohydrate. 

![[Pasted image 20221123145700.png]]

**Non-Sequitur Fallacy**

This fallacy occurs when someone suggests that something is true based on completely unrelated evidence. Here we see Dr. David Unwin suggest that type 2 diabetes is caused by postprandial blood glucose excursions. It is true that high glycemic index foods typically cause higher blood glucose excursions. However, whether or not these blood glucose excursions are the cause of type 2 diabetes is a completely separate claim that needs completely different supporting evidence.

![[Pasted image 20221123145706.png]]

#patreon_articles 
#twitter 
#logic 
#fallacies
#nutrition 