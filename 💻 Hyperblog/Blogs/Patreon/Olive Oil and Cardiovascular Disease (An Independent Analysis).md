Co-authored by [Alan Watson](https://twitter.com/Alan_Watson_).

There is a flavour of vegan whole-foods purists who claim that any added fat in the form of oils increases the risk of cardiovascular disease (CVD). Despite massive evidence to the contrary, you often hear this rabble chanting "no oil— even olive oil!" as a mantra. But is this true?

Well, I decided to do a meta-analysis on olive oil consumption in order to see if this enthusiasm for dry-ass salads was justified. I combed the literature for any prospective cohort studies or prospective randomized trials investigating rates of CVD as a function of olive oil intake. It was slim pickings, but I managed to find a handful of studies. So, let's get into it.

**Inclusion Criteria:**

-   Prospective cohort studies or prospective randomized trials investigating the relationship between olive oil and CVD.
-   Endpoints (events, mortality, incidences, etc) directly related to CVD, coronary heart disease, ischemic heart disease, or myocardial infarction are all acceptable.
-   Risk estimates stratified from lowest to highest olive oil intakes.

**Exclusion Criteria:**

-   Studies pooling results across multiple cohorts from different countries.
-   Studies that report irrelevant endpoints (e.g. stroke, cerebrovascular disease, atrial fibrillation, etc.)
-   Studies investigating the same cohorts as other included studies. Tie-breakers are decided based on differences in study quality (e.g., chosen subgroups, endpoints, multivariate adjustment models, etc).

A total of 11 studies were collected from the scientific literature via PubMed search. Four studies were excluded due to reporting irrelevant endpoints (stroke and conception difficulty). One study was removed due to a having a duplicate cohort.

**Results:**

Altogether there were six studies that met all of the inclusion criteria. Overall, the highest levels olive oil intake per day associated with a reduced risk of CVD (RR 0.76 [0.61-0.96], P=0.02). These findings support the hypothesis that higher intakes, as opposed to lower intakes, of olive oil associate with a decreased risk of CVD. This lends support to the recommendations of typical Western dietary guidelines to consume olive oil as a means of lowering one's risk of CVD.  

![[Pasted image 20221123150840.png]]

Another analysis was performed which included stroke.

![[Pasted image 20221123150843.png]]

Overall the results are consistent with the previous results. The highest levels of olive oil intake associate with a reduced risk of total CVD, including stroke (RR 0.77 [0.67-0.90], P=0.0008).

A final subgroup analysis was conducted and limited to just stroke and cerebrovascular disease events.

![[Pasted image 20221123150848.png]]

When only considering stroke and cerebrovascular disease events, higher intakes of olive oil associated with potent reduction in risk (RR 0.69 [0.54-0.88], P=0.003).

In conclusion, these results favour regular consumption of olive oil to reduce the risk of CVD. There also appears to be an additional benefit for reducing the risk of stroke.

#patreon_articles 
#nutrition 
#disease 
#olive_oil 
#cardiovascular_disease 