Recently, I tried a beef analogue made by Impossible Foods. When I commented about this on social media, I was met with both support and criticism. Most of the criticisms were just a mix of appeals to nature, whole foods purism, or emotional reasoning. Nobody could provide me with an actual reason for why beef would actually be superior to Impossible Foods' product for health or disease endpoints. So, I figured I'd just compare the two myself.

The number one objection seemed to center around the quality of ingredients used in the production of the Impossible burger. People tended to take issue with the number of processed ingredients included in the product. Just for fun, let's take a quick look at the ingredients.

![[1-16.jpg]]

Now, I could go through this list and do an in-depth comparative analysis between each one of these ingredients and beef. But let's just stick to the most contentious ingredients: soy protein and oils. 

Dose-response analyses from nutritional epidemiology would seem to suggest that there is a danger of getting too little plant protein, and a danger of getting too much animal protein [1](https://www.bmj.com/content/370/bmj.m2412).

![[Pasted image 20221123154934.png]]

Perhaps from this we could make an argument against complete abstinence from animal protein on the basis of health outcomes (which is a separate consideration than ethics). But it is much harder to make an argument for reducing plant protein in the diet. For those of us who might want to adjust our intakes to include more plant protein and less animal protein, products like Impossible burger might actually help us achieve that.

On to the oils. Even vegans have complained about the volume of saturated fat (SFA) in the Impossible burger. Which is understandable. But the question is not whether or not Impossible burger is the pinnacle of healthy foods, but rather whether or not it is likely to be healthier than beef. 

When coconut oil is compared to beef fat, the differential effects on low density lipoprotein cholesterol (LDL-C) are null [2](https://pubmed.ncbi.nlm.nih.gov/4025191/)[3](https://pubmed.ncbi.nlm.nih.gov/30006369/). 

![[Pasted image 20221123154940.png]]

![[Pasted image 20221123154943.png]]

So in terms of saturated fat and its effects on LDL-C, it might appear that there could be no difference between Impossible burger and beef. However, Impossible burger also includes sunflower oil, which improves the ratio of polyunsaturated fat (PUFA) to saturated fat in the product compared to beef.  
  
This same meta-analysis also suggests that the differential effects on LDL-C between sunflower oil and beef fat are null. 

![[Pasted image 20221123154955.png]]

Unfortunately, the authors of this meta-analysis are fuzzy on the details regarding the source data for the comparison of sunflower and beef fat. There is only one study that investigates that substitution, but LDL-C was not one of the endpoints reported in the paper [4](https://pubmed.ncbi.nlm.nih.gov/10799380/). So, I personally have no idea where they got that data. However, we have other ways of investigating this.  
  
Achieving a higher ratio of polyunsaturated fat (PUFA) to SFA has a predictable effect on blood lipids [5](https://pubmed.ncbi.nlm.nih.gov/25286466/).

![[Pasted image 20221123154959.png]]

The P:S ratios for Impossible burger versus an equivalent beef product (fatty brisket in this case) are 0.43 and 0.09, respectively. Considering the higher fibre content, better P:S ratio, and the lack of dietary cholesterol, the Impossible burger is very likely to win in terms of managing LDL-C. Not to mention the lipid-lowering effect of soy protein isolate compared to red meat [6](https://pubmed.ncbi.nlm.nih.gov/17344494/).

Lastly, let's compare the nutritional value of both Impossible burger and our chosen beef-equivalent product, fatty brisket.

![[1-38.png]]

These are adjusted to be a single 240 calorie serving of both foods. That way we're ensuring an apples-to-apples comparison. The Impossible burger is higher in vitamins, minerals, and protein. Which segues nicely into the next topic; leghemoglobin.

Impossible burger is made using leghemoglobin, a plant-derived analogue to the hemoglobin that is responsible for the characteristic flavour of red meat. This means that beef and Impossible meat analogues are sources of heme iron. 

So, for those who want to shit on soy for having less bioavailable iron naturally, that concern literally does not apply to Impossible burger, as it very likely stands toe-to-toe with beef. All we need now is data showing that leghemoglobin can safely improve poor iron status in humans. So far, we only have papers investigating the mechanistic plausibility of leghemoglobin allergenicity [7](https://pubmed.ncbi.nlm.nih.gov/28921896/). The data seems to suggest that leghemoglobin poses very little risk to humans.

In conclusion, it is probable that Impossible burger would lead to better health outcomes overall if eaten to the exclusion of beef. However, it still has issues that would probably make it a suboptimal dietary staple. For example, it's probably better to consume whole soybeans, potatoes, and perhaps even coconuts as opposed to eating concentrated, processed versions of those foods in the form of an Impossible burger. But that doesn't change the fact that Impossible burger is likely superior to beef for health outcomes.

**Key points:**

-   It has been suggested that the processed nature of Impossible burger makes it inferior to beef.
-   Upon close inspection there are good reasons to suspect that Impossible burger would lead to better health outcomes when compared to beef.
-   Eating the whole foods that are low in saturated fat and sodium is probably still optimal.

**References:**

[1] [https://www.bmj.com/content/370/bmj.m2412](https://www.bmj.com/content/370/bmj.m2412)

[2] [https://pubmed.ncbi.nlm.nih.gov/4025191/](https://pubmed.ncbi.nlm.nih.gov/4025191/)

[3] [https://pubmed.ncbi.nlm.nih.gov/30006369/](https://pubmed.ncbi.nlm.nih.gov/30006369/)

[4] [https://pubmed.ncbi.nlm.nih.gov/10799380/](https://pubmed.ncbi.nlm.nih.gov/10799380/)

[5] [https://pubmed.ncbi.nlm.nih.gov/25286466/](https://pubmed.ncbi.nlm.nih.gov/25286466/)

[6] [https://pubmed.ncbi.nlm.nih.gov/17344494/](https://pubmed.ncbi.nlm.nih.gov/17344494/)

[7] [https://pubmed.ncbi.nlm.nih.gov/28921896/](https://pubmed.ncbi.nlm.nih.gov/28921896/)

#patreon_articles 
#nutrition 
#vegan_talking_points 
#mock_meats
#animal_foods 
#meat 
#soy 
#nutrients 