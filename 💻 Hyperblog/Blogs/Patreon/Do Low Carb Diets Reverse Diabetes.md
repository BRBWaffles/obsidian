I have covered this topic in tremendous depth [on my main blog](https://www.the-nutrivore.com/post/sugar-doesn-t-cause-diabetes-and-ketosis-doesn-t-reverse-it). I have also covered how low carb high fat diets (LCHF) diets relate to insulin resistance [here](https://www.patreon.com/posts/do-low-carb-34341540). When I wrote those two articles, I was merely skeptical that LCHF had an independent capacity to remit type 2 diabetes (T2DM). However, after completing my work on the [Low Carbohydrate Diets and Health](https://thenutrivore.blogspot.com/2020/10/low-carbohydrate-diets-and-health.html) meta analysis, I'm now very, very confident that LCHF does not actually reverse T2DM in any independent capacity at all.

I find the evidence quite clear that T2DM is a disease of chronically high energy status, and I explain this in detail in [this video](https://www.youtube.com/watch?v=Zu2Sl3qEh9I). This is why I included subgrouping by weight loss in my meta-analysis. Without the luxury of a meta-regression analysis, this is the next best way to tease out an effect of LCHF on either one of these endpoints that is independent of weight loss.

T2DM can be diagnosed about four different ways. But, typically T2DM is diagnosed using either fasting blood glucose (FBG) or hemoglobin A1C (HbA1C). An FBG of >7mmol/L, or an HbA1C of >6.5% is sufficient to diagnose an individual with T2DM. I collected data for both of these endpoints for my Low Carbohydrate Diets and Health meta analysis, and here's what they showed:

**HbA1C by weight-loss vs baseline:**

![[1-28.png]]

**FBG by weight-loss vs baseline:**

![[1-27.png]]

Note that I am comparing LCHF to baseline rather than control. This is because I want to know whether or not LCHF has an effect at all, not whether or not it performs better than control. I want to know if subjects saw an improvement from where they were, not whether or not there was a difference relative to the comparator diet.

As you can see, there is insufficient evidence to suggest that either HbA1C or FBG will budge an inch without weight loss. Granted, in the case of FBG, there is only one study in the lowest weight loss subgroup. As I've mentioned before in my writing, you cannot meta-analyze a a sample of one. But this doesn't bode well for LCHF advocates either way. There is virtually no evidence that LCHF lowers HbA1C or FBG independent of weight loss. It has not been persuasively demonstrated. It's the same story for insulin too.

**Fasting insulin by weight-loss vs baseline:**

![[1-29.png]]

Again, there is insufficient evidence that insulin can be reduced without weight loss either. Based on all of the data together, I'm utterly unconvinced that LCHF remits T2DM in any capacity independent of weight loss.

**Key points:**

-   It has been claimed that LCHF reverse and/or remit T2DM independent of weight loss.
-   No aggregate of the available literature supports this hypothesis.
-   LCHF has not been shown to reduce fasting blood glucose independent of weight loss.
-   LCHF has not been shown to reduce HbA1C independent of weight loss.
-   LCHF has not been shown to reduce fasting insulin independent of weight loss.

#patreon_articles 
#nutrition 
#low_carb 
#keto 
#type_2_diabetes 
#blood_glucose 
#hba1c 