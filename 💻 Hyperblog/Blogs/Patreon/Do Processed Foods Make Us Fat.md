I've heard dozens of answers to this question, ranging from plausible to absolutely absurd. Luckily, researchers have actually investigated this question and have yielded some highly practical answers. The truth is that there is an entire constellation of mechanisms that lead to processed foods having a tendency to make us fat. But here are just a few of the most important ones.

The first reason is that processed foods just taste really damn good. When we eat processed foods instead of whole foods, we just tend to eat more [1](https://www.ncbi.nlm.nih.gov/pubmed/31105044). The feeding efficiency of calorie-dense junk food is just so high that achieving satiety typically requires additional calories.

The second reason is that processed foods are incredibly easy to digest, and this has a massive impact on our energy expenditure after a meal. It costs double the calories to digest and metabolize a whole food meal when compared to a processed food meal [2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2897733/). This means that even when calories are equated, a meal of processed food is more likely to cause weight gain than a meal of whole foods.

The third reason is that processed foods are often sorely lacking in protein. Protein has an independent effect on increasing our energy expenditure and lean body mass. Our lean tissue is a massive energy sink, and provides a buffer for excess calories. Chronically under-consuming protein can decrease our lean body mass and lead to lower energy expenditures over time [3](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3777747/). This exacerbates the first two issues, potentially creating a downward spiral.

**Key points:**

-   Processed foods usually taste super awesome and can cause us to overeat.
-   Processed foods cause us to burn fewer calories, making fat gain more likely.
-   Processed foods often lack protein and can negatively affect our lean body mass.

**References:**

[1] Hall KD, et al. Ultra-Processed Diets Cause Excess Calorie Intake and Weight Gain: An Inpatient Randomized Controlled Trial of Ad Libitum Food Intake. Cell Metab. July 2019 [https://www.ncbi.nlm.nih.gov/pubmed/31105044](https://www.ncbi.nlm.nih.gov/pubmed/31105044)

[2] Sadie B. Barr and Jonathan C. Wright. Postprandial energy expenditure in whole-food and processed-food meals: implications for daily energy expenditure. Food Nutr Res. July 2010. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2897733/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2897733/) 

[3] George A. Bray, et al. Effect of Dietary Protein Content on Weight Gain, Energy Expenditure, and Body Composition During Overeating. JAMA. January 2012. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3777747/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3777747/)

#patreon_articles 
#nutrition 
#disease 
#processed_food 
#obesity 
#weight_gain 
