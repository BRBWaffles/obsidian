Phytates are a class of molecules found in most plants that act to bind certain minerals in our foods. Phytate primarily binds iron, zinc, calcium, manganese, and possibly magnesium. The binding of minerals to phytate inhibits the absorption of these minerals, and explains why we refer to phytate as an anti-nutrient. We're warned by many nutrition gurus to limit the phytate load of our diet, and some even suggest that we avoid plants all together merely on the basis of phytate and compounds like phytate. Here I'll argue that there are nuances to this discussion worth considering before we decide to never eat another legume in our lives.

While it is true that phytate binds certain minerals, I'm not convinced that this alone justifies food avoidance on the basis of phytate alone. In terms of the overall diet, it is true that the mineral to phytate ratio is actually a pretty decent proxy for the nutritional status of certain minerals [1](https://www.ncbi.nlm.nih.gov/pubmed/20715598)[2](https://www.ncbi.nlm.nih.gov/pubmed/22990464). But, it's possible that this data is confounded by low intakes of mineral-rich animal foods. So, how relevant is this to people like us in Western society? 

Most research indicates that the inhibition of mineral bioavailability due to phytate is a function of food pairing, rather than the mere inclusion of phytate-rich foods in the overall diet [3](https://www.ncbi.nlm.nih.gov/pubmed/458251). In other words, the negative effects of phytate have more to do with how you eat rather than what you eat. If you eat oats for breakfast, the phytate from those oats is not at all likely to inhibit any of the minerals from the steak you have for dinner. However if you eat oats and steak in the same meal, it is highly likely that many of the minerals in the steak will not be absorbed.

There is also another layer of nuance. If you must pair phytate-rich foods with mineral-rich foods, there are steps you can take to limit phytate's ability to bind minerals in the gastrointestinal tract. Soaking phytate-rich foods significantly reduces the phytate content of various foods [4](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3550828/).  Adding vitamin C to the meal can improve mineral bioavailability and absorption [5](https://www.ncbi.nlm.nih.gov/pubmed/1989423)[6](https://www.ncbi.nlm.nih.gov/pubmed/2911999). Lastly, as odd as it sounds, adding meat itself might actually enhance the absorption of non-heme iron [7](https://www.ncbi.nlm.nih.gov/pubmed/6538742)[8](https://www.ncbi.nlm.nih.gov/pubmed/12499338).

**Key points:**

-   Phytate-rich foods do inhibit the absorption of minerals from other foods in the diet.
-   The phytate-to-mineral ratio of the diet is a decent proxy for overall mineral status, but is likely due to inappropriate food pairing.
-   Pairing phytate-rich foods with mineral-rich foods is the most relevant consideration regarding the inhibition of mineral absorption.
-   Food avoidance on the basis of phytate is probably unjustified.
-   Soaking, vitamin C, and perhaps even meat itself can improve the bioavailability of minerals from phytate-rich foods.
-   Many phytate-rich foods are healthy and nutritious in ways unrelated to their mineral content.

**References:**

[1] Gibson RS, et al. A review of phytate, iron, zinc, and calcium concentrations in plant-based complementary foods used in low-income countries and implications for bioavailability.  Food Nutr Bull. 2010 Jun. [https://www.ncbi.nlm.nih.gov/pubmed/20715598](https://www.ncbi.nlm.nih.gov/pubmed/20715598) 

[2] Abizari AR, et al. Phytic acid-to-iron molar ratio rather than polyphenol concentration determines iron bioavailability in whole-cowpea meal among young women. J Nutr. 2012 Nov. [https://www.ncbi.nlm.nih.gov/pubmed/22990464](https://www.ncbi.nlm.nih.gov/pubmed/22990464) 

[3] Solomons NW, et al. Studies on the bioavailability of zinc in man. II. Absorption of zinc from organic and inorganic sources. J Lab Clin Med. 1979 Aug. [https://www.ncbi.nlm.nih.gov/pubmed/458251](https://www.ncbi.nlm.nih.gov/pubmed/458251)

[4] Vellingiri Vadivel and Hans K. Biesalski. Effect of certain indigenous processing methods on the bioactive compounds of ten different wild type legume grains. J Food Sci Technol. 2012 Dec. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3550828](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3550828/)

[5] Siegenberg D, et al. Ascorbic acid prevents the dose-dependent inhibitory effects of polyphenols and phytates on nonheme-iron absorption. Am J Clin Nutr. 1991 Feb. [https://www.ncbi.nlm.nih.gov/pubmed/1989423](https://www.ncbi.nlm.nih.gov/pubmed/1989423)

[6] Hallberg L, et al. Iron absorption in man: ascorbic acid and dose-dependent inhibition by phytate. Am J Clin Nutr. 1989 Jan. [https://www.ncbi.nlm.nih.gov/pubmed/2911999](https://www.ncbi.nlm.nih.gov/pubmed/2911999) 

[7] Hallberg L and Rossander L. Improvement of iron nutrition in developing countries: comparison of adding meat, soy protein, ascorbic acid, citric acid, and ferrous sulphate on iron absorption from a simple Latin American-type of meal. Am J Clin Nutr. 1984 Apr. [https://www.ncbi.nlm.nih.gov/pubmed/6538742](https://www.ncbi.nlm.nih.gov/pubmed/6538742)

[8] Baech SB, et al. Nonheme-iron absorption from a phytate-rich meal is increased by the addition of small amounts of pork meat. Am J Clin Nutr. 2003. [https://www.ncbi.nlm.nih.gov/pubmed/12499338](https://www.ncbi.nlm.nih.gov/pubmed/12499338)

#patreon_articles 
#nutrition 
#disease 
#antinutrients 
#phytate 
#clownery 
