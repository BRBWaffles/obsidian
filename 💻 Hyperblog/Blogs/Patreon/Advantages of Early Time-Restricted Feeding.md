A recent study has been floating around the nutrition blogosphere for the last year that seems to indicate that early time-restricted feeding (eTRF) has some unique advantages [1](https://www.cell.com/cell-metabolism/fulltext/S1550-4131(18)30253-5). The protocol involved the experimental group eating all their meals within a six hour window starting at 7am. The control group had the same meals spaced out within a twelve hour window, eating on typical breakfast-lunch-dinner schedule. The study was carried out over five weeks.

The results? The eTRF group had lower insulin area-under-the-curve, greater insulin sensitivity, greater β-cell responsiveness, lower hsCRP, lower blood pressure, and reported greater satiety and feelings of fullness. Sounds great, and I won't deny that all of those things are awesome results. However, like everything in life, there's a cost. The eTRF group also had higher triglycerides, higher average blood glucose, higher total cholesterol, lower HDL-cholesterol, higher LDL-cholesterol, and higher levels of an inflammatory cytokine called IL-6. The eTRF group also had wider variations in cortisol, with some of the cohort having higher cortisol than baseline, whereas the control group had no participants with cortisol higher than baseline.

The point here is that we have to be very careful when we claim things have an advantage, because advantages usually come with drawbacks. Often when something seems to have an advantage, those advantages are often equally offset by costs that may not be immediately obvious.

**Key points:** 

-   eTRF seems to improve glucose disposal and insulin sensitivity.
-   eTRF seems to improve feelings of satiety and fullness. 
-   eTRF seems to be anti- and pro-inflammatory in different ways.
-   eTRF seems to perturb blood lipids in a negative way.
-   eTRF seems to be more physiologically stressful.

**References:**

[1] Elizabeth F. Sutton, et al. Early Time-Restricted Feeding Improves Insulin Sensitivity, Blood Pressure, and Oxidative Stress Even without Weight Loss in Men with Prediabetes. Cell Metab. June 2018. [https://www.cell.com/cell-metabolism/fulltext/S1550-4131(18)30253-5](https://www.cell.com/cell-metabolism/fulltext/S1550-4131(18)30253-5)

#patreon_articles 
#nutrition 
#disease 
#fasting 
#clownery