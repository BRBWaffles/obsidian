It has been suggested that a plant-based diet leaves one vulnerable to certain nutrient deficiencies. While I believe that this is true, I also believe that these concerns are incredibly overblown. The typical nutrients of concern are: vitamin B12, vitamin D, calcium, iron, and zinc. However, I'm not actually persuaded that any of these nutrients are uniquely concerning on a vegan diet in the general population. 

Research investigating B12 status in vegans comes up mixed, but in populations wherein supplementation is widely practiced, serum B12 does not significantly differ from that of omnivores [1](https://pubmed.ncbi.nlm.nih.gov/26502280). It is also not clear that merely being vegan makes much of a difference for vitamin D status [2](https://pubmed.ncbi.nlm.nih.gov/19339396). Maintaining adequate calcium status as a vegan is merely about consuming an adequate amount of calcium-rich foods [3](https://pubmed.ncbi.nlm.nih.gov/12491091). Lastly, I'm not convinced that iron and zinc are of general concern. It hasn't been persuasively demonstrated that animal food restriction uniquely predisposes to iron or zinc deficiency [4](https://pubmed.ncbi.nlm.nih.gov/27880062)[5](https://pubmed.ncbi.nlm.nih.gov/23595983). 

Food selection is king. When people are making improper food selections to target these nutrients, of course deficiency can occur [6](https://pubmed.ncbi.nlm.nih.gov/14988640). On a vegan diet, we don't get iron from fruit and vegetables unless we're eating a lot of black olives, haha. Soaked legumes and cooked whole grains would be better choices in that regard. 

However, there is one nutrient that I did stumble across that appears to be consistently problematic across vegan populations. Vitamin B6 deficiency seems to loom over various vegan and vegetarian populations, despite more than adequate intakes [7](https://pubmed.ncbi.nlm.nih.gov/16925884)[8](https://pubmed.ncbi.nlm.nih.gov/16988496)[9](https://pubmed.ncbi.nlm.nih.gov/1797957). There are plausible mechanisms by which vitamin B6 could be limiting on a vegan diet without supplementation, which I write about [here](https://thenutrivore.blogspot.com/2019/05/animal-nutrients-part-1-vitamin-b6.html). 

Essentially, vitamin B6 from plant foods typically comes in the form of pyridoxine glucoside, which is significantly less bioavailable than free pyridoxine or preformed pyridoxal [10](https://pubmed.ncbi.nlm.nih.gov/2843032)[11](https://pubmed.ncbi.nlm.nih.gov/9237945). Some of the only decent non-animal sources of vitamin B6 are avocados, bananas, and perhaps lentils. Not even nutritional yeast, which is usually a B-vitamin powerhouse, has a decent amount of vitamin B6.  So, it's slim pickings for adequate sources if you're avoiding animal foods. But this might not be enough. While factoring in bioavailability it would take either four average bananas per day to meet sufficiency for vitamin B6, or four California variety avocados per day. For many people there are barriers to doing either consistently.

I'm not trying to say that vitamin B6 will necessarily become a problem on a vegan diet. My perspective on this is that those following diets that severely restrict animal foods should probably find it prudent to consider adding vitamin B6 as pyridoxal-5-phosphate to their supplementation regimen as a precaution.

**Key points:**

-   Vitamin B12, vitamin D, calcium, iron, and zinc aren't overly concerning on vegan diets.
-   Nutritional adequacy on a vegan diet can mostly be met with proper food selection.
-   Vitamin B6 deficiency is very common in both vegan and vegetarian populations. 
-   Vitamin B6 from non-animal foods is not particularly bioavailable to humans.
-   Supplementing with vitamin B6 as P-5-P would be prudent on vegan diets.

**References:**

[1] R Schüpbach, et al. Micronutrient Status and Intake in Omnivores, Vegetarians and Vegans in Switzerland. Eur J Nutr. 2017. Feb. [https://pubmed.ncbi.nlm.nih.gov/26502280](https://pubmed.ncbi.nlm.nih.gov/26502280/)

[2] Jacqueline Chan, et al. Serum 25-hydroxyvitamin D Status of Vegetarians, Partial Vegetarians, and Nonvegetarians: The Adventist Health Study-2. Am J Clin Nutr. 2009 May. [https://pubmed.ncbi.nlm.nih.gov/19339396](https://pubmed.ncbi.nlm.nih.gov/19339396/)

[3] Kathrin Kohlenberg-Mueller and Ladislav Raschka. Calcium Balance in Young Adults on a Vegan and Lactovegetarian Diet. J Bone Miner Metab. 2003. [https://pubmed.ncbi.nlm.nih.gov/12491091](https://pubmed.ncbi.nlm.nih.gov/12491091/)

[4] Lisa M Haider, et al. The Effect of Vegetarian Diets on Iron Status in Adults: A Systematic Review and Meta-Analysis. Crit Rev Food Sci Nutr. 2018 May. [https://pubmed.ncbi.nlm.nih.gov/27880062](https://pubmed.ncbi.nlm.nih.gov/27880062/)

[5] Meika Foster, et al. Effect of Vegetarian Diets on Zinc Status: A Systematic Review and Meta-Analysis of Studies in Humans. J Sci Food Agric. 2013 Aug. [https://pubmed.ncbi.nlm.nih.gov/23595983](https://pubmed.ncbi.nlm.nih.gov/23595983)

[6] Annika Waldmann, et al. Dietary Iron Intake and Iron Status of German Female Vegans: Results of the German Vegan Study. Ann Nutr Metab. 2004. [https://pubmed.ncbi.nlm.nih.gov/14988640](https://pubmed.ncbi.nlm.nih.gov/14988640)

[7] A Waldmann, et al. Dietary Intake of Vitamin B6 and Concentration of Vitamin B6 in Blood Samples of German Vegans. Public Health Nutr. 2006 Sep. [https://pubmed.ncbi.nlm.nih.gov/16925884](https://pubmed.ncbi.nlm.nih.gov/16925884/)

[8] D Majchrzak, et al. B-vitamin Status and Concentrations of Homocysteine in Austrian Omnivores, Vegetarians and Vegans. Ann Nutr Metab. 2006. [https://pubmed.ncbi.nlm.nih.gov/16988496](https://pubmed.ncbi.nlm.nih.gov/16988496/)

[9] N Vudhivai, et al. Vitamin B1, B2 and B6 Status of Vegetarians. J Med Assoc Thai. 1991 Oct. [https://pubmed.ncbi.nlm.nih.gov/1797957](https://pubmed.ncbi.nlm.nih.gov/1797957/)

[10] R D Reynolds, et al. Bioavailability of Vitamin B-6 From Plant Foods. Am J Clin Nutr. 1988 Sep. [https://pubmed.ncbi.nlm.nih.gov/2843032](https://pubmed.ncbi.nlm.nih.gov/2843032/)

[11] H Nakano, et al. Pyridoxine-5'-beta--glucoside Exhibits Incomplete Bioavailability as a Source of Vitamin B-6 and Partially Inhibits the Utilization of Co-Ingested Pyridoxine in Humans. J Nutr. 1997 Aug. [https://pubmed.ncbi.nlm.nih.gov/9237945](https://pubmed.ncbi.nlm.nih.gov/9237945/)

#patreon_articles 
#nutrition 
#disease 
#nutrient_deficiency 
#nutrients 
#vitamin_b6 
#vegan_talking_points 
