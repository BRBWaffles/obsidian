Many of you guys contacted me and asked me to distill my recent [video](https://www.patreon.com/posts/vlog-4-building-53314386) on building a healthy diet down into something easier to understand. So I took the opportunity to add more foods and food categories, such as alcohol, coffee, and chocolate. 

As I suspected, the results are largely consistent with the guidelines, but there are some key differences. I also hope to build this over time as I get more foods, food groups, and endpoints to add to the list. Enjoy!

**OVERWHELMINGLY POSITIVE**

-   Coffee/Tea (<580ml/day)
-   Fruit (>220g/day)
-   Legumes (>160g/day)
-   Nuts (>20g/day)
-   Unsaturated Fat (>28g/day)
-   Vegetables (>200g/day)
-   Whole Grains (>100g/day)

**MOSTLY POSITIVE**

-   Cheese (>20g/day)
-   Cocoa (<25g/day)
-   Fish (>120g/day)
-   Milk (~500ml/day)
-   Mushrooms (>20g/day)
-   Soy (>40g/day)
-   Yogurt (>230g/day)

**MOSTLY NEUTRAL**

-   Eggs (<4/week)
-   Lean Meat (~100g/day)
-   Unfried Potatoes (~60g/day)
-   Refined Grains (<90g/day)

**MOSTLY NEGATIVE**

-   Alcohol (<20ml/day)
-   Fried Potatoes (<60g/day)
-   Saturated Fat (<22g/day)
-   Sodium (<2000mg/day)

**OVERWHELMINGLY NEGATIVE**

-   Processed Meat (<1g/day)
-   Red Meat (<20g/day)
-   Sugary Drinks (<150ml/day)

**Results:**

These results are excluding the "Mostly Negative" and "Overwhelmingly Negative" groups. Interestingly it checks all of the boxes. Complete essential nutrition is achieved, including adequate fibre and low saturated fat. Shockingly, the diet also seems to provide adequate choline, 6.5g of omega-3, and over 5g of potassium! This diet is ballin' straight outta fucking control!

![[1-68.png]]

![[1-69.png]]

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/29666853/](https://pubmed.ncbi.nlm.nih.gov/29666853/)   
[2] [https://pubmed.ncbi.nlm.nih.gov/31584249/](https://pubmed.ncbi.nlm.nih.gov/31584249/)   
[3] [https://pubmed.ncbi.nlm.nih.gov/31278047/](https://pubmed.ncbi.nlm.nih.gov/31278047/)   
[4] [https://pubmed.ncbi.nlm.nih.gov/28324761/](https://pubmed.ncbi.nlm.nih.gov/28324761/)   
[5] [https://pubmed.ncbi.nlm.nih.gov/27517544/](https://pubmed.ncbi.nlm.nih.gov/27517544/)   
[6] [https://pubmed.ncbi.nlm.nih.gov/28671591/](https://pubmed.ncbi.nlm.nih.gov/28671591/)   
[7] [https://pubmed.ncbi.nlm.nih.gov/30061161/](https://pubmed.ncbi.nlm.nih.gov/30061161/)   
[8] [https://pubmed.ncbi.nlm.nih.gov/25646334/](https://pubmed.ncbi.nlm.nih.gov/25646334/)   
[9] [https://pubmed.ncbi.nlm.nih.gov/31970674/](https://pubmed.ncbi.nlm.nih.gov/31970674/)   
[10] [https://pubmed.ncbi.nlm.nih.gov/28655835/](https://pubmed.ncbi.nlm.nih.gov/28655835/)   
[11] [https://pubmed.ncbi.nlm.nih.gov/28446499/](https://pubmed.ncbi.nlm.nih.gov/28446499/)   
[12] [https://pubmed.ncbi.nlm.nih.gov/29039970/](https://pubmed.ncbi.nlm.nih.gov/29039970/)   
[13] [https://pubmed.ncbi.nlm.nih.gov/30801613/](https://pubmed.ncbi.nlm.nih.gov/30801613/)   
[14] [https://pubmed.ncbi.nlm.nih.gov/29141965/](https://pubmed.ncbi.nlm.nih.gov/29141965/)   
[15] [https://pubmed.ncbi.nlm.nih.gov/29210053/](https://pubmed.ncbi.nlm.nih.gov/29210053/)   
[16] [https://pubmed.ncbi.nlm.nih.gov/28397016/](https://pubmed.ncbi.nlm.nih.gov/28397016/)   
[17] [https://pubmed.ncbi.nlm.nih.gov/29978377/](https://pubmed.ncbi.nlm.nih.gov/29978377/)   
[18] [https://pubmed.ncbi.nlm.nih.gov/29987352/](https://pubmed.ncbi.nlm.nih.gov/29987352/)   
[19] [https://pubmed.ncbi.nlm.nih.gov/28592612/](https://pubmed.ncbi.nlm.nih.gov/28592612/)  
[20] [https://pubmed.ncbi.nlm.nih.gov/31055709/](https://pubmed.ncbi.nlm.nih.gov/31055709/)   
[21] [https://pubmed.ncbi.nlm.nih.gov/26997174/](https://pubmed.ncbi.nlm.nih.gov/26997174/)   
[22] [https://pubmed.ncbi.nlm.nih.gov/21162794/](https://pubmed.ncbi.nlm.nih.gov/21162794/)   
[23] [https://pubmed.ncbi.nlm.nih.gov/24691133/](https://pubmed.ncbi.nlm.nih.gov/24691133/)   
[24] [https://pubmed.ncbi.nlm.nih.gov/31915830/](https://pubmed.ncbi.nlm.nih.gov/31915830/)   
[25] [https://pubmed.ncbi.nlm.nih.gov/31754945/](https://pubmed.ncbi.nlm.nih.gov/31754945/)  
[26] [https://pubmed.ncbi.nlm.nih.gov/34165394/](https://pubmed.ncbi.nlm.nih.gov/34165394/)

#patreon_articles 
#dietary_guidelines 
#whole_foods 
#healthy_diets
#nutrition 
#disease 