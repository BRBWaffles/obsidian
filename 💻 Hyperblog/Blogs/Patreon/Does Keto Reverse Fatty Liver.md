Do low-carbohydrate ketogenic diets (LCKD) reverse fatty liver? This idea is circulated widely in many LCKD communities, and the idea itself is actually very simple and has some mechanistic plausibility— ketones are made from fatty acids in the liver, therefore a LCKD will burn through those fatty acids faster than a non-ketogenic diet. Let's investigate this.  
  
There have been plenty of trials investigating the effects of LCKDs on abdominal fat mass (AFM) in humans [1](https://pubmed.ncbi.nlm.nih.gov/19373224/)[2](https://pubmed.ncbi.nlm.nih.gov/31963475/)[3](https://pubmed.ncbi.nlm.nih.gov/18326593/)[4](https://pubmed.ncbi.nlm.nih.gov/31277506/)[5](https://pubmed.ncbi.nlm.nih.gov/19082851/)[6](https://pubmed.ncbi.nlm.nih.gov/33042004/)[7](https://pubmed.ncbi.nlm.nih.gov/32179679/). Most of them see a benefit of the LCKD. In the aggregate, LCKDs tend to lead to an average of 730g of AFM.

**Abdominal Fat Mass:**

![[1-71.png]]

**Intrahepatic Triglycerides:**

![[Pasted image 20221123154528.png]]

The majority of studies find a statistically significant reduction in AFM, but does this actually mean that the keto rabble were right? No really. Not yet, anyway. Other research shows that non-ketogenic weight loss can produce linear reductions in AFM [8](https://pubmed.ncbi.nlm.nih.gov/31685793/).

![[Pasted image 20221123154517.png]]

In this paper, obese women were put on weight loss diets that were 50% carbohydrates by energy. There was a linear reduction in AFM that was commensurate with a reduction in total body weight (R^2 = 0.981). 

So, what happens when we scrutinize the LCKD literature in the same way, and stratify the studies by achieved weight loss and AFM reduction?

![[Pasted image 20221123154457.png]]

We see virtually the exact same relationship (R^2 = 0.857). The more weight that subjects lost, the more AFM that they lost as well. In fact, no study investigating the relationship between LCKDs and AFM has succeeded in keeping subjects weight stable. Every last study saw reductions in body fat, even these two additional studies that couldn't be included in the forest plots above due to unclear reporting [9](https://pubmed.ncbi.nlm.nih.gov/29456073/)[10](https://pubmed.ncbi.nlm.nih.gov/17219068/).  
  
It is also untrue that LCKDs offer immunity to AFM accumulation. This is evidenced by a case report of a 57-year old woman who developed hepatic steatosis in response to a self-administered LCKD [11](https://pubmed.ncbi.nlm.nih.gov/32064187/). Her diet consisted of eggs, cheese, butter, oil, nuts, leafy green vegetables, and low-carbohydrate plant-based milk alternatives.

![[Pasted image 20221123154452.png]]

In conclusion, we can say that LCKDs do tend to reduce AFM in humans. This is because LCKDs tend to lead to weight loss when compared to the average diet. However, that is not to say that LCKDs actually lead to unique reductions in AFM independent of weight loss. That has not yet been demonstrated, and more research is required to elucidate the effects of LCKDs on AFM.

**Key points:**

-   It is claimed that ketogenic diets uniquely reduce abdominal fat mass independent of weight loss.
-   No study to date has controlled for weight loss well enough to divulge this.
-   There is no evidence that ketogenic diets reduce abdominal fat mass independent of weight loss.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/19373224/](https://pubmed.ncbi.nlm.nih.gov/19373224/) 

[2] [https://pubmed.ncbi.nlm.nih.gov/31963475/](https://pubmed.ncbi.nlm.nih.gov/31963475/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/18326593/](https://pubmed.ncbi.nlm.nih.gov/18326593/) 

[4] [https://pubmed.ncbi.nlm.nih.gov/31277506/](https://pubmed.ncbi.nlm.nih.gov/31277506/) 

[5] [https://pubmed.ncbi.nlm.nih.gov/19082851/](https://pubmed.ncbi.nlm.nih.gov/19082851/) 

[6] [https://pubmed.ncbi.nlm.nih.gov/33042004/](https://pubmed.ncbi.nlm.nih.gov/33042004/) 

[7] [https://pubmed.ncbi.nlm.nih.gov/32179679/](https://pubmed.ncbi.nlm.nih.gov/32179679/) 

[8] [https://pubmed.ncbi.nlm.nih.gov/31685793/](https://pubmed.ncbi.nlm.nih.gov/31685793/) 

[9] [https://pubmed.ncbi.nlm.nih.gov/29456073/](https://pubmed.ncbi.nlm.nih.gov/29456073/) 

[10] [https://pubmed.ncbi.nlm.nih.gov/17219068/](https://pubmed.ncbi.nlm.nih.gov/17219068/) 

[11] [https://pubmed.ncbi.nlm.nih.gov/32064187/](https://pubmed.ncbi.nlm.nih.gov/32064187/)

#patreon_articles 
#nutrition 
#disease 
#non_alcoholic_fatty_liver_disease 
#keto 