Since I escaped the clutches of low carb zealotry, I've warmed up to a lot of conventional ideas about what constitutes a generally healthful diet. I've changed my positions on saturated fat, vegetable oils, sugar, carbohydrates, calories, etc. All of which I've discussed at length on my blogs and social media. I've also changed my views on dietary fibre, despite not really discussing it that much in the past.

As I write this, I am of the opinion that dietary fibre is critically important for long-term human health, despite it technically not being an essential nutrient. My current diet gives me about 30-50g/day of fibre on average. The DRI for fibre is 38g/day for men and 25g/day for women. But, I was curious to know if there was any evidence that perhaps higher intakes could yield additional benefits.

I stumbled across this systematic review and meta-analysis of dietary fibre intakes and cardiovascular disease (CVD) and coronary heart disease risk (CHD) [1](https://pubmed.ncbi.nlm.nih.gov/24355537/). As I read through it I was struck by these graphs, which illustrate the estimated effect of dietary fibre intake on CVD and CHD risk. 

![[1-1.jpg]]

They suggested that we have at least one study (marked as red bars) suggesting that fibre could confer a benefit to CVD and CHD risk reduction all the way up to >60g/day. I'd never heard of intakes that high being studied before.

I managed to track down the paper [2](https://pubmed.ncbi.nlm.nih.gov/23543118/). The fibre intakes were estimated using two different methods. One method yielded an estimated intake of ~63g/day, and multivariate analyses showed that intakes this high could reduce CVD risk by ~40% and CHD risk by ~30%. Huge numbers. 

In groups whose fibre intakes approximated the DRI, CVD risk was reduced by ~40% yet again, but CHD risk was only reduced by 20%. Still huge numbers, but this could suggest benefits to consuming fibre at levels that exceed the current DRI.

**Key points:**

-   Meeting the DRI for fibre intake confers consistent benefits to CVD and CHD risk.
-   There is evidence that suggests eating even more fibre could confer additional benefits.

**References:**

[1] Diane E Threapleton, et al. Dietary fibre intake and risk of cardiovascular disease: systematic review and meta-analysis. BMJ. 2013 Dec. [https://pubmed.ncbi.nlm.nih.gov/24355537/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3898422/)

[2] Diane E Threapleton, et al. Dietary fibre and cardiovascular disease mortality in the UK Women's Cohort Study. Eur J Epidemiol. 2013 Apr. [https://pubmed.ncbi.nlm.nih.gov/23543118/](https://pubmed.ncbi.nlm.nih.gov/23543118/)

#patreon_articles 
#nutrition 
#disease 
#fibre 
#cardiovascular_disease