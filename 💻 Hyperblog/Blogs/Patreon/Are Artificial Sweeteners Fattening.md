No health and nutrition echo-chamber is without its ridiculous quackery, but sometimes there are examples of quackery that is so deeply rooted that they transcend the differences between these echo-chambers. For example, the notion that vegetable oils are bad for our health is a piece of nonsensical rhetoric that has managed to find its way into many competing camps. This is also true of the idea that artificial sweeteners cause weight gain— keto quacks believe it, vegan quacks believe it, and even if-it-fits-your-macros gym bro quacks believe it.

We have no shortage of data on this subject. We have meta-analyses of both prospective cohort studies and randomized controlled trials investigating the relationship between artificial sweeteners and weight gain. Let's review some of those findings.

In one meta-analysis investigating the relationship between artificial sweeteners and body composition, there was a statistically significant association between artificial sweeteners and increased BMI [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4135487/).

![[1-33.png]]

However, when the same endpoints were measured in randomized controlled trials, the authors' finders were somewhat contradictory. They showed a statistically significant decrease in fat mass, waist circumference, and BMI.

![[1-32.png]]

Another meta-analysis investigating the same question found no overall association between artificial sweeteners and weight gain in prospective cohort studies. When the included studies were stratified by baseline body weight, a statistically significant reduction in weight was observed in overweight or obese subjects [2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6313893/).

![[1-31.png]]

It would be negligent of me to not also address claims made about artificial sweeteners and insulin secretion in humans. As it has been suggested by some (particularly in the low carb community) that artificial sweeteners could increase weight gain by increasing levels of circulating insulin. Which is an interesting hypothesis, however this hypothesis would have difficulty explaining why artificial sweeteners seem to be superior for weight loss, even when compared to water in human trials [3](https://pubmed.ncbi.nlm.nih.gov/24862170/).

But, let's dig into this question. There is at least one study that directly measured insulin secretion in humans after dosing various types of artificial or non-nutritive sweeteners [4](https://pubmed.ncbi.nlm.nih.gov/27956737/). In this study, subjects were given a preload meal of artificial sweeteners for breakfast, and also consumed artificial sweeteners with a lunch. For both meals, a glucose-based drink was used as the control.

![[Pasted image 20221123153414.png]]

Findings are consistent with another study investigating the relationship between an artificial sweetener called sucralose and changes in plasma insulin [5](https://pubmed.ncbi.nlm.nih.gov/19221011/). Only the glucose control caused a statistically significant increase in plasma insulin. However, the glucose control actually lowered blood glucose compared to baseline after 90 minutes. The effect persisted for two and half hours afterward.

![[Pasted image 20221123153420.png]]

All in all, most of the narratives surrounding artificial sweeteners and weight gain, or even insulin secretion, do not pan out it human experiments. In light of the randomized controlled trial data, some of the associations between overweight and obesity in epidemiology are probably best explained by reverse causality— the artificial sweeteners aren't causing weight gain. The artificial sweeteners associate with overweight and obesity because overweight and obese people are more likely to drink artificial sweeteners. Presumably as a strategy to lose weight.

I have a couple hypotheses as to why artificial sweeteners would be better for weight loss. My first hypothesis is that the sweet taste actually causes people to drink more liquid than they would otherwise drink without the artificial sweeteners. This could actually reduce caloric intake by augmenting the satiety effects of each meal. My second hypothesis is that the artificial sweeteners could satisfy a desire for additional palette entertainment that would otherwise be satisfied with more calorie-dense foods. This could lead to greater diet adherence overall, due to dieters perhaps feeling less deprived.

**Key points**

-   It has been suggested that artificial sweeteners cause weight gain.
-   Human experiments shows that artificial sweeteners tend to lead to weight loss.
-   Artificial sweeteners do no increase plasma glucose or plasma insulin.

**References:** 

[1] [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4135487/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4135487/)

[2] [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6313893/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6313893/)

[3] [https://pubmed.ncbi.nlm.nih.gov/24862170/](https://pubmed.ncbi.nlm.nih.gov/24862170/)

[4] [https://pubmed.ncbi.nlm.nih.gov/27956737/](https://pubmed.ncbi.nlm.nih.gov/27956737/)

[5] [https://pubmed.ncbi.nlm.nih.gov/19221011/](https://pubmed.ncbi.nlm.nih.gov/19221011/)

#patreon_articles 
#nutrition 
#disease 
#obesity 
#artificial_sweeteners 
#metabolic_syndrome 
#type_2_diabetes 
