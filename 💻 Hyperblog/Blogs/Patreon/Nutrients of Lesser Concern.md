If you spend much time in the nutrition corner of social media, you may have noticed that there is an enormous amount of talk about nutrient deficiencies. It seems as though everyone is running deficient in something— vitamin D, calcium, zinc, etc. In some cases it may be true, sure. It may also be a decent recommendation to monitor your status of certain problem nutrients. But, did you know there are nutrients you probably never have to worry about?

I made a couple of interesting discoveries relating to nutrient deficiencies while attempting to research nutrient depletion-repletion studies. As it turns out, just as there are nutrient deficiencies that are relatively common, there are also nutrient deficiencies that almost never happen. There are certain nutrients that are very, very rarely associated with deficiencies.

Those nutrients are:

-   Vitamin B5
-   Choline
-   Manganese
-   Phosphorus

Deficiencies of vitamin B5, manganese, and phosphorus have only ever been produced in metabolic wards. Meaning that we've only observed clinical deficiencies of these nutrients when we've locked people up and only feed them diets devoid of those nutrients.

Deficiencies in choline are somewhat different. We first discovered that choline was required in the diet when patients receiving intravenous nutrition were fed choline-free formulas. That should say something about how common choline deficiencies are likely to be. We had to feed bed-ridden people choline-free diets intravenously to even discover that this nutrient was essential to get in the diet. 

It's been shown that 10% of the population require approximately double the AI of choline if they are deficient in riboflavin. But that's highly conditional and not a general effect. All in all it doesn't seem like choline deficiencies typically present themselves spontaneously in the population. Likely choline isn't much to worry about either. As I discuss [here](https://www.patreon.com/posts/elusive-choline-33684646), choline requirements depend on a large number of other nutritional factors and are incredibly difficult to characterize.

**Key points:**

-   Deficiencies in vitamin B5, manganese, and phosphorus almost never happen in free-living humans.
-   Deficiencies in choline are difficult to characterize because choline requirements largely depend on overall diet quality.

#patreon_articles 
#nutrition 
#disease 
#nutrients 
#vitamin_b5
#choline 
#manganese
#phosphorus
