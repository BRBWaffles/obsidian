This is an open letter to the British Journal of Sports Medicine, and a direct request for a correction/retraction of Zoe Harcombe's meta-analysis of saturated fat and coronary heart disease mortality in prospective cohort studies.

Essentially, there are three fundamental issues with the [paper](https://bjsm.bmj.com/content/51/24/1743.long) that I believe invalidate the conclusions. 

-   **The authors did not actually test their hypothesis**

Firstly, the included cohort studies were not investigated in a manner that tests the hypothesis stated in the objectives of the paper. One of the two stated objectives was to investigate the validity of the US and UK dietary guidelines to keep daily saturated fat (SFA) intake below 10% of energy. However, the included cohorts are not meta-analyzed in a manner that could actually test this. In order to test the validity of the US or UK dietary guidelines' recommendations to keep SFA under 10% of energy, cohorts would need to be stratified such that one subgroup has an intake range that crosses the threshold of 10% of energy as SFA. 

In other words, one subgroup would need to consist of cohorts that are consuming <10% of energy as SFA in the lowest 'tiles of intake and >10% of energy as SFA in the highest 'tiles of intake. Thus crossing the threshold of effect presupposed by both the US and UK dietary guidelines.

However, the cohorts in the paper are summated strictly to test for a completely linear relationship with no subgrouping or stratification of any kind. The paper concludes that the findings do not support the US and UK dietary guidelines, yet the authors made no attempt to actually test either the US or UK dietary guidelines. Both the US and UK dietary guidelines presuppose a non-linear relationship between cardiovascular disease (CVD) and SFA intake. 

The US dietary guidelines recommend that daily SFA intake be kept under 10% of energy [1](https://health.gov/sites/default/files/2019-10/DGA_Cut-Down-On-Saturated-Fats.pdf).

![[Pasted image 20221123152845.png]]

However, in addition to the recommendation to keep daily SFA intake below 10% of energy, the UK dietary guidelines give recommendations based on absolute intakes of SFA in grams per day [2](https://www.nhs.uk/live-well/eat-well/eat-less-saturated-fat/).

Both the US and UK dietary guidelines presuppose that consuming under 10% of energy as SFA is safe, but consuming above 10% of energy as SFA is unsafe and is associated with an increased CVD risk. The UK dietary guidelines additionally presupposes that consuming under 20g per day or 30g per day of SFA for women and men respectively is safe, but consuming intakes above those limits is unsafe and is associated with an increased CVD risk. 

The authors only tested for a linear relationship by pooling all of the cohorts together. Therefore, the conclusions cannot logically follow from the methods, nor the results. Nor do the methods logically follow from the objectives specified by the authors. There was no investigation based on intakes of SFA as a percentage of calories or as absolute intakes of SFA in grams per day.

Also, the UK dietary guidelines regarding recommended intakes of SFA are only directed toward those in the 19-64 years age group. Thus, the cohort in the 60-79 years age group from [Xu 2006](https://pubmed.ncbi.nlm.nih.gov/17023718/) cannot be used to test the hypothesis. This cohort is not within the age range toward which the UK dietary guidelines are directed.

-   **The authors are investigating the wrong endpoints**

Secondly, during the time in which this paper was written and published, the US and UK dietary guidelines suggested that daily intakes of SFA over 10% of energy increase CVD and/or heart disease risk [3](https://health.gov/sites/default/files/2019-09/2015-2020_Dietary_Guidelines.pdf)[4](https://www.nhs.uk/live-well/eat-well/different-fats-nutrition/). 

![[Pasted image 20221123152953.png]]

![[Pasted image 20221123152956.png]]

Also, the original US dietary guidelines published in 1977 also emphasized heart disease risk as the primary endpoint targeted by their recommendations [5](https://naldc.nal.usda.gov/download/1759572/PDF).

![[Pasted image 20221123153002.png]]

 Whereas the original UK dietary guidelines published in 1983 emphasize both heart disease risk and mortality [6](https://pubmed.ncbi.nlm.nih.gov/6136848/).

![[Pasted image 20221123153008.png]]

However, the authors only investigated coronary heart disease (CHD) mortality. This endpoint is not explicitly given higher priority over any other CVD related endpoint by either the US or UK dietary guidelines at the time the paper was published. 

I would conclude that the authors' inclusion/exclusion criteria are not structured in a fashion that actually investigates the recommendations put forth by either the US or UK dietary guidelines. The authors' inclusion/exclusion criteria need to be restructured to suit the hypothesis stated in their objectives.

The ambiguity of the US and UK dietary guidelines, both past and present, may necessitate the use of multiple meta-analyses with differing inclusion/exclusion criteria, such that different endpoints are investigated individually. Alternatively, a single-meta-analysis with inclusion/exclusion criteria that are loose enough to capture many different CVD-related endpoints may be sufficient. Nevertheless, merely selecting CHD mortality as the only relevant endpoint is arbitrary and should be corrected.

-   **Two of the included cohorts can't be used to test the hypothesis**

Lastly, two of the included papers cannot be included in the meta-analysis if the meta-analysis is to actually test both the US and UK dietary guidelines correctly. The papers [Boniface (2002)](https://pubmed.ncbi.nlm.nih.gov/12122556/) and [Pietinen (1997)](https://pubmed.ncbi.nlm.nih.gov/9149659/) do not report SFA intake as a percentage of energy. Nor can that data be calculated or inferred based on any other data included in either paper. There is no way to precisely know what percentage of calories were SFA within these two cohorts. Therefore they cannot be included in a meta-analysis that requires that information in order to test their stated hypothesis.

However, we _can_ actually investigate the relationship between absolute intakes of SFA and CHD mortality. Absolute intake data for SFA was either included in each paper, or could be calculated based on other data that was included in each paper. This allows us to specifically investigate the UK dietary guidelines to limit SFA intake to below 20-30g/day.

-   **My subgroup analysis of the authors' meta-analysis**

Figure 3 represents the authors' included cohorts, limited strictly to those that can be used to test either the US or UK dietary guidelines. The included cohorts support the UK dietary guidelines to keep SFA intake under ~25g/day for people between the ages of 19-64. A statistically significant increase is risk is seen. Conversely, no statistically significant increase in risk is observed when testing the US dietary guidelines, however only two studies are being analyzed in that subgroup.

**Figure 1**

![[Pasted image 20221123153223.png]]

Based on these findings and the inconsistencies within this manuscript, I request that the authors either correct these inconsistencies or that the manuscript be retracted on the basis of these inconsistencies and methodological errors. Thank you very much for your time and patience!"

[Supplementary Data](https://docs.google.com/spreadsheets/d/13HI3D5A34wkakXPMHwCB0sVjP3bEeBTvnLhbjeu8PLM/edit?usp=sharing)  
  
Reply from the BMJ (July 8th, 2020):

![[Pasted image 20221123153305.png]]

#patreon_articles 
#nutrition 
#disease 
#saturated_fat 
#cardiovascular_disease 
#clowns 
#clownery  
#debate
#zoe_harcombe 