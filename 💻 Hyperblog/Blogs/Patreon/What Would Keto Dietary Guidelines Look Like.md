There has been much talk about whether or not the guidelines should include low-carbohydrate or ketogenic options. Especially from higher-impact folks within the low-carb community, such as Nina Teicholz and Zoe Harcombe.This might be surprising to some of you, but this is actually a subject about which I agree with low-carb community.

I do actually feel strongly that the acceptable ranges of both fat and carbohydrates in the guidelines are unnecessarily constricted. The guidelines should probably include low carbohydrate options. So, I thought it would be a fun exercise to try to reconstruct the guidelines to accommodate a low-carbohydrate ketogenic dietary pattern.

Let's take a look at the current guidelines for adults between the ages of 19-59:

![[1-88.png]]

Right off the bat, it is clear that this exercise will necessarily involve eliminating some food groups from consideration. I'll also have to include some other food groups, and adjust others. Here's a list of modifications that I made:

-   1) Starchy Vegetables and have been removed.
-   2) Grain group replaced with Healthy Fats group.
-   3) Nuts and Seeds have been moved from Protein Foods to Healthy Fats.
-   4) Fruit was renamed to Fatty Fruit and was included in Healthy Fats.
-   5) Cocoa Products added to Healthy Fats.
-   6) Dairy was replaced with Plant Milks.
-   7) Plant Protein Products added to Protein Foods.
-   8) Oils was changed to Unsaturated Oils

Most of these changes should be relatively self-explanatory. Largely the changes are aiming to replace healthy carbohydrate sources with healthy fat sources. However, there are a few of changes that probably require some unpacking:

-   Change number five was to both appease the saturated fat loving low carb nuts, as well as to include an additional healthy high-fat food.
-   Change number six was actually to lower carbohydrates, because unsweetened plant milks are almost universally lower in carbohydrates than actual dairy milk.
-   Change number seven was because many animal foods are simply too high in saturated fat to be compatible with a guidelines-compliant ketogenic diet at the levels consumed in the original guidelines.

Here are the guidelines after my changes:

![[1-89.png]]

I tried to formulate these new keto-friendly guidelines to be consistent with the existing guidelines in spirit, meaning that they are cognizant of the primary recommendations. For example, the above formulation will typically produce a diet that is under 10% of energy as saturated fat, under 10% of energy as added sugar, under 2500mg per day of sodium, and restricts alcohol.

Here is an example of a 2000 kcal day of eating according to our hypothetical ketogenic guidelines, as well as the nutritional breakdown:

![[Pasted image 20221123152402.png]]

![[Pasted image 20221123152406.png]]

Despite being primarily plant-based, the diet provides under 35g/day of net carbohydrates. Also, despite having almost 142g/day of dietary fat, the diet also provides under 22g/day of saturated fat. Saturated fat can also be taken down further simply through the omission of the chocolate, so it's not a big issue at all in my view. The diet also provides very little dietary cholesterol, which is also in keeping with the spirit of the guidelines.

In conclusion, I think it is indeed possible to achieve a healthy ketogenic diet that aligns well with the recommendations provided by the dietary guidelines. I hope one day such a diet could be included in the guidelines, particularly as ketogenic diets continue to gain traction in the general population. With more people pursuing ketogenic diets, the more pressing it will become to have official guidance on the matter.

#patreon_articles 
#keto 
#dietary_guidelines 
#nutrition 