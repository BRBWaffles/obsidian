It's likely that most of us will have the intuition that fried potato products like potato chips and French fries are ultra-processed foods that are assuredly bad for out health. However, the evidence suggesting that these foods are actually bad for us isn't the greatest. Let's dive in.

First, let's see if fried potatoes even meet the definition of an ultra-processed food. The NOVA classification system is the closest thing to a standardized method for categorizing foods by their degree of processing [1](https://world.openfoodfacts.org/nova). According to this classification system, fried potatoes would be a "class 3" food, known as a "processed food".

![[1-79.png]]

However, many other foods, that nobody in their right mind would consider ultra-processed, also meet this bar. Foods like pan-fried steak, canned fish, or even cheese would also meet these criteria for categorization as a processed food. So yes, potato products are processed foods, but in terms of their categorization, not any more so than seared broccoli.

Now on to the observational evidence. Firstly, fried potato consumption is strongly associated with an increase in total mortality in one prospective cohort study [2](https://pubmed.ncbi.nlm.nih.gov/28592612/). They discovered a 2.26x increase in the risk of total death over the 8-year follow-up with intakes of fried potato products exceeding three servings per week.

![[1-77.png]]

There are some issues with this study, however. For starters, they did not disclose the mortality endpoints that constituted their total mortality figure, so it's unclear how much of that mortality would actually be relevant. For example, accidental deaths or even suicides could be potentially confounding.

Also, the precise food frequency questionnaire (FFQ) used in this cohort study was an older FFQ with fewer food options. There is a clear limitation with this particular FFQ, due to them lumping potato chips in with other non-potato snacks, as well as only providing a single entry for fried potatoes, which included multiple different products.

![[1-78.png]]

Another issue is the adjustment model. Rather than adjusting for dietary variables individually, they choose to adjust for "adherence to a Mediterranean diet", which could miss some major confounders if not carefully formulated.

Lastly, the cohort itself was a biased sample, as they were subjects from the Osteoarthritis Initiative cohort. This is a cohort that could already be at a higher risk of death, particularly accidental death due to injury. Couple this with the fact that the actual causes of death were not disclosed, and we can see a clear opportunity for confounding.

Contrast this with two cohort studies that used better data collection methods, better adjustment models, larger sample sizes, and populations that were less susceptible to potential bias [3](https://pubmed.ncbi.nlm.nih.gov/27680993/). They found no significant association between the consumption of fried potato products and any outcome related to cardiovascular disease (CVD). 

![[1-76.png]]

Granted, this is not entirely apples-to-apples. It could still be the case that fried potatoes increase the risk of non-CVD related diseases, like cancer or dementia. However, CVD is the endpoint that is most plausible (and the most discussed) with regards to fried potato consumption.

Segueing on to human experiments, I managed to find a single intervention trial using potato chips as the exposure [4](https://pubmed.ncbi.nlm.nih.gov/19158207/). The intervention itself is actually really cleverly designed. Essentially, researchers kept a group of subjects weight stable while feeding two different diets in sequence.

The first diet consisted of 400g of boiled potatoes per day, along with an amount of salt and heated vegetable fat that would equal the amounts of salt and fat found in potato chips when matched for carbohydrates. The next diet consisted of an isocaloric substitution of potato chips for the 400g of boiled potatoes and vegetable fat. 

This is an absolutely brilliant design. It could elucidate any independent effect of actually frying the potatoes themselves. That is, of course, if it were powered to do so. Unfortunately it is a non-randomized, single-armed pilot study involving only 14 subjects. It would be dubious to infer much of anything from this trial.

The paper purports that there were statistically significant increases in the inflammatory markers: AAHb, IL-6, hsCRP, and GGT. I calculated the change scores myself and found that there was actually no statistically significant increase in either AAHb or hsCRP. There was a statistically significant increase in both IL-6 and GGT. However, both were still smack-dab in the middle of the reference range, and the increases themselves were small and likely clinically irrelevant.

All in all, I remain agnostic about the potential long-term health value of fried potatoes, as I was not able to find any truly persuasive evidence that these foods actually cause harm. Especially with regards to CVD. Until better data comes out, it appears that their effect on health is likely rather neutral.

**Key points:**

-   Fried potatoes do not qualify as ultra-processed foods.
-   Fried potatoes have been shown to increase the risk of total mortality in prospective cohort studies that use questionable methods.
-   In prospective cohort studies that are well-designed and well-powered, fried potatoes show no increase in cardiovascular disease.
-   Current human trials do not have enough power to tell us how fried potato consumption affects intermediate markers of disease.

**References:**

[1] [https://world.openfoodfacts.org/nova](https://world.openfoodfacts.org/nova) 

[2] [https://pubmed.ncbi.nlm.nih.gov/28592612/](https://pubmed.ncbi.nlm.nih.gov/28592612/) 

[3] [https://pubmed.ncbi.nlm.nih.gov/27680993/](https://pubmed.ncbi.nlm.nih.gov/27680993/) 

[4] [https://pubmed.ncbi.nlm.nih.gov/19158207/](https://pubmed.ncbi.nlm.nih.gov/19158207/)

#patreon_articles 
#nutrition 
#potatoes 
#all_cause_mortality 
#food_frequency_questionnaires 
#epidemiology 
#cohort_studies 
#cardiovascular_disease 
#biomarkers 
