I missed one week's blog article earlier this month due to writing the [meta-analysis of low carb diets](https://thenutrivore.blogspot.com/2020/10/low-carbohydrate-diets-and-health.html). So, this week gets two articles to make up for it.  
  
I've written extensively about how the risk of cardiovascular disease (CVD) conferred by saturated fat (SFA) can easily be lost in the means. That is to say certain, less rigorous statistical analyses that have previously been performed on SFA as it relates to CVD tend to hide dose-dependent effects. This has been a significant obstacle to effective public health communication on the health risks of higher SFA diets. 

However, even when these analyses are done well, there are nuances that are lost. Considering SFA as a broad category actually hides instances wherein certain sources of SFA seem to reduce the risk of CVD. In this article I'll go over some high-SFA foods that are actually beneficial to CVD risk.

**Cocoa Products**

A meta-analysis in 2018 found that the consumption of cocoa products was associated with a decreased risk for all CVD endpoints, including stroke [1](https://pubmed.ncbi.nlm.nih.gov/30061161/). Their findings indicate that cocoa product consumption achieved a maximal risk reduction at 50g/week, and the association was null beyond 100g/week.

![[Pasted image 20221123152504.png]]

However, a meta-analysis published in the previous year performed two separate analyses related to CVD [2](https://pubmed.ncbi.nlm.nih.gov/28671591/). This can have advantages over considering all CVD endpoints together as a composite endpoint. The first analysis was for coronary heart disease (CHD), and the second analysis was for stroke. 

![[Pasted image 20221123152509.png]]

Their dose-response curve for CHD finds no upper limit to the risk reductions of cocoa product consumption. From the lowest to highest intakes, cocoa product consumption lowers the risk of CHD by 10%.

![[Pasted image 20221123152513.png]]

Just for fun, we can also look at their findings for stroke. There is also no upper limit to the risk reductions of cocoa product consumption that are observed. From the highest to the lowest intakes, cocoa product consumption reduces stroke risk by 16%.

**Non-Homogenized Dairy**

Starting with the most obvious example, cheese is consistently associated with lower rates of heart disease. A meta-analysis from 2017 generated two dose response curves— one for CVD and another for CHD [3](https://pubmed.ncbi.nlm.nih.gov/27517544/). 

![[Pasted image 20221123152518.png]]

For CVD (left), we see maximal risk reductions around 35g/day. Results become null beyond 80g/day. That's an enormous amount of cheese! For CHD, (right), there is no upper limit to risk reductions at all, despite daily intakes being as high as 120g. That's over a quarter pound of cheese per day, and over 22g of SFA!

Similar to that of cheese, yogurt consumption has been shown to associate with dose-dependent reductions in CVD as well, though not as powerfully [4](https://pubmed.ncbi.nlm.nih.gov/31970674/).

![[Pasted image 20221123152523.png]]

The highest yogurt intakes were close to a pound of yogurt per day. Sweet jumping Jesus that's a lot of yogurt, and likely around 10g of SFA. Yet, yogurt confers a ~13% reduction in CVD risk.

**Key points:**

-   Saturated fat consumption is associated with higher rates of heart disease.
-   There are sources of saturated fat that are inversely associated with heart disease.
-   Higher chocolate, cheese, and yogurt intakes associated with lower rates of CVD.

**References:**

[1] Yongcheng Ren, et al. Chocolate consumption and risk of cardiovascular diseases: a meta-analysis of prospective studies. Heart. 2019 Jan. [https://pubmed.ncbi.nlm.nih.gov/30061161/](https://pubmed.ncbi.nlm.nih.gov/30061161/)

[2] Sheng Yuan, et al. Chocolate Consumption and Risk of Coronary Heart Disease, Stroke, and Diabetes: A Meta-Analysis of Prospective Studies. Nutrients. 2017 Jul. [https://pubmed.ncbi.nlm.nih.gov/28671591/](https://pubmed.ncbi.nlm.nih.gov/28671591/)

[3] Guo-Chong Chen, et al. Cheese consumption and risk of cardiovascular disease: a meta-analysis of prospective studies. Eur J Nutr. 2017 Dec. [https://pubmed.ncbi.nlm.nih.gov/27517544/](https://pubmed.ncbi.nlm.nih.gov/27517544/)

[4] Xiang Gao, et al. Yogurt Intake Reduces All-Cause and Cardiovascular Disease Mortality: A Meta-Analysis of Eight Prospective Cohort Studies. Chin J Integr Med. 2020 Jun. [https://pubmed.ncbi.nlm.nih.gov/31970674/](https://pubmed.ncbi.nlm.nih.gov/31970674/)

#patreon_articles 
#nutrition 
#disease 
#dairy 
#chocolate 
#cheese 
#yogurt 
#cardiovascular_disease 
