There is no doubt that choline is an essential nutrient. Without it, lots of terrible things happen to the human body [1](https://www.ncbi.nlm.nih.gov/pubmed/19874943). But, despite knowing this, choline escapes having a recommended dietary allowance (RDA). Instead, choline has an adequate intake (AI) of 425mg/day for women and 550mg/day for men [2](https://www.ncbi.nlm.nih.gov/books/NBK114308/). An AI is set in place of an RDA when there is insufficient evidence or confidence that an RDA can be estimated. 

So what gives? Why is it so hard to quantify choline requirements in humans? It's because choline requirements can very easily fluctuate wildly on a daily basis. In a lot of ways, your choline requirement is determined by genetics, nutritional status in general, and your dietary choices. Many nutrients can be expected to lower one's choline requirement through the lowering of homocysteine: vitamin B2, vitamin B6, vitamin B9, vitamin B12, betaine, and methionine can all work to lower a person's choline requirements [3](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2714377/)[4](https://www.ncbi.nlm.nih.gov/pubmed/12190367)[5](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3610948/)[6](https://www.ncbi.nlm.nih.gov/pubmed/15916468) Additionally, certain dietary choices increase your choline requirements: fat intake, calorie intake, alcohol intake, and fructose intake can all be expected to increase your requirement for choline due to choline's role in facilitating hepatic triglyceride efflux [7](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4230213/)[8](https://www.ncbi.nlm.nih.gov/pubmed/22338037).

Since everyone's diet and genetics are different, everyone needs a different amount of choline [9](https://www.ncbi.nlm.nih.gov/pubmed/27342765). Someone who is homozygous for the T allele of the C677 MTHFR genotype is going to require an enormous amount of choline, but could perhaps lower this requirement with vitamin B2. Someone with fatty liver will likely require more choline than average. Someone abstaining from alcohol on a low-sugar, low-fat diet with lots of protein is likely to need much less dietary choline than the previous two examples.

What would a diet look like if you wanted to maximally reduce your choline requirement? Based on the nutrients that can be expected to lower the requirement, you'd want to consume plenty of low-sugar fruits and vegetables, low-fat legumes and other whole grains, starchy tubers, lean meats, liver, nutritional yeast, and very little dietary fat. 

But what about eggs? Eggs are a good source of choline, but on some level could be self-defeating since the food is high in fat. It might actually be a better strategy to just maximally reduce your choline needs and get smaller amounts of choline from lots of other sources, such as low fat seafood, certain whole grains, or even something like homemade mushroom soup.

**Key points:** 

-   Choline requirements are difficult to characterize, but we know a minimum requirement exists.
-   Genetics can either decrease or increase choline requirements in certain contexts.
-   Dietary choices that increase B-vitamins, betaine, and protein likely decrease choline requirements.
-   Dietary choices that increase the intakes of fat, calories, sugar, and alcohol likely increase choline requirements.

**References:**

[1] Buchman AL. The addition of choline to parenteral nutrition. Gastroenterology. 2009 Nov. [https://www.ncbi.nlm.nih.gov/pubmed/19874943](https://www.ncbi.nlm.nih.gov/pubmed/19874943) 

[2] The Institute of Medicine. Dietary Reference Intakes for Thiamin, Riboflavin, Niacin, Vitamin B6, Folate, Vitamin B12, Pantothenic Acid, Biotin, and Choline. National Academies Press. 1998. [https://www.ncbi.nlm.nih.gov/books/NBK114308/](https://www.ncbi.nlm.nih.gov/books/NBK114308/) 

[3] Marie A. Caudill, et al. Choline Intake, Plasma Riboflavin, and the Phosphatidylethanolamine N-Methyltransferase G5465A Genotype Predict Plasma Homocysteine in Folate-Deplete Mexican-American Men with the Methylenetetrahydrofolate Reductase 677TT Genotype. J Nutr. 2009 Apr. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2714377/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2714377/) 

[4] Schnyder G, et al. Effect of homocysteine-lowering therapy with folic acid, vitamin B12, and vitamin B6 on clinical outcome after percutaneous coronary intervention: the Swiss Heart study: a randomized controlled trial. JAMA. 2002 Aug. [https://www.ncbi.nlm.nih.gov/pubmed/12190367](https://www.ncbi.nlm.nih.gov/pubmed/12190367) 

[5] Marc P. McRae. Betaine supplementation decreases plasma homocysteine in healthy adult participants: a meta-analysis. J Chiropr Med. 2013 Mar. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3610948/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3610948/) 

[6] Olthof MR, et al. Effect of homocysteine-lowering nutrients on blood lipids: results from four randomised, placebo-controlled studies in healthy humans. PLoS Med. 2005 May. [https://www.ncbi.nlm.nih.gov/pubmed/15916468](https://www.ncbi.nlm.nih.gov/pubmed/15916468) 

[7] Danxia Yu, et al. Higher Dietary Choline Intake Is Associated with Lower Risk of Nonalcoholic Fatty Liver in Normal-Weight Chinese Women. J Nutr. 2014 Dec. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4230213/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4230213/)

[8] Guerrerio AL, et al. Choline intake in a large cohort of patients with nonalcoholic fatty liver disease. Am J Clin Nutr. 2012 Apr. [https://www.ncbi.nlm.nih.gov/pubmed/22338037](https://www.ncbi.nlm.nih.gov/pubmed/22338037)

[9] Ganz AB, et al. Genetic impairments in folate enzymes increase dependence on dietary choline for phosphatidylcholine production at the expense of betaine synthesis. FASEB J. 2016 Oct. [https://www.ncbi.nlm.nih.gov/pubmed/27342765](https://www.ncbi.nlm.nih.gov/pubmed/27342765)

#patreon_articles 
#nutrition 
#disease 
#choline 