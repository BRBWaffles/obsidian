Recently, I extended a debate invitation to Bart Kay via email. That exchange is documented [here](https://www.youtube.com/watch?v=M7vTJ02xxrw). Essentially I gave him an ultimatum. Either he lifts the [unreasonable stipulations](https://youtu.be/rYcy9YhS8NU?t=609) that he had previously placed upon [Avi Bitterman](https://twitter.com/AviBittMD) for their debate, or him and I will not debate. He claimed that my stipulations were unreasonable because they were not related to the debate. However, this is also true of his stipulations against Avi.

Basically, either he gives me a justification that renders his stipulations valid in a way that renders mine invalid, or his position entails a contradiction. If both of our stipulations are valid, then he can't say that I'm dodging him without it also being entailed that he is dodging Avi. If both of our stipulations are invalid, then he has no excuse for dodging a debate with Avi. He's effectively trapped until he provides the argument.

I don't believe this debate will actually happen, because Bart has previously shown himself to be unable to engage with [basic logical concepts](https://www.youtube.com/watch?v=Cknpks3QlBk) like consistency. For this reason, I'm releasing my debate notes to my patrons. Here is the line of questioning that I was going to run on Bart's debate proposition. Ultimately my game plan was to pin him on empirics, which would have been very straight forward. Enjoy!

**Bart's Debate Proposition:**

> _"100% Carnivore diet is the appropriate and best health choice for all people."_

-   It's unclear if this qualifies as a proposition.
-   What does "appropriate" mean? In relation to what?
-   What does "best" mean?
-   What does "health" mean? Some endpoint?
-   What constitutes a 100% carnivore diet? Does 100% horse hooves count?

**Bart's Clarified Debate Proposition:**

> _"Z is X and Y W choice for all people."_

-   It's unclear what sort of claim this proposition is making.
-   Is this a scientific claim?
-   If not a scientific claim, is this claim simply a belief?
-   If a belief, agree with the proposition, laugh, and leave.
-   If a scientific claim, proceed to the line of questioning.

**Line of Questioning:**

-   What's the evidence?
-   What is the argument that is this evidence is more expected on the proposition than the negation of the proposition? (An argument is required, because maybe the "evidence" is less or equally expected on the proposition (which would not be evidence for the proposition)).
-   If he does provide an argument, examine the premises carefully.
-   If a premise is unconvincing, ask for the argument for the premise.
-   Repeat if necessary or until you become convinced or until Bart rage-quits.

#patreon_articles 
#carnivore 