I recently had the misfortune of skimming through Paul Saladino's book the Carnivore Code. God, what a smoking pile of horseshit that book was. That being said, one thing it did do well was give us a succinct rundown of typical fallacious rhetoric surrounding dietary oxalate.

Essentially the argument revolves around fringe cases of dietary oxalate inducing various pathological states in human beings. He then uses these case reports to buttress an argument against all plant foods, in support of his particular flavour of the carnivore diet. However, if we pick through his citations a completely different picture emerges.

Firstly, he makes the claim that rhubarb has killed people through oxalate poisoning, which is true [1](https://jamanetwork.com/journals/jama/article-abstract/222117)[2](https://pubmed.ncbi.nlm.nih.gov/1288268/). However, these cases specifically involve consumption of rhubarb leaves and/or roots, not stalks. Stalks are also relatively high-oxalate, but do not appear to be associated with toxicity.

It is widely accepted that the leaves of the rhubarb plant are inedible. This is like me saying that puffer fish flesh is dangerous, even when properly prepared. When in reality, the danger is isolated to only a couple different tissues within the fish that can be removed with proper preparation.

His next reference involves a chain-smoking, alcoholic, insulin-dependent diabetic who died from eating sorrel soup [3](https://pubmed.ncbi.nlm.nih.gov/2574796/). He technically didn't die of oxalate poisoning per se, but rather he died due to diabetic acidosis that resulted from oxalate producing acute hypocalcemia. In a normal person, this imbalance is corrected by a subsequent increase in PTH and a restoration of normal blood calcium. However, because he was diabetic, he was uniquely vulnerable to acidosis. So he died.

Additionally, the soup he was consuming had 6-8g of oxalate. Whereas a typical spinach salad, or rhubarb stalk, typically has under 900mg. Upon autopsy, it was discovered that he had chronic kidney disease, liver disease, and oxalate crystals in his kidneys. These are diabetic complications that were exacerbated by eating an almost unheard of amount of oxalate. Not particularly robust evidence.

Similar to his last reference, he then cites another individual with pre-existing morbidities who developed end-stage renal disease after consuming nothing but vegetable smoothies for over a week [4](https://pubmed.ncbi.nlm.nih.gov/29203127/). This woman was consuming nothing but these smoothies, and her average daily intake of oxalate was 1.3g. This is 940% more oxalate than the national average of the United States.

This carnivore quack's next piece of groundbreaking evidence is a case report of a middle-aged, diabetic, hypertensive, alcoholic male who developed renal dysfunction after chronically consuming a quarter pound of peanuts per day [5](https://pubmed.ncbi.nlm.nih.gov/26877960/). Even the authors of the case report seemed skeptical that peanuts alone were responsible for his decline.

He then goes on to cite a case report of three prepubescent children who all began pissing blood after consuming up to a litre of almond milk per day for two years straight [6](https://pubmed.ncbi.nlm.nih.gov/26382627/). In addition to this, one of the children was also consuming moderate amounts of other almond-based products. 

Virtually all of these case reports involve high oxalate foods being processed in one way or another such that the oxalate content of the final product is maximized. The product is then typically dosed at extremely high amounts in individuals with pre-existing illnesses. This isn't evidence in support of a carnivore diet. This is evidence against consuming abnormally high levels of oxalate when you have pre-existing health conditions. This doesn't mean that spinach salads are dangerous. It means that living off of spinach juice is probably a bad idea.

Pretty much the only caveat that I could find was star fruit [7](https://pubmed.ncbi.nlm.nih.gov/16169255/). One star fruit can contain up to 10g of oxalate, and is pretty much the only whole food associated with oxalate-induced complications. However, again, these complications seem to be isolated to individuals with pre-existing health problems.

So, no. Generally speaking, I don't find this to be persuasive evidence against the inclusion of plant foods. Plant foods don't have to be juiced and eaten to the exclusion of everything else in order to be enjoyed, so I don't personally see how his conclusion logically follows from his premises.

**Key points:**

-   There are case reports of oxalate-induced illness and death in the literature.
-   These case reports almost universally involve individuals with pre-existing health conditions.
-   These case reports also almost universally involve juicing, blending, or otherwise processing high oxalate foods.
-   Star fruit is the only whole food associated with oxalate-induced illness, but only really in people with chronic kidney disease.
-   High oxalate mono-diets are fucking bad for you.

**References:**

[1] Henry Leffmann, M.D. Death from rhubarb leaves due to oxalic acid poisoning. JAMA. Sept. 1919. [https://jamanetwork.com/journals/jama/article-abstract/222117](https://jamanetwork.com/journals/jama/article-abstract/222117)

[2] P Sanz and R Reig. Clinical and pathological findings in fatal plant oxalosis. A review. Am J Forensic Med Pathol. 1992 Dec. [https://pubmed.ncbi.nlm.nih.gov/1288268/](https://pubmed.ncbi.nlm.nih.gov/1288268/) 

[3] M Farré, et al. Fatal oxalic acid poisoning from sorrel soup. Lancet. 1989 Dec 23.  [https://pubmed.ncbi.nlm.nih.gov/2574796/](https://pubmed.ncbi.nlm.nih.gov/2574796/) 

[4] Swetha Makkapati, et al. "Green Smoothie Cleanse" Causing Acute Oxalate Nephropathy. Am J Kidney Dis. 2018 Feb. [https://pubmed.ncbi.nlm.nih.gov/29203127/](https://pubmed.ncbi.nlm.nih.gov/29203127/) 

[5] Hyeoncheol Park, et al. Peanut-induced acute oxalate nephropathy with acute kidney injury. Kidney Res Clin Pract. 2014 Jun. [https://pubmed.ncbi.nlm.nih.gov/26877960/](https://pubmed.ncbi.nlm.nih.gov/26877960/) 

[6] Demetrius Ellis and Jessica Lieb. Hyperoxaluria and Genitourinary Disorders in Children Ingesting Almond Milk Products. J Pediatr. 2015 Nov. [https://pubmed.ncbi.nlm.nih.gov/26382627/](https://pubmed.ncbi.nlm.nih.gov/26382627/) 

[7] Meng-Han Tsai, et al. Status epilepticus induced by star fruit intoxication in patients with chronic renal disease. Seizure. 2005 Oct. [https://pubmed.ncbi.nlm.nih.gov/16169255/](https://pubmed.ncbi.nlm.nih.gov/16169255/) 

#patreon_articles 
#nutrition 
#antinutrients 
#oxalate 
#clownery 
