A commonly misunderstood concept in the low carb world is the balance between muscle anabolism and muscle catabolism. The ketogenic diet (KD) has the capacity to perturb this balance negatively, though it is not guaranteed. This is because different macronutrients affect both lean body mass (LBM) and fat mass (FM) differently when considered in isolation. Let me explain.

-   **Protein** is both anabolic and sparing to LBM, but catabolic to FM.
-   **Carbs** are sparing of LBM and FM, but anabolic to neither.
-   **Fat** is catabolic to LBM, but anabolic to FM.

When we enter into nutritional ketosis, we deplete liver glycogen and must synthesize glucose by breaking down protein and liberating amino acids (AA). This can be protein in the diet or protein on our body. Eventually we can use other substrates like glycerol and aldehydes to synthesize glucose, but the contribution of AAs to gluconeogenesis (GNG) will always be substantial. This is why we sometimes hear low carb advocates claim that carbs are "non-essential". This is because when we don't eat them, we synthesize them.

However, we cannot rely entirely on dietary protein to satisfy our body's entire demand for glucose. For example, if our acute need for glucose exceeds our capacity to digest, absorb, and metabolize AAs from our diet to glucose, we will be pulling those amino acids from our skeletal muscle instead [1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3636601/). 

Even if we could satisfy 100% of our glucose requirements with dietary AAs in the fed state, we still have to sleep at some point. During sleep, we're fasting by definition and relying on endogenous AAs to synthesize glucose. When protein and calories are matched between a KD and a non-ketogenic diet (nKD), a nKD will typically be more sparing of LBM [2](https://www.ncbi.nlm.nih.gov/pubmed/30335720)[3](https://www.ncbi.nlm.nih.gov/pubmed/22283635). 

All this being said, it is certainly possible to gain muscle on a KD, despite the catabolic stimulus being very strong. It is likely that we merely need to provide adequate protein and a sufficiently robust anabolic stimulus, like resistance training [4](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6724590/). It is likely that protein needs are going to be higher on a KD in order to achieve the same balance between anabolism and catabolism that can be achieved on a nKD. A KD will always cost us anabolic potential even if it does result in net _increases_ in LBM. Meaning that even if we made gains, we probably could have made more gains, or we could have made the same gains with less effort, on a nKD. 

Ultimately, either we're spending dietary AAs on glucose instead of spending them to build muscle, or we're breaking down already built muscle by liberating AAs to spend on glucose. Glucose isn't free. Either way we lose anabolic potential.

**Key points:**

-   Muscle hypertrophy occurs when anabolism outweighs catabolism. 
-   We have an obligate need to catabolize lean tissue while in ketosis.
-   Ketogenic diets unavoidably cost us anabolic potential by default.
-   Amino acids used to make glucose cannot be used to build muscle.
-   Typical gains are still achievable in ketosis, but require extra protein.

**References:** 

[1] Claire Fromentin, et al. Dietary Proteins Contribute Little to Glucose Production, Even Under Optimal Gluconeogenic Conditions in Healthy Humans. Diabetes. May 2013.  [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3636601/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3636601/)

[2] Greene, DA, et al. A Low-Carbohydrate Ketogenic Diet Reduces Body Mass Without Compromising Performance in Powerlifting and Olympic Weightlifting Athletes. J Strength Cond Res. December 2018. [https://www.ncbi.nlm.nih.gov/pubmed/30335720](https://www.ncbi.nlm.nih.gov/pubmed/30335720)

[3] Wood, RJ, et al. Preservation of fat-free mass after two distinct weight loss diets with and without progressive resistance exercise. Metab Syndr Relat Disord. June 2012. [https://www.ncbi.nlm.nih.gov/pubmed/22283635](https://www.ncbi.nlm.nih.gov/pubmed/22283635)

[4] Antonio Paoli, et al. Ketogenic Diet and Skeletal Muscle Hypertrophy: A Frenemy Relationship? J Hum Kinet. August 2019. [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6724590/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6724590/)

#patreon_articles 
#nutrition 
#disease 
#keto 
#hypertrophy 
#exercise 
#protein 
