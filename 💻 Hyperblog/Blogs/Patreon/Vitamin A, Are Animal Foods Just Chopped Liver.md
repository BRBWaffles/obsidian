Is it necessary to consume animal foods to meet sufficiency of vitamin A? Some people in the nutrition space seem to think so. Proponents of so-called "ancestral diets" such as Chris Masterjohn and Chris Kresser have been squawking about this idea like a couple of demented parrots for years, but is there any merit to it?

The argument essentially goes like this:

There is genetic variation in the activity of the BCO1 enzyme, which is responsible for converting carotenoids into to active form of vitamin A, retinol. Therefore, plant foods are an unreliable source of retinol in those of us with these genetic variants, because they won't be able to convert enough.

Cool story. It definitely seems as though there could be physiological plausibility for this hypothesis. But, let's see how it plays out in meatspace. No pun intended.

Right off the bat, we can easily locate research wherein retinol status is ascertained in people with these genetic variants [1](https://pubmed.ncbi.nlm.nih.gov/19103647/). It seems as though even the people with the worst impairments maintain adequate retinol status, despite only getting an average of a measly 133mcg/day of retinol.

The only thing that appears to change is that the ratio of beta-carotene to retinol, which is increased based on the exact type of genetic variation in the conversion rate. But it doesn't appear as though much else actually changes. In fact, the entire variance in the population sample in this study had fasting plasma retinol within the reference range. The authors themselves commented that all volunteers had adequate serum vitamin A concentrations.

Additionally, a rather recent study published in 2020 found no differences in retinol status between BCO1 genotypes in a population consuming extremely low amounts of preformed retinol [14](https://pubmed.ncbi.nlm.nih.gov/32560166/). Again, virtually all study participants were within the reference range for plasma vitamin A, and plasma retinol concentrations did not vary between genotypes. In fact, vitamin A deficiency was correlated similarly with lower plasma carotenoid concentrations and plasma retinol concentrations.

So, it does not actually seem to be the case that having these genetic variants meaningfully affects retinol status. There is a more parsimonious way of reconciling these data, though. It just means the rate of conversion is probably slower. The shape of the conversion curve is likely just longer and flatter in so-called "impaired" genotypes, but it's very likely the case that and the area under that conversion curve is likely the same.

It's akin to the difference between low glycaemic carbohydrates and high glycaemic carbohydrates when matched for calories. The rates of absorption between the two yield differently shaped curves. The low glycaemic curve is long in duration and low in concentration in the postprandial window, while the high glycaemic curve is short in duration and high in concentration in the postprandial window. However, the area under both of these curves is identical.

This is likely analogous to the conversion rates between so-called "imapired" genotypes and wildtype genotypes. Those with the so-called "impaired genotypes" are simply converting for longer, but much less at a time, whereas those with the wildtype genotypes are converting for a shorter period of time, but much more at a time. But the total amount converted between the two genotypes is likely close to equivalent.

But, we can take this a step further and actually see what happens when we try to correct vitamin A deficiency using dietary carotenoids in human subjects [2](https://pubmed.ncbi.nlm.nih.gov/15883432/)[3](https://pubmed.ncbi.nlm.nih.gov/17413103/)[4](https://pubmed.ncbi.nlm.nih.gov/9808223/)[5](https://pubmed.ncbi.nlm.nih.gov/10584052/)[6](https://pubmed.ncbi.nlm.nih.gov/15321812/)[7](https://pubmed.ncbi.nlm.nih.gov/16210712/). On the whole, eating foods rich in carotenoids reliably improves and/or normalizes vitamin A status. Even foods that have been genetically engineered to have higher levels of carotenoids reliably improve vitamin A status in humans [8](https://pubmed.ncbi.nlm.nih.gov/19369372/).

As a side note, the only cases of vitamin A toxicity (hypervitaminosis A) from whole foods that I could find in the literature involved the consumption of preformed retinol from liver [9](https://pubmed.ncbi.nlm.nih.gov/25850632/)[10](https://pubmed.ncbi.nlm.nih.gov/21902932/)[11](https://pubmed.ncbi.nlm.nih.gov/10424294/)[12](https://pubmed.ncbi.nlm.nih.gov/31089689/)[13](https://pubmed.ncbi.nlm.nih.gov/3655980/). In one case, a child died from consuming chicken liver pate sandwiches. I could find no case reports of vitamin A toxicity related to carotenoids.

**Key points:**

-   Genetic impairments in the conversion of carotenoids to retinol do not seem to impact retinol status or make deficiency more likely.
-   Dietary carotenoids from whole plant foods can easily maintain adequate retinol status.
-   The only case reports of vitamin A toxicity from whole foods are attributable to liver consumption.

**References:**

[1] W C Leung, et al. Two Common Single Nucleotide Polymorphisms in the Gene Encoding Beta-Carotene 15,15'-monoxygenase Alter Beta-Carotene Metabolism in Female Volunteers. FASEB J. 2009 Apr. [https://pubmed.ncbi.nlm.nih.gov/19103647/](https://pubmed.ncbi.nlm.nih.gov/19103647/) 

[2] Paul J van Jaarsveld, et al. Beta-carotene-rich Orange-Fleshed Sweet Potato Improves the Vitamin A Status of Primary School Children Assessed With the Modified-Relative-Dose-Response Test. Am J Clin Nutr. 2005 May. [https://pubmed.ncbi.nlm.nih.gov/15883432/](https://pubmed.ncbi.nlm.nih.gov/15883432/) 

[3] Judy D Ribaya-Mercado, et al. Carotene-rich Plant Foods Ingested With Minimal Dietary Fat Enhance the Total-Body Vitamin A Pool Size in Filipino Schoolchildren as Assessed by Stable-Isotope-Dilution Methodology. Am J Clin Nutr. 2007 Apr. [https://pubmed.ncbi.nlm.nih.gov/17413103/](https://pubmed.ncbi.nlm.nih.gov/17413103/) 

[4] S de Pee, et al. Orange Fruit Is More Effective Than Are Dark-Green, Leafy Vegetables in Increasing Serum Concentrations of Retinol and Beta-Carotene in Schoolchildren in Indonesia. Am J Clin Nutr. 1998 Nov. [https://pubmed.ncbi.nlm.nih.gov/9808223/](https://pubmed.ncbi.nlm.nih.gov/9808223/) 

[5] G Tang, et al. Green and Yellow Vegetables Can Maintain Body Stores of Vitamin A in Chinese Children. Am J Clin Nutr. 1999 Dec. [https://pubmed.ncbi.nlm.nih.gov/10584052/](https://pubmed.ncbi.nlm.nih.gov/10584052/) 

[6] Marjorie J Haskell, et al. Daily Consumption of Indian Spinach (Basella Alba) or Sweet Potatoes Has a Positive Effect on Total-Body Vitamin A Stores in Bangladeshi Men. Am J Clin Nutr. 2004 Sep. [https://pubmed.ncbi.nlm.nih.gov/15321812/](https://pubmed.ncbi.nlm.nih.gov/15321812/) 

[7] Guangwen Tang, et al. Spinach or Carrots Can Supply Significant Amounts of Vitamin A as Assessed by Feeding With Intrinsically Deuterated Vegetables. Am J Clin Nutr. 2005 Oct. [https://pubmed.ncbi.nlm.nih.gov/16210712/](https://pubmed.ncbi.nlm.nih.gov/16210712/) 

[8] Guangwen Tang, et al. Golden Rice Is an Effective Source of Vitamin A. Am J Clin Nutr. 2009 Jun. [https://pubmed.ncbi.nlm.nih.gov/19369372/](https://pubmed.ncbi.nlm.nih.gov/19369372/) 

[9] Yosuke Homma, et al. A Case Report of Acute Vitamin A Intoxication Due to Ocean Perch Liver Ingestion. J Emerg Med. 2015 Jul. [https://pubmed.ncbi.nlm.nih.gov/25850632/](https://pubmed.ncbi.nlm.nih.gov/25850632/) 

[10] E Dewailly, et al. Vitamin A Intoxication From Reef Fish Liver Consumption in Bermuda. J Food Prot. 2011 Sep. [https://pubmed.ncbi.nlm.nih.gov/21902932/](https://pubmed.ncbi.nlm.nih.gov/21902932/) 

[11] K Nagai, et al. Vitamin A Toxicity Secondary to Excessive Intake of Yellow-Green Vegetables, Liver and Laver. J Hepatol. 1999 Jul. [https://pubmed.ncbi.nlm.nih.gov/10424294/](https://pubmed.ncbi.nlm.nih.gov/10424294/) 

[12] Martha E van Stuijvenberg, et al. South African Preschool Children Habitually Consuming Sheep Liver and Exposed to Vitamin A Supplementation and Fortification Have Hypervitaminotic A Liver Stores: A Cohort Study. Am J Clin Nutr. 2019 Jul. [https://pubmed.ncbi.nlm.nih.gov/31089689/](https://pubmed.ncbi.nlm.nih.gov/31089689/) 

[13] T O Carpenter, et al. Severe Hypervitaminosis A in Siblings: Evidence of Variable Tolerance to Retinol Intake. J Pediatr. 1987 Oct. [https://pubmed.ncbi.nlm.nih.gov/3655980/](https://pubmed.ncbi.nlm.nih.gov/3655980/)

[14] Sophie Graßmann, et al. SNP rs6564851 in the BCO1 Gene Is Associated with Varying Provitamin a Plasma Concentrations but Not with Retinol Concentrations among Adolescents from Rural Ghana. Nutrients. 2020 Jun [https://pubmed.ncbi.nlm.nih.gov/32560166/](https://pubmed.ncbi.nlm.nih.gov/32560166/)

#patreon_articles 
#nutrition 
#disease 
#vitamin_a 
#nutrients 
#nutrient_deficiency 
#animal_foods 
#clownery 