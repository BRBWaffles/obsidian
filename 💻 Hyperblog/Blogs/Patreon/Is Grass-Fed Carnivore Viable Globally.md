There have been a handful of papers published investigating the environmental effects of holistic management (HM). The most prominent was a 2020 paper by Rowntree, et al., which suggested that within the context of a HM system, cattle farming would seem to be carbon negative [1](https://www.frontiersin.org/articles/10.3389/fsufs.2020.544984/full). However, there are some issues with this paper. Let's go through some of them now.

Firstly, the paper reads as though we're measuring the effect of cattle grazing on soil carbon sequestration (SOC) on pasture land over a twenty year period. The illustrate this in **Figure 1**:

![[Pasted image 20221123155140.png]]

However, what they don't explicitly state in the paper is that these data are cross-sectional. This needs to be inferred from reading their methodology. In actuality, each datapoint within **Figure 1** represents a completely different area of land, if not completely different farms.

The number of years represents the number of years each area of land had been grazed. We're not actually investigating temporal changes in SOC over a single area of land. Even the language within the paper gives the impression that we're investigating changes in SOC over time:

> _"Although there was very little difference in soil C stock between year 0 and year 1, we elected to include this in the model as a true year 0 site. We also experienced dry, difficult sampling conditions in year 13, enabling collection of two intact soil cores."_

In my view, this language is deceptive and can easily give the reader a false impression about the methodologies used.

Secondly, I'm fairly confident that Rowntree, et al. only used 20 years worth of pastured land in their model was because current estimates of SOC via grazing show diminishing returns beyond that point [2](https://www.sciencedirect.com/science/article/pii/S0301479714002588). Of course this also means that there is pretty shallow cap to the SOC potential of HM.

![[Pasted image 20221123155145.png]]

Thirdly, Rowntree, et al. erroneously attributed all of the SOC (equal to -4.4 kg CO₂-e kg CW⁻¹ per year) to cattle, despite the fact that the farming operations also contained poultry, pigs, sheep, goats, and even rabbits. These other animals actually required exogenous feed, meaning that the feed originated from outside of the farm itself. 

Overall, poultry actually represented the largest single proportion of production on the farms. How they think they are able to attribute the SOC purely to cattle is beyond me. This actually means the net negative SOC figure of -4.4 kg CO₂-e kg CW⁻¹ is completely invented, based on ridiculous assumptions that nobody should take seriously. In their aggregated estimates, WOP was actually carbon positive in net.

Lastly, each area of farmland started as a previously degraded area of land with virtually no vegetation. In order to transition the degraded land to grazing land, not only were the cattle fed exogenous hay for the first three years, but the first three years also involved annual grass seeding via aerial dispersion with planes. 

This is highly problematic, as there is no control. We don't know what the land would look like if it were only seeded and not converted to grazing land. It could actually be the case that the cattle on that land may be detracting from the quality and the SOC potential. We can't know, and this is yet another reason to question the assumption that the cattle on the farm were carbon negative.

Additionally, something that is often overlooked when grazing systems are argued for is the opportunity cost of pasture land. While it may be true that grazing agricultural methods are closer to being carbon-neutral than conventional animal agricultural methods, it is also true that at least half of current pasture land could be made significantly more carbon negative if reforested [3](https://www.nature.com/articles/s41893-020-00603-4).

![[Pasted image 20221123155149.png]]

In this model, we're looking at carbon sequestration along gradations of animal agriculture, from business-as-usual to completely vegan. As you can see, as we substitute forests for grazing on pasture, there is a stepwise increase in carbon sequestration potential. Not only that, but even if we attempted to scale grazing agriculture globally, it would barely even be worth it.

In a comprehensive report entitled "Grazed and Confused?", the global per-person yield of animal protein from grazing agriculture was estimated in a number of scenarios [4](https://www.oxfordmartin.ox.ac.uk/publications/grazed-and-confused/). In the first scenario, they modeled a situation wherein all available grasslands were repurposed for grazing. This model yielded around 7-18g/day per person of animal protein. In the second scenario, they modeled a situation that was similar to the first, but livestock diets could be supplemented with plant agriculture waste. This model yielded 11-32g/day of animal protein per person, globally. However, this particular estimate is not relevant to a carnivore world.

They also modeled a third scenario that assumed all pasturable land on Earth would be repurposed for grazing. This scenario is not very relevant, because it was altogether implausible that such a thing can be done. But this model yielded around 80g/day of animal protein per person.

If people require 2000 kcal/day on average, and pasture-raised beef is an average of 161 kcal per 100g, this means we would need a minimum of 3-35 Earths worth of space to feed the world on pasture-raised beef. In the context of HM, the numbers must be multiplied by 1.5 in order to account for the additional space required over the continuous grazing methods assumed in the calculation. This gets us to 4.5-52.5 Earths.  

![[Pasted image 20221123155200.png]]

Even if we were not to go carnivore, the highest estimate from within the plausible scenarios would still leave us about 42% short of meeting current global animal protein intakes of 55g/day. This would require about 1.72 Earths worth of space, which is not a tenable solution to animal food security, or food scarcity in general. There are a few solutions, though.

The first option would be to savagely curtail the population size through anti-natalist legislation, but I don't think anyone would consider that to be terribly ethical. The second option would be to try to increase the amount of pasturable land off-world. This could be achieved by either terraforming Mars or constructing either orbital or stellar megastructures, such as O'Neill cylinders or a Dyson sphere. None of these options are practical, though.

However, plant-exclusive or near-plant-exclusive agricultural systems has the capacity to reduce our agricultural footprint down to a range that cope with long-term population growth [5](https://online.ucpress.edu/elementa/article/doi/10.12952/journal.elementa.000116/112904/Carrying-capacity-of-U-S-agricultural-land-Ten). 

![[1-86.png]]

In conclusion, it does not appear as though grazing agricultural systems such as HM can adequately provide us with viable solutions for global food security that would also insulate us from the typical environmental pitfalls of animal agriculture. Grazing agricultural systems consume an enormous amount of land, and likely do not scale to a point where we could avoid plant agriculture altogether. As global food energy demands cannot currently be met with a carnivore-based agricultural system.

**Key points:**

-   Grazing animal agricultural systems do not scale such that global food energy demands could be met on a carnivore diet.
-   The current literature supporting regenerative animal agricultural methods such as holistic management are riddled with errors and dishonesty.
-   Extensive reforestation of current pasture land is likely the best long-term strategy for global carbon sequestration.

**References:**

[1] [https://www.frontiersin.org/articles/10.3389/fsufs.2020.544984/full](https://www.frontiersin.org/articles/10.3389/fsufs.2020.544984/full)

[2] [https://www.sciencedirect.com/science/article/pii/S0301479714002588](https://www.sciencedirect.com/science/article/pii/S0301479714002588) 

[3] [https://www.nature.com/articles/s41893-020-00603-4](https://www.nature.com/articles/s41893-020-00603-4) 

[4] [https://www.oxfordmartin.ox.ac.uk/publications/grazed-and-confused/](https://www.oxfordmartin.ox.ac.uk/publications/grazed-and-confused/) 

[5] [https://online.ucpress.edu/elementa/article/doi/10.12952/journal.elementa.000116/112904/Carrying-capacity-of-U-S-agricultural-land-Ten](https://online.ucpress.edu/elementa/article/doi/10.12952/journal.elementa.000116/112904/Carrying-capacity-of-U-S-agricultural-land-Ten)

#patreon_articles 
#environment 
#beef 
#carnivore 
