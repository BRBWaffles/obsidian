Recently, Chris Masterjon hosted a [live Q&A](https://www.youtube.com/watch?v=WRmUzYD8l7Q) about nutrition and health. A viewer asked Chris if polyunsaturated fats (PUFA) were toxic and to be avoided. Chris' answers were mostly reasonable and didn't quite devolve into a blatant anti-PUFA narrative until about 25 minutes into the video.

Chris spends most of the video caveating his empirical claims at the end. To his credit at least he is prefacing his scaremongering with the caveat that he doesn't believe PUFA-avoidance is rational if it compromises the nutritional adequacy of the diet. Which is fine. I don't think food avoidance makes much sense in that case myself as well.

His empirical claims begin around 25 minutes into the video, so let's start there.

[**24:47**](https://youtu.be/WRmUzYD8l7Q?t=1489) 

**Claim:** There is some indication that if the population is old enough, substituting PUFAs for saturated fat (SFA) can increase the risk of cancer.

**Fact:** I have touched on this topic before [here](https://www.patreon.com/posts/do-vegetable-48046151), and discussed in detail why it is probably wrong. Essentially, the only randomized controlled trial (RCT) that actually observed a potential association between PUFA and cancer risk had a couple issues [1](https://pubmed.ncbi.nlm.nih.gov/4100347/). Firstly, the effect itself was unadjusted and non-significant. Secondly, the effect nullified after an adjustment for adherence.

A recent meta-analysis investigating the relationship between PUFA and cancer in RCTs found the same non-significant increase in risk for some cancers [2](https://pubmed.ncbi.nlm.nih.gov/32114592/). However their risk-of-bias assessment revealed that most of the included trials either had an unknown or high risk of bias due to poor compliance and attention.

Cochrane also did their own meta-analysis investigating this question in one of their secondary endpoint analyses [3](https://pubmed.ncbi.nlm.nih.gov/32428300/). Their findings were also null, as we might suspect.

![[1-60.png]]

There is very limited evidence from certain observational studies that PUFA intake may increase the risk of certain types of skin cancer [4](https://pubmed.ncbi.nlm.nih.gov/31298947/). However, higher PUFA intakes seem to lower cancer risk, or have no effect, overall [5](https://pubmed.ncbi.nlm.nih.gov/32020162/)[6](https://pubmed.ncbi.nlm.nih.gov/30545042/).

[**25:00**](https://youtu.be/WRmUzYD8l7Q?t=1503) 

**Claim:** You cannot make a strong case that increasing PUFAs improves heart disease outcomes.

**Fact:** This is an extremely bold claim, because it is one of the most robust and well-studied effects of diet on chronic disease risk that we have in nutrition science. I wrote about this in detail [here](https://thenutrivore.blogspot.com/2020/05/saturated-fat-cutting-through-noise.html).

Again, referencing Lee Hooper's Cochrane review on SFA from 2020, we see that the extend to which SFA lowers cholesterol will dictate the degree of heart disease risk reduction [3](https://pubmed.ncbi.nlm.nih.gov/32428300/).

![[1-62.png]]

This relationship was also discovered in a subgroup meta-regression analysis conducted in 2016 by Silverman et al [7](https://pubmed.ncbi.nlm.nih.gov/27673306/). Four diet trials were included in this analysis. These trials were investigating the relationship between dietary fat and heart disease when PUFA is substituted for SFA. Again, the degree of cholesterol-lowering dictates reductions in disease risk.

![[1-59.png]]

[**25:25**](https://youtu.be/WRmUzYD8l7Q?t=1525)

**Claim:** While reducing serum cholesterol may lower heart disease risk, increasing PUFA increases the tendency for LDL to oxidize, which also contributes to heart disease.

**Fact:** I have written about this point [here](https://www.patreon.com/posts/does-saturated-35112489). While it is true that oxidized LDL are implicated in the pathogenesis of heart disease, it has never been persuasively shown that increasing dietary PUFA increases heart disease risk. It is also true, as references in my linked article, that eating SFA to the exclusion of PUFA also predisposes LDL to oxidation. MUFA does not seem predispose LDL to oxidation to the same extent as SFA or PUFA, but MUFA is actually not as effective as PUFA in lowering CVD risk [8](https://pubmed.ncbi.nlm.nih.gov/26429077/).

![[1-61.png]]

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/4100347/](https://pubmed.ncbi.nlm.nih.gov/4100347/)

[2] [https://pubmed.ncbi.nlm.nih.gov/32114592/](https://pubmed.ncbi.nlm.nih.gov/32114592/)

[3] [https://pubmed.ncbi.nlm.nih.gov/32428300/](https://pubmed.ncbi.nlm.nih.gov/32428300/)

[4] [https://pubmed.ncbi.nlm.nih.gov/31298947/](https://pubmed.ncbi.nlm.nih.gov/31298947/)

[5] [https://pubmed.ncbi.nlm.nih.gov/32020162/](https://pubmed.ncbi.nlm.nih.gov/32020162/)

[6] [https://pubmed.ncbi.nlm.nih.gov/30545042/](https://pubmed.ncbi.nlm.nih.gov/30545042/)

[7] [https://pubmed.ncbi.nlm.nih.gov/27673306/](https://pubmed.ncbi.nlm.nih.gov/27673306/)

#patreon_articles 
#nutrition 
#polyunsaturated_fat 
#clownery 
#cardiovascular_disease 
#LDL 