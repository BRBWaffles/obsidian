It's extremely common to hear (especially in the low-carb/ketogenic dieting sphere) that dietary cholesterol (DC) has next to no effect on serum cholesterol or lipoproteins. But is this true? Well, it may depend on who you're measuring.

Let's look at this graph.

![[Pasted image 20221123152033.png]]

As we can see, DC intakes that go from average, around 300mg/day, to 500mg/day would have a negligible effect on total cholesterol (TC) levels [1](https://www.ncbi.nlm.nih.gov/pubmed/1534437). So of course when we study the average population, we see confusing effects of DC on cardiovascular disease (CVD) endpoints [2](https://www.ncbi.nlm.nih.gov/pubmed/26109578). The effect of DC on CVD can easily be lost in the noise.

An astute paleo-dieter might point out that this is measuring TC, and that much of the effect may be represented in HDL cholesterol (HDL-C). Which would ostensibly be a good thing, according to some. So, let's investigate this.

![[Pasted image 20221123152038.png]]

This graph represents changes in LDL-C as a function of increasing DC [3](https://www.ncbi.nlm.nih.gov/pubmed/30596814). As you can see, going from the average of 300mg/day to any quantity of DC above it yields no significant changes in LDL-C. However, going from 0mg/day to 300mg/day increases LDL-C by almost 10mg/dL. This isn't nothing. It's a meaningful change, and the two graphs cohere well.

But, just to hammer this home, let's check out HDL-C. 

![[Pasted image 20221123152041.png]]

DC does, as my dad would say, "sweet piss-all" to HDL-C. Which effectively means that for the average person, LDL-C is soaking the brunt of DC's impact. But let's not all go vegan and drop our DC intakes to zero just yet. There's definitely more to this story.

Numerous studies have demonstrated that it's not so much the cholesterol in LDL that is the problem, it's the LDL particles (LDLp) themselves [4](https://www.ncbi.nlm.nih.gov/pubmed/30694319)[5](https://www.ncbi.nlm.nih.gov/pubmed/21392724). When adjusted for the number of LDLp, the association between LDL-C and CVD becomes virtually null. Personally, I take this to mean that if dietary cholesterol isn't raising LDL particles, it probably isn't increases CVD risk.

**Key points:**

-   Studies finding null effects of DC on CVD may be confounded by baseline DC intakes.
-   DC increases LDL cholesterol on average.
-   DC doesn't increase HDL cholesterol on average.
-   LDLp causes CVD, not LDL-C, so DC may not be a big deal after all.

**References:**

[1] Hopkins PN. Effects of dietary cholesterol on serum cholesterol: a meta-analysis and review. Am J Clin Nutr. 1992 Jun. [https://www.ncbi.nlm.nih.gov/pubmed/1534437](https://www.ncbi.nlm.nih.gov/pubmed/1534437)

[2] Berger S, et al. Dietary cholesterol and cardiovascular disease: a systematic review and meta-analysis. Am J Clin Nutr. 2015 Aug. [https://www.ncbi.nlm.nih.gov/pubmed/26109578](https://www.ncbi.nlm.nih.gov/pubmed/26109578)

[3] Vincent MJ, et al. Meta-regression analysis of the effects of dietary cholesterol intake on LDL and HDL cholesterol. Am J Clin Nutr. 2019 Jan. [https://www.ncbi.nlm.nih.gov/pubmed/30596814](https://www.ncbi.nlm.nih.gov/pubmed/30596814) 

[4] Ference BA, et al. Association of Triglyceride-Lowering LPL Variants and LDL-C-Lowering LDLR Variants With Risk of Coronary Heart Disease. JAMA. 2019 Jan. [https://www.ncbi.nlm.nih.gov/pubmed/30694319](https://www.ncbi.nlm.nih.gov/pubmed/30694319) 

[5] Otvos JD, et al. Clinical implications of discordance between low-density lipoprotein cholesterol and particle number. J Clin Lipidol. 2011 Mar. [https://www.ncbi.nlm.nih.gov/pubmed/21392724](https://www.ncbi.nlm.nih.gov/pubmed/21392724)

#patreon_articles 
#nutrition 
#disease 
#LDL 
#dietary_cholesterol 
#ApoB 
#animal_foods 
