There exists a pretty pervasive trope that tends to circle around plant-based communities, suggesting either that an essential omega-3 fatty acid called docosahexaenoic acid (DHA) is unnecessary in the diet, or that our DHA requirement can be entirely satisfied through supplementing with a precursor fatty acid that can be found in plant foods called alpha-linolenic acid (ALA). I was recently sent a paper that could help tease this apart.

Firstly, I can't stress enough that DHA is an essential nutrient. Whether or not we can convert enough DHA from ALA, the fact of the matter is that we need DHA in our bodies in order for our bodies to function optimally. DHA is integral to proper neurological and immunological functioning. It's probably a good idea to make sure your DHA status is optimal.

I was recently sent a paper by my buddy, [Zachary](https://twitter.com/ZacharyWenger) (you should follow him on Twitter, by the way). Basically, in this study, researchers divided people into six groups and gave them varying doses of either ALA (via flaxseed oil), DHA (via fish oil), or a placebo [1](https://pubmed.ncbi.nlm.nih.gov/18779299/). Their plasma and red blood cell (RBC) membrane ALA, eicosapentaenoic acid (EPA), and DHA representation was measured every two weeks for twelve weeks. Here are the results:

![[1-30.png]]

Take a moment to review the data carefully. As you can see, over twelve weeks fish oil does nothing to ALA status, but increases both EPA and DHA status. Flaxseed oil increases ALA status and, much to my surprise, it actually increases EPA status as well. But sadly, flaxseed oil does absolutely nothing to DHA status. 

This will be a disappointing finding for many people in the plant-based community. Some might criticize the finding, due to inadequate assessment of baseline omega-3 status using a gold standard measure like the omega-3 index. It could be that flaxseed oil actually does increase (or maintain) adequate DHA status, but perhaps we don't convert more than we need, with the end result being no measurable difference in RBC DHA if omega-3 status is optimal at baseline.

This is absolutely fair. However, we do have data investigating changes RBC DHA levels in response to flaxseed oil in those with suboptimal DHA status, as measured by the omega-3 index [2](https://pubmed.ncbi.nlm.nih.gov/17053155/). The results are consistent with the previous study discussed in this article— flaxseed oil increases EPA status but not DHA status. So the conclusion that flaxseed oil probably isn't helping your DHA status appears reasonably solid.

But hope is not lost. Preformed DHA can be obtained from vegan-friendly sources. You see, the reason fish are so rich in these long-chain omega-3 fatty acids is because they biomagnify up the food chain. The source of these fatty acids is actually from algae that fish on lower trophic levels will eat. For this reason we need not source our DHA from animal sources at all. We can just take algae supplements. An example of one such supplement can be found [here](https://www.amazon.com/dp/B07V7FHHWQ/ref=sspa_dk_detail_0?psc=1&pd_rd_i=B07V7FHHWQ&pd_rd_w=vcWRV&pf_rd_p=7d37a48b-2b1a-4373-8c1a-bdcc5da66be9&pd_rd_wg=zSZ5h&pf_rd_r=H4E990WYSMDA4E8K4NJ5&pd_rd_r=cd03fdea-cad0-4246-849c-199b9cccb2ed&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyNDVSMVU5UkVLWU1MJmVuY3J5cHRlZElkPUEwOTk4Mjc2MkNLU0pYQllHUkNQVSZlbmNyeXB0ZWRBZElkPUEwNjYwNTgyM0FIVVA4SjNSR1FQMCZ3aWRnZXROYW1lPXNwX2RldGFpbCZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=).

**Key points:**

-   It has been speculated that flaxseed oil can provide us with adequate DHA through its conversion from ALA.
-   Experiments investigating the relationship between flaxseed oil and DHA show that flaxseed oil does not improve DHA status.
-   It would be prudent for vegans to supplement with plant-based sources of DHA.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/18779299/](https://pubmed.ncbi.nlm.nih.gov/18779299/)

[2] [https://pubmed.ncbi.nlm.nih.gov/17053155/](https://pubmed.ncbi.nlm.nih.gov/17053155/)

#patreon_articles 
#nutrition 
#flaxseeds 
#polyunsaturated_fat 
#docosahexaenoic_acid 
#eicosapentaenoic_acid 
#vegan_talking_points 