Maybe I'm weird, but for some reason I often find myself searching on Google Images for pictures of fatty acid composition charts. However, the vast majority of the ones I have seen are not nearly as comprehensive as I would prefer. So I used data from the [Nutri-Dex](https://www.the-nutrivore.com/nutri-dex) to create a series of comprehensive fatty acid composition charts. The charts are sorted by each fatty acid subtype, as well as alphabetically. Enjoy!

![[Pasted image 20221123153503.png]]

![[Pasted image 20221123153507.png]]

![[Pasted image 20221123153510.png]]

![[Pasted image 20221123153514.png]]

![[Pasted image 20221123153518.png]]

![[Pasted image 20221123153521.png]]

![[Pasted image 20221123153524.png]]

#patreon_articles 
#nutrition 
#dietary_fat 
#polyunsaturated_fat 
#saturated_fat 
#monounsaturated_fat 
#trans_fat 
#vegetable_oil 
#animal_fats 