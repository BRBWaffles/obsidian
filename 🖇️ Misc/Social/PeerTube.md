# Description

Welcome to The Nutrivore PeerTube instance! This is a video streaming service, passionately run by [Nick Hiebert](https://www.the-nutrivore.com/), dedicated to debunking health and nutrition-related pseudoscience, and cultivating insightful philosophical discussions pertaining to topics like veganism and science. We also welcome casual discourse on a wide array of topics, including cooking, gaming, technology, and more. All levels of knowledge and interest are encouraged to engage! Nick strongly believes in freedom of speech and digital autonomy, and this instance was created in response to the enshittification of [YouTube](https://youtube.com/).

# Code of Conduct

• **Be respectful!** Please refrain from abusive behaviour. While mocking and/or shaming sophistry is permitted, behaviour that is consistently rude, belligerent, or obnoxious may result in being muted or banned. Be cognizant of the fact that despite our commitment to anti-censorship, there's no reason why we should tolerant abusive behaviour in our own home.

• **Back it up!** All empirical claims must be supported by empirical evidence upon reasonable request. Empirical claims regarding health and nutrition are no exception. If you make an empirical claim, back it up. Consistently failing to support empirical claims will be considered abusive behaviour and may result in you being banned.

• **Support us!** This instance costs time and money to run. If you wish to help support the instance, consider pledging to the admin. Of course, [LiberaPay](https://liberapay.com/TheNutrivore/) is the preferred donation platform, as it is non-profit, free to use, and open-source. However, [Patreon](https://www.patreon.com/thenutrivore), [PayPal](https://www.paypal.com/paypalme/TheNutrivore), and [YouTube Membership](https://www.youtube.com/@TheNutrivore/join) are also options. Alternatively, if a crypto donation option is preferred, the admin also accepts [Cardano](https://handle.me/thenutrivore).

• **PS.** If you feel as though you or someone else has been unfairly banned, the admin can be contacted at thenutrivore@the-nutrivore.social, and you're welcome to plead your case. Don't be shy!

# Terms

By accessing or using our platform, you agree to comply with and be bound by the following Terms of Service. Please read these terms carefully.

**1. Acceptance of Terms**

By creating an account and using our platform, you agree to abide by these Terms of Service and any applicable laws and regulations. If you do not agree to these terms, please do not use our service.

**2. Account Registration and Use**

To access our content, you must register for an account. You agree to provide accurate, current, and complete information during the registration process and to update such information to keep it accurate, current, and complete. You are responsible for safeguarding your account credentials and for all activities that occur under your account.

**3. Content**

Our platform is designed for sharing and viewing content provided by us. Users may comment on videos but are not permitted to upload their own content. You may not use our platform to distribute, share, or comment on content that is unlawful, defamatory, harassing, obscene, or otherwise objectionable.

**4. User Conduct**

You agree not to use the platform for any unlawful purposes or to engage in any behaviour that disrupts the service or harms other users. You shall not interfere with the operation of the platform, and you shall not attempt to gain unauthorized access to any part of the platform or its systems.

**5. Termination**

We reserve the right to suspend or terminate your account if you violate these Terms of Service or if we believe your actions may harm the platform or other users. You may terminate your account at any time by contacting us.

**6. Limitation of Liability**

Our platform is provided “as is” and “as available” without any warranties of any kind. We do not guarantee that the platform will be uninterrupted or error-free. In no event shall we be liable for any indirect, incidental, special, or consequential damages arising out of your use of the platform.

**7. Changes to Terms**

We may update these Terms of Service from time to time. Any changes will be posted on this page with an updated effective date. Your continued use of the platform after any changes indicates your acceptance of the revised terms.

# Server Specs

**OS:** NixOS 24.11.20240906.574d1ea (Vicuna) x86_64
**Kernel:** Linux 6.10.8
**CPU:** AMD Ryzen 5 5500U (12) @ 4.46 GHz
**GPU:** AMD Radeon Vega Series
**Memory:** 32GB

# Debate Channel

This channel is dedicated to Nick's debates!

# Video Description

Discord: https://discord.gg/eeYQ2wJknS
Mastodon: https://the-nutrivore.social

# Support

The Nutrivore PeerTube instance costs money to run, and every little bit helps! Consider pledging to any of these support options!

[LiberaPay](https://liberapay.com/TheNutrivore/)
[Patreon](https://patreon.com/TheNutrivore)
[PayPal](https://paypal.me/TheNutrivore)
[Ko-Fi](https://ko-fi.com/thenutrivore)
[Merch](https://streamlabs.com/thenutrivore/merch)
[Nutri-Dex](https://the-nutrivore.com/Nutri-Dex)
[Cardano](https://handle.me/thenutrivore)

