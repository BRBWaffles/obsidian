# WELCOME!

Welcome to The Nutrivore Mastodon instance! We are an evidence-based community, passionately run by [Nick Hiebert](https://www.the-nutrivore.com/), dedicated to debunking health and nutrition-related pseudoscience, and cultivating insightful philosophical discussions pertaining to topics like veganism and science. We also welcome casual discourse on a wide array of topics, including cooking, gaming, technology, and more. All levels of knowledge and interest are encouraged to engage!

While we are not specifically a debate instance, we advocate for resolving disagreements through respectful, good-faith verbal debate. For now, please take verbal debates to [The Nutrivore Discord server](https://discord.com/invite/YrcEvgRTqy) until we deploy a better, free, and open-source solution. We strongly believe in freedom of speech and digital autonomy, and this instance was created in response to the enshittification of [X (formerly Twitter)](https://x.com/).

We understand and are sensitive to the fact that philosophical conversations can sometimes stray into uncomfortable territory that might appear odd or even offensive to some. You can trust the mod team to fairly appraise all reports of abuse, and you needn't worry about being unfairly banned for engaging in good faith, philosophical discourse on this server. Though we do ask that you do not abuse our leniency with overtly grotesque dialogue just for the sake of it. Let's not make anti-censorship synonymous with vulgarity, please.

---

# ADDENDUM

• **Be respectful!** Please refrain from abusive behaviour. While mocking and/or shaming sophistry is permitted, behaviour that is consistently rude, belligerent, or obnoxious may result in being muted or banned. Be cognizant of the fact that despite our commitment to anti-censorship, there's no reason why we should tolerant abusive behaviour in our own home.

• **Back it up!** All empirical claims must be supported by empirical evidence upon reasonable request. Empirical claims regarding health and nutrition are no exception. If you make an empirical claim, back it up. Consistently failing to support empirical claims will be considered abusive behaviour and may result in you being banned.

• **Don't dodge!** Be aware that voice chat is the preferred debate medium on this instance. While nobody is obligated to engage in voice debate, we ask that if you do not have a legitimate reason for avoiding voice debate when challenged, you should also refrain from making the sorts of claims that led to the challenge to begin with. Once someone has retracted a claim, continuing to invite them to debate the retracted claim will be considered abusive behaviour and may result in you being banned.

• **Support us!** This instance costs time and money to run. If you wish to help support the instance, consider pledging to the admin. Of course, [LiberaPay](https://liberapay.com/TheNutrivore/) is the preferred donation platform, as it is non-profit, free to use, and open-source. However, [Patreon](https://www.patreon.com/thenutrivore), [PayPal](https://www.paypal.com/paypalme/TheNutrivore), and [YouTube Membership](https://www.youtube.com/@TheNutrivore/join) are also options. Alternatively, if a crypto donation option is preferred, the admin also accepts [Cardano](https://handle.me/thenutrivore).

• **PS.** If you feel as though you or someone else has been unfairly banned, the admin can be contacted at thenutrivore@the-nutrivore.social, and you're welcome to plead your case. Don't be shy!

---

# RULES

No spamming or excessive @-mentions: Avoid flooding the instance with repetitive messages or tagging multiple users unnecessarily. Respect everyone's time and attention.

No circumventing bans with sock accounts: If you're banned, do not create or use alternate accounts to rejoin the instance. Respect the consequences of your actions.

No doxing or posting others' private information: Protect everyone's privacy. Do not share personal information without consent.

No illegal or profoundly distasteful posts or activity: Abide by the law and maintain a respectful environment. Avoid sharing content that is illegal or grossly inappropriate.

No self-promotion without mod or admin permission: Promote your content only with explicit approval from a moderator or admin. Unsolicited advertisements are not allowed.

No harassing or discriminating against other members: Treat everyone with respect. Harassment or discrimination based on any attribute is strictly prohibited.

No complaining about moderator decisions in the server: Respect moderator decisions. If you have concerns, address them privately with the moderators or admins.

No racial/sexual/anti-intellectual slurs: Slurs and derogatory comments that are unwelcome to the recipient are not tolerated.

Don't waste the admin's or the moderation staff's time: Posting low-value garbage that is specifically designed to annoy or disrupt the admin or the mods may result in your account being frozen or banned.