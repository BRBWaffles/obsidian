[PEERTUBE](https://video.the-nutrivore.social/a/nick/video-channels)

I’m transitioning my video content from YouTube to my self-hosted PeerTube instance, because I strongly believe in freedom of speech, copyleft, and digital autonomy. YouTube has proven unreliable due to both censorship and copyright strikes levied against my channel. This shift allows me to better control my content and avoid these silly pitfalls.  
  
Full-length videos are now exclusively available on PeerTube, while YouTube will only host clips. By registering on PeerTube and subscribing to my channels, you’ll have access to all content. Just be sure to enable email notifications to be alerted of new uploads. I believe that those who are truly fans of my work and want the best experience possible will see this as a positive change.  
  
Recommended Clients: Unfortunately, I can't recommend any mobile clients for PeerTube at this time. They're all trash. Please use your browser, either on your mobile device or personal computer.

[MASTODON](https://the-nutrivore.social)

Microblogging will now be posted to my own self-hosted Mastodon instance. I've kinda grown tired of microblogging on X, where political correctness and unclear ban policies force me to constantly self-censor. I considered and ultimately rejected several alternative platforms. Eventually, I realized the solution to this problem is to simply self-host my own space―somewhere where I can be myself and speak as I please. And I recently obtained the necessary infrastructure to do this.

This instance is 100% mine, and I can't be censored or fucked with there. I invite everyone who values my content and interactions to follow me on this new platform (whether you agree with my views or not). Just make an account on another instance (like Mastodon.social), follow me, and turn on notifications.

Recommended Clients: [Moshidon](https://github.com/LucasGGamerM/moshidon) (android), [Official](https://github.com/mastodon/mastodon-android) (android), [Tusky](https://github.com/tuskyapp/Tusky) [(android),](https://github.com/tuskyapp/Tusky) [or](https://github.com/tuskyapp/Tusky) [Official](https://github.com/tuskyapp/Tusky) [(iOS)](https://github.com/tuskyapp/Tusky)

[DISCORD](https://discord.gg/eeYQ2wJknS)

The Nutrivore Discord server is an evidence-based community dedicated to debunking pseudoscience, especially in the fields of nutrition, health and fitness. Casual discourse is welcome, and includes topics such as cooking, gaming, technology, and more. Current members span many disciplines and include both students and professionals, all levels of interest and expertise are welcome.  
  
The Discord server is not explicitly a debate server. However, we strongly encourage that disagreements be resolved using debate (preferably verbal), as long as it is conducted respectfully and in good faith. Though we also firmly believe that shaming sophistry and bad faith behaviour is a net good overall. The Discord server is planned to be replaced with a Matrix server in future.

Recommended Clients: [Vencord](https://github.com/Vendicated/Vencord) (desktop) and [Official](https://discord.com/download) [(multi-platform)](https://discord.com/download)

[LIBERAPAY](https://liberapay.com/TheNutrivore)

I'm transitioning from Patreon to LiberaPay, because it is free, open source, and 100% your patronage goes to me and not some corporation. By supporting me on LiberaPay, you help to keep my platform online and active. However, unlike Patreon, LiberaPay is not a paywall service. You don't get access to exclusive content via LiberaPay by becoming a patron. That isn't to say that LiberaPay patrons wouldn't be able to get exclusive content through other means— it just means that such content isn't delivered through the LiberaPay platform itself.