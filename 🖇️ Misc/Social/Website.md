# Services

All prices in CAD

Bundled Tutoring

Any of the above services can be bundled for a $10 discount off each hour. For example, bundling two sessions of Debate Analysis would be $140 total, rather than $160 total at $80/hr.

Debate Coaching
$60/hr

Participate in a structured course consisting of five one-hour modules, covering critical thinking, debate strategy, propositional logic, and more. Throughout the course you will receive both personalized and generalizable advice on how to improve your debate performance.

Debate Analysis
$80/hr

Participate in focused one-hour sessions wherein your own recorded debates are analyzed for constructive feedback and advice to help you improve as a debater. You may also participate in mock debates, staged debates, and other exercises to help you get more comfortable with debate and verbal confrontation.

Nutrition Science
$40/hr

Participate in a one-hour Q&A session specifically to inquire about nutrition science. Ask questions about research design, methodology, epistemology, and study interpretation. Also, by participating you will also gain access to nutrition science interpretation cheat-sheets that will streamline and simply the research appraisal process.

Custom NixOS Builds
$40/hr

NixOS has become popular in my community, with many people choosing to explore it over Windows, MacOS, and other Linux distributions. Naturally, as a consequence of this, I receive numerous requests for help regarding the Nix programming language and NixOS system configuration. So, to fast-track newcomers and to make my life a little bit easier for both of us, I'm offering to build custom NixOS configurations for interested clients.

#  Platforms

[MASTODON](https://the-nutrivore.social)

Microblogging will now be posted to my own self-hosted Mastodon instance. I've kinda grown tired of microblogging on X, where political correctness and unclear ban policies force me to constantly self-censor. I considered and ultimately rejected several alternative platforms. Eventually, I realized the solution to this problem is to simply self-host my own space―somewhere where I can be myself and speak as I please. And I recently obtained the necessary infrastructure to do this.

This instance is 100% mine, and I can't be censored or fucked with there. I invite everyone who values my content and interactions to follow me on this new platform (whether you agree with my views or not). My instance is a "single-user" instances, meaning I'm the only user. But you can still follow me! Just make an account on another instance, like [Mastodon.social](https://mastodon.social) (alternatively, you can select whatever instance you wish from the [official index](https://instances.social)), follow me, and turn on notifications.

Recommended Clients: [Moshidon](https://github.com/LucasGGamerM/moshidon) (android), [Official](https://github.com/mastodon/mastodon-android) (android), [Tusky](https://github.com/tuskyapp/Tusky) [(android),](https://github.com/tuskyapp/Tusky) [or](https://github.com/tuskyapp/Tusky) [Official](https://github.com/tuskyapp/Tusky) [(iOS)](https://github.com/tuskyapp/Tusky)

[PEERTUBE](https://video.the-nutrivore.social/a/nick/video-channels)

I’m transitioning my video content from YouTube to my self-hosted PeerTube instance, because I strongly believe in freedom of speech, copyleft, and digital autonomy. YouTube has proven unreliable due to both censorship and copyright strikes levied against my channel. This shift allows me to better control my content and avoid these silly pitfalls.  
  
Full-length videos are now exclusively available on PeerTube, while YouTube will only host clips. By registering on PeerTube and subscribing to my channels, you’ll have access to all content. Just be sure to enable email notifications to be alerted of new uploads. I believe that those who are truly fans of my work and want the best experience possible will see this as a positive change.  
  
Recommended Clients: Unfortunately, I can't recommend any mobile clients for PeerTube at this time. They're all trash. Please use your browser, either on your mobile device or personal computer. Just be use to enable email notifications in your account settings.

[DISCORD](https://discord.gg/eeYQ2wJknS)

The Nutrivore Discord server is an evidence-based community dedicated to debunking pseudoscience, especially in the fields of nutrition, health and fitness. Casual discourse is welcome, and includes topics such as cooking, gaming, technology, and more. Current members span many disciplines and include both students and professionals, all levels of interest and expertise are welcome.  
  
The Discord server is not explicitly a debate server. However, we strongly encourage that disagreements be resolved using debate (preferably verbal), as long as it is conducted respectfully and in good faith. Though we also firmly believe that shaming sophistry and bad faith behaviour is a net good overall. The Discord server is planned to be replaced with a Matrix server in future.

Recommended Clients: [Vencord](https://github.com/Vendicated/Vencord) (desktop) and [Official](https://discord.com/download) [(multi-platform)](https://discord.com/download)

# Support

[LIBERAPAY](https://www.the-nutrivore.com/contact)
Fee: 0%

A free and open source recurring donation platform focused on privacy and transparency. It allows supporters to set up weekly donations to creators and projects. It is my preferred patronage option, as creators are not charged any fees for transacting on their platform.

[PATREON](https://www.the-nutrivore.com/contact)
Fee: 5%

A membership platform that allows creators to receive ongoing financial support from patrons in exchange for exclusive content and perks. It provides tools for managing patron relationships but takes a cut of earnings.

[PAYPAL](https://www.the-nutrivore.com/contact)
Fee: 1%

A widely-used digital payment service that enables easy one-time or recurring donations. It offers buyer and seller protections but takes a percentage fee on transactions. It is not among my most preferred payment platforms, but I understand that it is familiar and favoured by many.

[KO-FI](https://www.the-nutrivore.com/contact)
Fee: 5%

A creator-friendly platform for receiving one-time and monthly donations, with an emphasis on simplicity and low fees. It allows creators to offer digital downloads and commissions to supporters. One of my least preferred options, as the fees can be high.

[CARDANO](https://www.the-nutrivore.com/contact)
Fee: 0.17 ADA

A blockchain platform that enables peer-to-peer donations using its native cryptocurrency ADA. It offers fast and low-cost transactions but requires users to be familiar with cryptocurrency wallets and exchanges. Not my first choice, but if you prefer crypto this is what I take.

# Interviews

# Podcast Interviews

[Sigma Nutrition Radio, Episode 360](https://sigmanutrition.com/episode360/)

- Best and worse ways to calculate the nutrient density of a diet.
- Nutrient density per calorie vs. per weight vs. per serving.
- The diminishing returns of aiming to maximize nutrient density.
- Anti-nutrients: how relevant are they?
- Understanding the effect of phytate, oxalate, etc.
- Hard to get nutrients in typical diets.
- Synergistic and moderating effects of different nutrients.
- Non-essential nutrients & importance for health.

[Mark Bell's Power Project, Episode 670](https://www.youtube.com/watch?v=omzCi2CGoxo)

- Quick background on seed oils
- How does red meat increase disease risk?
- Exact health benefits of vegetable oils.
- Tucker Goodrich & Alan Flanagan debate review.
- Should you remove seed oils all together at once?
- Maybe vegetable oils aren’t that important?

[The Ian Cramer Podcast, Episode 92](https://iancramerpodcast.libsyn.com/heibert-garageband-export-final)

- The healthfulness of different fats
- Do oils cause cardiovascular disease?
- The pathophysiology of atherosclerosis.
- Nutrient density of different foods.
- Why we shouldn't listen to low carb quacks.
- The ethical case for bivalve consumption.

[Foolproof Mastery, Episode 14](https://www.youtube.com/watch?v=3w0wvckA1Hw)

- Vegetable oils & health outcomes.
- Lipid peroxidation.
- Lag time to LDL oxidation.
- Statistical analysis.
- Study design.

[Foolproof Mastery, Episode 15](https://www.youtube.com/watch?v=9k7COJgwCo4)

- Meat eating in Hong Kong.
- Different eating patterns.
- Glycine methionine ratio.
- Oysters as a vegan food.
- Nick's supplements.

[Ketogeek's Podcast, Episode 78](https://podcasts.google.com/feed/aHR0cHM6Ly9rZXRvZ2Vlay5saWJzeW4uY29tL3Jzcw/episode/MzU0YjkxOGYtNjljNy00OGZlLWE2YjMtYTk3YThhMDA0YWZi?hl=en-CA&ved=2ahUKEwjYgoqB7ND2AhVSbs0KHROGCBoQjrkEegQIAhAF&ep=6)

- How did you start getting into the seed oil debate?
- What are some common claims made against seed oils?
- Does PUFA cause heart disease, cancer, fatty liver, or hormonal imbalances? 
- Critique of the ancestral approach towards food and nutrition. 
- Why and when epidemiology can be good evidence.
- Resources people can read regarding PUFA and health.

[Legendary Life Podcast, Episode 391](https://www.legendarylifepodcast.com/391-5-common-food-and-nutrition-myths-debunked-with-nick-hiebert/)

- The importance of identifying quality & evidence-based information.
- Salt doesn’t cost high blood pressure?
- Foods that are high in potassium. 
- Why eating processed foods might make you fat.
- Western Diet Culture Mentality.
- Are there good foods and bad foods.
- Nutrient Density: Guide and Tools.

[Muscle Memoirs Podcast, Episode 82](https://www.youtube.com/watch?v=WfApzH4Dj3M)

- Mechanistic arguments against seed oils.
- Linoleic acid consumption and heart disease.
- Are hunter-gatherer diets optimal for modern humans?
- Saturated fat and blood lipids.
- Meta-analysis in nutrition.

[Muscle Memoirs Podcast, Episode 11](https://www.youtube.com/watch?v=SF1BBOA5FAQ)

- The story behind the Nutri-Dex.
- Misconceptions about nutrient density.
- Using the cheat sheet to optimize body composition.
- Top foods that are beneficial for bodybuilding.
- How are the most satiating foods determined?
- Nick gives some of his tops foods.

[Fit and Furious Podcast, Episode 51](https://www.youtube.com/watch?v=7I1IJSZIGm0)

- The story behind the Nutri-Dex.
- Seed oil consumption and health.
- Natural diets versus artificial diets.
- Ethical veganism and Nick's diet.

[Strenuous Life Podcast, Episode 244](https://www.grapplearts.com/contrarian-thinking-about-nutrition-with-nick-hiebert-strenuous-life-podcast-ep-244/)

- The nutrient density of different foods
- The ten all time greatest, most nutrient dense foods.
- The ketogenic diet.
- Fats vs carbs vs proteins.
