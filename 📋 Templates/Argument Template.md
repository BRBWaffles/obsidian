### Title

| <font color="CC6600">**Definiendum**</font> | <font color="CC6600">**Definiens**</font> |
|:----------------------------------------:|:------------------------------------------ |
|       <font color="CC6600">****</font>       |                                            | 
|       <font color="CC6600">****</font>       |                                            |
|       <font color="CC6600">****</font>       |                                            |
|       <font color="CC6600">****</font>       |                                            |
|       <font color="CC6600">****</font>       |                                            |
|       <font color="CC6600">****</font>       |                                            |
|       <font color="CC6600">****</font>       |                                            |
|       <font color="CC6600">****</font>       |                                            |
|       <font color="CC6600">****</font>       |                                            |

<div style="text-align: center">
<font color="CC6600">
<b>P1)</b></font> .
<br />
<font color="CC6600">
<b>()</b>
<br />
<b>P2)</b></font> .
<br />
<font color="CC6600">
<b>()</b>
<br />
<b>P3)</b></font> .
<br />
<font color="CC6600">
<b>()</b>
<br />
<b>P4)</b></font> .
<br />
<font color="CC6600">
<b>()</b>
<br />
<b>C)</b></font> Therefore, .
<br />
<font color="CC6600">
<b>(∴)</b>
<br />
<br />
</font>
</div>