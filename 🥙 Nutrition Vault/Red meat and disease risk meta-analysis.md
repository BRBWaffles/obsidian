Wang, Xia, et al. ‘Red and Processed Meat Consumption and Mortality: Dose-Response Meta-Analysis of Prospective Cohort Studies’. _Public Health Nutrition_, vol. 19, no. 5, Apr. 2016, pp. 893–905. _PubMed_, https://doi.org/10.1017/S1368980015002062.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/26143683/

**Red Meat and Disease Risk**
![[Pasted image 20220214165311.png]]

**Red Meat and All Cause Mortality:**
![[Pasted image 20220214165414.png]]

**Red Meat and CVD Mortality:**
![[Pasted image 20220214165446.png]]

**Red Meat and Cancer Mortality:**
![[Pasted image 20220214165456.png]]

**Sensitivity Analyses:**
![[Pasted image 20220214165548.png]]

**Conclusions:**
>In summary, the present meta-analysis suggests that higher consumption of total red meat and processed meat is associated with an increased risk of total mortality and deaths from CVD and cancer. Significant association between unprocessed red meat and mortality risk was found in US studies but not others. More studies are still needed to confirm the results and explore the underlying mechanisms.

**PDF:**
[[📂 Media/PDFs/div-class-title-red-and-processed-meat-consumption-and-mortality-dose-response-meta-analysis-of-prospective-cohort-studies-div.pdf]]

| **Endpoints** | **Exposures**   | **Populations** | **General**        | **People** |
| ------------- | --------------- | --------------- | ------------------ | ---------- |
| #all_cause_mortality          | #red_meat       | #humans         | #nutrition         | #xia_wang  |
| #cardiovascular_disease          | #meat           | #multinational  | #disease           | #frank_hu  |
| #cancer       | #animal_foods   | #united_states  | #meta_analysis     |            |
|               | #animal_protein | #europe         | #systematic_review |            |
|               |                 | #asia           | #epidemiology      |            |
|               |                 | #japan          | #mortality         |            |
|               |                 | #italy          | #dose_response     |            |
|               |                 | #united_kingdom |                    |            |
|               |                 | #australia      |                    |            |

****