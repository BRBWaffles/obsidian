**Twin Cycles Hypothesis:**
https://pubmed.ncbi.nlm.nih.gov/23075228/

**Personal Fat Threshold:**
https://pubmed.ncbi.nlm.nih.gov/25515001/

**Counterpoint Study:**
https://pubmed.ncbi.nlm.nih.gov/21656330/

**Counterbalance Study:**
https://pubmed.ncbi.nlm.nih.gov/25683066/

**DiRECT Trial:**
https://pubmed.ncbi.nlm.nih.gov/29221645/

**DiRECT Trial Physical Activity:**
https://pubmed.ncbi.nlm.nih.gov/36398460/

**DiRECT Two-Year Follow-up:**
https://pubmed.ncbi.nlm.nih.gov/30852132/

**DiRECT Five-Year Follow-up:**
https://pubmed.ncbi.nlm.nih.gov/38423026/

**ReTUNE Trial:**
https://pubmed.ncbi.nlm.nih.gov/37593846/

#disease 
#type_2_diabetes 
#nutrition 
#roy_taylor
#blood_glucose 
#hba1c 