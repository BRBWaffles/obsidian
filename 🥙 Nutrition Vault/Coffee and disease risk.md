https://pubmed.ncbi.nlm.nih.gov/31055709/

"Moderate coffee consumption (e.g. 2-4 cups/day) was associated with reduced all-cause and cause-specific mortality, compared to no coffee consumption. The inverse association between coffee and all-cause mortality was consistent by potential modifiers except region."

https://pubmed.ncbi.nlm.nih.gov/29549497/

"In older people, habitual coffee consumption was not associated with increased risk of functional impairment, and it might even be beneficial in women and those with hypertension, obesity or diabetes."

#disease
#coffee
#all_cause_mortality
#cardiovascular_disease
#cancer
#type_2_diabetes
#agility
#obesity
#hypertension
#disability
#brewed_drinks
#nutrition