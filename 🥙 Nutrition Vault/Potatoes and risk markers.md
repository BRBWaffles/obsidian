Smith, Daniel L., et al. ‘French-Fried Potatoes Consumption and Energy Balance: A Randomized Controlled Trial’. _The American Journal of Clinical Nutrition_, Feb. 2022, p. nqac045. _PubMed_, https://doi.org/10.1093/ajcn/nqac045.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/35179193/

**Conclusions:**
>There were no significant differences in FM or in glucoregulatory biomarkers after 30 days of potato consumption versus almonds. Results do not support a causal relationship between increased French fried potato consumption and the negative health outcomes studied.

**PDF:**
[[📂 Media/PDFs/French-fried_potatoes_consumption_and_energy_balance-_a_randomized_controlled_trial..pdf]]

| **Endpoints**   | **Exposures** | **Populations** | **General**    | **People** |
| --------------- | ------------- | --------------- | -------------- | ---------- |
| #anthropometics | #almonds      | #humans         | #nutrition     |            |
| #insulin        | #nuts         |                 | #disease       |            |
| #blood_glucose  | #fried_foods  |                 | #weight_loss   |            |
| #blood_lipids   | #potatoes     |                 | #weight_gain   |            |
|                 | #french_fries |                 | #energy_intake |            |

****