Ibsen, Daniel B., et al. ‘Replacement of Red and Processed Meat With Other Food Sources of Protein and the Risk of Type 2 Diabetes in European Populations: The EPIC-InterAct Study’. _Diabetes Care_, vol. 43, no. 11, Nov. 2020, pp. 2660–67. _PubMed_, https://doi.org/10.2337/dc20-1038.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/32868270/

**Population Characteristics:**
![[Pasted image 20220214162931.png]]

**Substitution Analysis:**
![[Pasted image 20220214163000.png]]

**Conclusions:**
>In conclusion, this study suggests that the replacement habitually of 1 serving/ day of red and processed meat with 1 serving/day of cheese, yogurt, nuts, or cereals may be associated with a lower rate of development of type 2 diabetes. Replacing red and processed meat with other sources of protein may have a public health impact for the prevention of type 2 diabetes.

**PDF:**
[[📂 Media/PDFs/dc201038.pdf]]

| **Endpoints** | **Exposures**   | **Populations** | **General**            |
| ------------- | --------------- | --------------- | ---------------------- |
| #type_2_diabetes         | #red_meat       | #EPIC_cohort    | #disease               |
|               | #poultry        | #EPIC_InterAct  | #nutrition             |
|               | #pork           | #humans         | #epidemiology          |
|               | #fish           |                 | #substitution_analyses | 
|               | #cheese         |                 |                        |
|               | #yogurt         |                 |                        |
|               | #milk           |                 |                        |
|               | #dairy          |                 |                        |
|               | #meat           |                 |                        |
|               | #eggs           |                 |                        |
|               | #legumes        |                 |                        |
|               | #nuts           |                 |                        |
|               | #cereals        |                 |                        |
|               | #processed_meat |                 |                        |

****