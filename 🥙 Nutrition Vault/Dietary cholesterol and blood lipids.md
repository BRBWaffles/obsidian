Hopkins, P. N. ‘Effects of Dietary Cholesterol on Serum Cholesterol: A Meta-Analysis and Review’. _The American Journal of Clinical Nutrition_, vol. 55, no. 6, June 1992, pp. 1060–70. _PubMed_, https://doi.org/10.1093/ajcn/55.6.1060.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/1534437/

**Effect of Dietary Cholesterol on Serum Cholesterol:**
![[Pasted image 20220214153205.png]]

**Scatterplot of Serum Cholesterol Changes:**
![[Pasted image 20220214153223.png]]

**Conclusions:**
>Serum cholesterol concentration is clearly increased by added dietary cholesterol but the magnitude of predicted change is modulated by baseline dietary cholesterol. The greatest response is expected when baseline dietary cholesterol is near zero, while little, if any, measurable change would be expected once baseline dietary cholesterol was > 400-500 mg/d. People desiring maximal reduction of serum cholesterol by dietary means may have to reduce their dietary cholesterol to minimal levels (< 100-150 mg/d) to observe modest serum cholesterol reductions while persons eating a diet relatively rich in cholesterol would be expected to experience little change in serum cholesterol after adding even large amounts of cholesterol to their diet. Despite modest average effects of dietary cholesterol, there are some individuals who are much more responsive (and others who are not responsive). Individual degrees of response to dietary cholesterol may be mediated by differences in cholesterol absorption efficiency, neutral sterol excretion, conversion of hepatic cholesterol to bile acids, or modulation of HMG-CoA reductase or other key enzymes involved in intracellular cholesterol economy, each ultimately resulting in changes of plasma LDL cholesterol con- centration mediated primarily by up- or down-regulation of LDL receptors.

**PDF:**
[[📂 Media/PDFs/ajcn%2F55.6.1060.pdf]]

| **Endpoints**      | **Exposures**        | **Populations** | **General**          |
| ------------------ | -------------------- | --------------- | -------------------- |
| #blood_lipids      | #dietary_cholesterol | #humans         | #disease             |
| #serum_cholesterol | #eggs                |                 | #animal_foods        |
|                    | #supplements         |                 | #animal_fats         |
|                    | #nutrients           |                 | #low_carb_talking_points |
|                    |                      |                 | #nutrition           |
|                    |                      |                 | #lipidology          |
|                    |                      |                 | #n00trients          | 

****

Vincent, Melissa J., et al. ‘Meta-Regression Analysis of the Effects of Dietary Cholesterol Intake on LDL and HDL Cholesterol’. _The American Journal of Clinical Nutrition_, vol. 109, no. 1, Jan. 2019, pp. 7–16. _PubMed_, https://doi.org/10.1093/ajcn/nqy273.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/30596814/

**Meta-Regression for LDL:**
![[Pasted image 20220214154142.png]]

**Meta-Regression for HDL:**
![[Pasted image 20220214154217.png]]

**PDF:**
[[📂 Media/PDFs/nqy273.pdf]]

**Supplements:**
[[📂 Media/Misc/Online Supporting Material 1 - resubmission 8-10-18.docx]]
[[📂 Media/Misc/Online Supporting Material 2 - resubmission 8-10-18.docx]]
[[📂 Media/Misc/Online Supporting Material 3 - resubmission 8-10-18.docx]]

| **Endpoints**      | **Exposures**        | **Populations** | **General**          |
| ------------------ | -------------------- | --------------- | -------------------- |
| #blood_lipids      | #dietary_cholesterol | #humans         | #disease             |
| #serum_cholesterol | #supplements         |                 | #nutrition           |
| #LDL               | #nutrients           |                 | #animal_foods        |
| #HDL               | #eggs                |                 | #animal_fats         |
|                    |                      |                 | #low_carb_talking_points |
|                    |                      |                 | #n00trients          | 

---

Carson, Jo Ann S., et al. ‘Dietary Cholesterol and Cardiovascular Risk: A Science Advisory From the American Heart Association’. _Circulation_, vol. 141, no. 3, Jan. 2020, pp. e39–53. _PubMed_, https://doi.org/10.1161/CIR.0000000000000743.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/31838890/

**Regression Analysis:**
![[Pasted image 20220612145302.png]]

**Notes:**
1) Why is this persuasive? They're trying to fit a non-linear relationship to a linear model, lol.

**PDF:**
[[📂 Media/PDFs/CIR.0000000000000743 (1).pdf]]

**Supplements:**
[[📂 Media/PDFs/data supplement 1.pdf]]
[[📂 Media/PDFs/data supplement 2.pdf]]
[[📂 Media/PDFs/data supplement 3.pdf]]

| **Endpoints**      | **Exposures**        | **Populations** | **General**          |
| ------------------ | -------------------- | --------------- | -------------------- |
| #blood_lipids      | #dietary_cholesterol | #humans         | #disease             |
| #serum_cholesterol | #supplements         |                 | #nutrition           |
| #LDL               | #nutrients           |                 | #animal_foods        |
| #HDL               | #eggs                |                 | #animal_fats         |
|                    |                      |                 | #low_carb_talking_points |
|                    |                      |                 | #n00trients          | 

****

Schonfeld, G., et al. ‘Effects of Dietary Cholesterol and Fatty Acids on Plasma Lipoproteins.’ _Journal of Clinical Investigation_, vol. 69, no. 5, May 1982, pp. 1072–80. _DOI.org (Crossref)_, https://doi.org/10.1172/JCI110542.

**Effects of Eggs:**
![[Pasted image 20220612150526.png]]

**Link:**
https://pubmed.ncbi.nlm.nih.gov/7068846/

**PDF:**
[[📂 Media/PDFs/JCI82110542.pdf]]

| **Endpoints** | **Exposures** | **Populations** | **General** | **People** |
| ------------- | ------------- | --------------- | ----------- | ---------- |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |

****

Connor, W. E., et al. ‘THE INTERRELATED EFFECTS OF DIETARY CHOLESTEROL AND FAT UPON HUMAN SERUM LIPID LEVELS’. _The Journal of Clinical Investigation_, vol. 43, Aug. 1964, pp. 1691–96. _PubMed_, https://doi.org/10.1172/JCI105044.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/14201552/

**Effects of Eggs:**
![[Pasted image 20220612151101.png]]

**PDF:**
[[📂 Media/PDFs/jcinvest00460-0187.pdf]]

| **Endpoints** | **Exposures** | **Populations** | **General** | **People** |
| ------------- | ------------- | --------------- | ----------- | ---------- |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |

****