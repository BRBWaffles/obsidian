Antvorskov, Julie C., et al. ‘Dietary Gluten and the Development of Type 1 Diabetes’. _Diabetologia_, vol. 57, no. 9, Sept. 2014, pp. 1770–80. _PubMed_, https://doi.org/10.1007/s00125-014-3265-1.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/24871322/

**Conclusions:**
>The present review describes the findings and development of our knowledge over the last decades on the connection between gluten and the pathogenesis of type 1 diabetes. The studies have primarily focused on describing incidence of type 1 diabetes in relation to the timing of introducing gluten into the diet, and the influence of gluten on the intestinal flora and the immune system. Most important is to evaluate the effect of a gluten-free diet on human type 1 diabetes: in this regard, a promising case has been published. In the future, the balance between the innate and adaptive immune systems must be clarified. The research field covering gluten, diet and type 1 diabetes has proven surprisingly interesting and requires further attention.

**PDF:**
[[📂 Media/PDFs/125_2014_Article_3265.pdf]]

| **Endpoints** | **Exposures**   | **Populations** | **General**          | **People** |
| ------------- | --------------- | --------------- | -------------------- | ---------- |
| #T1DM         | #gluten         | #infants        | #nutrition           |            |
| #microbiome   | #whole_grains   | #children       | #auto_immune         |            |
|               | #refined_grains | #humans         | #disease             |            |
|               |                 | #mice           | #clownery            |            |
|               |                 |                 | #clown_papers        |            |
|               |                 |                 | #low_carb_talking_points |            |
|               |                 |                 | #epidemiology        |            |

****