Furse, Samuel, et al. ‘Dietary PUFA Drives Diverse Systems-Level Changes in Lipid Metabolism’. _Molecular Metabolism_, Feb. 2022, p. 101457. _ScienceDirect_, https://doi.org/10.1016/j.molmet.2022.101457.

**Link:**
https://www.sciencedirect.com/science/article/pii/S2212877822000266?via%3Dihub

[[📂 Media/PDFs/1-s2.0-S2212877822000266-main.pdf]]

| **Endpoints**      | **Exposures**     | **Populations** | **General**   |
| ------------------ | ----------------- | --------------- | ------------- |
| #non_alcoholic_fatty_liver_disease             | #omega-6          | #humans         | #disease      |
| #emphysema         | #omega_3          | #mice           | #clown_papers |
| #bronchitis        | #vegetable_oil    | #infants        | #nutrition    |
| #heart_rate        | #fish_oil         | #children       | #clownery     |
| #cardiovascular_disease               | #docosahexaenoic_acid              |                 |               | 
| #child_development | #eicosapentaenoic_acid              |                 |               |
| #skin_cancer       | #arachidonic_acid |                 |               |
| #stroke            | #linoleic_acid    |                 |               |
| #mood_disorders    | #a_linolenic_acid |                 |               |
| #mental_health     | #polyunsaturated_fat             |                 |               |
| #cognitive_decline | #dietary_fat      |                 |               |
| #memory            |                   |                 |               |

****