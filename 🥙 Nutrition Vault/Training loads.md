https://pubmed.ncbi.nlm.nih.gov/33433148/

"Although muscle hypertrophy improvements seem to be load independent, increases in muscle strength are superior in high-load RT programs. Untrained participants exhibit greater muscle hypertrophy, whereas undertaking more RT sessions provides superior gains in those with previous training experience."

#fitness 
#hypertrophy
#exercise
#fitness 