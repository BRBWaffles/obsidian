Al-Shaar, Laila, et al. ‘Red Meat Intake and Risk of Coronary Heart Disease among US Men: Prospective Cohort Study’. _BMJ (Clinical Research Ed.)_, vol. 371, Dec. 2020, p. m4141. _PubMed_, https://doi.org/10.1136/bmj.m4141.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/33268459/

**Population Characteristics:**
![[Pasted image 20220213222459.png]]

**Meat and CHD Risk:**
![[Pasted image 20220213222524.png]]

**Substitution Analysis:**
![[Pasted image 20220213222551.png]]

**Substitutions for Dairy:**
![[Pasted image 20220213222625.png]]

**Substitutions for Fish:**
![[Pasted image 20220213222642.png]]

**Conclusions:**
![[Pasted image 20220213222655.png]]

**Notes:**

**PDF:**
[[📂 Media/PDFs/bmj.m4141.full.pdf]]

| **Endpoints** | **Exposures**   | **Populations**                      | **General**            | **People**      |
| ------------- | --------------- | ------------------------------------ | ---------------------- | --------------- |
| #coronary_heart_disease          | #red_meat       | #health_professionals_followup_study | #mortality             | #walter_willett |
|               | #animal_foods   | #united_states                       | #disease               | #frank_hu       |
|               | #dairy          |                                      | #substitution_analyses |                 |
|               | #fish           |                                      | #nutrition             |                 |
|               | #meat           |                                      | #cohort_studies        |                 |
|               | #processed_meat |                                      |                        |                 |

****