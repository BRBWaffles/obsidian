https://pubmed.ncbi.nlm.nih.gov/19099589/

"Dietary modification led to improvements in glycemic control and medication reduction/elimination in motivated volunteers with type 2 diabetes. The diet lower in carbohydrate led to greater improvements in glycemic control, and more frequent medication reduction/elimination than the low glycemic index diet. Lifestyle modification using low carbohydrate interventions is effective for improving and reversing type 2 diabetes."

https://pubmed.ncbi.nlm.nih.gov/28193517/

"We review the components of energy balance and the mechanisms acting to resist weight loss in the context of static, settling point, and set-point models of body weight regulation, with the set-point model being most commensurate with current data."

https://pubmed.ncbi.nlm.nih.gov/29466592/

"In this 12-month weight loss diet study, there was no significant difference in weight change between a healthy low-fat diet vs a healthy low-carbohydrate diet, and neither genotype pattern nor baseline insulin secretion was associated with the dietary effects on weight loss. In the context of these 2 common weight loss diet approaches, neither of the 2 hypothesized predisposing factors was helpful in identifying which diet was better for whom."

https://pubmed.ncbi.nlm.nih.gov/7598063/

"Excess dietary fat leads to greater fat accumulation than does excess dietary carbohydrate, and the difference was greatest early in the overfeeding period."

https://pubmed.ncbi.nlm.nih.gov/11029975/

"Thus, fat storage during overfeeding of isoenergetic amounts of diets rich in carbohydrate or in fat was not significantly different, and carbohydrates seemed to be converted to fat by both hepatic and extrahepatic lipogenesis."

#nutrition 
#disease 
#obesity 
#weight_loss 
#weight_gain 
#keto 
#low_carb 
#low_carb_talking_points 
#energy_intake 
#insulin_sensitivity 
