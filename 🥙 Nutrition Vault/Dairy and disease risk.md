https://pubmed.ncbi.nlm.nih.gov/23945722/

"This meta-analysis suggests that there is a significant inverse association between intakes of dairy products, low-fat dairy products, and cheese and risk of type 2 diabetes. Any additional studies should assess the association between other specific types of dairy products and the risk of type 2 diabetes and adjust for more confounding factors."

https://pubmed.ncbi.nlm.nih.gov/22810464/

"The observational evidence does not support the hypothesis that dairy fat or high-fat dairy foods contribute to obesity or cardiometabolic risk, and suggests that high-fat dairy consumption within typical dietary patterns is inversely associated with obesity risk. Although not conclusive, these findings may provide a rationale for future research into the bioactive properties of dairy fat and the impact of bovine feeding practices on the health effects of dairy fat."

https://pubmed.ncbi.nlm.nih.gov/33184632/

"Contrary to our hypothesis, neither dairy diet improved glucose tolerance in individuals with metabolic syndrome. Both dairy diets decreased insulin sensitivity through mechanisms largely unrelated to changes in key determinants of insulin sensitivity."

https://pubmed.ncbi.nlm.nih.gov/30782711/

"The association between dairy consumption and cancer risk has been explored in PMASRs with a variety of study designs and of low to moderate quality. To fully characterise valid associations between dairy consumption and risk of cancer and/or mortality rigorously conducted, PMASRs including only high-quality prospective study designs are required."

https://pubmed.ncbi.nlm.nih.gov/32185512/

"This meta-analysis provides evidence that dairy products can increase BMD in healthy postmenopausal women. Dairy product consumption should be considered an effective public health measure to prevent osteoporosis in postmenopausal women."

https://pubmed.ncbi.nlm.nih.gov/18539555/

"Increased dietary calcium/dairy products, with and without vitamin D, significantly increases total body and lumbar spine BMC in children with low base-line intakes."

https://pubmed.ncbi.nlm.nih.gov/31089733/

"This systematic review and meta-analysis is the first to evaluate the association between subtypes of milk and CRC risk. An inverse association between cheese consumption and the risk of CRC, particularly proximal colon cancer, was also found. No harmful effects associated with the consumption of any type of dairy product, including whole-fat dairy products, were observed."

#disease
#type_2_diabetes
#dairy
#saturated_fat
#obesity
#cardiovascular_disease
#cancer
#metabolic_syndrome
#glucose_tolerance
#insulin_sensitivity
#bone_mineral_density
#postmenopausal
#yogurt
#cheese
#osteoporosis
#children
#colorectal_cancer
#milk #cheese
#animal_fats
#animal_protein
#animal_foods
#nutrition