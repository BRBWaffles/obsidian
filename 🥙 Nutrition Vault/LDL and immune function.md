https://pubmed.ncbi.nlm.nih.gov/30657536/

"Results of this study suggest that lower measured LDL-C levels were significantly associated with increased risk of sepsis and admission to ICU in patients admitted to the hospital with infection; however, this association was due to comorbidities because both clinical models adjusted for confounders, and the genetic model showed no increased risk. Levels of LDL-C do not appear to directly alter the risk of sepsis or poor outcomes in patients hospitalized with infection."

https://pubmed.ncbi.nlm.nih.gov/30726226/

"PCSK9 LOF variants are not associated with increased risk of hospitalization for a serious infection. Among those hospitalized for a serious infection, PCSK9 LOF variants was not associated with odds of sepsis."

https://pubmed.ncbi.nlm.nih.gov/31075943/

"In conclusion, our analysis showed no positive association of sepsis with statin use in patients with dementia."

https://pubmed.ncbi.nlm.nih.gov/27688630/

"In this meta-analysis, we found that the use of statins did not significantly improve either in-hospital mortality or 28-day mortality in patients with sepsis. In the low-dose group, there were fewer quality multicenter studies; hence, conclusions based on the results of this subgroup are limited."

https://pubmed.ncbi.nlm.nih.gov/31694394/

"LDL-C and triglyceride polygenic scores were not significantly associated with hospitalization for infection, antibiotic use, or sepsis mortality."

#disease 
#LDL 
#statins
#sepsis 
#mendelian_randomization 
#immune_function
#inflammation
#lipidology 
#blood_lipids 