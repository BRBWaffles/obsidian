<div style="text-align: center";>
<b>P1)</b> If whole foods are necessarily better than oils, then whole grains are superior to salad dressing.
<br />
<b>(P→Q)</b>
<br />
<b>P2)</b> Whole grains are not superior to salad dressing.
<br />
<b>(¬Q)</b>
<br />
<b>C)</b> Therefore, whole foods are not necessarily better than oils.
<br />
<b>(∴¬P)</b>
</div>

#nutrition 
#debate 
#philosophy 
#whole_grains 
#whole_foods
#vegetable_oil 
#epistemology 
