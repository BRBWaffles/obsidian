https://pubmed.ncbi.nlm.nih.gov/28933024/

"The current literature shows that robust gains in muscular strength can be achieved even with short RIs (< 60 s). However, it seems that longer duration RIs (> 2 min) are required to maximize strength gains in resistance-trained individuals. With regard to untrained individuals, it seems that short to moderate RIs (60-120 s) are sufficient for maximizing muscular strength gains."

#strength
#fitness
#hypertrophy
#exercise 