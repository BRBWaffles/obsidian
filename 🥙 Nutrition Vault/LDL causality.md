Sniderman, Allan D., et al. ‘Apolipoprotein B vs Low-Density Lipoprotein Cholesterol and Non–High-Density Lipoprotein Cholesterol as the Primary Measure of Apolipoprotein B Lipoprotein-Related Risk: The Debate Is Over’. _JAMA Cardiology_, vol. 7, no. 3, Mar. 2022, pp. 257–58. _Silverchair_, https://doi.org/10.1001/jamacardio.2021.5080.

**Link:**
https://jamanetwork.com/journals/jamacardiology/article-abstract/2786334

****


**Conclusions:**
>

**PDF:**


**Supplements:**


| **Endpoints** | **Exposures** | **Populations** | **General** | **People** |
| ------------- | ------------- | --------------- | ----------- | ---------- |
| #cardiovascular_disease          | #ApoB         | #humans         | #disease    | #sniderman |
| #coronary_heart_disease          | #LDL          | #multinational  |             |            | 
|               | #nonHDL       | #united_kingdom |             |            |
|               | #blood_lipids | #united_states  |             |            |

****

https://pubmed.ncbi.nlm.nih.gov/27673306/

"In this meta-regression analysis, the use of statin and nonstatin therapies that act via upregulation of LDL receptor expression to reduce LDL-C were associated with similar RRs of major vascular events per change in LDL-C. Lower achieved LDL-C levels were associated with lower rates of major coronary events."

https://pubmed.ncbi.nlm.nih.gov/32076698/

"Lifelong exposure to high LDL-cholesterol increases the risk of symptomatic aortic stenosis, suggesting that LDL-lowering treatment may be effective in its prevention."

https://pubmed.ncbi.nlm.nih.gov/31756303/

"Lowering LDL-cholesterol is likely to prevent abdominal aortic aneurysm and aortic stenosis, in addition to CAD and other atheromatous cardiovascular outcomes. Lowering triglycerides is likely to prevent CAD and aortic valve stenosis but may increase thromboembolic risk."

https://pubmed.ncbi.nlm.nih.gov/27310947/

"Because LDL cholesterol has limited value to predict incident CVD, we recommend calculating non-HDL cholesterol or apoB with our equation to predict risk of incident CVD in the general Korean population."

https://pubmed.ncbi.nlm.nih.gov/29677301/

"In these meta-analyses and meta-regressions, more intensive compared with less intensive LDL-C lowering was associated with a greater reduction in risk of total and cardiovascular mortality in trials of patients with higher baseline LDL-C levels. This association was not present when baseline LDL-C level was less than 100 mg/dL, suggesting that the greatest benefit from LDL-C-lowering therapy may occur for patients with higher baseline LDL-C levels."

https://pubmed.ncbi.nlm.nih.gov/34550307/

"This cohort study showed that cumulative LDL-C and TWA LDL-C during young adulthood and middle age were associated with the risk of incident CHD, independent of midlife LDL-C level. These findings suggest that past levels of LDL-C may inform strategies for primary prevention of CHD and that maintaining optimal LDL-C levels at an earlier age may reduce the lifetime risk of developing atherosclerotic CVD."

https://pubmed.ncbi.nlm.nih.gov/30694319/

"Triglyceride-lowering LPL variants and LDL-C-lowering LDLR variants were associated with similar lower risk of CHD per unit difference in ApoB. Therefore, the clinical benefit of lowering triglyceride and LDL-C levels may be proportional to the absolute change in ApoB."

https://pubmed.ncbi.nlm.nih.gov/34773457/

#disease 
#lipidology 
#blood_lipids 
#coronary_heart_disease 
#LDL 
#ApoB 