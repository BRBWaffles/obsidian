Flament, Frederic, et al. ‘Effect of the Sun on Visible Clinical Signs of Aging in Caucasian Skin’. _Clinical, Cosmetic and Investigational Dermatology_, vol. 6, 2013, pp. 221–32. _PubMed_, https://doi.org/10.2147/CCID.S44686.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/24101874/

**Sun Exposure and Premature Aging:**
![[Pasted image 20220214150601.png]]

**Sun Damage as Age Predictor:**
![[Pasted image 20220214150724.png]]

**Conclusions:**
>With all the elements described in this study, we could calculate the importance of UV and sun exposure in the visible aging of a Caucasian woman’s face. This effect is about 80%. The interactions between chronological and photoinduced aging are complex, and the quantification of only the effect of sun-exposure is difficult to obtain. Our approach of using new descriptive skin-aging atlases is a solution to specify the extrinsic influence. Twenty-two clinical signs are used to describe and assess facial aging, wrinkles and skin texture, sagging of tissues, pigmentation manifestations, and vascular disorders. This study seems to confirm that pigmentation heterogeneity is a pure photoaging sign, whereas sagging of tissues is essentially a result of chronological aging. Vascular disorders could be considered as a precursor of future photoaging. Wrinkles and skin texture are influenced by both extrinsic and intrinsic aging, depending on the behavior of the individual with regard to the sun. The study confirms the accountability of sun exposure in premature aging of the face.

**PDF:**
[[📂 Media/PDFs/CCID-44686-sun-impact-on-clinical-visible-signs-of-ageing_092513.pdf]]

| **Endpoints** | **Exposures** | **Populations** | **General**          |
| ------------- | ------------- | --------------- | -------------------- |
| #aging        | #sunlight     | #humans         | #dermatology         |
| #skin_health  | #sunburn      |                 | #disease             |
|               |               |                 | #low_carb_talking_points |

****