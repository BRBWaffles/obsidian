Tey, S. L., et al. ‘Effects of Aspartame-, Monk Fruit-, Stevia- and Sucrose-Sweetened Beverages on Postprandial Glucose, Insulin and Energy Intake’. _International Journal of Obesity (2005)_, vol. 41, no. 3, Mar. 2017, pp. 450–57. _PubMed_, https://doi.org/10.1038/ijo.2016.225.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/27956737/

**Change in Insulin:**
![[📂 Media/PDFs/Pasted image 20220222183613.png]]

**Conclusions:**
>In conclusion, the consumption of calorie free beverages sweetened with non-nutritive sweeteners has minimal influences on total daily energy intake, glucose and insulin responses compared to a sucrose sweetened beverage in healthy lean males. It appears that the source of non-nutritive sweeteners (artificial or natural) does not differ in their effects on energy intake, postprandial glucose and insulin.

**PDF:**
[[📂 Media/PDFs/ijo.2016.225.pdf]]

| **Endpoints**  | **Exposures**          | **Populations** | **General**                | **People** |
| -------------- | ---------------------- | --------------- | -------------------------- | ---------- |
| #insulin       | #artificial_sweeteners | #humans         | #nutrition                 |            |
| #blood_glucose |                        | #overweight     | #low_carb_talking_points       |            |
|                |                        | #obese          | #novel_food                |            |
|                |                        |                 | #processed_food            |            |
|                |                        |                 | #sugar                     |            |
|                |                        |                 | #sugar_sweetened_beverages |            |
|                |                        |                 | #hormones                  |            |

****

Ma, Jing, et al. ‘Effect of the Artificial Sweetener, Sucralose, on Gastric Emptying and Incretin Hormone Release in Healthy Subjects’. _American Journal of Physiology. Gastrointestinal and Liver Physiology_, vol. 296, no. 4, Apr. 2009, pp. G735-739. _PubMed_, https://doi.org/10.1152/ajpgi.90708.2008.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/19221011/

**Hormonal Responses:**
![[📂 Media/PDFs/Pasted image 20220222184126.png]]

**Conclusions:**
>In conclusion, we have not been able to demonstrate that sucralose given by intragastric infusion stimulates GLP-1 or GIP release in humans or elicits a feedback response to slow gastric emptying. This implies that artificial sweeteners may have no therapeutic benefit in the dietary management of diabetes, other than as a substitute for carbohydrate.

**PDF:**
[[📂 Media/PDFs/ajpgi.90708.2008.pdf]]

| **Endpoints**  | **Exposures**          | **Populations** | **General**                | **People** |
| -------------- | ---------------------- | --------------- | -------------------------- | ---------- |
| #insulin       | #artificial_sweeteners | #humans         | #nutrition                 |            |
| #blood_glucose |                        |                 | #low_carb_talking_points       |            |
| #satiety       |                        |                 | #novel_food                |            |
|                |                        |                 | #processed_food            |            |
|                |                        |                 | #sugar                     |            |
|                |                        |                 | #sugar_sweetened_beverages |            |
|                |                        |                 | #hormones                  |            |

****