Zhao, Zhanwei, et al. ‘Red and Processed Meat Consumption and Colorectal Cancer Risk: A Systematic Review and Meta-Analysis’. _Oncotarget_, vol. 8, no. 47, Oct. 2017, pp. 83306–14. _PubMed_, https://doi.org/10.18632/oncotarget.20667.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/29137344/

**Colorectal Cancer (Red Meat):**
![[Pasted image 20220213234214.png]]

**Colon Cancer (Red Meat):**
![[Pasted image 20220213234343.png]]

**Proximal Colon Cancer (Red Meat):**
![[Pasted image 20220213234429.png]]

**Colorectal Cancer (Processed Meat):**
![[Pasted image 20220213235513.png]]

**Colon Cancer (Processed Meat):**
![[Pasted image 20220213235609.png]]

**Distal Colon Cancer (Processed Meat):**
![[Pasted image 20220213235709.png]]

**Cancer Subtype Analysis:**
![[Pasted image 20220213235416.png]]

**Conclusions:**
![[Pasted image 20220213235808.png]]

**PDF:**
[[📂 Media/PDFs/20667-296279-4-PB.pdf]]

**Supplements:**
[[📂 Media/PDFs/oncotarget-08-83306-s001.pdf]]
[[📂 Media/Misc/oncotarget-08-83306-s002.docx]]

| **Endpoints**          | **Exposures**   | **Populations** | **General**        | **People**    |
| ---------------------- | --------------- | --------------- | ------------------ | ------------- |
| #colon_cancer          | #red_meat       | #singapore      | #mortality         | #zhanwei_zhao |
| #colon_cancer          | #animal_foods   | #united_states  | #disease           |               |
| #proximal_colon_cancer | #processed_meat | #switzerland    | #nutrition         |               |
| #distal_colon_cancer   | #meat           | #italy          | #cohort_studies    |               |
| #rectal_cancer         |                 | #netherlands    | #meta_analysis     |               |
| #cancer                |                 | #china          | #systematic_review |               |
|                        |                 | #united_kingdom |                    |               |
|                        |                 | #japan          |                    |               |
|                        |                 | #canada         |                    |               |
|                        |                 | #norway         |                    |               |
|                        |                 | #india          |                    |               |
|                        |                 | #uruguay        |                    |               |
|                        |                 | #jordan         |                    |               |
|                        |                 | #korea          |                    |               |
|                        |                 | #spain          |                    |               |
|                        |                 | #finland        |                    |               |
|                        |                 | #australia      |                    |               |
|                        |                 | #sweden         |                    |               |
|                        |                 | #europe         |                    |               |

|     |     |     |     |     |     |     |     |     | 
| --- | --- | --- | --- | --- | --- | --- | --- | --- |


****