Lund-Blix, Nicolai A., et al. ‘Gluten Intake and Risk of Islet Autoimmunity and Progression to Type 1 Diabetes in Children at Increased Risk of the Disease: The Diabetes Autoimmunity Study in the Young (DAISY)’. _Diabetes Care_, vol. 42, no. 5, May 2019, pp. 789–96. _PubMed_, https://doi.org/10.2337/dc18-2315.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/30796108/

**Gluten and T1DM Risk:**
![[Pasted image 20220219214824.png]]

**Conclusions:**
>Our finding of the early introduction of gluten before 4 months of age as a potential risk factor for progression to type 1 diabetes supports general infant feeding recommendations from the American Academy of Pediatrics. Given our finding of no association between the amount of gluten intake at age 1–2 years or throughout childhood and adolescence and the risk of islet autoimmunity and progression to type 1 diabetes, we conclude that there is no rationale to reduce the amount of gluten during childhood and adolescence in the high risk population to prevent the development of type 1 diabetes.

**PDF:**
[[📂 Media/PDFs/dc182315.pdf]]

| **Endpoints**   | **Exposures**   | **Populations** | **General**   | **People** |
| --------------- | --------------- | --------------- | ------------- | ---------- |
| #autoantibodies | #gluten         | #infants        | #disease      |            |
| #T1DM           | #whole_grains   | #children       | #nutrition    |            |
|                 | #refined_grains | #humans         | #auto_immune  |            |
|                 |                 |                 | #epidemiology |            |

****

Ziegler, Anette-G., et al. ‘Early Infant Feeding and Risk of Developing Type 1 Diabetes-Associated Autoantibodies’. _JAMA_, vol. 290, no. 13, Oct. 2003, pp. 1721–28. _PubMed_, https://doi.org/10.1001/jama.290.13.1721.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/14519706/

**Risk for Islet Autoantibodies:**
![[Pasted image 20220219193337.png]]

**Conclusions:**
>In conclusion, this study finds early introduction of gluten-containing foods to be a risk factor for the development of type 1 DM-associated autoimmunity in children with HLA-DR3/DR4-DQ8 genotype of parents with type 1 DM. Although CIs are large and therefore the magnitude of the contribution to type 1 DM risk cannot be accurately assessed from this study, the data suggest that the prevalence of islet autoimmunity could be reduced if all families complied with infant feeding guidelines and did not introduce gluten-containing and solid foods to infants until after age 3 months. A significant effect on type 1 DM incidence may be expected if the association also is found with type 1 DM risk and if it is found in children of parents without DM.

**PDF:**
[[📂 Media/PDFs/joc30385.pdf]]

| **Endpoints**   | **Exposures**   | **Populations** | **General**   | **People** |
| --------------- | --------------- | --------------- | ------------- | ---------- |
| #autoantibodies | #gluten         | #infants        | #disease      |            |
| #T1DM           | #whole_grains   | #children       | #nutrition    |            |
|                 | #refined_grains | #humans         | #auto_immune  |            |
|                 |                 |                 | #epidemiology |            |

****

Norris, Jill M., et al. ‘Timing of Initial Cereal Exposure in Infancy and Risk of Islet Autoimmunity’. _JAMA_, vol. 290, no. 13, Oct. 2003, pp. 1713–20. _PubMed_, https://doi.org/10.1001/jama.290.13.1713.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/14519705/

**Age of Cereal Exposure and Islet Autoimmunity:**
![[Pasted image 20220219193833.png]]

**Conclusions:**
>Of our cohort, 31% were exposed to cereals outside of the 4- to 6-month age time window, yielding an adjusted HR of 4.3 for IA. Using this to calculate the population percent attributable risk, we found that 50% of IA would be eliminated in this population of children at moderate and high risk for type 1 DM if cereals were first introduced to the infant's diet between 4 and 6 months of age. While this population percent attributable risk is not directly applicable to the general population because it was derived from a population that was selected for being at increased risk for type 1 DM, it does indicate that manipulation of this infant diet exposure could have a strong impact on risk in children at increased risk for type 1 DM and potentially in the general public as well. We recommend that these results be confirmed in other prospective cohorts of children at risk for type 1 DM before any interventions are implemented. Additional studies may shed light on the importance of quantity of exposure and/or whether the risk is related to exposure to specific antigens or to other components of cereals. Our results do not suggest any need to change the current US infant feeding guidelines with regard to cereal introduction.

**PDF:**
[[📂 Media/PDFs/joc30623.pdf]]

**Supplements:**

| **Endpoints**   | **Exposures**   | **Populations** | **General**   | **People** |
| --------------- | --------------- | --------------- | ------------- | ---------- |
| #autoantibodies | #gluten         | #infants        | #disease      |            |
| #T1DM           | #whole_grains   | #children       | #nutrition    |            |
|                 | #refined_grains | #humans         | #auto_immune  |            |
|                 |                 |                 | #epidemiology |            |

****