---
annotation-target: "1-s2.0-S1751731122000040-main.pdf"
---

https://www.sciencedirect.com/science/article/pii/S1751731122000040

#nutrition 
#clown_papers 
#animal_foods 
#animal_protein 
#nutrient_deficiency 
#antinutrients 
#n00trients
#philosophy 
#disease 
#public_health 
#ty_beal 
#clownery 

>%%
>```annotation-json
>{"text":">Weasel words. Citing a couple of authors to represent the \"public heath institutions\" that advocate for animal food restriction, whilst also citing ten authors who challenge this notion, is a cheap move. The evidence for reducing animal foods is actually extensive, and those with contrary views represent the minority within that domain.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":13237,"end":13693},{"type":"TextQuoteSelector","exact":"Even though advocacy for moderate toheavy restriction is echoed by various public heath institutionsworldwide, suggesting apparent consensus, the scientific debateis not settled as the evidence has been challenged by various scien-tists, both for red meat (Truswell, 2009; Hite et al., 2010; Alexanderet al., 2015; Klurfeld, 2015; Kruger & Zhou, 2018; Händel et al.,2020; Hill et al., 2020; Johnston et al., 2019; Leroy and Cofnas,2020; Sholl et al., 2021)","prefix":"andeggs (Willett et al., 2019).","suffix":"and saturated fat, which is not"}]}],"created":"2022-02-28T20:29:36.271Z","updated":"2022-02-28T20:29:36.271Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%andeggs (Willett et al., 2019).%%HIGHLIGHT%% ==Even though advocacy for moderate toheavy restriction is echoed by various public heath institutionsworldwide, suggesting apparent consensus, the scientific debateis not settled as the evidence has been challenged by various scien-tists, both for red meat (Truswell, 2009; Hite et al., 2010; Alexanderet al., 2015; Klurfeld, 2015; Kruger & Zhou, 2018; Händel et al.,2020; Hill et al., 2020; Johnston et al., 2019; Leroy and Cofnas,2020; Sholl et al., 2021)== %%POSTFIX%%and saturated fat, which is not*
>%%LINK%%[[#^9qwaol4bx2|show annotation]]
>%%COMMENT%%
>>Weasel words. Citing a couple of authors to represent the "public heath institutions" that advocate for animal food restriction, whilst also citing ten authors who challenge this notion, is a cheap move. The evidence for reducing animal foods is actually extensive, and those with contrary views represent the minority within that domain.
>%%TAGS%%
>
^9qwaol4bx2


>%%
>```annotation-json
>{"text":">Red herring. While there are some plant foods that are high in saturated fat, we need not consume them. Those sources of saturated fat can be avoided on an animal food restricted diet. This is less true of animal foods. Particularly terrestrial animal foods.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":13694,"end":13810},{"type":"TextQuoteSelector","exact":"and saturated fat, which is not exclusiveto animal source foods (Astrup et al., 2020; Krauss & Kris-Etherton, 2020).","prefix":"ofnas,2020; Sholl et al., 2021)","suffix":"Among other concerns, one of the"}]}],"created":"2022-02-28T20:32:44.853Z","updated":"2022-02-28T20:32:44.853Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%ofnas,2020; Sholl et al., 2021)%%HIGHLIGHT%% ==and saturated fat, which is not exclusiveto animal source foods (Astrup et al., 2020; Krauss & Kris-Etherton, 2020).== %%POSTFIX%%Among other concerns, one of the*
>%%LINK%%[[#^7kjrdfw663|show annotation]]
>%%COMMENT%%
>>Red herring. While there are some plant foods that are high in saturated fat, we need not consume them. Those sources of saturated fat can be avoided on an animal food restricted diet. This is less true of animal foods. Particularly terrestrial animal foods.
>%%TAGS%%
>
^7kjrdfw663


>%%
>```annotation-json
>{"text":">Potential contradiction. You need to validate your potential confounders with evidence that meets your bar for causal inference. Otherwise you don't have a reason to consider them confounders. Virtually all accepted confounders in this domain are validated via epidemiological data itself. Therefore, the author's position appears to contain a contradiction, without further elaboration.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":13810,"end":14128},{"type":"TextQuoteSelector","exact":"Among other concerns, one of the objections is that pleas forrestriction are based on conflicting findings and observational rela-tionships that are not necessarily causal, suffering from confound-ing and bias (Grosso et al., 2017; Händel et al., 2020; Hill et al.,2020; Leroy & Barnard, 2020; Nordhagen et al., 2020).","prefix":"; Krauss & Kris-Etherton, 2020).","suffix":"Unwar-ranted use of causal lang"}]}],"created":"2022-02-28T20:36:14.406Z","updated":"2022-02-28T20:36:14.406Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%; Krauss & Kris-Etherton, 2020).%%HIGHLIGHT%% ==Among other concerns, one of the objections is that pleas forrestriction are based on conflicting findings and observational rela-tionships that are not necessarily causal, suffering from confound-ing and bias (Grosso et al., 2017; Händel et al., 2020; Hill et al.,2020; Leroy & Barnard, 2020; Nordhagen et al., 2020).== %%POSTFIX%%Unwar-ranted use of causal lang*
>%%LINK%%[[#^foba3isort|show annotation]]
>%%COMMENT%%
>>Potential contradiction. You need to validate your potential confounders with evidence that meets your bar for causal inference. Otherwise you don't have a reason to consider them confounders. Virtually all accepted confounders in this domain are validated via epidemiological data itself. Therefore, the author's position appears to contain a contradiction, without further elaboration.
>%%TAGS%%
>
^foba3isort


>%%
>```annotation-json
>{"text":">Red herring. Nutritional epidemiology has excellent validation, considering its limitations and shortcomings:\n\n>https://pubmed.ncbi.nlm.nih.gov/34526355/\nhttps://pubmed.ncbi.nlm.nih.gov/23700648/\n\n>The use of causal language is likely to be warranted more often than it is unwarranted.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":14129,"end":14360},{"type":"TextQuoteSelector","exact":"Unwar-ranted use of causal language is nonetheless widespread in theinterpretation of nutritional epidemiological data, thereby posinga systemic problem and undermining the field’s credibility(Cofield et al., 2010; Ioannidis, 2018)","prefix":"2020; Nordhagen et al., 2020).","suffix":". Moreover, the associationsbetw"}]}],"created":"2022-02-28T20:40:22.990Z","updated":"2022-02-28T20:40:22.990Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%2020; Nordhagen et al., 2020).%%HIGHLIGHT%% ==Unwar-ranted use of causal language is nonetheless widespread in theinterpretation of nutritional epidemiological data, thereby posinga systemic problem and undermining the field’s credibility(Cofield et al., 2010; Ioannidis, 2018)== %%POSTFIX%%. Moreover, the associationsbetw*
>%%LINK%%[[#^rxxra0mmlw|show annotation]]
>%%COMMENT%%
>>Red herring. Nutritional epidemiology has excellent validation, considering its limitations and shortcomings:
>
>>https://pubmed.ncbi.nlm.nih.gov/34526355/
>https://pubmed.ncbi.nlm.nih.gov/23700648/
>
>>The use of causal language is likely to be warranted more often than it is unwarranted.
>%%TAGS%%
>
^rxxra0mmlw


>%%
>```annotation-json
>{"text":">Red herring. Whether or not the associations are \"weak\" is an epistemic question. Additionally, what is the rationale for favouring absolute risk over relative risk? They both provide unique information that informs two different research questions.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":14362,"end":14570},{"type":"TextQuoteSelector","exact":"Moreover, the associationsbetween red meat and metabolic disease have not only been eval-uated as weak, translating into small absolute risks based on low tovery low certainty evidence (Johnston et al., 2019)","prefix":"et al., 2010; Ioannidis, 2018).","suffix":", but also differaccording to ge"}]}],"created":"2022-02-28T20:44:44.484Z","updated":"2022-02-28T20:44:44.484Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%et al., 2010; Ioannidis, 2018).%%HIGHLIGHT%% ==Moreover, the associationsbetween red meat and metabolic disease have not only been eval-uated as weak, translating into small absolute risks based on low tovery low certainty evidence (Johnston et al., 2019)== %%POSTFIX%%, but also differaccording to ge*
>%%LINK%%[[#^agik1o0tbj8|show annotation]]
>%%COMMENT%%
>>Red herring. Whether or not the associations are "weak" is an epistemic question. Additionally, what is the rationale for favouring absolute risk over relative risk? They both provide unique information that informs two different research questions.
>%%TAGS%%
>
^agik1o0tbj8


>%%
>```annotation-json
>{"text":">Potential contradiction. What is true of the evidence between fruits and vegetables and cancer and meat and cancer, such that we can infer causality for one and not the other? Causal inference is necessary if the authors wish to affirm that fruit and vegetable consumption is confounding.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":14763,"end":15178},{"type":"TextQuoteSelector","exact":"Associations are partic-ularly noticeable in North America, where meat is often consumedthrough a fast-food window and where high-meat consumers tendto also eat less healthy diets and follow less healthy lifestyles ingeneral. In a Canadian study, eating more meat was only associatedwith more all-cause cancer incidence for the subpopulation eatingthe lowest amounts of fruits and vegetables (Maximova et al.,2020).","prefix":"al., 2017; Iqbal et al., 2021).","suffix":"Several large-scale population-"}]}],"created":"2022-02-28T21:03:34.381Z","updated":"2022-02-28T21:03:34.381Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%al., 2017; Iqbal et al., 2021).%%HIGHLIGHT%% ==Associations are partic-ularly noticeable in North America, where meat is often consumedthrough a fast-food window and where high-meat consumers tendto also eat less healthy diets and follow less healthy lifestyles ingeneral. In a Canadian study, eating more meat was only associatedwith more all-cause cancer incidence for the subpopulation eatingthe lowest amounts of fruits and vegetables (Maximova et al.,2020).== %%POSTFIX%%Several large-scale population-*
>%%LINK%%[[#^sab47h209n|show annotation]]
>%%COMMENT%%
>>Potential contradiction. What is true of the evidence between fruits and vegetables and cancer and meat and cancer, such that we can infer causality for one and not the other? Causal inference is necessary if the authors wish to affirm that fruit and vegetable consumption is confounding.
>%%TAGS%%
>
^sab47h209n




>%%
>```annotation-json
>{"text":">Misunderstanding. From the wider literature, the typical threshold for harm with meat is at approximately 100g/day on average. The Oxford-EPIC cohort lacks power in those ranges:\n\n>https://pubmed.ncbi.nlm.nih.gov/23497300/\n\n>Data on the exposure contrasts in the 45-and-Up Study are even more unpersuasive.\n\n>https://pubmed.ncbi.nlm.nih.gov/28040519/","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":15179,"end":15470},{"type":"TextQuoteSelector","exact":"Several large-scale population-based studies, performed inindividuals with ‘healthy lifestyles’, such as the Oxford-EPIC Study(Key et al., 2003) and the 45-and-Up Study (Mihrshahi et al., 2017),also find that the negative effects of red meat consumption on all-cause mortality become benign.","prefix":"etables (Maximova et al.,2020).","suffix":"If red meat were indeed causall"}]}],"created":"2022-02-28T21:22:31.514Z","updated":"2022-02-28T21:22:31.514Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%etables (Maximova et al.,2020).%%HIGHLIGHT%% ==Several large-scale population-based studies, performed inindividuals with ‘healthy lifestyles’, such as the Oxford-EPIC Study(Key et al., 2003) and the 45-and-Up Study (Mihrshahi et al., 2017),also find that the negative effects of red meat consumption on all-cause mortality become benign.== %%POSTFIX%%If red meat were indeed causall*
>%%LINK%%[[#^a73dutpahlj|show annotation]]
>%%COMMENT%%
>>Misunderstanding. From the wider literature, the typical threshold for harm with meat is at approximately 100g/day on average. The Oxford-EPIC cohort lacks power in those ranges:
>
>>https://pubmed.ncbi.nlm.nih.gov/23497300/
>
>>Data on the exposure contrasts in the 45-and-Up Study are even more unpersuasive.
>
>>https://pubmed.ncbi.nlm.nih.gov/28040519/
>%%TAGS%%
>
^a73dutpahlj


>%%
>```annotation-json
>{"text":">Potential contradiction. The association between all-cause mortality and red meat consumption is stronger than the inverse association between all-cause mortality and fruit and vegetable consumption:\n\n>https://pubmed.ncbi.nlm.nih.gov/28446499/\n\n>Again, what is true of the evidence between fruits and vegetables and all-cause mortality and red meat and all-cause mortality, such that we can infer causality for one and not the other?","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":15471,"end":15765},{"type":"TextQuoteSelector","exact":"If red meat were indeed causallydriving the associations, one would anticipate finding strongereffects in systematic reviews looking specifically at red meat intake(able to evaluate a large intake gradient) compared to dietary pat-tern studies (smaller intake gradient) (Johnston et al., 2018).","prefix":"-cause mortality become benign.","suffix":"Onthe contrary, the absolute ri"}]}],"created":"2022-02-28T21:23:01.380Z","updated":"2022-02-28T21:23:01.380Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%-cause mortality become benign.%%HIGHLIGHT%% ==If red meat were indeed causallydriving the associations, one would anticipate finding strongereffects in systematic reviews looking specifically at red meat intake(able to evaluate a large intake gradient) compared to dietary pat-tern studies (smaller intake gradient) (Johnston et al., 2018).== %%POSTFIX%%Onthe contrary, the absolute ri*
>%%LINK%%[[#^un6kp8zpkw|show annotation]]
>%%COMMENT%%
>>Potential contradiction. The association between all-cause mortality and red meat consumption is stronger than the inverse association between all-cause mortality and fruit and vegetable consumption:
>
>>https://pubmed.ncbi.nlm.nih.gov/28446499/
>
>>Again, what is true of the evidence between fruits and vegetables and all-cause mortality and red meat and all-cause mortality, such that we can infer causality for one and not the other?
>%%TAGS%%
>
^un6kp8zpkw


>%%
>```annotation-json
>{"text":">Red herring. Just because the contribution of meat and diet/lifestyle factors have similar magnitudes of effect doesn't mean a mutual adjustment would do anything to the effect. Both exposures could be interacting with the outcome without interacting with each other.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":15766,"end":16167},{"type":"TextQuoteSelector","exact":"Onthe contrary, the absolute risk reductions from both reviews speci-fic to intake versus dietary pattern (Johnston et al., 2019) were verysimilar in their magnitude of effect, indicating the possibility that,even after adjustment, a multitude of other diet or lifestyle compo-nents may be confounding the associations irrespective of whetherthey are negative or positive (Zeraatkar & Johnston, 2019).","prefix":"dient) (Johnston et al., 2018).","suffix":"While such troubling incongruity"}]}],"created":"2022-02-28T21:41:28.741Z","updated":"2022-02-28T21:41:28.741Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%dient) (Johnston et al., 2018).%%HIGHLIGHT%% ==Onthe contrary, the absolute risk reductions from both reviews speci-fic to intake versus dietary pattern (Johnston et al., 2019) were verysimilar in their magnitude of effect, indicating the possibility that,even after adjustment, a multitude of other diet or lifestyle compo-nents may be confounding the associations irrespective of whetherthey are negative or positive (Zeraatkar & Johnston, 2019).== %%POSTFIX%%While such troubling incongruity*
>%%LINK%%[[#^fa2kgbbcz39|show annotation]]
>%%COMMENT%%
>>Red herring. Just because the contribution of meat and diet/lifestyle factors have similar magnitudes of effect doesn't mean a mutual adjustment would do anything to the effect. Both exposures could be interacting with the outcome without interacting with each other.
>%%TAGS%%
>
^fa2kgbbcz39


>%%
>```annotation-json
>{"text":">Red herring. These associations are seen in populations that are not consuming Westernized diets:\n\n>https://pubmed.ncbi.nlm.nih.gov/33320898/","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":16167,"end":16460},{"type":"TextQuoteSelector","exact":"While such troubling incongruity can be partially ascribed todifferences in methodological set-up between studies, it has beenhypothesised that the associations found in the West could at leastpartially be seen as cultural constructs generated by responses tonorms of eating right (Hite, 2018)","prefix":"ve (Zeraatkar & Johnston, 2019).","suffix":". An important question to con-s"}]}],"created":"2022-02-28T22:00:19.582Z","updated":"2022-02-28T22:00:19.582Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%ve (Zeraatkar & Johnston, 2019).%%HIGHLIGHT%% ==While such troubling incongruity can be partially ascribed todifferences in methodological set-up between studies, it has beenhypothesised that the associations found in the West could at leastpartially be seen as cultural constructs generated by responses tonorms of eating right (Hite, 2018)== %%POSTFIX%%. An important question to con-s*
>%%LINK%%[[#^ht1ueh55av8|show annotation]]
>%%COMMENT%%
>>Red herring. These associations are seen in populations that are not consuming Westernized diets:
>
>>https://pubmed.ncbi.nlm.nih.gov/33320898/
>%%TAGS%%
>
^ht1ueh55av8



>%%
>```annotation-json
>{"text":">Bullshit. These associations are seen even **within** higher socioeconomic strata:\n\n>https://pubmed.ncbi.nlm.nih.gov/22412075/","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":16462,"end":17022},{"type":"TextQuoteSelector","exact":"An important question to con-sider, therefore, is ‘‘whether intake of animal and plant proteinsis a marker of overall dietary patterns or of social class” (Naghshiet al., 2020). Upper-middle classes, who are particularly sensitiveto the ideologies of eating virtuous, tend to eat less red meat andsaturated fat because of what they symbolise, and because of whatthey are being told by authorities and moralising societal discourse(Leroy & Hite, 2020). However, those same people are also moreeducated, wealthier, and healthier in general (Leroy & Cofnas,2020).","prefix":"s of eating right (Hite, 2018).","suffix":"Even if multivariable models ar"}]}],"created":"2022-02-28T22:07:24.115Z","updated":"2022-02-28T22:07:24.115Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%s of eating right (Hite, 2018).%%HIGHLIGHT%% ==An important question to con-sider, therefore, is ‘‘whether intake of animal and plant proteinsis a marker of overall dietary patterns or of social class” (Naghshiet al., 2020). Upper-middle classes, who are particularly sensitiveto the ideologies of eating virtuous, tend to eat less red meat andsaturated fat because of what they symbolise, and because of whatthey are being told by authorities and moralising societal discourse(Leroy & Hite, 2020). However, those same people are also moreeducated, wealthier, and healthier in general (Leroy & Cofnas,2020).== %%POSTFIX%%Even if multivariable models ar*
>%%LINK%%[[#^urznngklf4|show annotation]]
>%%COMMENT%%
>>Bullshit. These associations are seen even **within** higher socioeconomic strata:
>
>>https://pubmed.ncbi.nlm.nih.gov/22412075/
>%%TAGS%%
>
^urznngklf4


>%%
>```annotation-json
>{"text":">Potential contradiction. Again, what is true of the evidence between smoking/alcohol/obesity and health outcomes and red meat and health outcomes such that we can infer causality for one and not the other?","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":17023,"end":17288},{"type":"TextQuoteSelector","exact":"Even if multivariable models are used to account for suchconfounding effects as smoking, alcohol consumption, or obesity,it may not be possible to disentangle the effects of all dietary andlifestyle factors involved, especially given the low certainty of evi-dence.","prefix":"general (Leroy & Cofnas,2020).","suffix":"Therefore, WHO (2015) mentions"}]}],"created":"2022-02-28T22:09:56.702Z","updated":"2022-02-28T22:09:56.702Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%general (Leroy & Cofnas,2020).%%HIGHLIGHT%% ==Even if multivariable models are used to account for suchconfounding effects as smoking, alcohol consumption, or obesity,it may not be possible to disentangle the effects of all dietary andlifestyle factors involved, especially given the low certainty of evi-dence.== %%POSTFIX%%Therefore, WHO (2015) mentions*
>%%LINK%%[[#^d9ws9rpwhp|show annotation]]
>%%COMMENT%%
>>Potential contradiction. Again, what is true of the evidence between smoking/alcohol/obesity and health outcomes and red meat and health outcomes such that we can infer causality for one and not the other?
>%%TAGS%%
>
^d9ws9rpwhp


>%%
>```annotation-json
>{"text":">Appeal to authority. Causal inference is an epistemic question, informed by statistics. It's funny how there is no appraisal of methodology when the results concord with the authors' biases. ","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":17289,"end":17422},{"type":"TextQuoteSelector","exact":"Therefore, WHO (2015) mentions that eating unprocessedred meat ‘‘has not yet been established as a cause of cancer” (em-phasis added)","prefix":"the low certainty of evi-dence.","suffix":", while IARC (2015) stated that"}]}],"created":"2022-02-28T22:13:03.751Z","updated":"2022-02-28T22:13:03.751Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%the low certainty of evi-dence.%%HIGHLIGHT%% ==Therefore, WHO (2015) mentions that eating unprocessedred meat ‘‘has not yet been established as a cause of cancer” (em-phasis added)== %%POSTFIX%%, while IARC (2015) stated that*
>%%LINK%%[[#^95jk99jbml9|show annotation]]
>%%COMMENT%%
>>Appeal to authority. Causal inference is an epistemic question, informed by statistics. It's funny how there is no appraisal of methodology when the results concord with the authors' biases. 
>%%TAGS%%
>
^95jk99jbml9



>%%
>```annotation-json
>{"text":">Red herring. This is true of any association, as per the Duhem-Quine thesis. Causal inference is a separate consideration, and the fact that auxiliary hypotheses can be proposed is tangential.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":17424,"end":17950},{"type":"TextQuoteSelector","exact":"while IARC (2015) stated that ‘‘chance, bias, andconfounding could not be ruled out” with respect to the associationbetween red meat intake and colorectal cancer. According to some(e.g., Hite, 2018), nutritional epidemiology of chronic disease isthus at risk of capturing cultural artefacts and health beliefs withinobservational relationships, rather than reliably quantifying actualhealth effects. Such observations are then used to reinforce dietaryadvice, potentially creating a positive feedback loop (Leroy & Hite,2020).","prefix":"e of cancer” (em-phasis added),","suffix":"This problem is further underli"}]}],"created":"2022-02-28T22:17:09.349Z","updated":"2022-02-28T22:17:09.349Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%e of cancer” (em-phasis added),%%HIGHLIGHT%% ==while IARC (2015) stated that ‘‘chance, bias, andconfounding could not be ruled out” with respect to the associationbetween red meat intake and colorectal cancer. According to some(e.g., Hite, 2018), nutritional epidemiology of chronic disease isthus at risk of capturing cultural artefacts and health beliefs withinobservational relationships, rather than reliably quantifying actualhealth effects. Such observations are then used to reinforce dietaryadvice, potentially creating a positive feedback loop (Leroy & Hite,2020).== %%POSTFIX%%This problem is further underli*
>%%LINK%%[[#^5cxa81frfid|show annotation]]
>%%COMMENT%%
>>Red herring. This is true of any association, as per the Duhem-Quine thesis. Causal inference is a separate consideration, and the fact that auxiliary hypotheses can be proposed is tangential.
>%%TAGS%%
>
^5cxa81frfid


>%%
>```annotation-json
>{"text":">Equivocation. The authors' references don't support the claim. Until this point they were discussing the impact of meat products on disease outcomes, not disease risk markers or biochemical mechanisms.\n\n>However, one of the only studies that did attempt to replace animal foods in the diet also showed one of the largest effect sizes in reducing the risk of acute myocardial infarction:\n\n>https://pubmed.ncbi.nlm.nih.gov/7911176/\n\n>On top of that, human mechanistic studies also support that meat increases CVD risk factors:\n\n>https://pubmed.ncbi.nlm.nih.gov/31161217/","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":17951,"end":18297},{"type":"TextQuoteSelector","exact":"This problem is further underlined by the lack of supportfrom intervention trials (O’Connor et al., 2017; Turner & Lloyd,2017; Leroy & Cofnas, 2020), which are designed to account forknown and unknown confounders, and the fact that the mechanis-tic rationale for red meats remains speculative at best (Delgadoet al., 2020; Leroy & Barnard, 2020).","prefix":"dback loop (Leroy & Hite,2020).","suffix":"Taken together, various public h"}]}],"created":"2022-02-28T22:25:45.757Z","updated":"2022-02-28T22:25:45.757Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%dback loop (Leroy & Hite,2020).%%HIGHLIGHT%% ==This problem is further underlined by the lack of supportfrom intervention trials (O’Connor et al., 2017; Turner & Lloyd,2017; Leroy & Cofnas, 2020), which are designed to account forknown and unknown confounders, and the fact that the mechanis-tic rationale for red meats remains speculative at best (Delgadoet al., 2020; Leroy & Barnard, 2020).== %%POSTFIX%%Taken together, various public h*
>%%LINK%%[[#^b8t0hz487yu|show annotation]]
>%%COMMENT%%
>>Equivocation. The authors' references don't support the claim. Until this point they were discussing the impact of meat products on disease outcomes, not disease risk markers or biochemical mechanisms.
>
>>However, one of the only studies that did attempt to replace animal foods in the diet also showed one of the largest effect sizes in reducing the risk of acute myocardial infarction:
>
>>https://pubmed.ncbi.nlm.nih.gov/7911176/
>
>>On top of that, human mechanistic studies also support that meat increases CVD risk factors:
>
>>https://pubmed.ncbi.nlm.nih.gov/31161217/
>%%TAGS%%
>
^b8t0hz487yu


>%%
>```annotation-json
>{"text":">Potential contradiction. The authors need to define \"conclusive proof\", and demonstrate how it has been shown for all variables they affirm as confounding.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":18297,"end":18687},{"type":"TextQuoteSelector","exact":"Taken together, various public health organisations make a casefor the reduction of animal source foods based on their interpreta-tion of the prevailing scientific evidence. Others, however, arguethat conclusive proof for (some of) these recommendations is miss-ing, particularly given the contribution of animal source foods toclosing essential micronutrient gaps (Leroy & Barnard, 2020). ","prefix":"., 2020; Leroy & Barnard, 2020).","suffix":"Argu-ing for strong reductions c"}]}],"created":"2022-02-28T22:29:45.941Z","updated":"2022-02-28T22:29:45.941Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%., 2020; Leroy & Barnard, 2020).%%HIGHLIGHT%% ==Taken together, various public health organisations make a casefor the reduction of animal source foods based on their interpreta-tion of the prevailing scientific evidence. Others, however, arguethat conclusive proof for (some of) these recommendations is miss-ing, particularly given the contribution of animal source foods toclosing essential micronutrient gaps (Leroy & Barnard, 2020).== %%POSTFIX%%Argu-ing for strong reductions c*
>%%LINK%%[[#^qate25c8juk|show annotation]]
>%%COMMENT%%
>>Potential contradiction. The authors need to define "conclusive proof", and demonstrate how it has been shown for all variables they affirm as confounding.
>%%TAGS%%
>
^qate25c8juk


>%%
>```annotation-json
>{"text":">Appeal to nature. Just because meat is an integral part of our evolutionary history does not actually mean that it is necessary beneficial for the long-term health of modern humans.\n\n>In fact, there are valid reasons to suspect that foods to which we are most strongly adapted are actually likely to be detrimental for long-term health, via antagonistic pleiotropy.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":18687,"end":20417},{"type":"TextQuoteSelector","exact":"Argu-ing for strong reductions contradicts common-sense approaches,especially from an anthropological perspective (Gupta, 2016;Leroy et al., 2020a). Meat, marrow, and seafood are evolutionarycomponents of the human diet, even if they may have displayedsome nutritional and biochemical differences compared to whatis produced today in intensified operations, e.g., with respect tofat composition (Kuipers et al., 2010; Manzano-Baena &Salguero-Herrera 2018) and the presence of phytochemicals (vanVliet et al., 2021a, and 2021b). The health impact of these differ-ences may be significant but remains difficult to quantify, thoughpolyunsaturated fatty acids/saturated fatty acids and omega 3/6ratios of wild ruminants living in current times are similar topasture-raised (grass-fed) beef, but dissimilar to grain-fed beef(Cordain et al., 2002b). Be that as it may, the abundant consump-tion of animal source foods over 2.5 million years has resulted inan adapted human anatomy, metabolism, and cognitive capacitythat is divergent from other apes (Milton, 2003; Mann, 2018). Also,many hunter-gatherer populations consume far larger amounts ofmeat and other animal source foods (sometimes > 300 kg/p/y),than what is now consumed in the West (around 100 kg/p/y). Thisis likely still much below what was once valid for early humanspreying on megafauna (Ben-Dor & Barkai, 2020). On a caloric basis,the animal:plant ratio of Western diets (about 1:2 in the US;Rehkamp, 2016) is the inverse of most pre-agricultural diets (meanof 2:1; Cordain et al., 2000). Such high amounts of animal sourcefoods are not necessarily indicative of a health advantage, but itcan be assumed that animal source foods are at least compatiblewith good health. ","prefix":"t gaps (Leroy & Barnard, 2020).","suffix":"So-called ‘‘diseases of modernit"}]}],"created":"2022-02-28T22:31:49.005Z","updated":"2022-02-28T22:31:49.005Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%t gaps (Leroy & Barnard, 2020).%%HIGHLIGHT%% ==Argu-ing for strong reductions contradicts common-sense approaches,especially from an anthropological perspective (Gupta, 2016;Leroy et al., 2020a). Meat, marrow, and seafood are evolutionarycomponents of the human diet, even if they may have displayedsome nutritional and biochemical differences compared to whatis produced today in intensified operations, e.g., with respect tofat composition (Kuipers et al., 2010; Manzano-Baena &Salguero-Herrera 2018) and the presence of phytochemicals (vanVliet et al., 2021a, and 2021b). The health impact of these differ-ences may be significant but remains difficult to quantify, thoughpolyunsaturated fatty acids/saturated fatty acids and omega 3/6ratios of wild ruminants living in current times are similar topasture-raised (grass-fed) beef, but dissimilar to grain-fed beef(Cordain et al., 2002b). Be that as it may, the abundant consump-tion of animal source foods over 2.5 million years has resulted inan adapted human anatomy, metabolism, and cognitive capacitythat is divergent from other apes (Milton, 2003; Mann, 2018). Also,many hunter-gatherer populations consume far larger amounts ofmeat and other animal source foods (sometimes > 300 kg/p/y),than what is now consumed in the West (around 100 kg/p/y). Thisis likely still much below what was once valid for early humanspreying on megafauna (Ben-Dor & Barkai, 2020). On a caloric basis,the animal:plant ratio of Western diets (about 1:2 in the US;Rehkamp, 2016) is the inverse of most pre-agricultural diets (meanof 2:1; Cordain et al., 2000). Such high amounts of animal sourcefoods are not necessarily indicative of a health advantage, but itcan be assumed that animal source foods are at least compatiblewith good health.== %%POSTFIX%%So-called ‘‘diseases of modernit*
>%%LINK%%[[#^mbq0pgqh9ld|show annotation]]
>%%COMMENT%%
>>Appeal to nature. Just because meat is an integral part of our evolutionary history does not actually mean that it is necessary beneficial for the long-term health of modern humans.
>
>>In fact, there are valid reasons to suspect that foods to which we are most strongly adapted are actually likely to be detrimental for long-term health, via antagonistic pleiotropy.
>%%TAGS%%
>
^mbq0pgqh9ld


>%%
>```annotation-json
>{"text":">Selection bias. There are also a number of epistemic barriers that challenge inferences about the long-term health value of ancestral foods for modern humans, such as survivorship bias and the selection shadow.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":20417,"end":20688},{"type":"TextQuoteSelector","exact":"So-called ‘‘diseases of modernity” were rare inancestral communities, in contrast to what is now seen in regionswhere Western diets rich in energy-dense foods and (sedentary)lifestyles prevail. In the US, 71% of packaged foods are ultra-processed (Baldridge et al., 2019)","prefix":"ast compatiblewith good health.","suffix":", whereas children in the Anglo-"}]}],"created":"2022-02-28T22:36:18.135Z","updated":"2022-02-28T22:36:18.135Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%ast compatiblewith good health.%%HIGHLIGHT%% ==So-called ‘‘diseases of modernity” were rare inancestral communities, in contrast to what is now seen in regionswhere Western diets rich in energy-dense foods and (sedentary)lifestyles prevail. In the US, 71% of packaged foods are ultra-processed (Baldridge et al., 2019)== %%POSTFIX%%, whereas children in the Anglo-*
>%%LINK%%[[#^mya9wrqvfh|show annotation]]
>%%COMMENT%%
>>Selection bias. There are also a number of epistemic barriers that challenge inferences about the long-term health value of ancestral foods for modern humans, such as survivorship bias and the selection shadow.
>%%TAGS%%
>
^mya9wrqvfh


>%%
>```annotation-json
>{"text":">Strawman. No public health authority is suggesting that healthy diets are defined by the absence of red meat and saturated fat. Rather they are merely single characteristics of broader dietary patterns that have been shown to be health promoting, and these patterns have many other characteristics that contribute to healthfulness.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":21072,"end":21319},{"type":"TextQuoteSelector","exact":"Even if this has been described as a ‘‘paradox”(Cordain et al., 2002a), it mainly indicates that today’s assumptionsabout healthy diets, as being de facto low in red meat and saturatedfat, are flawed and represent a romanticised Western viewpoint.","prefix":"ase (e.g.,Kaplan et al., 2017).","suffix":"To sum up, although animal sourc"}]}],"created":"2022-02-28T22:37:27.632Z","updated":"2022-02-28T22:37:27.632Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%ase (e.g.,Kaplan et al., 2017).%%HIGHLIGHT%% ==Even if this has been described as a ‘‘paradox”(Cordain et al., 2002a), it mainly indicates that today’s assumptionsabout healthy diets, as being de facto low in red meat and saturatedfat, are flawed and represent a romanticised Western viewpoint.== %%POSTFIX%%To sum up, although animal sourc*
>%%LINK%%[[#^pfl8unau5nb|show annotation]]
>%%COMMENT%%
>>Strawman. No public health authority is suggesting that healthy diets are defined by the absence of red meat and saturated fat. Rather they are merely single characteristics of broader dietary patterns that have been shown to be health promoting, and these patterns have many other characteristics that contribute to healthfulness.
>%%TAGS%%
>
^pfl8unau5nb


>%%
>```annotation-json
>{"text":">Appeal to nature. The status of red meat as an evolutionary food is tangential to the question of whether or not red meat increases long-term disease risk in modern populations.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":21319,"end":21601},{"type":"TextQuoteSelector","exact":"To sum up, although animal source foods are primary compo-nents of the Western diet, they are also evolutionary foods towhich the human body is anatomically and metabolically adapted,up to the level of the microbiome (Sholl et al., 2021), and hasalways obtained key nutrients from. ","prefix":"romanticised Western viewpoint.","suffix":"Although further researchmay be"}]}],"created":"2022-02-28T22:40:24.319Z","updated":"2022-02-28T22:40:24.319Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%romanticised Western viewpoint.%%HIGHLIGHT%% ==To sum up, although animal source foods are primary compo-nents of the Western diet, they are also evolutionary foods towhich the human body is anatomically and metabolically adapted,up to the level of the microbiome (Sholl et al., 2021), and hasalways obtained key nutrients from.== %%POSTFIX%%Although further researchmay be*
>%%LINK%%[[#^4zzwqxvz8kx|show annotation]]
>%%COMMENT%%
>>Appeal to nature. The status of red meat as an evolutionary food is tangential to the question of whether or not red meat increases long-term disease risk in modern populations.
>%%TAGS%%
>
^4zzwqxvz8kx


>%%
>```annotation-json
>{"text":">Red herring. Again, this is true of any association. Causal inference is a separate consideration, and the fact that auxiliary hypotheses can be proposed is, again, tangential.\n\n>Potential contradiction. Posited confounders require validation that meets the authors' bar for causal inference. No such definition has been provided, and no evidence was for validation was offered for any potential confounders that were mentioned thus far. That which is stated without evidence can be dismissed without evidence.","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":21601,"end":21907},{"type":"TextQuoteSelector","exact":"Although further researchmay be needed, their role in chronic diseases could as well be amere artefact based on association with the actual damage fromother dietary and lifestyle factors. It is uncertain yet possible thathigh intake of red meat could become problematic in a contempo-rary Western context. ","prefix":"ys obtained key nutrients from.","suffix":"Whereas co-consumption of plants"}]}],"created":"2022-02-28T22:43:47.043Z","updated":"2022-02-28T22:43:47.043Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%ys obtained key nutrients from.%%HIGHLIGHT%% ==Although further researchmay be needed, their role in chronic diseases could as well be amere artefact based on association with the actual damage fromother dietary and lifestyle factors. It is uncertain yet possible thathigh intake of red meat could become problematic in a contempo-rary Western context.== %%POSTFIX%%Whereas co-consumption of plants*
>%%LINK%%[[#^q8a94gcwcqr|show annotation]]
>%%COMMENT%%
>>Red herring. Again, this is true of any association. Causal inference is a separate consideration, and the fact that auxiliary hypotheses can be proposed is, again, tangential.
>
>>Potential contradiction. Posited confounders require validation that meets the authors' bar for causal inference. No such definition has been provided, and no evidence was for validation was offered for any potential confounders that were mentioned thus far. That which is stated without evidence can be dismissed without evidence.
>%%TAGS%%
>
^q8a94gcwcqr


>%%
>```annotation-json
>{"text":">Misunderstanding. After accounting for ~15-year age overestimations, the CVD burden within the Tsimane is comparable to that of the MESA cohort:\n\n>https://immunityageing.biomedcentral.com/articles/10.1186/s12979-019-0165-8\nhttps://gurven.anth.ucsb.edu/sites/secure.lsit.ucsb.edu.anth.d7_gurven/files/sitefiles/papers/horvathetal2016.pdf\nhttps://gurven.anth.ucsb.edu/sites/default/files/sitefiles/papers/irimiaetal2021.pdf\n\n\n>Category error. The category \"chronic disease\" is a superset, including many individual diseases. The only disease investigated in the authors' reference was CVD progression (measured by CAC).","target":[{"source":"vault:/1-s2.0-S1751731122000040-main.pdf","selector":[{"type":"TextPositionSelector","start":20912,"end":21071},{"type":"TextQuoteSelector","exact":"More-over, contemporary cultures that have maintained traditional dietsand lifestyles typically have low burdens of chronic disease (e.g.,Kaplan et al., 2017).","prefix":"d sodas (Khandpur et al. 2020).","suffix":"Even if this has been described"}]}],"created":"2022-02-28T22:49:16.747Z","updated":"2022-02-28T22:49:16.747Z","document":{"title":"Animal board invited review: Animal source foods in healthy, sustainable, and ethical diets â€“ An argument against drastic limitation of livestock in the food system","link":[{"href":"urn:x-pdf:ea3d03a8cbfbced4b1a564c96dbd2637"},{"href":"vault:/1-s2.0-S1751731122000040-main.pdf"}],"documentFingerprint":"ea3d03a8cbfbced4b1a564c96dbd2637"},"uri":"vault:/1-s2.0-S1751731122000040-main.pdf"}
>```
>%%
>*%%PREFIX%%d sodas (Khandpur et al. 2020).%%HIGHLIGHT%% ==More-over, contemporary cultures that have maintained traditional dietsand lifestyles typically have low burdens of chronic disease (e.g.,Kaplan et al., 2017).== %%POSTFIX%%Even if this has been described*
>%%LINK%%[[#^g60gbu0n9n|show annotation]]
>%%COMMENT%%
>>Misunderstanding. After accounting for ~15-year age overestimations, the CVD burden within the Tsimane is comparable to that of the MESA cohort:
>
>>https://immunityageing.biomedcentral.com/articles/10.1186/s12979-019-0165-8
>https://gurven.anth.ucsb.edu/sites/secure.lsit.ucsb.edu.anth.d7_gurven/files/sitefiles/papers/horvathetal2016.pdf
>https://gurven.anth.ucsb.edu/sites/default/files/sitefiles/papers/irimiaetal2021.pdf
>
>
>>Category error. The category "chronic disease" is a superset, including many individual diseases. The only disease investigated in the authors' reference was CVD progression (measured by CAC).
>%%TAGS%%
>
^g60gbu0n9n
