https://pubmed.ncbi.nlm.nih.gov/29470825/

"The results of the present systematic review and meta-analysis suggest a significant effect of RT frequency as higher training frequencies are translated into greater muscular strength gains. However, these effects seem to be primarily driven by training volume because when the volume is equated, there was no significant effect of RT frequency on muscular strength gains. Thus, from a practical standpoint, greater training frequencies can be used for additional RT volume, which is then likely to result in greater muscular strength gains. However, it remains unclear whether RT frequency on its own has significant effects on strength gain. It seems that higher RT frequencies result in greater gains in muscular strength on multi-joint exercises in the upper body and in women, and, finally, in contrast to older adults, young individuals seem to respond more positively to greater RT frequencies. More evidence among resistance-trained individuals is needed as most of the current studies were performed in untrained participants."

#fitness
#exercise
#hypertrophy
#strength 