https://pubmed.ncbi.nlm.nih.gov/7457208/

"Rural inhabitants of Upernavik in Greenland still experience cancer and coronary heart disease."

https://pubmed.ncbi.nlm.nih.gov/7457208/

"Alaskan natives living traditional lifestyles still get cancer."

https://pubmed.ncbi.nlm.nih.gov/12535749/

"Canadian, Alaskan, and Greenland Inuit still experience coronary heart disease."

https://pubmed.ncbi.nlm.nih.gov/2206175/

"Greenland Inuit populations still experience coronary heart disease."

https://pubmed.ncbi.nlm.nih.gov/31880790/

"Greenland Inuit mummies from the 16th century still had coronary heart disease."

#disease
#primitive_cultures 
#ancestral_food 
#coronary_heart_disease
#coronary_artery_calcification
#cancer
#antagonistic_pleiotropy