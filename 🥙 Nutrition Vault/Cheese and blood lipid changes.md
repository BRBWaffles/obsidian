de Goede, Janette, et al. ‘Effect of Cheese Consumption on Blood Lipids: A Systematic Review and Meta-Analysis of Randomized Controlled Trials’. _Nutrition Reviews_, vol. 73, no. 5, May 2015, pp. 259–75. _PubMed_, https://doi.org/10.1093/nutrit/nuu060.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/26011901/

****


**Conclusions:**
>The consumption of hard cheese results in lower LDLC and HDL-C levels than the consumption of butter, despite a similar P/S ratio. Further research is warranted to determine whether calcium, specific types of SFAs in cheese, or effects of the food matrix could explain this finding.

**PDF:**
[[📂 Media/PDFs/nutrit%2Fnuu060.pdf]]

**Supplements:**
[[📂 Media/Misc/Goede_Supplements_S1.docx]]

[[📂 Media/Misc/Goede_Supplements_S2-prismachecklist.doc]]

[[📂 Media/Misc/Goede_Supplements_S3.docx]]

| **Endpoints** | **Exposures** | **Populations** | **General**      | **People** |
| ------------- | ------------- | --------------- | ---------------- | ---------- |
| #LDL          | #cheese       | #humans         | #clinical_trials |            |
| #HDL          |               |                 | #disease         |            |
| #blood_lipids |               |                 | #nutrition       |            |
|               |               |                 |                  |            |
|               |               |                 |                  |            |
|               |               |                 |                  |            |
|               |               |                 |                  |            |
|               |               |                 |                  |            |
|               |               |                 |                  |            |

****