https://pubmed.ncbi.nlm.nih.gov/31105044/

"Limiting consumption of ultra-processed foods may be an effective strategy for obesity prevention and treatment."

https://pubmed.ncbi.nlm.nih.gov/20613890/

"Ingestion of the particular PF meal tested in this study decreases postprandial energy expenditure by nearly 50% compared with the isoenergetic WF meal. This reduction in daily energy expenditure has potential implications for diets comprised heavily of PFs and their associations with obesity."

https://pubmed.ncbi.nlm.nih.gov/23097268/

#disease 
#obesity 
#processed_food
#energy_intake
#weight_gain 
#weight_loss
#anthropometics 
#blood_glucose 
#blood_lipids 
#LDL 
#HDL 
#triglycerides 
#energy_expenditure
#carbohydrates 
#nutrition 