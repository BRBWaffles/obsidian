‘TEA of Cultivated Meat. Future Projections for Different Scenarios’. _CE Delft - EN_. Accessed 19 Feb. 2022.

**Link:**
https://cedelft.eu/publications/tea-of-cultivated-meat/

**Carbon Footprint of Cultured Meat:**
![[Pasted image 20220219150549.png]]

**Conclusions:**
>We conclude that CM can offer environmental gains compared to conventional meats (beef, pork, chicken). CM uses much less land compared to all conventional meats. It also has a much lower carbon footprint than beef and is comparable to current global average footprints for pork and chicken when produced using conventional energy. When using sustainable energy, CM has a lower carbon footprint than ambitious production benchmarks for all conventional meats.
>- CM can compete with all conventional meat environmentally, and scores much better than beef. This conclusion is based on comparison with our ambitious benchmark for conventional products, and therefore quite robust. When producers switch to sustainable energy, CM becomes the most environmentally friendly option for meat production.
>- The most important drivers of the environmental impact of CM are processing energy, medium quantity and medium composition. For all these drivers an important option for improvement is a switch sustainable energy.
>- Potential variability regarding quantity of medium used, residence time of cells in the reactors, maximum cell density and process temperature are unlikely to influence these conclusions.

**PDF:**
[[📂 Media/PDFs/CE_Delft_190107_LCA_of_cultivated_meat_Def.pdf]]

| **Topics**     | **General**   |
| -------------- | ------------- |
| #cultured_meat | #nutrition    |
| #agriculture   | #vegan        |
|                | #animal_foods | 

****