https://pubmed.ncbi.nlm.nih.gov/30060746/

"These data demonstrate that WG and FV intake can have positive effects on metabolic health; however, different markers of inflammation were reduced on each diet suggesting that the anti-inflammatory effects were facilitated via different mechanisms. The anti-inflammatory effects were not related to changes in gut microbiota composition during the intervention, but were correlated with microbiota composition at baseline."

https://pubmed.ncbi.nlm.nih.gov/26950143/

"Cereal fibers have been studied more than any other kind in relation to regularity. This is the first comprehensive review comparing the effects of the three major food sources of fiber on bowel function and regularity since 1993."

#whole_grains 
#fruit 
#vegetables 
#inflammation 
#microbiome
#constipation 
#plant_foods 
#nutrition