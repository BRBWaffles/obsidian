https://pubmed.ncbi.nlm.nih.gov/24886626/

"Olive oil consumption, specifically the extra-virgin variety, is associated with reduced risks of cardiovascular disease and mortality in individuals at high cardiovascular risk."

#disease
#olive_oil
#nuts
#cardiovascular_disease
#cancer
#all_cause_mortality
#monounsaturated_fat
#polyunsaturated_fat
#saturated_fat
#blood_lipids
#mediterranean_diet 
#nutrition