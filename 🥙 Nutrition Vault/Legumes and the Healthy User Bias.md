Carnivore diet clown, Paul Saladino, has previously stated on numerous occasions that legumes are toxic, and that the association between higher legumes intake and lower disease risk isn't trustworthy. His primary justification for this claim is the healthy user bias, which Paul grossly misunderstands.

The healthy user bias is actually a type of selection bias that characterizes a tendency for certain demographics of people to participate in research over other demographics. What Paul is referring to is actually a statistical concept called multicollinearity. In this case, multicollinearity refers to either good or bad habits tending to correlate together. For example, if legume intake tends to correlate with lower smoking rates and lower sedentariness, then the true effect of legumes on a particular disease endpoint could be obscured and difficult to determine.

It's actually true that there is evidence of some multicollinearity in much of the literature on this subject. Typically higher intakes of legumes correlate with lower rates of unhealthy habits. But, not always. There are instances wherein legume intake doesn't also correlate with other healthy behaviours in any particularly robust way.

Typically, the Japanese are a good population for investigating these relationships with little potential for bias from things like extreme multicollinearity. This seems to be because dietary patterns within that population tend to be very homogenous. The Japanese culture also doesn't seem to suffer from a lot of the food stigmas that can lead to the same sorts of selection biases that we find in the West.

A study published by Nagura et al. (2009) investigated the relationship between bean consumption and total mortality in men and women, ages 40 to 79 [[1](https://pubmed.ncbi.nlm.nih.gov/19138438/)]. The follow-up time, participant numbers, and event rates were very impressive. Validated food frequency questionnaires were used to collect the dietary input data as well. This is the sort of data that nutritional epidemiologists love, because it checks all the boxes for high statistical power and strong internal validity.

Overall, from the lowest to highest bean intake, there were no particularly meaningful differences in BMI, smoking rates, physical activity, sleep alcohol consumption, or education. The rates of hypertension, diabetes, or mental stress also not terribly different. Other dietary covariates such as cholesterol, saturated fat, and omega-3 fatty acids were also largely balanced. In fact, higher bean consumption was associated with an increase in cholesterol and saturated fat intake, but not to any appreciable degree.

When only adjusting for age and sex, bean intake was associated with a statistically significant 14% decreased risk of dying from any cause. After adjustment for other risk factors, as well as dietary and lifestyle covariates, there was a statistically significant 10% decrease in the risk of total mortality. The fully adjusted model further accounted for fruit and vegetable intake and found a statistically significant 8% reduction in the risk of all-cause mortality.

Additionally, many common exposures that are considered both healthy and unhealthy are usually included in the multivariable adjustment models of prospective epidemiological investigations. If there was extreme multicollinearity, this would drive up the variance inflation factor and render the results more likely to be non-significant. But rather we see tight confidence intervals and statistical significance, which is evidence of independence rather than extreme multicollinearity. So, there is actually very little evidence for the sort of interactions that Paul is talking about.

Eat that, Saladino. ¯\_(ツ)_/¯

**Key points:**

-   Neither the healthy user bias nor extreme multicollinearity can explain the association between legumes and all-cause mortality.

**References:**

[1] [https://pubmed.ncbi.nlm.nih.gov/19138438/](https://pubmed.ncbi.nlm.nih.gov/19138438/)

#nutrition 
#legumes 
#nutrition 
#healthy_user_bias 
#multicollinearity 
#all_cause_mortality 