Peters, John C., et al. ‘The Effects of Water and Non-Nutritive Sweetened Beverages on Weight Loss during a 12-Week Weight Loss Treatment Program’. _Obesity (Silver Spring, Md.)_, vol. 22, no. 6, June 2014, pp. 1415–21. _PubMed_, https://doi.org/10.1002/oby.20737.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/24862170/

**Weight Loss:**
![[📂 Media/PDFs/Pasted image 20220222184505.png]]

**Conclusions:**
>These results strongly suggest that NNS beverages can be part of an effective weight loss strategy and individuals who desire to consume them should not be discouraged from doing so because of concerns that they will undermine short-term weight loss efforts. A longer term follow-up of this randomized cohort, now underway, will clarify the utility of NNS beverages in weight loss maintenance.

**PDF:**
[[📂 Media/PDFs/oby.20737.pdf]]

| **Endpoints**   | **Exposures**          | **Populations** | **General** | **People** |
| --------------- | ---------------------- | --------------- | ----------- | ---------- |
| #weight_loss    | #artificial_sweeteners | #humans         | #nutrition  |            |
| #anthropometics |                        | #overweight     | #disease    |            |
| #body_weight    |                        |                 | #novel_food |            |
| #energy_intake  |                        |                 |             |            |

****