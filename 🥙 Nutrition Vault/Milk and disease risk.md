https://pubmed.ncbi.nlm.nih.gov/23340316/

"The results of Trial 1 demonstrate an association between CFC and cow's milk consumption but Trial 2 failed to show an effect from type of casein. Some other component in cow's milk common to both A1 and A2 milk may be causing a problem in these susceptible children."

#disease
#constipation 
#milk 
#dairy
#casein
#children 
#animal_fats
#animal_foods 
#animal_protein 
#nutrition