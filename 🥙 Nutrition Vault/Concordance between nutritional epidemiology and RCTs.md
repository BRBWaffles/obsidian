https://pubmed.ncbi.nlm.nih.gov/34526355/

"On average, the difference in pooled results between estimates from BoE(RCT) and BoE(CS) was small. But wide prediction intervals and some substantial statistical heterogeneity in cohort studies indicate that important differences or potential bias in individual comparisons or studies cannot be excluded. Observed differences were mainly driven by dissimilarities in population, intervention or exposure, comparator, and outcome. These findings could help researchers further understand the integration of such evidence into prospective nutrition evidence syntheses and improve evidence based dietary guidelines."

https://pubmed.ncbi.nlm.nih.gov/23700648/

"The examined quantitative characteristics of the citation maps in each association cannot predict the probability that the findings from the two designs agree or disagree. It is unclear whether there is a good way to describe the maturity of the evidence base on an association between nutrients and outcomes. At a minimum, purely bibliometric approaches are not a good way to prioritize which nutrient exposures merit further study, and for which health outcomes."

https://pubmed.ncbi.nlm.nih.gov/34308960/

![[📂 Media/PDFs/Schwingshackled_Supplement.pdf]]

https://pubmed.ncbi.nlm.nih.gov/11134917/

[[📂 Media/PDFs/Evaluating_concordance_of_bodies_of_evidence_from_randomized_controlled_trials_dietary_intake_and_biomarkers_of_intake_in_cohort_studies__a_meta-epidemiological_study_Beyerbach_2021.pdf]]

#epidemiology 
#nutrition_science
#nutrition 