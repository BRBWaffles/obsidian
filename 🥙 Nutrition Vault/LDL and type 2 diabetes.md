https://pubmed.ncbi.nlm.nih.gov/32040442/

"The relationship between LDL-C and Type 1 diabetes mellitus (T1DM) with this analysis producing negative pooled results (Beta value -0.202, 95%CI -2.888~2.484, P-value=0.883)."

https://pubmed.ncbi.nlm.nih.gov/31995593/

"The study findings suggest that triglycerides may causally impact various glycemic traits. However, the findings need to be replicated in larger studies."

#disease 
#LDL 
#blood_glucose 
#blood_lipids 
#type_2_diabetes 
#mendelian_randomization
#lipidology 