https://www.nature.com/articles/s41586-021-04224-5

"We demonstrate the universality and high frequency of oncogenic phenomena in mammals and reveal substantial diferences in cancer mortality across major mammalian orders. We show that the phylogenetic distribution of cancer mortality is associated with diet, with carnivorous mammals (especially mammal-consuming ones) facing the highest cancer-related mortality."

#cancer
#meat
#animals
#antagonistic_pleiotropy
#disease
#evolution
#nutrition 
