https://pubmed.ncbi.nlm.nih.gov/29518203/

"The inverse BMI-lung cancer association is not entirely due to smoking and reverse causation. Central obesity, particularly concurrent with low BMI, may help identify high-risk populations for lung cancer."

#disease
#obesity
#lung_cancer
#anthropometics