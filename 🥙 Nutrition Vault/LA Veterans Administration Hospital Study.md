**Eight Year Report:**
https://www.ahajournals.org/doi/10.1161/01.CIR.40.1S2.II-1

**Five Year Report:**
https://www.jlr.org/article/S0022-2275(20)39591-2/pdf

**Cancer Outcomes:**
https://pubmed.ncbi.nlm.nih.gov/4100347/

**Final Report:**
https://pubmed.ncbi.nlm.nih.gov/4189785/

**Diet Design:**
https://pubmed.ncbi.nlm.nih.gov/13907771/

**Gallstone Risk:**
https://pubmed.ncbi.nlm.nih.gov/4681896/

#nutrition 
#classic_dietary_fat_trials 
#disease 
#coronary_heart_disease 
#animal_fats 
#plant_fats 
#dietary_fat 
#polyunsaturated_fat 
#saturated_fat 
