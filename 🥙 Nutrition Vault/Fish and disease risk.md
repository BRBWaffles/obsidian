https://pubmed.ncbi.nlm.nih.gov/29317009/

"There was potential evidence of regional differences in the association between fish consumption and mortality. It may be helpful to examine the associations by considering types of fish consumed and methods of fish preparation."

https://pubmed.ncbi.nlm.nih.gov/21914258/

"Our results indicate that either low (1 serving/week) or moderate fish consumption (2-4 servings/week) has a significantly beneficial effect on the prevention of CHD mortality. High fish consumption (>5 servings/week) possesses only a marginally protective effect on CHD mortality, possibly due to the limited studies included in this group."

https://pubmed.ncbi.nlm.nih.gov/23788668/

"No associations with cancer or ischemic heart disease mortality were observed. Further analyses suggested that the inverse associations with total, ischemic stroke, and diabetes mortality were primarily related to consumption of saltwater fish and intake of long-chain n-3 fatty acids. Overall, our findings support the postulated health benefits of fish consumption."

https://pubmed.ncbi.nlm.nih.gov/23489806/

"This meta-analysis suggests that there is a dose-dependent inverse relationship between fish consumption and HF incidence. Fish intake once or more times a week could reduce HF incidence."

#disease
#fish
#cardiovascular_disease
#coronary_heart_disease
#heart_failure
#cancer #stroke
#type_2_diabetes
#all_cause_mortality
#docosahexaenoic_acid
#eicosapentaenoic_acid
#omega_3
#animal_foods
#animal_fats 
#animal_protein 
#nutrition