https://pubmed.ncbi.nlm.nih.gov/30061161/

"Chocolate consumption may be associated with reduced risk of CVD at <100 g/week consumption. Higher levels may negate the health benefits and induce adverse effects associated with high sugar consumption."

https://pubmed.ncbi.nlm.nih.gov/25646334/

"Our data support an inverse relation of chocolate intake with incident DM, which appears only to apply in younger and normal-body weight men after controlling for comprehensive life styles including total energy consumption."

https://pubmed.ncbi.nlm.nih.gov/28671591/

"In conclusion, chocolate intake is associated with decreased risks of CHD, stroke, and diabetes. Consuming chocolate in moderation (≤6 servings/week) may be optimal for preventing these disorders."

https://pubmed.ncbi.nlm.nih.gov/28324761/

"Findings from this large Japanese cohort supported a significant inverse association between chocolate consumption and risk of developing stroke in women. However, residual confounding could not be excluded as an alternative explanation for our findings."

https://pubmed.ncbi.nlm.nih.gov/21559039/

"These data are consistent with beneficial effects of dark chocolate/cocoa products on total and LDL cholesterol and no major effects on HDL and TG in short-term intervention trials."

#disease
#cardiovascular_disease
#type_2_diabetes
#stroke
#plant_fats
#chocolate
#blood_lipids
#LDL
#HDL
#triglycerides
#cholesterol
#plant_fats 
#plant_foods 
#fermented_food 
#processed_food 
#nutrition