Sukino, Shin, et al. ‘Effects of a Low Advanced Glycation End Products Diet on Insulin Levels: The Feasibility of a Crossover Comparison Test’. _Journal of Clinical Medicine Research_, vol. 10, no. 5, May 2018, pp. 405–10. _PubMed_, https://doi.org/10.14740/jocmr3301w.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/29581803/

**Between Group Results:**
![[📂 Media/PDFs/Pasted image 20220219170000.png]]

**Conclusions:**
>This is the first study to investigate the effects of a low-AGE diet on insulin levels in non-diabetic Asian subjects. As a result, although the AUC results for insulin levels did not reach significance between the normal and low-AGE diet groups, the ES was large, whereas the plasma glucose level did not significantly differ between the two groups

**PDF:**
![[📂 Media/PDFs/jocmr-10-405.pdf]]

| **Endpoints**  | **Exposures** | **Populations** | **General** | **People** |
| -------------- | ------------- | --------------- | ----------- | ---------- |
| #insulin       | #advanced_glycation_endproducts         | #humans         | #nutrition  |            |
| #blood_glucose | #fried_foods  | #asia           | #disease    |            |
|                |               |                 | #type_2_diabetes       |            |

****