de Lorgeril, M., et al. ‘Mediterranean Alpha-Linolenic Acid-Rich Diet in Secondary Prevention of Coronary Heart Disease’. _Lancet (London, England)_, vol. 343, no. 8911, June 1994, pp. 1454–59. _PubMed_, https://doi.org/10.1016/s0140-6736(94)92580-1.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/7911176/

**Nutrient Intake:**
![[Pasted image 20220221142624.png]]

**Plasma Fatty Acids:**
![[Pasted image 20220221142659.png]]

**Food Intake:**
![[Pasted image 20220221142719.png]]

**Outcomes:**
![[Pasted image 20220221142744.png]]

**Conclusions:**
>The rapid protective effect and similarity of serum lipids in our 2 groups suggest that the protective effect of the experimental diet could be through thrombogenesis since the incidence of myocardial infarction was markedly reduced. The fact that no sudden death occurred in the experimental group against 8 in the control group, suggests a possible additional antiarrhythmic effect, consistent with observations in man,6,24 and animals30 indicating that n-3 fatty acids, especially alpha-linolenic acid, markedly reduced the incidence of lethal arrythmias.

**PDF:**
[[📂 Media/PDFs/s0140-6736%2894%2992580-1 (1).pdf]]

| **Endpoints** | **Exposures**   | **Populations** | **General**                 | **People** |
| ------------- | --------------- | --------------- | --------------------------- | ---------- |
| #coronary_heart_disease          | #animal_fats    | #humans         | #secondary_prevention       |            |
|               | #meat           |                 | #disease                    |            |
|               | #legumes        |                 | #mediterranean_diet         |            |
|               | #plant_fats     |                 | #classic_dietary_fat_trials |            |
|               | #animal_protein |                 | #blood_lipids               |            |
|               | #red_meat       |                 | #nutrition                  |            |
|               | #fruit          |                 |                             |            |
|               | #plant_protein  |                 |                             |            |
|               | #bread          |                 |                             |            |
|               | #canola_oil     |                 |                             |            |
|               | #monounsaturated_fat           |                 |                             |            |
|               | #polyunsaturated_fat           |                 |                             |            |
|               | #dietary_fat    |                 |                             |            |
|               | #saturated_fat            |                 |                             |            |
|               | #animal_foods   |                 |                             |            |
|               | #plant_foods    |                 |                             |            |
|               | #linoleic_acid  |                 |                             |            |

****
