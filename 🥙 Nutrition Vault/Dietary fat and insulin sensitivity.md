https://pubmed.ncbi.nlm.nih.gov/11237931/

"A high-fat, low-carbohydrate intake reduces the ability of insulin to suppress endogenous glucose production and alters the relation between oxidative and nonoxidative glucose disposal in a way that favors storage of glucose."

https://pubmed.ncbi.nlm.nih.gov/15310747/

"These data demonstrate that the decreased glucose disposal during the OGTT after the 56-h HF/LC diet was in part related to decreased oxidative carbohydrate disposal in skeletal muscle and not to decreased glycogen storage. The rapid increase in PDK activity during the HF/LC diet appeared to account for the reduced potential for oxidative carbohydrate disposal."

https://pubmed.ncbi.nlm.nih.gov/11679437/

"The data from the lipid infusion protocol suggest a functional relationship between IMCL levels and insulin sensitivity. Similar effects could be induced by a high-fat diet, thereby underlining the physiological relevance of these observations."

https://pubmed.ncbi.nlm.nih.gov/17062764/

"Upregulation of muscle PDK4 expression was an early molecular adaptation to these changes, and we showed for the first time in healthy humans, unlike insulin-resistant individuals, that insulin can suppress PDK4 but not PDK2 gene expression in skeletal muscle."

https://pubmed.ncbi.nlm.nih.gov/19017772/

"African American and Caucasian adolescents respond to FFA elevation similarly through increased fasting insulin secretion to maintain fasting glucose homeostasis and reduced peripheral glucose uptake and insulin resistance. Thus, African American adolescents are not more susceptible to FFA-induced insulin resistance than Caucasian youth."

https://pubmed.ncbi.nlm.nih.gov/27434027/

"This meta-analysis of randomised controlled feeding trials provides evidence that dietary macronutrients have diverse effects on glucose-insulin homeostasis. In comparison to carbohydrate, SFA, or MUFA, most consistent favourable effects were seen with PUFA, which was linked to improved glycaemia, insulin resistance, and insulin secretion capacity."

https://pubmed.ncbi.nlm.nih.gov/28758920/

"In conclusion, a single day of high-fat, overfeeding impaired whole-body insulin sensitivity in young, healthy adults. This highlights the rapidity with which excessive consumption of calories through high-fat food can impair glucose metabolism, and suggests that acute binge eating may have immediate metabolic health consequences for the individual."

https://pubmed.ncbi.nlm.nih.gov/32815226/

"The observation that those conditions are accompanied by mitochondrial dysfunction has set the basis to propose a link between mitochondrial dysfunction, metabolic inflexibility, and metabolic health. We here highlight the evidence about the notion that MetF influences metabolic health."

#nutrition 
#dietary_fat 
#plant_fats 
#animal_fats 
#insulin_sensitivity 
#insulin 
#blood_glucose 
#low_carb 