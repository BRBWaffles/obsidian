https://rss.onlinelibrary.wiley.com/doi/pdf/10.1111/j.1740-9713.2011.00506.x

"We have found 12 papers in which claims coming from observational studies were tested in randomised clinical trials. Many of the trials are quite large. In most of the observational studies multiple claims were tested, often in factorial designs, e.g. vitamin D and calcium individually and together along with a placebo group. Note that none of the claims replicated in the direction claimed in the observational studies and that there was statistical significance in the opposite direction five times"

#nutrition 
#epidemiology 
#clown_papers
#clownery 
