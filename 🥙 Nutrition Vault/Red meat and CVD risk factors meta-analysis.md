Guasch-Ferré, Marta, et al. ‘Meta-Analysis of Randomized Controlled Trials of Red Meat Consumption in Comparison With Various Comparison Diets on Cardiovascular Risk Factors’. _Circulation_, vol. 139, no. 15, Apr. 2019, pp. 1828–45. _PubMed_, https://doi.org/10.1161/CIRCULATIONAHA.118.035225.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/30958719/

**Forest Plot of Protein Sources on LDL:**
![[Pasted image 20220214161941.png]]

**Summary Effects of Proteins Sources:**
![[Pasted image 20220214162016.png]]

**Summary of Plant Foods:**
![[Pasted image 20220214162138.png]]

**Conclusions:**
>In conclusion, previously noted inconsistencies regarding the effects of red meat on CVD risk factors may be attributable, in part, to the composition of the comparison diet. Findings from the present systematic review and meta-analysis showed that total red meat intake did not differentially influence blood lipids and apolipoproteins, with the exception of triglycerides, when all comparison diets were analyzed together. In comparison with red meat, consumption of high-quality plant protein sources (ie, soy, nuts, and legumes) leads to more favorable changes in blood concentrations of total cholesterol and LDL-C. Future interventions should consider appropriate comparison foods when examining the effects of red meat intake on cardiovascular risk factors.

**PDF:**
[[📂 Media/PDFs/CIRCULATIONAHA.118.035225.pdf]]

**Supplements:**
[[📂 Media/PDFs/circ_circulationaha-2018-035225_supp1.pdf]]

| **Endpoints**      | **Exposures**   | **Populations** | **General**        | **People**          |
| ------------------ | --------------- | --------------- | ------------------ | ------------------- |
| #blood_lipids      | #red_meat       | #humans         | #disease           | #marta_guasch_ferre |
| #LDL               | #animal_foods   |                 | #cardiovascular_disease               |                     |
| #HDL               | #animal_protein |                 | #nutrition         |                     |
| #triglycerides                | #plant_foods    |                 | #meta_analysis     |                     |
| #serum_cholesterol | #plant_protein  |                 | #systematic_review |                     |
|                    | #meat           |                 |                    |                     |

****