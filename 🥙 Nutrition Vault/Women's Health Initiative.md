https://pubmed.ncbi.nlm.nih.gov/31533514/

"Conclusions Dietary patterns that emphasize higher intake of fruits, vegetables, whole grains, nuts/seeds, legumes, a high unsaturated:saturated fat ratio, and lower intake of red and processed meats, added sugars, and sodium are associated with lower CVD risk in postmenopausal women with type 2 diabetes mellitus."

https://pubmed.ncbi.nlm.nih.gov/18562168/

https://pubmed.ncbi.nlm.nih.gov/16467234/

#womens_health_initiative
#nutrition 
#disease  
#fibre 
#soluble_fibre
#insoluble_fibre
#inflammation 
#interleukin_6 
#tumor_necrosis_factor 
#c_reactive_protein 
#cardiovascular_disease 
#coronary_heart_disease 
#red_meat 
#processed_meat 
#sugar 
#sodium 
#saturated_fat 
#polyunsaturated_fat 
#monounsaturated_fat 
#fruit 
#vegetables 
#whole_grains 
#nuts 
#paleo_diet
#ancestral_food 