https://pubmed.ncbi.nlm.nih.gov/31161217/

"Effects of red meat, white meat, and nonmeat protein sources on atherogenic lipoprotein measures in the context of low compared with high saturated fat intake: a randomized controlled trial"



#disease 
#cardiovascular_disease
#meat 
#white_meat
#red_meat 
#saturated_fat 
#blood_lipids 
#LDL 
#HDL 
#triglycerides
#ApoB 
#ApoA1
#plant_fats 
#plant_protein
#animal_fats
#animal_foods
#animal_protein
#nutrition