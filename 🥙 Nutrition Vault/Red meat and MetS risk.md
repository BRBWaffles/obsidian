Becerra-Tomás, Nerea, et al. ‘Replacing Red Meat and Processed Red Meat for White Meat, Fish, Legumes or Eggs Is Associated with Lower Risk of Incidence of Metabolic Syndrome’. _Clinical Nutrition (Edinburgh, Scotland)_, vol. 35, no. 6, Dec. 2016, pp. 1442–49. _PubMed_, https://doi.org/10.1016/j.clnu.2016.03.017.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/27087650/

**Red Meat and MetS Risk:**
![[Pasted image 20220214164236.png]]

**Red Meat and MetS Markers:**
![[Pasted image 20220214164357.png]]

**Substitution Analysis:**
![[Pasted image 20220214164830.png]]

**Conclusions:**
>In conclusion, the present study suggests that total meat (when consumed to a level of around more than one serving/day), RM and PRM promote MetS development. In contrast, poultry consumption is associated with a lower risk of MetS. The substitution of other protein-rich foods for RM or PRM is also associated with a lower risk of MetS. Therefore, replacing RM and PRM by other healthy foods should be recommended to decrease the risk of MetS in individuals at high cardiovascular risk. Further studies are warranted to confirm these findings and elucidate the possible mechanisms involved.

**PDF:**
[[📂 Media/PDFs/becerra-cln-repl 1.pdf]]

| **Endpoints**   | **Exposures**   | **Populations** | **General**            | **People** |
| --------------- | --------------- | --------------- | ---------------------- | ---------- |
| #metabolic_syndrome           | #red_meat       | #PREDIMED       | #disease               |            |
| #abdominal_fat  | #meat           | #humans         | #epidemiology          |            |
| #triglycerides             | #processed_meat | #spain          | #substitution_analyses |            |
| #HDL            | #eggs           |                 | #nutrition             |            |
| #blood_pressure | #legumes        |                 | #mediterranean_diet    |            |
| #blood_glucose  | #fish           |                 |                        |            |
|                 | #poultry        |                 |                        |            |
|                 | #animal_foods   |                 |                        |            |
|                 | #plant_foods    |                 |                        |            |

****