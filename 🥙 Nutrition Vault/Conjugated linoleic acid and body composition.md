Onakpoya, Igho J., et al. ‘The Efficacy of Long-Term Conjugated Linoleic Acid (CLA) Supplementation on Body Composition in Overweight and Obese Individuals: A Systematic Review and Meta-Analysis of Randomized Clinical Trials’. _European Journal of Nutrition_, vol. 51, no. 2, Mar. 2012, pp. 127–34. _PubMed_, https://doi.org/10.1007/s00394-011-0253-9.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/21990002/

**CLA and Body Composition:**
![[Pasted image 20220220102348.png]]

**Dose-Response:**
![[Pasted image 20220220102405.png]]

**Conclusions:**
>The evidence from RCTs fails to convincingly demonstrate that CLA supplementation generates any clinically relevant effects on body composition on the long term.

**PDF:**
[[📂 Media/PDFs/s00394-011-0253-9.pdf]]

| **Endpoints**   | **Exposures**  | **Populations** | **General** | **People** |
| --------------- | -------------- | --------------- | ----------- | ---------- |
| #anthropometics | #conjugated_linoleic_acid           | #humans         | #nutrition  |            |
| #weight_loss    | #linoleic_acid |                 | #disease    |            |
| #weight_gain    |                |                 | #overweight |            |
|                 |                |                 | #obesity    |            |

****