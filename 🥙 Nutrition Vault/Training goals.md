https://www.strongerbyscience.com/realistic-training-goals/

"Setting good goals and having reasonable expectations about rates of progress is important, _especially_ for people who are new to lifting and don’t have an experience base to draw from.  I wasn’t aware of any other resources that actually laid out reasonable rates of progress and long-term goals to shoot for based on any real data (plenty of people have given recommendations based solely on number they pulled out of thin air), so I figured I’d whip one up to kick off this new year."

#exercise 
#fitness 
#self_improvement
#strength 