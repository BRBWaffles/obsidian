https://pubmed.ncbi.nlm.nih.gov/31528179/

"Unlike the obvious changes in other T cell subsets with age and gender, the stable level of TSCM in peripheral blood may support their capacity for sustaining long-term immunological memory, while their importance may increase together with aging."

#antagonistic_pleiotropy
#disease 
#senescence
#evolution
#aging