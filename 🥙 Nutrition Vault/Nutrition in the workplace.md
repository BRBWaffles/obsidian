Melián-Fleitas, Liliana, et al. ‘Influence of Nutrition, Food and Diet-Related Interventions in the Workplace: A Meta-Analysis with Meta-Regression’. _Nutrients_, vol. 13, no. 11, Nov. 2021, p. 3945. _www.mdpi.com_, https://doi.org/10.3390/nu13113945.

**Link:**
https://www.mdpi.com/2072-6643/13/11/3945

**Conclusions:**
>Given that most people spend a large part of their time in the workplace and, therefore, eat at least one of their daily meals there, well-planned interventions—preferably including several strategies—have been demonstrated, in general, as useful for combating overweight and obesity. From the meta-regression study, it was observed that the interventions give better results in people who presented high Body Mass Index (BMI) values (obesity). In contrast, intervention 2 (interventions related to workplace environment) would not give the expected results (it would increase the BMI).

**PDF:**
![[📂 Media/PDFs/nutrients-13-03945.pdf]]

**Supplements:**


| **Endpoints**     | **Exposures**        | **Populations** | **General**           | **People** |
| ----------------- | -------------------- | --------------- | --------------------- | ---------- |
| #body_weight      | #workplace_nutrition | #humans         | #socioeconomic_status |            |
| #visceral_adipose |                      |                 | #disease              |            |
| #BMI              |                      |                 | #nutrition            |            |
| #blood_lipids     |                      |                 |                       |            |
| #blood_pressure   |                      |                 |                       |            |
| #metabolic_syndrome             |                      |                 |                       |            |
| #hba1c            |                      |                 |                       |            |
|                   |                      |                 |                       |            |
|                   |                      |                 |                       |            |

****

Geaney, Fiona, et al. ‘The Effect of Complex Workplace Dietary Interventions on Employees’ Dietary Intakes, Nutrition Knowledge and Health Status: A Cluster Controlled Trial’. Preventive Medicine, vol. 89, Aug. 2016, pp. 76–83. PubMed, https://doi.org/10.1016/j.ypmed.2016.05.005.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/27208667/

**Conclusions:**
>The FCW study has shown that a well-structured complex workplace dietary intervention that combines nutrition education and environmental dietary modification reduces employees’ dietary intakes of salt and saturated fat, improves their nutrition knowledge and decreases their BMI at 7-9 months follow-up. This study provides critical evidence on the effectiveness of complex workplace dietary interventions in a manufacturing working population. The FCW combined dietary intervention is scalable and wide scale implementation should be considered in local, national and international workplaces. At a more global level, the increasing prevalence of NCDs is one of the dominant public health issues of our time. It is likely that the WHO will not reach their specific targets (2% per year reduction in NCD deaths and a halt in the increase of obesity and type 2 diabetes in adults and adolescents) without positive changes to our food environments at local, national and transnational levels because obesogenic food environments are the main drivers of the obesity epidemic and of the increasing prevalence of diet-related NCDs

**PDF:**
![[📂 Media/PDFs/S0091743516300822.pdf]]

**Supplements:**


| **Endpoints** | **Exposures**        | **Populations** | **General**           | **People** |
| ------------- | -------------------- | --------------- | --------------------- | ---------- |
| #BMI          | #workplace_nutrition | #humans         | #nutrition            |            |
| #type_2_diabetes         |                      |                 | #disease              |            |
| #obesity      |                      |                 | #socioeconomic_status |            |
|               |                      |                 |                       |            |
|               |                      |                 |                       |            |
|               |                      |                 |                       |            |
|               |                      |                 |                       |            |
|               |                      |                 |                       |            |
|               |                      |                 |                       |            |

****