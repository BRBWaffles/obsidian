https://pubmed.ncbi.nlm.nih.gov/15800565/

"Urinary ketones are detected in premenopausal women complying with a low-carbohydrate/high-protein diet and are associated with serum ketone concentration. However, serum ketones do not reflect weight loss."

#weight_loss 
#keto 
#low_carb
#nutrition 
