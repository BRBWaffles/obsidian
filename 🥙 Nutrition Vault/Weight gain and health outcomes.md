https://pubmed.ncbi.nlm.nih.gov/10548135/

"Obesity and overweight are established risk factors for morbidity and mortality. In addition, we are now finding that weight gain in adulthood also has physical and psychological consequences. The adverse effects of weight gain on mental health can become an added burden for patients with schizophrenia or other types of mental illness. A weight gain during adulthood of 10 kg (22 lb) or more, even among persons who remain within the “normal” weight range, increases the risk of a variety of illnesses, ranging from cardiovascular diseases to cancer, and compromises the patient’s quality of life"

#nutrition 
#disease 
#obesity
#coronary_heart_disease 
#stroke
#cardiovascular_disease 
#weight_gain
#type_2_diabetes
#cancer 
#schizophrenia 
#mental_health 
#nutrition 
#public_health