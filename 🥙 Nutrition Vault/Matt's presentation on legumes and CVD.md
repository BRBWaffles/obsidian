**Notes:**
- Mozaffarian et al 2016
	- Cardiovascular Health

**Blood Pressure:**
- Jayalath et al. 2014
	- Legume and blood pressure meta
	- >2 servings/day from one month to one year
	- Stat-sig reduction in SBP
- Kou el al. 2017 & Mosallenezhad et al. 2021
	- 25g/day of soy
	- Stat-sig reduction in SBP an DBP

**Soy intervention doses and BP:**
![[Pasted image 20220306135116.png]]

**Serum Lipids:**
- Bazzano et al. 2011
	- 1 serving/day non-soy legumes
	- 3-8 weeks
	- Stat sig reductions in TC and LDL
- Tokede et al. 2015
	- 30g/day protein from soy products
	- Stat sig reduction in LDL

**Blood Glucose:**
- Haafix et al. 2021
	- Legume-based snacks/meals.
	- Suggested benefit from legumes
	- Stat sig reduction for BG and A1C in diabetics
- Asbaghi et al. 2021
	- Stat sig for soy beans

**CVD Risk Factors:**
- Hosseinpour-Nazi et al. 2015
	- 31 diabetics for 8 weeks, w/ 4 week washout
	- 2 servings of legumes subbing for red meat
	- Stat sig benefits to LDL, TG, BG, and insulin with legumes
- Azadbakt et al. 2008
	- 41 diabetics for 4 years
	- Replacing 1/2 of animal protein calories with legumes
	- Stat sig reduction in TC, TG, BG, and CRP, and DBP

**Mendelian Randomization Markers:**
![[Pasted image 20220306143219.png]]

**Cohort Studies:**
- Viguillouk et al 2019
	- 28 cohorts
	- 0-225g/day
	- Stat sig 8%A reduction in CVD incidence

**Meta 1 Breakdown:**
![[Pasted image 20220306144016.png]]

**Meta 2 Breakdown:**
![[Pasted image 20220306144231.png]]

**Meta 3 Breakdown:**
![[Pasted image 20220306144544.png]]

**Meta 4 Breakdown:**
![[Pasted image 20220306145003.png]]

| **Endpoints**   | **Exposures** | **Populations** | **General**    | People       |
| --------------- | ------------- | --------------- | -------------- | ------------ |
| #cardiovascular_disease            | #legumes      | #humans         | #disease       | #matt_madore |
| #coronary_heart_disease            | #soy          | #multinational  | #plant_foods   |              |
| #blood_glucose  |               |                 | #plant_protein |              |
| #blood_lipids   |               |                 |                |              |
| #blood_pressure |               |                 |                |              |
|                 |               |                 |                |              |
|                 |               |                 |                |              |
|                 |               |                 |                |              |
|                 |               |                 |                |              |
|                 |               |                 |                |              |
