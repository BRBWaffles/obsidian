Alami, Farkhondeh, et al. ‘The Effect of a Fruit-Rich Diet on Liver Biomarkers, Insulin Resistance, and Lipid Profile in Patients with Non-Alcoholic Fatty Liver Disease: A Randomized Clinical Trial’. _Scandinavian Journal of Gastroenterology_, June 2022, pp. 1–12. _PubMed_, https://doi.org/10.1080/00365521.2022.2071109.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/35710164/

**Calorie Intake Differences:**
![[Pasted image 20220728110640.png]]

**Notes:**
- [[Roy Taylor's work on T2DM]] suggests that there may be a sigmoidal relationship between liver fat and BMI.

**Conclusions:**
>In conclusion, the present study found that 6 months of
intervention with FRD exacerbated steatosis, dyslipidemia,
and glycemic control of NAFLD patients. It is possible that
excessive fruit consumption makes worse the condition of
patients with fatty liver. According to the findings of the
study, fruits intake increases the fat content of the hepatocyte probably through the lipogenic effect of fructose. To
clarify the issue, more studies specifying a range for fruit
intake (with minimum and maximum values) and considering
obese patients and patients with different grades of fatty
liver are warranted.

**PDF:**
[[📂 Media/PDFs/The effect of a fruit-rich diet on liver biomarkers, insulin resistance, and lipid profile in patients with non-alcoholic fatty liver disease- a randomized clinical trial.pdf]]

**Supplements:**


| **Endpoints**        | **Exposures** | **Populations** | **General**      | **People** |
| -------------------- | ------------- | --------------- | ---------------- | ---------- |
| #liver_enzymes       | #fruit        | #humans         | #nutrition       |            |
| #insulin_sensitivity |               |                 | #clinical_trials |            |
| #blood_glucose       |               |                 | #disease         |            |
| #insulin             |               |                 | #non_alcoholic_fatty_liver_disease           |            |
|                      |               |                 | #clown_papers    |            |
|                      |               |                 | #clownery        |            |

****

Johnston, Richard D., et al. ‘No Difference Between High-Fructose and High-Glucose Diets on Liver Triacylglycerol or Biochemistry in Healthy Overweight Men’. _Gastroenterology_, vol. 145, no. 5, Nov. 2013, pp. 1016-1025.e2. _DOI.org (Crossref)_, https://doi.org/10.1053/j.gastro.2013.07.012.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/23872500/

****


**Conclusions:**
>Features of NAFLD including steatosis, and elevated serum transaminases and
triglycerides occurred during energy overfeeding. The present study reports no difference in
these parameters between fructose and glucose. The greater uric acid concentration with
fructose was evidence of a reduced pre pyruvate metabolic control, though it appears to have
no hepatic impact in terms of hepatic volume, TAG storage, insulin resistance, glycogen
synthesis, fasted ATP content, and biochemical assays of liver function. As such, any advice on
low fructose diets in NAFLD remains unjustified. Further assessments are needed to assess if
the energy overfeeding changes are monosaccharide specific, and to assess the outcomes of
low monosaccharide intakes in NAFLD patients. 

**PDF:**
[[📂 Media/PDFs/j.gastro.2013.07.012.pdf]]

| **Endpoints**  | **Exposures** | **Populations** | **General**      | **People** |
| -------------- | ------------- | --------------- | ---------------- | ---------- |
| #non_alcoholic_fatty_liver_disease         | #glucose      | #humans         | #disease         |            |
| #liver_enzymes | #fructose     |                 | #clinical_trials |            |
|                |               |                 | #nutrition       |            |
|                |               |                 |                  |            |
|                |               |                 |                  |            |
|                |               |                 |                  |            |
|                |               |                 |                  |            |
|                |               |                 |                  |            |
|                |               |                 |                  |            |

****