Toews, Ingrid, et al. ‘Association between Intake of Non-Sugar Sweeteners and Health Outcomes: Systematic Review and Meta-Analyses of Randomised and Non-Randomised Controlled Trials and Observational Studies’. _BMJ (Clinical Research Ed.)_, vol. 364, Jan. 2019, p. k4718. _PubMed_, https://doi.org/10.1136/bmj.k4718.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/30602577/

**Conclusions:**
>Results of observational studies on the health effects of NSSs should be interpreted with caution, and attention should focus on plausible residual confounding as well as reverse causality (such as a higher consumption of NSSs by overweight or obese populations aiming at weight management). Appropriate long term studies that consider baseline consumption of sugar and NSSs and have an appropriate comparator should investigate whether NSSs are a safe and effective alternative to sugar, and results should be interpreted in light of these study design characteristics.

**PDF:**
[[📂 Media/PDFs/bmj.k4718.full.pdf]]

**Supplements:**
[[📂 Media/PDFs/toei046063.ww1.pdf]]
[[📂 Media/PDFs/toei046063.ww2.pdf]]
[[📂 Media/PDFs/toei046063.ww3.pdf]]

| **Endpoints**   | **Exposures**          | **Populations** | **General**        | **People** |
| --------------- | ---------------------- | --------------- | ------------------ | ---------- |
| #weight_loss    | #artificial_sweeteners | #humans         | #nutrition         |            |
| #weight_gain    |                        |                 | #meta_analysis     |            |
| #body_weight    |                        |                 | #systematic_review |            |
| #anthropometics |                        |                 | #disease           |            |
| #BMI            |                        |                 | #overweight        |            |
| #bladder_cancer |                        |                 | #obesity           |            |
|                 |                        |                 | #epidemiology      |            |
|                 |                        |                 | #cancer            |            |

****