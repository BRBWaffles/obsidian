Volaco, Alexei, et al. ‘Socioeconomic Status: The Missing Link Between Obesity and Diabetes Mellitus?’ _Current Diabetes Reviews_, vol. 14, no. 4, 2018, pp. 321–26. _PubMed_, https://doi.org/10.2174/1573399813666170621123227.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/28637406/

**Conclusions:**
>Cross sectional and prospective studies confirm the relationship between lower socioeconomic status and obesity and diabetes. The lower SES is associated to metabolic implications that are linked to insulin resistance and possibly may also interfere with the ability of beta cell to secrete insulin and change the gut microbiota, increasing even more the future risk of developing diabetes.

**PDF:**


**Supplements:**


| **Endpoints** | **Exposures**         | **Populations** | **General** | **People** |
| ------------- | --------------------- | --------------- | ----------- | ---------- |
| #obesity      | #socioeconomic_status | #humans         | #disease    |            |
| #body_weight  |                       |                 |             |            |
| #type_2_diabetes         |                       |                 |             |            |

****

Seligman, Hilary K., et al. ‘Food Insecurity and Glycemic Control among Low-Income Patients with Type 2 Diabetes’. _Diabetes Care_, vol. 35, no. 2, Feb. 2012, pp. 233–38. _PubMed_, https://doi.org/10.2337/dc11-1627.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/22210570/

**Conclusions:**
>Food insecurity is an independent risk factor for poor glycemic control in the safety net setting. This risk may be partially attributable to increased difficulty following a diabetes-appropriate diet and increased emotional distress regarding capacity for successful diabetes self-management. Screening patients with diabetes for food insecurity may be appropriate, particularly in the safety net setting.

**PDF:**
![[📂 Media/PDFs/233.pdf]]

**Supplements:**
![[📂 Media/PDFs/supp_dc11-1627v111627_DC111627SupplementaryData.pdf]]

| **Endpoints** | **Exposures**         | **Populations** | **General** | **People** |
| ------------- | --------------------- | --------------- | ----------- | ---------- |
| #obesity      | #socioeconomic_status | #humans         | #disease    |            |
| #body_weight  |                       |                 |             |            |
| #type_2_diabetes         |                       |                 |             |            | 

****