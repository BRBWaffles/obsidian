Diallo, Abou, et al. ‘Red and Processed Meat Intake and Cancer Risk: Results from the Prospective NutriNet-Santé Cohort Study’. _International Journal of Cancer_, vol. 142, no. 2, Jan. 2018, pp. 230–37. _PubMed_, https://doi.org/10.1002/ijc.31046.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/28913916/

**Population Characteristics:**
![[Pasted image 20220214155554.png]]

**Cancer Risk:**
![[Pasted image 20220214155619.png]]

**Breast Cancer Risk:**
![[Pasted image 20220214155642.png]]

**Conclusions:**
>In conclusion, this prospective cohort study brings new contribution into the role of red and processed meat intake as cancer risk factors. We observed that red meat intake was associated with increased overall and breast cancer risk, in line with mechanistic hypotheses from experimental studies. If confirmed, these findings suggest that limiting red meat intake may not only be beneficial for colorectal cancer, but also for the prevention of other tumor locations such as breast cancer.

**PDF:**
[[📂 Media/PDFs/Intl Journal of Cancer - 2017 - Diallo - Red and processed meat intake and cancer risk  Results from the prospective.pdf]]

| **Endpoints**    | **Exposures**   | **Populations**        | **General** |
| ---------------- | --------------- | ---------------------- | ----------- |
| #cancer          | #red_meat       | #humans                | #disease    |
| #breast_cancer   | #processed_meat | #france                | #nutrition  |
| #prostate_cancer | #animal_foods   | #nutrinet_sante_cohort |             |
|                  | #animal_protein |                        |             |
|                  | #meat           |                        |             |

****

‘WHO Report Says Eating Processed Meat Is Carcinogenic: Understanding the Findings’. The Nutrition Source, 3 Nov. 2015, https://www.hsph.harvard.edu/nutritionsource/2015/11/03/report-says-eating-processed-meat-is-carcinogenic-understanding-the-findings/.

**Link:**
https://monographs.iarc.who.int/wp-content/uploads/2018/06/mono114.pdf

**Conclusions:**
>Based on the balance of evidence, and taking into account study design, size, quality, control of potential confounding, exposure assessment, and magnitude of risk, an increased risk of cancer of the colorectum was seen in relation to consumption of red meat and of processed meat. The large amount of data, strength of association, and consistency across cohort studies in different populations, including most of the larger cohort studies, makes chance, bias, and confounding unlikely as explanations for the association of consumption of processed meat with cancer of the colorectum. However, chance, bias, or confounding could not be ruled out for consumption of red meat, as no association was observed in several of the larger studies. The available evidence from a subset of studies suggested that some cooking methods used in the preparation of red meat may contribute to the observed associations.

**PDF:**
[[📂 Media/PDFs/mono114.pdf]]

| **Endpoints**      | **Exposures**   | **Populations** | **General**        | **People** |
| ------------------ | --------------- | --------------- | ------------------ | ---------- |
| #colorectal_cancer | #red_meat       | #multinational  | #disease           |            |
| #stomach_cancer    | #processed_meat |                 | #cancer            |            |
| #pancreatic_cancer |                 |                 | #systematic_review |            |
| #prostate_cancer   |                 |                 | #meta_analysis     |            |
| #breast_cancer     |                 |                 | #cohort_studies    |            |
| #lung_cancer       |                 |                 | #IARC              |            |
| #esophageal_cancer |                 |                 |                    |            |
|                    |                 |                 |                    |            |
|                    |                 |                 |                    |            |

****