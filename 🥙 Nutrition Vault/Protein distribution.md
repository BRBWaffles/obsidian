https://pubmed.ncbi.nlm.nih.gov/32321161/

"For RT-induced muscle hypertrophy in healthy young men, consuming a protein-enriched meal at breakfast and less protein at dinner while achieving an adequate overall PI is more effective than consuming more protein at dinner."

https://pubmed.ncbi.nlm.nih.gov/24477298/

"The consumption of a moderate amount of protein at each meal stimulated 24-h muscle protein synthesis more effectively than skewing protein intake toward the evening meal."

https://pubmed.ncbi.nlm.nih.gov/23459753/

"This study provides novel information on the effect of modulating the distribution of protein intake on anabolic responses in skeletal muscle and has the potential to maximize outcomes of resistance training for attaining peak muscle mass."

#strength 
#hypertrophy
#protein 
#exercise
#nutrition