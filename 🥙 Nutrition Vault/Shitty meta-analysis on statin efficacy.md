Byrne, Paula, et al. ‘Evaluating the Association Between Low-Density Lipoprotein Cholesterol Reduction and Relative and Absolute Effects of Statin Treatment: A Systematic Review and Meta-Analysis’. _JAMA Internal Medicine_, Mar. 2022. _Silverchair_, https://doi.org/10.1001/jamainternmed.2022.0134.

**Link:**
https://jamanetwork.com/journals/jamainternalmedicine/article-abstract/2790055

**Notes:**
>- No distinction between time-based and event-based stopping conditions.
>- Relative risk and absolute risk are answering two different questions.
>- Included many trials with chronic kidney failure and on hemodialysis (high risk with unclear potential benefit of lipid lowering).
>- Unclear distinction between high and moderate risk populations, with high risk populations already having achieved significant lipid reductions.
>- Authors assume linearity in the relationship between LDL and their endpoints (average 3-4 years versus 20 years, when the largest risk reductions seem to be achieved).
>- Mixed primary and secondary prevention trials.

**Conclusions:**
> The results of this meta-analysis suggest that the absolute risk reductions of treatment with statins in terms of all-cause mortality, myocardial infarction, and stroke are modest compared with the relative risk reductions, and the presence of significant heterogeneity reduces the certainty of the evidence. A conclusive association between absolute reductions in LDL-C levels and individual clinical outcomes was not established, and these findings underscore the importance of discussing absolute risk reductions when making informed clinical decisions with individual patients.

**PDF:**
[[📂 Media/PDFs/Evaluating_the_Association_Between_Low-Density_Lipoprotein_Cholesterol_Reduction_and_Relative_and_Absolute_Effects_of_Statin_Treatment_A_Systematic_Review_and_Meta-analysis.pdf]]

| **Endpoints** | **Exposures**      | **Populations** | **General**   | **People** |
| ------------- | ------------------ | --------------- | ------------- | ---------- |
| #coronary_heart_disease          | #statins           | #humans         | #blood_lipids |            |
| #all_cause_mortality          | #LDL               | #multinational  | #disease      |            |
| #stroke       | #serum_cholesterol |                 | #clownery     |            |
|               |                    |                 | #clown_papers |            |               |                    |                 |               |            |

****