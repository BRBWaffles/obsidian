https://pubmed.ncbi.nlm.nih.gov/23447425/

"Statins significantly reduce the incidence of all-cause mortality and major coronary events as compared to control in both secondary and primary prevention. This analysis provides evidence for potential differences between individual statins, which are not fully explained by their low-density lipoprotein cholesterol-reducing effects. The observed differences between statins should be investigated in future prospective studies."

https://pubmed.ncbi.nlm.nih.gov/27764723/

"Statins significantly reduced levels of blood lipids, with a high dose of atorvastatin being the most effective in blood-lipid level modification. Statins reduced the risk of CHD mortality and all-cause mortality, with atorvastatin and fluvastatin being the most effective in reducing the risk of CHD mortality and all-cause mortality. Statins increased the risk of muscle disease and kidney damage."

https://pubmed.ncbi.nlm.nih.gov/20934984/

"Statin therapies offer clear benefits across broad populations. As generic formulations become more available efforts to expand access should be a priority."

https://pubmed.ncbi.nlm.nih.gov/31475865/

"While both statin and established non-statin therapies (PCSK9 inhibitor and ezetimibe) reduced cardiovascular risk per decrease in apolipoprotein B, interventions which reduce apolipoprotein B independently of LDL-R were not associated with cardiovascular benefit."

https://pubmed.ncbi.nlm.nih.gov/31221259/

"Genetically low LDL cholesterol due to PCSK9 variation was causally associated with low risk of cardiovascular mortality, but not with low all-cause mortality in the general population."

https://pubmed.ncbi.nlm.nih.gov/35168005/

After adjustment for comorbidities and malnutrition.

#disease
#LDL 
#all_cause_mortality 
#coronary_heart_disease 
#cardiovascular_disease
#PCSK9 
#blood_lipids 
#ApoB 
#statins
#lipidology