https://pubmed.ncbi.nlm.nih.gov/27543472/

"Coconut oil even though rich in saturated fatty acids in comparison to sunflower oil when used as cooking oil media over a period of 2 years did not change the lipid-related cardiovascular risk factors and events in those receiving standard medical care."

#disease 
#cardiovascular_disease 
#coronary_heart_disease 
#Lpa
#inflammation 
#polyunsaturated_fat 
#saturated_fat 
#dietary_fat
#LDL 
#HDL 
#ApoB 
#ApoA1 
#blood_lipids 
#nutrition
#coconut_oil
#vegetable_oil