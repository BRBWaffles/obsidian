Kuno, Toshiki, et al. ‘The Association of Statins Use with Survival of Patients with COVID-19’. _Journal of Cardiology_, vol. 79, no. 4, Apr. 2022, pp. 494–500. _PubMed_, https://doi.org/10.1016/j.jjcc.2021.12.012.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/34974938/

**Conclusions:**
>Use of statins prior to or during hospitalization was not associated with a decreased risk of in-hospital mortality, however, continuous use of statins might have potential benefit of a decreased risk of in-hospital mortality due to COVID-19.

**PDF:**
[[📂 Media/PDFs/PIIS0914508721003671.pdf]]

| **Endpoints** | **Exposures** | **Populations** | **General**   | **People** |
| ------------- | ------------- | --------------- | ------------- | ---------- |
| #COVID19      | #statins      | #humans         | #mortality    |            |
|               |               |                 | #epidemiology |            |
|               |               |                 | #virology     |            |
|               |               |                 | #lipidology   |            |
|               |               |                 | #disease      |            |

****