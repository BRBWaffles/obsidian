https://pubmed.ncbi.nlm.nih.gov/32113648/

"Genetic evidence fails to support a cardioprotective role for apoA-I. This is in line with the cumulative evidence showing that HDL-related phenotypes are unlikely to have a protective role in CAD."

https://pubmed.ncbi.nlm.nih.gov/28847206/

"Among patients with atherosclerotic vascular disease who were receiving intensive statin therapy, the use of anacetrapib resulted in a lower incidence of major coronary events than the use of placebo."

https://pubmed.ncbi.nlm.nih.gov/17984165/

"Torcetrapib therapy resulted in an increased risk of mortality and morbidity of unknown mechanism. Although there was evidence of an off-target effect of torcetrapib, we cannot rule out adverse effects related to CETP inhibition."

https://pubmed.ncbi.nlm.nih.gov/23126252/

"In patients who had had a recent acute coronary syndrome, dalcetrapib increased HDL cholesterol levels but did not reduce the risk of recurrent cardiovascular events."

https://pubmed.ncbi.nlm.nih.gov/28514624/

"Although the cholesteryl ester transfer protein inhibitor evacetrapib had favorable effects on established lipid biomarkers, treatment with evacetrapib did not result in a lower rate of cardiovascular events than placebo among patients with high-risk vascular disease."

https://pubmed.ncbi.nlm.nih.gov/32203549/

#disease 
#HDL 
#HDLp 
#LDL 
#coronary_heart_disease 
#cardiovascular_disease
#CETP_inhibitors
#ApoA1 
#blood_lipids 
#CETP_inhibitors
#lipidology 
