Brown, Andrew W., et al. ‘Toward More Rigorous and Informative Nutritional Epidemiology: The Rational Space between Dismissal and Defense of the Status Quo’. _Critical Reviews in Food Science and Nutrition_, Oct. 2021, pp. 1–18. _PubMed_, https://doi.org/10.1080/10408398.2021.1985427.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/34678079/

**Conclusions:**
>To date, nutritional epidemiology has relied heavily on relatively weak methods including simple observational designs and substandard measurements. Despite low internal validity and other sources of bias, claims of causality are made commonly in this literature. Nutritional epidemiology investigations can be improved through greater scientific rigor and adherence to scientific reporting commensurate with research methods used. Some commentators advocate jettisoning nutritional epidemiology entirely, perhaps believing improvements are impossible. Still others support only normative refinements. But neither abolition nor minor tweaks are appropriate. Nutritional epidemiology, in its present state, offers utility, yet also needs marked, reformational renovation. Changing the status quo will require ongoing, unflinching scrutiny of research questions, practices, and reporting—and a willingness to admit that “good enough” is no longer good enough. As such, a workshop entitled “Toward more rigorous and informative nutritional epidemiology: the rational space between dismissal and defense of the status quo” was held from July 15 to August 14, 2020. This virtual symposium focused on: (1) Stronger Designs, (2) Stronger Measurement, (3) Stronger Analyses, and (4) Stronger Execution and Reporting. Participants from several leading academic institutions explored existing, evolving, and new better practices, tools, and techniques to collaboratively advance specific recommendations for strengthening nutritional epidemiology

**PDF:**
[[📂 Media/PDFs/Toward more rigorous and informative nutritional epidemiology The rational space between dismissal and defense of the status quo.pdf]]

| **General**          | **People** |
| -------------------- | ---------- |
| #clown_papers        |            |
| #clownery            |            |
| #nutrition           |            |
| #epidemiology        |            |
| #low_carb_talking_points |            |

****