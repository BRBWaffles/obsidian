https://pubmed.ncbi.nlm.nih.gov/29987352/

"Total potato consumption is not related to risk for many chronic diseases but could pose a small increase in risk for T2D if consumed boiled. A clear risk relation was found between French-fries consumption and risk of T2D and hypertension. For several outcomes, the impact of different preparation procedures could not be assessed."

https://pubmed.ncbi.nlm.nih.gov/28592612/

"The frequent consumption of fried potatoes appears to be associated with an increased mortality risk. Additional studies in larger sample sizes should be performed to confirm if overall potato consumption is associated with higher mortality risk."

https://pubmed.ncbi.nlm.nih.gov/33261659/

"Evidence mapping found sufficient data on the association between potato intake and cardio-metabolic disease risk factors to warrant for a systematic review/meta-analysis of observational studies."

#disease
#nutrition 
#potatoes 
#all_cause_mortality
#coronary_heart_disease
#stroke 
#insulin_sensitivity 
#hypertension 
#anthropometics 
#type_2_diabetes 
#fried_foods
#colorectal_cancer 
#carbohydrates 
#hypertension 
