Lennerz, Belinda S., et al. ‘Behavioral Characteristics and Self-Reported Health Status among 2029 Adults Consuming a “Carnivore Diet”’. _Current Developments in Nutrition_, vol. 5, no. 12, Dec. 2021, p. nzab133. _PubMed_, https://doi.org/10.1093/cdn/nzab133.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/34934897/

**Population Characteristics**
![[Pasted image 20220214132844.png]]

**Food Intake:**
![[Pasted image 20220214132854.png]]

**Changes in Health Status:**
![[Pasted image 20220214132906.png]]

**Self-Reported Disease Prevalence:**
![[Pasted image 20220214132917.png]]

**Pre- and Post-Diet Markers:**
![[Pasted image 20220214132923.png]]

**Conclusions:**
>Our study reports on a large group of participants following a carnivore diet, with perceived health benefits and absence of symptoms consistent with nutritional deficiencies, providing insights into a poorly characterized dietary approach. However, the data are limited by several major design limitations inherent to the survey design. A clearer understanding of the long-term safety and benefits of a carnivore diet, exact dietary habits of people following this diet, and the generalizability of our findings, must await additional research.

**PDF:**
[[📂 Media/PDFs/nzab133.pdf]]

**Supplements:**
[[📂 Media/PDFs/nzab133_supplemental_file.pdf]]

| **Endpoints**     | **Exposures**   | **Populations** | **General**          | People        |
| ----------------- | --------------- | --------------- | -------------------- | ------------- |
| #obesity          | #red_meat       | #humans         | #clown_papers        | #shawn_baker  |
| #overweight       | #meat           | #multinational  | #disease             | #jake_mey     |
| #underweight      | #carnivore      |                 | #nutrition           | #david_ludwig |
| #hypertension     | #animal_fats    |                 | #low_carb_talking_points |               |
| #cardiovascular_disease              | #animal_foods   |                 | #clownery            |               |
| #type_2_diabetes             | #animal_protein |                 |                      |               |
| #digestive_health | #keto           |                 |                      |               |
| #hormones         | #white_meat     |                 |                      |               |
| #auto_immune      | #eggs           |                 |                      |               |
| #frailty          | #fish           |                 |                      |               |
| #mental_health    | #poultry        |                 |                      |               |
| #kidney_disease   | #dairy          |                 |                      |               |
| #skin_health      | #seafood        |                 |                      |               |
| #cancer           | #organ_meats    |                 |                      |               |
| #blood_lipids     |                 |                 |                      |               |
| #blood_glucose    |                 |                 |                      |               |
| #LDL              |                 |                 |                      |               |
| #HDL              |                 |                 |                      |               |
| #triglycerides               |                 |                 |                      |               |
| #coronary_artery_calcification              |                 |                 |                      |               |
| #hba1c            |                 |                 |                      |               |
| #BMI              |                 |                 |                      |               |
| #liver_enzymes    |                 |                 |                      |               |
| #c_reactive_protein              |                 |                 |                      |               |
| #body_weight      |                 |                 |                      |               |

****