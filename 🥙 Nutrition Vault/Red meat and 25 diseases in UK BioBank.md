https://pubmed.ncbi.nlm.nih.gov/33648505/

"Higher unprocessed red meat, processed meat, and poultry meat consumption was associated with higher risks of several common conditions; higher BMI accounted for a substantial proportion of these increased risks suggesting that residual confounding or mediation by adiposity might account for some of these remaining associations. Higher unprocessed red meat and poultry meat consumption was associated with lower IDA risk."

#disease
#coronary_heart_disease
#type_2_diabetes
#stroke
#pneumonia
#afib
#thrombosis
#obesity
#weight_gain 
#diverticular_disease
#colon_polyops
#meat 
#red_meat 
#processed_meat
#animal_foods 
#animal_protein
#nutrition
#anemia