https://pubmed.ncbi.nlm.nih.gov/31142450/

"A higher consumption of ultra-processed foods (>4 servings daily) was independently associated with a 62% relatively increased hazard for all cause mortality. For each additional serving of ultra-processed food, all cause mortality increased by 18%."

https://pubmed.ncbi.nlm.nih.gov/34348832/

"An increase in UPF consumption was associated with higher risk of all-cause mortality in a representative sample of the Spanish population. Moreover, the theoretical substitution of UPF with unprocessed or minimally processed foods leads to a decrease in mortality. These results support the need to promote diets based on unprocessed or minimally processed foods."

https://pubmed.ncbi.nlm.nih.gov/33536027/

"High consumption of ultra-processed foods is associated with increased risks of overall cardiovascular and heart disease mortality. These harmful associations may be more pronounced in women. Our findings need to be confirmed in other populations and settings."

https://pubmed.ncbi.nlm.nih.gov/30789115/

"Higher frequency of ultra-processed food intake was associated with higher risk of all-cause mortality in a representative sample of US adults. More longitudinal studies with dietary data reflecting the modern food supply are needed to confirm our results."

https://pubmed.ncbi.nlm.nih.gov/33333551/

"A high proportion of UPF in the diet was associated with increased risk of CVD and all-cause mortality, partly through its high dietary content of sugar. Some established biomarkers of CVD risk were likely to be on the pathway of such associations. These findings should serve as an incentive for limiting consumption of UPF, and encouraging natural or minimally processed foods, as several national nutritional policies recommend."

https://pubmed.ncbi.nlm.nih.gov/30742202/

"An increase in ultraprocessed foods consumption appears to be associated with an overall higher mortality risk among this adult population; further prospective studies are needed to confirm these findings and to disentangle the various mechanisms by which ultraprocessed foods may affect health."

https://pubmed.ncbi.nlm.nih.gov/31623843/

"A higher consumption of ultra-processed food was associated with higher mortality in the general population. Furthermore, the theoretical iso-caloric substitution ultra-processed food by unprocessed or minimally processed foods would suppose a reduction of the mortality risk. If confirmed, these findings support the necessity of the development of new nutritional policies and guides at the national and international level."

#disease 
#all_cause_mortality 
#coronary_heart_disease 
#cardiovascular_disease 
#stroke
#processed_food 
#carbohydrates 
#nutrition 