https://pubmed.ncbi.nlm.nih.gov/11063452/

"These data suggest that adherence to the 1995 Dietary Guidelines for Americans, as measured by the HEI-f, will have limited benefit in preventing major chronic disease in women."

https://pubmed.ncbi.nlm.nih.gov/22571690/

"The present study provides longitudinal support for a reduced risk of all-cause mortality in an older population who have greater compliance with published dietary guidelines."

https://pubmed.ncbi.nlm.nih.gov/15226474/

"These results suggest that overall compliance with the Dietary Guidelines, as measured by the HEI, protects against nuclear opacities."

https://pubmed.ncbi.nlm.nih.gov/24944055/

"A greater compliance with Chinese or US dietary guidelines is associated with lower total mortality in Chinese adults. Favorable associations are more evident in men than women and more consistent for cardiometabolic mortality than cancer mortality."

https://pubmed.ncbi.nlm.nih.gov/28825166/

 To conclude, adherence to the Dutch dietary guidelines was associated with a lower mortality risk and a lower risk of developing some but not all of the chronic diseases on which the guidelines were based."
 
 https://pubmed.ncbi.nlm.nih.gov/22157209/
 
"Available data on the potential association between dietary guidelines and cancer are limited and inconclusive. A meta-analysis of studies on overall cancer risk shows no protective effect for good adherence to the dietary guidelines as compared with poor adherence. However, good adherence was associated with a 21% reduced risk of colorectal cancer, and 22% reduced cancer-specific mortality."

https://pubmed.ncbi.nlm.nih.gov/29439463/

"This study contributes to the evidence that diet quality is associated with health outcomes, including weight status, and will be useful in framing recommendations for obesity prevention and management."

https://pubmed.ncbi.nlm.nih.gov/34158032/

#dietary_guidelines
#disease
#breast_cancer
#colorectal_cancer
#weight_loss
#obesity
#coronary_heart_disease
#type_2_diabetes
#heart_failure
#lung_cancer
#dementia
#cardiovascular_disease 
#cancer
#vision_loss
#all_cause_mortality
#whole_grains
#vegetables
#dairy
#fruit
#milk
#meat
#fat
#saturated_fat
#dietary_cholesterol
#sodium
#nutrition