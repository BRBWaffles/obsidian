https://pubmed.ncbi.nlm.nih.gov/20856100/

https://pubmed.ncbi.nlm.nih.gov/31391548/

https://pubmed.ncbi.nlm.nih.gov/28630693/

https://pubmed.ncbi.nlm.nih.gov/31206566/

https://pubmed.ncbi.nlm.nih.gov/17467370/

https://pubmed.ncbi.nlm.nih.gov/27904560/

https://pubmed.ncbi.nlm.nih.gov/19328271/

https://pubmed.ncbi.nlm.nih.gov/19328270/

https://pubmed.ncbi.nlm.nih.gov/31783477/

https://pubmed.ncbi.nlm.nih.gov/20707949/

https://pubmed.ncbi.nlm.nih.gov/23206381/

https://pubmed.ncbi.nlm.nih.gov/26753989/

https://pubmed.ncbi.nlm.nih.gov/18435934/

https://pubmed.ncbi.nlm.nih.gov/28744912/

https://pubmed.ncbi.nlm.nih.gov/26653304/

https://pubmed.ncbi.nlm.nih.gov/30121379/

[[📂 Media/PDFs/Validity of the food frequency questionnaire for adults in nutritional epidemiological studies_ A systematic review and meta-analysis (Cui, 2021).pdf]]

#food_frequency_questionnaires
#epidemiology 
#validation 
#nutrition 
#energy_intake 
#nutrients 
#nutritional_status 
#nutrition_science 
#nutrition 