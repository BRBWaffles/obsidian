https://pubmed.ncbi.nlm.nih.gov/33899735/

"Multivariable MR analyses also found that small HDL particles and smaller mean HDL particle diameter may have a protective effect. We identified four genetic markers for HDL particle size and CAD. Further investigations are needed to fully understand the role of HDL particle size."

#disease 
#lipidology 
#blood_lipids 
#LDL 
#coronary_heart_disease 
#cardiovascular_disease 
#LDLp 
#ApoB 
#mendelian_randomization 
#HDLp
