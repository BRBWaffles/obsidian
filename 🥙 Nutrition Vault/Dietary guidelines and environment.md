https://pubmed.ncbi.nlm.nih.gov/28768562/

"Our results showed that adhering to the WHO and Dutch dietary guidelines will lower the risk of all-cause mortality and moderately lower the environmental impact. The DASH diet was associated with lower mortality and land use, but because of high dairy product consumption in the Netherlands it was also associated with higher GHG emissions."

#dietary_guidelines
#environment
#GHG
#nutrition