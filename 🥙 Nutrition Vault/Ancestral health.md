Fodor, J. George, et al. ‘“Fishing” for the Origins of the “Eskimos and Heart Disease” Story: Facts or Wishful Thinking?’ _The Canadian Journal of Cardiology_, vol. 30, no. 8, Aug. 2014, pp. 864–68. _PubMed_, https://doi.org/10.1016/j.cjca.2014.04.007.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/25064579/

**Conclusions:**
>The totality of reviewed evidence leads us to the conclusion that Eskimos have a similar prevalence of CAD as non-Eskimo populations they have excessive mortality due to cerebrovascular strokes, their overall mortality is twice as high as that of non-Eskimo populations and their life expectancy is approximately 10 years shorter than the Danish population.

**PDF:**
[[📂 Media/PDFs/j.cjca.2014.04.007.pdf]]

| **Endpoints** | **Exposures**   | **Populations**     | **General**              | **People** |
| ------------- | --------------- | ------------------- | ------------------------ | ---------- |
| #coronary_heart_disease          | #ancestral_food | #primitive_cultures | #nutrition               |            |
|               |                 | #humans             | #antagonistic_pleiotropy |            |
|               |                 | #hunter_gatherers   | #anthropology            |            |

****

Dugdale, A. E. ‘Infant Feeding, Growth and Mortality: A 20-Year Study of an Australian Aboriginal Community’. _The Medical Journal of Australia_, vol. 2, no. 7, Oct. 1980, pp. 380–85. _PubMed_, https://doi.org/10.5694/j.1326-5377.1980.tb131878.x.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/7453611/

**Conclusions:**
>This study has shown that infant mortality rate in a population can fall from 280 per 1000 live births to 40 per 1000 live births without major change in the type of feeding or in the level of nutrition. Neither did more detailed examination of infant growth and deaths show any consistent and systematic differences between infants on the two types of feeding. It appears that, in this community over the period studied, the type of milk given to the infant did not influence the growth rate and that neither the type of feeding nor the level of nutrition was responsible for the initial high death rate or its improvement. It can be argued that this community is not typical or representative of villages in developing countries. All communities have their peculiarities, and this argument can be supported or rebutted only when similar long-term studies have been done elsewhere. There must be data available around the world, but such studies are rare.

**PDF:**
[[📂 Media/PDFs/dugdale1980.pdf]]

| **Endpoints** | **Exposures**   | **Populations**     | **General**              | **People** |
| ------------- | --------------- | ------------------- | ------------------------ | ---------- |
| #all_cause_mortality          | #ancestral_food | #primitive_cultures | #nutrition               |            |
|               |                 | #humans             | #antagonistic_pleiotropy |            |
|               |                 | #children           | #disease                 |            |
|               |                 | #infants            | #anthropology            |            |
|               |                 | #hunter_gatherers   |                          |            |

****

Van Arsdale, P. W. ‘Population Dynamics among Asmat Hunter-Gatherers of New Guinea: Data, Methods, Comparisons’. _Human Ecology_, vol. 6, no. 4, Dec. 1978, pp. 435–67. _PubMed_, https://doi.org/10.1007/BF00889419.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/12335596/

****


**Conclusions:**
>Brief comparisons with other Melanesian and 3rd world societies are presented; the Asmat average annual growth rate of 1.5% since 1st permanent contact in 1953 contrasts with the generally higher rates reported for most of these other groups.

**PDF:**
![[📂 Media/PDFs/bf00889419.pdf]]

| **Endpoints** | **Exposures**   | **Populations**     | **General**              | **People** |
| ------------- | --------------- | ------------------- | ------------------------ | ---------- |
| #all_cause_mortality          | #ancestral_food | #primitive_cultures | #nutrition               |            |
|               |                 | #humans             | #antagonistic_pleiotropy |            |
|               |                 | #children           | #disease                 |            |
|               |                 | #infants            | #anthropology            |            |
|               |                 | #hunter_gatherers   | #child_development       |            |

****

Johnston, F. E., and C. E. Snow. ‘The Reassessment of the Age and Sex of the Indian Knoll Skeletal Population: Demographic and Methodological Aspects’. _American Journal of Physical Anthropology_, vol. 19, no. 3, Sept. 1961, pp. 237–44. _PubMed_, https://doi.org/10.1002/ajpa.1330190304.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/14452143/

**Conclusions:**
>- The demography of the Indian Knoll population is similar to the more primitive, non-agricultural groups, and exhibits little over-all similarity, in terms of age distribution, to more economically and technologically advanced groups.
>- The average difference between skeletal aging by means of closure of the cranial sutures and by means of a composite of skeletal and dental criteria was, at Indian Knoll, 4.6 years. This average is very close to the standard error of estimate of McKern and Stewart ('57) of 4.4614 years, between predicted and actual ages.
>- The sex ratios of the two studies of the same population were similar, indicating that skeletal sexing, at least as far as Indian Knoll is concerned, gave relatively constant results.

**PDF:**
[[📂 Media/PDFs/ajpa.1330190304.pdf]]

| **Populations**   | **General**   | **People** |
| ----------------- | ------------- | ---------- |
| #north_america    | #archeology   |            |
| #hunter_gatherers | #anthropology |            |
| #humans           | #disease      |            |

****

Walker, Robert, et al. ‘Growth Rates and Life Histories in Twenty-Two Small-Scale Societies’. _American Journal of Human Biology: The Official Journal of the Human Biology Council_, vol. 18, no. 3, June 2006, pp. 295–311. _PubMed_, https://doi.org/10.1002/ajhb.20510.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/16634027/

**Conclusions:**
>It is beneficial to include growth trajectories as integral components of life histories. A complete assessment of an organism’s life history requires age-specific rates of growth, survivorship, and fertility. It is unfortunate that such data are rare (and occasionally low-quality) for hunter-gatherers, given that human evolution occurred in a foraging context, and that the last century has seen a drastic global reduction in societies with this lifestyle. Detection of different developmental patterns is difficult without cross-cultural growth information, and perhaps this explains the previous lack of attention to the question of divergent human life-history strategies.

**PDF:**
[[📂 Media/PDFs/ajhb.20510.pdf]]

| **Endpoints** | **Exposures**   | **Populations**   | **General**        | **People** |
| ------------- | --------------- | ----------------- | ------------------ | ---------- |
| #growth       | #ancestral_food | #multinational    | #disease           |            |
|               |                 | #hunter_gatherers | #child_development |            |
|               |                 | #infants          | #nutrition         |            |
|               |                 | #children         |                    |            |
|               |                 | #humans           |                    |            |

****

Burger, Oskar, et al. ‘Human Mortality Improvement in Evolutionary Context’. _Proceedings of the National Academy of Sciences of the United States of America_, vol. 109, no. 44, Oct. 2012, pp. 18210–14. _PubMed_, https://doi.org/10.1073/pnas.1215627109.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/23071331/

**Conclusions:**
>Mortality improvement in humans is on par with or greater than the reductions in mortality in other species achieved by laboratory selection experiments and endocrine pathway mutations. This observed plasticity in age-specific risk of death is at odds with conventional theories of aging.

**PDF:**
![[📂 Media/PDFs/pnas.1215627109.pdf]]

| **Endpoints** | **Populations**     | **General**    | **People** |
| ------------- | ------------------- | -------------- | ---------- |
| #longevity    | #primitive_cultures | #anthropology  |            |
|               | #hunter_gatherers   | #public_health |            |
|               | #humans             | #disease       |            |

****

Hill, Kim, et al. ‘High Adult Mortality among Hiwi Hunter-Gatherers: Implications for Human Evolution’. _Journal of Human Evolution_, vol. 52, no. 4, Apr. 2007, pp. 443–54. _PubMed_, https://doi.org/10.1016/j.jhevol.2006.11.003.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/17289113/

**Conclusions:**
>Summarizing then, we conclude that mortality rates after childhood decreased substantially in the postcontact period. The main sex difference was increasingly higher mortality in men relative to women as age increased (Fig. 1). Adult-male survival in the precontact period was particularly low, with an adult life expectancy of only 30 additional years at age 15. The infant age group showed two reversals of the general trends. First, infant mortality was higher in the postcontact period than in the precontact period, and second, female mortality was higher than male mortality in this age group. The latter trend is mainly due to female-biased infanticide.

**PDF:**
[[📂 Media/PDFs/j.jhevol.2006.11.003.pdf]]

| **Endpoints** | **Exposures** | **Populations**     | **General**    | **People** |
| ------------- | ------------- | ------------------- | -------------- | ---------- |
| #all_cause_mortality          | #contact      | #hunter_gatherers   | #anthropology  |            |
|               |               | #primitive_cultures | #public_health |            |
|               |               | #humans             | #disease       |            |

****

Finch, Caleb E. ‘Evolution in Health and Medicine Sackler Colloquium: Evolution of the Human Lifespan and Diseases of Aging: Roles of Infection, Inflammation, and Nutrition’. _Proceedings of the National Academy of Sciences of the United States of America_, vol. 107 Suppl 1, Jan. 2010, pp. 1718–24. _PubMed_, https://doi.org/10.1073/pnas.0909606106.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/19966301/

**Conclusions:**
>Humans have evolved much longer lifespans than the great apes, which rarely exceed 50 years. Since 1800, lifespans have doubled again, largely due to improvements in environment, food, and medicine that minimized mortality at earlier ages. Infections cause most mortality in wild chimpanzees and in traditional forager-farmers with limited access to modern medicine. Although we know little of the diseases of aging under premodern conditions, in captivity, chimpanzees present a lower incidence of cancer, ischemic heart disease, and neurodegeneration than current human populations. These major differences in pathology of aging are discussed in terms of genes that mediate infection, inflammation, and nutrition. Apolipoprotein E alleles are proposed as a prototype of pleiotropic genes, which influence immune responses, arterial and Alzheimer’s disease, and brain development.

**PDF:**
[[📂 Media/PDFs/pnas.200909606.pdf]]

| **Endpoints** | **Exposures**   | **Populations**     | **General**      | **People** |
| ------------- | --------------- | ------------------- | ---------------- | ---------- |
| #longevity    | #ancestral_food | #primitive_cultures | #inflammation    |            |
| #all_cause_mortality          | #novel_food     | #humans             | #immune_function |            |
| #dementia     |                 | #hunter_gatherers   | #mental_health   |            |
| #cancer       |                 |                     | #anthropology    |            |
| #coronary_heart_disease          |                 |                     | #nutrition       |            |

****