Guasch-Ferre, Marta, et al. ‘Consumption of Total Olive Oil and Risk of Total and Cause-Specific Mortality in US Adults’. _Current Developments in Nutrition_, vol. 5, no. Suppl 2, June 2021, p. 1036. _PubMed Central_, https://doi.org/10.1093/cdn/nzab053_029.

**Link:**
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8181296/

**Substitution Analysis:**
![[Pasted image 20220324180451.png]]

**Conclusions:**
>We found that greater consumption of olive oil was associated with lower risk of total and cause specific mortality. Replacing other types of fat, such as margarine, butter, mayonnaise, and dairy fat, with olive oil was associated with a lower risk of mortality. Our results support current dietary recommendations to increase the intake of olive oil and other unsaturated vegetable oils in place of other fats to improve overall health and longevity.

**PDF:**
[[📂 Media/PDFs/Consumption of Olive Oil and Risk of Total and Cause-Specific Mortality Among U.S. Adults (Gausch-Ferré, 2022) (1).pdf]]

**Supplements:**


| **Endpoints**            | **Exposures**  | **Populations**                      | **General** | **People**      |
| ------------------------ | -------------- | ------------------------------------ | ----------- | --------------- |
| #cancer                  | #olive_oil     | #health_professionals_followup_study | #disease    | #walter_willett |
| #cardiovascular_disease                     | #vegetable_oil | #nurses_health_study                 | #mortality  | #frank_hu       |
| #all_cause_mortality                     | #mayonnaise    | #humans                              | #nutrition  | #yanping_li     |
| #respiratory_disease     | #margarine     |                                      |             |                 |
| #neurodegerative_disease |                |                                      |             |                 |

****