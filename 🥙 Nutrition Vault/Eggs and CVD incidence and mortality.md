Zhong, Victor W., et al. ‘Associations of Dietary Cholesterol or Egg Consumption With Incident Cardiovascular Disease and Mortality’. JAMA, vol. 321, no. 11, Mar. 2019, pp. 1081–95. PubMed, https://doi.org/10.1001/jama.2019.1572.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/30874756/

**Conclusions:**
>

**PDF:**
[[📂 Media/PDFs/jama-321-1081-s001.pdf]]

**Supplements:**
[[📂 Media/PDFs/jama_zhong_2019_oi_190019.pdf]]

| **Endpoints** | **Exposures**        | **Populations** | **General**   | **People** |
| ------------- | -------------------- | --------------- | ------------- | ---------- |
| #cardiovascular_disease          | #dietary_cholesterol | #humans         | #nutrition    |            |
|               | #eggs                |                 | #mortality    |            |
|               |                      |                 | #epidemiology |            |
|               |                      |                 | #disease      |            |
|               |                      |                 |               |            |
|               |                      |                 |               |            |
|               |                      |                 |               |            |
|               |                      |                 |               |            |
|               |                      |                 |               |            |

****