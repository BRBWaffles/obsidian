https://pubmed.ncbi.nlm.nih.gov/27535127/

"Daily use with 20 g of linoleic acid-rich SB-mayo elicited reductions in TC and LDL-C concentrations without significantly changing LDL-C:HDL-C ratio or small LDL particle distributions compared to the PO-mayo diet."

https://pubmed.ncbi.nlm.nih.gov/16459230/

"Daily ingestion of 15 g of DAG plus mayonnaise containing at least 0.4 g/d of PSE for 4 wk may significantly decrease cholesterol."

#disease
#mayonnaise
#polyunsaturated_fat
#saturated_fat
#soybean_oil
#palm_oil 
#blood_lipids
#cardiovascular_disease 
#coronary_heart_disease
#LDL 
#HDL
#triglycerides 
#blood_glucose
#ApoB 
#ApoA1
#LDLp
#oxidative_stress
#c_reactive_protein 
#TBARs
#phytosterols 
#processed_food 
#nutrition