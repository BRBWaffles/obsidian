https://pubmed.ncbi.nlm.nih.gov/31038679/

"The findings suggest that a diet high in fruit, vegetables, and whole grains may be associated with reduced risk of frailty. Nevertheless, additional longitudinal studies are needed to confirm the association of dietary patterns with frailty."

#disease 
#frailty
#whole_grains 
#fruit 
#vegetables 
#plant_foods 
#nutrition