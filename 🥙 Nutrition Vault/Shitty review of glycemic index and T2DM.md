Livesey, Geoffrey, et al. ‘Dietary Glycemic Index and Load and the Risk of Type 2 Diabetes: Assessment of Causal Relations’. _Nutrients_, vol. 11, no. 6, June 2019, p. E1436. _PubMed_, https://doi.org/10.3390/nu11061436.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/31242690/

**Conclusions:**
>Glycemic index and glycemic load are dietary factors probably causal of type 2 diabetes and should be considered by future dietary guideline committees for inclusion in food and nutrient-based recommendations.

**Notes:**
>- Virtually all results can be reconciled with the conventional T2DM pathogenesis paradigm while making fewer assumptions, with better supporting data from human RCTs.
>- Adjusting should be made clearer, as many high GI/GL foods correlate with many obesogenic dietary patterns.

**PDF:**
[[📂 Media/PDFs/nutrients-11-01436.pdf]]

**Supplements:**
[[📂 Media/PDFs/nutrients-11-01436-s001.pdf]]

| **Endpoints** | **Exposures**   | **Populations** | **General**          | **People**            |
| ------------- | --------------- | --------------- | -------------------- | --------------------- |
| #type_2_diabetes         | #glycemic_index | #multinational  | #disease             | #walter_willett       |
|               | #glycemic_load  |                 | #epidemiology        | #antonia_trichopoulou |
|               | #carbohydrates  |                 | #low_carb_talking_points |                       |
|               |                 |                 | #clown_papers        |                       |
|               |                 |                 | #bradford_hill       |                       |
|               |                 |                 | #clownery            |                       |
|               |                 |                 | #nutrition           |                       |

****