Stockwell, Tim, et al. ‘Do “Moderate” Drinkers Have Reduced Mortality Risk? A Systematic Review and Meta-Analysis of Alcohol Consumption and All-Cause Mortality’. _Journal of Studies on Alcohol and Drugs_, vol. 77, no. 2, Mar. 2016, pp. 185–98. _PubMed_, https://doi.org/10.15288/jsad.2016.77.185.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/26997174/

**Alcohol Intake Strata and ACM:**
![[Pasted image 20220219152720.png]]

**Conclusions:**
>In summary, our study suggests that a skeptical position is warranted in relation to the evidence that low-volume consumption is associated with net health benefits. This conclusion is consistent with a recent Mendelian randomization study that found that a genetic variant associated with reduced drinking lowered rather than increased cardiovascular risk among low-volume drinkers (Holmes et al., 2014). We recommend that future prospective studies on alcohol and health minimize bias attributable to the misclassification of former and occasional drinkers by carefully excluding these from the abstainer reference group

**PDF:**
[[📂 Media/PDFs/jsad.2016.77.185.pdf]]

**Supplements:**


| **Endpoints** | **Exposures** | **Populations** | **General**        | **People** |
| ------------- | ------------- | --------------- | ------------------ | ---------- |
| #all_cause_mortality          | #alcohol      | #humans         | #nutrition         |            |
|               |               | #multinational  | #disease           |            |
|               |               |                 | #meta_analysis     |            |
|               |               |                 | #systematic_review |            |
|               |               |                 | #processed_food    |            |
|               |               |                 | #fermented_food    |            |

****

Liu, Pin-ming, et al. ‘[Alcohol intake and stroke in Eastern Asian men:a systemic review and meta-analysis of 17 prospective cohort studies]’. _Zhonghua Yi Xue Za Zhi_, vol. 90, no. 40, Nov. 2010, pp. 2834–38.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/21162794/

**Conclusions:**
>In Eastern Asian men, light alcohol intake (≤ 20 g/d) is associated with a lowered risk of ischemic stroke whereas heavy alcohol intake is associated with an elevated risk of stroke, particularly hemorrhagic stroke and all-cause mortality.

**PDF:**
[[📂 Media/PDFs/东亚男性饮酒和卒中关系的前瞻性队列研究荟萃分析.pdf]]

**Supplements:**


| **Endpoints** | **Exposures** | **Populations** | **General**        | **People** |
| ------------- | ------------- | --------------- | ------------------ | ---------- |
| #stroke       | #alcohol      | #humans         | #nutrition         |            |
|               |               | #asia           | #disease           |            |
|               |               | #multinational  | #systematic_review |            |
|               |               |                 | #processed_food    |            |
|               |               |                 | #fermented_food    |            |

****