https://zbib.org

**Link:**
https://academic.oup.com/aje/article/179/3/282/103471

**Meta-Analysis:**
![[Pasted image 20220214174618.png]]

**Dose-Response**
![[Pasted image 20220214174639.png]]

**Conclusions:**
>In conclusion, findings from this meta-analysis showed a positive association between the consumption of red meat, particularly processed meat, and all-cause mortality. These results add to and extend the evidence that high red meat consumption may have adverse health consequences.

**PDF:**
[[📂 Media/PDFs/becerra-cln-repl 1.pdf]]

**Supplements:**


| **Endpoints** | **Exposures**   | **Populations**                      | **General**        | **People**       |
| ------------- | --------------- | ------------------------------------ | ------------------ | ---------------- |
| #all_cause_mortality          | #red_meat       | #united_states                       | #disease           | #susanna_larsson | 
|               | #processed_meat | #china                               | #nutrition         |                  |
|               | #meat           | #OXCHECK                             | #epidemiology      |                  |
|               | #animal_foods   | #united_kingdom                      | #meta_analysis     |                  |
|               | #animal_protein | #shanghai_mens_health_study          | #systematic_review |                  |
|               |                 | #shanghai_womens_health_study        |                    |                  |
|               |                 | #NIH_AARP                            |                    |                  |
|               |                 | #health_professionals_followup_study |                    |                  |
|               |                 | #nurses_health_study                 |                    |                  |
|               |                 | #EPIC_cohort                         |                    |                  |
|               |                 | #NHANES                              |                    |                  |

****