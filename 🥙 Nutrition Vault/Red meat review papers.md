Yip, C. S. C., et al. ‘A Summary of Meat Intakes and Health Burdens’. _European Journal of Clinical Nutrition_, vol. 72, no. 1, Jan. 2018, pp. 18–29. _PubMed_, https://doi.org/10.1038/ejcn.2017.117.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/28792013/

**Included Studies:**
![[Pasted image 20220213232022.png]]

**Meta-Analysis:**
![[Pasted image 20220213232103.png]]

**Dose-Responses:**
![[Pasted image 20220213232207.png]]

**Relative Risks:**
![[Pasted image 20220213232635.png]]

**PDF:**
[[📂 Media/PDFs/ejcn.2017.117.pdf]]

| **Endpoints**      | **Exposures**   | **Populations** | **General**        | **People** |
| ------------------ | --------------- | --------------- | ------------------ | ---------- |
| #all_cause_mortality               | #red_meat       | #united_states  | #disease           | #yip       |
| #cancer            | #meat           | #japan          | #mortality         |            |
| #cardiovascular_disease               | #animal_foods   | #europe         | #nutrition         |            |
| #coronary_heart_disease               | #processed_meat | #australia      | #cohort_studies    |            |
| #stroke            |                 | #canada         | #meta_analysis     |            |
| #type_2_diabetes              |                 | #south_america  | #systematic_review |            |
| #colorectal_cancer |                 |                 | #public_health     |            |
| #colon_cancer      |                 |                 |                    |            |
| #rectal_cancer     |                 |                 |                    |            |
| #esophageal_cancer |                 |                 |                    |            |
| #pancreatic_cancer |                 |                 |                    |            |
| #stomach_cancer    |                 |                 |                    |            |
| #lung_cancer       |                 |                 |                    |            |

****