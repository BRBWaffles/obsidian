https://pubmed.ncbi.nlm.nih.gov/30335720/

"Weight class athletes consuming an ad libitum LCKD decreased body mass and achieved lifting performances that were comparable with their UD. Coaches and athletes should consider using an LCKD to achieve targeted weight reduction goals for weight class sports."

https://pubmed.ncbi.nlm.nih.gov/22283635/

"LFD&PRE, CRD, and CRD&PRE preserve FFM similarly. PRE is an important component of a LFD during weight loss in this population."

#keto 
#strength 
#hypertrophy 
#exercise 
#nutrition 