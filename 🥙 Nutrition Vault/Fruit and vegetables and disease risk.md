https://pubmed.ncbi.nlm.nih.gov/28338764/

"Fruit and vegetable intakes were associated with reduced risk of cardiovascular disease, cancer and all-cause mortality. These results support public health recommendations to increase fruit and vegetable intake for the prevention of cardiovascular disease, cancer, and premature mortality."

https://pubmed.ncbi.nlm.nih.gov/25073782/

"This meta-analysis provides further evidence that a higher consumption of fruit and vegetables is associated with a lower risk of all cause mortality, particularly cardiovascular mortality."

https://pubmed.ncbi.nlm.nih.gov/31382535/

"Given a common co-occurrence of two non-communicable diseases in the elderly and the low frequency of fruit and vegetable consumption in this population, interventions to promote consuming five or more servings per day could have a significant positive impact on reducing mortality."

https://pubmed.ncbi.nlm.nih.gov/30639206/

"This systematic review supports existing recommendations for fruit and vegetable intakes. Current comparative risk assessments might significantly underestimate the protective associations of fruit and vegetable intakes."

https://pubmed.ncbi.nlm.nih.gov/30995309/

"The results support the need to consider the degree of food processing in future epidemiological studies and randomized controlled trials in order to adjust official recommendations for fruit consumption."

https://pubmed.ncbi.nlm.nih.gov/33000670/

"Conclusions Fruits and vegetables are associated with cardiovascular benefit, with some sources associated with greater benefit and none showing an adverse association."

https://pubmed.ncbi.nlm.nih.gov/11171873/

"Our results suggest an inverse association between vegetable intake and risk of CHD. These prospective data support current dietary guidelines to increase vegetable intake for the prevention of CHD."

#fruit
#vegetables
#cardiovascular_disease
#cancer
#all_cause_mortality
#type_2_diabetes 
#nutrition
#stroke 
#coronary_heart_disease 
#plant_foods
#nutrition 


