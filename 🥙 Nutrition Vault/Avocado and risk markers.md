Wang, Li, et al. ‘A Moderate-Fat Diet with One Avocado per Day Increases Plasma Antioxidants and Decreases the Oxidation of Small, Dense LDL in Adults with Overweight and Obesity: A Randomized Controlled Trial’. _The Journal of Nutrition_, vol. 150, no. 2, Feb. 2020, pp. 276–84. _PubMed_, https://doi.org/10.1093/jn/nxz231.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/31616932/

**Conclusions:**
>Avocados have a unique nutrient and bioactive profile that
appears to play an important role in reducing LDL oxidation,
hence decreasing LDL atherogenicity. Additional long-term
prospective and intervention studies are needed to evaluate the
effect of avocado consumption on clinical CVD outcomes and
determine the role that avocados may play in the primary and
secondary prevention of CVD.

**PDF:**
[[📂 Media/PDFs/jn%2Fnxz231.pdf]]

**Supplements:**


| **Endpoints** | **Exposures** | **Populations** | **General** | **People** |
| ------------- | ------------- | --------------- | ----------- | ---------- |
| #LDL          | #avocados     | #humans         | #disease    |            |
| #oxLDL        | #fruit        |                 | #cardiovascular_disease        |            |
| #blood_lipids | #dietary_fat  |                 | #coronary_heart_disease        |            |
|               | #monounsaturated_fat         |                 | #nutrition  |            |

****

Wang, Li, et al. ‘Abstract 17: One Avocado Per Day Lowers Plasma Oxidized-LDL and Increases Plasma Antioxidants in Overweight and Obese Adults’. _Circulation_, vol. 131, no. suppl_1, Mar. 2015, pp. A17–A17. _ahajournals.org (Atypon)_, https://doi.org/10.1161/circ.131.suppl_1.17.

**Link:**
https://www.ahajournals.org/doi/10.1161/circ.131.suppl_1.17

****


**Conclusions:**
>Including one avocado per day in a heart-healthy diet lowers plasma oxidized LDL and lutein concentration; the benefits extend beyond their fatty acid content. The change in oxidized LDL by diet was correlated with a change in small LDL but not large LDL particles.

**PDF:**


**Supplements:**


| **Endpoints** | **Exposures** | **Populations** | **General** | **People** |
| ------------- | ------------- | --------------- | ----------- | ---------- |
| #LDL          | #avocados     | #humans         | #disease    |            |
| #oxLDL        | #fruit        |                 | #cardiovascular_disease        |            |
| #blood_lipids | #dietary_fat  |                 | #coronary_heart_disease        |            |
|               | #monounsaturated_fat         |                 |             |            |

****