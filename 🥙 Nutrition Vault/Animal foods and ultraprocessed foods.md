https://pubmed.ncbi.nlm.nih.gov/35199827/

**PDF:**
[[📂 Media/PDFs/Ultra-processed_food_intake_and_animal-based_food_intake_and_mortality_in_the_Adventist_Health_Study-2.pdf]]

**Supplement:**
[[📂 Media/Misc/AH2_UP_Supp.docx]]

#nutrition 
#adventist_health_study 
#ultraprocessed_foods 
#processed_food 
#animal_foods 
#meat 
#low_carb_talking_points 
