https://pubmed.ncbi.nlm.nih.gov/30809527/

"In conclusion, all these findings provide new knowledge that could lead to re-establishment of the role of olive oil in human nutrition."

https://pubmed.ncbi.nlm.nih.gov/26136585/

"The consumption of olive oil polyphenols decreased plasma LDL concentrations and LDL atherogenicity in healthy young men."

https://pubmed.ncbi.nlm.nih.gov/15168036/

"Sustained consumption of virgin olive oil with the high phenolic content was more effective in protecting LDL from oxidation and in rising HDL cholesterol levels than that of other type of olive oils. Dose-dependent changes in oxidative stress markers, and phenolic compounds in urine, were observed with the phenolic content of the olive oil administered. Our results support the hypothesis that virgin olive oil consumption could provide benefits in the prevention of oxidative processes."

https://pubmed.ncbi.nlm.nih.gov/31539817/

"Olive oil taken on a regular basis can be a good dietary fat alternative, especially to manage IL-6. However, further research is required to clarify the effects of olive oil consumption on inflammation, comparing to other fats. Moreover, olive oil daily dosage, different time-length intervention and follow-up periods should be taken into consideration."

#disease
#olive_oil 
#monounsaturated_fat 
#LDL 
#oxLDL
#LPL
#ApoB 
#LDLp 
#HDL
#triglycerides
#blood_glucose 
#blood_lipids
#polyphenols
#inflammation 
#interleukin_6 
#c_reactive_protein
#tumor_necrosis_factor
#nutrition