https://www.frontiersin.org/articles/10.3389/fsufs.2020.544984/full#h12

pdf:
[[📂 Media/PDFs/fsufs-04-544984 (1).pdf]]

supp:
[[📂 Media/Misc/Data_Sheet_1_Ecosystem Impacts and Productive Capacity of a Multi-Species Pastured Livestock System.xlsx]]

#nutrition 
#animal_foods 
#animal_agriculture 
#clownery 