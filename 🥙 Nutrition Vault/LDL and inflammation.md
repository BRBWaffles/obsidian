https://pubmed.ncbi.nlm.nih.gov/32544081/

"This study validated the effects of residual inflammation risk in patients with low LDL-c Level in a general population, using long-term burdens of hs-CRP or LDL-c other than a single time-point level."

https://pubmed.ncbi.nlm.nih.gov/29530884/

"LDL-C reduction with evolocumab reduces cardiovascular events across hsCRP strata with greater absolute risk reductions in patients with higher-baseline hsCRP. Event rates were lowest in patients with the lowest hsCRP and LDL-C."

#disease 
#LDL 
#inflammation 
#c_reactive_protein 
#coronary_heart_disease 
#cardiovascular_disease
#all_cause_mortality
#lipidology 
#blood_lipids 