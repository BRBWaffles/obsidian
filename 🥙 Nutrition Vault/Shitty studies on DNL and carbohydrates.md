Sanders, Francis W. B., et al. ‘Hepatic Steatosis Risk Is Partly Driven by Increased de Novo Lipogenesis Following Carbohydrate Consumption’. _Genome Biology_, vol. 19, no. 1, June 2018, p. 79. _PubMed_, https://doi.org/10.1186/s13059-018-1439-8.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/29925420/

**PDF:**
![[📂 Media/PDFs/13059_2018_Article_1439.pdf]]

| **Endpoints** | **Exposures**  | **Populations** | **General**   |
| ------------- | -------------- | --------------- | ------------- |
| #non_alcoholic_fatty_liver_disease        | #carbohydrates | #humans         | #disease      |
|               | #dietary_fat   | #mice           | #clown_papers |
|               |                |                 | #nutrition    |
|               |                |                 | #de_novo_lipogenesis          |
|               |                |                 | #clownery     | 
|               |                |                 |               |

****

Lustig, Robert H., et al. ‘Isocaloric Fructose Restriction and Metabolic Improvement in Children with Obesity and Metabolic Syndrome’. _Obesity (Silver Spring, Md.)_, vol. 24, no. 2, Feb. 2016, pp. 453–60. _PubMed_, https://doi.org/10.1002/oby.21371.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/26499447/

**PDF:**
[[📂 Media/PDFs/nihms-728529.pdf]]


| **Endpoints**        | **Exposures** | **Populations** | **General**          | **People**     |
| -------------------- | ------------- | --------------- | -------------------- | -------------- |
| #non_alcoholic_fatty_liver_disease               | #fructose     | #humans         | #low_carb_talking_points | #robert_lustig |
| #weight_loss         |               | #children       | #clown_papers        |                |
| #blood_lipids        |               |                 | #nutrition           |                |
| #blood_glucose       |               |                 | #disease             |                |
| #LDL                 |               |                 | #clownery            |                |
| #HDL                 |               |                 |                      |                |
| #triglycerides                  |               |                 |                      |                |
| #insulin_sensitivity |               |                 |                      |                |
| #blood_pressure      |               |                 |                      |                |
| #anthropometics      |               |                 |                      |                |

****