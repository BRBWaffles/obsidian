https://pubmed.ncbi.nlm.nih.gov/28320601/

"The Tsimane of Bolivia still experience both coronary heart disease and coronary artery calcification."

https://gurven.anth.ucsb.edu/sites/default/files/sitefiles/papers/irimiaetal2021.pdf

"Brain volume as a % of ICV inversely correlates with age. We have data on this metric in populations where ages are known. Compared to these populations, the estimates of Tsimane ages seem to be overestimated by about 10-15 years for ~85 year olds"

https://gurven.anth.ucsb.edu/sites/secure.lsit.ucsb.edu.anth.d7_gurven/files/sitefiles/papers/horvathetal2016.pdf

"2. DNA Methylation Age estimates Similar story.
0-40: No apparent age discrepancy, seems accurate.
40-50: ~5 years overestimation
50-60: ~7.5 years overestimation 
60+ : ~10-20 years overestimation"

https://immunityageing.biomedcentral.com/articles/10.1186/s12979-019-0165-8

"CD8 Naive T cell has a strong inverse correlation with age. Tsimane have higher CD8 Naive T cells that would be expected based on their age, indicating a lower true age. Again, based on the numbers this may be ~10 or even 20+ years of an overestimation too. Though this has more variance than the DNA methylation correlations."

![[Pasted image 20220202165620.png]]

"What explains all 4 of these findings is really simple. The ages of the Tsimane were overestimated and correcting the discrepancy fits them right back into MESA CAC range."

![[Pasted image 20220202165704.png]]

"This is what the graph looks like when we include general population point estimates. Assuming the DNA methylation age is the true age, it would mean there's an overestimation of age."

![[Pasted image 20220202170027.png]]

#disease
#primitive_cultures 
#ancestral_food
#coronary_heart_disease
#coronary_artery_calcification
#antagonistic_pleiotropy
#senescence
#aging