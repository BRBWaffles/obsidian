Liu, Pin-ming, et al. ‘[Alcohol intake and stroke in Eastern Asian men:a systemic review and meta-analysis of 17 prospective cohort studies]’. _Zhonghua Yi Xue Za Zhi_, vol. 90, no. 40, Nov. 2010, pp. 2834–38.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/21162794/

****


**Conclusions:**
>In Eastern Asian men, light alcohol intake (≤ 20 g/d) is associated with a lowered risk of ischemic stroke whereas heavy alcohol intake is associated with an elevated risk of stroke, particularly hemorrhagic stroke and all-cause mortality.

**PDF:**


**Supplements:**


| **Endpoints** | **Exposures** | **Populations** | **General** | **People** |
| ------------- | ------------- | --------------- | ----------- | ---------- |
|               |               |                 | #nutrition  |            |

****