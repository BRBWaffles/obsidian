https://pubmed.ncbi.nlm.nih.gov/32644139/

"The findings of this meta-analysis suggest that the adverse association of SSBs with MetS does not extend to other food sources of fructose-containing sugars, with a protective association for yogurt and fruit throughout the dose range and for 100% fruit juice and mixed fruit juices at moderate doses. Therefore, current policies and guidelines on the need to limit sources of free sugars may need to be reexamined."

#disease 
#metabolic_syndrome 
#sugar
#sugar_sweetened_beverages
#fruit 
#yogurt
#fructose 
#nutrition