https://pubmed.ncbi.nlm.nih.gov/19110020/

"Adding naturally occurring levels of fiber to juice did not enhance satiety. These results suggest that solid fruit affects satiety more than pureed fruit or juice, and that eating fruit at the start of a meal can reduce energy intake."

https://pubmed.ncbi.nlm.nih.gov/31139631/

"Current evidence suggests that whole, fresh fruit consumption is unlikely to contribute to excess energy intake and adiposity, but rather has little effect on these outcomes or constrains them modestly. Single-meal RCTs, RCTs lasting 3-24 weeks, and long-term observational studies are relatively consistent in supporting this conclusion. Whole, fresh fruit probably does not contribute to obesity and may have a place in the prevention and management of excess adiposity."

https://pubmed.ncbi.nlm.nih.gov/19110020/

"Overall, whole apple increased satiety more than applesauce or apple juice. Adding naturally occurring levels of fiber to juice did not enhance satiety. These results suggest that solid fruit affects satiety more than pureed fruit or juice, and that eating fruit at the start of a meal can reduce energy intake."

#fruit
#disease
#obesity
#weight_gain
#satiety
#energy_intake
#processed_food 
#plant_foods
#blood_lipids 
#nutrition 