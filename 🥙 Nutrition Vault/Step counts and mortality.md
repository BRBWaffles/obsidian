Inoue, Kosuke, et al. ‘Association of Daily Step Patterns With Mortality in US Adults’. _JAMA Network Open_, vol. 6, no. 3, Mar. 2023, p. e235174. _PubMed_, https://doi.org/10.1001/jamanetworkopen.2023.5174.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/36976556/

****


**Conclusions:**
>In this cohort study of US adults, the number of days per week taking 8000 steps or more was associated with a lower risk of all-cause and cardiovascular mortality in a curvilinear fashion. These findings suggest that individuals may receive substantial health benefits by walking just a couple days a week.

**PDF:**
![[📂 Media/PDFs/inoue_2023_oi_230184_1678995959.82466.pdf]]

**Supplements:**
![[📂 Media/PDFs/zoi230184supp1_prod_1678995959.83466.pdf]]

| **Endpoints**           | **Exposures** | **Populations** | **General**     | **People** |
| ----------------------- | ------------- | --------------- | --------------- | ---------- |
| #all_cause_mortality    | #steps        | #humans         | #cohort_studies |            |
| #cardiovascular_disease | #exercise     | #NHANES         | #mortality      |            |
|                         |               | #united_states  | #disease        |            |
|                         |               |                 |                 |            |
|                         |               |                 |                 |            |
|                         |               |                 |                 |            |
|                         |               |                 |                 |            |
|                         |               |                 |                 |            |
|                         |               |                 |                 |            |

****