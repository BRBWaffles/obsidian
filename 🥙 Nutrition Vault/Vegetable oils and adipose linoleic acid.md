Guyenet, Stephan J., and Susan E. Carlson. ‘Increase in Adipose Tissue Linoleic Acid of US Adults in the Last Half Century’. _Advances in Nutrition (Bethesda, Md.)_, vol. 6, no. 6, Nov. 2015, pp. 660–64. _PubMed_, https://doi.org/10.3945/an.115.009944.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/26567191/

**Adipose LA vs Year:**
![[Pasted image 20220223152022.png]]

**Adipose LA vs LA Intake**
![[Pasted image 20220223152056.png]]

**Conclusions:**
>Our findings suggest that adipose tissue LA has increased substantially in the United States over the last half century, and, to our knowledge, for the first time demonstrate that this increase is highly correlated with increased dietary LA intake over the same period. Interestingly, 3 recent investigations (2013–2015) of adipose tissue LA in European countries found concentrations of 11% (57), 12.5% (58), and 10.6% (59). These values are similar to earlier reports from the United States. Because LA is in involved in numerous physiologic and pathophysiologic processes, these changes have potentially significant implications for public health. These findings call for further research into the consequences, positive or negative, of such a major shift in the adipose tissue concentration of a single bioactive FA occurring over a relatively short period of time in the United States.

**PDF:**
[[📂 Media/PDFs/an009944.pdf]]

| **Endpoints**  | **Exposures**  | **Populations** | **General**   | **People** |
| -------------- | -------------- | --------------- | ------------- | ---------- |
| #linoleic_acid | #vegetable_oil | #humans         | #nutrition    |            |
|                | #plant_fats    | #united_states  | #epidemiology |            |
|                |                |                 |               |            |
|                |                |                 |               |            |
|                |                |                 |               |            |
|                |                |                 |               |            |
|                |                |                 |               |            |
|                |                |                 |               |            |
|                |                |                 |               |            |

****