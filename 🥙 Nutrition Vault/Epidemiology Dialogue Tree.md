---

mindmap-plugin: basic

---

# Epi Denial Dialogue Tree

- An n=200 RCT
  Compound X vs placebo
  100% mortality with compound X
  Do you infer causality?
	- Yes.
		- An n=200 non-randomized CT
		  Self-selected and self-assigned
		  Compound X vs placebo
		  100% mortality with compound X
		  Do you infer causality?
			- Yes.
				- An n=200 non-randomized CT
				  Self-selected and self-assigned
				  High compound X vs low compound X
				  20% more death with compound X
				  Do you infer causality?
					- Yes.
						- An n=200,000 cohort study
						  High compound X vs low compound X
						  Adjustment for confounders and covariates.
						  20% more death with compound X
						  Do you infer causality?
							- Yes.
								- You are now an epidemiology respecter.
							- No.
								- Potential contradiction.
					- No.
						- Absurdity.
			- No.
				- Absurdity.
	- No.
		- Absurdity.

#debate 
#epidemiology 
#epistemology 
#philosophy
#disease 
#causal_inference 
