https://pubmed.ncbi.nlm.nih.gov/30694319/

"Triglyceride-lowering LPL variants and LDL-C-lowering LDLR variants were associated with similar lower risk of CHD per unit difference in ApoB. Therefore, the clinical benefit of lowering triglyceride and LDL-C levels may be proportional to the absolute change in ApoB."

https://pubmed.ncbi.nlm.nih.gov/21392724/

"For individuals with discordant LDL-C and LDL-P levels, the LDL-attributable atherosclerotic risk is better indicated by LDL-P."

https://pubmed.ncbi.nlm.nih.gov/32203549/

#disease 
#lipidology 
#blood_lipids 
#coronary_heart_disease 
#cardiovascular_disease 
#LDL_discordance
#LDL 
#ApoB