https://pubmed.ncbi.nlm.nih.gov/27472409/

"LDL and other cholesterol-rich, apoB-containing lipoproteins, once they become retained and modified within the arterial wall, cause atherosclerosis. This simple, robust pathophysiologic understanding may finally allow us to eradicate ASCVD, the leading killer in the world."

https://pubmed.ncbi.nlm.nih.gov/7749869/

"Although atherosclerosis is a complex and multifactorial process, we conclude that there exists a key pathogenic event, namely, lipoprotein retention, that is both necessary and sufficient to provoke lesion initiation in an otherwise-normal artery. Other potential contributors to early atherogenesis, such as hyperlipidemia; lipoprotein influx; lipoprotein modification; turbulent blood flow; and alterations in the endothelium, smooth muscle cells, and matrix, individually fail to meet this dual criterion of necessity and sufficiency. Lipoprotein retention, however, is an absolute requirement for lesion development and appears to be sufficient in most circumstances to provoke otherwise-normal cellular and matrix elements to participate in a cascade of events leading to atherosclerosis (see the Figure). Essentially all later events can be traced to these early changes."

https://pubmed.ncbi.nlm.nih.gov/17938300/

"The finding that certain human populations of individuals who maintain lifelong low plasma levels of apolipoprotein B lipoproteins have an approximately 90% decreased risk of coronary artery disease gives hope that our further understanding of the pathogenesis of this leading killer could lead to its eradication."

https://pubmed.ncbi.nlm.nih.gov/9637699/

"LDL with normal receptor-binding activity but with severely impaired proteoglycan binding will be a unique resource for analyzing the importance of LDL- proteoglycan interaction in atherogenesis. If the subendothelial retention of LDL by proteoglycans is the initial event in early atherosclerosis, then LDL with defective proteoglycan binding may have little or no atherogenic potential."

https://pubmed.ncbi.nlm.nih.gov/22176921/

"The LDL-proteoglycan interaction is a highly regulated process that might provide new therapeutic targets against cardiovascular disease."

https://pubmed.ncbi.nlm.nih.gov/12066187/

"We conclude that subendothelial retention of apoB100-containing lipoprotein is an early step in atherogenesis."

https://pubmed.ncbi.nlm.nih.gov/14726411/

"Site A in apoB100 becomes functional in sPLA2-modified LDL and acts cooperatively with site B resulting in increased proteoglycan-binding activity. The increased binding for proteoglycans of cholesterol-enriched LDL is solely dependent on site B."

https://pubmed.ncbi.nlm.nih.gov/8842355/

"Therefore, three properties of LDL: circulating levels, oxidizability, and affinity with intima proteoglycans, that may modulate its atherogenicity, were shifted in a favorable direction by diets rich in linoleic acid and natural antioxidants."

https://pubmed.ncbi.nlm.nih.gov/9261142/

"The present results show that, after modification of the lysine residues of apoB-100 during oxidation, the binding of LDL to proteoglycans is decreased, and suggest that oxidation of LDL tends to lead to intracellular rather than extracellular accumulation of LDL during atherogenesis."

https://pubmed.ncbi.nlm.nih.gov/25528432/

#disease 
#lipidology 
#blood_lipids 
#LDL_retention
#proteoglycans
#coronary_heart_disease 
#cardiovascular_disease 
#LDL 
#ApoB 