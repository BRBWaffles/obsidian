https://pubmed.ncbi.nlm.nih.gov/30042516/

"These data suggest that, while isoflavones were detected in SPC, chronic WPC or SPC supplementation did not appreciably affect biomarkers related to muscle androgenic signaling or SQ estrogenic signaling. The noted fiber type-specific responses to WPC and SPC supplementation warrant future research."

https://pubmed.ncbi.nlm.nih.gov/33383165/

https://matt576.medium.com/soy-stupefaction-4ea0192f3f2c

#disease
#nutrition
#phytoestrogens
#isoflavones
#legumes
#soy
#hormones
#plant_foods
#plant_protein 
#nutrition