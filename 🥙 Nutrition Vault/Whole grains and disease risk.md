https://pubmed.ncbi.nlm.nih.gov/11305627/

"Protection by whole grain intake against chronic disease is suggested in Norway, where four times as much whole grain is consumed as in the United States."

#whole_grains
#plant_foods 
#bread
#blood_lipids 
#all_cause_mortality
#carbohydrates
#nutrition 

