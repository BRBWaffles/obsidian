https://pubmed.ncbi.nlm.nih.gov/29666853/

"These findings indicated no significant association between a high intake of soy products and all-cause, CVD, and cancer mortality. Further studies are needed to clarify the association between the types of soy products and the risk of"

https://pubmed.ncbi.nlm.nih.gov/31915830/

"Dietary intakes of tofu, soy protein, and soy isoflavones, but not total legumes or total soy, are inversely associated with incident type 2 diabetes. Our findings support recommendations to increase intakes of certain soy products for the prevention of type 2 diabetes. However, the overall quality of evidence was low and more high-quality evidence from prospective studies is needed."

https://pubmed.ncbi.nlm.nih.gov/31754945/

"The CKB study demonstrated that moderate soy intake was not associated with breast cancer risk among Chinese women. Higher amount of soy intake might provide reasonable benefits for the prevention of breast cancer."

https://pubmed.ncbi.nlm.nih.gov/31278047/

"Soy and its isoflavones may favorably influence risk of mortality. In addition, soy protein intake was associated with a decreased risk in the mortality of breast cancer. Our findings may support the current recommendations to increase intake of soy for greater longevity."

https://pubmed.ncbi.nlm.nih.gov/28067550/

"There were no associations between soy isoflavones consumption and risk of cardiovascular disease, stroke, and coronary heart disease. Conclusion Overall evidence indicated that consumption of soy was negatively associated with the risk of cardiovascular disease, stroke, and coronary heart disease risk."

https://pubmed.ncbi.nlm.nih.gov/29407270/

"Soy products and soy constituents (soy protein and soy isoflavones) may be associated with a lower risk of type 2 diabetes mellitus. Future studies should focus on the dose-response effect and the mechanism."

https://pubmed.ncbi.nlm.nih.gov/24586662/

"We meta-analyzed more and newer research results, and separated women according to menopausal status to explore soy isoflavone-breast cancer association. We founded that soy isoflavone intake could lower the risk of breast cancer for both pre- and post-menopausal women in Asian countries. However, for women in Western countries, pre- or post-menopausal, there is no evidence to suggest an association between intake of soy isoflavone and breast cancer."

https://pubmed.ncbi.nlm.nih.gov/26683956/

"Current evidence indicates that soy food intake is associated with lower endometrial cancer risk. Further larger cohort studies are warranted to fully clarify such an association."

https://pubmed.ncbi.nlm.nih.gov/28816973/

"In male, miso soup intake (1-5 cups/day) was significantly associated with GC risk.High intake of nonfermented soy food might reduce the risk of GC, while miso soup intake might increase the risk in male."

https://pubmed.ncbi.nlm.nih.gov/23725149/

"Our meta- analysis showed that soy food intake might be associated with better survival, especially for ER negative, ER+/ PR+, and postmenopausal patients."

https://pubmed.ncbi.nlm.nih.gov/26228387/

"The overall current literature suggests that phytoestrogen intake is associated with a decreased risk of PCa, especially genistein and daidzein intake. Increased serum concentration of enterolactone was also associated with a significant reduced risk of PCa. Further efforts should be made to clarify the underlying biological mechanisms."

https://pubmed.ncbi.nlm.nih.gov/27927636/

#disease
#cancer
#breast_cancer 
#prostate_cancer
#endometrial_cancer
#fermented_food
#gastric_cancer
#all_cause_mortality
#cardiovascular_disease
#coronary_heart_disease
#stroke
#type_2_diabetes
#post_diagnosis
#postmenopausal 
#premenopausal 
#phytoestrogens
#isoflavones
#legumes
#soy
#plant_foods
#plant_protein
#nutrition