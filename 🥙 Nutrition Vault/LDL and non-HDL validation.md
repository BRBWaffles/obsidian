https://pubmed.ncbi.nlm.nih.gov/21356116/

"The discrimination power of non-HDL-C is similar to that of apoB to rank diabetic patients according to atherogenic cholesterol and lipoprotein burden. Since true correlation between variables reached unity, non-HDL-C may provide not only a metabolic surrogate but also a candidate biometrical equivalent to apoB, as non-HDL-C calculation is readily available."

#disease 
#lipidology 
#blood_lipids 
#LDL 
#HDL 
#ApoB 
#validation