https://pubmed.ncbi.nlm.nih.gov/30412134/

"The evidence suggested that citizens could benefit from increased whole grain intake for reducing systemic inflammation. Further well-designed studies are required to investigate the mechanism under the appearance."

https://pubmed.ncbi.nlm.nih.gov/31301131/

"However, beneficial effects of whole grains were found in some subgroups. Given the high between-study heterogeneity, deriving firm conclusions is difficult."

https://pubmed.ncbi.nlm.nih.gov/28230784/

"Increasing consumption of WGs can alter parameters of health, but more research is needed to better elucidate the relationship between the amount consumed and the health-related outcome."

https://pubmed.ncbi.nlm.nih.gov/29305946/

"Whole-grains reduced diabetes risk and the mechanisms appear to work through reduced post-prandial blood glucose and peripheral insulin resistance that were statistically linked to enhanced metabolic flexibility."

#whole_grains 
#LDL 
#HDL 
#blood_glucose 
#plant_foods 
#plant_protein 
#carbohydrates
#interleukin_6 
#inflammation 
#insulin_sensitivity 
#metabolic_flexibility
#refined_grains
#c_reactive_protein 
#tumor_necrosis_factor 
#nutrition 