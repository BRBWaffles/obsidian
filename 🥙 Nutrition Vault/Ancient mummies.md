https://pubmed.ncbi.nlm.nih.gov/23489753/

"Ancient Egyptians, ancient Peruvians, the Ancestral Puebloans of southwest America, and the Unangan of the Aleutian Islands all experienced coronary heart disease and coronary artery calcification thousands of years ago."

https://europepmc.org/article/med/29100258

"5000 year old man frozen in ice consumed an ancestral diet and had coronary heart disease and coronary artery calcification."

https://pubmed.ncbi.nlm.nih.gov/24948424/

"Evidence from mummies suggests that 18th and 19th century Lithuanians consuming their traditional high-meat diet still experienced coronary heart disease and coronary artery calcification."

https://pubmed.ncbi.nlm.nih.gov/25667092/

"Ancient Peruvians presumably consuming their traditional diet still had coronary heart disease."

#disease
#ancestral_food
#coronary_heart_disease
#coronary_artery_calcification
#primitive_cultures 
#antagonistic_pleiotropy