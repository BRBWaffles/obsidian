https://pubmed.ncbi.nlm.nih.gov/26016870/

"In contrast to milk fat without MFGM, milk fat enclosed by MFGM does not impair the lipoprotein profile. The mechanism is not clear although suppressed gene expression by MFGM correlated inversely with plasma lipids. The food matrix should be considered when evaluating cardiovascular aspects of different dairy foods."

https://pubmed.ncbi.nlm.nih.gov/25671415/

"In conclusion, our meta-analysis did not find sufficient evidence that whey and its derivates elicited a beneficial effect in reducing circulating CRP. However, they may significantly reduce CRP among participants with highly supplemental doses or increased baseline CRP levels."

https://pubmed.ncbi.nlm.nih.gov/31089732/

"The majority of studies documented a significant anti-inflammatory effect in both healthy and metabolically abnormal subjects, although not all the articles were of high quality."

#blood_lipids
#dairy
#saturated_fat
#milk
#LDL
#HDL
#triglycerides
#PCSK9
#ApoB
#nonHDL
#ApoA1
#cholesterol
#animal_fats
#animal_foods
#animal_protein
#protein
#c_reactive_protein
#inflammation
#whey
#nutrition