Louwman, M. W., et al. ‘Signs of Impaired Cognitive Function in Adolescents with Marginal Cobalamin Status’. _The American Journal of Clinical Nutrition_, vol. 72, no. 3, Sept. 2000, pp. 762–69. _PubMed_, https://doi.org/10.1093/ajcn/72.3.762.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/10966896/

**Definition of B12 Status:**
![[Pasted image 20220223150319.png]]

>This is a highly unusual definition of normal B12 status, as it does not use the reference range as a benchmark.

**Characteristics:**
![[Pasted image 20220223150350.png]]

>There is a clear gradation in B12 status across the group.

**Conclusions:**
>In conclusion, a shortage of cobalamin without the typical hematologic signs of cobalamin deficiency was associated with an impairment of cognitive performance that has consequences for daily life. These findings have implications not only for subjects consuming or previously consuming a macrobiotic diet, but for all subjects who avoid animal products, whether because of medical reasons, beliefs, or poverty

**PDF:**
[[📂 Media/PDFs/762.pdf]]

| **Endpoints**        | **Exposures** | **Populations** | **General**          | **People** |
| -------------------- | ------------- | --------------- | -------------------- | ---------- |
| #cognitive_function  | #vegan        | #humans         | #child_development   |            |
| #mental_health       | #plant_foods  | #children       | #nutrition           |            |
| #vitamin_B12         | #animal_foods | #adolescents    | #disease             |            |
| #nutrient_deficiency | #omnivore     |                 | #epidemiology        |            |
| #nutritional_status  |               |                 | #clownery            |            |
|                      |               |                 | #clown_papers        |            |
|                      |               |                 | #low_carb_talking_points |            |

****