https://pubmed.ncbi.nlm.nih.gov/34796724/

"By contrast, we found associations of SFAs with CHD in opposite directions dependent on the food source. These findings should be further confirmed, but support public health recommendations to consider food sources alongside the macronutrients they contain, and suggest the importance of the overall food matrix."

#nutrition 
#disease 
#low_carb_talking_points
#nutrition 
#epidemiology 
#saturated_fat 
#carbohydrates 

