Zhong, Victor W., et al. ‘Associations of Processed Meat, Unprocessed Red Meat, Poultry, or Fish Intake With Incident Cardiovascular Disease and All-Cause Mortality’. _JAMA Internal Medicine_, vol. 180, no. 4, Apr. 2020, pp. 503–12. _PubMed_, https://doi.org/10.1001/jamainternmed.2019.6969.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/32011623/

**Dose-Response Curves:**
![[Pasted image 20220214172516.png]]
![[Pasted image 20220214172532.png]]

**Meat and CVD Risk:**
![[Pasted image 20220214172843.png]]

**Conclusions:**
>This study’s findings suggest that among US adults, higher intake of processed meat, unprocessed red meat, or poultry, but not fish, was significantly associated with a small increased risk of incident CVD. Higher intake of processed meat or unprocessed red meat, but not poultry or fish, was significantly associated with a small increased risk of all-cause mortality.

**PDF:**
[[📂 Media/PDFs/jamainternal_zhong_2020_oi_190112.pdf]]

**Supplements:**
[[📂 Media/PDFs/jamainternmed-180-503-s001 (1).pdf]]

| **Endpoints** | **Exposures**   | **Populations** | **General**        | **People**    |
| ------------- | --------------- | --------------- | ------------------ | ------------- |
| #all_cause_mortality          | #red_meat       | #united_states  | #nutrition         | #victor_zhong |
| #cardiovascular_disease          | #meat           | #humans         | #disease           |               |
|               | #poultry        |                 | #meta_analysis     |               |
|               | #fish           |                 | #systematic_review |               |
|               | #animal_foods   |                 | #dose_response     |               |
|               | #animal_protein |                 | #epidemiology      |               |

****