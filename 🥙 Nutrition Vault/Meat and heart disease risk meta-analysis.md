Papier, Keren, et al. ‘Meat Consumption and Risk of Ischemic Heart Disease: A Systematic Review and Meta-Analysis’. _Critical Reviews in Food Science and Nutrition_, July 2021, pp. 1–12. _PubMed_, https://doi.org/10.1080/10408398.2021.1949575.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/34284672/

**Meta-Analysis:**
![[Pasted image 20220214173330.png]]

**Subgroup Analysis:**
![[Pasted image 20220214173914.png]]

**Conclusions:**
>In conclusion, this large meta-analysis of meat intake and IHD risk shows that unprocessed red and processed meat might be risk factors for IHD. This supports public health recommendations to reduce the consumption of unprocessed red and processed meat intake for the prevention of IHD.

**PDF:**
[[📂 Media/PDFs/Meat consumption and risk of ischemic heart disease A systematic review and meta analysis.pdf]]

**Supplements:**
[[📂 Media/Misc/bfsn_a_1949575_sm5775.docx]]

| **Endpoints** | **Exposures**   | **Populations**                      | **General**        | **People** |
| ------------- | --------------- | ------------------------------------ | ------------------ | ---------- |
| #coronary_heart_disease          | #red_meat       | #multinational                       | #nutrition         |            |
|               | #processed_meat | #united_states                       | #disease           |            |
|               | #poultry        | #australia                           | #epidemiology      |            |
|               | #meat           | #japan                               | #meta_analysis     |            |
|               | #animal_foods   | #japan_collaborative_cohort          | #systematic_review |            |
|               | #animal_protein | #nurses_health_study                 |                    |            |
|               |                 | #health_professionals_followup_study |                    |            |
|               |                 | #adventist_health_study              |                    |            |
|               |                 | #shanghai_womens_health_study        |                    |            |
|               |                 | #shanghai_mens_health_study          |                    |            |
|               |                 | #EPIC_cohort                         |                    |            |
|               |                 | #japan_public_health_center          |                    |            |
|               |                 | #UK_biobank                          |                    |            |
|               |                 | #PURE_study                          |                    |            |

****