https://pubmed.ncbi.nlm.nih.gov/32696704/

"Conclusions Replacing SSBs with noncaloric beverages for 12 months did not affect serum triglyceride to high-density lipoprotein cholesterol ratio. Among individuals with central adiposity, replacing SSBs with either ASBs or USBs lowered body weight. However, USBs may have the most favorable effect on sweet taste preference."

https://pubmed.ncbi.nlm.nih.gov/24493081/

https://pubmed.ncbi.nlm.nih.gov/30590448/

https://pubmed.ncbi.nlm.nih.gov/27900447/

https://pubmed.ncbi.nlm.nih.gov/24552754/

#disease 
#metabolic_syndrome 
#sugar
#all_cause_mortality 
#sugar_sweetened_beverages
#anthropometics 
#blood_lipids 
#LDL 
#HDL 
#triglycerides 
#visceral_adipose
#processed_food 
#carbohydrates 
#fructose
#nutrition