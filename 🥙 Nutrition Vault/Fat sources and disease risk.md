https://pubmed.ncbi.nlm.nih.gov/29566185/

"The largely different associations of plant MUFAs and animal MUFAs with CHD risk suggest that plant-based foods are the preferable sources of MUFAs for CHD prevention. These findings are observational and warrant confirmation in intervention settings."

https://pubmed.ncbi.nlm.nih.gov/31266749/

"In patients with type 2 diabetes, higher intake of PUFAs, in comparison with carbohydrates or saturated fatty acids, is associated with lower total mortality and cardiovascular disease mortality. These findings highlight the important role of quality of dietary fat in the prevention of cardiovascular disease and total mortality among adults with type 2 diabetes."

https://pubmed.ncbi.nlm.nih.gov/30636521/

"Intakes of SFAs, trans-fatty acids, animal MUFAs, α-linolenic acid, and arachidonic acid were associated with higher mortality. Dietary intake of marine omega-3 PUFAs and replacing SFAs with plant MUFAs or linoleic acid were associated with lower total, CVD, and certain cause-specific mortality."

https://pubmed.ncbi.nlm.nih.gov/30689516/

"Higher intake of plant MUFAs was associated with lower total mortality, and animal MUFAs intake was associated with higher mortality. Significantly lower mortality risk was observed when saturated fatty acids, refined carbohydrates, or trans fats were replaced by protein MUFAs, but not animal MUFAs. These data suggest that other constituents in animal foods, such as saturated fatty acids, may confound the associations for MUFAs when they are primarily derived from animal products. More evidence is needed to elucidate the differential associations of plant MUFAs and animal MUFAs with mortality."

https://pubmed.ncbi.nlm.nih.gov/26429077/

"Our findings indicate that unsaturated fats, especially PUFAs, and/or high-quality carbohydrates can be used to replace saturated fats to reduce CHD risk."

https://pubmed.ncbi.nlm.nih.gov/27479196/

#disease
#animal_fats
#plant_fats
#animal_foods
#plant_foods
#polyunsaturated_fat
#monounsaturated_fat
#saturated_fat
#trans_fat
#type_2_diabetes
#coronary_heart_disease
#linoleic_acid
#a_linolenic_acid
#docosahexaenoic_acid
#eicosapentaenoic_acid
#all_cause_mortality
#cardiovascular_disease
#whole_grains
#omega_3
#omega_6
#nutrition

