Tremblay, André J., et al. ‘Dietary Medium-Chain Triglyceride Supplementation Has No Effect on Apolipoprotein B-48 and Apolipoprotein B-100 Kinetics in Insulin-Resistant Men’. _The American Journal of Clinical Nutrition_, vol. 99, no. 1, Jan. 2014, pp. 54–61. _PubMed_, https://doi.org/10.3945/ajcn.113.068767.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/24172309/

****


**Conclusions:**
>In conclusion, the results of the present study show that 20 g
supplemental MCTs/d for 4 wk had no effect on plasma lipids,
TRL apo B-48– and apo B-100–containing lipoprotein kinetics,
or the intestinal expression of genes involved in lipoprotein and
fatty acid metabolism in IR subjects.

**PDF:**
![[📂 Media/PDFs/54.pdf]]

**Supplements:**


| **Endpoints** | **Exposures** | **Populations** | **General**  | **People** |
| ------------- | ------------- | --------------- | ------------ | ---------- |
| #kinetics     | #medium_chain_triglycerides          | #humans         | #nutrition   |            |
| #blood_lipids |               |                 | #dietary_fat |            |
| #ApoB         |               |                 |              |            |
|               |               |                 |              |            |
|               |               |                 |              |            |
|               |               |                 |              |            |
|               |               |                 |              |            |
|               |               |                 |              |            |
|               |               |                 |              |            |

****