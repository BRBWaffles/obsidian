https://pubmed.ncbi.nlm.nih.gov/28444290/

"Both the naturally randomized genetic studies and the randomized intervention trials consistently demonstrate that any mechanism of lowering plasma LDL particle concentration should reduce the risk of ASCVD events proportional to the absolute reduction in LDL-C and the cumulative duration of exposure to lower LDL-C, provided that the achieved reduction in LDL-C is concordant with the reduction in LDL particle number and that there are no competing deleterious off-target effects."

https://pubmed.ncbi.nlm.nih.gov/32052833/

"This second Consensus Statement on LDL causality discusses the established and newly emerging biology of ASCVD at the molecular, cellular, and tissue levels, with emphasis on integration of the central pathophysiological mechanisms. Key components of this integrative approach include consideration of factors that modulate the atherogenicity of LDL at the arterial wall and downstream effects exerted by LDL particles on the atherogenic process within arterial tissue."

#disease 
#LDL 
#coronary_heart_disease 
#cardiovascular_disease 
#lipidology 
#blood_lipids 
#ApoB 
#EAS_consensus
