https://pubmed.ncbi.nlm.nih.gov/21131862/

"We conclude that older adults require a higher dose of weekly loading than the young to maintain myofiber hypertrophy attained during a progressive RT program, yet gains in specific strength among older adults were well preserved and remained at or above levels of the untrained young."

#strength 
#hypertrophy
#fitness
#exercise 