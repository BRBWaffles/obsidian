https://pubmed.ncbi.nlm.nih.gov/33268459/

"Substituting high quality plant foods such as legumes, nuts, or soy for red meat might reduce the risk of CHD. Substituting whole grains and dairy products for total red meat, and eggs for processed red meat, might also reduce this risk."

https://pubmed.ncbi.nlm.nih.gov/7911176/

"An alpha-linolenic acid-rich Mediterranean diet seems to be more efficient than presently used diets in the secondary prevention of coronary events and death."

https://pubmed.ncbi.nlm.nih.gov/31327110/

"This study shows that substituting plant protein for animal protein, especially red meat protein, is associated with a reduced risk of CRC, and suggests that protein source impacts CRC risk."

https://pubmed.ncbi.nlm.nih.gov/31682257/

"In this large prospective study, higher plant protein intake was associated with lower total and CVD-related mortality. Although animal protein intake was not associated with mortality outcomes, replacement of red meat protein or processed meat protein with plant protein was associated with lower total, cancer-related, and CVD-related mortality."

https://pubmed.ncbi.nlm.nih.gov/33411911/

https://pubmed.ncbi.nlm.nih.gov/32699048/

https://pubmed.ncbi.nlm.nih.gov/33624505/

https://pubmed.ncbi.nlm.nih.gov/33411911/

#disease 
#coronary_heart_disease 
#cardiovascular_disease
#cancer
#all_cause_mortality
#meat 
#red_meat
#white_meat 
#processed_meat 
#colorectal_cancer
#diary
#legumes
#soy
#nuts
#eggs
#animal_foods
#animal_protein 
#plant_foods 
#plant_protein 
#a_linolenic_acid 
#omega_3 
#nutrition