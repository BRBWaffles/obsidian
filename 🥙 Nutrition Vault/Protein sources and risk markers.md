https://pubmed.ncbi.nlm.nih.gov/25351652/

"Dietary advice given for substitution of red meat with legume intakes within a TLC diet-improved lipid profiles and glycemic control among diabetes patients, which were independent from BMI change."

https://pubmed.ncbi.nlm.nih.gov/17344494/

"Short-term soy-nut consumption improved glycemic control and lipid profiles in postmenopausal women with the metabolic syndrome."

https://pubmed.ncbi.nlm.nih.gov/22215165/

https://pubmed.ncbi.nlm.nih.gov/32314018/

#disease
#cardiovascular_disease
#insulin_sensitivity
#metabolic_syndrome
#insulin_sensitivity
#LDL
#blood_glucose
#blood_lipids
#red_meat
#legumes
#anthropometics 
#energy_expenditure 
#weight_gain 
#weight_loss 
#blood_glucose
#insulin
#weight_loss
#nutrition