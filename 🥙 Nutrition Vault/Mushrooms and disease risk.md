https://pubmed.ncbi.nlm.nih.gov/24691133/

"In conclusion, the results from this meta-analysis suggested that greater edible mushroom consumption may be associated with a lower risk of breast cancer. Our research provided a perspective that oral administration of mushrooms perhaps contribute to breast cancer primary prevention. Whereas available data are still sparse, the findings need to be updated and confirmed with well-designed prospective studies in future."

#disease
#premenopausal
#postmenopausal
#breast_cancer
#plant_foods
#nutrition