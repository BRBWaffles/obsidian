Schoeneck, Malin, and David Iggman. ‘The Effects of Foods on LDL Cholesterol Levels: A Systematic Review of the Accumulated Evidence from Systematic Reviews and Meta-Analyses of Randomized Controlled Trials’. _Nutrition, Metabolism, and Cardiovascular Diseases: NMCD_, vol. 31, no. 5, May 2021, pp. 1325–38. _PubMed_, https://doi.org/10.1016/j.numecd.2020.12.032.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/33762150/

**Foods and LDL:**
![[Pasted image 20220220134337.png]]

**Conclusions:**
>In conclusion, several foods can distinctly modify LDL cholesterol levels. This updated summary of the accumulated evidence may help inform clinicians and future guidelines for dyslipidemia and CVD prevention.

**PDF:**
[[📂 Media/PDFs/PIIS0939475321000028.pdf]]

**Supplements:**
[[📂 Media/Misc/mmc1 (1).docx]]
[[📂 Media/PDFs/mmc2.pdf]]

| **Endpoints** | **Exposures**  | **Populations** | **General**           | **People** |
| ------------- | -------------- | --------------- | --------------------- | ---------- |
| #LDL          | #sugar         | #humans         | #nutrition            |            |
|               | #whole_grains  |                 | #blood_lipids         |            |
|               | #soluble_fibre |                 | #vegan_talking_points |            |
|               | #flaxseeds     |                 | #disease              |            |
|               | #soy           |                 | #systematic_review    |            |
|               | #fish          |                 | #meta_analysis        |            |
|               | #legumes       |                 |                       |            |
|               | #hazelnuts     |                 |                       |            |
|               | #walnuts       |                 |                       |            |
|               | #almonds       |                 |                       |            |
|               | #vegetable_oil |                 |                       |            |
|               | #olive_oil     |                 |                       |            |
|               | #animal_fats   |                 |                       |            |
|               | #saturated_fat           |                 |                       |            |
|               | #polyunsaturated_fat          |                 |                       |            |
|               | #avocados      |                 |                       |            |
|               | #turmeric      |                 |                       |            |
|               | #tomatoes      |                 |                       |            |
|               | #green_tea     |                 |                       |            |
|               | #coffee        |                 |                       |            |

****