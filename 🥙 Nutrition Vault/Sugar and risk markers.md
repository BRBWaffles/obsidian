https://pubmed.ncbi.nlm.nih.gov/22866961/

"Similar decreases in weight and indices of adiposity are observed when overweight or obese individuals are fed hypocaloric diets containing levels of sucrose or high fructose corn syrup typically consumed by adults in the United States."

https://pubmed.ncbi.nlm.nih.gov/15808893/

"These results show that when caloric beverages are consumed with a meal they add to energy intake from food, without significantly affecting satiety ratings."

https://pubmed.ncbi.nlm.nih.gov/27001643/

#disease 
#weight_loss 
#weight_gain 
#energy_intake 
#anthropometics
#type_2_diabetes 
#coronary_heart_disease 
#obesity 
#hypocaloric
#energy_intake 
#processed_food 
#satiety 
#sugar
#sugar_sweetened_beverages
#carbohydrates 
#fructose 
#nutrition

