https://www.ahajournals.org/doi/10.1161/JAHA.121.024014

**PDF:**
[[JAHA.121.024014.pdf]]

**Supplement:**
[[📂 Media/PDFs/jah37280-sup-0001-tables1-s10-figs1 (1).pdf]]

#nutrition 
#avocados
#vegetable_oil 
#cardiovascular_disease 
#disease 
#polyunsaturated_fat 
#monounsaturated_fat 
