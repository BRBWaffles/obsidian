Wißing, Christoph, et al. ‘Stable Isotopes Reveal Patterns of Diet and Mobility in the Last Neandertals and First Modern Humans in Europe’. _Scientific Reports_, vol. 9, no. 1, Mar. 2019, p. 4433. _PubMed_, https://doi.org/10.1038/s41598-019-41033-3.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/30872714/

**Upper Pleistocene Modern Humans Trophic Placement:**
![[📂 Media/PDFs/Pasted image 20220219162019.png]]

**Conclusions:**
>Our results indicate that UPMHs exploited their environment to a greater extent than Neandertals and support the hypothesis that UPMHs had a substantial impact not only on the population dynamics of large mammals but also on the whole structure of the ecosystem since their initial arrival in Europe.

**PDF:**
[[41598_2019_41033_MOESM1_ESM.pdf]]

**Supplements:**
[[📂 Media/PDFs/41598_2019_Article_41033.pdf]]

| **Exposures**   | **Populations**     | **General**          | **People** |
| --------------- | ------------------- | -------------------- | ---------- |
| #protein        | #primitive_cultures | #anthropology        |            |
| #animal_protein | #hunter_gatherers   | #nutrition           |            |
| #plant_protein  | #humans             | #ancestral_food      |            |
|                 |                     | #evolution           |            |
|                 |                     | #carnivore           |            |
|                 |                     | #low_carb_talking_points |            |

****

Drucker, Dorothée G., et al. ‘Isotopic Analyses Suggest Mammoth and Plant in the Diet of the Oldest Anatomically Modern Humans from Far Southeast Europe’. _Scientific Reports_, vol. 7, no. 1, July 2017, p. 6833. _PubMed_, https://doi.org/10.1038/s41598-017-07065-3.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/28754955/

**Human Trophic Placement:**
![[📂 Media/PDFs/Pasted image 20220219162427.png]]

**Conclusions:**
>Despite limited sample size, the case of Buran-Kaya III, with its exceptional archaeological context and good collagen preservation, shows that the mammoth could be the source of such high 15N signal and suggests that it should be more systematically considered as an alternative explanation to aquatic resources. Isotopic studies of western European late Neanderthals also point to the significant consumption of mammoth as well11, 39. Tus, the role of mammoth in human subsistence during the early Upper Palaeolithic should be further examined in future research.

**PDF:**
[[📂 Media/PDFs/41598_2017_7065_MOESM1_ESM.pdf]]

**Supplements:**
[[📂 Media/PDFs/41598_2019_41033_MOESM1_ESM 1.pdf]]

| **Exposures**   | **Populations**     | **General**          | **People** |
| --------------- | ------------------- | -------------------- | ---------- |
| #protein        | #primitive_cultures | #anthropology        |            |
| #animal_protein | #hunter_gatherers   | #nutrition           |            |
| #plant_protein  | #humans             | #ancestral_food      |            |
|                 |                     | #low_carb_talking_points |            |
|                 |                     | #carnivore           |            |

****

Ben-Dor, Miki, et al. ‘The Evolution of the Human Trophic Level during the Pleistocene’. _American Journal of Physical Anthropology_, vol. 175 Suppl 72, Aug. 2021, pp. 27–56. _PubMed_, https://doi.org/10.1002/ajpa.24247.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/33675083/

**Human Trophic Placement:**
![[📂 Media/PDFs/Pasted image 20220219162715.png]]

**Conclusions:**
>We conclude that it is possible to reach a credible reconstruction of the HTL without relying on a simple analogy with recent hunter-gatherers' diets. The memory of an adaptation to a trophic level that is embedded in modern humans' biology in the form of genetics, metabolism, and morphology is a fruitful line of investigation of past HTLs, whose potential we have only started to explore.

**PDF:**
[[📂 Media/PDFs/American J Phys Anthropol - 2021 - Ben‐Dor - The evolution of the human trophic level during the Pleistocene.pdf]]

| **Exposures**   | **Populations**     | **General**          | **People** |
| --------------- | ------------------- | -------------------- | ---------- |
| #protein        | #primitive_cultures | #anthropology        |            |
| #animal_protein | #hunter_gatherers   | #nutrition           |            |
| #plant_protein  | #humans             | #ancestral_food      |            |
|                 |                     | #low_carb_talking_points |            |
|                 |                     | #carnivore           |            |

****