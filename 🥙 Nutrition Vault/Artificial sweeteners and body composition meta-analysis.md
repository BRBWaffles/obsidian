Miller, Paige E., and Vanessa Perez. ‘Low-Calorie Sweeteners and Body Weight and Composition: A Meta-Analysis of Randomized Controlled Trials and Prospective Cohort Studies’. _The American Journal of Clinical Nutrition_, vol. 100, no. 3, Sept. 2014, pp. 765–77. _PubMed_, https://doi.org/10.3945/ajcn.113.082826.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/24944060/

**Forest Plots:**
![[Pasted image 20220222185056.png]]

**Conclusions:**
>In conclusion, the meta-analysis of observational studies showed a small positive association between LCS intake and BMI, but no association with body weight or fat mass. On the other hand, data from RCTs, which provide the highest quality of evidence for examining the potentially causal effects of LCS intake on body weight, indicate that substituting LCSs for calorically dense alternatives results in a modest reduction of body weight, BMI, fat mass, and waist circumference. Compared with the consistent findings among the RCTs, results from prospective cohort studies were limited and more difficult to interpret, particularly because of inadequate control of important confounders, including total energy intake and baseline differences between LCS consumers and nonconsumers in body weight and composition. On the basis of the available scientific literature to date, substituting LCS options for their regular-calorie versions results in a modest weight loss and may be a useful dietary tool to improve compliance with weight-loss or weight-maintenance plans.

**PDF:**
[[📂 Media/PDFs/765.pdf]]

**Supplements:**
[[📂 Media/Misc/113.082826_ajcn082826SupplementaryData1.docx]]

| **Endpoints**   | **Exposures**          | **Populations** | **General**        | **People** |
| --------------- | ---------------------- | --------------- | ------------------ | ---------- |
| #weight_loss    | #artificial_sweeteners | #humans         | #nutrition         |            |
| #weight_gain    |                        |                 | #meta_analysis     |            |
| #body_weight    |                        |                 | #systematic_review |            |
| #anthropometics |                        |                 | #disease           |            |
| #BMI            |                        |                 | #overweight        |            |
|                 |                        |                 | #obesity           |            |
|                 |                        |                 | #epidemiology      |            |

****