Klein, Samuel, et al. ‘Why Does Obesity Cause Diabetes?’ _Cell Metabolism_, vol. 34, no. 1, Jan. 2022, pp. 11–20. _PubMed_, https://doi.org/10.1016/j.cmet.2021.12.012.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/34986330/

**Conclusions:**
>Obesity, particularly when associated with increased abdominal and intra-abdominal fat distribution and increased intrahepatic and intramuscular triglyceride content, is a major risk factor for prediabetes and type 2 diabetes because it causes both insulin resistance and b cell dysfunction. Accordingly, the worldwide increase in the prevalence of obesity has led to the concomitant increase in the prevalence of type 2 diabetes. A better understanding of the mechanisms responsible for the adverse effects of excess body fat on the factors involved in the pathogenesis of type 2 diabetes can lead to novel therapeutic interventions to prevent and treat this debilitating disease. A series of studies conducted in mouse models and in people have demonstrated alterations in adipose tissue biology that link obesity with insulin resistance and b cell dysfunction. These alterations include adipose tissue fibrosis (increased rates of fibrogenesis and expression of genes involved in extracellular matrix formation), inflammation (increased proinflammatory macrophage and T cell content and the production of PAI-1), and the production of exosomes that can induce insulin resistance. However, none of these factors can influence systemic metabolic function without a mechanism for adipose tissue communication with other organs. It is possible that several adipose tissue secretory products that are released into the bloodstream—including PAI-1, adiponectin, FFAs, and exosomes—are involved in this signaling process, but additional research is needed to fully assess their clinical importance. In addition, it is also likely that crosstalk among adipose tissue, the liver, muscle, and pancreatic islets contribute to insulin resistance and hepatic steatosis (Figure 3). Decreasing body fat mass by inducing a negative energy balance, not by surgical removal, can ameliorate or normalize obesity-induced metabolic dysfunction and can even achieve diabetes remission if there is adequate restoration of b cell function.

**PDF:**
[[📂 Media/PDFs/Why_does_obesity_cause_diabetes.pdf]]

| **Endpoints**        | **Exposures** | **Populations** | **General**      | **People** |
| -------------------- | ------------- | --------------- | ---------------- | ---------- |
| #type_2_diabetes                | #obesity      | #humans         | #disease         |            |
| #insulin_sensitivity |               | #mice           | #physiology      |            |
|                      |               |                 | #pathophysiology |            |
|                      |               |                 | #nutrition       |            |

****