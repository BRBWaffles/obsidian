Loria Kohen, V., et al. ‘Impact of Two Low-Calorie Meals with and without Bread on the Sensation of Hunger, Satiety and Amount of Food Consumed’. _Nutricion Hospitalaria_, vol. 26, no. 5, Oct. 2011, pp. 1155–60. _PubMed_, https://doi.org/10.1590/S0212-16112011000500035.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/22072367/

**Conclusions:**
>The inclusion of bread in a low-calorie meal may result in a greater sensation of satiety after eating. These results contradict the recommendation to exclude bread from a food plan aimed at weight loss.

**PDF:**


**Supplements:**


| **Endpoints** | **Exposures**   | **Populations** | **General**    | **People** |
| ------------- | --------------- | --------------- | -------------- | ---------- |
| #satiety      | #bread          | #humans         | #disease       |            |
| #body_weight  | #refined_grains | #women          | #energy_intake |            |
|               |                 |                 | #nutrition     |            |

****