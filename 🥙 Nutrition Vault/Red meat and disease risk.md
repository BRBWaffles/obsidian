Saito, Eiko, et al. ‘Association between Meat Intake and Mortality Due to All-Cause and Major Causes of Death in a Japanese Population’. _PloS One_, vol. 15, no. 12, 2020, p. e0244007. _PubMed_, https://doi.org/10.1371/journal.pone.0244007.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/33320898/

**Population Characteristics:**
![[Pasted image 20220213220316.png]]

**Disease Risk (Men):**
![[Pasted image 20220213220410.png]]

**Adjustment Model:**
![[Pasted image 20220213220546.png]]

**Disease Risk (Women):**
![[Pasted image 20220213220456.png]]

**Adjustment Model:**
![[Pasted image 20220213220603.png]]

**Disease Risk by Age:**
![[Pasted image 20220213220831.png]]

**Adjustment Model:**
![[Pasted image 20220213220841.png]]

**Conclusion:**
>In conclusion, we found that greater consumption of unprocessed and processed red meats is associated with higher mortality risk. Compared with red meat, other dietary components, such as fish, poultry, nuts, legumes, low-fat dairy products, and whole grains, were associated with lower risk. These results indicate that replacement of red meat with alternative healthy dietary components may lower the mortality risk.

**Notes:**

**PDF:**
[[📂 Media/PDFs/journal.pone.0244007.pdf]]

| **Endpoints**      | **Exposures** | **Populations**             | **General**          | **People** |     |
| ------------------ | ------------- | --------------------------- | -------------------- | ---------- | --- |
| #cancer            | #red_meat     | #japan_public_health_center | #mortality           |            |     |
| #cardiovascular_disease               | #animal_foods | #japan                      | #disease             |            |     |
| #all_cause_mortality               | #beef         | #men                        | #healthy_user_bias   |            |     |
| #stroke            | #pork         | #women                      | #low_carb_talking_points |            |     |
| #colorectal_cancer | #chicken      |                             | #nutrition           |            |     |
|                    |               |                             | #dose_response       |            |     |

****

Pan, An, et al. ‘Red Meat Consumption and Mortality: Results from 2 Prospective Cohort Studies’. _Archives of Internal Medicine_, vol. 172, no. 7, Apr. 2012, pp. 555–63. _PubMed_, https://doi.org/10.1001/archinternmed.2011.2287.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/22412075/

**Men Characteristics:**
![[Pasted image 20220213212514.png]]

**Women Characteristics:**
![[Pasted image 20220213212533.png]]

**All Cause Mortality:**
![[Pasted image 20220213212640.png]]

**Adjustment Model:**
![[Pasted image 20220213212704.png]]

**CVD Mortality:**
![[Pasted image 20220213212809.png]]

**Adjustment Model:**
![[Pasted image 20220213212829.png]]

**Cancer Mortality:**
![[Pasted image 20220213212843.png]]

**Adjustment Model:**
![[Pasted image 20220213212857.png]]

**Dose-Response:**
![[Pasted image 20220213212948.png]]

**Adjustment Model:**
![[Pasted image 20220213213007.png]]

**Substitution Analysis:**
![[Pasted image 20220213213138.png]]

**Conclusion:**
![[Pasted image 20220213213202.png]]

**Notes:**
Healthy-user bias was mildly present

**PDF:**
[[📂 Media/PDFs/ioi110027_555_563.pdf]]

 | **Endpoints** | **Exposures**   | **Populations**                      | **General**            | **People**      |     |
 | ------------- | --------------- | ------------------------------------ | ---------------------- | --------------- | --- |
 | #cancer       | #red_meat       | #nurses_health_study                 | #mortality             | #walter_willett |     |
 | #cardiovascular_disease          | #animal_foods   | #health_professionals_followup_study | #disease               | #frank_hu       |     |
 | #all_cause_mortality          | #meat           | #united_states                       | #substitution_analyses |                 |     |
 |               | #processed_meat |                                      | #nutrition             |                 |     |
 |               |                 |                                      | #cohort_studies        |                 |     |

****

Zheng, Yan, et al. ‘Association of Changes in Red Meat Consumption with Total and Cause Specific Mortality among US Women and Men: Two Prospective Cohort Studies’. _BMJ (Clinical Research Ed.)_, vol. 365, June 2019, p. l2110. _PubMed_, https://doi.org/10.1136/bmj.l2110.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/31189526/

**Red Meat and Mortality Risk:**
![[Pasted image 20220214171128.png]]

**Substitution Analysis:**
![[Pasted image 20220214171156.png]]

**Conclusions:**
>Increases in red meat consumption, especially processed meat, over eight years were associated with a higher risk of death in the subsequent eight years in US women and men. Increased consumption of healthier animal or plant foods was associated with a lower risk of death compared with red meat consumption. Our analysis provides further evidence to support the replacement of red and processed meat consumption with healthy alternative food choices.

**PDF:**
[[📂 Media/PDFs/bmj.l2110.full.pdf]]

**Supplements:**
[[📂 Media/PDFs/zhey047253.ww1.pdf]]

| **Endpoints**            | **Exposures**   | **Populations**                      | **General**   | **People**      |
| ------------------------ | --------------- | ------------------------------------ | ------------- | --------------- |
| #all_cause_mortality                     | #red_meat       | #nurses_health_study                 | #disease      | #walter_willett |
| #cardiovascular_disease                     | #processed_meat | #health_professionals_followup_study | #nutrition    | #frank_hu       |
| #neurodegerative_disease | #meat           | #humans                              | #epidemiology | #yan_zheng      |
|                          | #nuts           | #male_health                         | #mortality    |                 |
|                          | #poultry        | #female_health                       |               |                 |
|                          | #fish           |                                      |               |                 |
|                          | #dairy          |                                      |               |                 |
|                          | #eggs           |                                      |               |                 |
|                          | #legumes        |                                      |               |                 |
|                          | #whole_grains   |                                      |               |                 |
|                          | #vegetables     |                                      |               |                 |

****

https://zbib.org

**Link:**


****


**Conclusions:**
>

**PDF:**
[[📂 Media/PDFs/Red_meat_consumption_and_allcause_and_cardiovascular_mortality-_results_from_the_UK_Biobank_study.pdf]]

**Supplements:**


| **Endpoints** | **Exposures** | **Populations** | **General** | **People** |
| ------------- | ------------- | --------------- | ----------- | ---------- |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |
|               |               |                 |             |            |

****