_Impact of Supplementation with Milk-Cereal Mix during 6-12 Months of Age on Growth at 12 Months: A 3-Arm Randomized Controlled Trial in Delhi, India by Sunita Taneja – FreeMedArt — Free Medical Articles_. https://freemedart.ru/impact-of-supplementation-with-milk-cereal-mix-during-6-12-months-of-age-on-growth-at-12-months-a-3-arm-randomized-controlled-trial-in-delhi-india-by-sunita-taneja/. Accessed 20 Feb. 2022.

**Link:**
https://academic.oup.com/ajcn/article/115/1/83/6391404

**Conclusions:**
>In conclusion, supplementation during the second half of infancy, for a period of 180 d, with a cereal mix having a higher quantity of milk-based protein with added MMN leads to improvement in linear growth and other anthropometric indexes (weight-for-age, weight-for-length, and MUAC) in children from low-resource settings, compared with no supplementation. Complementary feeding programs may consider providing foods with high-quality, particularly milk-based, protein and MMN. However, the increase in the cost of the supplements due to increased protein content will need to be considered, especially when planning for a large-scale rollout.

**PDF:**
[[📂 Media/PDFs/Impact_of_supplementation_with_milkcereal_mix_during_612_months_of_age_on_growth_at_12_months-_a_3-arm_randomized_controlled_trial_in_Delhi_India.pdf]]

| **Endpoints**   | **Exposures**   | **Populations** | **General**          | **People** |
| --------------- | --------------- | --------------- | -------------------- | ---------- |
| #growth         | #cereals        | #infants        | #nutrition           |            |
| #anthropometics | #whole_grains   | #children       | #child_development   |            |
|                 | #refined_grains | #humans         | #low_carb_talking_points |            |
|                 | #whey           |                 |                      |            |
|                 | #dairy          |                 |                      |            |
|                 | #animal_foods   |                 |                      |            |
|                 | #plant_foods    |                 |                      |            |
|                 | #animal_protein |                 |                      |            |

****