https://pubmed.ncbi.nlm.nih.gov/33300582/

"These findings suggest that slightly increasing current protein intake for several months by 0.1 g/kg/d in a dose-dependent manner over a range of doses from 0.5 to 3.5 g/kg/d may increase or maintain lean body mass."

https://pubmed.ncbi.nlm.nih.gov/28385919/

https://pubmed.ncbi.nlm.nih.gov/28698222/

#protein
#exercise
#strength
#hypertrophy
#nutrition