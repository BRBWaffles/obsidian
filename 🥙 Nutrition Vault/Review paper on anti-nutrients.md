Petroski, Weston, and Deanna M. Minich. ‘Is There Such a Thing as “Anti-Nutrients”? A Narrative Review of Perceived Problematic Plant Compounds’. _Nutrients_, vol. 12, no. 10, Sept. 2020, p. E2929. _PubMed_, https://doi.org/10.3390/nu12102929.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/32987890/

**Suggested Implications of Anti-Nutrients:**
![[📂 Media/PDFs/Pasted image 20220219163552.png]]

**Conclusions:**
>- **Lectins:** Until further human clinical trials demonstrate otherwise, the health-promoting effects of lectin-containing foods would seem to far outweigh any possible negative effects of lectins.
>- **Oxalate:** Oxalate containing foods possess an array of protective, beneficial compounds which may outweigh any possible negative effects of oxalate.
>- **Goitrogens:** For those with thyroid disease, or at higher risk of thyroid disease, long-term daily intake of progoitrin-rich items, like Russian kale, broccoli rabe or collard greens may decrease iodine uptake, and should be cooked with iodized salt to avoid reduced iodine uptake.
>- **Phytoestrogens:** Phytoestrogen-containing foods such as legumes, grains, seeds, nuts, fruits, and vegetables, are rich sources of essential vitamins, minerals, fiber and other health-promoting phytochemicals.
>- **Phytates:** Overall, by consuming a colorful, plant-based diet, the benefits of phytate containing foods to human health exceed the impacts on mineral absorption.
>- **Tannins:** Overall, evidence suggests that the many health benefits of consuming a diverse, plant-based diet, rich in polyphenol and bioactive containing foods and beverages, far outweighs the potential impact of tannins on iron status.

**PDF:**
[[📂 Media/PDFs/nutrients-12-02929.pdf]]

| **Endpoints**       | **Exposures**   | **Populations** | **General**          | **People** |
| ------------------- | --------------- | --------------- | -------------------- | ---------- |
| #nutritional_status | #lectins        | #humans         | #nutrition           |            |
| #hypothyroidism     | #oxalate        | #mice           | #minerals            |            |
| #kidney_stones      | #goitrogens     |                 | #low_carb_talking_points |            |
| #hormones           | #phytoestrogens |                 | #dunk_papers         |            |
| #sex_hormones       | #phytates       |                 |                      |            |
| #gut_health         | #tannins        |                 |                      |            |
| #inflammation       | #antinutrients  |                 |                      |            |

****