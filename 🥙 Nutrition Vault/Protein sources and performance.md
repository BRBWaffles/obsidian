https://pubmed.ncbi.nlm.nih.gov/29722584/

"Overall, the results indicate that protein source is not likely an important factor influencing gains in strength and LBM in response to RET"

https://pubmed.ncbi.nlm.nih.gov/25628520/

"In addition to an appropriate training, the supplementation with pea protein promoted a greater increase of muscle thickness as compared to Placebo and especially for people starting or returning to a muscular strengthening. Since no difference was obtained between the two protein groups, vegetable pea proteins could be used as an alternative to Whey-based dietary products."

https://pubmed.ncbi.nlm.nih.gov/32486007/

"These data indicate that increases in lean mass and strength in untrained participants are comparable when strength training and supplementing with soy or whey matched for leucine."

https://pubmed.ncbi.nlm.nih.gov/30621129/

"We conclude that ingestion of whey and pea protein produce similar outcomes in measurements of body composition, muscle thickness, force production, WOD performance and strength following 8-weeks of HIFT."

https://pubmed.ncbi.nlm.nih.gov/33726784/

"In conclusion, increasing daily protein intake to 1.5 g/kg through ingestion of either whey or soy protein supplements mitigates field performance deterioration during successive speed-endurance training sessions without affecting exercise-induced muscle damage and redox status markers."

https://pubmed.ncbi.nlm.nih.gov/22066824/

"Our data suggest that resistance training, particularly in combination with a soy protein based supplement improves body composition and metabolic function in middle aged untrained and moderately overweight males."

https://pubmed.ncbi.nlm.nih.gov/22338070/

"We conclude that ingestion of 35 g whey protein results in greater amino acid absorption and subsequent stimulation of de novo muscle protein synthesis compared with the ingestion of 10 or 20 g whey protein in healthy, older men."

https://pubmed.ncbi.nlm.nih.gov/30289425/

"Protein supplementation did not confer a benefit in protecting LLM, but only supplemental WP augmented LLM and muscle protein synthesis during recovery from inactivity and a hypoenergetic state."

https://matt576.medium.com/protein-in-a-plant-based-diet-8a9763ffb8e3

#strength
#hypertrophy
#exercise
#protein
#plant_protein
#animal_protein
#animal_foods
#plant_foods 
#nutrition