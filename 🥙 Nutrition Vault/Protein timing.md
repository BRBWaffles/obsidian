https://pubmed.ncbi.nlm.nih.gov/24299050/

"These results refute the commonly held belief that the timing of protein intake in and around a training session is critical to muscular adaptations and indicate that consuming adequate protein in combination with resistance exercise is the key factor for maximizing muscle protein accretion."

https://pubmed.ncbi.nlm.nih.gov/28698222/

"Dietary protein supplementation significantly enhanced changes in muscle strength and size during prolonged RET in healthy adults. Increasing age reduces and training experience increases the efficacy of protein supplementation during RET. With protein supplementation, protein intakes at amounts greater than ~1.6 g/kg/day do not further contribute RET-induced gains in FFM."

#strength
#exercise
#protein
#hypertrophy
#nutrition