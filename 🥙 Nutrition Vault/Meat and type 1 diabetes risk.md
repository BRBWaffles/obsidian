Syrjälä, Essi, et al. ‘A Joint Modeling Approach for Childhood Meat, Fish and Egg Consumption and the Risk of Advanced Islet Autoimmunity’. _Scientific Reports_, vol. 9, no. 1, May 2019, p. 7760. _PubMed_, https://doi.org/10.1038/s41598-019-44196-1.

**Link:**
https://pubmed.ncbi.nlm.nih.gov/31123290/

**Risk of T1DM:**
![[Pasted image 20220219212204.png]]

**Conclusions:**
>In conclusion, our findings from this study suggest that a child’s intake of meat might be directly, and fish inversely, related to the development of advanced IA and T1D. Disease hazards in longitudinal nutritional epidemiology are more appropriately and efficiently modeled by joint models than with naive approaches.

**PDF:**
[[📂 Media/PDFs/s41598-019-44196-1.pdf]]

**Supplements:**
[[📂 Media/PDFs/41598_2019_44196_MOESM1_ESM.pdf]]

| **Endpoints**   | **Exposures**   | **Populations** | **General**   | **People** |
| --------------- | --------------- | --------------- | ------------- | ---------- |
| #T1DM           | #meat           | #infants        | #nutrition    |            |
| #autoantibodies | #red_meat       | #children       | #disease      |            |
|                 | #eggs           | #humans         | #epidemiology |            |
|                 | #poultry        |                 | #auto_immune  |            |
|                 | #animal_foods   |                 |               |            |
|                 | #animal_protein |                 |               |            |

****