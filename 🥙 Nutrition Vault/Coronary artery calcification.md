https://pubmed.ncbi.nlm.nih.gov/22473330/

"A detailed understanding of bone-vascular interactions is necessary to address the unmet clinical needs of an increasingly aged and dysmetabolic population."

https://pubmed.ncbi.nlm.nih.gov/14500910/

"Because atherosclerosis is a chronic vascular inflammation, we propose that arterial plaque calcification is best conceptualized as a convergence of bone biology with vascular inflammatory pathobiology."

https://pubmed.ncbi.nlm.nih.gov/29301708/

"We will review the pathology of coronary calcification in humans with a focus on risk factors, relationship with plaque progression, correlation with plaque (in)stability, and effect of pharmacologic interventions."

#blood_lipids 
#coronary_artery_calcification 
#LDL
#inflammation
#lipidology 
#bones
#disease 