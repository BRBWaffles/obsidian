https://pubmed.ncbi.nlm.nih.gov/23274906/

"These findings question the respective roles of dietary proteins and endogenous sources in generating significant amounts of glucose in order to maintain blood glucose levels in healthy subjects."

#gluconeogenesis
#low_carb_talking_points 
#protein 
#keto 
#nutrition 