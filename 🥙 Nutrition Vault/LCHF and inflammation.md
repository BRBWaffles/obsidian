https://pubmed.ncbi.nlm.nih.gov/17536128/

"Diet composition of the weight loss diet influenced a key marker of inflammation in that LC increased while HC reduced serum CRP but evidence did not support that this was related to oxidative stress."

#disease 
#inflammation 
#low_carb
#c_reactive_protein
#weight_loss
#blood_glucose
#nutrition 


